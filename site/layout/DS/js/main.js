var admInputs;
var admAlertAssigned = false;

function fieldError( formName, fields ) {
	$( "#"+formName ).unbind('submit');//have to
	var validator = $( "#"+formName ).validate();
	$( "#"+formName ).submit( function () {
		$( 'select.demographic-filter option' ).attr( "selected", "selected" )
	} );

	var f = JSON.parse(fields);
	var scrolled = false;
	$.each( f.errors, function ( a, b ) {
		if(!scrolled){
			scrollTo($( '#'+a ));
			scrolled = true;
		}
	} );
	validator.showErrors(
		f.errors
	);

	setTimeout(function(){
		var bt = $( 'button[type="submit"].bloading' );
		bt.removeClass( 'bloading' ).html( bt.attr('vhtml') ).css('background-color','' ).unbind('click');
	},1000);

	assignForms();

}

function scrollTo(o){
	var top = 0;
	if (o != undefined && o != null && typeof o.offset == 'function' && o.offset() != null && o.offset().top != null)
		top = o.offset().top;
	$('html, body').stop().animate({
		scrollTop: (top-50)
	}, 1500,'easeInOutExpo');
}

function clearErrorMessage(){
	$( "label.error" ).remove();
	$( "label.has-error" ).remove();
	$( 'div.has-error' ).removeClass( 'has-error' );
	render();
}

function goIfOk(url){
	if(confirm('Are you sure?'))
		location.href = url;
}

function message(type,message){
	switch(type){
		default:
		case 'good':
			noty({
				text: '<strong>'+message+'</strong>',
				type: 'success',
				timeout: 2000
			});
			break;
		case 'bad':
			noty({
				text: '<strong>'+message+'</strong>',
				type: 'error',
				timeout: 2000
			});
			break;
	}
}

function pheew(id){
	$('#'+id).fadeOut()
}

function noHtml(id){
	$('#'+id).html("")
}

var changeAlert = function(){
	if (!admAlertAssigned){
		window.onbeforeunload = function() {
			return "Your changes have not been saved yet. Are you sure to continue?";
		};
		admAlertAssigned = true;
	}
}

var flushChangeAlert = function(){
	admInputs.unbind("change",changeAlert);
	admAlertAssigned = false;
	window.onbeforeunload = null;
	admInputs.bind("change",changeAlert);
}

var msg = function(msg,type,timeout){
	if (type == undefined) type = "success";
	if (timeout == undefined) timeout = 1000;
	noty({text: msg,type: type, timeout: timeout});
};

$(document).ready(function(){
	admInputs = $("input, select, textarea");
	admInputs.bind("change",changeAlert);

	$("button.submit").bind("click",function(){
		flushChangeAlert();
	});

	$(".datepicker").each(function(i,v){
		$(v).datepicker({dateFormat:$(v).attr("dateFormat")});
	});
});


jQuery.extend( jQuery.easing,
	{
		def: 'easeOutQuad',
		easeInOutExpo: function (x, t, b, c, d) {
			if (t==0) return b;
			if (t==d) return b+c;
			if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
			return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
		}
	});

/**
 * returning value must be json > [["2","\u0130stanbul"]]
 * @param id
 * @param src
 */
function populateSelectByXHR( id, src, value ) {
	if(!value) value = 0 ;
	$.ajax({
		url: src,
		beforeSend: function( xhr ) { xhr.overrideMimeType( "text/plain; charset=x-user-defined" ); }
	}).done( function ( data ) {
		var values = JSON.parse( data );
		var s0 = $( '#' + id );
		s0.find( 'option' ).remove();
		if ( s0.attr( 'data-chooseOne' ) )
			s0.append( '<option value="">' + s0.attr( 'data-chooseOne' ) + '</option>' )

		if ( values.length > 0 ) {
			for ( var i0 = 0; i0 < values.length; i0++ ) {
				s0.append( '<option value="' + values[i0][0] + '" ' + (values[i0][0] == value ? "selected" : "") + '>' + values[i0][1] + '</option>' )
			}
		}
		else {
			s0.append( '<option value="">' + s0.attr( 'defaultValue' ) + '</option>' )
		}
	});
}


function notifik( mes, ttype, timeoutSec, redirectAfter ) {
	var title = 'info', type = 'success';

	if ( ttype == 'error' )
		title = type = 'error';

	message( type, mes );

	if ( typeof redirectAfter != 'undefined' )
		setTimeout( function () {
			location.href = redirectAfter;
		}, timeoutSec * 1000 );
}

function openDashboard(){
	setTimeout(function(){
		window.open("/dashboard","adMingle");
	},1000);
	window.message("good","Publisher session set");

};
