function rowCB( nRow, aData, iDisplayIndex ) {
	var r = $(nRow ).find("td:last" );
	$( nRow ).attr( "id", "row_"+aData.rowId );
	r.append('<button class="btn btn-xs" href="/'+editUrl+'/'+aData.rowId+'"><i class="icon-edit bs-tooltip" data-placement="top"></i></button>');
	if (typeof specialButtons != "undefined"){
		$.each(specialButtons,function(k,v){
			r.append('<button class="btn btn-xs" href="/'+ v.action+'/'+aData.rowId+'" ' + (v.iframe ? 'data-iframe="ps"' : '') + '><i class="icon-' + v.icon + ' bs-tooltip" data-placement="top"></i></button>');
		});
	}
}

function drawCB() {
	render();
}