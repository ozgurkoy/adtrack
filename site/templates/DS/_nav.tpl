{*{$perms|@print_r}*}
{*{$userPerms|@print_r}*}
<ul id="nav">
	{assign var="currentLevel" value=0}
	{assign var="begin" value=0}
	{foreach from=$menus key=mid item=men}
	{if sizeof(array_intersect($userRoles, $men.roleIds))>0}
		{if $currentLevel<$men.level}
		<ul class="sub-menu">
		{/if}
		{if $currentLevel>$men.level}
		</li></ul></li>
		{/if}
		{if $currentLevel==$men.level && $begin!=0}
			</li>
		{/if}
		{assign var="begin" value=1}
		<li>
			{if sizeof($men.subMenus)>0}
				{foreach from=$men.subMenus item=sm}
					<a href="/{if strlen($sm)>2}{$sm|trim}{else}javascript:;{/if}" style="display: none"></a>
				{/foreach}
			{/if}
			<a href="/{$men.link}">
				<i class="{$men.css}"></i>
				{$men.label|ucfirst}
			</a>

		{assign var="currentLevel" value=$men.level}
	{/if}
	{/foreach}
	{if $currentLevel>0}</ul>{/if}</li>

	{if $isRoot==1}
    <li>
        <a href="javascript:;" style="display: none"><i class="arrow icon-angle-left"></i></a>
        <a href="javascript:;">
            <i class="icon-user"></i>
            User management
            <i class="arrow icon-angle-left"></i></a>

        <ul class="sub-menu">
            <li>
                <a href="/ds_addUser" style="display: none"></a>
                <a href="/ds_users">
                    <i class="icon-user"></i>
                    Users
                </a>

            </li>
            <li>
                <a href="/ds_roleMod" style="display: none"></a>
                <a href="/ds_addRole" style="display: none"></a>
                <a href="/ds_roles">
                    <i class="icon-code"></i>
                    Roles
                </a>

            </li>
            <li>
                <a href="/ds_addAction" style="display: none"></a>
                <a href="/ds_actions">
                    <i class="icon-road"></i>
                    Actions
                </a>

            </li>
        </ul>
    </li>
	<li>
		<a href="/ds_menus" style="display: none"></a>

		<a href="/ds_menus">
			<i class="icon-list"></i>
			MENUS
		</a>
	</li>
	{/if}

</ul>
<script>

	{*{if !(isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug)}*}
	if($.cookie('panelOpen')===null || $.cookie('panelOpen')!=='true' )
		$( '#container' ).addClass( 'sidebar-closed' );
	{*{/if}*}
</script>