<div class="crumbs">
	<ul id="breadcrumbs" class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/">Home</a>
		</li>
		{if sizeof($left)>0}
		{foreach from=$left item=lf}
		<li class="current">
			<a href="{if strlen($lf.0)>0}/{$lf.0}{else}javascript:;{/if}" title="">{$lf.1}</a>
		</li>
		{/foreach}
		{/if}
	</ul>
	{if sizeof($right)>0}
	<ul class="crumb-buttons">
		<li class="first"><a href="/{$right.0}" title=""><i class="icon-plus"></i><span>{$right.1}</span></a></li>
	</ul>
	{/if}
</div>
<div class="page-header"></div>