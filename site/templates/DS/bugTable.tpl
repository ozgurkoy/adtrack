
{if $_items->gotValue}
	{doWhile}
	{callUserFunc $_items->loadLastOperator()}
	{callUserFunc $_items->loadAssignee()}
	{callUserFunc $_items->loadOpener()}
	{callUserFunc $_items->historyCnt()}
	{assign var=st value=$_items->status}
	{assign var=pr value=$_items->priority}

		<tr class="bugDetailer" style="cursor: pointer" data-tid="{$_items->id}">
			<td>
				<strong>[{$_items->id}]</strong>
			</td>
			<td>
				&nbsp;
				&nbsp;
				{$_items->title}
				<span class="label label-info pull-left">{$_items->hc}</span>

			</td>
			{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}
			<td>
				{if strlen($_items->assignee->username)>0}
				@{$_items->assignee->username}
				{else}
				-
				{/if}
			</td>
			{/if}
			<td>
				{$_items->status_options.$st}
			</td>
			<td class="{$_items->priority_options.$pr}Prio  hidden-xxs">
				{$_items->priority_options.$pr}
			</td>
			<td class="hidden-xxs">
				{timeAgo time=$_items->jdate} ago by @{$_items->opener->username}
			</td>{*
												{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}

												<td>
													@{$_items->assignee->username}
												</td>
												{/if}*}

			<td class="hidden-xxs">
				{timeAgo time=$_items->lastOperation} ago by @{$_items->lastOperator->username}
			</td>{*
			{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}

				<td class="ops  hidden-xxs">
					<button class="btn btn-xs" href="/ds_addBug/{$type}/{$_items->id}"><i class="icon-cog"></i></button>
					<button class="btn btn-xs" href="javascript:goIfOk('/ds_delBug/{$_items->id}')"><i class="icon-remove"></i></button>
				</td>
			{/if}*}
		</tr>
	{/doWhile ($_items->populate() != false)}
{/if}
