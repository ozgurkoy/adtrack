{readPerms}
{include file="DS/_top.tpl"}
<script src="/site/layout/DS/js/main.js"></script>
<link href="/site/layout/DS/assets/css/main.css" rel="stylesheet" type="text/css" />
<link href="/site/layout/DS/css/custom.css" rel="stylesheet" type="text/css" />


<div class="tabbable tabbable-custom tabbable-full-width" style="overflow-x: hidden;">
	<div class="container">
	{include file="DS/_bugTabs.tpl"}
	<div class="tab-content row" style="padding-top: 50px">
		<div class="widget-content no-padding">
			<table class="table table-hover table-striped table-bordered table-highlight-head">
				<thead>
				<tr>
					<th class="col-md-10">User</th>
					<th class="col-md-2"></th>
				</tr>
				</thead>
				<tbody>
				{if $_items->gotValue}
				{doWhile}
				<tr>
					<td>
						{$_items->username}
					</td>
					<td>
						<button class="btn btn-xs" href="javascript:goIfOk('/ds_delFollower/{$id}/{$_items->id}')"><i class="icon-remove"></i></button>

					</td>
				</tr>
				{/doWhile ($_items->populate())}
				{/if}

	</div>
	</div>
</div>
	<script>
		{literal}

		$( document ).on("click","button[href]", function () {
			//console.log("button click");
			if ( $( this ).attr( 'data-blank' ) )
				window.open( $( this ).attr( 'href' ) );
			else if ( $( this ).attr( 'data-iframe' ) )
				window.ps.location.href = $( this ).attr( 'href' );
			else
				location.href = $( this ).attr( 'href' );
		} )
		{/literal}
	</script>