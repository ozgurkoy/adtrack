{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl"}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="/darkside">Home</a>
					</li>
					<li class="current">
						<a href="/ds_menus" title="">Menus</a>
					</li>
				</ul>
				<ul class="crumb-buttons">
					<li class="first"><a href="/ds_addMenu" title=""><i class="icon-plus"></i><span>New Menu</span></a></li>
				</ul>

			</div>
			<div class="page-header"></div>
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Menus</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding">
							<table class="table table-hover table-striped table-bordered table-highlight-head">
								<thead>
								<tr>
									<th>Label</th>
									{*<th>Word</th>*}
									<th>Roles</th>
									<th>Active</th>
									<th class="col-md-2">Action</th>
								</tr>
								</thead>
								<tbody>
								{if sizeof($menuList)>0}
									{foreach from=$menuList key=mid item=men}
										<tr>
											<td>{"&nbsp;"|str_repeat:$men.level*9}<i class="{$men.css}"></i> {$men.label|upper}</td>
											<td>{$men.roles}</td>
											<td>{booleanYes v=$men.isActive}</td>
											<td>
												{if $men.first!=true}
												<button class="btn btn-xs" href="/ds_moveUpMenu/{$mid}"><i class="icon-arrow-up"></i></button>
												{/if}
												<button class="btn btn-xs" href="/ds_addMenu/{$mid}"><i class="icon-cog"></i></button>
												<button class="btn btn-xs" href="javascript:goIfOk('/ds_delMenu/{$mid}')"><i class="icon-remove"></i></button>
											</td>
										</tr>
									{/foreach}
								{/if}
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="DS/footer.tpl"}
</body>
</html>