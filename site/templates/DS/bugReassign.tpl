{readPerms}
{include file="DS/_top.tpl"}
<div class="tabbable tabbable-custom tabbable-full-width" style="overflow-x: hidden;">

	{include file="DS/_bugTabs.tpl"}
	<div class="tab-content row" style="padding-top: 50px;overflow-x:hidden">
		<form class="form-horizontal row-border" action="/ds_assignBugAction/{$bug->id}" name="assignBug" id="assignBug" method="POST" target="ps"  >
			{getFormToken f="assignBug`$bug->id`"}
			{formBox type="populateSelect" label="Assignee" defaultValue="Choose Assignee" name="assignee" vkey="username" options=$users value=$bug->assignee}
			<div class="form-actions">
				<button type="submit" class="submit btn btn-primary pull-right">
					Submit <i class="icon-angle-right"></i>
				</button>
			</div>

		</form>

	</div>
</div>