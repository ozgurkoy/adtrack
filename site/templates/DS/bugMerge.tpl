{readPerms}
{include file="DS/_top.tpl"}
<div class="tabbable tabbable-custom tabbable-full-width" style="overflow-x: hidden;">

	{include file="DS/_bugTabs.tpl"}
	<div class="tab-content row" style="padding-top: 50px;overflow-x:hidden">
		<form class="form-horizontal row-border" action="/ds_bugMergeAction/{$bug->id}" name="mergeBug" id="mergeBug" method="POST" target="ps"  >
			{getFormToken f="mergeBug`$bug->id`"}
			{formBox type="populateSelect" label="Merge onto" defaultValue="Choose Bug to merge onto" name="mergeId" vkey="id,title" options=$bugs value=null}
			<div class="form-actions">
				<button type="submit" class="submit btn btn-primary pull-right">
					Submit <i class="icon-angle-right"></i>
				</button>
			</div>

		</form>

	</div>
</div>