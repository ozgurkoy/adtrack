{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>
	<script src="/site/layout/DS/js/jquery-ias.js"></script>
	<style>
		{literal}
		.noscroll {
			overflow-x: hidden;
			overflow-y: hidden;

		}
		.blurredElement {
			display:none;
			opacity: 0.5;
			background-color: #FFFFFF;
			position: fixed;
			z-index: 900;
			left: 0;
			right: 0;
			top: 0;
			bottom: 0;
		}
		.frameRow iframe {
			border: 0;
			width: 100%;
			/*height: 500px;*/
			-webkit-box-shadow: 0px -5px 147px -26px rgba(120,120,120,1);
			-moz-box-shadow: 0px -5px 147px -26px rgba(120,120,120,1);
			box-shadow: 0px -5px 147px -26px rgba(120,120,120,1);
			position: relative;
			z-index: 1000;
		}
		.highPrio{
			background-color: lightpink !important;
		}
		.lowPrio{
			background-color: lightcyan !important;
		}
		.mediumPrio{
			background-color: lightgoldenrodyellow !important;
		}
		.bugDetailer td{
			word-break: normal;
		}

		#loadMoreButton {
			margin: auto;
			width: 90px;
			height: 30px;
			background: url("/site/layout/DS/assets/img/icons/iconsweets/is/dark/arrow-down-small.png") no-repeat right 0px;
			overflow: hidden;
			display: inherit;
		}
		#loadMoreButton.loading{
			background: url("/site/layout/DS/assets/img/ajax-loading.gif") no-repeat left center;
		}
		#filterr{
			padding:10px;
		}
		#filterr select,#filterr input{
			margin:10px;
			border: 1px solid #efefef;
			height:30px;
		}
		@media (max-width: 480px) {
			#filterr select, #filterr input {
				width:90px;
			}
			#filterr #fbutton{
				width:205px;
			}
		}
		.aaactive{
			background-color: #ebf4f8 !important;
		}
		{/literal}
	</style>
</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->
<div class="blurredElement"></div>
<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<span class="hidden-xxs">
			{breadCrumb left="ds_bug/`$type`,`$typeLabel` List" right="ds_addBug/`$type`,New `$typeLabel`"}
				</span>
			<!-- /Breadcrumbs line -->
			<div class="row">

					<div class="col-md-12">
					<div class="widget box">
						<div  id="filterr">
							<form action="/ds_bug/{$type}" method="GET" id="bugSearchForm">
							<span class="hidden-xxs">Filter By :</span>
							<select name="priority">
								<option value="">Priority</option>
								{foreach from=$_items->priority_options key=pk item=pv}
									<option value="{$pk}" {if isset($S.priority) && $S.priority==$pk}selected{/if}>{$pv}</option>
								{/foreach}
							</select>
							<select name="status">
								<option value="">Status</option>
								{foreach from=$_items->status_options key=pk item=pv}
									<option value="{$pk}" {if isset($S.status) && $S.status==$pk}selected{/if}>{$pv}</option>
								{/foreach}
							</select>
							{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}
							<select name="branch">
								<option value="">Branch</option>
								<option value="-1" {if isset($S.branch) && $S.branch==-1}selected{/if}>Not branch`d</option>
								{doWhile}
								<option value="{$branch->id}" {if isset($S.branch) && $S.branch==$branch->id}selected{/if}>{$branch->name}</option>
								{/doWhile ($branch->populate())}
							</select>
							<select name="project">
								<option value="">Project</option>
								<option value="-1" {if isset($S.project) && $S.project==-1}selected{/if}>Not projected</option>
								{doWhile}
									<option value="{$project->id}" {if isset($S.project) && $S.project==$project->id}selected{/if}>{$project->name}</option>
								{/doWhile ($project->populate())}
							</select>
							<select name="assignee">
								<option value="">Assignee</option>
								<option value="-1" {if isset($S.assignee) && $S.assignee==-1}selected{/if}>Not Assigned</option>

								{doWhile}
									<option value="{$user->id}" {if isset($S.assignee) && $S.assignee==$user->id}selected{/if}>{$user->username}</option>
								{/doWhile ($user->populate())}
							</select>
							{/if}
							<input type="text" name="title" placeholder="term / #id" value="{if isset($S.title) && strlen($S.title)>0}{$S.title}{/if}">
							<input type="submit" name="search" value="Filter" id="fbutton">
							</form>
						</div>
						{if $_items->gotValue}

						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if isset($customLabel)}
									{$customLabel}
								{else}
								{$typeLabel} List
								{/if}
							</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding" id="buggbox">
							<table class="table table-hover table-striped table-bordered table-highlight-head" id="buggTable">
								<thead>
								<tr id="sorter">
									<th class="col-md-1" style="width:4%"><a href="/ds_bug/{$type}?{$QSOrder}&ob=0&od={$od}">ID</a></th>
									<th class="col-md-4"><a href="/ds_bug/{$type}?{$QSOrder}&ob=1&od={$od}">Title</a></th>
									{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}
										<th class="col-md-1">Assignee</th>
									{/if}
									<th class="col-md-1"><a href="/ds_bug/{$type}?{$QSOrder}&ob=2&od={$od}">Status</a></th>
									<th class="col-md-1 hidden-xxs"><a href="/ds_bug/{$type}?{$QSOrder}&ob=3&od={$od}">Priority</a></th>
									<th class="col-md-2 hidden-xxs"><a href="/ds_bug/{$type}?{$QSOrder}&ob=4&od={$od}">Created</a></th>
									{*{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}*}
										{*<th class="col-md-2">Assignee</th>*}
									{*{/if}*}

									<th class="col-md-2 hidden-xxs"><a href="/ds_bug/{$type}?{$QSOrder}&ob=5&od={$od}">Last Action</a></th>
									{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}
									{*<th class="col-md-1 hidden-xxs"></th>*}
									{/if}
								</tr>
								</thead>
								<tbody>
								{include file="DS/bugTable.tpl"}
								</tbody>
							</table>

						<div id="navco">
							{if $nextPage == 1}
								<a href="/ds_bug/{$type}?{$QS}&page={$page+1}" class="wwwA"></a>
							{/if}
						</div>
						</div>



						{else}

							<div class="col-md-12" style="padding:30px;text-align: center">No records found. Happy!</div>
						{/if}
					</div>
				</div>
				{*<a href="javascript:;" id="loadMoreButton">Load More</a>*}

			</div>
			<!-- /.row -->
			<!-- /Bug Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="DS/footer.tpl"}
<script>
	var page=0;
	var baseUrl = location.href.replace(/http(?:s)?:\/\/(?:.+?)(\/.+)$/,'$1')
	var perPage = {$perPage};
	var ob = {$ob};
	var od = {$od};

	{if isset($S.bugId)}
	var openId = {$S.bugId};
	{/if}

	{literal}
	ph = $('#sorter a:eq('+ob+')' ).html();
	$('#sorter a:eq('+ob+')' ).html(ph+' '+(od==1?'↓':'↑'));

	$('#buggTable' ).on('click','td', function () {
			var $e = $( this ).parent();
			if($(this ).hasClass('ops'))
				return; //skip last box

			if ( $e.next().hasClass( 'frameRow' ) ) {
				$e.removeClass( '.aaactive' );
				$e.next().remove();
				$e.find('td' ).removeClass('aaactive');
			}
			else {
				$e.find('td' ).addClass('aaactive');
				$( '.blurredElement' ).show();
				var ef = location.href.split( '#' );
				location.href = ef[0]+'#'+$e.attr( 'data-tid' );

				$e.after( '<tr class="frameRow" data-tid="'+$e.attr( 'data-tid' )+'"><td colspan="99" class="framer"><iframe src="/ds_bugDetail/' + $e.attr( 'data-tid' ) + '" name="detailF' + $e.attr( 'data-tid' ) + '" id="detailF' + $e.attr( 'data-tid' ) + '" style="height:'+($(window).height() * 0.8)+'px"></iframe></td></tr>' )
				var offset = $(this).offset(); // Contains .top and .left

				offset.left -= 20;
				offset.top -= 30;
				$('html, body').animate({
					scrollTop: offset.top,
					scrollLeft: offset.left
				}, function () {
					lockScroll()
				});
				$( '.blurredElement' ).click( closePreview );
			}
		}
	);

	function closePreview(){
		$( '.frameRow' ).remove();
		unlockScroll();
		$( '.blurredElement' ).hide();
		var ef = location.href.split( '#' );
		location.href = ef[0]+'#'; //dont show on refresh, @murat
	}

	function lockScroll(){
		var scrollPosition = [
			self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
			self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop
		];
		var html = jQuery('html'); // it would make more sense to apply this to body, but IE7 won't have that
		html.data('scroll-position', scrollPosition);
		html.data('previous-overflow', html.css('overflow'));
		html.css('overflow', 'hidden');
//		window.scrollTo(scrollPosition[0], scrollPosition[1]);
	}
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			closePreview();
		}   // esc
	});
	function unlockScroll(){
		var html = jQuery('html');
		var scrollPosition = html.data('scroll-position');
		html.css('overflow', 'scroll');
//		window.scrollTo(scrollPosition[0], scrollPosition[1])
	}
	$( '#loadMoreButton' ).click( function () {
		$( this ).addClass( 'loading' ).html(' ');
		$.get( baseUrl + '&page=' + (++page) + '&onlyTable=1', function ( d ) {

			var rowz = d.match( /<tr/g );

			if ( rowz == null || rowz.length < perPage )
				$( '#loadMoreButton' ).hide();

			$( '#buggTable tbody' ).append( d );

			setTimeout( function () {
				$( '#loadMoreButton' ).removeClass( 'loading' ).html( ' Load More ' );
			}, 500 );
		} )
	} );
	function goBugs(id){
		window.frames['detailF'+id].location.href='/ds_bugDetail/'+id;
	}
	function destroyBug(id){
		$( 'tr[data-tid="' + id + '"]' ).fadeOut();
		closePreview();
	}
	admAlertAssigned=true;

	if((e=location.href.match(/.+?\#(\d+)+/))!==null ){
		openId = e[1];
	}

	if( typeof openId !='undefined' && (eltocho=$( 'tr[data-tid='+openId+']' )).length > 0)
		$( 'tr[data-tid='+openId+']' ).find('td:first').click();


	$('#filterr' ).find('select' ).change(function(){
		$('form#bugSearchForm' ).submit()
	})

	var ias = $.ias({
		container:  "#buggTable",
		item:       "tr.bugDetailer",
		pagination: "#navco",
		next:       "a.wwwA"
	});

	ias.extension(new IASSpinnerExtension());            // shows a spinner (a.k.a. loader)
	{/literal}
</script>
</body>
</html>