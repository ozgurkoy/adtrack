{getSessionParams}
<script src="/site/layout/DS/jquery.colorbox-min.js"></script>
<link href="/site/layout/DS/colorbox.css" rel="stylesheet" />
<style>
	{literal}
	@media (max-width: 480px) {

		.navbar .nav > li > a {
			padding: 14px 15px !important;
		}
		.hidden-xxs {
			display:none !important;
		}
	}
	@media (min-width: 481px) {
		span.hidden-xxs {
			display:inline !important;
		}
	}
	{/literal}
</style>
<!-- Top Navigation Bar -->
<div class="container">

	<!-- Only visible on smartphones, menu toggle -->
	<ul class="nav navbar-nav">
		<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
	</ul>

	<!-- Logo -->
	<a class="navbar-brand" href="/">
		<img src="/site/layout/DS/assets/img/logo.png" alt="logo"/>
		<strong>TRACK</strong>
	</a>
	<!-- /logo -->
	<!-- Sidebar Toggler -->
	<a href="#" class="{if $isRoot!=1}sidebar-closed{/if} toggle-sidebar bs-tooltip" data-placement="bottom" style="display:inline-block" data-original-title="Toggle navigation">
		{if $isRoot==1}
		<i class="icon-reorder"></i>
		{/if}
	</a>
	<!-- /Sidebar Toggler -->
	{if $isRoot==1}
	<ul class="nav navbar-nav navbar-left">
		<li>
			<a href="/ds_bug/1/1">
					<i class="icon-bug"></i>
					<span class="hidden-xxs">Bugs</span>
				</a>
		</li>
		<li>
			<a href="/ds_task">
				<i class="icon-reply"></i>
				<span class="hidden-xxs">Tasks</span>
			</a>
		</li>
		<li>
			<a href="/ds_enhancement">
				<i class="icon-question-sign "></i>
				<span class="hidden-xxs">Questions</span>
			</a>
		</li>
		<li>
			<a href="/ds_bug/99">
				<i class="icon-arrow-down"></i>
				<span class="hidden-xxs">Assigned to me</span>
			</a>
		</li>
	</ul>
	{/if}

	<!-- Top Right Menu -->
	<ul class="nav navbar-nav navbar-right">
		<!-- Notifications -->
		<li class="dropdown" id="notsBars">

		</li>


		<!-- User Login Dropdown -->
		<li class="dropdown user">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
				<!--<img alt="" src="/site/layout/DS/assets/img/avatar1_small.jpg" />-->
				<i class="icon-male"></i>
				<span class="username">{$_session.DS_username}</span>
				<i class="icon-caret-down small"></i>
			</a>
			<ul class="dropdown-menu">
				<li><a href="/ds_profile"><i class="icon-user"></i> My Profile</a></li>
				{*<li><a href="pages_calendar.html"><i class="icon-calendar"></i> My Calendar</a></li>*}
				{*<li><a href="#"><i class="icon-tasks"></i> My Tasks</a></li>*}
				{*<li class="divider"></li>*}
				<li><a href="/ds_logout"><i class="icon-key"></i> Log Out</a></li>
			</ul>
		</li>
		<!-- /user login dropdown -->
	</ul>
	<!-- /Top Right Menu -->
</div>
<!-- /top navigation bar -->