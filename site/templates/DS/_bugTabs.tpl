
<script src="/site/layout/DS/jquery.colorbox-min.js"></script>
<style>
	{literal}
	.infoBlock{
		float:left;
		/*width:33%;*/
		background-color: #feffde;
		padding: 10px;
		margin: 10px 0px;
		position:relative;
	}
	h3.id{
		position: absolute;
		top:0px;
		right:3px;
		margin-bottom: 0; padding: 0; margin-left: 10px;
	}
	.infoBlock td{
		font-size: 12px;
	}
	.tt{
		font-weight: bold;
	}
	{/literal}
</style>
<style>
	{literal}
	@media (max-width: 480px) {

		.hidden-xxs {
			display:none !important;
		}
		.col-xxs-12{
			width: 100% !important;
		}
	}
	@media (min-width: 481px) {

		span.hidden-xxs {
			display:inline !important;
		}
	}
	div.bugPreview{
		float:left;
		width:100%;
		height:10px;
		opacity: 0.9;
		overflow: hidden;
	}
	div.bugPreview.bugOpen{
		float:left;
		width:100%;
		height:auto;
		opacity: 1;
		/*overflow: hidden;*/
	}
	{/literal}
</style>
<ul class="nav nav-tabs" style="position:fixed;background: #fff;z-index:10000;opacity:0.90;width:100%;">
	<li {if $action=="detail"}class="active"{/if}><a href="/ds_bugDetail/{$id}">History</a></li>
	<li class="

	{if $action=="att"}active{/if}"><a href="/ds_bugAttachments/{$id}">Attachments</a></li>
	{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}
	<li class="hidden-xxs {if $action=="ass"}active{/if}"><a href="/ds_assignBug/{$id}">Reassign</a></li>
	{/if}

	{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}

	{if $bug->status != 3 }
		<li class="hidden-xxs {if $action=="close"}active{/if}"><a href="/ds_closeBug/{$id}">Close</a></li>
		{if !($bug->merged > 0)}
		<li class="hidden-xxs {if $action=="merge"}active{/if}"><a href="/ds_bugMerge/{$id}">Merge</a></li>
		{/if}
	{/if}

	<li class="hidden-xxs {if $action=="foll"}active{/if}"><a href="/ds_bugFollower/{$id}">Followers</a></li>
	<li class="hidden-xxs {if $action=="edit"}active{/if}"><a href="/ds_addBug/{$bug->bugType}/{$id}">Edit</a></li>
	<li class="hidden-xxs {if $action=="del"}active{/if}"><a href="/ds_bugDel/{$id}">Delete</a></li>
	{/if}

</ul>

<h3 class="id">{$typeLabel} #{$bug->id}</h3>
