{readPerms}
{if $bug->status != 3 }

	{include file="DS/_top.tpl"}
	<div class="tabbable tabbable-custom tabbable-full-width" style="overflow-x: hidden;">

		{include file="DS/_bugTabs.tpl"}
		<div class="tab-content row" style="padding-top: 50px;overflow-x:hidden">
			<form class="form-horizontal row-border" action="/ds_delBug/{$bug->id}" name="closeBug" id="closeBug" method="POST" target="ps"  >
				{getFormToken f="delBug`$bug->id`"}
				{formBox type="checkbox" label="Delete the issue" name="deleteMe" value=null}
			<div class="form-actions">
				<button type="submit" class="submit btn btn-primary pull-right">
					Delete <i class="icon-angle-right"></i>
				</button>
			</div>
			</form>

		</div>
	</div>
{else}
	Bug is already closed.
{/if}