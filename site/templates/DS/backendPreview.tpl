{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>
	<link href="/site/layout/DS/joyful/2048/style/main.css" rel="stylesheet" type="text/css">

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_backend,Backend Jobs;,`$obj->description`" }
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget-content no-padding" style="text-align:center">
						<h1>{$obj->description}</h1>
						<br/>
						<img src="/site/layout/DS/assets/img/ajax-loading.gif">
						<h2>Play !</h2>
						<div class="game-container">
							<div class="game-message">
								<p></p>
								<div class="lower">
									<a class="keep-playing-button">Keep going</a>
									<a class="retry-button">Try again</a>
								</div>
							</div>

							<div class="grid-container">
								<div class="grid-row">
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
								</div>
								<div class="grid-row">
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
								</div>
								<div class="grid-row">
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
								</div>
								<div class="grid-row">
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
									<div class="grid-cell"></div>
								</div>
							</div>

							<div class="tile-container">

							</div>
						</div>

					</div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
<script>
	var code = '{$obj->code}';
	var intv = null;
	{literal}
	intv = setInterval( function () {
		$.get( '/ds_backendStatus/' + code, function ( data ) {
			data = JSON.parse( data );
			if ( data.status == 'good' ) {
				location.href = data.url;
			}
			else if ( data.status == 'error' ) {
				alert( "Some error occured , please let IT know about this" );
				clearInterval( intv );
			}

		} );
	}, 1500 );
	{/literal}
</script>

<script src="/site/layout/DS/joyful/2048/js/animframe_polyfill.js"></script>
<script src="/site/layout/DS/joyful/2048/js/keyboard_input_manager.js"></script>
<script src="/site/layout/DS/joyful/2048/js/html_actuator.js"></script>
<script src="/site/layout/DS/joyful/2048/js/grid.js"></script>
<script src="/site/layout/DS/joyful/2048/js/tile.js"></script>
<script src="/site/layout/DS/joyful/2048/js/local_score_manager.js"></script>
<script src="/site/layout/DS/joyful/2048/js/game_manager.js"></script>
<script src="/site/layout/DS/joyful/2048/js/application.js"></script>

{include file="DS/footer.tpl"}
</body>
</html>