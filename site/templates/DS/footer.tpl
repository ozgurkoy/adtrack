<!-- Footer -->
<div class="footer">
	{*<a href="#" class="sign-up">Don't have an account yet? <strong>Sign Up</strong></a>*}
</div>
<!-- /Footer -->
<iframe name="ps" id="ps" src="/site/blank.html" style="display:none;width:500px;float:right"></iframe>
<script type="text/javascript">
	{literal}

	var _refreshCounter = 600;
	var _refreshTime    = 60000; // time in ms 1000ms = 1 sec

	var _refreshInterval = setInterval( function () {
		$.get( '/ds_xhr/refreshDS' );
		if ( _refreshCounter-- <= 0 )
			clearInterval( _refreshInterval )
	}, _refreshTime );


	function assignForms() {
		$( 'form' ).each( function ( a, b ) {
			$( b ).unbind( 'submit' );
			var b0 = $(b ).find( 'button[type="submit"]' );

			$( b ).submit( function () {
				var $t = $( b );
				var k = $t.find( 'button[type="submit"]' );

				if ( b0.outerWidth() + 5 > b0.css( 'width' ) )
					b0.css( 'width', b0.outerWidth() + 5 );

				k.css( 'cssText', 'background-color:transparent !important;width:' + k.outerWidth() + 'px' );

				k.addClass( 'bloading' ).attr( 'vhtml', k.html() ).html( ' ' )
				k.click( function ( e ) {
					e.preventDefault();
				} )
			} )

		} );
	}

	function clearButtons(){
		setTimeout(function(){
			var bt = $( 'button[type="submit"].bloading' );
			bt.removeClass( 'bloading' ).html( bt.attr('vhtml') ).css('background-color','' ).unbind('click');
		},1000);
	}

    function render() {
        var d = location.href.replace( 'http://' + SITEROOT + '/', '' ).split( '/' )[0];

        $( "ul#nav a[href*='" + d + "']" ).parents( "li" ).addClass( "open" );
        $( "ul#nav a[href*='" + d + "']" ).parents( "li" ).addClass( "current" );

	    $( '.clearHere' ).html( '' );

	    $( document ).on("click","button[href]", function () {
		    //console.log("button click");
		    if ( $( this ).attr( 'data-blank' ) )
			    window.open( $( this ).attr( 'href' ) );
		    else if ( $( this ).attr( 'data-iframe' ) )
			    window.ps.location.href = $( this ).attr( 'href' );
		    else
			    location.href = $( this ).attr( 'href' );
	    } )

	    assignForms();

    }

	$(document ).ready(function(){
		var sub = '';
		if($('.tabbable').length>0){
			sub = $('.tabbable' ).find('li.active' ).text()
		}
		else if($('.widget-header h4' ).length>0){
			sub = $( '.widget-header h4' ).text();
		}
		else if($('ul#breadcrumbs li:first').length>0){
			sub = $('ul#breadcrumbs li:first').text();
		}

		document.title = (sub.length>0?sub+' - ':'') + 'admingle Track' +
		'';
		render();
	});

	function timeoutWarning(){
		$.blockUI({ css: {
			border: 'none',
			padding: '15px',
			backgroundColor: '#000',
			'-webkit-border-radius': '10px',
			'-moz-border-radius': '10px',
			opacity: .5,
			color: '#fff'
		},
		message: "Your session has timed out",
		showOverlay: true,
		timeout: 5000}
	);
	}

	$('.toggle-sidebar' ).click( function () {
//		$( '#container' ).toggleClass( 'sidebar-closed' )
		$.cookie( "panelOpen", $( '#container' ).hasClass( 'sidebar-closed' ), { expires: 7, path: '/' } );
	})

	{/literal}
	{if isset($isRoot) && $isRoot!=1}
	$( '#container' ).addClass( 'sidebar-closed' );
	{/if}

</script>