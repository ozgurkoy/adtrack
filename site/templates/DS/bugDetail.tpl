{readPerms}

{include file="DS/_top.tpl"}
<div class="tabbable tabbable-custom tabbable-full-width" style="overflow-x: hidden;">

{include file="DS/_bugTabs.tpl"}
<div class="tab-content row" style="padding: 30px">
	<div class="infoBlock {if $isRoot == 1}col-md-4 col-xs-4{else}col-md-12 col-xs-12{/if} col-xxs-12">
		{callUserFunc $bug->loadProject()}
		{callUserFunc $bug->loadBCategory()}
		{callUserFunc $bug->loadAssignee()}
		{callUserFunc $bug->loadOpener()}
		{assign var=st value=$bug->status}
		{assign var=pr value=$bug->priority}

		<table width="100%">
			<tr>
				<td width="10%" class="tt">Title</td>
				<td width="90%">: {$bug->title}</td>
			</tr>
			<tr>
				<td class="tt">Project</td>
				<td>:
					{if $bug->project && $bug->project->gotValue}
					{$bug->project->name}
					{else}
					-
					{/if}
				</td>
			</tr>
			<tr>
				<td class="tt">Status</td>
				<td>: {$bug->status_options.$st}</td>
			</tr>
			<tr>
				<td width="10%" class="tt">Priority</td>
				<td width="90%">: {$bug->priority_options.$pr}</td>
			</tr>
		</table>
	</div>
	{if $isRoot == 1}
	<div class="infoBlock col-md-4  col-xs-4 hidden-xxs">
		{callUserFunc $bug->loadProject()}
		{callUserFunc $bug->loadBCategory()}
		<table width="100%">
			<tr>

				<td class="tt">Date</td>
				<td>: {timestampToDate d=$bug->jdate}</td>
			</tr>
			<tr>
				<td class="tt">Opener</td>
				<td>:

					{if $bug->opener && $bug->opener->gotValue}
						{$bug->opener->username}
					{else}
						-
					{/if}
				</td>
			</tr>
			<tr>
				<td class="tt">Assignee</td>
				<td>:
					{if $bug->assignee && $bug->assignee->gotValue}
						{$bug->assignee->username}
					{else}
						-
					{/if}
				</td>
			</tr>
			<tr>
				<td class="tt">Category</td>
				<td>:
					{if $bug->bcategory && $bug->bcategory->gotValue}
					{$bug->bcategory->name}
					{else}
					-
					{/if}
				</td>
			</tr>
		</table>
	</div>
	<div class="infoBlock col-md-4  col-xs-4 hidden-xxs">
		{callUserFunc $bug->collectTags()}

		{callUserFunc $bug->loadBranch()}
		<table width="100%">
			<tr>
				<td width="10%" class="tt">Tags</td>
				<td width="90%">:
					{if empty($bug->tagz)}
						-
					{else}
						{$bug->tagz}
					{/if}
				</td>
			</tr>
			<tr>
				<td class="tt">Link</td>
				<td>:
					{if strlen($bug->link)>0}
						<a href="{$bug->link}" target="_blank">{$bug->link|truncate:20:"..."}</a>
					{else}
						-
					{/if}
				</td>
			</tr>
			<tr>
				<td class="tt">Branch</td>
				<td>:
					{if $bug->branch && $bug->branch->gotValue}
						{$bug->branch->name}
					{else}
						-
					{/if}

				</td>
			</tr>
			<tr>
				<td class="tt">Expected</td>
				<td>:
					{if empty($bug->startDate)}
						-
					{else}
						{$bug->startDate}
					{/if}
			</tr>
		</table>
	</div>
	{/if}
	{if !($bug->merged > 0)}
	<div style="float:right; background: #4d7496; padding: 10px;">
		<a href="javascript:respond()" id="respondButton" style="color:#fff;font-size:16
		px;margin-top:-30px;">{if $bug->status==3}Reopen{else}Respond{/if}</a>
	</div>
	{/if}
	<div id="respondForm" style="display:none; margin-top: 0px; float: left; width: 100%;">
		<h3>Respond to ticket</h3>
		<form class="form-horizontal row-border" action="/ds_addDetail/{$id}" name="bugDetail" id="bugDetail" method="POST" target="ps" enctype="multipart/form-data"  >
			{getFormToken f="bugDetail`$id`"}
			{formBox type="textarea" label="Details" name="details" value=null}
			{formBox type="file" label="File/SS" name="file[]" multi=1 value=null}
			{if $bug->status != 3}
			{formBox type="checkbox" label="Close Issue" name="doClose" value=null}
			{/if}
			<div class="form-actions">
				<button type="submit" class="submit btn btn-primary pull-right">
					{if $bug->status == 3}Reopen bug{else}Submit{/if} <i class="icon-angle-right"></i>
				</button>
			</div>

		</form>
	</div>
	<div id="bdetails">
	{doWhile}
	{if !($isRoot!=1 && $list->rootOnly == 1)}
		{callUserFunc $list->loadBugUser()}
		{callUserFunc $list->loadBugFiles()}
	<div style=" float:left;clear:both;width:100%;background-color: #efefef; padding: 10px; margin: 10px 0px;">
		{assign var=op value=$list->operation}
		<div class="col-md-11 col-xs-11 col-lg-11 col-md-11" style="padding-left:0;text-align: left">
			<span style="font-weight: bold">{$list->bugUser->username} on {timestampToDate d=$list->jdate}</span>
		</div>

		<div class="col-md-1 col-xs-1 col-lg-1 col-md-1 hidden-xxs" style="text-align: right">
			<span style="font-style: italic">{$list->operation_options.$op}</span>
		</div>
		{if $list->relatedBug>0}
			<a href="/ds_bug?bugId={$list->relatedBug}" target="_parent">⇢ Go to related Issue #{$list->relatedBug}</a><br><br>
		{/if}
		{previewBug detail=$list->details}<br/>
		{if $list->bugFiles->gotValue}
			<br />
			<span style="font-weight: bold"> Attachments: </span><br />
			{doWhile}
				{if stripos($list->bugFiles->fileType,"image") !== false}
					<a href="/files/{$list->bugFiles->filename}"  data-lb="1" target="_blank"><img src="/files/{$list->bugFiles->filename}" style="width:90px"></a>
				{else}
					<a href="/files/{$list->bugFiles->filename}">{$list->bugFiles->filename}</a> <br>
				{/if}
			{/doWhile ($list->bugFiles->populate())}
		{/if}
	</div>
	{/if}
	{/doWhile ($list->populate())}

	</div>
</div>
</div>

<script>
	{literal}
	$('a[data-lb="1"]' ).each(function(a,b){
		$( b ).click( function ( e ) {
			e.preventDefault();
			window.parent.$.colorbox( {href : b.href} ).show();
			return false;
		});
	})

	{/literal}
</script>
<script>
	{literal}
	function respond(){
		if($('#respondForm:visible' ).length>0)
			$('#respondButton' ).html('Respond');
		else
			$('#respondButton' ).html('X');

		$('#respondForm').toggle();
		$('#bdetails').toggle();
		FormComponents.init();
	}
	function bugToggle(e){
		$(e ).next().toggleClass('bugOpen');
	}
	$('div.bugPreview' ).before('<a href="javascript:;" onclick="bugToggle(this)">[...]</a>')
	{/literal}
</script>