{readPerms}
{include file="DS/_top.tpl"}
<div class="tabbable tabbable-custom tabbable-full-width" style="overflow-x: hidden;">

	{include file="DS/_bugTabs.tpl"}
	<div class="tab-content row" style="padding-top: 50px;padding-left: 50px">
		{if $list->gotValue != true}
			No attachments.
		{else}
		{doWhile}
			<div class="col-md-4" style="min-width:170px;text-align: center;cursor:pointer;margin:10px;min-height:90px; float:left;background-color: #efefef;padding:10px" >
			{callUserFunc $list->loadBugHistory()->loadBugUser()}
			{if stripos($list->fileType,"image") !== false}
				<a href="/files/{$list->filename}" data-lb="1">
				<img src="/files/{$list->filename}" style="height:80px;">
				</a>
			{else}
				<a href="/files/{$list->filename}" style="height:90px;">
					<img src="/site/layout/DS/att.png" style="height:56px;margin-bottom:5px"><br/>
					{$list->filename|truncate:10:".."}
				</a>
			{/if}
		<br />{timeAgo time=$list->jdate} ago by {$list->bugFiles->bugUser->username}
			</div>
		{/doWhile ($list->populate())}
		{/if}
	</div>
</div>
<script>
	{literal}
	$('a[data-lb="1"]' ).each(function(a,b){
		$( b ).parent().click( function ( e ) {
			e.preventDefault();
			window.parent.$.colorbox( {href : b.href} ).show();
			return false;
		});
	})

	{/literal}
</script>