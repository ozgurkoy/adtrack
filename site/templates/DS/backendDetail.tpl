{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	{literal}
	<script>
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
	</script>
	<style>
		.preview{
			height:200px;
			overflow:hidden;
			width: 100%;
			position: relative;
		}
		.preview:hover .previewMore {
			display: block !important;
		}
		.preview .previewMore {
			display: none;
			z-index: 1000;
			background-color: #f0f0f0;
			color: #000;
			font-size: 25px;
			position: absolute;
			right: 10px;
			bottom: 0;

		}
	</style>
	{/literal}
</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_backend,Backend Jobs;,`$obj->description` detail" }
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget-content no-padding">
						<h1>{$obj->label}</h1>
						{assign var=st value=$obj->status}
						{*{if $obj->status_options[$st]=="ERROR" || $obj->status_options[$st]=="KILLED"}*}
						<br/>
						<a href="/site/backend/lib/_reRun.php?id={$obj->id}">Re-run Task</a>
						{*{/if}*}
						{if $obj->status_options[$st]=="WORKING"}
						<a href="/site/backend/lib/_forceKill.php?id={$obj->code}">Kill Task</a>
						{/if}
						<h3>Status :</h3>
						{$obj->status_options[$st]}
						<h3>Start Time :</h3>
						{timestampToDateWithTime d=$obj->jdate}
						{if $obj->endTime > 0}
						<h3>End Time :</h3>
						{timestampToDateWithTime d=$obj->endTime}
						{/if}
						{if $obj->status_options[$st]=="ERROR"}
						<h3>Error Details :</h3>
						{assign var=st value=$obj->status}
						<div class="preview">
							{$obj->errorState}
							<div class="previewMore"><a href="#">More..</a></div>
						</div>
						{/if}
						<h3>Output <sub style="font-size:10px">scrollable ↓ <a href="javascript:void($('#outputBox').selectText())">select</a></sub></h3>
						<div style="width:100%;height:400px;overflow-y:scroll" id="outputBox">
							{$obj->contentOutput|nl2br}
							{if strlen($content)>0}
							<br/><br/>
							Real PROC Output:
							{$content|nl2br}
							{/if}
						</div>
					</div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="DS/footer.tpl"}
<script>
	{literal}
	$('.previewMore' ).click(function(){
		$( '.preview' ).css( 'overflow', 'visible' );
		$( this ).remove();
	});
	jQuery.fn.selectText = function(){
		var doc = document;
		var element = this[0];
		console.log(this, element);
		if (doc.body.createTextRange) {
			var range = document.body.createTextRange();
			range.moveToElementText(element);
			range.select();
		} else if (window.getSelection) {
			var selection = window.getSelection();
			var range = document.createRange();
			range.selectNodeContents(element);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	};

	{/literal}

</script>
</body>
</html>