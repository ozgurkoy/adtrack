{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
    <div id="sidebar" class="sidebar-fixed">
        <div id="sidebar-content">

	        <!-- Search Input -->
	        <form class="sidebar-search">
		        <div class="input-box">
			        <button type="submit" class="submit">
				        <i class="icon-search"></i>
			        </button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
		        </div>
	        </form>


	        <!--=== Navigation ===-->
	        {include file="DS/_nav.tpl" menus=$menus}
	        <!-- /Navigation -->

        </div>
        <div id="divider" class="resizeable"></div>
    </div>
    <div id="content">
        <div class="container">
            <!-- Breadcrumbs line -->
            <div class="crumbs">
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="/darkside">Dashboard</a>
                    </li>
{*
                    <li class="current">
                        <a href="pages_calendar.html" title="">Calendar</a>
                    </li>
*}
                </ul>

            </div>
            <!-- /Breadcrumbs line -->

            <!--=== Page Header ===-->
            <div class="page-header" style="height:83px">
                <div class="page-title">
                    <h3>Dashboard</h3>


                </div>
            </div>

            <!-- /Page Header -->

            <!--=== Page Content ===-->


	        <div class="row clearHere" id="editRow">
	        </div> <!-- /.row -->
	        <div class="row">
		        {if $nots->gotValue}
			        {doWhile}

		        <div class="col-sm-6 col-md-3">
			        <div class="statbox widget box box-shadow">
				        <div class="widget-content">
						        {if $nots->type == 3}
							        <div class="visual yellow">
								        <i class="icon-zoom-in"></i>
								    </div>
						        {elseif $nots->type == 2}
							        <div class="visual green">
								        <i class="icon-star"></i>
							        </div>
						        {elseif $nots->type == 1}
							        <div class="visual red">
								        <i class="icon-warning-sign"></i>
							        </div>
						        {/if}
					        <div class="title">
						        {$nots->title}
					        </div>
					        <div class="value" style="font-size:16px;height:70px">{$nots->note|strip_tags|truncate:30:".."}

						        <span style="display:block;font-weight: normal;font-style: italic;font-size:10px">
									{dformat t=$nots->jdate f="M / d H:i"}
						        </span>
					        </div>
					        {if !is_null($nots->link)}
					        <a class="more" href="/{$nots->link}">View More <i class="pull-right icon-angle-right"></i></a>
						    {elseif $nots->dsn_campaign>0}
					        <a class="more" href="/ds_campaignHistory/{$nots->dsn_campaign}">View More <i class="pull-right icon-angle-right"></i></a>
						    {elseif $nots->dsn_advertiser>0}
					        <a class="more" href="/ds_addAdvertiser/{$nots->dsn_advertiser}">View More <i class="pull-right icon-angle-right"></i></a>
						    {elseif $nots->dsn_publisher>0}
					        <a class="more" href="/ds_publisherProfile/{$nots->dsn_publisher}">View More <i class="pull-right icon-angle-right"></i></a>
					        {/if}
				        </div>
			        </div>
			        <!-- /.smallstat -->
		        </div>

			        {/doWhile ($nots->populate())}
		        {/if}
		        <!-- /.col-md-3 -->
		        <!-- /.col-md-6 -->
	        </div> <!-- /.row -->

	        <div class="row clearHere" id="editRow">
	        </div> <!-- /.row -->

            <!-- /Page Content -->
        </div>
        <!-- /.container -->

    </div>
</div>
{include file="DS/footer.tpl"}

</body>
</html>