{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_language,Language" right="ds_addLanguage,New Language"}
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> {$_job|ucfirst} List</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding">
							<table class="table table-hover table-striped table-bordered table-highlight-head">
								<thead>
								<tr>
									<th>Last Update</th>
									<th>Language Name</th>
									<th>Short Name</th>
									<th>Code Name</th>
									<th>Flag</th>
									<th>Word Count</th>
									<th>Active</th>
									<th class="col-md-2">Action</th>
								</tr>
								</thead>
								<tbody>
								{if $_items->gotValue}
									{doWhile}
										{callUserFunc $_items->calcWordsCount()}
										<tr>
											<td>
												{dformat t=$_items->lastUpdate}
											</td>
											<td>
												{$_items->langName}
											</td>
											<td>
												{$_items->langShort}
											</td>
											<td>
												{$_items->langShortDef}
											</td>
											<td>
												{$_items->flag}
											</td>
											<td>
												{$_items->_wordsCount}
											</td>
											<td>
												{$_items->active}
											</td>
											<td>
												<button class="btn btn-xs" href="/ds_add{$_job|ucfirst}/{$_items->id}"><i class="icon-cog"></i></button>
												<button class="btn btn-xs" href="javascript:goIfOk('/ds_del{$_job|ucfirst}/{$_items->id}')"><i class="icon-remove"></i></button>
												<button id="btn-load-then-single-complete" tag="{$_items->id}" class="btn btn-xs btn-success single-lang-update" data-loading-text="Loading..." data-complete-text="Language Updated!">Full Update</button>
											</td>
										</tr>
									{/doWhile ($_items->populate() != false)}
								{/if}
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>
			<!-- /.row -->

			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Update {$_job|ucfirst}</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content align-center">
							<div class="btn-toolbar">
								<button id="btn-load-then-complete" class="btn btn-success single-lang-update" tag="0" data-loading-text="Loading..." data-complete-text="Languages Updated!">Update Languages</button>
							</div>


						</div>
						<div id="updateResult">
						</div>

					</div>
				</div>


			</div>

			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="DS/footer.tpl"}

<script type="text/javascript">

{literal}

var timeNow =function(){
	var date = new Date();
	var str = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

	return str;
}

var success =function(count){
	$("#updateResult").append("<div class='alert alert-success fade in'>"+
			"<i class='icon-remove close' data-dismiss='alert'></i>"+
			"<strong>Success!</strong> "+count+" words has been successfully added at "+ timeNow() +
			"</div>"
	);
}

var errorUpdate =function(error){
	$("#updateResult").append("<div class='alert alert-danger fade in'>"+
			"<i class='icon-remove close' data-dismiss='alert'></i>"+
			"<strong>Error!</strong>"+ timeNow() +" - "+error+
			"</div>"
	);
}


$(document).ready(function(){

//===== Stateful Buttons =====//
	$('.single-lang-update').on( 'click', function() {

		var btn = $(this);
		btn.button('loading');
		$.post("ds_langWordsSync/"+btn.attr("tag"),
				function(data){
					data = JSON.parse(data);
					console.log(data.error);
					btn.button('complete');
					if(data.error=="0"){
						// wirte error
						success(data.wordsCount);
					} else {
						// write
						errorUpdate(data.data);

					}
				}, "json"
		);
	});

});


{/literal}
</script>

	</body>
</html>