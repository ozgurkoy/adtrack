<ul class="nav nav-tabs">
	<li {if $_action == "addEdit"}class="active"{/if}><a href="/ds_campaignAddEdit/{$_item->id}">{if $_item->gotValue}{if isset($copyCampaign) && $copyCampaign}Copy{else}Edit{/if}{else}Add{/if} Campaign</a></li>
	{if $_item->gotValue && (!isset($copyCampaign) || (isset($copyCampaign) && !$copyCampaign))}
		<li {if $_action == "overview"}class="active"{/if}><a href="/ds_campaignOverview/{$_item->id}">Overview Campaign</a></li>
		<li {if $_action == "history"}class="active"{/if}><a href="/ds_campaignHistory/{$_item->id}">History</a></li>
		<li {if $_action == "publishers"}class="active"{/if}><a href="/ds_campaignPublishers/{$_item->id}">Publishers</a></li>
		{if $_item->state <= 8 && isset($userPerms.campaign) && $perms.campaign.addNew & $userPerms.campaign}
			<li {if $_action == "campaignRevert"}class="active"{/if}><a href="/ds_campaignRevert/{$_item->id}">Revert Campaign</a></li>
		{/if}
		{if $_item->state >= 8}
			<li {if $_action == "CampaignActions"}class="active"{/if}><a href="/ds_campaignActions/{$_item->id}">Campaign Actions</a></li>
			{if $_item->race == 1}
				<li {if $_action == "budget"}class="active"{/if}><a href="/ds_campaignBudgetActions/{$_item->id}">Budget Actions</a></li>
			{/if}
			<li {if $_action == "boost"}class="active"{/if}><a href="/ds_campaignBoost/{$_item->id}">Boost Jobs</a></li>
			<li {if $_action == "accepters"}class="active"{/if}><a href="/ds_campaignAcceptedUsers/{$_item->id}">Accepted Users</a></li>
			<li {if $_action == "report"}class="active"{/if}><a href="/ds_campaignReport/{$_item->id}">Instant Report</a></li>
			{if $_item->race == 3}
				<li {if $_action == "click"}class="active"{/if}><a href="/ds_campaignClicks/{$_item->id}">Click Stats</a></li>
				<li {if $_action == "fraud"}class="active"{/if}><a href="/ds_campaignFraud/{$_item->id}">Fraud Control</a></li>
			{/if}
			{if $_item->race == 4}
				<li {if $_action == "cplReview"}class="active"{/if}><a href="/ds_campaignCplReview/{$_item->id}">CPL Review</a></li>
			{/if}
			{if $_item->race == 5}
				<li {if $_action == "cplRemoteReview"}class="active"{/if}><a href="/ds_campaignCplRemoteReview/{$_item->id}">CPL Review</a></li>
			{/if}
			{if $_item->race == 6}
				<li {if $_action == "cpaAffReview"}class="active"{/if}><a href="/ds_cpaAffReview/{$_item->id}">CPA Review</a></li>
			{/if}
		{/if}
	{/if}
</ul>