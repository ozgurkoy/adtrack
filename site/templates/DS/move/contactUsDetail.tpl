{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_contactUs,Contact Us;,Contact Us Detail" }
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box hidden-xs">
                        <div class="widget-content no-padding">
                            <table class="table table-striped">
                                <tbody>
                                    <tr>
                                        <td>Date</td>
                                        <td>{dformat t=$_item->jdate f="Y-m-d H:i"}</td>
                                    </tr>
                                    <tr>
                                        <td>Name Surname</td>
                                        <td>{$_item->cname}</td>
                                    </tr>
                                    <tr>
                                        <td>E-Mail</td>
                                        <td>{$_item->cemail}</td>
                                    </tr>
                                    <tr>
                                        <td>adMingle Username</td>
                                        <td>{if is_object($_item->contact_publisher) && $_item->contact_publisher->gotValue && $_item->contact_publisher->user_publisher->gotValue}{$_item->cname}{/if}</td>
                                    </tr>
                                    <tr>
                                        <td>IP</td>
                                        <td>{$_item->ip}</td>
                                    </tr>
                                    <tr>
                                        <td>Subject</td>
                                        <td>{$_item->subject}</td>
                                    </tr>
                                    <tr>
                                        <td>Message</td>
                                        <td>{$_item->cmessage}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

<script type="text/javascript">
	var editUrl = 'ds_contactUsDetail';
</script>
<script src="/site/layout/DS/js/genericTableList.js"></script>

{include file="DS/footer.tpl"}
</body>
</html>