{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_country,Countries" right="ds_addCountry,New Country"}
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> {$_job|ucfirst} List</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding">
							<table id="countrylist" class="table table-hover table-striped table-bordered table-highlight-head">
								<thead>
								<tr>
									<th>Name</th>
									<th>Local Name</th>
									<th>Phone Code</th>
									<th>URL</th>
									<th>Invite Mode</th>
									<th>Active</th>
									<th class="col-md-2">Action</th>
								</tr>
								</thead>
								<tbody>
								{if $_items->gotValue}
									{doWhile}
										<tr>
											<td>
												{$_items->name} - {$_items->shortName}
											</td>
											<td>
												{$_items->localName}
											</td>
											<td>
												{$_items->countryCode}
											</td>
											<td>
												{$_items->url}
											</td>
											<td>
												{$_items->invite}
											</td>
											<td>
												{$_items->active}
											</td>
											<td>
												<button class="btn btn-xs" href="/ds_add{$_job|ucfirst}/{$_items->id}"><i class="icon-cog"></i></button>
												<button class="btn btn-xs" href="javascript:goIfOk('/ds_del{$_job|ucfirst}/{$_items->id}')"><i class="icon-remove"></i></button>

											</td>
										</tr>
									{/doWhile ($_items->populate() != false)}
								{/if}
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="DS/footer.tpl"}
</body>
</html>