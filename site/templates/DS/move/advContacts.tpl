{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_advertisers,Advertisers;ds_addAdvertiser/`$advertiser->id`,`$advertiser->companyName`;,Contacts"}
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $advertiser->gotValue}Edit{else}New{/if} Advertiser

							</h4>
						</div>
						<div class="widget-content ">
							<div class="row">
								<div class="col-md-12">
									<div class="tabbable tabbable-custom">
										{include file="DS/form/advertiser/nav.tpl" current="contacts" item=$advertiser}
										<div class="tabbable tabbable-custom">

											<table class="table table-hover table-striped table-bordered table-highlight-head">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th class="col-md-2">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {if $_item->gotValue}
                                            {doWhile}
                                            {callUserFunc $_item->loadContactPosition()}
                                            {callUserFunc $_item->loadUserEmail()}
                                            {assign var=title value=$_item->title}
                                            <tr>
                                                <td>
                                                    {$_item->title_options.$title} {$_item->name}
                                                </td>
                                                <td>
	                                                {if $_item->contactPosition}
                                                    {$_item->contactPosition->positionName}
													{/if}
                                                </td>
                                                <td>
                                                    {$_item->contactUserEmail->email}
                                                </td>
                                                <td>
                                                    {$_item->contactTel}
                                                    {if strlen($_item->contactGsm)>0}
                                                    <br/>{$_item->contactGsm}
                                                    {/if}
                                                </td>
                                                <td>
                                                    <button class="btn btn-xs" href="/ds_add{$_job|ucfirst}/{$advertiser->id}/{$_item->id}"><i class="icon-cog"></i></button>
	                                                <button class="btn btn-xs" href="javascript:goIfOk('/ds_del{$_job|ucfirst}/{$_item->id}/{$advertiser->id}')"> <i class="icon-remove"></i></button>

                                                </td>
                                            </tr>
                                            {/doWhile ($_item->populate() != false)}
                                            {/if}
                                            </tbody>
                                        </table>
										<div class="align-right padding-top-10px"><button class="btn btn-success" href="/ds_addAdvContact/{$advertiser->id}">New Contact</button></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="DS/footer.tpl"}
</body>
</html>