{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">
			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_contactUs,Contact Us" }
			<!-- /Breadcrumbs line -->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> {$_job|ucfirst} List</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content no-padding">
							<table class="table table-striped table-bordered table-hover table-checkable datatable" src="/ds_contactUsList">
								<thead>
								<tr>
									<th>Date</th>
									<th>Name Surname</th>
									<th>E-mail</th>
									<th>adMingle Username</th>
									<th class="noSort col-md-2">Action</th>
								</tr>
								</thead>
								<tfoot>
								<tr>
									<th></th>
									<th tp="text">Search by name</th>
									<th tp="text">Search by e-mail</th>
									<th tp="text">Search by username</th>
									<th></th>
								</tr>
								</tfoot>
								<tbody>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>


			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

<script type="text/javascript">
	var editUrl = 'ds_contactUsDetail';
</script>
<script src="/site/layout/DS/js/genericTableList.js"></script>

{include file="DS/footer.tpl"}
</body>
</html>