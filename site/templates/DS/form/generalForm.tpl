{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.25/angular.min.js"></script>
	<script>
		{literal}
		var fpp = angular.module('jgrip', []).config(function() { });
		fpp.directive('dynAttr', function() {
			return {
				scope: { list: '=dynAttr' },
				link: function(scope, elem, attrs){
					for(attr in scope.list){
						elem.attr(scope.list[attr].attr, scope.list[attr].value);
					}
					//console.log(scope.list);
				}
			};
		});

		fpp.directive('formComplete',function(){
			return function(scope, element, attrs) {
				if (scope.$last) setTimeout(function(){
					scope.$emit('onRepeatLast', element, attrs);
				}, 1);
			};
		});

		/*global angular */
		fpp.directive('script', function() {
				return {
					restrict: 'E',
					scope: false,
					link: function(scope, elem, attr)
					{
						if (attr.type==='text/javascript-lazy')
						{
							var s = document.createElement("script");
							s.type = "text/javascript";
							var src = elem.attr('src');
							if(src!==undefined)
							{
								s.src = src;
							}
							else
							{
								var code = elem.text();
								s.text = code;
							}
							document.head.appendChild(s);
							elem.remove();
							/*var f = new Function(code);
							 f();*/
						}
					}
				};
		});


		{/literal}
	</script>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

	<script>
		{literal}
		fpp.controller('jgrepC', function ($scope) {
			$scope.orderByValue = function (value) {
				return value;
			};
			// entity to edit
			$scope.entity = {
				name: 'Max',
				country: 2,
				licenceAgreement: true,
				description: 'I use AngularJS'
			};

			// fields description of entity
			$scope.fields = [
				{
					name: 'name',
					title: 'Name',
					info:'aasdfasf',
					type:'text',
					required: true,
					custom:[
						{ attr: 'dataMask', value: 'dddaaatt' },
						{ attr: 'abc', value: 'val' }
					]
				},
				{
					name: 'name',
					title: 'Name',
					info:'aasdfasf',
					type:'date',
					value:'11-11-2012',
					required: true
				},
				{
					name: 'file',
					title: 'File',
					required: true,
					type: {
						view: 'file'
					}
				},
				{
					name: 'country',
					title: 'Country',
					type: 'select',
					value:6,
					options: [
							{key:'USA',value:5},
							{key:'German',value:1},
							{key:'Russia',value:6}
					]
				},
				{
					name: 'cbs',
					title: 'Group',
					type: 'checkboxGroup',
					fields: [
							{title:'g1',name:'g1',value:0},
							{title:'g2',name:'g2',value:1},
							{title:'g3',name:'g3',value:1}
					]
				},
				{
					name: 'licenceAgreement',
					title: 'Licence Agreement',
					type:'checkbox',
					value:0
				},
				{
					name: 'description',
					title: 'Description',
					type: 'textarea',
					value:'asdfasfsa1\n asdfasf'
				}
			];
			$scope.$on('onRepeatLast', function(scope, element, attrs){
				//work your magic
				"use strict";

				App.init(); // Init layout and core plugins
				Plugins.init(); // Init all plugins
				FormComponents.init(); // Init all form-specific plugins

			});
		});
		{/literal}
	</script>

</head>

<body ng-app="jgrip">

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_language,Languages;a,b" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								[ACTION] [JOB]
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">

								<input type="hidden" name="object" value="123"/>
								<input type="hidden" name="id" value="123"/>
								<div ng-controller="jgrepC"  >
									<div ng-repeat="field in fields" form-complete="1" >
										<span ng-switch on="field.type">
										    <span ng-switch-when="text">
											    <span ng-include="'/site/templates/DS/ng/text.html'"></span>
										    </span>
										    <span ng-switch-when="date">
											    <span ng-include="'/site/templates/DS/ng/date.html'"></span>
										    </span>
										    <span ng-switch-when="textarea">
											    <span ng-include="'/site/templates/DS/ng/textarea.html'"></span>
										    </span>
										    <span ng-switch-when="checkbox">
											    <span ng-include="'/site/templates/DS/ng/checkbox.html'"></span>
										    </span>
										    <span ng-switch-when="checkboxGroup">
											    <span ng-include="'/site/templates/DS/ng/checkboxGroup.html'"></span>
										    </span>
										    <span ng-switch-when="select">
											    <span ng-include="'/site/templates/DS/ng/select.html'"></span>
										    </span>
											<span ng-switch-default>{literal}{{{/literal}field.type{literal}}}{/literal}</span>

										</span>
									</div>
								</div>
								{*{formBox type="checkboxGroup" label="Target" name="target" value="" options=$options}*}

								{*{formBox type="text" label="Language Name" name="langName" value="kafas"}*}
								{*{formBox type="textarea" label="Short Name" name="langShort" value="hebe"}*}
								{*{formBox type="checkbox" label="Active" name="active" value=1}*}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->
	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>