{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="/darkside">Home</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="/ds_roles">Roles</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $role->gotValue}
									Edit
								{else}
									Add New
								{/if}
								Role
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_roleAction" name="role" id="role" method="POST" target="ps">
								{if $role->gotValue}
									<input type="hidden" name="id" value="{$role->id}"/>
								{/if}
								{getFormToken f="role"}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Role Label</label>
									<div class="col-md-10">
										<input type="text" id="label" name="label" class="form-control" value="{$role->label}">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Auth Level</label>
									<div class="col-md-10">
										<input type="text" id="authLevel" name="authLevel" class="form-control" value="{if $role->gotValue}{$role->authLevel}{else}15{/if}">
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

<script>
	{literal}
	$( "#authLevel" ).spinner({
		step: 5,
		min:0,
		numberFormat: "n"
	});

	{/literal}
</script>
</body>
</html>