{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{assign var="formName" value="bug"}
{if $bug->gotValue}
	{assign var="actionLabel" value="Edit"}
	{assign var="formName" value="editBug"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_bug/`$type`,`$typeLabel` List;ds_addBug/`$type`,`$actionLabel` `$typeLabel`"}
			<!-- /Breadcrumbs line -->
			<!--=== Bug Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel} {$typeLabel}
							</h4>
						</div>
						<div class="widget-content">

							{callUserFunc $bug->loadLastOperator()}
							{callUserFunc $bug->loadAssignee()}
							{callUserFunc $bug->loadBranch()}
							{callUserFunc $bug->loadBcategory()}
							{callUserFunc $bug->loadProject()}
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$formName}" id="{$formName}" method="POST" target="ps" enctype="multipart/form-data"  >
								{getFormToken f="bug"}
								{if $bug->gotValue}
									<input type="hidden" name="id" value="{$bug->id}"/>
								{/if}
								{formBox type="text" label="Title" name="title" value=$bug->title}
								{formBox type="text" label="Link" name="link" value=$bug->link}
								{if $bug->gotValue!==true}
								{formBox type="textarea" label="Description" name="details" value=null}
								{/if}
								{formBox type="arrayKVSelect" label="Priority" defaultValue="Choose Priority" name="priority" options=$bug->priority_options value=$bug->priority}

								{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}
								{formBox type="arrayKVSelect" label="Type" defaultValue="Choose Type" name="bugType" options=$bug->bugType_options value=$bug->bugType value=$type}
								{else}
									{if $bug->gotValue}
									<input type="hidden" name="bugType" value="{$bug->bugType}"/>
									{else}
									<input type="hidden" name="bugType" value="3"/>{*only question*}
									{/if}
								{/if}
								{formBox type="populateSelect" label="Project" defaultValue="Choose Project" name="project" vkey="name" options=$projects value=$bug->project}
								{if !(isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug)}
									<script>
										{literal}
										with($('#project')){(o=find('option')).length>1 && val(o[1].value)};
										{/literal}
									</script>
								{/if}
								{if isset($userPerms.bug) && $perms.bug.bugAdmin & $userPerms.bug}
								{formBox type="date" label="Expected Date" defaultValue="Choose Expected Start Date" name="startDate" value=$bug->startDate}

								{if $bug->gotValue!==true}
								{formBox type="populateSelect" label="Assignee" defaultValue="Choose Assignee" name="assignee" vkey="username" options="DSuser" value=$bug->assignee}
								{/if}


								{formBox type="populateSelect" label="Branch" name="branch" options="branch" vkey="name" value=$bug->branch}
								{formBox type="text" label="Tags" name="tags" value=$bug->tagz}

								{formBox type="populateSelect" label="Category" name="category" options="category" vkey="name" value=$bug->bcategory}
								{/if}

								{if $bug->gotValue!==true}
								{formBox type="file" label="File/SS" name="file[]" multi=1 value=null}
								{/if}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Bug Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
<script>
	{literal}
	$(function() {
	{/literal}

	var availTags = [];
	{foreach from=$tags item=tag}
	availTags.push('{$tag}');
	{/foreach}
	{literal}
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}

	$( "#tags" )
		// don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) {
				if ( event.keyCode === $.ui.keyCode.TAB &&
						$( this ).autocomplete( "instance" ).menu.active ) {
					event.preventDefault();
				}
			})
			.autocomplete({
				minLength: 0,
				source: function( request, response ) {
					// delegate back to autocomplete, but extract the last term
					response( $.ui.autocomplete.filter(
							availTags, extractLast( request.term ) ) );
				},
				focus: function() {
					// prevent value inserted on focus
					return false;
				},
				select: function( event, ui ) {
					var terms = split( this.value );
					// remove the current input
					terms.pop();
					// add the selected item
					terms.push( ui.item.value );
					// add placeholder to get the comma-and-space at the end
					terms.push( "" );
					this.value = terms.join( ", " );
					return false;
				}
			});
	});
	{/literal}
</script>
</body>
</html>