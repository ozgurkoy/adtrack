{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="/darkside">Home</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="/ds_menus">Menus</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{if $menu->gotValue}
									Edit
								{else}
									Add New
								{/if}
								Menu
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_menuAction" name="menu" id="menu" method="POST" target="ps">
								{if $menu->gotValue}
									<input type="hidden" name="id" value="{$menu->id}"/>
								{/if}
								{getFormToken f="menu"}
								<div class="form-group">
									<label class="col-md-2 control-label bs-tooltip" for="input17" data-placement="bottom" data-original-title="Label for the menu">Label</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<input type="text" class="form-control" id="label" name="label" value="{$menu->label}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label bs-tooltip" for="input17" data-placement="bottom" data-original-title="Link for the menu, leave empty if goes to nowhere">Link</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<input type="text" class="form-control" id="link" name="link" value="{$menu->link}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label bs-tooltip" for="input17" data-placement="bottom" data-original-title="Icon class for the menu item">CSS Class</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<input type="text" class="form-control" id="css" name="css" value="{$menu->css}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label bs-tooltip" for="input17" data-placement="bottom" data-original-title="Sub menu links for menu, like addWord, editLang. Seperate with a comma.">Sub Links</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<input type="text" class="form-control" id="subMenus" name="subMenus" value="{$menu->subMenus}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Is Active?</label>
									<div class="col-md-10">
										<input type="checkbox" id="isActive" name="isActive" class="form-control uniform" value="1" {if $menu->isActive==1}checked="checked"{/if}>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Top Menu</label>
									<div class="col-md-10">
										<select name="topMenu" id="topMenu">
											<option value="0">Root</option>
											{if sizeof($menuList)>0}

												{foreach from=$menuList key=mid item=men}
													{assign var=lev value=$men.level+1}
													<option value="{$mid}" {if $menu->topMenu&&$menu->topMenu->id==$mid}selected{/if}>{"&nbsp;"|str_repeat:$lev*5}{$men.label}</option>
												{/foreach}
											{/if}
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="widget box">
										<div class="widget-header">
											<h4><i class="icon-reorder"></i> Roles</h4>
										</div>
										<div class="widget-content clearfix">
											<!-- Left box -->
											<div class="left-box">
												<input type="text" id="box1Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box1Clear" class="filter">x</button>
												<select id="box1View" multiple="multiple" class="multiple">
													{if $roles->gotValue}
														{doWhile}
															<option value="{$roles->id}">{$roles->label}</option>
														{/doWhile ($roles->populate())}
													{/if}
												</select>
												<span id="box1Counter" class="count-label"></span>
												<select id="box1Storage"></select>
											</div>
											<!--left-box -->

											<!-- Control buttons -->
											<div class="dual-control">
												<button id="to2" type="button" class="btn">&nbsp;&gt;&nbsp;</button>
												<button id="allTo2" type="button" class="btn">&nbsp;&gt;&gt;&nbsp;</button><br>
												<button id="to1" type="button" class="btn">&nbsp;&lt;&nbsp;</button>
												<button id="allTo1" type="button" class="btn">&nbsp;&lt;&lt;&nbsp;</button>
											</div>
											<!--control buttons -->

											<!-- Right box -->
											<div class="right-box">
												<input type="text" id="box2Filter" class="form-control box-filter" placeholder="Filter entries..."><button type="button" id="box2Clear" class="filter">x</button>
												<select id="box2View" name="menuRoles[]" multiple="multiple" class="multiple">
													{if $menu->menuRoles && $menu->menuRoles->gotValue}
														{doWhile}
															<option value="{$menu->menuRoles->id}">{$menu->menuRoles->label}</option>
														{/doWhile ($menu->menuRoles->populate())}
													{/if}
												</select>
												<span id="box2Counter" class="count-label"></span>
												<select id="box2Storage"></select>
											</div>
											<!--right box -->
										</div>
									</div>
								</div>

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
<script type="text/javascript">
	var id=-1;
	{literal}

	$('#menuWord').typeahead({
		remote: '/xhr/words/%QUERY/'+id+'/wordOnly',
		name: 'menuWord'
	});


	{/literal}
</script>
{include file="DS/footer.tpl"}
</body>
</html>