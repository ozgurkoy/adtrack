{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			<div class="crumbs">
				<ul id="breadcrumbs" class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="/darkside">Home</a>
					</li>
					<li>
						<i class="icon-road"></i>
						<a href="/ds_roles">Roles</a>
					</li>
				</ul>

			</div>
			<!-- /Breadcrumbs line -->

			<!--=== Page Header ===-->
			<div class="page-header">
			</div>
			<!-- /Page Header -->

			<!--=== Page Content ===-->


			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$role->label} Role Actions
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_roleModAction" name="roleMod" id="roleMod" method="POST" target="ps">
								<input type="hidden" name="id" value="{$role->id}"/>
								{getFormToken f="roleMod"}
								{if $actions->gotValue}
									{doWhile}
									{callUserFunc $actions->parseTasks()}
                                <table class="table table-hover table-striped table-bordered table-highlight-head">
                                    <thead>
                                    <tr>
                                        <th class="col-md-3">{$actions->label} Actions</th>
	                                    {foreach from=$actions->parsedTasks key=task item=k0}
                                        <th>{$task}</th>
										{/foreach}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {callUserFunc $role->parsePerms()}
                                    {assign var="actionId" value=$actions->id}
                                    {*{callUserFunc $actions->loadPermissionofAction()}*}
                                    <tr>
	                                    <td></td>
	                                    {foreach from=$actions->parsedTasks key=task item=bit}
										<td>
                                            <input type="checkbox" name="d_{$actions->id}[]"
                                                   class="form-control uniform" value="{$bit}"
                                                   {if array_key_exists($actions->id, $role->parsedPerms) && ($role->parsedPerms[$actionId] & $bit) > 0}checked="checked"{/if}>
                                        </td>
	                                    {/foreach}
                                    </tr>
                                    </tbody>
                                </table>
								{/doWhile ($actions->populate() != false)}

								{/if}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
</body>
</html>