{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_cplLandingPages,CPL Landing Pages;,$actionLabel" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel} CPL Landing Pages
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" enctype="multipart/form-data" action="/ds_cplLandingPageAction" name="{$_job}" id="{$_job}" method="POST" target="ps">

								{if $_item->gotValue}
									<input type="hidden" name="id" value="{$_item->id}"/>
									<input type="hidden" name="oldPreHash" value="{$_item->preHash}"/>
								{/if}
								{getFormToken f=$_job}
								{callUserFunc $_item->loadCplCampaign_advertiser()}
								{callUserFunc $_item->loadForm()}
								{callUserFunc $_item->loadLayout()}
								{formBox type="text" label="Name" name="name" value=$_item->name}
								{formBox type="text" label="Form submitted message" info="The message user will see after the form is submitted." name="endMessage" value=$_item->endMessage}
								{formBox type="text" label="Campaign Hash" info="Code for campaign preview. Like http://l.admingle.com/Alitalia" name="preHash" value=$_item->preHash}
								{formBox type="textarea" label="Footer HTML" info="JS code etc." name="footerText" value=$_item->footerText|@htmlentities|stripslashes}
								{formBox type="populateSelect" name="cplCampaign_advertiser" label="Advertiser" options="advertiser" vkey="companyName" value=$_item->cplCampaign_advertiser}
								{formBox type="populateSelect" name="form" label="Form" options="cplForm" vkey="label" value=$_item->form}
								{formBox type="populateSelect" name="layout" label="Layout" options="cplLayout" vkey="name" value=$_item->layout}
								{formBox type="file" label="Favicon" info="Favicon for the landing page, USE ICO FILE FOR MAXIMUM COMPATIBILITY" name="favicon" value=$_item->endMessage}
								<div id="layoutParams" class="form-group">

								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Save & Preview <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
<script>
	{if $_item->gotValue}
	var data = {$_item->details};

	{literal}
	$(document ).ready(function(){
		chooseLayout( $('#layout' ).val(),function(){
			applyData( data );
		});
	});
	{/literal}
	{/if}

	{literal}
	var lap = $( "#layoutParams" );
	String.prototype.ucwords = function() {
		str = this.toLowerCase();
		return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
				function($1){
					return $1.toUpperCase();
				});
	};

	$('#layout' ).change(function(){
		chooseLayout( $(this ).val() );
	});

	var es = $('#endMessage').typeahead({
		remote: '/ds_xhr/cplWord/%QUERY'
	});

	function chooseLayout(layoutVal, callback){
		var pane = $( '#layoutParams' );
		pane.html( '' );
		$.get('/ds_xhr/layoutProps/'+layoutVal, function ( data ) {
			data = JSON.parse( data );
			if ( data.regulars ) {
				pane.append( '<h2 style="margin-left:25px">Regular Fields</h2>' );
				$.each( data.regulars, function ( a, b ) {
					appendField( pane, a, b );
				} );
			}
			if ( data.repeats ) {
				pane.append( '<h2 style="margin-left:25px">Repeating Areas</h2>' );
				$.each( data.repeats, function ( rname, robj ) {
					var repeat = rname.replace( '/\s/', '' ),
							rand = 'C_' + Math.floor( (Math.random() * 10000) + 40 );

					pane.append( '<h3 style="margin-left:25px">' + robj.label.ucwords() +' '+(robj.maxCount && robj.maxCount>0?'<sub style="font-style: italic;">(Max '+robj.maxCount+' items will appear on the layout)</sub> &nbsp;':'') + ' ' +
							'<a href="javascript:;" data-repeat="repeatBox_' + rname + '" class="addSub">+</a>' +
							'</h3>' );

					pane.append( '<div id="repeatBox_' + rname + '" class="ridd"  style="position:relative" >' +
							'<a href="javascript:;" style="display:none;position:absolute;top:5px;right:10px" class="removeSub">remove</a>' +
							'</div' );
					$.each( robj.fields, function ( a, b ) {
						appendField( $( '#repeatBox_' + rname ), a, b, repeat );
					} );
				} );
			}

			if ( typeof callback != 'undefined' )
				callback();
		});
	}

	function applyData( data ) {
		var self = this;
		self.applyField = function ( fieldName, value ) {
			var f = $( '#field_' + fieldName );
			if ( f.length > 0 ) {
				if ( f.attr( 'type' ) == 'file' ) {
					var fid = f.attr( 'id' ), fname = f.attr( 'name' );
					f.attr( 'id', '___' + fid );
					f.attr( 'name', '___' + fname );

					if ( value && value.length > 0 ) {
						f.after( '<img style="height:100px" class="_iprv" src="' + value + '">' );
						f.after( '<input type="hidden" name="' + fname + '" class="file_rep _iprv" value="' + value + '" id="' + fid + '">' );
						f.after( '<a href="javascript:;" style="clear:both;float:left;width:100%" class="fileChange _iprv">Change</a>' )
						f.hide();
					}
				}
				else
					f.val( value );
			}
			else
				alert( '#field_' + fieldName + ' Problem, inform ozgur@admingle.com' );
		};
		for ( var field in data ) {
			console.log(field+' '+typeof data[field]+' '+data[field].length);
			if(typeof data[field] == 'object'){
				//repeat
				$.each(data[field], function ( index, ob ) {
					if ( index > 0 )
						duplicate( 'repeatBox_' + field );
					for ( var obj in ob ) {
						self.applyField( field + '_' + obj + '_' + index, ob[obj] );
					}
				});
			}
			else{
				this.applyField( field, data[field] );
			}
		}
	}

	function duplicate( id ){
		var ox = $( '#' + id ),
				oy = ox.clone();
		oy.find( '._iprv' ).remove();
		oy.attr('id', 'C_'+Math.floor( (Math.random() * 10000) + 40 ) );
		oy.find( 'input.ff,textarea.ff' ).each( function ( i, ele ) {
			var pvid = $( ele ).attr( 'id' ).replace( '___', '' ),
					pvName = $( ele ).attr( 'name' ).replace( '___', '' ),
					idPart = pvid.substr( 0, pvid.lastIndexOf( '_' ) + 1 ),
					namePart = pvName.substr( 0, pvName.lastIndexOf( '[' ) );

			$( ele ).attr( 'name', $( ele ).attr( 'name' ).replace( '___', '' ) );
			$( ele ).show().attr( 'id', idPart + $( '*[id^="' + idPart + '"]' ).length )
			$( ele ).show().attr( 'name', namePart + '[' + $( '*[name^="' + namePart + '"]' ).length + ']' )
		});
		oy.css( 'border-top', '1px solid #ececec' );
//		ox.parent().append( oy );
		ox.after( oy );
		oy.find( '.removeSub' ).show();
	}


	lap.on( "click", ".removeSub", function() {
		$( this ).parents( '.ridd:first' ).remove();
	});


	lap.on( "click", ".fileChange", function () {
		with ( $( this ).parents( '.form-group:first' ) ) {
			var fid = find( '.ff' ).attr( 'id' ),fname = find( '.ff' ).attr( 'name' );
			find( '._iprv' ).remove();
			find( '.ff' ).show().attr( 'id', fid.replace( '___', '' ) ).attr( 'name', fname.replace( '___', '' ) );
		}
		$( this ).remove();
	});

	lap.on( "click", ".addSub", function() {
		duplicate( $( this ).attr( 'data-repeat' ) );
	});

	function appendField( pane, name, fobj, repeat, value ) {
		//file has to be image. otherwise why host it on server?
		var fieldId = 'field_' + (typeof repeat == 'undefined' ? name : repeat + '_' + name );
		var		rlength = $( '*[id^="' + fieldId + '"]' ).length;

		if ( typeof repeat != 'undefined' ){
			fieldId += '_' + rlength;
		}
		var fieldName = (typeof repeat == 'undefined' ? '[' + name + ']' : '[' + repeat + '][' + name + ']['+rlength+']' );

		pane.append( '' +
				'<div class="form-group ' + (typeof repeat == 'undefined' ? '' : repeat ) + '">' +
				'<label class="col-md-2 control-label">' + fobj.label.ucwords() +
				((fobj.description&&fobj.description.length>0)?'<span style="display: block; font-size: 10px; font-style: italic; font-weight: normal;">' + fobj.description+'</span>':'') +
				'</label>' +
				'<div class="col-md-10">' +
				(fobj.type == "file" ?
				 '<input type="file" class="ff" id="' + fieldId + '" name="formFields' + fieldName + '">' + (typeof value == 'undefined' ? '' : '<img src="' + value + '">' ) :
				 '<textarea class="ff" id="' + fieldId + '" name="formFields' + fieldName + '">' + (typeof value == 'undefined' ? "" : value) + '</textarea>' +
						 '<br /><a href="javascript:multiLang('+"'"+fieldId+"'"+')" id="mlswitch_'+fieldId+'">Multilang</a>'
						) +
				'</div>' +
				'</div>' );
	}

	function multiLang( fieldId ) {
		$( '#mlswitch_' + fieldId ).remove();

		$( '#' + fieldId ).typeahead( {
			remote : '/ds_xhr/cplWord/%QUERY'
		} );

	}

	function winOpen( url ) {
		window.open( url );
	}

	{/literal}

</script>
</body>
</html>