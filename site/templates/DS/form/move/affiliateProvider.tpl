{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_APList,Affiliate Providers;,$actionLabel" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel} {$_job|ucfirst}
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_addAPAction" name="{$_job}" id="{$_job}" method="POST" target="ps">
								{if $_item->gotValue}
									<input type="hidden" name="id" value="{$_item->id}"/>
								{/if}

								{getFormToken f=$_job}

								{formBox type="text" label="Name" name="name" value=$_item->name}
								{formBox type="text" label="Hash Code" name="hashCode" value=$_item->hashCode}
								{formBox type="checkbox" label="API Support" name="apiSupport" value=$_item->apiSupport}
								{formBox type="text" label="API Address" name="apiAddress" value=$_item->apiAddress}
								{formBox type="checkbox" label="Tracking Pixel Support" name="trackingPixelSupport" value=$_item->trackingPixelSupport}
								{formBox type="text" label="Tracking Pixel URL" name="trackingPixelUrl" value=$_item->trackingPixelUrl}
								{formBox type="text" label="Tracking Pixel Information" name="trackingPixelInformation" value=$_item->trackingPixelInformation}
								{formBox type="text" label="Details" info="details" name="details" value=$_item->details}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>