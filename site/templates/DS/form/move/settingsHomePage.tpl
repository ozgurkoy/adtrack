{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
						      method="POST" target="ps">
							{getFormToken f=$_job}
							<div class="widget-content">

								{formBox type="text" label="Facebook Page" info="Leave Empty to remove from fooler link" name="facebook" value=$ADM_SETTINGS.FACEBOOK_PAGE}
								{formBox type="text" label="Twitter Account" info="Leave Empty to remove from fooler link" name="twitterUser" value=$ADM_SETTINGS.TWITTER_USER}
								{formBox type="text" label="Linkedin" info="Leave Empty to remove from fooler link" name="linkedin" value=$ADM_SETTINGS.LINKEDIN_PAGE}
								{formBox type="text" label="YouTube" info="Leave Empty to remove from fooler link" name="youtube" value=$ADM_SETTINGS.YOUTUBE_PAGE}
								{formBox type="text" label="Google Plus" info="Leave Empty to remove from fooler link" name="googlePlus" value=$ADM_SETTINGS.GOOGLEPLUS_PAGE}
								{formBox type="text" label="Instagram" info="Leave Empty to remove from fooler link" name="instagram" value=$ADM_SETTINGS.INSTAGRAM_PAGE}
								{formBox type="text" label="VK Page" info="Leave Empty to remove from fooler link" name="vk" value=$ADM_SETTINGS.VK_PAGE}

								{formBox type="text" label="Blog" name="blog" value=$ADM_SETTINGS.BLOG_PAGE}
								{formBox type="textarea" label="Homepage Address" name="HOME_ADDRESS" value=$ADM_SETTINGS.HOME_ADDRESS}
								{formBox type="textarea" label="Homepage Emails" name="HOME_EMAILS" value=$ADM_SETTINGS.HOME_EMAILS}
								{formBox type="textarea" label="Homepage Phone" name="HOME_PHONE" value=$ADM_SETTINGS.HOME_PHONE}
								{formBox type="radio" label="Show Publisher video" name="SHOW_PUBLISHER_VIDEO" options=$visibleOptions value=$ADM_SETTINGS.SHOW_PUBLISHER_VIDEO}
								{formBox type="radio" label="Show Advertiser video" name="SHOW_ADVERTISER_VIDEO" options=$visibleOptions value=$ADM_SETTINGS.SHOW_ADVERTISER_VIDEO}
								{formBox type="radio" label="Show countact us Map" name="HOME_MAP_SHOW" options=$visibleOptions value=$ADM_SETTINGS.HOME_MAP_SHOW}
								{formBox type="radio" label="Show Cookie Approval" name="SHOW_COOKIE_WARNING" options=$visibleOptions value=$ADM_SETTINGS.SHOW_COOKIE_WARNING}
								{formBox type="textarea" label="Homepage Map URL" name="HOME_MAP_URL" value=$ADM_SETTINGS.HOME_MAP_URL}


								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>


							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>