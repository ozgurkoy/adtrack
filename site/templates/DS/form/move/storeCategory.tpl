{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_store,Store Brands;ds_storeCategory/`$brand->id`,`$brand->name` Categories"
			right="ds_addStoreCategory/`$brand->id`,New Category"}
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel} Store Category to {$brand->name}
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" enctype="multipart/form-data" target="ps">
								<input type="hidden" name="brandId" value="{$brand->id}"/>

								{if $_item->gotValue}
									<input type="hidden" name="id" value="{$_item->id}"/>
								{/if}
								{getFormToken f=$_job}
								{formBox type="text" label="Name" name="name" value=$_item->name}
								{formBox type="text" label="Price" name="price" value=$_item->price}
								{formBox type="textarea" label="Description" name="description" value=$_item->description}
								{formBox type="checkbox" label="Active" name="active" value=$_item->active}
								{formBox type="checkbox" label="Can have products" name="containsProducts" value=$_item->containsProducts}
								{formBox type="file" label="Image" name="image" value=$_item->image}

								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Top Category</label>
									<div class="col-md-10">
										<select name="topCategory" id="topCategory">
											<option value="0">Root</option>
											{if sizeof($catList)>0}
												{foreach from=$catList key=mid item=men}
													{assign var=lev value=$men.level+1}
													<option value="{$mid}" {if $_item->parentCategory && $_item->parentCategory->id == $mid}selected{/if}>{"&nbsp;"|str_repeat:$lev*5}{$men.name}</option>
												{/foreach}
											{/if}
										</select>
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>