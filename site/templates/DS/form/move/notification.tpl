{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_notifications,notifications;,$actionLabel" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel} {$_job|ucfirst}
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
								{if $_item->gotValue}
									<input type="hidden" name="id" value="{$_item->id}"/>
								{/if}
								{getFormToken f=$_job}

								{formBox type="text" label="Title" name="title" value=$_item->title}
								{formBox type="checkbox" label="Is Active" name="isActive" value=$_item->isActive}
								{formBox type="arrayKVSelect" label="Type" name="type" options=$_item->type_options value=$_item->type}
								<div class="form-group"  id="footerLn">
									<label class="col-md-2 control-label bs-tooltip" for="input17" data-placement="bottom" data-original-title=""><i class="icon-info-sign"></i> Definition</label>
									<div class="col-md-10">
										<div class="col-md-10">
											<input type="text" class="form-control" id="word" name="word" value="{$_item->word}">
										</div>
									</div>
								</div>

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
<script>
	{literal}

	$('#word').typeahead({
		remote: '/ds_xhr/word/%QUERY',
		name: 'word'
	});
	{/literal}
</script>
</body>
</html>