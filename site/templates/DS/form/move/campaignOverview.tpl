{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>
	<style>
		{literal}
		.camp_state {margin-bottom: 10px;}
		{/literal}
	</style>
</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_campaigns,Campaigns;ds_campaignOverview/`$_item->id`,'`$_item->title`' Campaign;,Campaign Overview" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<!-- Tabs-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						{include file="DS/_campaignTabs.tpl"}
						<div class="tab-content row">
							<!--=== AddEdit ===-->

							<div class="tab-pane active" id="tab_overview">
									<div class="col-md-3">
										<div class="widget box">
											<div class="widget-header">
												<h4><i class="icon-reorder"></i>
													Campaign Status
												</h4>
											</div>
											<div class="widget-content">
												<button class="btn btn-block camp_state" id="camp_state_1" state="1" disabled>Reviewing</button>
												<button class="btn btn-block camp_state" id="camp_state_2" state="2" disabled>Accept/Reject Campaign</button>
												<button class="btn btn-block camp_state" id="camp_state_4" state="4" disabled>Offer sent, waiting for response</button>
												<button class="btn btn-block camp_state" id="camp_state_5" state="5" disabled>Offer response received</button>
												<button class="btn btn-block camp_state" id="camp_state_7" state="7" disabled>Waiting for payment</button>
												<button class="btn btn-block camp_state" id="camp_state_8" state="8" disabled>Waiting for start</button>
												<button class="btn btn-block camp_state" id="camp_state_9" state="9" disabled>Campaign is active</button>
												<button class="btn btn-block camp_state" id="camp_state_10" state="10" disabled>Campaign finished</button>
												{if isset($userPerms.campaign) && isset($perms.campaign.deleteCampaign) && $perms.campaign.deleteCampaign & $userPerms.campaign}
												<button class="btn btn-danger btn-block btn-delete-campaign">Delete Campaign</button>
												{/if}
											</div>
										</div>
									</div>
									<div class="col-md-9">
										<div class="widget box">
											<div class="widget-header">
												<h4><i class="icon-reorder"></i>
													Action
												</h4>
											</div>
											<div class="widget-content">
												{if $_item->state == 1}
													<form class="form-horizontal row-border" action="/ds_campaignApprove" name="campaignOverview" id="campaignOverview" method="POST" target="ps">
														{getFormToken f="campaignOverview"}
														<input type="hidden" name="id" value="{$_item->id}"/>
														<div class="alert alert-info fade in">
															The campaign waiting for your approval. Please review it and approve or discard.
														</div>
														{formBox type="radio" label="Action" name="approve" class="radio-inline" options=$approveActions value=null}
														{formBox type="textarea" label="Deny Reason" name="denyReason" value=null class="hide-default"}
														<div class="row">
															<div class="col-md-12 form-vertical no-margin">
																<div class="form-actions">

																	<button type="button" href="/ds_campaignAddEdit/{$_item->id}" class="btn-warning  btn pull-left">
																		<i class="icon-angle-left"></i> Edit
																	</button>
																	 &nbsp;
																	{if $_item->race !=1 }
																	<button type="button" data-blank="1" href="/ds_campaignPublisherPreview/{$_item->publicID}" style="margin-left:10px" class="btn-info btn pull-left">
																		<i class="icon-angle-up"></i> Preview
																	</button>
																	{/if}
																	<button type="submit" class="submit btn btn-primary pull-right">
																		Save <i class="icon-angle-right"></i>
																	</button>
																</div>
															</div>
														</div>
													</form>
												{elseif $_item->state == 2}
													<form class="form-horizontal row-border" action="/ds_campaignSendOffer" name="campaignOffer" id="campaignOffer" method="POST" target="ps">
														{getFormToken f="campaignOffer"}
														<input type="hidden" name="id" value="{$_item->id}"/>
														<div class="alert alert-info fade in">
															Campaign accepted. Please create your proposal below.
														</div>
														{formBox type="text" label="Publisher Count" name="pubCount" value=$_item->publisherCount disabled="disabled"}
														{if $_item->race == 1}
														{formBox type="text" label="Total Follower" name="folCount" value=$_item->campaign_campaignTwitter->follower disabled="disabled"}
														{formBox type="text" label="Total Influence" name="infCount" value=$_item->campaign_campaignTwitter->influencer disabled="disabled"}
														{/if}
														{formBox type="text" label="Budget" name="budget" value=$_item->budget disabled="disabled"}
														{formBox type="text" label="Agency Commission" name="commission" value=$_item->commission disabled="disabled"}
														{if $_item->race == 1}
														{formBox type="text" label="Expected Follower" name="expectedFolCount" value=$_item->campaign_campaignTwitter->getExpectedValue("follower") disabled="disabled"}
														{formBox type="text" label="Expected Influence" name="expectedInfCount" value=$_item->campaign_campaignTwitter->getExpectedValue("influencer") disabled="disabled"}
														{/if}
														{formBox type="text" label="Extra Budget" name="extraBudget" value=null}
														{formBox type="text" label="Total Cost" name="totalCost" value=$_item->budget+$_item->commission disabled="disabled"}

														<div class="row">
															<div class="col-md-12 form-vertical no-margin">
																<div class="form-actions">
																	<button type="submit" class="submit btn btn-primary pull-right">
																		Save <i class="icon-angle-right"></i>
																	</button>
																</div>
															</div>
														</div>
													</form>
												{elseif $_item->state == 3 || $_item->state == 5}
													<form class="form-horizontal row-border" action="/ds_campaignRevive" name="campaignRevive" id="campaignRevive" method="POST" target="ps">
														{getFormToken f="campaignRevive"}
														<input type="hidden" name="id" value="{$_item->id}"/>
														<div class="alert alert-danger fade in">
															Campaign {if $_item->state == 3}discarded{else}offer denied{/if}. Click the button below to revive it.
														</div>
														{if $_item->state == 3}
														<div class="alert alert-warning fade in">
															Deny reason: {$_item->denyReason}
														</div>
														{/if}
														<p>
															<button class="btn btn-warning btn-notification">Revive</button>
														</p>
													</form>
												{elseif $_item->state == 4}
													<form class="form-horizontal row-border" action="/ds_campaignApproveOffer" name="campaignApproveOffer" id="campaignApproveOffer" method="POST" target="ps">
														{getFormToken f="campaignApproveOffer"}
														<input type="hidden" name="id" value="{$_item->id}"/>
														<input type="hidden" name="offerReply" id="offerReply" value="approve"/>
														<div class="alert alert-warning fade in">
															Campaign offer sent. Click the button below to approve by your side.
														</div>
														<p>
															<button class="btn btn-warning btn-success">Approve Offer</button>
															<button class="btn btn-warning btn-danger" id="declineOffer">Decline Offer</button>
														</p>
													</form>
												{elseif $_item->state == 6 || $_item->state == 7}
													<form class="form-horizontal row-border" action="/ds_campaignPublish" name="campaignPublish" id="campaignPublish" method="POST" target="ps">
														{getFormToken f="campaignPublish"}
														<input type="hidden" name="id" value="{$_item->id}"/>
														<div class="alert alert-info fade in">
															Campaign offer accepted.
														</div>
														{* if isset($balanceWarning)}
															<div class="alert alert-danger">
																Advertiser budget is not available for this campaign. Please send an information to the company.
															</div>
														{/if *}
														{formBox type="text" label="Publisher Count" name="pubCount" value=$_item->publisherCount disabled="disabled"}
														{formBox type="text" label="Budget" name="budget" value=$_item->budget disabled="disabled"}
														{formBox type="text" label="Agency Commission" name="commission" value=$_item->commission disabled="disabled"}
														{if $_item->race == 1}
															{formBox type="text" label="Expected Follower" name="expectedFolCount" value=$_item->campaign_campaignTwitter->getExpectedValue("follower") disabled="disabled"}
															{formBox type="text" label="Expected Influence" name="expectedInfCount" value=$_item->campaign_campaignTwitter->getExpectedValue("influencer") disabled="disabled"}
														{/if}
														{formBox type="text" label="Extra Budget" name="extraBudget" value=$_item->extraBudget}
														{formBox type="text" label="Total Cost" name="totalCost" value=$_item->calculateTotalCost() disabled="disabled"}
														{formBox type="text" label="Start date" name="startDate" value=$_item->startDate class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat disabled="disabled"}
														{formBox type="text" label="End date" name="endDate" value=$_item->endDate class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat disabled="disabled"}
														{formBox type="radio" label="Publish Time" name="publishOption" options=$publishTimeOptions value=null class="radio-inline"}
														{formBox type="text" label="Publish Date" name="publishDate" value=null class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat}
														{formBox type="text" label="Publish Hour" name="publishTime" value=null class="timepicker-fullscreen" icon="icon-calendar"}

														<div class="row">
															<div class="col-md-12 form-vertical no-margin">
																<div class="form-actions">
																	<button type="submit" class="submit btn btn-primary pull-right">
																		Save <i class="icon-angle-right"></i>
																	</button>
																</div>
															</div>
														</div>
													</form>
													<form class="form-horizontal row-border" action="/ds_campaignSendWarning" name="campaignSendWarning" id="campaignSendWarning" method="POST" target="ps">
														{getFormToken f="campaignSendWarning"}
													</form>
												{elseif $_item->state == 8}
													<form class="form-horizontal row-border" action="#">
														<div class="alert alert-info fade in">
															Campaign will start in {dformat t=$_item->activationDate f="Y-m-d H:i"}.
														</div>
													</form>
												{elseif $_item->state == 9}
													<form class="form-horizontal row-border" action="#">
														<div class="alert alert-info fade in">
															Campaign is active right now.
														</div>
													</form>
												{elseif $_item->state == 10}
													<form class="form-horizontal row-border" action="#">
														<div class="alert alert-info fade in">
															Campaign completed.
														</div>
													</form>
												{/if}
											</div>
										</div>
									</div>
							</div>
							<!-- /AddEdit -->

							<!--=== Overview ===-->
							<div class="tab-pane" id="tab_overview">
							</div>
							<!-- /Overview -->
						</div>
						<!-- /.tab-content -->
					</div>
					<!--END TABS-->
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

<script type="text/javascript">
	var currentState = '{$_item->state}'
		,toggleReason = null
		,togglePublishTime = null
		,budget={$_item->budget}
		,commission={$_item->commission};
	{literal}
	$(document).ready(function(){
		$(".camp_state").each(function(k,v){
			if (currentState == 3 && parseInt($(v).attr("state")) == 2){
				$(v).addClass("btn-danger");
			} else if (currentState == 5 && $(v).attr("state") == 5){
				$(v).addClass("btn-danger");
			} else if(parseInt($(v).attr("state"))<=currentState){
				$(v).addClass("btn-success");
			}
		});

		toggleReason = function(){
			if ($("input[name='approve']:radio:checked").length && $("input[name='approve']:radio:checked").val() == "Deny"){
				$("#denyReason").parent().parent().slideDown();
			} else {
				$("#denyReason").parent().parent().slideUp();
			}
		}

		togglePublishTime = function(){
			if ($("input[name='publishOption']:radio:checked").length && $("input[name='publishOption']:radio:checked").val() == "-1"){
				$("#publishDate").parent().parent().slideDown();
				$("#publishTime").parent().parent().slideDown();
				$("#publishDate").val("");
				$("#publishTime").val("");
			} else {
				$("#publishDate").parent().parent().slideUp();
				$("#publishTime").parent().parent().slideUp();
				$("#publishDate").val("-");
				$("#publishTime").val("-");
			}
		}

		if (currentState == 1){
			$("input[name='approve']").change(function(){
				toggleReason();
			});
			toggleReason();
		} else if (currentState == 2){
			$("#extraBudget").change(function(){
				$("#totalCost").val(budget + commission + parseFloat($("#extraBudget").val()));
			});
		} else if (currentState == 4){
			$("#declineOffer").click(function(){
				$("#offerReply").val("decline");
			});
		} else if (currentState == 6){
			$('.timepicker-fullscreen').pickatime({
				editable:true,
				interval: 10,
				format: 'H:i'
			});
			$("input[name='publishOption']").change(function(){
				togglePublishTime();
			});
			togglePublishTime();
		}
	});
	{/literal}
	{if isset($userPerms.campaign) && isset($perms.campaign.deleteCampaign) && $perms.campaign.deleteCampaign & $userPerms.campaign}

	{literal}
	var params = {
		"emailPass" : "",
		"acceptedPublisherPass" : ""
	};
	var deleteCampaign = function(){
		$.ajax({
			type: "POST",
			url: "/ds_campaignDelete/{/literal}{$_item->id}{literal}",
			data: params,
			dataType: "json",
			timeout: 100000, // in milliseconds
			beforeSend:function(){
				$.blockUI({
					message: '<h1>Processing</h1>',
					css: { border: '3px solid #a00' }
				});
			},
			success:function(data){
				$.unblockUI();
				if (data.success){
					bootbox.alert("Campaign deleted",function(){
						location.replace("/ds_campaigns");
					});
				} else {
					switch(data.error){
						case "emailWaiting":
							bootbox.confirm("Campaign available emails sent to publishers. Are you sure to continue?", function(confirmed) {
								if (confirmed) {
									params.emailPass = "ok";
									deleteCampaign();
								} else {
									params = {
										"emailPass" : "",
										"acceptedPublisherPass" : ""
									};
								}
							});
							break;
						case "acceptedPublisherPassExists":
							bootbox.confirm("There are accepted publisher for this campaign. Are you sure to continue?", function(confirmed) {
								if (confirmed) {
									params.acceptedPublisherPass = "ok";
									deleteCampaign();
								} else {
									params = {
										"emailPass" : "",
										"acceptedPublisherPass" : ""
									};
								}
							});
							break;
						default:
							bootbox.alert("An unknown error occured. Please try again");
							break;
					}
				}
			},
			error:function(){
				$.unblockUI();
				bootbox.alert("An error occured. Please try again");
			}
		});
	};
	$(".btn-delete-campaign").click(function(){
		bootbox.confirm("Are you sure? This action can't be undone.", function(confirmed) {
			if (confirmed){
				deleteCampaign();
			}
		});
	});

	{/literal}
	{/if}
</script>

</body>
</html>