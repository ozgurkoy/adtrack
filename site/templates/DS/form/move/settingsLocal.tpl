{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_localSettings,Local Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">

								{getFormToken f=$_job}

								{formBox type="arrayKVSelect" label="Country" name="country" options=$cArray value="$cCode"}
								{formBox type="arrayKVSelect" label="Default Language" name="language" options=$lang value=$ADM_SETTINGS.DEFAULT_LANGUAGE}
								{formBox type="arrayKVSelect" label="Currency" name="currency" options=$currency value=$ADM_SETTINGS.CURRENCY_CODE}

								{formBox type="text" label="Thousand separator" name="thousand" value=$ADM_SETTINGS.NUMBER_FORMAT_THOUSAND_SEPERATOR}
								{formBox type="text" label="Decimal separator" name="decimal" value=$ADM_SETTINGS.NUMBER_FORMAT_DECIMAL_SEPERATOR}
								{formBox type="arrayKVSelect" label="currency Symbol Location" name="currencySymbol" options=$currencyLoc info="Location of the currency Symbol" value="$ADM_SETTINGS.CURRENCY_SYMBOL_AFTER"}
								{formBox type="text" label="JS Date Format" name="jsDateFormat" value=$ADM_SETTINGS.jsDateFormat info="yy-mm-dd"}
								{formBox type="text" label="Date Format" name="dateFormat" value=$ADM_SETTINGS.dateFormat info="d/m/Y"}
								{formBox type="text" label="Time Format" name="timeFormat" value=$ADM_SETTINGS.timeFormat info=" (H:i) "}
								{formBox type="text" label="Phone Mask" name="phoneMask" value=$ADM_SETTINGS.PHONE_MASK info="Like (999)999-9999 "}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>