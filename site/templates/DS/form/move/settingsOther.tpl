{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
						      method="POST" target="ps">
							{getFormToken f=$_job}
							<div class="widget-content">

								{formBox type="text" label="ADVANCED_DEBUG_MODE" name="ADVANCED_DEBUG_MODE" value=$ADM_SETTINGS.ADVANCED_DEBUG_MODE}
								{formBox type="text" label="PUSH_KEY_PASSPHRASE" name="PUSH_KEY_PASSPHRASE" value=$ADM_SETTINGS.PUSH_KEY_PASSPHRASE}
								{formBox type="text" label="PUSH_GATEWAY_ADDRESS" name="PUSH_GATEWAY_ADDRESS" value=$ADM_SETTINGS.PUSH_GATEWAY_ADDRESS}
								{formBox type="text" label="TWEET_MAX_DAY" name="TWEET_MAX_DAY" value=$ADM_SETTINGS.TWEET_MAX_DAY}
								{formBox type="text" label="MAX_CAMP_ID_BEFORE_CPC" name="MAX_CAMP_ID_BEFORE_CPC" value=$ADM_SETTINGS.MAX_CAMP_ID_BEFORE_CPC}

								{formBox type="text" label="MAIL_DEBUG_MODE" name="MAIL_DEBUG_MODE" value=$ADM_SETTINGS.MAIL_DEBUG_MODE}
								{formBox type="text" label="campaignImageMaxWidth" name="campaignImageMaxWidth" value=$ADM_SETTINGS.campaignImageMaxWidth}
								{formBox type="text" label="campaignImageMaxHeight" name="campaignImageMaxHeight" value=$ADM_SETTINGS.campaignImageMaxHeight}
								{formBox type="text" label="campaignCharLimitWithLink" name="campaignCharLimitWithLink" value=$ADM_SETTINGS.campaignCharLimitWithLink}
								{formBox type="text" label="campaignCharLimitWithoutLink" name="campaignCharLimitWithoutLink" value=$ADM_SETTINGS.campaignCharLimitWithoutLink}

								{formBox type="text" label="campListItemCount" name="campListItemCount" value=$ADM_SETTINGS.campListItemCount}
								{formBox type="text" label="validImageTypes" name="validImageTypes" value=$ADM_SETTINGS.validImageTypes}
								{formBox type="text" label="maxImageFileSize" name="maxImageFileSize" value=$ADM_SETTINGS.maxImageFileSize}
								{formBox type="text" label="publisherProfileImageMaxWidth" name="publisherProfileImageMaxWidth" value=$ADM_SETTINGS.publisherProfileImageMaxWidth}
								{formBox type="text" label="publisherProfileImageMaxHeight" name="publisherProfileImageMaxHeight" value=$ADM_SETTINGS.publisherProfileImageMaxHeight}

								{formBox type="text" label="publisherBackgroundImageMaxHeight" name="publisherBackgroundImageMaxHeight" value=$ADM_SETTINGS.publisherBackgroundImageMaxHeight}
								{formBox type="text" label="publisherBackgroundImageMaxWidth" name="publisherBackgroundImageMaxWidth" value=$ADM_SETTINGS.publisherBackgroundImageMaxWidth}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>


							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>