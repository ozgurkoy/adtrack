{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>
	{literal}
	<style>
		.checkbox img{
			width: 32px;
			height: 32px;
		}
	</style>
	{/literal}
</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								Site Login Options
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action_login" id="{$_job}_login"
							      method="POST" target="ps">
								{getFormToken f="`$_job`login"}

								<div class="form-group">
									<label class="col-md-2 control-label">Select: </label>

									<div class="col-md-10">
										<label class="checkbox">
											<input type="checkbox" class="uniform" name="TWITTER_LOGIN"
											       value="twitter" {if $ADM_SETTINGS.TWITTER_LOGIN} checked {/if}>
											<img src="site/layout/images/social/twitter.png"/> Twitter
										</label>
										<label class="checkbox">
											<input type="checkbox" class="uniform" name="GOOGLE_LOGIN"
											       value="gplus" {if $ADM_SETTINGS.GOOGLE_LOGIN} checked {/if}>
											<img src="site/layout/images/social/gplus.png"/> gplus
										</label>
										<label class="checkbox">
											<input type="checkbox" class="uniform" name="FB_LOGIN"
											       value="facebook" {if $ADM_SETTINGS.FB_LOGIN} checked {/if}>
											<img src="site/layout/images/social/facebook.png"/> facebook
										</label>
										<label class="checkbox">
											<input type="checkbox" class="uniform" name="VK_LOGIN"
											       value="vk" {if $ADM_SETTINGS.VK_LOGIN} checked {/if}>
											<img src="site/layout/images/social/vk.png"/> vk
										</label>
										<label class="checkbox">
											<input type="checkbox" class="uniform" name="XING_LOGIN"
											       value="xing" {if $ADM_SETTINGS.XING_LOGIN} checked {/if}>
											<img src="site/layout/images/social/xing.png"/> xing
										</label>
										<label class="checkbox">
											<input type="checkbox" class="uniform" name="INSTAGRAM_LOGIN"
											       value="xing" {if isset($ADM_SETTINGS.INSTAGRAM_LOGIN) && $ADM_SETTINGS.INSTAGRAM_LOGIN} checked {/if}>
											<img src="site/layout/images/social/instagram.png"/> instagram
										</label>

									</div>
								</div>


								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
								</form>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								CPC Campaign Sharing Options
							</h4>

							<div class="widget-content">
								<form class="form-horizontal row-border" action="/ds_{$_job}Action_sharing"
								      name="{$_job}_tumblr" id="{$_job}_tumblr"
								      method="POST" target="ps">
									{getFormToken f="`$_job`sharing"}

									<div class="form-group">
										<label class="col-md-2 control-label">Select: </label>

										<div class="col-md-10">
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="twitter" {if in_array("twitter",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/twitter.png"/> Twitter
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="gplus" {if in_array("gplus",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/gplus.png"/> gplus
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="facebook" {if in_array("facebook",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/facebook.png"/> facebook
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="tumblr" {if in_array("tumblr",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/tumblr.png"/> tumblr
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="blogger" {if in_array("blogger",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/blogger.png"/> blogger
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="linkedin" {if in_array("linkedin",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/linkedin.png"/> linkedin
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="pinterest" {if in_array("pinterest",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/pinterest.png"/> pinterest
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="vk" {if in_array("vk",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/vk.png"/> vk
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="whatsapp" {if in_array("whatsapp",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/whatsapp.png"/> whatsapp
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="xing" {if in_array("xing",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/xing.png"/> xing
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="instagram" {if in_array("odnoklassniki",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/odnoklassniki.png"/> odnoklassniki
											</label>
											<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="wordpress" {if in_array("mailru",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/mailru.png"/> mail.ru
											</label>
											{*<label class="checkbox">
												<input type="checkbox" class="uniform" name="share[]"
												       value="youtube" {if in_array("youtube",$SHARE_OPTIONS)} checked {/if}>
												<img src="site/layout/images/social/youtube.png"/> youtube
											</label>*}

										</div>
									</div>
									<div class="form-actions">
										<button type="submit" class="submit btn btn-primary pull-right">
											Submit <i class="icon-angle-right"></i>
										</button>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->
	<!-- /Page Content -->
</div>
<!-- /.container -->

</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>