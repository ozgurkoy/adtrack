{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}

	<script type="text/javascript" src="/site/layout/DS/plugins/bootstrap-wysihtml5/wysihtml5.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.min.js"></script>

	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar-content">

			<!-- Search Input -->
			<form class="sidebar-search">
				<div class="input-box">
					<button type="submit" class="submit">
						<i class="icon-search"></i>
					</button>
				<span>
					<input type="text" name="quickWord" id="quickWord" placeholder="Search for word...">
				</span>
				</div>
			</form>


			<!--=== Navigation ===-->
			{include file="DS/_nav.tpl" menus=$menus}
			<!-- /Navigation -->

		</div>
		<div id="divider" class="resizeable"></div>
	</div>
<div id="content">
	<div class="container">
		<!-- Breadcrumbs line -->
		<div class="crumbs">
			<ul id="breadcrumbs" class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="/darkside">Home</a>
				</li>
				<li>
					<i class="icon-road"></i>
					<a href="/ds_policy">Policy</a>
				</li>
			</ul>

		</div>
		<!-- /Breadcrumbs line -->

		<!--=== Page Header ===-->
		<div class="page-header">
		</div>
		<!-- /Page Header -->

		<!--=== Page Content ===-->


		<div class="row">
			<div class="col-md-12">
				<div class="widget box">
					<div class="widget-header">
						<h4><i class="icon-reorder"></i>
							{if $policy->gotValue}
								Edit
							{else}
								Add New
							{/if}
							User
						</h4>
					</div>
					<div class="widget-content">
						<form class="form-horizontal row-border" action="/ds_policyAction" name="policy" id="policy" method="POST" target="ps">
							{if $policy->gotValue}
								<input type="hidden" name="id" value="{$policy->id}"/>
							{/if}
							{getFormToken f="policy"}
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Language</label>
								<div class="col-md-10">
									{if $policy->gotValue}
										{callUserFunc $policy->loadPolicyLanguage()}
										<label for="input17">{$policy->policyLanguage->langName}</label>
									{else}
										<label for="input17">When creating the policy all language would create</label>
									{/if}
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Title</label>
								<div class="col-md-10">
									<input type="text" id="policyName" name="policyName" class="form-control" value="{$policy->policyName}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Content</label>
								<div class="col-md-10">
									<textarea rows="25" id="wysiwyg" name="wysiwyg" class="form-control wysiwyg">{$policy->content}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 control-label" for="input17">Change Comments</label>
								<div class="col-md-10">
									<input type="text" id="comments" name="comments" class="form-control" value="">
								</div>
							</div>
							{if $policy->gotValue}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Is Active?</label>
									<div class="col-md-10">
										<input type="checkbox" id="isActive" name="isActive" class="form-control uniform" value="1" {if $policy->active==1}checked="checked"{/if}>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Notify Users?</label>
									<div class="col-md-10">
										<input type="checkbox" id="notifyUsers" name="notifyUsers" class="form-control uniform" value="1" checked="checked">
									</div>
								</div>
							{/if}
							<div class="form-actions">
								<button type="submit" class="submit btn btn-primary pull-right">
									Submit <i class="icon-angle-right"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
		<!-- /Page Content -->
	</div>
	<!-- /.container -->

</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>