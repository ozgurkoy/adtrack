{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_clubs,Clubs;ds_viewClub/`$_item->id`,`$_item->clubName`;,`$actionLabel`" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel}
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
								{getFormToken f=$_job}
								<input type="hidden" name="club_clubMoneyAction" value="{$_item->id}"/>
								{if $cma->gotValue}
									<input type="hidden" name="id" value="{$cma->id}"/>
								{/if}
								{formBox type="text" label="Amount" name="amount" info="Amount of the payment that made to club owner" class="only-numeric" value=$cma->amount}
								{formBox type="text" label="Payment Date" name="paymentDate" class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat value=$cma->paymentDate}
								{formBox type="textarea" label="Account Detail" info="Account details of the payment" name="accountDetail" value=$cma->accountDetail}
								{formBox type="textarea" label="Receipt Info" info="Receipt information of the payment" name="receiptInfo" value=$cma->receiptInfo}
								{formBox type="textarea" label="Notes" info="" name="note" value=$cma->note}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>