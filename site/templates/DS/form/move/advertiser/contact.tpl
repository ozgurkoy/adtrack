{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{callUserFunc $_item->loadContactPosition()}
			
			{breadCrumb left="ds_advertisers,Advertisers;ds_addAdvertiser/`$advertiser->id`,`$advertiser->companyName`;,Contacts;,Add New Contact" right="ds_addBank/`$advertiser->id`,New Contact"}

			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">

						<div class="widget-header">
							<h4>
								<i class="icon-reorder"></i>
								{$actionLabel} {$_job|ucfirst} to "{$advertiser->companyName}"
							</h4>
						</div>
                        <div class="widget-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tabbable tabbable-custom">
	                                    {include file="DS/form/advertiser/nav.tpl" current="contacts" item=$advertiser}

                                        <form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
	                                        <input type="hidden" id="advertiserId" name="advertiserId" value="{$advertiser->id}"/>

	                                        {if $_item->gotValue}
                                            <input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
		                                    {callUserFunc $_item->loadUserEmail()}
                                            {/if}
                                            {getFormToken f=$_job}
                                            {formBox type="arrayKVSelect" label="Title" name="title" options=$_item->title_options value=$_item->title}
                                            {formBox type="text" label="Name" name="name" value=$_item->name}
                                            {formBox type="populateSelect" label="Contact Position" name="contactPosition" value=$_item->contactPosition options="advContactPosition" vkey="positionName" }
                                            {formBox type="text" label="Contact Position(Other)" name="contactPositionOther" value=$_item->contactPositionOther}
	                                        {if $_item->gotValue}
                                            {formBox type="text" label="Contact E-mail" name="contactEmail" value=$_item->contactUserEmail->email}
	                                        {else}
                                            {formBox type="text" label="Contact E-mail" name="contactEmail" value=""}
	                                        {/if}
                                            {formBox type="text" label="Contact Phone" name="contactTel" value=$_item->contactTel}
                                            {formBox type="text" label="Contact GSM" name="contactGsm" value=$_item->contactGsm}
                                            {formBox type="text" label="Contact Fax" name="contactFax" value=$_item->contactFax}
                                            <div class="form-actions">
                                                <button type="submit" class="submit btn btn-primary pull-right">
                                                    Submit <i class="icon-angle-right"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>