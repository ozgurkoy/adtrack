{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_advertisers,Advertisers;,$actionLabel" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4>
								<i class="icon-reorder"></i>
								{if $_item->gotValue}Edit{else}New{/if} Advertiser
							</h4>
						</div>
						<div class="widget-content">
							<div class="row">
								<div class="col-md-12">
									<div class="tabbable tabbable-custom">
									{include file="DS/form/advertiser/nav.tpl" item=$_item current="edit"}
									<div class="tab-content">
										<div class="tab-pane active" id="tab_1_1">
											<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
												{if $_item->gotValue}
													<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
												{/if}
												{getFormToken f=$_job}
												{callUserFunc $_item->loadUser()}
												{callUserFunc $_item->loadCountry_advertiser()}

												{if $_item->user_advertiser}
													{assign var=username value=$_item->user_advertiser->username}
												{else}
													{assign var=username value=""}
												{/if}
												{formBox type="text" label="Username" name="username" value=$username}
												{formBox type="text" label="Password" name="password" value="" info="Type to change"}
												{formBox type="text" label="Company" name="companyName" value=$_item->companyName}
												{formBox type="text" label="Website" name="webSite" value=$_item->webSite}
												{formBox type="arrayKVSelect" label="Company Type" name="companyType" options=$_item->companyType_options value=$_item->companyType}
												{formBox type="arrayKVSelect" label="Company Size" name="companySize" options=$_item->companySize_options value=$_item->companySize}
												{formBox type="populateSelect" label="Country" name="country" options="country" value=$_item->country_advertiser vkey="name"}
												{formBox type="arraySelect" label="City" name="city" defaultValue="Choose a country first" options=$_emptyArray value=$_item->city_advertiser attached="country" src="/ds_xhr/object/city/country_city/cityName"}
												{formBox type="textarea" label="Address" name="address" value=$_item->address}
												{formBox type="text" label="CPC Cost" name="cpcCost" value=$_item->cpcCost class="only-numeric"}
												{formBox type="text" label="Commission Rate" name="commissionRate" value=$_item->commissionRate class="only-numeric"}
												{if $_item->gotValue!==true}
													<h3>Contact Details</h3>
													{formBox type="text" label="Name" name="name" value=""}
													{formBox type="populateSelect" label="Contact Position" name="contactPosition"  options="advContactPosition" vkey="positionName"}
													{formBox type="text" label="Contact Position(Other)" name="contactPositionOther" value=""}
													{formBox type="text" label="Contact E-mail" name="contactEmail" value=""}
													{formBox type="text" label="Contact Phone" name="contactTel" value=""}
													{formBox type="text" label="Contact GSM" name="contactGsm" value=""}
													{formBox type="text" label="Contact Fax" name="contactFax" value=""}
												{/if}
												{if $_item->gotValue}
												{formBox type="arrayKVSelect" label="Advertiser Status" name="status" options=$statusOptions value=$_item->user_advertiser->status}
												{else}
												{formBox type="arrayKVSelect" label="Advertiser Status" name="status" options=$statusOptions value=null}
												{/if}
												<div class="form-actions">
													<button type="submit" class="submit btn btn-primary pull-right">
														Submit <i class="icon-angle-right"></i>
													</button>
												</div>

											</form>

										</div>
										<div class="tab-pane" id="tab_1_2">
											<p>I'm in Section 2.</p>
										</div>
										<div class="tab-pane" id="tab_1_3">
											<p>I'm in Section 3.</p>
										</div>
									</div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
{literal}
	<script type="text/javascript">
		$(document).ready(function(){
			$( ".only-numeric" ).keydown(function( event ) {
				if ((!(event.which > 47 && event.which < 58) && event.which != 190 && event.which != 8 && event.which != 9) || (event.which == 190 && $(this).val().indexOf(".") != -1)) {
					event.preventDefault();
				}
			});
		});
	</script>
{/literal}
</body>
</html>