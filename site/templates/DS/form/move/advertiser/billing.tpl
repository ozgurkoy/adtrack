{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{callUserFunc $_item->loadCountry_advertiserBilling()}

			{breadCrumb left="ds_advertisers,Advertisers;ds_addAdvertiser/`$advertiser->id`,`$advertiser->companyName`;,Billings;,Add New Billing"}

			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">

						<div class="widget-header">
							<h4>
								<i class="icon-reorder"></i>
								{$actionLabel} {$_job|ucfirst} to "{$advertiser->companyName}"
							</h4>
						</div>
						<div class="widget-content">
							<div class="row">
								<div class="col-md-12">
									<div class="tabbable tabbable-custom">
										{include file="DS/form/advertiser/nav.tpl" current="billing" item=$advertiser}

										<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
											<input type="hidden" id="advertiserId" name="advertiserId" value="{$advertiser->id}"/>

											{if $_item->gotValue}
												<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
											{/if}
											{getFormToken f=$_job}
											{formBox type="text" label="Label" name="accountName" value=$_item->accountName}
											{formBox type="text" label="Billing Name" name="billingName" value=$_item->billingName}
											{formBox type="textarea" label="Address" name="address" value=$_item->address}
											{formBox type="populateSelect" label="Country" name="country_advertiserBilling" options="country" value=$_item->country_advertiserBilling vkey="name"}
											{formBox type="arraySelect" label="City" name="city_advertiserBilling" defaultValue="Choose a country first" options=$_emptyArray value=$_item->city_advertiserBilling attached="country_advertiserBilling" src="/ds_xhr/object/city/country_city/cityName"}
											{formBox type="text" label="Tax Office" name="taxOffice" value=$_item->taxOffice}
											{formBox type="text" label="Tax No" name="taxNo" value=$_item->taxNo}
											{formBox type="checkbox" label="Is Deleted" name="deleted" value=$_item->deleted}
											<div class="form-actions">
												<button type="submit" class="submit btn btn-primary pull-right">
													Submit <i class="icon-angle-right"></i>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>