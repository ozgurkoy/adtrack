{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_advertisers,Advertisers;ds_addAdvertiser/`$advertiser->id`,`$advertiser->companyName`;,Brands;,Add New Brand"}
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">

						<div class="widget-header">
							<h4>
								<i class="icon-reorder"></i>
								{$actionLabel} {$_job|ucfirst} to "{$advertiser->companyName}"
							</h4>
						</div>
						<div class="widget-content">
							<div class="row">
								<div class="col-md-12">
									<div class="tabbable tabbable-custom">
										{include file="DS/form/advertiser/nav.tpl" current="brand" item=$advertiser}
										{callUserFunc $_item->loadIndustry()}
										{callUserFunc $_item->loadAdvContacts()}
										{callUserFunc $advertiser->loadAdvertiser_advContact()}
										{*{$advertiser->advertiser_advContact|@print_r}*}

										<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
											<input type="hidden" id="advertiserId" name="advertiserId" value="{$advertiser->id}"/>

											{if $_item->gotValue}
												<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
											{/if}
											{getFormToken f=$_job}
											{formBox type="text" label="Name" name="name" value=$_item->name}
											{formBox type="radio" label="Status" name="status" value=$_item->status options=$_item->status_options}
											{formBox type="radio" label="Representation" name="representationType" value=$_item->representationType options=$_item->representationType_options}
											{formBox type="textarea" label="Info" name="info" value=$_item->info}
											{formBox type="populateSelect" label="Industries" name="industry" value=$_item->industry options="advertiserIndustry" vkey="name"}
											{formBox type="transferSelect" label="Contacts" name="advContacts" value=$_item->advContacts options=$advertiser->advertiser_advContact vkey="name"}

											<div class="form-actions">
												<button type="submit" class="submit btn btn-primary pull-right">
													Submit <i class="icon-angle-right"></i>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>