{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_advertisers,Advertisers;ds_addAdvertiser/`$advertiser->id`,`$advertiser->companyName`;,Money Actions;,Add New Money Action"}

			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">

						<div class="widget-header">
							<h4>
								<i class="icon-reorder"></i>
								{$actionLabel} {$_job|ucfirst} to "{$advertiser->companyName}"
							</h4>
						</div>
						<div class="widget-content">
							<div class="row">
								<div class="col-md-12">
									<div class="tabbable tabbable-custom">
										{include file="DS/form/advertiser/nav.tpl" current="money" item=$advertiser}

										<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
											<input type="hidden" id="advertiserId" name="advertiserId" value="{$advertiser->id}"/>
											{if $_item->gotValue}
											<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
											{/if}
											{getFormToken f=$_job}
											{runThroughFunction function="timestampToDate" var="paymentDate" l="Y-m-d" d=$_item->paymentDate}
											{formBox type="text" label="Amount" name="amount" value=$_item->amount info="Use - if you want to make it negative"}
											{formBox type="arrayKVSelect" label="State" name="state" options=$_item->state_options value=$_item->state}
											{formBox type="arrayKVSelect" label="Payment Type" name="paymentType" options=$_item->paymentType_options value=$_item->paymentType}
											{formBox type="text" label="Payment date" name="paymentDate" value=$paymentDate class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.jsDateFormat}
											{formBox type="textarea" label="Info" name="info" value=$_item->info}
											<div class="form-actions">
												<button type="submit" class="submit btn btn-primary pull-right">
													Submit <i class="icon-angle-right"></i>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>