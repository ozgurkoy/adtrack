<ul class="nav nav-tabs">
	<li {if $current=="edit"}class="active"{/if}><a href="/ds_addAdvertiser/{$item->id}" >{if $item->gotValue}Edit{else}New{/if} Advertiser</a></li>
	{if $item->gotValue}
		<li {if $current=="contacts"}class="active"{/if}><a href="/ds_advContacts/{$item->id}" >Contacts</a></li>
		<li {if $current=="billing"}class="active"{/if}><a href="/ds_advertiserBillings/{$item->id}">Billing Information</a></li>
		<li {if $current=="brand"}class="active"{/if}><a href="/ds_advertiserBrandListing/{$item->id}">Brands</a></li>
		<li {if $current=="money"}class="active"{/if}><a href="/ds_advertiserMoneyActions/{$item->id}">Money Actions</a></li>
	{/if}
</ul>
