{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_cplLayouts,CPL Layouts;,$actionLabel" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel} CPL Layout
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps"  enctype="multipart/form-data">
								{if $_item->gotValue}
									<input type="hidden" name="id" value="{$_item->id}"/>
								{/if}

								{getFormToken f=$_job}

								{formBox type="text" label="Name" name="name" value=$_item->name}
								{formBox type="arrayKVSelect" label="Error Position" name="errorPosition" options=$positionValues value=$_item->errorPosition}
								{formBox type="checkbox" label="Include Bootstrap" name="includeBootstrap" info="Check this if the template does not have bootstrap attached" value=$_item->includeBootstrap}
								{formBox type="file" label="File" name="file" value=""}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
					<div id="layoutPreview" {if $_item->gotValue}class="non"{/if}></div>
					{if $_item->gotValue}
					<h3>Template Preview:</h3>
					<iframe style="width:100%;border:0;height:500px;float:left;clear:both;display:none;" id="layoutff">

					</iframe>
					{/if}
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
<script>
	{if $_item->gotValue}
	var id = {$_item->id};
	var code = '{$_item->code}';
	var data = {$_item->options};
	{literal}
	$.post('/ds_cplLayoutPreview',{options:data,id:id},function(data){
		uploadResponse( data );
		if(typeof code != 'undefined'){
			$( '#layoutff' ).attr('src', '/ds_cplLayoutOverlay/' + code ).show();

		}

	});
	{/literal}

	{/if}
	{literal}
	function uploadResponse(data){
		$( '#layoutPreview' ).show().html( data );
	}

	{/literal}

</script>
</body>
</html>