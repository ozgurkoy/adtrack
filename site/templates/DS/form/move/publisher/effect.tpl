{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>
	<script type="text/javascript" src="/site/layout/DS/plugins/sparkline/jquery.sparkline.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.tooltip.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.resize.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.time.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.orderBars.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.pie.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.selection.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.growraf.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.categories.min.js"></script>
	<script type="text/javascript" src="/site/layout/DS/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_publishers,Publishers;,$actionLabel" }
			<!-- /Breadcrumbs line -->
			<div class="page-header">
				<div class="page-title">
					<h3>Publisher Profile / {$_item->name} {$_item->surname}</h3>
					{*<span>Good morning, John!</span>*}
				</div>
			</div>

			{if $_item->publisher_snTwitter}
				<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom tabbable-full-width">


						{include file="DS/form/publisher/nav.tpl" item=$_item current="effect"}

						<div class="tab-content row">
							<!--=== Overview ===-->
							<div class="col-md-12">
								<div class="tabbable tabbable-custom">
									<div class="tab-content row">
										<div class="tab-pane active" id="tab_1_1">
											<form class="form-horizontal row-border"
											      action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
											      method="POST" target="ps">
												{if $_item->gotValue}
													<input type="hidden" id="objectId" name="id"
													       value="{$_item->id}"/>
												{/if}
												{getFormToken f=$_job}
												{formBox type="text" label="Custom Cost" name="customCost" value=$_item->publisher_snTwitter->customCost}
												{formBox type="text" label="Min Cost" name="minCost" value=$_item->publisher_snTwitter->minCost}
												{formBox type="text" label="Max Cost" name="maxCost" value=$_item->publisher_snTwitter->maxCost}
												{formBox type="text" label="Number of tweets per campaign" name="numberOfTweetsPerCampaign" value=$_item->publisher_snTwitter->numberOfTweetsPerCampaign}
												{formBox type="checkbox" label="Can set own message" name="canSetOwnMessage" value=$_item->csm}
												{formBox type="text" label="Calculated Cost" name="originalCost" value=$_item->publisher_snTwitter->originalCost  customAttribute="disabled" attributeValue="disabled"}
												{formBox type="text" label="True Reach" name="trueReach" value=$_item->publisher_snTwitter->trueReach customAttribute="disabled" attributeValue="disabled"}
												<div class="form-actions">
													<button type="submit"
													        class="submit btn btn-primary pull-right">
														Submit <i class="icon-angle-right"></i>
													</button>
												</div>

											</form>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Cost Change</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content">
							<div id="costGraph" class="chart"></div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i> Reach Change</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
									<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
								</div>
							</div>
						</div>
						<div class="widget-content">
							<div id="reachGraph" class="chart"></div>
						</div>
					</div>
				</div>
			</div>

			{else}
			Publisher's twitter is not connected.
			{/if}
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>
{include file="DS/footer.tpl"}
<script type="text/javascript">
	var cost = [], reach = [];
	{foreach from=$data.costs key=date item=val}
	cost.push(['{$date}','{$val}']); reach.push(['{$date}','{$data.reaches.$date}']);
	{/foreach}

	{literal}
	function monthGraph(id, label, data, color){
		var data1 = [
			{ label: label, data: data, color: App.getLayoutColorCode(color) }
		];

		$.plot('#'+id, data1, $.extend(true, {}, Plugins.getFlotDefaults(), {
			xaxis: {
				mode: "categories",
				tickSize: [1, "day"],
				tickLength: 1
			},
			series: {
				lines: {
					fill: true,
					lineWidth: 1.5
				},
				points: {
					show: true,
					radius: 2.5,
					lineWidth: 1.1
				}
			},
			grid: {
				hoverable: true,
				clickable: true
			},
			tooltip: true,
			tooltipOpts: {
				content: '%s : %y'
			}
		}));
	}
	{/literal}
	monthGraph('costGraph', 'Cost', cost, 'green')
	monthGraph('reachGraph', 'True Reach', reach, 'red')


</script>
</body>
</html>