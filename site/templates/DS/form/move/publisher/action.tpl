{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_publishers,Publishers;,$actionLabel" }
			<!-- /Breadcrumbs line -->
			<div class="page-header">
				<div class="page-title">
					<h3>Publisher Profile / {$_item->name} {$_item->surname}</h3>
					{*<span>Good morning, John!</span>*}
				</div>
			</div>

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom tabbable-full-width">


						{include file="DS/form/publisher/nav.tpl" item=$_item current="action"}
						{callUserFunc $_item->loadUser()}
						{assign var=stat value=$_item->user_publisher->status}
						<div class="tab-content row">
							<!--=== Overview ===-->
							<div class="col-md-12">
								<div class="tabbable tabbable-custom">
									{if $stat != 1}
									<div class="tab-content row">
										<div class="col-md-12">
											<h5 class="widget-title"><i class="icon-unchecked"></i> Active User</h5>
										</div>
										<div class="tab-pane active" id="tab_1_1">
											<form class="form-horizontal row-border" action="/ds_publisherActive" name="publisherBlockAction" id="publisherBlockAction" method="POST" target="ps">
												<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
												{getFormToken f="publisherActive"}
												{formBox type="textarea" label="Active Reason" name="reason" value=""}
												<div class="">
													<button type="submit"
													        class="submit btn btn-primary pull-right">
														Submit <i class="icon-angle-right"></i>
													</button>
												</div>

											</form>

										</div>
									</div>
									{else}
									<div class="tab-content row">
										<div class="col-md-12">
											<h5 class="widget-title"><i class="icon-unchecked"></i> Block User</h5>
										</div>
										<div class="tab-pane active" id="tab_1_1">
											<form class="form-horizontal row-border" action="/ds_publisherBlock" name="publisherBlockAction" id="publisherBlockAction" method="POST" target="ps">
												<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
												{getFormToken f="publisherBlock"}
												{formBox type="textarea" label="Block Reason" name="reason" value=""}
												<div class="">
													<button type="submit"
													        class="submit btn btn-primary pull-right">
														Submit <i class="icon-angle-right"></i>
													</button>
												</div>

											</form>

										</div>
									</div>
									{/if}
									<div class="tab-content row">
										<div class="col-md-12">
											<h5 class="widget-title"><i class="icon-unchecked"></i> Reset and send new password</h5>
										</div>
										<div class="tab-pane active" id="tab_1_1">
											<form class="form-horizontal row-border" action="/ds_publisherResetPassword" name="publisherBlockAction" id="publisherBlockAction" method="POST" target="ps">
												<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
												{getFormToken f="publisherResetPassword"}
												{formBox type="text" label="New Password" name="password" value="" info="Leave empty for random password"}
												<div class="">
													<button type="submit"
													        class="submit btn btn-primary pull-right">
														Submit <i class="icon-angle-right"></i>
													</button>
												</div>

											</form>

										</div>
									</div>

									<div class="tab-content row">
										<div class="col-md-12">
											<h5 class="widget-title"><i class="icon-unchecked"></i> Send message to user</h5>
										</div>
										<div class="tab-pane active" id="tab_1_1">
											<form class="form-horizontal row-border" action="/ds_publisherSendMessage" name="publisherSendMessage" id="publisherSendMessage" method="POST" target="ps">
												<input type="hidden" id="objectId" name="id" value="{$_item->id}"/>
												{getFormToken f="publisherSendMessage"}
												{formBox type="text" label="Title" name="title" value="" info="" customAttribute="maxlength" attributeValue="190"}
												{formBox type="textarea" label="Message" name="message" value="" info="Mobile messages can't use this field. So if you check mobile option, please give your message on title"}
												{formBox type="checkboxGroup" label="Target" name="target" value="" options=$options}
												<div class="">
													<button type="submit"
													        class="submit btn btn-primary pull-right">
														Submit <i class="icon-angle-right"></i>
													</button>
												</div>

											</form>

										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
<script type="text/javascript">
	{literal}
	var messageSent = function(){
		window.message("good","Message sent");
		$("#publisherSendMessage").trigger("reset")
	};
	{/literal}
</script>
</body>
</html>