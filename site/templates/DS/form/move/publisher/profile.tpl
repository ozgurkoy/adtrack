{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
        <div class="container">
            <!-- Breadcrumbs line -->
            {breadCrumb left="ds_publishers,Publishers;,$actionLabel" }
            <!-- /Breadcrumbs line -->
	        <div class="page-header">
		        <div class="page-title">
			        <h3>Publisher Profile / {$_item->name} {$_item->surname}</h3>
			        {*<span>Good morning, John!</span>*}
		        </div>
	        </div>

            <!--=== Page Content ===-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable tabbable-custom tabbable-full-width">


                        {include file="DS/form/publisher/nav.tpl" item=$_item current="profileEdit"}

                        <div class="tab-content row">
                            <!--=== Overview ===-->
                            <div class="col-md-12">
                                <div class="tabbable tabbable-custom">
                                    <div class="tab-content row">
                                        <div class="tab-pane active" id="tab_1_1">
                                            <form class="form-horizontal row-border"
                                                  action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
                                                  method="POST" target="ps">
                                                {if $_item->gotValue}
                                                <input type="hidden" id="objectId" name="id"
                                                       value="{$_item->id}"/>
                                                {/if}
                                                {getFormToken f=$_job}
                                                {callUserFunc $_item->loadUser()}
                                                {callUserFunc $_item->loadCountry()}
	                                            {runThroughFunction function="timestampToDate" var="bday" l="Y-m-d" d=$_item->bday}
                                                {if $_item->user_publisher}
                                                {assign var=username value=$_item->user_publisher->username}
                                                {else}
                                                {assign var=username value=""}
                                                {/if}
	                                            {formBox type="text" label="First Name" name="name" value=$_item->name}
                                                {formBox type="text" label="Last Name" name="surname" value=$_item->surname}
                                                {formBox type="arrayKVSelect" label="User Type" name="type" options=$_item->type_options value=$_item->type}
                                                {formBox type="text" label="Date of Birth" name="bday" value=$bday class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat}
	                                            {formBox type="radio" label="Has Parent Approval" name="hasParentApproval" options=$yesNo value=$_item->hasParentApproval class="radio-inline"}
	                                            <div class="form-group parentApprovalMail" style="display: none">
		                                            <label class="col-md-2 control-label" for="input17">Send Notification e-mail</label>
		                                            <div class="col-md-10">
			                                            <input type="checkbox" class="uniform form-control" name="parentApprovalMail" value="0" checked>
		                                            </div>
	                                            </div>
	                                            {formBox type="radio" label="Identity Card Owned" name="hasIdentity" options=$yesNo value=$_item->hasIdentity class="radio-inline"}
	                                            {formBox type="text" label="Identity Number" name="identityNo" value=$_item->identityNo }
	                                            {formBox type="text" label="GSM Number" name="gsmNumber" value=$_item->gsmNumber }
	                                            {formBox type="populateSelect" label="Country" name="country" options="country" value=$_item->country vkey="name"}
                                                {formBox type="arraySelect" label="City" name="city" defaultValue="Choose a country first" options=$_emptyArray value=$_item->city attached="country" src="/ds_xhr/object/city/country_city/cityName"}
                                                {formBox type="transferSelect" label="Interests" name="trends" options="trend" vkey="title" value=$_item->trends}
                                                <div class="form-actions">
                                                    <button type="submit"
                                                            class="submit btn btn-primary pull-right">
                                                        Submit <i class="icon-angle-right"></i>
                                                    </button>
                                                </div>

                                            </form>

                                        </div>
                                        <div class="tab-pane" id="tab_1_2">
                                            <p>I'm in Section 2.</p>
                                        </div>
                                        <div class="tab-pane" id="tab_1_3">
                                            <p>I'm in Section 3.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- /Page Content -->
        </div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

<script type="text/javascript">
	{literal}

	$(document).ready(function(){
		$("input[name='hasParentApproval']").change(function(x){
			if($(this).val()==1){
				$("input[name='parentApprovalMail']").val(1);
				$("input[name='parentApprovalMail']").attr('checked','checked');
				$("input[name='parentApprovalMail']").parent().addClass('checked');
				$(".parentApprovalMail").toggle(600);
			}else{
				if($(".parentApprovalMail").is(":visible")){
					$("input[name='parentApprovalMail']").parent().removeClass('checked');
					$(".parentApprovalMail").toggle(600);
				}else{
					$("input[name='parentApprovalMail']").val(0);
				}
				$("input[name='parentApprovalMail']").attr('checked',0);
			}
		});
	});
	{/literal}
</script>

</body>
</html>