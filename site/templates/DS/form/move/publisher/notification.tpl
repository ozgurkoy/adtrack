{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_publishers,Publishers;,$actionLabel" }
			<!-- /Breadcrumbs line -->
			<div class="page-header">
				<div class="page-title">
					<h3>Publisher Profile / {$_item->name} {$_item->surname}</h3>
					{*<span>Good morning, John!</span>*}
				</div>
			</div>

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom tabbable-full-width">


						{include file="DS/form/publisher/nav.tpl" item=$_item current="notification"}

						<div class="tab-content row">
							<!--=== Overview ===-->
							<div class="col-md-12">
								<div class="tabbable tabbable-custom">
									<div class="tab-content row">
										<div class="tab-pane active" id="tab_1_1">
											<form class="form-horizontal row-border"
											      action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
											      method="POST" target="ps">
												{if $_item->gotValue}
													<input type="hidden" id="objectId" name="id"
													       value="{$_item->id}"/>
												{/if}
												{getFormToken f=$_job}

												{formBox type="text" label="GSM" name="gsmNumber" value=$_item->gsmNumber}
												{*<div class="form-group errorContext">
													<label class="col-md-2 control-label" for="notifications[sms]" id="lbl_notifications[sms]">
														SMS Notifications
													</label>

													<div class="col-md-10">
														{if $nots->gotValue}
														{*{$notifications|@print_r}
														{doWhile}
														{assign var=notType value=$nots->type}
														{assign var=notId value=$nots->id}
														{assign var=notTypeLabel value=$nots->type_options.$notType}
														{if $nots->type==1}

															<label class="checkbox" for="notifications[{$nots->id}]">
																<div class="checker"><span><input type="checkbox" name="notifications[{$nots->id}]" id="notifications[{$nots->id}]" value="1" class="uniform" {if isset($notifications[$notTypeLabel][$notId])}checked{/if}></span></div> {$nots->title}
															</label>
														{/if}
														{/doWhile ($nots->populate())}
														{else}
															No publisher notifications defined
														{/if}
													</div>
												</div>
												*}
												{callUserFunc $_item->loadUser()->loadUser_userEmail()}
												{if $_item->user_publisher->user_userEmail}
												{doWhile}
													{callUserFunc $nots->resetIndex()}
													{formBox type="text" label="E-mail" name="email[`$_item->user_publisher->user_userEmail->id`]" value=$_item->user_publisher->user_userEmail->email}
												{/doWhile ($_item->user_publisher->user_userEmail->populate())}

													<div class="form-group errorContext">
														<label class="col-md-2 control-label" for="notifications[{$_item->user_publisher->user_userEmail->id}]" id="lbl_notifications[{$_item->user_publisher->user_userEmail->id}]">
															E-mail Notifications
														</label>

														<div class="col-md-10">
															{if $nots->gotValue}

															{*{$notifications|@print_r}*}
															{doWhile}
															{assign var=notType value=$nots->type}
															{assign var=notId value=$nots->id}
															{assign var=notTypeLabel value=$nots->type_options.$notType}
															{if $nots->type==2}
																<label class="checkbox" for="notifications[{$nots->id}]">
																	<div class="checker">
																		<span><input type="checkbox" name="notifications[{$nots->id}]" id="notifications[{$nots->id}]" value="1" class="uniform" {if isset($notifications[$notTypeLabel][$notId])}checked{/if}></span>
																	</div> {$nots->title}
																</label>
															{/if}
															{/doWhile ($nots->populate())}
															{else}
																No publisher notifications defined
															{/if}
														</div>
													</div>


												{/if}
												<div class="form-actions">
													<button type="submit"
													        class="submit btn btn-primary pull-right">
														Submit <i class="icon-angle-right"></i>
													</button>
												</div>

											</form>

										</div>
										<div class="tab-pane" id="tab_1_2">
											<p>I'm in Section 2.</p>
										</div>
										<div class="tab-pane" id="tab_1_3">
											<p>I'm in Section 3.</p>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>