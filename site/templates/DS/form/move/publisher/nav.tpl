<ul class="nav nav-tabs">
	<li {if $current=="profile"}class="active"{/if}><a href="/ds_publisherProfile/{$_item->id}" >Overview</a></li>
	<li {if $current=="profileEdit"}class="active"{/if}><a href="/ds_publisherProfileEdit/{$_item->id}">Edit Account</a></li>
	<li {if $current=="notification"}class="active"{/if}><a href="/ds_publisherNotifications/{$_item->id}">Notifications</a></li>
	<li {if $current=="money"}class="active"{/if}><a href="/ds_publisherMoneyActions/{$_item->id}">Money Accounts</a></li>
	<li {if $current=="action"}class="active"{/if}><a href="/ds_publisherActions/{$_item->id}">Actions</a></li>
	<li {if $current=="effect"}class="active"{/if}><a href="/ds_publisherEffect/{$_item->id}">Effect</a></li>
</ul>