{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_publisherMoneyAction,Publisher Money Actions;ds_publisherMoneyActionDetails/`$_item->id`,Action Details" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
							<div class="toolbar no-padding">
								<div class="btn-group">
										<span class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
											Export <i class="icon-angle-down"></i>
										</span>
									<ul class="dropdown-menu pull-right">
										<li><a href="#" class="btn-export" tag="EXCEL"><i class="icol-doc-excel-csv"></i> Excel</a></li>
										<!--<li><a href="#" class="btn-export" tag="PDF"><i class="icol-doc-pdf"></i> PDF</a></li>
										<li><a href="#" class="btn-export" tag="HTML"><i class="icol-html"></i> Print HTML</a></li>-->
									</ul>
								</div>
							</div>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">

								{if $_item->gotValue}
									<input type="hidden" name="id" id="id" value="{$_item->id}"/>
								{/if}
								{if $editable}
									{getFormToken f=$_job}
								{/if}

								<div class="form-group">
									<label class="col-md-2 control-label">Dates:</label>
									<div class="col-md-10">
										<div class="row">
											<div class="col-md-2">
												<input type="text" name="regular" readonly value="{dformat t=$_item->requestDate f="Y-m-d"}"  class="form-control"><span class="help-block">Requested Date</span>
											</div>
											{*
											<div class="col-md-4">
												<input type="text" name="regular" readonly value="{dformat t=$_item->expectedDate}" class="form-control"><span class="help-block ">Expected Date</span>
											</div>
											*}
											<div class="col-md-2">
												<input type="text" name="regular" readonly value="{dformat t=$_item->lastUpdate f="Y-m-d"}" class="form-control"><span class="help-block ">Last Update Date</span>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Payment Type</label>
									<div class="col-md-4">
										<input type="text" readonly value="{$_item->withdrawOption->name}"  class="form-control">
									</div>
								</div>
								{formBox type="arrayKVSelect" label="adMingle Account" name="adMingleBank" options=$_adMingleAccount value=$_item->adMingleBank}
								<div class="form-group">
									<label class="col-md-2 control-label">Publisher Account Details</label>
									<div class="col-md-4">
										{$_accountTxt}
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Publisher Username</label>
									<div class="col-md-4" style="padding-top:7px;">
										<a href="/ds_publisherProfile/{$_item->publisher_publisherMoneyAction->id}">{$_item->publisher_publisherMoneyAction->user_publisher->username}</a>
									</div>
								</div>
								{formBox type="arrayKVSelect" label="Status" name="status" options=$_item->status_options value=$_item->status}

								{if $_item->status == 1 || $_item->status == 4 || $_item->status == 6}
									{formBox type="text" label="Payment Date" name="paymentDate" class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat}
									{formBox type="text" label="Tax Rate" name="taxRate" info="ex. 10 / 15 , number don't need %  " class="only-numeric" value=$_item->taxRate}
								{else}
									<div class="form-group">
										<label class="col-md-2 control-label" for="input17">Payment Date</label>
										<div class="col-md-4">
											<input type="text" readonly value="{dformat t=$_item->paymentDate f="Y-m-d"}"  class="form-control">
										</div>
									</div>
									{formBox type="text" label="Tax Rate" name="taxRate" info="ex. 10 / 15 , number don't need %  " class="only-numeric" customAttribute="readonly" attributeValue="readonly" value=$_item->taxRate}

								{/if}
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Amount</label>
									<div class="col-md-4">
										<input type="text" readonly value="{cformat n=$_item->amount*-1 d=3}"  class="form-control">
									</div>
								</div>

								{formBox type="textarea" label="Receipt Info" name="receipt" value=$_item->receiptInfo }
								{formBox type="textarea" label="Extra Info" name="note" value=$_item->note }

								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Related Campaigns</label>
									<div class="col-md-10">
										{if count($campaigns)}
											<ul>
												{foreach from=$campaigns item=c}
													<li><a href="/ds_campaignOverview/{$c.cid}">{dformat t=$c.approveDate f="d/m/Y H:i"} - {$c.title} - {cformat n=$c.revenue}</a></li>
												{/foreach}
											</ul>
										{/if}
									</div>
								</div>

								<div class="form-actions">
									{if $_item->status == 2}
										<button type="button" class="submit btn btn-success green btn-sendApproved">{if $_item->emailed}Re-{/if}Send Approved Mail<i class="icon-envelope-alt"></i></button>
									{/if}
									{if $editable}
									<button type="submit" class="submit btn btn-primary pull-right">Update <i class="icon-angle-right"></i></button>
									{/if}
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

<script type="text/javascript" charset="utf-8">

	{literal}
	$(document).ready(function(){

		$(".btn-export").click(function(e){
			var t = $(this);
			var detail = t.attr("tag");

			e.preventDefault();
			if (detail == "PDF"){
				bootbox.alert("Not Implemented PDF Yet, Stay tuned!");
			} else if(detail == "EXCEL"){
				bootbox.alert("Not Implemented Excel Yet, Stay tuned!");
			} else if(detail == "HTML"){
				bootbox.alert("Not Implemented HTML Yet, Stay tuned!");
			}
			return false;
		});

		$(".btn-sendApproved").click(function(){
			var btn = $(this);
			$.ajax({
				type: "POST",
				url: "/ds_publisherMoneyActionSendApprovedMail",
				data: {id:$("#id").val()},
				dataType: "html",
				timeout: 10000, // in milliseconds
				beforeSend:function(){
					$('div.form-actions').block({
						message: '<h1>Processing</h1>',
						css: { border: '3px solid #a00' }
					});
				},
				success:function(data){
					if (data == 1){
						bootbox.alert("Mail sent");
						btn.fadeOut();
					} else {
						bootbox.alert("Unhandled error occured. Please inform adMingle IT");
					}
					$('div.form-actions').unblock();
				},
				error:function(){
					$('div.demographical-filter').unblock();
					alert("An error occured. Please try again");
				}
			});
		});

	});
	{/literal}
</script>


</body>
</html>