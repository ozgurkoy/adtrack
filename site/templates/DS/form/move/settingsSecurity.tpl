{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_securitySettingsAction" name="securitySettings id="securitySettings" method="POST" target="ps">
							{getFormToken f="securitySettings"}
							<div class="widget-content">


								{formBox type="text" label="REFERRER_FRAUD_MIN_LIMIT" name="REFERRER_FRAUD_MIN_LIMIT" value=$ADM_SETTINGS.REFERRER_FRAUD_MIN_LIMIT}
								{formBox type="text" label="Number Of login fails" name="REFERRE" value=""}
								{formBox type="text" label="admin login fails" name="REFERRE" value=""}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>
							</div>

					</div>


					</form>
				</div>
			</div>


			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>

							<div class="widget-content">
								<table class="table table-hover table-striped table-bordered table-highlight-head">
									<thead>
									<tr>
										<th>IP</th>
										<th>Description</th>
										<th class="col-md-2">Action</th>
									</tr>
									</thead>
									<tbody>

											<tr>
												<td>
													127.12.4.76
												</td>
												<td>
													Test test
												</td>
												<td>

													<button class="btn btn-xs" href=""><i class="icon-remove"></i></button>
												</td>
											</tr>
											<tr>
												<td>
													127.12.4.76
												</td>
												<td>
													Test test
												</td>
												<td>
													<button class="btn btn-xs" href=""><i class="icon-remove"></i></button>
												</td>
											</tr>


									</tbody>
								</table>


								<form class="form-horizontal row-border" action="/ds_securitySettingsIPAction" name="securityIPSettings" id="securityIPSettings" method="POST" target="ps">
									{getFormToken f="securityIPSettings"}
									<div class="form-group">
										<label class="col-md-2 control-label">Add IP:</label>
										<div class="col-md-10">
											<div class="row">
												<div class="col-md-4">
													<input type="text" name="ip" class="form-control"><span class="help-block">IP address</span>
												</div>
												<div class="col-md-4">
													<input type="text" name="description" class="form-control"><span class="help-block align-center">Description</span>
												</div>
											</div>
										</div>
									</div>

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Add <i class="icon-angle-right"></i>
									</button>
								</div>
								</form>
							</div>
					</div>
				</div>
			</div>

			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>