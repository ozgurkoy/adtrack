{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>
	{literal}
		<style>
			.checkbox img {
				width: 32px;
				height: 32px;
			}
		</style>
	{/literal}
</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action_facebook"
							      name="{$_job}_facebook" id="{$_job}_facebook"
							      method="POST" target="ps">
								{getFormToken f="`$_job`facebook"}
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											<img width="48px" height="48px"
											     src="site/layout/images/social/facebook.png"/>
											Facebook
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="Key" name="FB_APP_ID" value=$ADM_SETTINGS.FB_APP_ID}
										{formBox type="text" label="Secret" name="FB_APP_SECRET" value=$ADM_SETTINGS.FB_APP_SECRET}


										<div class="form-actions">
											<button type="submit" class="submit btn btn-primary pull-right">
												Submit <i class="icon-angle-right"></i>
											</button>
										</div>

									</div>
								</div>
							</form>

							<form class="form-horizontal row-border" action="/ds_{$_job}Action_twitter"
							      name="{$_job}_twitter" id="{$_job}_twitter"
							      method="POST" target="ps">

								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											<img width="48px" height="48px" src="site/layout/images/social/twitter.png"/>
											Twitter
										</h4>
									</div>
									<div class="widget-content">
										{getFormToken f="`$_job`twitter"}
										{formBox type="text" label="Key" name="CONSUMER_KEY" value=$ADM_SETTINGS.CONSUMER_KEY}
										{formBox type="text" label="Secret" name="CONSUMER_SECRET" value=$ADM_SETTINGS.CONSUMER_SECRET}
										{formBox type="text" label="Callback URL" name="OAUTH_CALLBACK" value=$ADM_SETTINGS.OAUTH_CALLBACK}



										<div class="form-actions">
											<button type="submit" class="submit btn btn-primary pull-right">
												Submit <i class="icon-angle-right"></i>
											</button>
										</div>
									</div>
								</div>
							</form>

							<form class="form-horizontal row-border" action="/ds_{$_job}Action_gplus"
							      name="{$_job}_gplus" id="{$_job}_gplus"
							      method="POST" target="ps">
								{getFormToken f="`$_job`gplus"}
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											<img width="48px" height="48px" src="site/layout/images/social/gplus.png"/>
											Google Plus
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="Google app name" name="GOOGLE_APP_NAME" value=$ADM_SETTINGS.GOOGLE_APP_NAME}
										{formBox type="text" label="Client ID" name="GOOGLE_CLIENT_ID" value=$ADM_SETTINGS.GOOGLE_CLIENT_ID}
										{formBox type="text" label="Client Secret" name="GOOGLE_CLIENT_SECRET" value=$ADM_SETTINGS.GOOGLE_CLIENT_SECRET}
										{formBox type="text" label="Developer Key" name="GOOGLE_DEVELOPER_KEY" value=$ADM_SETTINGS.GOOGLE_DEVELOPER_KEY}
										{formBox type="text" label="Callback URL" name="GOOGLE_REDIRECT_URL" value=$ADM_SETTINGS.GOOGLE_REDIRECT_URL}


										<div class="form-actions">
											<button type="submit" class="submit btn btn-primary pull-right">
												Submit <i class="icon-angle-right"></i>
											</button>
										</div>
									</div>
								</div>
							</form>

							<form class="form-horizontal row-border" action="/ds_{$_job}Action_vk"
							      name="{$_job}_vk" id="{$_job}_vk"
							      method="POST" target="ps">

								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											<img width="48px" height="48px" src="site/layout/images/social/vk.png"/>
											VK
										</h4>
									</div>
									<div class="widget-content">
										{getFormToken f="`$_job`twitter"}
										{formBox type="text" label="Client ID" name="VK_CLIENT_ID" value=$ADM_SETTINGS.VK_CLIENT_ID}
										{formBox type="text" label="Permissions" name="VK_PERMISSIONS" value=$ADM_SETTINGS.VK_PERMISSIONS}
										{formBox type="text" label="Secret" name="VK_SECRET" value=$ADM_SETTINGS.VK_SECRET}
										{formBox type="text" label="Callback Url" name="VK_CALLBACK_URL" value=$ADM_SETTINGS.VK_CALLBACK_URL}


										<div class="form-actions">
											<button type="submit" class="submit btn btn-primary pull-right">
												Submit <i class="icon-angle-right"></i>
											</button>
										</div>
									</div>
								</div>
							</form>
							<form class="form-horizontal row-border" action="/ds_{$_job}Action_xing"
							      name="{$_job}_xing" id="{$_job}_xing"
							      method="POST" target="ps">

								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											<img width="48px" height="48px" src="site/layout/images/social/xing.png"/>
											Xing
										</h4>
									</div>
									<div class="widget-content">
										{getFormToken f="`$_job`twitter"}
										{formBox type="text" label="Key" name="XING_KEY" value=$ADM_SETTINGS.XING_KEY}
										{formBox type="text" label="Secret" name="XING_SECRET" value=$ADM_SETTINGS.XING_SECRET}
										{formBox type="text" label="Callback URL" name="XING_CALLBACK_URL" value=$ADM_SETTINGS.XING_CALLBACK_URL}

										<div class="form-actions">
											<button type="submit" class="submit btn btn-primary pull-right">
												Submit <i class="icon-angle-right"></i>
											</button>
										</div>
									</div>
								</div>
							</form>
							<form class="form-horizontal row-border" action="/ds_{$_job}Action_instagram"
							      name="{$_job}_instagram" id="{$_job}_instagram"
							      method="POST" target="ps">

								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											<img width="48px" height="48px" src="site/layout/images/social/instagram.png"/>
											Instagram
										</h4>
									</div>
									<div class="widget-content">
										{getFormToken f="`$_job`instagram"}
										{if isset($ADM_SETTINGS.INSTAGRAM_CLIENT_ID)}
											{formBox type="text" label="Client ID" name="INSTAGRAM_CLIENT_ID" value=$ADM_SETTINGS.INSTAGRAM_CLIENT_ID}
										{else}
											{formBox type="text" label="Client ID" name="INSTAGRAM_CLIENT_ID" value=""}
										{/if}
										{if isset($ADM_SETTINGS.INSTAGRAM_CLIENT_SECRET)}
											{formBox type="text" label="Client Secret" name="INSTAGRAM_CLIENT_SECRET" value=$ADM_SETTINGS.INSTAGRAM_CLIENT_SECRET}
										{else}
											{formBox type="text" label="Client Secret" name="INSTAGRAM_CLIENT_SECRET" value=""}
										{/if}
										{if isset($ADM_SETTINGS.INSTAGRAM_CALLBACK)}
											{formBox type="text" label="Callback URL" name="INSTAGRAM_CALLBACK" value=$ADM_SETTINGS.INSTAGRAM_CALLBACK}
										{else}
											{formBox type="text" label="Callback URL" name="INSTAGRAM_CALLBACK" value=""}
										{/if}

										<div class="form-actions">
											<button type="submit" class="submit btn btn-primary pull-right">
												Submit <i class="icon-angle-right"></i>
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.row -->
	<!-- /Page Content -->
</div>
<!-- /.container -->

</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>