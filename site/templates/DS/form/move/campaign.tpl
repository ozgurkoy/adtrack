{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New"}
{if $_item->gotValue}
	{if isset($copyCampaign) && $copyCampaign}
		{assign var="actionLabel" value="Copy"}
	{else}
		{assign var="actionLabel" value="Edit"}
	{/if}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<link href="/site/layout/DS/css/crop.css" rel="stylesheet" type="text/css"/>
	{literal}
		<style>
			.non{
				display:none !important;
			}
			.form-control[disabled], .form-control[readonly], fieldset[disabled]{
				background-color: #eee;
				color:#000;
			}

		</style>
	{/literal}

	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_campaigns,Campaigns;,$actionLabel" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
				<!-- Tabs-->
				<div class="tabbable tabbable-custom tabbable-full-width">
				{include file="DS/_campaignTabs.tpl"}
				<div class="tab-content row">
					<!--=== AddEdit ===-->

					<div class="tab-pane active" id="tab_addEdit">
						<form class="form-horizontal" action="/ds_campaignSave" name="{$_job}" id="{$_job}" method="POST" target="ps" enctype="multipart/form-data">
							{if $_item->gotValue && !$copyCampaign}
								<input type="hidden" name="id" value="{$_item->id}"/>
							{/if}

							{getFormToken f=$_job}

							{if $_item->gotValue && $_item->state  > 3 && !$copyCampaign}
							<div class="alert alert-warning fade in">
								Campaign passed the offer state. So you can't change any information from here
							</div>
							{/if}
							<div class="widget box">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										General Information
									</h4>
								</div>
								<div class="widget-content">
									<div class="row">
										<div class="col-md-6">
											{if $_item->gotValue && !$copyCampaign}
												{*<input type="hidden" name="id" value="{$_item->id}"/>*}
												{formBox type="radio" label="Camp Type" name="race" options=$races value=$_item->race disabled="disabled" class="radio-inline"}
												{if $_item->race == 1}
												{formBox type="radio" label="Publisher Type" name="effectType" options=$effectTypes value=$_item->race class="radio-inline"  customAttribute="data-campaign-only" attributeValue="camp_1" disabled="disabled"}
												{/if}
											{else}
												{if $copyCampaign}
													{formBox type="radio" label="Camp Type" name="race" options=$races value=$_item->race class="radio-inline"}
												{else}
													{formBox type="radio" label="Camp Type" name="race" options=$races value=1 class="radio-inline"}
												{/if}
												{formBox type="radio" label="Publisher Type" name="effectType" options=$effectTypes value=$_item->race class="radio-inline"  customAttribute="data-campaign-only" attributeValue="camp_1"}

											{/if}

											{getFormToken f=$_job}

											{*{if !$_item->gotValue || ($_item->gotValue && $_item->race == 1)}*}
											{*{/if}*}
											{if $_item->gotValue && !$copyCampaign}
												{formBox type="text" label="Advertiser" name="advertiser" value=$_item->advertiser->companyName disabled="disabled"}
												<input type="hidden" name="advertiserId" id="advertiserId" value="{$_item->advertiser->id}">{*< this is for related select boxes of cpl*}
											{else}
												{formBox type="populateSelect" label="Advertiser" name="advertiserId" options=$advertisers vkey="companyName" attributes="commissionRate,cpcCost" value=$_item->advertiser }
											{/if}
											{if $_item->gotValue && !$copyCampaign}
												{formBox type="text" label="Brand" name="brand" value=$_item->brand->name disabled="disabled"}
											{else}
												{if $copyCampaign}
													{formBox type="populateSelect" label="Brand" name="brand" options=$brands vkey="name" value=$_item->brand}
												{else}
													{formBox type="populateSelect" label="Brand" name="brand" options=null vkey="name" value=$_item->brand}
												{/if}
												{formBox type="text" label="New Brand" name="brandName" value=null}
											{/if}
										</div>
										<div class="col-md-6">
											{if $_item->gotValue && !$copyCampaign}
												{formBox type="text" label="Campaign Code" name="campaignCode" value=$_item->campCode disabled="disabled"}
											{/if}
											{formBox type="text" label="Camp Title" name="title" value=$_item->title}
											{formBox type="text" label="Start date" name="startDate" value=$_item->startDate class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat}
											{formBox type="text" label="End date" name="endDate" value=$_item->endDate class="datepicker" customAttribute="dateFormat" icon="icon-calendar" attributeValue=$ADM_SETTINGS.darksideJSDateFormat}
											{formBox type="text" label="Commission Rate" info="ex. 10 / 15 , number don't need %  " name="commissionRate" class="commissionRate-click only-numeric" value=$_item->commissionRate blockCustomAttr='data-campaign-only="camp_1,camp_2,camp_3,camp_4"' }
										</div>
									</div>
								</div>
							</div>
							<div class="widget box">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										Demographical Information
									</h4>
								</div>
								<div class="widget-content">
									<div class="row demographical-filter">
										<div class="col-md-6">
											{if $_item->gotValue}
												{assign var=ageRangesValue value="`,`_`$_item->campaign_campaignFilter->age`"}
												{formBox type="checkboxGroup" label="Age Ranges" name="agerange" class="checkbox-inline demographic-filter filter-age" options=$ageRanges value=$ageRangesValue}
												{formBox type="radio" label="Gender" name="sex" class="radio-inline demographic-filter filter-sex" options=$genders value=$_item->campaign_campaignFilter->sex }
												{formBox type="transferSelectWithoutWidget" label="Interest" name="interests" value=$_item->campaign_campaignFilter->trends_campaignFilter options=$trends vkey="title" class="demographic-filter filter-interest" labels=$trendLabels}
												{if $clubsCount || $_item->campaign_campaignFilter->incClub_campaignFilter->gotValue || $_item->campaign_campaignFilter->excClub_campaignFilter->gotValue }
													{formBox type="transferSelectWithoutWidget" label="Clubs" name="incClubs" value=$_item->campaign_campaignFilter->incClub_campaignFilter options=$clubs vkey="clubName" class="demographic-filter filter-incClub"}
													{formBox type="transferSelectWithoutWidget" label="Excluded Clubs" name="excClubs" value=$_item->campaign_campaignFilter->excClub_campaignFilter options=$clubsExclude vkey="clubName" class="demographic-filter filter-excClub"}
												{/if}
												{formBox type="transferSelectWithoutWidget" label="Excluded Campaign Publishers" name="excludedCampaign" value=$_item->campaign_campaignFilter->excCampaign_campaignFilter options=$excludedCampaign vkey="title" class="demographic-filter filter-excludedCampaign"}
												{formBox type="transferSelectWithoutWidget" label="City" name="city" value=$_item->campaign_campaignFilter->city_campaignFilter options=$cities vkey="cityName" class="demographic-filter filter-city"}
											{else}
												{formBox type="checkboxGroup" label="Age Ranges" name="agerange" options=$ageRanges value=null class="checkbox-inline demographic-filter filter-age"}
												{formBox type="radio" label="Gender" name="sex" class="radio-inline demographic-filter filter-sex" options=$genders value=null}
												{formBox type="transferSelectWithoutWidget" label="Interest" name="interests" value=null options=$trends vkey="title" class="demographic-filter filter-interest" labels=$trendLabels}
												{if $clubsCount}
													{formBox type="transferSelectWithoutWidget" label="Clubs" name="incClubs" value=null options=$clubs vkey="clubName" class="demographic-filter filter-incClub"}
													{formBox type="transferSelectWithoutWidget" label="Excluded Clubs" name="excClubs" value=null options=$clubsExclude vkey="clubName" class="demographic-filter filter-excClub"}
												{/if}
												{formBox type="transferSelectWithoutWidget" label="Excluded Campaign Publishers" name="excludedCampaign" value=null options=$excludedCampaign vkey="title" class="demographic-filter filter-excludedCampaign"}
												{formBox type="transferSelectWithoutWidget" label="City" name="city" value=null options=$cities vkey="cityName" class="demographic-filter filter-city"}
											{/if}
										</div>
										<div class="col-md-6">
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-user"></i>
														</div>
														<div class="title">Publisher Count</div>
														<div class="value counter-publisher">{if $_item->gotValue}{nformat d=0 n=$_item->publisherCount}{else}0{/if}</div>
													</div>
												</div>
											</div>
											{if !$_item->gotValue || ($_item->gotValue && $_item->race == 1)}
											<div class="follower-counter hide-default " data-campaign="camp_1">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual cyan">
															<i class="icon-user"></i>
														</div>
														<div class="title">Follower</div>
														<div class="value counter-follower">{if $_item->gotValue && $_item->race == 1}{nformat d=0 n=$_item->campaign_campaignTwitter->follower}{else}0{/if}</div>
													</div>
												</div>
											</div>
											<div class="influence-counter hide-default " data-campaign="camp_1">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual green">
															<i class="icon-user"></i>
														</div>
														<div class="title">Influence</div>
														<div class="value counter-influence">{if $_item->gotValue && $_item->race == 1}{nformat d=0 n=$_item->campaign_campaignTwitter->influencer}{else}0{/if}</div>
													</div>
												</div>
											</div>
											<div class="price-counter hide-default" data-campaign="camp_1">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual green">
															<i class="icon-user"></i>
														</div>
														<div class="title">Total Price</div>
														<div class="value counter-price">{if $_item->gotValue}{nformat d=0 n=$_item->campaign_campaignTwitter->maxCost}{else}0{/if}</div>
													</div>
												</div>
											</div>
											{/if}
											<div class="statbox widget box box-shadow">
												<div class="widget-content div-publishers">
													<button type="button" class="btn btn-primary btn-block hide-default" id="btn-load-publisher">Load publishers</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							{if !$_item->gotValue || ($_item->gotValue && $_item->race == 1)}
							<div class="widget box calculator-effect hide-default"  data-campaign="camp_1">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										adMingle.Effect Campaign Budget
									</h4>
								</div>
								<div class="widget-content">
									<div class="row demographical-filter">
										<div class="col-md-6">
											{if !$_item->gotValue || ($_item->gotValue && $_item->state < 5)}
												{formBox type="text" label="Budget" name="budget" value=$_item->budget+$_item->commission class="budget-effect only-numeric"}
											{else}
												{formBox type="text" label="Budget" name="budget" value=$_item->budget+$_item->commission class="budget-effect" disabled="disabled"}
											{/if}
										</div>
										<div class="col-md-6">
											<div class="hidden-xs" data-campaign-only="camp_1,camp_2,camp_3,camp_4">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Agency Commission</div>
														<div class="value budget-counter-commission-effect">0</div>
													</div>
												</div>
											</div>
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-user"></i>
														</div>
														<div class="title">Publisher Count</div>
														<div class="value budget-counter-publisher">0</div>
													</div>
												</div>
											</div>
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual cyan">
															<i class="icon-user"></i>
														</div>
														<div class="title">Follower</div>
														<div class="value budget-counter-follower">0</div>
													</div>
												</div>
											</div>
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual green">
															<i class="icon-user"></i>
														</div>
														<div class="title">Influence</div>
														<div class="value budget-counter-influence">0</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							{/if}
							{if !$_item->gotValue || ($_item->gotValue && $_item->race == 3)}
							<div class="widget box calculator-click hide-default"  data-campaign="camp_3">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										adMingle.Click Campaign Budget
									</h4>
								</div>
								<div class="widget-content">
									<div class="row demographical-filter">
										<div class="col-md-6">
											{assign var=pac value=0}

											{if !$_item->gotValue || ($_item->gotValue && $_item->state < 5)}
												{if is_object($_item->campaign_campaignCPC)}
													{assign var=pac value=$_item->campaign_campaignCPC->perAdvertiserClick}

													{formBox type="text" label="Advertiser Cost" info="Cost charged per click from advertiser<br />YOU HAVE TO INCLUDE THE AGENCY COMMISSION IN THIS NUMBER" blockCustomAttr='data-campaign-only="camp_3"' name="perAdvertiserClick" value=$pac}
													{formBox type="text" label="CPC Cost" name="cpcPrice" class="cpcPriceInput only-numeric" info="Publisher revenue" value=$_item->campaign_campaignCPC->cpcCost}
													{formBox type="text" label="Budget" name="budget" class="budget-click only-numeric" value=$_item->budget+$_item->commission|intval}
													{formBox type="text" label="Click Count" name="maxClick" class="only-numeric" value=$_item->campaign_campaignCPC->maxClick}
													{formBox type="text" label="Max Unreferred Click" name="maxAnonRefCount" value=$_item->campaign_campaignCPC->maxAnonRefCount}
													{formBox type="radio" label="Redirect After Campaign Finish" name="keepRedirecting" class="radio-inline" options=$keepRedirectingActions value=$_item->campaign_campaignCPC->keepRedirecting}
												{else}
													{formBox type="text" label="Advertiser Cost" info="Cost charged per click from advertiser<br />YOU HAVE TO INCLUDE THE AGENCY COMMISSION IN THIS NUMBER" blockCustomAttr='data-campaign-only="camp_3"' name="perAdvertiserClick" value=0}
													{formBox type="text" label="CPC Cost" name="cpcPrice" class="cpcPriceInput only-numeric" info="Publisher revenue" value=null}
													{formBox type="text" label="Budget" name="budget" class="budget-click only-numeric" value=0}
													{formBox type="text" label="Click Count" name="maxClick" class="only-numeric" value=0}
													{formBox type="text" label="Max Unreferred Click" name="maxAnonRefCount" value=0}
													{formBox type="radio" label="Keep Redirection" name="keepRedirecting" class="radio-inline" options=$keepRedirectingActions value=null}

												{/if}
											{else}
												{assign var=pac value=$_item->campaign_campaignCPC->perAdvertiserClick}
												{formBox type="text" label="Advertiser Cost" info="Cost charged per click from advertiser<br />YOU HAVE TO INCLUDE THE AGENCY COMMISSION IN THIS NUMBER" blockCustomAttr='data-campaign-only="camp_3"' name="perAdvertiserClick" value=$pac}
												{formBox type="text" label="CPC Cost" name="cpcPrice" class="cpcPriceInput" info="Publisher revenue" value=$_item->campaign_campaignCPC->cpcCost disabled="disabled"}
												{formBox type="text" label="Budget" name="budget" class="budget-click" value=$_item->budget+$_item->commission|intval disabled="disabled"}
												{formBox type="text" label="Click Count" name="maxClick" value=$_item->campaign_campaignCPC->maxClick disabled="disabled"}
												{formBox type="text" label="Max Unreferred Click" name="maxAnonRefCount" value=$_item->campaign_campaignCPC->maxAnonRefCount disabled="disabled"}
												{formBox type="radio" label="Redirect After Campaign Finish" name="keepRedirecting" class="radio-inline" options=$keepRedirectingActions value=$_item->campaign_campaignCPC->keepRedirecting disabled="disabled"}
											{/if}
										</div>
										<div class="col-md-6">
											<div class="hidden-xs" data-campaign-only="camp_1,camp_2,camp_3,camp_4">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Agency Commission</div>
														<div class="value budget-counter-commission-click">0</div>
													</div>
												</div>
											</div>

											<div class="hidden-xs" data-campaign-only="camp_9">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Advertiser/Publisher Multiplier</div>
														<div class="value ratioBoxx">0</div>
														{*<div class="title" style="line-height: 3px"><sub>Considering commission</sub></div>*}

													</div>
												</div>
											</div>
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Publisher Revenue Per Click</div>
														<div class="value budget-counter-price-click">0</div>
													</div>
												</div>
											</div>
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Advertiser Sale Price Per Click</div>
														<div class="value budget-counter-salePrice-click">0</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							{/if}
							{if !$_item->gotValue || ( $_item->gotValue && ( $_item->race == 4 || $_item->race == 5 ) )}
							<div class="widget box calculator-cpl hide-default"  data-campaign="camp_4,camp_5">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										adMingle.CPL Campaign Budget
									</h4>
								</div>
								<div class="widget-content">
									<div class="row demographical-filter">
										<div class="col-md-6">
											{if !$_item->gotValue || ($_item->gotValue && $_item->state < 5)}
												{if is_object($_item->campaign_cplCampaign) && $_item->race == 4}
													{formBox type="text" label="Lead Cost" name="perLead" class="cpcPriceInput only-numeric" info="Publisher revenue" value=$_item->campaign_cplCampaign->perLead}
													{formBox type="text" label="Budget" name="budget" class="budget-cpl budget-cplRemote only-numeric" value=$_item->budget+$_item->commission|intval}
													{formBox type="text" label="Lead Count" name="maxLeadLimit" class="only-numeric" value=$_item->campaign_cplCampaign->maxLeadLimit}
													{formBox type="text" label="Max Overlapping Denies" customAttribute="data-campaign-only" attributeValue="camp_4" info="Maximum number of denies before system takes it under Admingle supervision" name="maxOverlappingNegative" value=$_item->campaign_cplCampaign->maxOverlappingNegative}
													{formBox type="text" label="Max Valid Leads from the same IP per hour" customAttribute="data-campaign-only" attributeValue="camp_4" info="Maximum number of valid leads per IP per hour" name="maxPerHourPerIp" value=$_item->campaign_cplCampaign->maxPerHourPerIp}
													{formBox type="arraySelect" label="Landing Page" blockCustomAttr='data-campaign-only="camp_4"'  defaultValue="Choose Landing Page" name="layout" defaultValue="No landing pages found" options=$_emptyArray value=$_item->campaign_cplCampaign->id attached="advertiserId" src="/ds_xhr/loadLandingPages" customAttribute="data-chooseOne" attributeValue="Choose one"}
												{elseif is_object($_item->campaign_cplRemoteCampaign) && $_item->race == 5}
													{formBox type="text" label="Advertiser Cost" info="Cost charged per lead from advertiser" blockCustomAttr='data-campaign-only="camp_5"' name="perAdvertiserLead" value=$_item->campaign_cplRemoteCampaign->perAdvertiserLead}
													{formBox type="text" label="Lead Cost" name="perLead" class="cpcPriceInput only-numeric" info="Publisher revenue" value=$_item->campaign_cplRemoteCampaign->perLead}
													{formBox type="text" label="Lead Count" name="maxLeadLimit" class="only-numeric" value=$_item->campaign_cplRemoteCampaign->maxLeadLimit}
													{formBox type="text" label="Budget" name="budget" class="budget-cpl budget-cplRemote only-numeric" value=$_item->budget+$_item->commission|intval}
												{else}
													{formBox type="text" label="Advertiser Cost" info="Cost charged per lead from advertiser" blockCustomAttr='data-campaign-only="camp_5"' name="perAdvertiserLead" value=10}
													{formBox type="text" label="Lead Cost" name="perLead" class="cpcPriceInput only-numeric" info="Publisher revenue" value=null}
													{formBox type="text" label="Lead Count" name="maxLeadLimit" class="only-numeric" value=0}
													{formBox type="text" label="Budget" name="budget" class="budget-cpl budget-cplRemote only-numeric" value=$_item->budget+$_item->commission|intval}
													{formBox type="text" label="Max Valid Leads from the same IP per hour" customAttribute="data-campaign-only" attributeValue="camp_4" info="Maximum number of valid leads per IP per hour" name="maxPerHourPerIp" value=10}
													{formBox type="text" label="Max Overlapping Denies" customAttribute="data-campaign-only" attributeValue="camp_4" name="maxOverlappingNegative" value=10}
													{formBox type="arraySelect" label="Landing Page" blockCustomAttr='data-campaign-only="camp_4"' defaultValue="Choose Landing Page" name="layout" defaultValue="No landing pages found" options=$_emptyArray attached="advertiserId" src="/ds_xhr/loadLandingPages" customAttribute="data-chooseOne" attributeValue="Choose one"}
												{/if}
											{else}
												{if $_item->race == 4}
													{formBox type="text" label="Lead Cost" name="perLead" class="cplPriceInput" info="Publisher revenue" value=$_item->campaign_cplCampaign->perLead disabled="disabled"}
													{formBox type="text" label="Budget" name="budget" class="budget-cpl" value=$_item->budget+$_item->commission|intval disabled="disabled"}
													{formBox type="text" label="Lead Count" name="maxLeadLimit" value=$_item->campaign_cplCampaign->maxLeadLimit disabled="disabled"}
													{formBox type="text" label="Max Overlapping Denies" info="Maximum number of denies before system takes it under Admingle supervision"  name="maxOverlappingNegative" value=$_item->campaign_cplCampaign->maxOverlappingNegative disabled="disabled"}
													{formBox type="text" label="Max Valid Leads from the same IP per hour" info="Maximum number of valid leads per IP per hour" name="maxPerHourPerIp" value=10 disabled="disabled"}
												{elseif $_item->race == 5}
													{formBox type="text" label="Advertiser Cost" info="Cost charged per lead from advertiser" blockCustomAttr='data-campaign-only="camp_5"' name="perAdvertiserLead" value=$_item->campaign_cplRemoteCampaign->perAdvertiserLead}
													{formBox type="text" label="Lead Cost" name="perLead" class="cplPriceInput" info="Publisher revenue" value=$_item->campaign_cplRemoteCampaign->perLead disabled="disabled"}
													{formBox type="text" label="Lead Count" name="maxLeadLimit" value=$_item->campaign_cplRemoteCampaign->maxLeadLimit disabled="disabled"}
													{formBox type="text" label="Budget" name="budget" class="budget-cpl" value=$_item->budget+$_item->commission|intval disabled="disabled"}
												{/if}
											{/if}
										</div>
										<div class="col-md-6">
											<div class="hidden-xs" data-campaign-only="camp_1,camp_2,camp_3,camp_4">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Agency Commission</div>
														<div class="value budget-counter-commission-cpl">0</div>
													</div>
												</div>
											</div>
											<div class="hidden-xs" data-campaign-only="camp_3">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Advertiser/Publisher Multiplier</div>
														<div class="value ratioBoxx">0</div>
													</div>
												</div>
											</div>
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Publisher Revenue Per Lead</div>
														<div class="value budget-counter-price-cpl">0</div>
													</div>
												</div>
											</div>
											<div class="hidden-xs">
												<div class="statbox widget box box-shadow">
													<div class="widget-content">
														<div class="visual red">
															<i class="icon-money"></i>
														</div>
														<div class="title">Advertiser Sale Price Per Lead</div>
														<div class="value budget-counter-salePrice-cpl">0</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							{/if}
							<div class="widget box">
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										Campaign Messages & Other Informations
									</h4>
								</div>
								<div class="widget-content">
									<div class="row">
										<div class="col-md-6">
											{if !$_item->gotValue}
												{formBox type="limitedTextarea" label="Message 1" name="message[1]" id="message-1" charLimit="$charLimit" class="camp-message" value=null}
												{formBox type="limitedTextarea" label="Message 2" name="message[2]" id="message-2" charLimit="$charLimit" class="camp-message" value=null}
												{formBox type="limitedTextarea" label="Message 3" name="message[3]" id="message-3" charLimit="$charLimit" class="camp-message" value=null}
												{formBox type="limitedTextarea" label="Message 4" name="message[4]" id="message-4" charLimit="$charLimit" class="camp-message" value=null}
												{formBox type="limitedTextarea" label="Message 5" name="message[5]" id="message-5" charLimit="$charLimit" class="camp-message" value=null}
											{else}
												{assign var=cc value=0}
												{doWhile}
													{assign var=cc value=$cc+1}
													{formBox type="limitedTextarea" label="Message $cc" name="message[$cc]" id="message-$cc" charLimit="$charLimit" class="camp-message" value=$_item->campaign_campaignMessage->message}
												{/doWhile ($_item->campaign_campaignMessage->populate())}
											{/if}
											{if !($_item->gotValue && $_item->state  > 3)}
												<div class="form-group">
													<div class="col-md-12">
														<button type="button" class="btn btn-primary btn-block" id="btn-add-message">Add New Message</button>
													</div>
												</div>
											{/if}
										</div>
										<div class="col-md-6">
											{if $_item->race == 3 || $_item->race == 1} {*CPC*}
												{formBox type="arraySelect" label="Short Link Domain" name="SLDomain" options=$SLDomains value=$_item->campaign_campaignClick->SLDomain customAttribute="data-campaign-only" attributeValue="camp_3,camp_4,camp_6,camp_5,camp_1"}
												{formBox type="text" label="Campaign Link" name="link" info="Always use a trailing slash after domain extension, http://www.google.com/?query" value=$_item->campaign_campaignClick->link customAttribute="data-campaign-only" attributeValue="camp_3,camp_5,camp_1"}
												{formBox type="text" label="Hash Code" name="hashCode" value=$_item->campaign_campaignClick->hashCode  customAttribute="data-campaign-only" attributeValue="camp_3"}
												{formBox type="limitedTextarea" label="Camp Detail" name="campDetail" charLimit="146" value=$_item->campaign_campaignClick->campDetail}
												{if strlen($_item->campaign_campaignClick->linkHash) && !$copyCampaign}{formBox type="text" label="Link Hash" name="linkHash" value=$_item->campaign_campaignClick->linkHash disabled="disabled"}{/if}
											{elseif $_item->race == 4}  {*CPL*}
												{formBox type="arraySelect" label="Short Link Domain" name="SLDomain" options=$SLDomains value=$_item->campaign_cplCampaign->SLDomain customAttribute="data-campaign-only" attributeValue="camp_3,camp_4,camp_6,camp_5,camp_1"}
												{formBox type="limitedTextarea" label="Camp Detail" name="campDetail" charLimit="146" value=$_item->campaign_cplCampaign->campDetail}
											{elseif $_item->race == 5}  {*CPL.REMOTE*}
												{formBox type="arraySelect" label="Short Link Domain" name="SLDomain" options=$SLDomains value=$_item->campaign_cplRemoteCampaign->SLDomain customAttribute="data-campaign-only" attributeValue="camp_3,camp_4,camp_6,camp_5,camp_1"}
												{formBox type="populateSelect" label="Affiliate Provider" name="affService" options=$affService vkey="name" blockCustomAttr='data-campaign-only="camp_5,camp_6"' value=$_item->campaign_cplRemoteCampaign->cpaAffService}
												{formBox type="arrayKVSelect" label="Operation Type" blockCustomAttr='data-campaign-only="camp_5,camp_6"' defaultValue="Choose Operation Type" name="opType" options=$cplRemote->opType_options value=$_item->campaign_cplRemoteCampaign->opType}
												{formBox type="text" label="Campaign Link" name="link" info="Always use a trailing slash after domain extension, http://www.google.com/?query" value=$_item->campaign_cplRemoteCampaign->link customAttribute="data-campaign-only" attributeValue="camp_3,camp_5,camp_1"}
												{formBox type="text" label="Hash Code" name="hashCode" value=$_item->campaign_cplRemoteCampaign->hashCode  customAttribute="data-campaign-only" attributeValue="camp_5"}
												{formBox type="limitedTextarea" label="Camp Detail" name="campDetail" charLimit="146" value=$_item->campaign_cplRemoteCampaign->campDetail}
											{elseif $_item->race == 6}  {*CPA.AFF*}
												{formBox type="arraySelect" label="Short Link Domain" name="SLDomain" options=$SLDomains value=$_item->campaign_cpaAff->SLDomain customAttribute="data-campaign-only" attributeValue="camp_3,camp_4,camp_6,camp_5,camp_1"}
												{formBox type="populateSelect" label="Affiliate Provider" name="affService" options=$affService vkey="name" blockCustomAttr='data-campaign-only="camp_5,camp_6"' value=$_item->campaign_cpaAff->cpaAffService info=""}
												{formBox type="arrayKVSelect" label="Operation Type" blockCustomAttr='data-campaign-only="camp_5,camp_6"' defaultValue="Choose Operation Type" name="opType" options=$cplRemote->opType_options value=$_item->campaign_cpaAff->opType}
												{formBox type="text" label="Campaign Link" name="link" info="Always use a trailing slash after domain extension, http://www.google.com/?query" value=$_item->campaign_cpaAff->link customAttribute="data-campaign-only" attributeValue="camp_3,camp_5,camp_6,camp_1"}
												{formBox type="text" label="Hash Code" name="hashCode" value=$_item->campaign_cpaAff->hashCode  customAttribute="data-campaign-only" attributeValue="camp_6"}
												{formBox type="limitedTextarea" label="Camp Detail" name="campDetail" charLimit="146" value=$_item->campaign_cpaAff->campDetail customAttribute="data-campaign-only" attributeValue="camp_6"}
												{formBox type="textarea" label="Commission Info" name="commissionInfo" customAttribute="data-campaign-only" value=$_item->campaign_cpaAff->commissionInfo  attributeValue="camp_6"}
												{formBox type="text" label="Rev. Percentage" name="revPerc" value=$_item->campaign_cpaAff->revPerc customAttribute="data-campaign-only" attributeValue="camp_6"}
											{else}
												{formBox type="arraySelect" label="Short Link Domain" name="SLDomain" options=$SLDomains value=null customAttribute="data-campaign-only" attributeValue="camp_3,camp_4,camp_6,camp_5,camp_1"}
												{formBox type="populateSelect" label="Affiliate Provider" name="affService" options=$affService vkey="name" blockCustomAttr='data-campaign-only="camp_5,camp_6"' value=null info=""}
												{formBox type="arrayKVSelect" label="Operation Type" blockCustomAttr='data-campaign-only="camp_5,camp_6"' defaultValue="Choose Operation Type" name="opType" options=$cplRemote->opType_options value=null info=""}
												{formBox type="text" label="Campaign Link" name="link" info="Always use a trailing slash after domain extension, http://www.google.com/?query" value=null customAttribute="data-campaign-only" attributeValue="camp_3,camp_5,camp_6,camp_1"}
												{formBox type="text" label="Hash Code" name="hashCode" value=null  customAttribute="data-campaign-only" attributeValue="camp_3,camp_5,camp_6"}
												{formBox type="limitedTextarea" label="Camp Detail" name="campDetail" charLimit="146" value=null}
												{formBox type="textarea" label="Commission Info" name="commissionInfo" value=null customAttribute="data-campaign-only"  attributeValue="camp_6"}
												{formBox type="text" label="Rev. Percentage" name="revPerc" value=50 customAttribute="data-campaign-only" attributeValue="camp_6"}
											{/if}

											{formBox type="textarea" label="Campaign Notes" name="campNotes" value=$_item->notes}
											{formBox type="text" label="Keywords" name="keywords" class="tags" value=$_item->keywords}

											{if is_object($_item->campaign_campaignClick) && strlen($_item->campaign_campaignClick->previewFileName)}
												{assign var=pfile value=$_item->campaign_campaignClick->previewFileName}
											{elseif is_object($_item->campaign_cplCampaign) && strlen($_item->campaign_cplCampaign->previewFileName)}
												{assign var=pfile value=$_item->campaign_cplCampaign->previewFileName}
											{elseif is_object($_item->campaign_cplRemoteCampaign) && strlen($_item->campaign_cplRemoteCampaign->previewFileName)}
												{assign var=pfile value=$_item->campaign_cplRemoteCampaign->previewFileName}
											{elseif is_object($_item->campaign_cpaAff) && strlen($_item->campaign_cpaAff->previewFileName)}
												{assign var=pfile value=$_item->campaign_cpaAff->previewFileName}
											{/if}

										</div>
									</div>
									<div class="row">
										<div class="col-md-12 form-vertical no-margin">
											<div class="form-group">
												<label class="col-md-2 control-label" for="input17">Campaign Preview Image</label>
												<div class="col-md-10">
													<input type="file" class="uploadfile" id="uploadfile" name="uploadfile"/>
													<div class="newupload">Please select</div>
													<div class="example">

													</div>
												</div>
											</div>

											{if isset($pfile)}
												<div class="form-group">
													<label class="col-md-2 control-label" for="input17">Current Image</label>
													<div class="col-md-10">
														<div class="list-group">
															<li class="list-group-item no-padding">
																<img src="/site/layout/images/campaign/{$pfile}" alt="">
															</li>
														</div>
													</div>
												</div>
											{/if}
										</div>
									</div>
									<div class="row">
										<div class="col-md-12 form-vertical no-margin" data-campaign-only="camp_3,camp_4,camp_6,camp_5">
											<div class="form-group">
												<label class="col-md-2 control-label">Sharing Options: </label>
												<div class="col-md-10">
													<div class="col-md-12">
														{include file="DS/campaignSocialSharing.tpl"}
													</div>
												</div>
											</div>
										</div>
									</div>
									{if !$_item->gotValue || ($_item->gotValue && $_item->state  <= 3)}
									<div class="row">
										<div class="col-md-12 form-vertical no-margin">
											<div class="form-actions">
												<button type="submit" class="submit btn btn-primary pull-right">
													Submit <i class="icon-angle-right"></i>
												</button>
												{if !$_item->gotValue}
												<button type="reset" class="btn btn-danger">Cancel</button>
												{/if}
											</div>
										</div>
									</div>
									{/if}
								</div>
							</div>
						</form>
					</div>
					<!-- /AddEdit -->

					<!--=== Overview ===-->
					<div class="tab-pane" id="tab_overview" >
					</div>
					<!-- /Overview -->
				</div> <!-- /.tab-content -->
				</div>
				<!--END TABS-->
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
<script src="/site/layout/DS/js/crop.js" type="text/javascript"></script>
<script type="text/javascript">
	var advertiser = null
		,filterAge = null
		,filterSex = null
		,filterInterest = null
		,filterIncClub = null
		,filterExcClub = null
		,filterExcCampaign = null
		,filterCity = null
		,brand = null
		,buildFilter = null
		,newBrand = null
		,commission = null
		,gotValue = {if $_item->gotValue}true{else}false{/if}
		,campaignState = '{$_item->state}'
		,ageRange = "{if $_item->gotValue}{$_item->campaign_campaignFilter->age}{/if}"
		{*,cpcPrice = new Number({if $_item->gotValue && $_item->race == 3}{$_item->campaign_campaignCPC->cpcCost}{else}{$ADM_SETTINGS.CPC_COST}{/if}).toPrecision(4)*}
		,cpcPubPrice = parseFloat(new Number({if $_item->gotValue && $_item->race == 3}{$_item->campaign_campaignCPC->cpcCost}{else}{$ADM_SETTINGS.CPC_COST}{/if}).toPrecision(4))
		,cpcPrice = parseFloat(new Number({if $_item->gotValue && $_item->race == 3}{$pac}{else}{$ADM_SETTINGS.CPC_COST}*{$ADM_SETTINGS.CPC_MULTIPLIER}{/if}).toPrecision(4))
		{if $_item->gotValue && ($_item->race == 4||$_item->race == 5)}
		{if $_item->race == 4}
		,cplPrice = {$_item->campaign_cplCampaign->perLead}
		,cplAPrice = 0
		{elseif $_item->race == 5}
		,cplPrice = {$_item->campaign_cplRemoteCampaign->perLead}
		,cplAPrice = {$_item->campaign_cplRemoteCampaign->perAdvertiserLead}
		{/if}
		{else}
		,cplPrice = {$ADM_SETTINGS.CPL_COST}
		,cplAPrice = {$ADM_SETTINGS.CPL_COST}
		{/if}
		,cpcSalePrice = null
		,cplSalePrice = null
			,cplMultiplier = {$ADM_SETTINGS.CPL_MULTIPLIER}
			,cpcMultiplier = {$ADM_SETTINGS.CPC_MULTIPLIER}
		,cplLandingPage = '{$ADM_SETTINGS.CPL_LANDING_DOMAIN}'
		,blocked = false
		,campaignCharLimitWithLink = {$ADM_SETTINGS.campaignCharLimitWithLink}
		,campaignCharLimitWithoutLink = {$ADM_SETTINGS.campaignCharLimitWithoutLink}
		,seperatorThousands = "{$ADM_SETTINGS.NUMBER_FORMAT_THOUSAND_SEPERATOR}"
		,seperatorDecimal = "{$ADM_SETTINGS.NUMBER_FORMAT_DECIMAL_SEPERATOR}"
		,effectData = null
		,pubCount = 0
		,loadBtn = null
		,publishers = null
		,races = [null,'effect',null,'click','cpl','cplRemote']
		,canEditable = {if $_item->gotValue && $_item->state  > 3}false{else}true{/if};

	{literal}
	var addOtherOption = function(o){
		o.append($('<option>', {
			value: -1,
			text: 'Add New Brand'
		}));
	};

	var toggleRace = function ( recall ) {
		var race = getRace();
		$( 'div[data-campaign][data-campaign!="camp_' + race + '"]' ).slideUp(); // hide not relateds
		$( 'div[data-campaign][data-campaign*="camp_' + race + '"]' ).slideDown(); // show ours
		$( 'input[name="budget"][class*="budget-"]' ).attr( 'disabled', true );
		$( 'input[name="budget"][class*="budget-' + races[race] + '"]' ).attr( 'disabled', false );

		$('*[data-campaign-only]' ).each( function ( i, e ) {
			e = $( e );

			if ( e.attr( "data-campaign-only" ).indexOf( 'camp_' + race ) != -1 ){
				if( e.hasClass('form-group') == true)
					e.removeClass( 'non' );
				else{
					e.removeClass( 'non' );
					e.parents('.form-group:first').removeClass( 'non' );
				}
			}
			else{
				if( e.hasClass('form-group') != true ){
					e.addClass( 'non' );
					e.parents('.form-group:first').addClass( 'non' );
				}
				else
					e.addClass( 'non' );
			}

		});
		calculateCPLPrice();
		if ( typeof recall === "undefined" )
			buildFilter();
	};

	var createFilterString = function(){
		var filterData = "race=" + $("input[name='race']:radio:checked").val();
		filterAge.each(function(i,v){
			if (v.checked && v.value.length) filterData += "&agerange[]=" + v.value;
		});
		filterSex.each(function(i,v){
			if (v.checked && v.value.length) filterData += "&sex=" + v.value;
		});
		filterInterest.find("option").each(function(i,v){
			if (v.value.length) filterData += "&interest[]=" + v.value;
		});
		filterIncClub.find("option").each(function(i,v){
			if (v.value.length) filterData += "&incClub[]=" + v.value;
		});
		filterExcClub.find("option").each(function(i,v){
			if (v.value.length) filterData += "&excClub[]=" + v.value;
		});
		filterExcCampaign.find("option").each(function(i,v){
			if (v.value.length) filterData += "&excludedCampaign[]=" + v.value;
		});
		filterCity.find("option").each(function(i,v){
			if (v.value.length) filterData += "&city[]=" + v.value;
		});
		return filterData;
	};

	var buildFilter = function(){
		var filterData = "";
/*
		if (!$("input[name='race']:radio:checked").length){
			$("input[name='race']:first").attr("checked",true).parent().addClass("checked");
			toggleRace(false);
		}
*/

		filterData = createFilterString();
		/*
		filterAge.each(function(i,v){
			if (v.checked && v.value.length) filterData += "&agerange[]=" + v.value;
		});
		filterSex.each(function(i,v){
			if (v.checked && v.value.length) filterData += "&sex=" + v.value;
		});
		filterInterest.find("option").each(function(i,v){
			if (v.value.length) filterData += "&interest[]=" + v.value;
		});
		filterIncClub.find("option").each(function(i,v){
			if (v.value.length) filterData += "&incClub[]=" + v.value;
		});
		filterExcClub.find("option").each(function(i,v){
			if (v.value.length) filterData += "&excClub[]=" + v.value;
		});
		*/

		$.ajax({
			type: "POST",
			url: "/ds_campaignFilter",
			data: filterData,
			dataType: "json",
			timeout: 10000, // in milliseconds
			beforeSend:function(){
				$('div.demographical-filter').block({
					message: '<h1>Processing</h1>',
					css: { border: '3px solid #a00' }
				});
			},
			success:function(data){
				effectData = data;
				counters = ["publisher","follower","influence","price"];
				$.each(counters,function(k,v){
					if (v == "price"){
						var cr = (100/(100-getCommissionRate()));
						effectData["price"] = effectData["price"] * cr;
						changeVal($(".counter-" + v),(number_format(effectData["price"],0,seperatorDecimal,seperatorThousands)));
						changeVal($(".budget-counter-" + v),(number_format(effectData["price"],0,seperatorDecimal,seperatorThousands)));
						changeVal($(".budget-counter-commission-effect"),(number_format(effectData["price"]*(getCommissionRate()/100),0,seperatorDecimal,seperatorThousands)));
						if (!gotValue) $(".calculator-effect").find("#budget").val((effectData["price"]).toFixed());
					} else {
						changeVal($(".counter-" + v),number_format(data[v],0,seperatorDecimal,seperatorThousands),data[v]);
						changeVal($(".budget-counter-" + v),number_format(data[v],0,seperatorDecimal,seperatorThousands));
					}
					if(getRace() == 1)
						calculateEffect();
				});
				$('div.demographical-filter').unblock();
			},
			error:function(){
				$('div.demographical-filter').unblock();
				alert("An error occured. Please try again");
			}
		});
		return true;
	};

	var number_format = function(number, decimals, dec_point, thousands_sep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
				prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
				sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
				dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
				s = '',
				toFixedFix = function (n, prec) {
					var k = Math.pow(10, prec);
					return '' + Math.round(n * k) / k;
				};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}

	var changeVal = function(target,val,unformattedVal){
		target.animate({opacity:0},500,"linear",function(){
			$(this).html(val);
			$(this).animate({opacity:1},500);
			if ($(this).hasClass("counter-publisher")){
				pubCount = unformattedVal;
				publishers.html(loadBtn);
				bindBtn();
			}
		});
	};

	var calculateClick = function(direction){
		var b = $(".budget-click");
		var c = $("#maxClick");
		var p = getCPCPrice();

		if ((direction == "from" && b.val().length && b.val() > 0) || (direction == "to" && c.val().length && c.val() > 0)){
			if (direction == "from"){
//				c.val(Math.ceil(b.val()* ( (100-getCommissionRate())/100 )/p ));
				c.val(Math.ceil(b.val()/p ));
			}
			else{
//			    b.val(parseInt(c.val()*p));
//				b.val(Math.ceil(c.val()/ ( (100-getCommissionRate())/100 )*p ));
				b.val(Math.ceil(c.val()*p ));
			}

		}
		changeVal($(".budget-counter-commission-click"),number_format(parseFloat(b.val()*(getCommissionRate()/100)),0,seperatorDecimal,seperatorThousands));
	};
	var calculateCPL = function(direction){

		var b = $(".budget-cpl" ),
			c = $("#maxLeadLimit" ),
			p = getCPLPrice();

		if( getRace() == 4 ){
			if ((direction == "from" && b.val().length && b.val() > 0) || (direction == "to" && c.val().length && c.val() > 0)){
				if (direction == "from"){
					c.val(Math.ceil(b.val()/p));}
				else{
					b.val(parseInt(c.val()*p));}
			}
			changeVal($(".budget-counter-commission-cpl"),number_format(parseFloat(b.val()*(getCommissionRate()/100)),0,seperatorDecimal,seperatorThousands));
		}
		else{
			b.val( parseInt( $( '#perAdvertiserLead' ).val() * $( '#maxLeadLimit' ).val() ) );
		}
	};

	var calculateEffect = function(){
		var t = $(".budget-effect");
		var maxPrice = effectData["price"];
		if (t.val().length && t.val() > 0 && maxPrice > 0){
			if (maxPrice < t.val()) t.val(maxPrice);
			var fields = ["publisher","follower","influence"];
			var ratio = t.val() / maxPrice;
			$.each(fields,function(i,v){
				changeVal($(".budget-counter-" + v),number_format(parseInt(effectData[v]*ratio),0,seperatorDecimal,seperatorThousands));
			});
			changeVal($(".budget-counter-commission-effect"),number_format(parseFloat(t.val()*(getCommissionRate()/100)),0,seperatorDecimal,seperatorThousands));
		}
	}

	var calculateCPCPrice = function(){
//		cpcSalePrice = cpcPrice * cpcMultiplier * (100/(100-getCommissionRate()));
//		cpcSalePrice = cpcPrice * (100/(100-getCommissionRate()));
		cpcSalePrice = cpcPrice;
		$(".budget-counter-price-click").html(number_format(cpcPubPrice,3,seperatorDecimal,seperatorThousands));
		$(".budget-counter-salePrice-click").html(number_format(cpcPrice,3,seperatorDecimal,seperatorThousands));
		$("#perAdvertiserClick").val(parseFloat(cpcPrice).toPrecision(4));
		$("#cpcPrice").val(cpcPubPrice);
		calculateCPCRatio();
	};

	var calculateCPCRatio = function(){
//		cpcMultiplier = ((cpcPrice / cpcPubPrice) * (100/(100-getCommissionRate()))).toPrecision( 4 );
//		cpcMultiplier = ((cpcPrice / cpcPubPrice) ).toPrecision( 4 );
		$( '.ratioBoxx' ).html( cpcMultiplier );
	};

	var calculateCPLPrice = function(){
		if ( getRace() == 4 ) {
			$( '.budget-cpl.budget-cplRemote' ).removeAttr( 'readOnly' );
			cplSalePrice = cplPrice * cplMultiplier * (100 / (100 - getCommissionRate()));
			$( ".budget-counter-price-cpl" ).html( number_format( cplPrice, 3, seperatorDecimal, seperatorThousands ) );
			$( ".budget-counter-salePrice-cpl" ).html( number_format( cplSalePrice, 3, seperatorDecimal, seperatorThousands ) );
			$( "#perLead" ).val( cplPrice );
		}
		else{
			cplAPrice = $("#perAdvertiserLead").val();
			$('.budget-cpl.budget-cplRemote' ).attr('readonly','readonly')
			$( ".budget-counter-price-cpl" ).html( number_format( cplPrice, 3, seperatorDecimal, seperatorThousands ) );
			$( ".budget-counter-salePrice-cpl" ).html( number_format( cplAPrice, 3, seperatorDecimal, seperatorThousands ) );
			$( "#perLead" ).val( cplPrice );
		}
	};

	var getRace = function(){
		var e = $( "input[name='race']:checked" );
		return (e.length ? e.val() : null);
	};

	$('#affService' ).change(function(){
		var v= parseInt($(this ).val());

		$( '#opType' ).val( '' );
		if( gotValue !== true )
			$( '#opType' ).children( ':nth-child(3)' ).prop( 'disabled', true );
		else{
			$( '#opType,#affService' ).attr( 'disabled', false );
		}

		var ib= $('#affService' ).parents('.form-group:first' ).find('.help-block');
		ib.html( '' );
		if(!(v > 0) )
			return false;


		$.ajax({
			type: "GET",
			url: "/ds_xhr/getRemoteServiceDetails/"+v,
			data:{},
			dataType: "json",
			timeout: 100000, // in milliseconds
			success:function(data){
//				$( '#opType' ).children( ':nth-child(3)' ).prop( 'disabled', data.gotApi != 1 );
				$( '#hashCode' ).val( data.hash );
				ib.html( "<b>Details:</b><br>" +
						data.details + "<br><br>" +
						(
								data.tpSupport == 1 ? "<b>Tracking Pixel:</b><br>" +
								'Info : ' + data.tpInfo + '<br>' +
								'URL : ' + data.tpUrl + '' : ''
						)
				);
			},
			error:function(e){
				alert("An error occured. Please try again");
			}
		});

	});

	var getCPCPrice = function(){
		return cpcPrice; //parseFloat($(".budget-counter-salePrice-click").html());
//		return cpcSalePrice; //parseFloat($(".budget-counter-salePrice-click").html());
	};

	var getCPLPrice = function(){
		return cplSalePrice;
	};

	var getCommissionRate = function(){
		var cr = parseFloat(commission.val());
		if (cr.length == 0 || isNaN(cr)) cr = 0;
		if (cr >= 100) cr = 99;
		return cr;
	};

	var blockUnblock = function(){
		if (!blocked && (getRace() == null || advertiser.val().length == 0)){
			$('div.demographical-filter').block({
				message: '<h3>Please select the campaign race and advertiser first</h3>',
				css: { border: '3px solid #a00' }
			});
			blocked = true;
		} else if (blocked && getRace() != null && advertiser.val().length){
			$('div.demographical-filter').unblock();
			blocked = false;
		}
	};

	var bindBtn = function(){
		loadBtn.click(function(){
			if (pubCount> 0 && pubCount < 1000){
				getPublishers();
			} else if (pubCount > 0){
				bootbox.confirm("This action will take some time. Are you sure to continue?",function(confirmed){
					if (confirmed){
						getPublishers();
					}
				});
			}
			return false;
		});
		if (pubCount > 0){
			loadBtn.show();
		} else {
			loadBtn.hide();
		}
	};

	var getPublishers = function(){
		var filterData = createFilterString();
		$.ajax({
			type: "POST",
			url: "/ds_demographicalStatPublishers",
			data: filterData,
			dataType: "html",
			timeout: 100000, // in milliseconds
			beforeSend:function(){
				$('div.demographical-filter').block({
					message: '<h1>Processing</h1>',
					css: { border: '3px solid #a00' }
				});
			},
			success:function(data){
				publishers.html(data);
				$('div.demographical-filter').unblock();
			},
			error:function(){
				$('div.demographical-filter').unblock();
				alert("An error occured. Please try again");
			}
		});
	};

	$(document).ready(function(){
		//Find inputs
		advertiser = $("#advertiserId");
		filterAge = $(".filter-age input");
		filterSex = $(".filter-sex input");
		filterInterest = $(".filter-interest");
		filterIncClub = $(".filter-incClub");
		filterExcClub = $(".filter-excClub");
		filterExcCampaign = $(".filter-excludedCampaign");
		filterCity = $(".filter-city");
		brand = $("#brand");
		newBrand = $("#brandName").parent().parent();
		commission = $("#commissionRate");
		loadBtn = $("#btn-load-publisher");
		publishers = $(".div-publishers");


		//optype
		$( '#opType' ).val( '' );
		$( '#opType' ).children( ':nth-child(3)' ).prop( 'disabled', true );



		//bind to lbleffect
		$( "#lbl_effectType" ).parent().attr( "data-campaign", "camp_1" );

		//Hide new brand
		newBrand.hide();

		//Bind advertiser change event for brand list
		if (advertiser.length){
			advertiser.change(function(){
				var html = '<option value="">Loading...</option>';
				brand.html(html);
				if (advertiser.val()){
					$.get("/ds_advertiserBrands/" + advertiser.val(),function(data){
						html = '<option value="">Choose</option>';
						brand.html(html);
						addOtherOption(brand);
						html = brand.html();
						$.each(data,function(k,v){
							html += '<option value="' + k + '">' + v + '</option>';
						});
						brand.html(html);
					},"json");
				}
				else {
					brand.html(html);
				}
				var option = $(this).find("option:selected");
				$("#commissionRate").val(option.attr("commissionrate"));
				cpcPrice = parseFloat(option.attr("cpccost") * cpcMultiplier * (100/(100-getCommissionRate())));
				cpcPubPrice = option.attr("cpccost");
				calculateCPCPrice();
				blockUnblock();
			});
		}

		//No need publisher type for CPC campaigns
		$("input[name='race']").change(function(){
			toggleRace();
			blockUnblock();
		});

		//new brand toggle
		brand.change(function(){
			if (brand.val() == -1){
				newBrand.slideDown();
			} else {
				newBrand.slideUp();
			}
		});

		$('select#layout' ).change( function () {
			var sel = $( this );
			$( '.cplLandingLink' ).remove();
			$.get( '/ds_xhr/landingPageDetails/' + $( this ).val(), function ( data ) {
				data = JSON.parse( data );
				sel.parent().append( '<a href="'+cplLandingPage+'/'+data.preHash+'" class="cplLandingLink" target="_blank">'+cplLandingPage+'/'+data.preHash+'</a>' );
			} );
		});

		//When filter values change, recalculate the stats
		$(".demographic-filter").change(function(){buildFilter();});
		$(".demographical-filter button").click(function(){buildFilter();});
		$("#box1Viewinterests,#box2Viewinterests").dblclick(function(){buildFilter();});
		$("#box1ViewincClubs,#box2ViewincClubs").dblclick(function(){buildFilter();});
		$("#box1ViewexcClubs,#box2ViewexcClubs").dblclick(function(){buildFilter();});
		$("#box1ViewexcludedCampaign,#box2ViewexcludedCampaign").dblclick(function(){buildFilter();});
		$("#box1Viewcity,#box2Viewcity").dblclick(function(){buildFilter();});

		//budget calculation for effect campaign
		$(".budget-effect").change(function(){
			calculateEffect();
		});
		//budget calculation for click campaign
		$(".budget-click").change(function(){
			calculateCPCPrice();
			calculateClick("from");
		});
		//budget calculation for cpl campaign
		$(".budget-cpl").change(function(){
			calculateCPLPrice();
			calculateCPL("from");
		});

		$("#maxClick").change(function(){
			calculateClick("to");
		});
		$("#maxLeadLimit").change(function(){
			calculateCPL("to");
		});

		$("#cpcPrice").change(function(){
			cpcPubPrice = $(this ).val();
			calculateCPCRatio();
			calculateCPCPrice();
		});

		$("#perAdvertiserClick").change(function(){
			cpcPrice = $(this).val();
			calculateCPCRatio();

			calculateClick("from");
			calculateCPCPrice();
		});

		$("#perLead").change(function(){
			cplPrice = $(this).val();
			calculateCPLPrice();

			calculateCPL("from");
		});

		$("#perAdvertiserLead").change(function(){
			calculateCPLPrice();
			calculateCPL("from");
		});


		$("#link").change(function(){

			if ($(this).val().length){
				$("textarea.limited.camp-message").attr("data-limit",campaignCharLimitWithLink).data("limit",campaignCharLimitWithLink);
			} else {
				$("textarea.limited.camp-message").attr("data-limit",campaignCharLimitWithoutLink).data("limit",campaignCharLimitWithoutLink);
			}
			FormComponents.relimit();
		});


		commission.change(function(){
			switch (getRace()){
				case "1":
					buildFilter();
					break;
				case "3":
					calculateCPCPrice();
					calculateClick("from");
					break;
			}
		});

		calculateCPCPrice();
		calculateCPLPrice();
		calculateCPCRatio();
		blockUnblock();
		bindBtn();

		$("#perAdvertiserClick").val(cpcPrice);
		$("#cpcPrice").val(cpcPubPrice);

//		$("input[name='race']").change();

		//if this is campaign edit screen
		if (gotValue){
			$("#lbl_effectType").parent().hide();
			setTimeout(function(){
				switch (getRace()){
					case "1":
					{/literal}{if $_item->race == 1}{literal}
						effectData = {
							"publisher" : {/literal}{$_item->publisherCount}{literal},
							"follower" : {/literal}{$_item->campaign_campaignTwitter->follower}{literal},
							"influence" : {/literal}{$_item->campaign_campaignTwitter->influencer}{literal},
							"price" : {/literal}{$_item->campaign_campaignTwitter->maxCost}{literal},
						};
					{/literal}{/if}{literal}
						calculateEffect();
						break;
					case "3":
						calculateClick("from");
						break;
				}
				if (campaignState > 3){
					admInputs.attr("disabled",true);

					if( getRace()==6 ){
						$('#affService' ).change();
						$('#opType').val($('#opType').find('option[selected]').val())
						$( '#affService,#opType' ).attr( "disabled", true );
					}

				}
			},200);
		}
		else{
			$( "input[name='race']:first" ).parents('.radio-inline:first').click();
		}

		if (!gotValue)
			toggleRace();
		else
			toggleRace(false);

		loadImageCropper();

		$("#btn-add-message").click(function(){
			var t = $(this);
			var messageHTML = $("#message-1").parent().parent();
			var messageBox = messageHTML.parent();
			messageHTML = messageHTML.clone();
			var label = messageHTML.find("label.control-label");
			var textArea = messageHTML.find("textarea");
			var mNo = messageBox.find("div.form-group").length;
			label.attr("for","message-"+mNo);
			label.html("Message " + mNo+'<br/><a href="javascript:;" class="btn-remove-msg">Remove</a>');
			textArea.attr("name","message["+mNo+"]");
			textArea.attr("id","message-"+mNo);
			textArea.html("");
			t.before(messageHTML);
			FormComponents.relimit();
		});

		$(document).on("click",".btn-remove-msg",function(){
			var t = $(this);
			bootbox.confirm("Are you sure? This action can't be undone.", function(confirmed) {
				if (confirmed){
					t.parent().parent().remove();
				}
			});
		});
	});

	var one;

	oFReader = new FileReader(), rFilter = /^(?:image\/gif|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
	function loadImageFile() {

		if(document.getElementById("uploadfile").files.length === 0) return

		var oFile = document.getElementById("uploadfile").files[0];

		if(!rFilter.test(oFile.type)) {
			return;
		}

		oFReader.readAsDataURL(oFile);
	}

	oFReader.onload = function (oFREvent) {

		$('.example').html('<div class="default"><div class="cropMain"></div><div class="cropSlider"></div><button id="cropButton" class="btn blue">Save</button> &nbsp;<button class="btn" id="btnCancel">Cancel</button></div>');

		// create new object crop
		// you may change the "one" variable to anything
		one = new CROP();

		// link the .default class to the crop function
		one.init('.default');

		// load image into crop
		one.loadImg(oFREvent.target.result);

	};

	var loadImageCropper = function(){
		one = new CROP();
		one.init('.default');

		$('body').on("click", "button#cropButton", function() {
			$.ajax({
				type: "post",
				dataType: "json",
				url: "/ds_campaignUploadTempCampPicture",
				data: $.param(coordinates(one))
			})
					.done(function(data) {
						$('.example').html('<img src="' + data.url + '" border="0" />');
					});
			return false;
		});

		$('body').on("click", ".newupload", function() {
			$('.uploadfile').click();
		});

		$('body').change(".uploadfile", function() {
			loadImageFile();
			$('.uploadfile').wrap('<form>').closest('form').get(0).reset();
			$('.uploadfile').unwrap();

		});

		$("body").on("click","button#btnCancel",function(){
			$('.example').html("");
			return false;
		});

	};

	{/literal}
</script>

</body>
</html>