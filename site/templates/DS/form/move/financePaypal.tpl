{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
						      method="POST" target="ps">
							{getFormToken f=$_job}
							<div class="widget-content">

								{formBox type="checkbox" label="Enable publisher paypal" info="let publisher insert paypal account and request money" name="publisherPayPal" value=$ADM_SETTINGS.publisherPayPal}
								{formBox type="checkbox" label="Enable advertiser paypal" info="let advertisers pay with paypal" name="publisherPayPal" value=$ADM_SETTINGS.publisherPayPal}
								{formBox type="text" label="BUSINESS ID" name="PAYPAL_BUSINESS_ID" value="Problem with business ID"}
								{formBox type="text" label="AMOUNT FEE" name="PAYPAL_FEE_AMOUNT" value=$ADM_SETTINGS.PAYPAL_FEE_AMOUNT}
								{formBox type="text" label="TRANSACTION FEE" name="PAYPAL_TRANSACTION_FEE" value=$ADM_SETTINGS.PAYPAL_TRANSACTION_FEE}
								{formBox type="text" label="CURRENCY" name="PAYPAL_CURRENCY_CODE" value=$ADM_SETTINGS.PAYPAL_CURRENCY_CODE}
								{formBox type="text" label="REDIRECT URL" name="PAYPAL_REDIRECT_URL" value=$ADM_SETTINGS.PAYPAL_REDIRECT_URL}
								{formBox type="text" label="USERNAME" name="PAYPAL_USERNAME" value=$ADM_SETTINGS.PAYPAL_USERNAME}
								{formBox type="text" label="PASSWORD" name="PAYPAL_PASSWORD" value=$ADM_SETTINGS.PAYPAL_PASSWORD}
								{formBox type="text" label="SIGNATURE" name="PAYPAL_SIGNATURE" value=$ADM_SETTINGS.PAYPAL_SIGNATURE}
								{formBox type="text" label="API URL" name="PAYPAL_API_URL" value=$ADM_SETTINGS.PAYPAL_API_URL}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

<script type="text/javascript">
	{literal}
		$(document).ready(function(){

			$( "#spinner_cpcp" ).spinner({
			min: 0,
			step: 1
			});


			$( ".spinner_365" ).spinner({
				min: 0,
				max:365,
				step: 1
			});
	});
	{/literal}
</script>

</body>
</html>