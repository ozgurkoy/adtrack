{readMenu}
{readPerms}
{assign var="actionLabel" value="Add New Field"}
{if $_item->gotValue}
	{assign var="actionLabel" value="Edit Field"}
{/if}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function(){
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_cplFields/`$form->id`,CPL Form Fields of `$form->label`;,$actionLabel" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$actionLabel} CPL Form Field
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}" method="POST" target="ps">
								{if $_item->gotValue}
									<input type="hidden" name="id" value="{$_item->id}"/>
								{/if}
								<input type="hidden" name="fid" value="{$form->id}">

								{getFormToken f="CplField"}

								{formBox type="text" label="Label" name="label" value=$_item->label}
								{if strlen($_item->pfield)>0}
									<input type="hidden" name="def" value="{$_item->pfield}">

								{/if}
								{formBox type="textarea" label="Info" name="info" value=$_item->info info="The info for field <span style='float:right'> <a href='javascript:;' class='fmultil'>Multi-lang</a> </span>"}

								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Field Type</label>
									<div class="col-md-10">
										<select name="field" id="field" class="form-control">
											<option value="">Please select field type</option>
											{while $fields->populate()}
												<option value="{$fields->type}" {if $_item->field==$fields->id}selected{/if}>{$fields->type}</option>
											{/while}
										</select>
									</div>
								</div>
								{formBox type="checkbox" label="Is Mandatory" name="isMandatory" value=$_item->isMandatory}
								{formBox type="checkbox" label="Is Unique" name="isUnique" value=$_item->isUnique}
								<div class="form-row" id="addNewBox" style="display:none"><a href="javascript:;">Add new option</a></div>
								<div class="form-group non soxx" id="optionSampleBox" style="position:relative;padding-top: 0 !important">
									<label class="col-md-2 control-label" for="input17">
										✢ Option :
									</label>
									<a href="javascript:;" class="removeOption" style="position: absolute; right: 19px; top: 4px; z-index: 100; color: red; font-size: 10px; border: 1px solid red; padding: 5px;">Delete</a>
									<div class="col-md-10">
										<input type="text" name="" id="" class="form-control">
									</div>
								</div>

								<div id="optionsBox" class="non">
								</div>
								<div class="form-row" style="display:none;width:50%;margin:auto" id="previewRow">
									<b>PREVIEW :</b> <br /><br /><br />
									<div id="previewCol"></div>
								</div>
								{*{formBox type="textarea" label="Sub HTML" name="subHtml" value=$_item->subHtml}*}

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}
{callUserFunc $fields->reload()}
<script>
	var fieldTypes = {literal}{}{/literal},
			selected=false;

	{while $fields->populate()}
	fieldTypes['{$fields->type}'] = {literal}{{/literal}
		html:'{$fields->html|trim}',
		subHtml:'{$fields->subHtml|trim}'
	{literal}};{/literal}
	{/while}
	{literal}

	var es = $('#label').typeahead({
		remote: '/ds_xhr/cplWord/%QUERY'
	});

	es.on('typeahead:selected',function(){
		selected = true;
		updatePreview();

	});


	{/literal}
	{literal}
	var ef=$('#optionsBox').sortable({ items: '.soxx' ,update:function(){
		updatePreview();
	}});

	$( '#addNewBox' ).find('a').click( function () {
		addOption();

	} );

	$(' #optionsBox' ).on('click','.removeOption',function(){

		$( this ).parents( '.soxx:first' ).remove();
		updatePreview();
	});

	$('#label' ).change(function(){
		updatePreview();

	});

	$('select#field' ).change( function () {
		var self = $( this ),
				ft;

		if ( ft = fieldTypes[self.val()] ) {
			if ( ft.html.length > 0 ) {
				updatePreview();
				if ( ft.subHtml ) {
					$( '#addNewBox' ).show();
				}
				else{
					$( '#addNewBox' ).hide();
					$( '#optionsBox' ).html( '' );
				}
			}
			else {
				//radio-select specifically
				$( '#addNewBox' ).show();
				updatePreview();
			}
		}
		else {
			$( '#previewCol' ).html( '' );
			$( '#previewRow' ).hide();
			$( '#addNewBox' ).hide();

		}
	});

	function addOption(value){
		if(typeof value == 'undefined')
			value = '';

		var cbox = $( '#optionSampleBox' ).clone();
		cbox.find( 'input' ).attr( 'name', 'options[]' ).val(value);

		var rnd = Math.floor( (Math.random() * 10000) + 40 );
		cbox.attr( 'id', 'option_' + rnd ).removeClass('non');
		$('#optionsBox' ).append( cbox ).removeClass('non');
		var cinp = cbox.find( 'input' );
		cinp.on('keydown',function(){
			updatePreview();
		});
		var qx = cinp.attr( 'id', 'option_' + rnd ).typeahead({
			remote: '/ds_xhr/cplWord/%QUERY'
		});


		qx.on('typeahead:selected',function(){
			updatePreview();
		});

	}

	$('.fmultil' ).click(function(){
		$( '.fmultil' ).hide();
		$('textarea#info' ).typeahead({
			remote: '/ds_xhr/cplWord/%QUERY'
		});
	});


	function updatePreview(){
//		if(selected !== true) return false;

		$.post('/ds_cplPreviewField/'+(Math.floor( (Math.random() * 10000) + 4000) ), $('form#CplField' ).serialize(), function(data){
			$( '#previewCol' ).html( data );
			$( '#previewRow' ).show();
		});
	}

	{/literal}
	{if $_item->gotValue}
	updatePreview();
	{if !is_null($_item->options)}
	{foreach from=$_item->options item=o}
	addOption( '{$o}' );
	{/foreach}
	updatePreview();

	{/if}
	{/if}
</script>
</body>
</html>