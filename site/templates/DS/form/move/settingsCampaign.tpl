{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
						      method="POST" target="ps">
							{getFormToken f=$_job}
							<div class="widget-content">
								{*
								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										CPC Campaign - advertisers settings All Settings are for Self service panel
									</h4>
								</div>

								{formBox type="checkbox" label="Enable CPC Campaign" info="Enable Advertiser to create CPC Campaign" name="ENABLE_MODULE_CPC" value=$ADM_SETTINGS.ENABLE_MODULE_CPC}

								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Minimum Campaign Price</label>
									<div class="col-sm-10">
										<input type="text" class="form-control " id="spinner_cpcp" name="cpcCampaignMinimumPrice" value={$ADM_SETTINGS.cpcCampaignMinimumPrice}>
											<span class="help-block">Advertiser minimum budget for CPC Campaign</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Minimum Days to StartDate</label>
									<div class="col-sm-10">
										<input type="text" class="form-control spinner_365" id="cpcCampaignMinStartDate" name="cpcCampaignMinStartDate" value={$ADM_SETTINGS.cpcCampaignMinStartDate}>
										<span class="help-block">The minimum days to create campaign from today for advertisers, 1= today</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Minimum Days</label>
									<div class="col-sm-10">
										<input type="text" class="form-control spinner_365 " id="cpcCampaignMinDays" name="cpcCampaignMinDays" value={$ADM_SETTINGS.cpcCampaignMinDays}>
										<span class="help-block">Minimum days that the campaign will be active</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Maximum Days</label>
									<div class="col-sm-10">
										<input type="text" class="form-control spinner_365 " id="cpcCampaignMaxDays" name="cpcCampaignMaxDays" value={$ADM_SETTINGS.cpcCampaignMaxDays}>
										<span class="help-block">Maximum days that the campaign will be active </span>
									</div>
								</div>

								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										Effect Campaign
									</h4>
								</div>

								{formBox type="checkbox" label="Enable CPC Campaign" info="Enable Advertiser to create Effect Campaign" name="ENABLE_MODULE_AWARENESS" value=$ADM_SETTINGS.ENABLE_MODULE_AWARENESS}

								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Minimum Campaign Price</label>
									<div class="col-sm-10">
										<input type="text" class="form-control " id="spinner_cpcp" name="campaignMinimumPrice" value={$ADM_SETTINGS.campaignMinimumPrice}>
										<span class="help-block">Advertiser minimum budget for Effect Campaign</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Minimum Days to StartDate</label>
									<div class="col-sm-10">
										<input type="text" class="form-control spinner_365" id="campaignMinStartDate" name="campaignMinStartDate" value={$ADM_SETTINGS.campaignMinStartDate}>
										<span class="help-block">The time that the publishers can accept the campaign</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Minimum Days</label>
									<div class="col-sm-10">
										<input type="text" class="form-control spinner_365" id="campaignMinDays" name="campaignMinDays" value={$ADM_SETTINGS.campaignMinDays}>
										<span class="help-block">Minimum days that the campaign will be active</span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" for="input17">Maximum Days</label>
									<div class="col-sm-10">
										<input type="text" class="form-control spinner_365" id="campaignMaxDays" name="campaignMaxDays" value={$ADM_SETTINGS.campaignMaxDays}>
										<span class="help-block">Maximum days that the campaign will be active</span>
									</div>
								</div>


								<div class="widget-header">
									<h4><i class="icon-reorder"></i>
										General Settings
									</h4>
								</div>
								{formBox type="text" label="Defualt unrefferal clicks" name="unrefferalClick" info="The defualt number for unrefferal clicks on CPC" value=$ADM_SETTINGS.campaignHideCPCLink}
								*}

								{formBox type="checkbox" label="Hide CPC Link on Offer Page" name="campaignHideCPCLink" value=$ADM_SETTINGS.campaignHideCPCLink}


								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

<script type="text/javascript">
	{literal}
		$(document).ready(function(){

			$( "#spinner_cpcp" ).spinner({
			min: 0,
			step: 1
			});


			$( ".spinner_365" ).spinner({
				min: 0,
				max:365,
				step: 1
			});
	});
	{/literal}
</script>

</body>
</html>