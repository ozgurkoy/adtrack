{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
						      method="POST" target="ps">
							{getFormToken f=$_job}
							<div class="widget-content">


								{formBox type="checkbox" label="Display Publisher Tax" info="Display To publisher the amount of tax reduce from profit" name="publisherPayPal" value="0"}
								{formBox type="text" label="Publisher Tax" name="PROFIT_MULTIPLIER" value=""}

								{formBox type="text" label="Effect Profit Multiplier" name="PROFIT_MULTIPLIER" value=$ADM_SETTINGS.PROFIT_MULTIPLIER}
								{formBox type="text" label="Minimum Effect message cost" name="COUNTRY_MIN_COST" value=$ADM_SETTINGS.COUNTRY_MIN_COST}
								{formBox type="text" label="Country Cost Multiplier" info="adesments from the Calc API" name="COUNTRY_COST_MULTIPLIER" value=$ADM_SETTINGS.COUNTRY_COST_MULTIPLIER}
								{formBox type="text" label="Publisher Campaign Display Hour Difference" info="Hour Difference between regular publisher and trendcenters" name="publisherCampaignDisplayHourDifference" value=$ADM_SETTINGS.publisherCampaignDisplayHourDifference}

								{formBox type="text" label="default CPC cost for publisher" name="CPC_COST" value=$ADM_SETTINGS.CPC_COST}
								{formBox type="text" label="CPC Profit Multiplier" name="CPC_MULTIPLIER" value=$ADM_SETTINGS.CPC_MULTIPLIER}

								{formBox type="text" label="Publisher Minimum Transfer" name="publisherMinimumTransfer" value=$ADM_SETTINGS.publisherMinimumTransfer}
								{formBox type="text" label="Publisher Maximum Transfer" name="publisherMaximumTransfer" value=$ADM_SETTINGS.publisherMaximumTransfer}


								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

<script type="text/javascript">
	{literal}
		$(document).ready(function(){

			$( "#spinner_cpcp" ).spinner({
			min: 0,
			step: 1
			});


			$( ".spinner_365" ).spinner({
				min: 0,
				max:365,
				step: 1
			});
	});
	{/literal}
</script>

</body>
</html>