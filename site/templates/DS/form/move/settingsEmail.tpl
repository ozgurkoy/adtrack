{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
						      method="POST" target="ps">
							{getFormToken f=$_job}
							<div class="widget-content">
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Sending Settings
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="Email From Address" name="EMAIL_FROM_ADDRESS" value=$ADM_SETTINGS.EMAIL_FROM_ADDRESS}
										{formBox type="text" label="Email From Name" name="EMAIL_FROM_NAME" value=$ADM_SETTINGS.EMAIL_FROM_NAME}
										{formBox type="text" label="Noreply Address" name="EMAIL_NOREPLY_ADDRESS" value=$ADM_SETTINGS.EMAIL_NOREPLY_ADDRESS}
										{formBox type="text" label="Mail Service URL" name="EMAIL_MANDRILL_API_URL" value=$ADM_SETTINGS.EMAIL_MANDRILL_API_URL}
										{formBox type="text" label="Mail Service key" name="EMAIL_MANDRILL_KEY" value=$ADM_SETTINGS.EMAIL_MANDRILL_KEY}
									</div>
								</div>
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Receiving Settings
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="Info" name="info" value=$SYSTEM_EMAILS.info}
										{formBox type="text" label="Pay" name="pay" value=$SYSTEM_EMAILS.pay}
										{formBox type="text" label="Sales" name="sales" value=$SYSTEM_EMAILS.sales}
										{formBox type="text" label="Support" name="support" value=$SYSTEM_EMAILS.support}
										{formBox type="text" label="Help" name="help" value=$SYSTEM_EMAILS.help}
										{formBox type="text" label="Weekly Report Receivers" name="REPORT_RECIPIENTS" value=$ADM_SETTINGS.REPORT_RECIPIENTS info="Use ; for multiple recipients"}
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>


							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>