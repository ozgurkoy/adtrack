{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}" id="{$_job}"
						      method="POST" target="ps">
							{getFormToken f=$_job}
							<div class="widget-content">
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Short Link Server
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="API URL" name="SLSAPI" value=$ADM_SETTINGS.SL_SERVICE_URL}
										{formBox type="text" label="Country Code" name="SLSCountry" value=$ADM_SETTINGS.SL_COUNTRY_CODE}
										{formBox type="text" label="Campaign End URL" name="SLSCampEnd" value=$ADM_SETTINGS.SL_CAMPAIGN_END}
										{formBox type="text" label="Push Address" name="SLSPush" value=$ADM_SETTINGS.SL_PUSH_ADDRESS}
										{formBox type="text" label="Default Short Domain" name="SLSDomain" value=$ADM_SETTINGS.SHORTDOMAIN}
										{formBox type="textarea" label="Short Domain Options" name="SLSDomainOptions" value=$SLSDomainOptions info="1 domain per line"}
									</div>
								</div>
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Calculation Server
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="API URL" name="calcApi" value=$ADM_SETTINGS.CALCULATION_API_URL}
									</div>
								</div>
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Language Server
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="API URL" name="langApi" value=$ADM_SETTINGS.LANGMAN_API_URL}
									</div>
								</div>
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Tweet Gator
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="API URL" name="tgApi" value=$ADM_SETTINGS.TG_SERVICE_URL}
									</div>
								</div>
								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>


							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- /.row -->
			<!-- /Page Content -->
		</div>
		<!-- /.container -->

	</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>