{readMenu}
{readPerms}
<!DOCTYPE html>

<html lang="en">
<head>
	{include file="DS/_top.tpl"}
	<script>
		{literal}
		$(document).ready(function () {
			"use strict";

			App.init(); // Init layout and core plugins
			Plugins.init(); // Init all plugins
			FormComponents.init(); // Init all form-specific plugins
		});
		{/literal}
	</script>

</head>

<body>

<!-- Header -->
<header class="header navbar navbar-fixed-top" role="banner">
	{include file="DS/_header.tpl"}
</header>
<!-- /.header -->

<div id="container">
	<div id="sidebar" class="sidebar-fixed">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">
				<!--=== Navigation ===-->
				{include file="DS/_nav.tpl" menus=$menus}
				<!-- /Navigation -->

			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<div id="divider" class="resizeable"></div>
	</div>
	<div id="content">
		<div class="container">
			<!-- Breadcrumbs line -->
			{breadCrumb left="ds_ServicesSettings,Services Settings" }
			<!-- /Breadcrumbs line -->

			<!--=== Page Content ===-->
			<div class="row">
				<div class="col-md-12">
					<div class="widget box">
						<div class="widget-header">
							<h4><i class="icon-reorder"></i>
								{$_job|ucfirst}
							</h4>
						</div>
						<div class="widget-content">
							<form class="form-horizontal row-border" action="/ds_{$_job}Action" name="{$_job}"
							      id="{$_job}"
							      method="POST" target="ps">

								{getFormToken f=$_job}
								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Site Mode
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="arrayKVSelect" label="Invite Mode" name="sitemode" options=$siteModeArr value="$siteMode"}
									</div>
								</div>

								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Mobile Options Mode
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="checkbox" label="Enable Mobile Screen" name="ENABLE_MOBILE" value=$ADM_SETTINGS.ENABLE_MOBILE info="Preview screen to select mobile site or mobile app"}

										{formBox type="text" label="Iphone app store URL" name="IPHONE_URL" value=$ADM_SETTINGS.IPHONE_URL}
										{formBox type="text" label="Iphone app ID" name="IPHONE_APP_ID" value=$ADM_SETTINGS.IPHONE_APP_ID}
										{formBox type="text" label="Android app store URL" name="ANDROID_URL" value=$ADM_SETTINGS.ANDROID_URL}
										{formBox type="text" label="Android app ID" name="ANDROID_APP_ID" value=$ADM_SETTINGS.ANDROID_APP_ID}
									</div>
								</div>
								{*formBox type="arrayKVSelect" label="Country" name="country" options=$cArray value="$cCode"*}
								<div class="widget box">

									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Site Support
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="checkbox" label="Use Zendesk" name="ZENDESK" value=$ADM_SETTINGS.ZENDESK}
										{formBox type="textarea" label="Zendex Code" name="ZENDESK_CODE" value=$ADM_SETTINGS.ZENDESK_CODE}
										{formBox type="text" label="Zendesk API Key" name="ZENDESK_API" value=$ADM_SETTINGS.ZENDESK_API}

									</div>
								</div>

								<div class="widget box">
									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Site Analitycs
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="text" label="Google analytics ID" name="GOOGLE_ANALYTICS" value=$ADM_SETTINGS.GOOGLE_ANALYTICS}
										{formBox type="textarea" label="Other Analytics" name="ANALYTICS_CODE" value=$ADM_SETTINGS.ANALYTICS_CODE info="The code will be in JS script"}
									</div>
								</div>

								<div class="widget box">

									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											adMingle Store
										</h4>
									</div>
									<div class="widget-content">
										{formBox type="checkbox" label="Enable Store" name="ENABLE_STORE" value=$ADM_SETTINGS.ENABLE_STORE}

									</div>
								</div>

								<div class="widget box">

									<div class="widget-header">
										<h4><i class="icon-reorder"></i>
											Facebook Like Box
										</h4>
									</div>
									<div class="widget-content">
											{formBox type="text" label="Facebook Like Box on Signup Page" name="FACEBOOK_LIKE_BOX" value=$ADM_SETTINGS.FACEBOOK_LIKE_BOX info="https://www.facebook.com/PAGE NAME , Leave blank to remove from page"}
									</div>
								</div>

								<div class="form-actions">
									<button type="submit" class="submit btn btn-primary pull-right">
										Submit <i class="icon-angle-right"></i>
									</button>
								</div>

						</div>
						</form>

					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
		<!-- /Page Content -->
	</div>
	<!-- /.container -->

</div>
</div>

{include file="DS/footer.tpl"}

</body>
</html>