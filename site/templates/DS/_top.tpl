{*<meta charset="utf-8" />*}
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<title>adMingle Track</title>

<!--=== CSS ===-->

<!-- Bootstrap -->
<link href="/site/layout/DS/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<!-- jQuery UI -->
<!--<link href="/site/layout/DS/plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="/site/layout/DS/plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
<![endif]-->

<!-- Theme -->
<link href="/site/layout/DS/assets/css/main.css" rel="stylesheet" type="text/css" />
<link href="/site/layout/DS/assets/css/plugins.css" rel="stylesheet" type="text/css" />
<link href="/site/layout/DS/assets/css/responsive.css" rel="stylesheet" type="text/css" />
<link href="/site/layout/DS/assets/css/icons.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="/site/layout/DS/assets/css/fontawesome/font-awesome.css">
<!--[if IE 7]>
<link rel="stylesheet" href="/site/layout/DS/assets/css/fontawesome/font-awesome-ie7.css">
<![endif]-->

<!--[if IE 8]>
<link href="/site/layout/DS/assets/css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href="/site/layout/DS/css/custom.css" rel="stylesheet" type="text/css" />

<!--=== JavaScript ===-->

<script type="text/javascript" src="/site/layout/DS/assets/js/libs/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

<script type="text/javascript" src="/site/layout/DS/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/assets/js/libs/lodash.compat.min.js"></script>

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="/site/layout/DS/assets/js/libs/html5shiv.js"></script>
<![endif]-->

<!-- Smartphone Touch Events -->
<script type="text/javascript" src="/site/layout/DS/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/event.swipe/jquery.event.move.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/event.swipe/jquery.event.swipe.js"></script>

<!-- General -->
<script type="text/javascript" src="/site/layout/DS/assets/js/libs/breakpoints.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
<script type="text/javascript" src="/site/layout/DS/plugins/cookie/jquery.cookie.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

<!-- Page specific plugins -->
<!-- Charts -->
<!--[if lt IE 9]>
<script type="text/javascript" src="/site/layout/DS/plugins/flot/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/site/layout/DS/plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/flot/jquery.flot.growraf.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/tagsinput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/typeahead/typeahead.min.js"></script> <!-- AutoComplete -->
<script type="text/javascript" src="/site/layout/DS/plugins/inputlimiter/jquery.inputlimiter.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/fileinput/fileinput.js"></script>

<script type="text/javascript" src="/site/layout/DS/plugins/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/blockui/jquery.blockUI.min.js"></script>

<script type="text/javascript" src="/site/layout/DS/plugins/fullcalendar/fullcalendar.min.js"></script>

<!-- Pickers -->
<script type="text/javascript" src="/site/layout/DS/plugins/pickadate/picker.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/pickadate/picker.date.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/pickadate/picker.time.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Noty -->
<script type="text/javascript" src="/site/layout/DS/plugins/noty/jquery.noty.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/noty/layouts/top.js"></script>
<!-- Forms -->
<script type="text/javascript" src="/site/layout/DS/plugins/uniform/jquery.uniform.min.js"></script>

<script type="text/javascript" src="/site/layout/DS/plugins/noty/themes/default.js"></script>

<!-- DataTables -->
<script type="text/javascript" src="/site/layout/DS/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/datatables/jquery.dataTables.columnFilter.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
<script type="text/javascript" src="/site/layout/DS/plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
<script type="text/javascript" src="/site/layout/DS/plugins/datatables/DT_bootstrap.js"></script>

<!-- Bootbox -->
<script type="text/javascript" src="/site/layout/DS/plugins/bootbox/bootbox.js"></script>

<!-- App -->
<script type="text/javascript" src="/site/layout/DS/assets/js/app.js"></script>
<script type="text/javascript" src="/site/layout/DS/assets/js/plugins.js"></script>
<script type="text/javascript" src="/site/layout/DS/assets/js/plugins.form-components.js"></script>
<!-- Demo JS -->
<script type="text/javascript" src="/site/layout/DS/plugins/bootstrap-multiselect/bootstrap-multiselect.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/bootstrap-inputmask/jquery.inputmask.js"></script>

<!-- Form Validation -->
<script type="text/javascript" src="/site/layout/DS/plugins/validation/jquery.validate.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/plugins/duallistbox/jquery.duallistbox.min.js"></script>
<script type="text/javascript" src="/site/layout/DS/assets/js/custom.js"></script>
<script type="text/javascript" src="/site/layout/DS/js/scrollTo.js"></script>

<script type="text/javascript">
	var SITEROOT = '{$SITEROOT}';
</script>

<!-- adMingle Functions -->
<script type="text/javascript" src="/site/layout/DS/js/main.js"></script>