Hello {$username},<br/><br/>
{if $title=="NEW"}
There is a new entry on admingle.track #{$bug->history->id} - {$bug->history->title}<br />
{else}
There has been a new development on admingle.track #{$bug->history->id} - {$bug->history->title} <br />
Details are on <a href="http://{$SITEROOT}/ds_bug?bugId={$bugId}">{$SITEROOT}/ds_bug?bugId={$bugId}</a>
{/if}
on {timestampToDate d=$ddate} by @{$bug->bugUser->username} <br />
<hr>
{$bug->details|nl2br}
{if strlen($link)>0 &&  $title=="NEW"}
<br>
Related Link : <a href="{$link}">{$link}</a>
<hr>
{/if}