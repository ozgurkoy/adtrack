{if is_string($__options)}
	{createObjectFromName object=$__options name="OPS"}
	{callUserFunc $OPS->populateOnce(false)->load()}
{else}
	{assign var="OPS" value=$__options}
{/if}
<div class="form-group" {if isset($__blockCustomAttr)}{$__blockCustomAttr}{/if}>
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<select name="{$__name}" id="{$__name}" class="form-control {$__class}" {if !is_null($__defaultValue)}defaultValue="{$__defaultValue}"{/if}>
			<option value="">{if !is_null($__defaultValue)}{$__defaultValue}{else}Choose{/if}</option>
			{if is_object($OPS) && $OPS->gotValue}
				{doWhile}
					<option value="{$OPS->id}" {if !empty($__attributes)}{foreach from=$__attributes item=vv}{if isset($OPS->$vv)} {$vv}="{$OPS->$vv}"{/if}{/foreach}{/if} {if !is_null($__value) && $__value->id==$OPS->id}selected{/if}>
						{foreach from=$__vkey item=vv}
							{if property_exists($OPS,$vv)}
								{$OPS->$vv}
							{/if}
						{/foreach}
					</option>
				{/doWhile ($OPS->populate())}
			{/if}
		</select>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
{if !is_null($__attached) && !is_null($__src)}
	<script>
		$('select#{$__attached}' ).change(function(){literal}{{/literal}
			populateSelectByXHR('{$__name}','{$__src}/'+$('select#{$__attached}' ).val(),'{$__value->id}');
			{literal}
		});
		{/literal}
		if($('select#{$__attached}').find("option:selected").val().length>0)
			$( 'select#{$__attached}' ).change();

	</script>
{/if}
