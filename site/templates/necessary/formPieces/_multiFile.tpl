<div class="form-group errorContext">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<input type="file" name="{$__name}" id="{$__name}" data-style="fileinput" {if !empty($__class)}class="{$__class}"{/if} {if !empty($__customAttribute)}{$__customAttribute}="{$__attributeValue}"{/if} {if !empty($__value) && !empty($__disabled)}disabled{/if}>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
		{if isset($__value) && strlen($__value)>0}
			<img src="/{$__value}" style="width:60px">
		{/if}
	</div>
</div>