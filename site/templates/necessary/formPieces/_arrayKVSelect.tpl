
<div class="form-group" {if isset($__blockCustomAttr)}{$__blockCustomAttr}{/if}>
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<select name="{$__name}" id="{$__name}" class="form-control {$__class}" defaultValue="{if !is_null($__defaultValue)}{$__defaultValue}{else}Choose{/if}">
			<option value="">{if !is_null($__defaultValue)}{$__defaultValue}{else}Choose{/if}</option>
			{foreach from=$__options key=op item=opv}
				<option value="{$op}"  {if $__value==$op}selected{/if}>{$opv}</option>
			{/foreach}
		</select>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
{if !is_null($__attached) && !is_null($__src)}
	<script>
		$('select#{$__attached}' ).change(function(){literal}{{/literal}
			populateSelectByXHR('{$__name}','{$__src}/'+$('select#{$__attached}' ).val(),'{$__value}');
		{literal}
		});
		{/literal}
		if($('select#{$__attached}').find("option:selected").val().length>0)
			$( 'select#{$__attached}' ).change();

	</script>
{/if}