<div class="form-group">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>

	<div class="col-md-10">
		<input type="checkbox" class="uniform form-control" name="{$__name}" value="1" {if $__value==1}checked{/if}>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}

	</div>

</div>