<div class="form-group" {if isset($__blockCustomAttr)}{$__blockCustomAttr}{/if}>
	<label class="col-md-2 control-label" for="{$__inputID}">{$__label}</label>
	<div class="col-md-10">
		<textarea name="{$__name}" id="{$__inputID}" class="form-control" {if !empty($__customAttribute)}{$__customAttribute}="{$__attributeValue}"{/if} >{$__value}</textarea>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
