<div class="form-group errorContext">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<input type="file" name="{$__name}" id="{$__name}" data-style="fileinput" {if !empty($__class)}class="{$__class}"{/if} {if !empty($__customAttribute)}{$__customAttribute}="{$__attributeValue}"{/if} {if !empty($__value) && !empty($__disabled)}disabled{/if}>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
		{if isset($__value) && strlen($__value)>0}
			<img src="/{$__value}" style="width:60px">
		{/if}
		{if isset($__multi)}
			<a href="javascript:;" onclick="multipleFileInput(this,'{$__name}')">[+]</a>
			<script>
				{literal}
				if(typeof(multipleFileInput)=='undefined'){
					multipleFileInput = function(n,name){
						var f = $( n ).parents( '.form-group:first' ),
								g = f.clone( false );
						g.find('.col-md-10' ).html('<input type="file" data-style="fileinput"  name="'+name+'" id="'+name+'" >')
						f.after(g)
						FormComponents.init();
					}
				}
				{/literal}
			</script>
		{/if}

	</div>
</div>