{if is_string($__options)}
	{createObjectFromName object=$__options name="OPS"}
	{callUserFunc $OPS->populateOnce(false)->load()}
{else}
	{assign var="OPS" value=$__options}
{/if}
<div class="form-group">
	<div class="widget box">
		<div class="widget-header">
			<h4><i class="icon-reorder"></i> {$__label}</h4>
		</div>
		<div class="widget-content clearfix">
			<!-- Left box -->
			<div class="left-box">
				<input type="text" id="box1Filter{$__name}" class="form-control box-filter" placeholder="Search..."><button type="button" id="box1Clear{$__name}" class="filter">x</button>
				<select id="box1View{$__name}" multiple="multiple" class="multiple">
					{if $OPS && $OPS->gotValue}
						{doWhile}
						{if !is_object($__value) || (is_object($__value)  && !in_array($OPS->id, $__value->_ids))}
							<option value="{$OPS->id}" >
								{foreach from=$__vkey item=vv}
									{if isset($OPS->$vv)}
										{$OPS->$vv}
									{/if}
								{/foreach}
							</option>
						{/if}
						{/doWhile ($OPS->populate())}
					{/if}
				</select>
				<span id="box1Counter{$__name}" class="count-label"></span>
				<select id="box1Storage{$__name}"></select>
			</div>
			<!--left-box -->

			<!-- Control buttons -->
			<div class="dual-control">
				<button id="to2{$__name}" type="button" class="btn">&nbsp;&gt;&nbsp;</button>
				<button id="allTo2{$__name}" type="button" class="btn">&nbsp;&gt;&gt;&nbsp;</button><br>
				<button id="to1{$__name}" type="button" class="btn">&nbsp;&lt;&nbsp;</button>
				<button id="allTo1{$__name}" type="button" class="btn">&nbsp;&lt;&lt;&nbsp;</button>
			</div>
			<!--control buttons -->

			<!-- Right box -->
			<div class="right-box">
				<input type="text" id="box2Filter{$__name}" class="form-control box-filter" placeholder="Filter entries...">
				<button type="button" id="box2Clear{$__name}" class="filter">x</button>
				<select id="box2View{$__name}" name="{$__name}[]" multiple="multiple" class="multiple">
					{if $__value && $__value->gotValue}
						{doWhile}
							<option value="{$__value->id}"  >
								{foreach from=$__vkey item=vv}
									{if isset($__value->$vv)}
										{$__value->$vv}
									{/if}
								{/foreach}
							</option>
						{/doWhile ($__value->populate())}
					{/if}
				</select>
				<span id="box2Counter{$__name}" class="count-label"></span>
				<select id="box2Storage{$__name}"></select>
			</div>
			<!--right box -->

		</div>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}

	</div>
</div>

<script>
	var box1View = 'box1View{$__name}';
	var box1Counter = 'box1Counter{$__name}';
	var box1Filter = 'box1Filter{$__name}';
	var box1Storage = 'box1Storage{$__name}';
	var box1Clear = 'box1Clear{$__name}';
	var box2View = 'box2View{$__name}';
	var box2Counter = 'box2Counter{$__name}';
	var box2Filter = 'box2Filter{$__name}';
	var box2Storage = 'box2Storage{$__name}';
	var box2Clear = 'box2Clear{$__name}';
	var to1 = 'to1{$__name}';
	var to2 = 'to2{$__name}';
	var allTo1 = 'allTo1{$__name}';
	var allTo2 = 'allTo2{$__name}';

	{literal}
	var opts = {
		box1View       : box1View,
		box1Storage    : box1Storage,
		box1Filter     : box1Filter,
		box1Clear      : box1Clear,
		box1Counter    : box1Counter,
		box2View       : box2View,
		box2Storage    : box2Storage,
		box2Filter     : box2Filter,
		box2Clear      : box2Clear,
		box2Counter    : box2Counter,
		to1            : to1,
		allTo1         : allTo1,
		to2            : to2,
		allTo2         : allTo2,
		transferMode   : 'move',
		sortBy         : 'text',
		useFilters     : true,
		useCounters    : true,
		useSorting     : true,
		selectOnSubmit : true
	};

	$.configureBoxes( opts );

	{/literal}
</script>