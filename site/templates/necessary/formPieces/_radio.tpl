<div class="form-group errorContext">
	<label class="col-md-2 control-label" for="{$__name}" id="lbl_{$__name}">{$__label}</label>

	<div class="col-md-10">
		{foreach from=$__options key=op item=opv}
			<label class="{if isset($__class)}{$__class}{else}radio{/if}">
			<input type="radio" class="uniform" name="{$__name}" value="{$op}" {if $__value==$op}checked{/if} {if !empty($__value) && !empty($__disabled)}disabled{/if}>
			{$opv}
			</label>
		{/foreach}
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}

	</div>

</div>
