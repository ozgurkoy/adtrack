<div class="form-group" {if isset($__blockCustomAttr)}{$__blockCustomAttr}{/if}>
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<select name="{$__name}" id="{$__name}" class="form-control {$__class}" {if !is_null($__defaultValue)}defaultValue="{$__defaultValue}"{/if} {if !empty($__customAttribute)}{$__customAttribute}="{$__attributeValue}"{/if} {if !is_null($__src)}src="{$__src}"{/if}>
			<option value="">{if !is_null($__defaultValue)}{$__defaultValue}{else}Choose{/if}</option>
			{foreach from=$__options item=op}
			<option value="{$op}"  {if $__value==$op}selected{/if}>{$op}</option>
			{/foreach}
		</select>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>

{if !is_null($__attached) && !is_null($__src)}
	<script>
		$('#{$__attached}' ).change(function(){literal}{{/literal}
			//console.log('{$__name}  ','{$__src}/'+$('#{$__attached}' ).val());
			populateSelectByXHR('{$__name}  ','{$__src}/'+$('#{$__attached}' ).val(),'{$__value}');
			{literal}
		});
		{/literal}
		var vv = null;
		if($('#{$__attached}')[0].nodeName == 'select' )
			vv = $( '#{$__attached}' ).find( "option:selected" ).val();
		else
			vv = $('#{$__attached}').val();

		if(vv.length>0)
			$( '#{$__attached}' ).change();

	</script>
{/if}