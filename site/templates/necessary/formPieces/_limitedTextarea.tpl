<div class="form-group">
	<label class="col-md-2 control-label" for="{$__inputID}">{$__label}</label>
	<div class="col-md-10">
		<textarea name="{$__name}" id="{$__inputID}" class="limited form-control {$__class}" data-limit="{$__charLimit}">{$__value}</textarea>
		<span class="help-block limit-text"></span>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}
	</div>
</div>
