<div class="col-sm-6 col-md-3 {if !empty($__class)}{$__class}{/if}" id="{$__name}">
	<div class="statbox widget box box-shadow">
		<div class="widget-content">
			<div class="visual {$__color}">
				<i class="icon-{$__inputIcon}"></i>
			</div>
			<div class="title">{$__label}</div>
			<div class="value">{$__value}</div>
		</div>
	</div>
</div>