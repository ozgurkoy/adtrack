
<div class="form-group errorContext">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<div class="input-group">
			<input type="file" name="{$__name}" id="file_{$__name}" data-style="fileinput" {if !empty($__class)}class="{$__class}"{/if} {if !empty($__customAttribute)}{$__customAttribute}="{$__attributeValue}"{/if} {if !empty($__value) && !empty($__disabled)}disabled{/if}>
			<span class="input-group-btn">
					<button class="btn btn-default" id="uploadit{$__name}" type="button">Upload</button>
			</span>
		</div>
		<div class="progress progress-striped">
			<div class="progress-bar progress-bar-info" style="width: 0%"></div>
		</div>
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}

		<div class="list-group" id="imagePreviewDiv_{$__name}" hidden="true">
			<li class="input-group">
				<img id="imagePreview_{$__name}" src="" alt="Image Preview">
				<span class="input-group-btn">
				<button class="btn btn-default deleteImage{$__name}" type="button">delete</button>
				</span>
			</li>
		</div>
	</div>
</div>
<input type="hidden" name="{$__name}" id="{$__name}" value=""/>

<script>

	var fName = "#file_{$__name}";
	var uploadPath = "{$__uploadPath}";
	var controlName = "{$__name}";

	{literal}
	$(document).ready(function(){
	var slider = document.querySelector('.progress-bar');

	document.querySelector("#uploadit"+controlName).addEventListener('click', function(e) {

		var file = document.querySelector(fName).files[0];
		slider.style.width = "0%";

		var fd = new FormData();
		fd.append("afile", file);
		fd.append("t0ken", $("#t0ken").val());

		var xhr = new XMLHttpRequest();
		xhr.open('POST', '/'+uploadPath, true);

		xhr.upload.onprogress = function(e) {
			if (e.lengthComputable) {
				var percent = e.loaded / e.total;
				var percentComplete = (percent) * 100;
				slider.style.width = percentComplete;
			}
		};

		xhr.onload = function() {
			if (this.status == 200) {

				var resp = JSON.parse(this.response);
				if(resp.error == 0){
					slider.style.width = "100%";
					showImagePreview(resp.dataUrl);
					$("#"+controlName).val(resp.dataFileName);
				}else{
					alert(resp.errorText);
				}
			};
		};

		var showImagePreview = function(imagePath) {
				$("#imagePreviewDiv_"+controlName).show();
				document.getElementById("imagePreview_"+controlName).src=imagePath;

		};

		xhr.send(fd);
	}, false);

	 document.querySelector('.deleteImage'+controlName).addEventListener('click', function(e) {
	 $("#imagePreviewDiv_"+controlName).hide();
	 document.getElementById("imagePreview_"+controlName).src="";
	 slider.style.width = "0%";
		$("#"+controlName).val("");
	 });


	});
	{/literal}
</script>
