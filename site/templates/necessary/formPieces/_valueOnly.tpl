<div class="form-group">
	<label class="col-md-2 control-label">{$__label}</label>
	<div class="col-md-10 control-value">
		{$__value}
	</div>
</div>