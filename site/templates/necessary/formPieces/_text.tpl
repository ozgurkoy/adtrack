<div class="form-group" {if isset($__blockCustomAttr)}{$__blockCustomAttr}{/if}>
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		{if !empty($__updateBtnID)}
		<div class="input-group">
		{/if}
		<input type="text" name="{$__name}" id="{$__name}" class="form-control{if !empty($__class)} {$__class}{/if}" value="{$__value|replace:'\"':'ʺ'}" {if !empty($__customAttribute)}{$__customAttribute}="{$__attributeValue}"{/if} {if (!empty($__value) || (isset($__value) && $__value == 0)) && !empty($__disabled)}disabled{/if}{if !empty($__mask)} data-mask="{$__mask}" data-placeholder=""{/if}>

		{if !empty($__inputIcon)}
			<i class="{$__inputIcon} field-icon"></i>
		{/if}
		{if isset($__info)}
		<span class="help-block">{$__info}</span>
		{/if}
		{if !empty($__updateBtnID)}
			<span class="input-group-btn">
				<button class="btn btn-default btn-update" valID="{$__updateBtnID}" type="button">Update</button>
			</span>
		</div>
		{/if}
	</div>
</div>
