<div class="form-group">
	<label class="col-md-2 control-label" for="input17">{$__label}</label>
	<div class="col-md-10">
		<input type="text" class="form-control" data-mask="9999/99/99" value="{if is_numeric($__value)}{timestampToDate l="tr" d=$__value}{else}{$__value}{/if}" name="{$__name}" id="{$__name}">
		<span class="help-block">Select Year/Month/Day </span></div>
</div>