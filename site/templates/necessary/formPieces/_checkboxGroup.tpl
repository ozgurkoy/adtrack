<div class="form-group errorContext">
	<label class="col-md-2 control-label" for="{$__name}" id="lbl_{$__name}">{$__label}</label>

	<div class="col-md-10">
		{assign var=cc value=0}
		{foreach from=$__options key=op item=opv}
			<label class="{if isset($__class)}{$__class}{else}checkbox{/if}" for="{$__name}_{$op}">
				<div class="checker"><span><input type="checkbox" name="{$__name}[{$cc}]" id="{$__name}_{$op}" value="{$op}" class="uniform" {if !empty($__value) && $__value|strpos:$op}checked{/if} {if !empty($__value) && !empty($__disabled)}disabled{/if}></span></div> {$opv}
			</label>
			{assign var=cc value=$cc+1}
		{/foreach}
		{if isset($__info)}
			<span class="help-block">{$__info}</span>
		{/if}

	</div>

</div>
