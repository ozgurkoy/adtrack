<?php
{*// generated on {$time}*}
$__routings = array(
{foreach from=$routings key=route item=go}
{if !isset($pres[$route]) && !isset($posts[$route])}
'{$route}'=>'{$go}',
{else }
{*#(method, pre, preparams, post, postparams)*}
'{$route}'=>array(
	'{$go}',
	{if isset($pres[$route])}'{$pres[$route]}'{else}false{/if},
	{if isset($preArgs[$route])}{$preArgs[$route]}{else}false{/if},
	{if isset($posts[$route])}'{$posts[$route]}'{else}false{/if},
	{if isset($postArgs[$route])}{$postArgs[$route]}{else}false{/if}
),
{/if}
{/foreach}
);

{if isset($allMethods)}
#key to including other routing files on demand
$routingBulk = array(
	{foreach from=$allMethods key="ns" item="methods"}
		"{$ns}"=>"{', '|implode:$methods}",
	{/foreach}
	);
{/if}
