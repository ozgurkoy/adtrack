<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    {include file="headers/basic.tpl"}
    <link rel="stylesheet" media="all" href="/site/layout/styles/site/error/all.css" type="text/css"/>
    {include file="headers/js.tpl"}
    {include file="headers/analytics.tpl"}    
</head>

<body>
	<div id="wrapper">
		<div class="w1">
			<div id="header">
				<strong class="logo"><a href="#">ad Mingle</a></strong>
			</div>
			<div id="main">
                            
				<div class="user-section">
					<h1><a href="#">Error Occured</a></h1>
					<p>Please try again later</p>
				</div>
			</div>
		</div>
		{include file="sfooter.tpl"}
	</div>
</body>
</html>