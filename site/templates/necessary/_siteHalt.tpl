<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{$SERVER_NAME}</title>
	<link rel="stylesheet" media="all" href="/site/layout/styles/site/halt/all.css" type="text/css"/>
	<script type="text/javascript" charset="utf-8" src="/site/layout/js/errorsTR.js"></script>
	<script type="text/javascript" charset="utf-8">
		var SITE='/';
	</script>

	<script type="text/javascript" src="/site/layout/js/invite/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/site/layout/js/invite/cufon.js"></script>
	<script type="text/javascript" src="/site/layout/js/invite/jquery.main.js"></script>
	<script src="/site/layout/js/main.js" type="text/javascript"></script>
	<!-- {$from} -->
</head>
<body>
	<div id="wrapper">
		<div class="w1">
			<div id="header">
				<strong class="logo"><a href="#">ad Mingle</a></strong>
			</div>
			<div id="main">
				<div class="user-section">
					<h1><a href="#">Under Construction</a></h1>
					<p>Try again later</p>
				</div>
			</div>
		</div>
		<div id="footer">
			<div class="footer-holder">
				<div class="copy">&copy; 2014 adMingle.</div>
				<div class="social-block">
					<span class="caption">Follow us</span>
					<ul class="social-networks">
						<li><a class="facebook" target="_blank" href="http://facebook.com/admingle">Facebook</a></li>
		                <li><a class="linkedin" target="_blank" href="http://www.linkedin.com/company/admingle">LinkedIn</a></li>
						<li><a class="twitter" target="_blank" href="http://twitter.com/ad_mingle">Twitter</a></li>
						<!-- <li><a class="rss" target="_blank" href="#">rss</a></li> -->
						<li><a class="youtube" target="_blank" href="http://youtube.com/adMingle">youtube</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	{literal}
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-29154548-2']);
	  _gaq.push(['_setDomainName', '{/literal}{$SERVER_NAME}{literal}']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	{/literal}
</body>
</html>