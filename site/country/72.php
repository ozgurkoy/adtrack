<?php
/**
 * Class ValidateCountry
 *
 *
 * TODO : FIX FOR THIS COUNTRY
 */
class CountryValidate{

	/**
	 * country name
	 */
	const COUNTRY = "FR";


	/**
	 * @param array $details
	 * @return bool
	 */
	public static function validateBankInfo( array $details, ValidationResponse &$r ) {
		global $__DP;

		require $__DP."site/lib/php-iban/php-iban.php";
		// we will need only iban in Turkey
		if ( !(isset( $details[ "IBAN" ] ) && verify_iban( $details[ "IBAN" ] ) !== false) ){
			$r->errorCode = 417;
			return false;
		}

		return true;
	}

	/**
	 * For (199)1999999
	 * @param $phone
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validatePhone( $phone ) {

		return true;
	}
}
