<?php
/**
 * Class ValidateCountry
 *
 *
 * TODO : FIX FOR THIS COUNTRY

 */
class CountryValidate{

	/**
	 * country name
	 */
	const COUNTRY = "GB";


	/**
	 * @param array $details
	 * @return bool
	 */
	public static function validateBankInfo( array $details, ValidationResponse &$r ) {
		/*
		global $__DP;

		require $__DP."site/lib/php-iban/php-iban.php";

		// we will need only iban in Turkey
		if ( !(isset( $details[ "IBAN" ] ) && verify_iban( $details[ "IBAN" ] ) !== false) ){
			$r->errorCode = 417;
			return false;
		}
*/
		if(isset($details["accountnumber"]) &&isset($details["sort_code"])){
			return true;
		}

		return false;
	}

	/**
	 * For (199)1999999
	 * @param $phone
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validatePhone( $phone ) {

		preg_match( "|^\([1-9]\d{2}\)[0-9]\d{6}$|", $phone, $isValidGB );
		preg_match( "|^\([0-9]\d{2}\)[0-9]\d{7}$|", $phone, $isValidZA );

		$isValid = $isValidGB || $isValidZA;
		return sizeof($isValid)>0;
	}
}
