<?php
/**
 * Class ValidateCountry
 *
 *
 * TODO : FIX FOR THIS COUNTRY
 */
class CountryValidate{

	/**
	 * country name
	 */
	const COUNTRY = "DE";


	/**
	 * @param array $details
	 * @return bool
	 */
	public static function validateBankInfo( array $details, ValidationResponse &$r ) {
		global $__DP;

		require $__DP."site/lib/php-iban/php-iban.php";
		// we will need only iban in Turkey
		if ( !(isset( $details[ "IBAN" ] ) && verify_iban( $details[ "IBAN" ] ) !== false) ){
			$r->errorCode = 417;
			return false;
		}

		return true;
	}

	/**
	 * sam's bullshit custom payment method validation
	 * @param array              $details
	 * @param ValidationResponse $r
	 * @param ValidateFinance    $v
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validate_bank_( array $details, ValidationResponse &$r ) {
		global $__DP;

		require $__DP."site/lib/php-iban/php-iban.php";
		if ( !(isset( $details[ "IBAN" ] ) && verify_iban( $details[ "IBAN" ] ) !== false) ){
			$r->errorCode = 417;
			return false;
		}

		if ( !(isset( $details[ "BIC" ] ) && preg_match("/^[a-z]{6}[0-9a-z]{2}([0-9a-z]{3})?\\z/ui", $details[ "BIC" ]) > 0 ) ){
			$r->errorField = "BIC";
			$r->errorCode = 88;
			return false;
		}

		return true;
	}

	/**
	 * For (199)1999999
	 * @param $phone
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validatePhone( $phone ) {

		return true;
	}
}
