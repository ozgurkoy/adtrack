<?php
/**
 * Class ValidateCountry
 *
 *
 * TODO : FIX FOR THIS COUNTRY
 */
class CountryValidate{

	/**
	 * country name
	 */
	const COUNTRY = "IL";


	/**
	 * @param array $details
	 * @return bool
	 */
	public static function validateBankInfo( array $details, ValidationResponse &$r ) {

		return true;
	}

	/**
	 * For (199)1999999
	 * @param $phone
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validatePhone( $phone ) {

		return true;
	}

	/**
	 * Custom policy renderer
	 * @author Murat
	 */
	public static function customPolicy($policy,$policyParameters=null,Publisher $publisher){
		switch($policy){
			case 1:
				$publisher->loadUser();
				$email = $publisher->user_publisher->getDefaultEmail();
				JT::assign("publisher",$publisher);
				JT::assign("email",$email);
				echo JT::pfetch("country_IT_customPolicy_1");
				break;
			case 2:
				$publisher->loadPublisher_publisherMoneyAction($policyParameters);
				$publisher->loadCity();
				if ($publisher->publisher_publisherMoneyAction->gotValue){
					$campaigns = $publisher->publisher_publisherMoneyAction->getRelatedCampaigns();
					if (is_array($campaigns) && count($campaigns)){
						$firstDate = $campaigns[0]["approveDate"];
						$titles = Format::array_column($campaigns, "title");
						$campaignTitles = implode(",", $titles);
						JT::assign("publisher",$publisher);
						JT::assign("firstDate",$firstDate);
						JT::assign("campaignTitles",$campaignTitles);
						echo JT::pfetch("country_IT_customPolicy_2");
					} else {
						JLog::$writeToFile = true;
						JLog::log("cri","Couldn't find related campaigns for money request #{$publisher->publisher_publisherMoneyAction->id}");
						JLog::$writeToFile = false;
						JUtil::siteError();
					}


				} else {
					JUtil::jredirect( "/dashboard", true );
				}

				break;
			default:
				JUtil::jredirect( "/dashboard", true );
				break;
		}
	}

	/**
	 * @todo: Write true validation script
	 * @param $identityNo
	 * @return bool
	 * @author Murat
	 */
	public static function validateIdentity( $identityNumber ) {
		return true;
		if (preg_match('/^[a-z0-9\.\-\d_]$/i', $identityNumber)) {
			return true;
		} else {
			return false;
		}
	}
}
