<?php
/**
 * Class ValidateCountry
 */
class CountryValidate{

	/**
	 * country name
	 */
	const COUNTRY = "TR";



	/**
	 * @param array $details
	 * @return bool
	 */
	public static function validateBankInfo( array $details, ValidationResponse &$r ) {
		global $__DP;

		require $__DP."site/lib/php-iban/php-iban.php";

		// we will need only iban in Turkey
		if ( !(isset( $details[ "IBAN" ] ) && verify_iban( $details[ "IBAN" ] ) !== false) ){
			$r->errorCode = 417;
			return false;
		}

		return true;
	}

	/**
	 * For (199)1999999
	 * @param $phone
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validatePhone( $phone ) {
		preg_match( "|^\([1-9]\d{2}\)[0-9]\d{6}$|", $phone, $isValid );
		return sizeof($isValid)>0;
	}

	/**
	 * Custom policy renderer
	 * @author Murat
	 */
	public static function customPolicy($policy,$policyParameters=null,Publisher $publisher){
		switch($policy){
			case 1:
				$publisher->loadUser();
				$email = $publisher->user_publisher->getDefaultEmail();
				JT::assign("publisher",$publisher);
				JT::assign("email",$email);
				echo JT::pfetch("country_IT_customPolicy_1");
				break;
			case 2:
				$publisher->loadPublisher_publisherMoneyAction($policyParameters);
				$publisher->loadCity();
				if ($publisher->publisher_publisherMoneyAction->gotValue){
					$campaigns = $publisher->publisher_publisherMoneyAction->getRelatedCampaigns();
					if (is_array($campaigns) && count($campaigns)){
						$firstDate = $campaigns[0]["approveDate"];
						$titles = Format::array_column($campaigns, "title");
						$campaignTitles = implode(",", $titles);
						JT::assign("publisher",$publisher);
						JT::assign("firstDate",$firstDate);
						JT::assign("campaignTitles",$campaignTitles);
						echo JT::pfetch("country_IT_customPolicy_2");
					} else {
						JLog::$writeToFile = true;
						JLog::log("cri","Couldn't find related campaigns for money request #{$publisher->publisher_publisherMoneyAction->id}");
						JLog::$writeToFile = false;
						JUtil::siteError();
					}


				} else {
					JUtil::jredirect( "/dashboard", true );
				}

				break;
			default:
				JUtil::jredirect( "/dashboard", true );
				break;
		}
	}
}
