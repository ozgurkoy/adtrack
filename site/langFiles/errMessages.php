<?php

$_errMessages = array(
	"TR"=> array(
			"test"=>"test hatası",
			"test2"=>"test hatası 2",
			"sessionTimeOut" => "Oturumunuz zaman aşımından dolayı kapanmıştır. Lütfen tekrar giriş yapıp deneyiniz.",
			"editToNotOwnedBrand" => "Size ait olmayan bir markayı güncelleyemezsiniz.",
			"editToNotOwnedBank" => "Size ait olmayan bir banka bilgisini güncelleyemezsiniz.",
			"editToNotOwnedCampaign" => "Size ait olmayan bir kampanya detayını görüntüleyemezsiniz.",
			"editToNotOwnedCampaignReport" => "Size ait olmayan bir kampanya raporunu görüntüleyemezsiniz.",
			"updateToNotWaitingOfferReplyCampaign" => "Teklif aşamasında bir kampanyanın teklif cevabı güncellenemez.",
			"invalidCampOfferStatus" => "Geçersiz teklif cevap kodu.",
			"invalidBankID" => "Geçersiz banka kodu",
			"loggedInCantForgotPassword" => "Giriş yapmış olduğunuzdan dolayı şifre hatırlatma sayfasına giremezsiniz.",
			"loggedInCantConfirm" => "Giriş yapmış olduğunuzdan dolayı onay sayfasına giremezsiniz.",
			"usedReplaceCode" => "Geçersiz veya kullanılmış bir hatırlatma kodu girdiniz. Lütfen kontrol edip tekrar deneyiniz.",
			"accountAlreadyConfirmed" => "Bu hesap daha önceden onaylanmıştır.",
			"invalidInviteCode" => "Geçersiz davet kodu. Lütfen kontrol edip tekrar deneyiniz.",
			"invideCodeUsed" => "Girmiş olduğunuz davet kodu kullanılmıştır. Lütfen kontrol edip tekrar deneyiniz.",
			"viewToNotOwnedCampaign" => "Size ait olmayan bir kampanya detayını görüntüleyemezsiniz.",
			"profileNotCompleted" => "Profil bilgilerinizde eksik alanlar tespit edilmiştir. Lütfen kontrol ediniz.",
			"errorTwitterNotFound" => "Twitter bilgileriniz bulunamamıştır. Lütfen bizimle iletişime geçiniz.",
			"errorTwitterCouldNotAuth" => "Twitter hesabınızda adMingle yetkilerini kaldırdığınız tespit edilmiştir. Lütfen Twitter ile giriş yapınız."
			),
	"EN"=> array(
			"test"=>"test error",
			"test2"=>"test error 2",
			"sessionTimeOut" => "Your session timed out. Please re-login to try again.",
			"editToNotOwnedBrand" => "You may only update a brand that belongs to you.",
			"editToNotOwnedBank" => "You may only enter your own bank account information.",
			"editToNotOwnedCampaign" => "You may only preview a campaign that belongs to you.",
			"editToNotOwnedCampaignReport" => "You may only view a campaign report that belongs to you.",
			"updateToNotWaitingOfferReplyCampaign" => "You can not update a campaign status that is not on a waiting for reply status.",
			"invalidCampOfferStatus" => "Invalid campaign offer reply code.",
			"invalidBankID" => "Invalid Bank ID",
			"loggedInCantForgotPassword" => "As you are already signed-in, you may not access the forgot password page.",
			"loggedInCantConfirm" => "As you are already signed-in, you may not access the confirm page.",
			"usedReplaceCode" => "You entered an invalid or used password reminder code. Please check and try again.",
			"accountAlreadyConfirmed" => "This account is already confirmed.",
			"invalidInviteCode" => "Invalid invitation code. Please check and try again.",
			"invideCodeUsed" => "Your invite code is already used. Please check and try again.",
			"viewToNotOwnedCampaign" => "You may only view a campaign that belongs to you.",
			"profileNotCompleted" => "There seem to be mandatory fields missing in your profile information. Please check.",
			"errorTwitterNotFound" => "Your twitter log-in credentials were not found. Please contact us as soon as possible.",
			"errorTwitterCouldNotAuth" => "We detected that you revoked adMingle access to your Twitter account. Please log-in using your Twitter account."
			)

);
