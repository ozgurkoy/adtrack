<?php
function filterPost(&$ar) {
	$exceptions = array('admhtml','wordText','wysiwyg','html','subHtml','options','footerText','data','video');

	foreach ($ar as $pk => $pv) {
		if(in_array($pk,$exceptions)) {
			continue;
		}
		if(is_array($pv)) {
			filterPost($pv);
			$ar[$pk] = $pv;
		} else {
			$ar[$pk] = strip_tags($pv);
			//$ar[$pk] = mysql_escape_string($ar[$pk]);
		}
	}
}

if(sizeof($_POST) > 0) {
	filterPost($_POST);
}
