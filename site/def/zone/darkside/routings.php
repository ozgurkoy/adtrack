<?php
$__routings = array(
'ds_campaignSettings'=>array(
	'DSSettings_campaignSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_campaignSettingsAction'=>array(
	'DSSettings_campaignSettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_emailSettings'=>array(
	'DSSettings_emailSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_emailSettingsAction'=>array(
	'DSSettings_emailSettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_generalSettings'=>array(
	'DSSettings_generalSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_generalSettingsAction'=>array(
	'DSSettings_generalSettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_homePageSettings'=>array(
	'DSSettings_homePageSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_homePageSettingsAction'=>array(
	'DSSettings_homePageSettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_localSettings'=>array(
	'DSSettings_localSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_localSettingsAction'=>array(
	'DSSettings_localSettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_otherSettings'=>array(
	'DSSettings_otherSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_otherSettingsAction'=>array(
	'DSSettings_otherSettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_securitySettings'=>array(
	'DSSettings_securitySettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_securitySettingsAction'=>array(
	'DSSettings_securitySettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_securitySettingsIPAction'=>array(
	'DSSettings_securitySettingsIPAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_serviceSettings'=>array(
	'DSSettings_serviceSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_serviceSettingsAction'=>array(
	'DSSettings_serviceSettingsAction',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettings'=>array(
	'DSSettings_socialNetworksSettingsList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_socialNetworksSettingsKeys'=>array(
	'DSSettings_socialNetworksSettingsKeysList',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'list',
),
	'logOperation',
	array (
  0 => 'Local Settings',
)),
'ds_socialNetworksSettingsAction_facebook'=>array(
	'DSSettings_serviceSettingsActionfacebook',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettingsAction_twitter'=>array(
	'DSSettings_serviceSettingsActiontwitter',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettingsAction_gplus'=>array(
	'DSSettings_serviceSettingsActiongplus',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettingsAction_vk'=>array(
	'DSSettings_serviceSettingsActionvk',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettingsAction_xing'=>array(
	'DSSettings_serviceSettingsActionxing',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettingsAction_instagram'=>array(
	'DSSettings_serviceSettingsActioninstagram',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettingsAction_login'=>array(
	'DSSettings_serviceSettingsActionlogin',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
'ds_socialNetworksSettingsAction_sharing'=>array(
	'DSSettings_serviceSettingsActionsharing',
	'checkPerms',
	array (
  0 => 'localSettings',
  1 => 'edit',
),
	'logOperation',
	array (
  0 => 'localSettings A/E',
)),
);

