<?php
/************************************
 *
 *    required form fields file - darkside
 *    özgür köy
 *    0 -> error message
 *    1 -> function to run error check ( defaults to matchEmpty )
 *    2 -> parameters, seperated with commas
 *
 *    multiple operations for some field can be added like
 *    password
 *    password@
 *    password@@
 *
 *    TODO ::: CATCH ERRORS AND SHOW EM AT CLIENT  SIDE ,AUTOMATICALLY
 *
 ************************************/
$formFields = array(
	"login"                => array(
		"username"  => array( "Enter username" ),
		"password"  => array( "Enter password" ),
		"password@" => array( "Login&Pass Wrong", "checkLogin" )
	),
	"action"               => array(
		"label" => array( "Enter label" ),
		"tasks" => array( "Enter one task per line" )
	),
	"user"                 => array(
		"username"  => array( "Enter username" ),
		"username@" => array( "Enter different username", "checkExistingUsername" ),
		"email"     => array( "Enter email" ),
		"email@"    => array( "Enter email", "matchEmail" ),
		"password"  => array( "Must type password for a new user", "checkEmptyPassForNewUser" ),
	),
	"profile"              => array(
		"email"  => array( "Enter email" ),
		"email@" => array( "Enter email", "matchEmail" ),
	),
	"role"                 => array(
		"label" => array( "Enter label" )
	),
	"menu"                 => array(
		"label" => array( "Enter label" ),
	),
	"country"              => array(
		"shortName" => array( "Enter Short Name" ),
		"localName" => array( "Enter Local Name" ),
		"iso"       => array( "Enter ISO" ),
		#"url"       => array( "Enter URL" )
	),
	"bank"                 => array(
		"bankName"     => array( "Enter Bank Name" ),
		"country_bank" => array( "Choose country" ),
	),
	"bug" => array(
		"title"       => array( "Enter title" ),
		"details" => array( "Enter description" ),
		"project"     => array( "Choose Project" ),
	),
	"editBug" => array(
		"title"       => array( "Enter title" ),
		"project"     => array( "Choose Project" ),
	),
	"campaign"             => array(
		//"race"         => array( "Please select the campaign race" ),
		"title"      => array( "Please enter the campaign title" ),
		"agerange"   => array( "Please choose age range" ),
		"interests"  => array( "Please choose interests" ),
		//"advertiserId" => array( "Please choose advertiser" ),
		"startDate"  => array( "Please choose a start date" ),
		"endDate"    => array( "Please choose an end date" ),
		"sex"        => array( "Please choose gender" ),
		"effectType" => array( "Please choose an effect type", "checkEffectType" ),
		"SLDomain"   => array( "Please choose a shortlink domain for CPC campaign", "checkLinkForCPC" ),
		"link"       => array( "Please choose a link for CPC campaign", "checkLinkForCPC" ),
		"link@"      => array( "Please choose a valid link for CPC campaign", "validateURL" ),
		//"previewImage" => array( "Please choose a preview image for url", "validateIfFull", "campLink" ),
		"campNotes"  => array( "Please fill notes of the campaign", "validateIfFull", "campLink" ),
		"message"    => array( "Please fill in at least one message" ),
		"campDetail" => array( "Please fill details of the campaign", "validateIfFull", "campLink" ),
		"brandName"  => array( "Please type the new brand name", "validateIfValue", array( "brand", -1 ) )
	),
	"cplLandingPage" => array(
		"preHash"    => array( "Please choose a hash" ),
		"endMessage" => array( "Required field" ),
		"cplCampaign_advertiser" => array( "Required field" ),
		"form"    => array( "Required field" ),
		"layout"  => array( "Required field" ),
		"name"       => array( "Required field" ),
		"preHash@@"   => array( "No space in hash is allowed", "noWhiteSpace" ),
		"preHash@"   => array( "Please choose a different hash", "checkCPLHashExists" )
	),
	"CplFormField"=>array(
		"type" => array("Required field"),
//		"html" => array("Required field"),
	),
	"CplForm"=>array(
		"label" => array("Required field"),
	),
	"CplField"=>array(
		"label" => array("Required field"),
		"field" => array("Required field"),
	),
	"cplLayout"=>array(
		"name" => array("Required field"),
		"errorPosition" => array("Required field"),
	),
	"advertiser"           => array(
		"username"       => array( "This field is mandatory" ),
		"password"       => array( "This field is mandatory", "validateIfEmpty", "id" ),
		"companyName"    => array( "This field is mandatory" ),
		"companyType"    => array( "This field is mandatory" ),
		"commissionRate" => array( "This field is mandatory" ),
		"country"        => array( "This field is mandatory" ),
		"cpcCost"        => array( "Must be between 0-99", "checkCPCCost" ),
		"city"           => array( "This field is mandatory", "validateIfFull", "country" ),
	),
	"advContact"           => array(
		"name"            => array( "This field is mandatory" ),
		"title"           => array( "This field is mandatory", "validateIfEmpty", "id" ),
//		"contactPosition" => array( "This field is mandatory" ),
		"contactEmail"    => array( "This field is mandatory", "matchEmail" ),
	),
	"advertiserBrand"      => array(
		"name" => array( "This field is mandatory" ),
	),
	"advertiserBilling"    => array(
		"accountName" => array( "This field is mandatory" ),
		"billingName" => array( "This field is mandatory" ),
	),
	"localSettings"        => array(
		"country"        => array( "Please select System Country" ),
		"language"       => array( "Please select the default Language " ),
		"currency"       => array( "Please select the Currency" ),
		"thousand"       => array( "Please enter the thousand separator" ),
		"decimal"        => array( "Please enter the decimal separator" ),
		"currencySymbol" => array( "Please select the symbol location" ),
		"jsDateFormat"   => array( "Please enter Java Script date format " ),
		"dateFormat"     => array( "Please enter PHP Date format" ),
		"timeFormat"     => array( "Please enter PHP Time format" ),
	),
	"serviceSettings"      => array(
		"SLSAPI"      => array( "Required field" ),
		"SLSCampEnd"  => array( "Required field" ),
		"SLSCampEnd@" => array( "Url Not Valid", "validateURL" ),
		"SLSPush"     => array( "Required field" ),
		"SLSPush@"    => array( "Url Not Valid", "validateURL" ),
		"calcApi"     => array( "Required field" ),
		"calcApi@"    => array( "Url Not Valid", "validateURL" ),
		"langApi"     => array( "Required field" ),
		"langApi@"    => array( "Url Not Valid", "validateURL" ),
		"tgApi"       => array( "Required field" ),
		"tgApi@"      => array( "Url Not Valid", "validateURL" ),
		"SLSCountry"  => array( "Required field" ),
		"SLSDomain"   => array( "Required field" ),
	),
	"homePageSettings"     => array(
		"facebook@"   => array( "Url Not Valid", "validateURL" ),
		"linkedin@"   => array( "Url Not Valid", "validateURL" ),
		"youtube@"    => array( "Url Not Valid", "validateURL" ),
		"googlePlus@" => array( "Url Not Valid", "validateURL" ),
		"blog"        => array( "Required field" ),
		"blog@"       => array( "Url Not Valid", "validateURL" ),
	),
	"publisherSendMessage" => array(
		"title" => array( "Required field" ),
		"message" => array( "Required field" ),
		"target"  => array( "Required field" )
	),
	"storeBrand"           => array(
		"name"  => array( "Required field" ),
		"image" => array( "Required field", "checkUploadExist", "image" )
	),
	"storeCategory"           => array(
		"price" => array( "You must declare a price if you want to have products related to this category", "validateIfFull", "containsProducts" )
	),
	"generalSettings"      => array(
		"sitemode"         => array( "Required field" ),
		"IPHONE_URL"       => array( "Required field" ),
		"IPHONE_URL@"      => array( "Url Not Valid", "validateURL" ),
		"ANDROID_URL"      => array( "Required field" ),
		"ANDROID_URL@"     => array( "Url Not Valid", "validateURL" ),
		"IPHONE_APP_ID"    => array( "Required field" ),
		"ANDROID_APP_ID"   => array( "Required field" ),
		//"ZENDESK"          => array( "Required field" ),
		"GOOGLE_ANALYTICS" => array( "Required field" ),
	),
	"campaignOverview"     => array(
		"id"         => array( "Required field" ),
		"approve"    => array( "Please choose an action" ),
		"denyReason" => array( "Please enter the deny reason", "validateIfValue", array( "approve", "Deny" ) ),
	),
	"campaignRevive"       => array(
		"id" => array( "Required field" ),
	),
	"campaignOffer"        => array(
		"id" => array( "Required field" ),
	),
	"campaignApproveOffer" => array(
		"id"         => array( "Required field" ),
		"offerReply" => array( "Required field" ),
	),
	"campaignPublish"      => array(
		"id"            => array( "Required field" ),
		"publishOption" => array( "Required field" ),
		"publishDate"   => array( "Please select a publish date", "validateIfValue", array( "publishOption", "-1" ) ),
		"publishTime"   => array( "Please select a publish time", "validateIfValue", array( "publishOption", "-1" ) ),
	),
	"publisherProfile"     => array(
		"name"    => array( "Required field" ),
		"surname" => array( "Required field" ),
		"bday"    => array( "Required field" ),
		"type"    => array( "Required field" ),
	),
	"publisherMoneyActionDetails" => array(
		"id"    => array( "Required field" ),
		"status"    => array( "Required field" ),
		"paymentDate"   => array( "Please select a payment date", "validateIfValue", array( "status", PublisherMoneyAction::ENUM_STATUS_APPROVED ) ),
	),
	"clubNewPayment" => array(
		"club_clubMoneyAction"    => array( "Required field" ),
		"amount"    => array( "Amount should be numeric", "validateNumeric" ),
		"paymentDate"   => array( "Please select a payment date", "validateDate"),
	),
	"club" => array(
		"clubName" => array( "Required field" ),
		"clubPublicKey" => array( "Required field" ),
		"registrationText" => array( "Required field" ),
	),
);

?>