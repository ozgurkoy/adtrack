<?php
class JFormZone{

	public static function checkEmptyPassForNewUser( $password, $params=null, DSuser &$userObject ) {
		return $userObject->gotValue === true || !( empty( $password ) );
	}

	public static function checkExistingUsername( $username, $params=null  ) {
		$u = new DSUser();
		$filter = array( "username" => "LIKE $username" );
		if(isset($_POST["id"]))
			$filter[ "id" ] = "NOT IN " . $_POST[ "id" ];

		$u->load( $filter );
		$ret = !$u->gotValue;
		unset( $u );
		return $ret;
	}

	public static function checkLogin( $password, $params=null, &$userObject ) {
		return $userObject->login( $_POST[ "username" ], $_POST[ "password" ] );
	}

	/*CAMPAIGN FORM*/
	public static function checkEffectType( $e ) {
		if ( isset( $_POST[ "race" ] ) && $_POST[ "race" ] == "1" && ( !isset( $e ) || is_null( $e ) ) )
			return false;

		return true;
	}

	public static function checkLinkForCPC( $e ) {
		if ( isset( $_POST[ "race" ] ) && $_POST[ "race" ] == Campaign::ENUM_RACE_CPC && ( !isset( $e ) || empty( $e ) ) )
			return false;

		return true;
	}

	public static function validateURL( $e ) {
		if ( !empty( $e ) && strlen( $e ) > 0 )
			return preg_match( '|^http(s)?\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(/\S*)?$|i', $e );


		return true;
	}

	public static function matchIfValueMatches( $vl1,$params ){
		$p0 = explode(",",$params);
		$vl2 = $p0[0];
		$match = $p0[1];
		if( ($_REQUEST[$vl2]==$match) && ($vl1=='') ) return false;
		else return true;
	}
	/*END CAMPAIGN FORM*/

	public static function checkCPCCost( $vl ){
		return static::matchEmpty($vl) && ($vl >= 0 && $vl<=99);
	}


	public static function checkUploadExist( $vl, $name ) {
		if(isset($_POST["id"])) return true;
		return isset( $_FILES[ $name ] ) && strlen( $_FILES[ $name ][ "name" ] ) > 0;
	}

	public static function validateNumeric($n){
		return is_numeric($n);
	}

	public static function validateDate($d){
		return Validate::Date($d,"Y-m-d");

	}

	public static function checkCPLHashExists( $h ) {
		if ( !isset( $_POST[ "id" ] ) || $_POST[ "oldPreHash" ] != $h )
			return CplCampaignUser::isCodeUnique( $h );

		return true;

	}

	public static function noWhiteSpace( $h ) {
		return preg_match( "|[\s\t]|", $h )==0;
	}
}