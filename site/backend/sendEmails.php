<?php
include_once "lib/_backEndInterface.php";
include_once "lib/_base.php";

class SendEmails extends _backEndBase implements _backEndInterface {
	public $params = array();
	public $code = null;
	public $timeout = 10; //"
	public $description = "Sending campaign finishing emails";
	public $redirectToPendingPage = false;
	public $haltOnError = false;

	public function init( $params, $code ) {
		$this->code = $code;

		$this->params = $params;

		return $code;
	}

	public function run() {
		$bugId      = $this->params[ "bugId" ];
		$skipUser   = $this->params[ "skipUser" ];
		$onlyRoots  = $this->params[ "onlyRoots" ];
		$b          = new BugHistory( $bugId );
		$result     = $b->sendInformEmails( $skipUser, $onlyRoots );

		if ( $result == true )
			$this->complete();
		else
			$this->error( "Failed to send" );
	}

	public function complete( $id = null ) {
		$this->completed = true;
	}

	public function error( $err = null ) {
		$this->errorState = $err;
	}

	public function getTimeout() {
		return $this->timeout;
	}
}