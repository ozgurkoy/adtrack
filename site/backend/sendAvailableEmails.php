<?php
include_once "lib/_backEndInterface.php";
include_once "lib/_base.php";

class SendAvailableEmails extends _backEndBase implements _backEndInterface {
	public $params = array();
	public $code = null;
	public $timeout = 180; //"
	public $description = "Sending available emails";
	public $redirectToPendingPage = false;
	public $haltOnError = false;

	public function init( $params, $code ) {
		$this->code = $code;

		$this->params = $params;

		return $code;
	}

	public function run(){
		global $_S;

		$startId    = $this->params[ "startId" ];
		$campaignId = $this->params[ "campaignId" ];
		$utype      = $this->params[ "utype" ];
		$count      = $this->params[ "count" ];
		$filter = array(
			"emailed" => 0,
			"id"=>"&ge $startId",
			"availableDate" => "&le " . (time() + 60),
			"campaign_campaignPublisher" => array(
				"id" => $campaignId,
				"_custom" => "`campaign_campaignPublisher`.`race` IN (" . Campaign::ENUM_RACE_CPC . "," . Campaign::ENUM_RACE_CPL. "," . Campaign::ENUM_RACE_CPL_REMOTE . ")
							OR (
								`campaign_campaignPublisher`.`race` NOT IN  (" . Campaign::ENUM_RACE_CPC . "," . Campaign::ENUM_RACE_CPL ."," . Campaign::ENUM_RACE_CPL_REMOTE . ")
								AND `campaign_campaignPublisher`.`startDate` >= NOW()
							)"

			),
			"publisher_campaignPublisher" => array(
				"type" => ($utype == Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL ? Publisher::ENUM_TYPE_REGULAR : array(Publisher::ENUM_TYPE_TRENDSETTER,Publisher::ENUM_TYPE_CELEBRITY)),
				"user_publisher" => array(
					"status" => User::ENUM_STATUS_ACTIVE
				)
			)
		);

//		JPDO::beginTransaction();
		$cp = new CampaignPublisher();
		$cp->with(
			array(
				"publisher_campaignPublisher",
				Publisher::NO_LEFT_JOIN,
				array(
					array(
						"user_publisher",
						User::NO_LEFT_JOIN,
						array(
							array(
								"lang",
								User::DO_LEFT_JOIN
							)
						)
					)
				)
			),
			array(
				"campaign_campaignPublisher",
				Campaign::NO_LEFT_JOIN
			)
		);

		$cp->limit( 0, $count )->load( $filter );

		if ( $count > 0 && $cp->gotValue ) {
			$time = time();
			$c    = $cp->campaign_campaignPublisher;
			$ids  = array();
			$cnt  = 0;
			$cron = new Cron();
			$campaignImage = $c->getPreviewImageLink();

			do {
				$cnt++;

				$cp->publisher_campaignPublisher->user_publisher->user_publisher = $cp->publisher_campaignPublisher; // to avoid load publisher under user object
				$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
					'V2_campaignAvailable',
					array( "campName" => $cp->campaign_campaignPublisher->title ),
					array(
						"name"        => $cp->publisher_campaignPublisher->getFullName(),
						"campName"    => $cp->campaign_campaignPublisher->title,
						"publishDate" => Format::getDateTimeFormated( $time ), //date('d/m/Y (H:i)', $time),
						"campDetail"  => $_S . "myCampDetail/" . $cp->campaign_campaignPublisher->publicID
					),
					$cp->campaign_campaignPublisher->id,
					null,
					null,
					$campaignImage
				);
				Tool::echoLog($cnt . "# - " . $cp->publisher_campaignPublisher->user_publisher->username . ' (' . $cp->campaign_campaignPublisher->title . ') email scheduled to send');

				$ids[ ] = $cp->id;
			} while ( $cp->populate() );

			$cp = new CampaignPublisher();
			$cp->update( array( "id" => $ids ), array( "emailed" => 1 ) );

			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_PUBLISH_MAIL_SENT,
				"Campaign publish mails sent to " . ( !empty( $cron->type_options[ $utype ] ) ? $cron->type_options[ $utype ] : $utype ),
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);
			Tool::echoLog( ' (' . $c->title . ') publish mail(s) scheduled to send : '.$cnt );
		}
		else {
			Tool::echoLog( ' (' . $campaignId . ') no publisher found for that campaign' );
		}

//		JPDO::rollback();
		$this->complete();
	}

	public function complete( $id = null ) {
		$this->completed = true;

	}

	public function error( $err = null ) {
		$this->errorState = $err;
	}

	public function getTimeout() {
		return $this->timeout;
	}
}