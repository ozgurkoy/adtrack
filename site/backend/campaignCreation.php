<?php
include_once "lib/_backEndInterface.php";
include_once "lib/_base.php";

class CampaignCreation extends _backEndBase implements _backEndInterface {
	public $params = array();
	public $code = null;
	public $timeout = 3600; //"
	public $description = "Creating Campaign";
	public $redirectToPendingPage = true;
	public $redirectURL = "/ds_campaignOverview";
	public $haltOnError = true;

	public function init( $params, $code ) {
		$this->code = $code;

		$this->params = $params;

		return $code;
	}

	public function run() {
		try {
			JPDO::beginTransaction();

			$this->params[ "campaign" ] = Campaign::createStep1( $this->params );
			Tool::echoLog( "Step 1 finished" );
			Campaign::createStep2( $this->params );
			Tool::echoLog( "Step 2 finished" );
			Campaign::createStep3( $this->params );
			Tool::echoLog( "Step 3 finished" );
			Campaign::createStep4( $this->params );
			Tool::echoLog( "Step 4 finished" );
			Campaign::createStep5( $this->params );
			Tool::echoLog( "Step 5 finished" );
			echo "QUERY:::" . JPDO::$queryCount . PHP_EOL;
			JPDO::commit();
			$this->returnValue = $this->params[ "campaign" ]->id;

			$this->complete();
		}
		catch ( JError $j ) {
			JPDO::rollback();
			$this->error( "Creation failed : " . $j->getMessage() );
		}


	}

	public function complete() {
		$this->completed = true;
	}

	public function error( $err = null ) {
		$this->errorState = $err;
	}

	public function getTimeout() {
		return $this->timeout;
	}
}