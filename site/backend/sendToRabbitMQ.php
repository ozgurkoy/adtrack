<?php
include_once "lib/_backEndInterface.php";
include_once "lib/_base.php";

class SendToRabbitMQ extends _backEndBase implements _backEndInterface {
	public $params = array();
	public $code = null;
	public $timeout = 180; //"
	public $description = "Sending available emails";
	public $redirectToPendingPage = false;
	public $haltOnError = false;

	public function init( $params, $code ) {
		$this->code = $code;

		$this->params = $params;

		return $code;
	}

	public function run(){
		global $_S;

		$startId    = $this->params[ "startId" ];
		$cronStart  = $this->params[ "cronStart" ];
		$count      = $this->params[ "count" ];

		$filter = array(
			"id"=>"&ge $startId",
			"cronStart" => $cronStart
		);

		$m = new Mail();
		$m->limit( 0, $count )->load( $filter );

		if ($count > 0 && $m->gotValue){
			$rmq = new RabbitMQ();
			$rmq->declareQueue(RMQ_QUEUE_EMAIL);

			$mcount = 0;
			do {
				Tool::echoLog("{$cronStart} | {$mcount} => email state of " . $m->id . " is " . $m->state_options[$m->state]);

				$m->state = Mail::ENUM_STATE_QUEUED;
				$m->save();

				$msg_body = json_encode(array("id" => $m->id,"type" => "email"));
				$rmq->publishMessage($msg_body,RMQ_QUEUE_EMAIL);

				Tool::echoLog("{$cronStart} | {$mcount} => email queued: " . $m->id);
				$mcount++;
			}while($m->populate());

			Tool::echoLog("{$cronStart} => $mcount email(s) queued");
			$rmq->close();
		} else {
			Tool::echoLog("{$cronStart} => No mail found");
		}
		unset($m);
		$this->complete();
	}

	public function complete( $id = null ) {
		$this->completed = true;

	}

	public function error( $err = null ) {
		$this->errorState = $err;
	}

	public function getTimeout() {
		return $this->timeout;
	}
}