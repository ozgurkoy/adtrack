<?php
include_once "lib/_backEndInterface.php";
include_once "lib/_base.php";

class FinalizeCPAAffCampaign extends _backEndBase implements _backEndInterface {
	public $params = array();
	public $code = null;
	public $timeout = 180; //"
	public $description = "Finalize CPA.Aff Campaign";
	public $redirectToPendingPage = false;
	public $haltOnError = true;

	public function init( $params, $code ) {
		$this->code = $code;
		$this->params = $params;

		return $code;
	}

	public function run() {
		try{
			JPDO::beginTransaction();

			/*
			 * TODO : anything here ?
			 * */

			$this->complete();

		}
		catch(JError $j){
			JPDO::rollback();
			$this->error( "Failed to finalize" );
		}

	}

	public function complete( $id = null ) {
		$this->completed = true;

	}

	public function error( $err = null ) {
		$this->errorState = $err;
	}

	public function getTimeout() {
		return $this->timeout;
	}
}