<?php
interface _backEndInterface{
	public function init( $params, $code );
	public function run();
	public function getTimeout();

	/**
	 * call this method when the process is complete
	 *
	 * @return mixed
	 * @author Özgür Köy
	 */
	public function complete();

	/**
	 * called when there's an error, you can call it yourself. if the system gets an error this method is called.
	 *
	 * @param null $err
	 * @return mixed
	 * @author Özgür Köy
	 */
	public function error($err=null);
}