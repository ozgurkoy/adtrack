<?php
require_once '../../def/constants.php';
require_once $__DP.'core/run/exec.php';
if ( !isset( $_SESSION[ "DS_userId" ] ) )
	die( "Auth" );

echo "<PRE>";

if(isset($_GET["id"]) && strlen($_GET["id"])>0){
	Backend::killByCode( $_GET[ "id" ] );
	echo "Task killed".PHP_EOL;
}
else
	die( "input task id" );

echo "<a href='javascript:window.history.back();'>Go Back</a>";