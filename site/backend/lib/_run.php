<?php
$__DP = $argv[1];

require_once $__DP . 'site/def/constants.php';
require_once $__DP . 'core/run/exec.php';

set_time_limit( 100000 );

if ( !isset( $argv[ 2 ] ) ) {
	die( "Need operation code" );
}

$code = $argv[ 2 ];

error_reporting(0);
ini_set( "display_errors", false );
if ( ( $b = Backend::loadByCode( $code ) ) !== false ) {
	ob_start(array($b, "getOutput"));

	if($b->isOnGoing() === false)
		Backend::exec( $b );
	else
		echo Backend::JOB_ACTIVE;
}
else
	die( "Op not found" );