<?php

class _backEndBase
{
	public $params = array();
	public $code = null;
	public $timeout = 15; //"
	public $completed = false;
	public $errorStatus = null;
	public $haltOnError = false;
	public $description = "Pending process";
	public $returnValue = null;
	public $redirectToPendingPage = false;
	public $pendingPageURL = "/ds_backendPreview/";
	public $redirectURL = null;
}