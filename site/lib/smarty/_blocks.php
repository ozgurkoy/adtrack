<?php
class JTBlock
{
	public static function init( &$smarty ) {
		$mets = get_class_methods( "JTBlock" );
		foreach ( $mets as $m0 ) {
			$smarty->register_block( $m0,
				array( "JTBlock", $m0 ),
				false
			);
		}
	}

	public static function dynamic( $params, $content, &$smarty, &$repeat ) {
		return $content;
	}
}

