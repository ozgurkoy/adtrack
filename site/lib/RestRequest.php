<?php

class cRestRequest
{
	protected $url;
	public $ch;
	private $customHeader=false;
	protected $verb;
	protected $requestBody;
	protected $requestLength;
	protected $username;
	protected $password;
	protected $acceptType;
	protected $serializeType;
	public $responseBody;
	protected $responseInfo;

	public function __construct( $url = null, $verb = 'GET', $requestBody = null, $serializeType = null ) {
		$this->url           = $url;
		$this->verb          = $verb;
		$this->requestBody   = $requestBody;
		$this->requestLength = 0;
		$this->username      = null;
		$this->password      = null;
		$this->acceptType    = 'application/json';
		$this->serializeType = $serializeType;
		$this->responseBody  = null;
		$this->responseInfo  = null;
		$this->ch            = curl_init();


		if ( $this->requestBody !== null ) {
			$this->buildPostBody();
		}
	}

	public function flush ()
	{
		$this->requestBody		= null;
		$this->requestLength	= 0;
		$this->verb				= 'GET';
		$this->responseBody		= null;
		$this->responseInfo		= null;
	}

	public function execute ()
	{
		$ch =& $this->ch;

		$this->setAuth($ch);

		try
		{
			switch (strtoupper($this->verb))
			{
				case 'GET':
					$this->executeGet();
					break;
				case 'POST':
					$this->executePost();
					break;
				case 'PUT':
					$this->executePut();
					break;
				case 'DELETE':
					$this->executeDelete();
					break;
				default:
					throw new InvalidArgumentException('Current verb (' . $this->verb . ') is an invalid REST verb.');
			}
		}
		catch (InvalidArgumentException $e)
		{
			curl_close($ch);
			throw $e;
		}
		catch (Exception $e)
		{
			curl_close($ch);
			throw $e;
		}

	}

	public function buildPostBody ($data = null)
	{
		$data = ($data !== null) ? $data : $this->requestBody;

		if (!is_array($data))
		{
			throw new InvalidArgumentException('Invalid data input for postBody.  Array expected');
		}

		if ($this->serializeType == "json"){
			$fields_string = "";

			foreach($data as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			$data = rtrim($fields_string, '&');
		} else {
			$data = http_build_query($data, '', '&');
		}
		$this->requestBody = $data;
	}

	public function setHeaderOptions( $opts ) {
		$ch =& $this->ch;
		$this->customHeader = true;
		$h  = array();
		foreach ( $opts as $key => $value ) {
			$h[ ] = "$key: $value";
		}

		curl_setopt( $ch, CURLOPT_HTTPHEADER, $h );

		return $this;
	}

	protected function executeGet ()
	{
		$this->doExecute();
	}

	protected function executePost ()
	{
		$ch =& $this->ch;

		if (!is_string($this->requestBody))
		{
			$this->buildPostBody();
		}

		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->requestBody);
		curl_setopt($ch, CURLOPT_POST, 1);

		$this->doExecute();
	}

	protected function executePut ()
	{
		$ch =& $this->ch;

		if (!is_string($this->requestBody))
		{
			$this->buildPostBody();
		}

		$this->requestLength = strlen($this->requestBody);

		$fh = fopen('php://memory', 'rw');
		fwrite($fh, $this->requestBody);
		rewind($fh);

		curl_setopt($ch, CURLOPT_INFILE, $fh);
		curl_setopt($ch, CURLOPT_INFILESIZE, $this->requestLength);
		curl_setopt($ch, CURLOPT_PUT, true);

		$this->doExecute();

		fclose($fh);
	}

	protected function executeDelete ()
	{
		$ch =& $this->ch;

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');

		$this->doExecute();
	}

	protected function doExecute ()
	{
		$curlHandle =& $this->ch;

		$this->setCurlOpts($curlHandle);
		$this->responseBody = curl_exec($curlHandle);
		$this->responseInfo	= curl_getinfo($curlHandle);

		curl_close($curlHandle);
	}

	protected function setCurlOpts ()
	{
		$curlHandle =& $this->ch;

		curl_setopt($curlHandle, CURLOPT_TIMEOUT, 300);
		curl_setopt($curlHandle, CURLOPT_URL, $this->url);
		curl_setopt($curlHandle, CURLOPT_VERBOSE, true);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);

		if($this->customHeader === false)
			curl_setopt($curlHandle, CURLOPT_HTTPHEADER, array ('Accept: ' . $this->acceptType));
	}

	protected function setAuth ()
	{
		$curlHandle =& $this->ch;

		if ($this->username !== null && $this->password !== null)
		{
			curl_setopt($curlHandle, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
			curl_setopt($curlHandle, CURLOPT_USERPWD, $this->username . ':' . $this->password);
		}
	}

	public function getAcceptType ()
	{
		return $this->acceptType;
	}

	public function setAcceptType ($acceptType)
	{
		$this->acceptType = $acceptType;
	}

	public function getPassword ()
	{
		return $this->password;
	}

	public function setPassword ($password)
	{
		$this->password = $password;
	}

	public function getResponseBody ()
	{
		return $this->responseBody;
	}

	public function getResponseInfo ()
	{
		return $this->responseInfo;
	}

	public function getUrl ()
	{
		return $this->url;
	}

	public function setUrl ($url)
	{
		$this->url = $url;
	}

	public function getUsername ()
	{
		return $this->username;
	}

	public function setUsername ($username)
	{
		$this->username = $username;
	}

	public function getVerb ()
	{
		return $this->verb;
	}

	public function setVerb ($verb)
	{
		$this->verb = $verb;
	}
}
