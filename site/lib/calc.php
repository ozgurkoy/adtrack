<?php
/**
 * calculator for minglescore
 * step 1 : push user data and calculate user info
 * step 2 : calculate averages
 * step 3 : calculate costs
 *
 * @package default
 * @author Özgür Köy
 **/
class Calc
{

	/**
	 * period of calculation
	 *
	 * @var int
	 **/
	private $dayCount=15;
	
	/**
	 * max expected daily tweet
	 *
	 * @var int
	 **/
	private $expectedDailyTweet=10;
	
	/**
	 * great tweet's rt count, expected minimum to determine a tweet a 'great tweet'
	 *
	 * @var int
	 **/
	private $greatTweet=300;
	
	/**
	 * exclude greatest from average calculation if set to 1
	 *
	 * @var bool
	 **/
	private $excludeGreatestFromAverage=0;
	
	/**
	 * minimum FRQ value
	 *
	 * @var int
	 **/
	private $minFRQValue=0.7;
	
	/**
	 * min ARTCpRTT value
	 *
	 * @var int
	 **/
	private $minARCValue=0.7;
	
	/**
	 * minimum RTP value
	 *
	 * @var int
	 **/
	private $minRTPValue=0.6;
	
	/**
	 * maximum GTP value to be rated. will be added to 1 and multiplied with resource
	 *
	 * @var int
	 **/
	private $maxGTPValue=0.15;
	
	/**
	 * maximum RTPT value to be rated. will be added to 1 and multiplied with resource
	 *
	 * @var int
	 **/
	private $maxRTPTValue=0.7;

	/**
	 * maximum OS / Follower ratio to calculate, pass this ratio will be calculated with $excessOSMultiplier, better be an EVEN number
	 *
	 * @var int
	 **/
	private $OSFollowerRatio=12;
	
	/**
	 * see $OSFollowerRatio desc.
	 *
	 * @var int
	 **/
	private $excessOSMultiplier=0.1;
	
	/**
	 * user's followers cost
	 *
	 * @var int
	 **/
	private $followerCost=0.00045;
	
	/**
	 * minimum ratio of rts/tweets of a follower to consider it a rt'r. 
	 *
	 * @var int
	 **/
	private $maxRTRatioAllowed=0.7;
	
	/**
	 * minimum cost offered
	 *
	 * @var int
	 **/
	public $minCostAllowed=COUNTRY_MIN_COST;
	
	/**
	 * remote costs of OS, related to the population
	 *
	 * @var array
	 **/
	private $remoteCosts = array(
/*
	500=>	  0.002700,
	1000=>	  0.002500,
	2000=>	  0.001800,
	5000=>	  0.001600,
	10000=>	  0.001550,
	20000=>	  0.001350,
	30000=>	  0.001050,
	50000=>	  0.000950,
	75000=>	  0.000950,
	100000=>  0.000760,
	150000=>  0.000600,
	200000=>  0.000560,
	300000=>  0.000450,
	500000=>  0.000415,
	750000=>  0.000300,
	1000000=> 0.000285,
	1500000=> 0.000220,
	2000000=> 0.000175,
	2500000=> 0.000170,
	3000000=> 0.000160
*/

	500=>	  0.002700,
	1000=>	  0.002550,
	2000=>	  0.001900,
	5000=>	  0.001800,
	10000=>	  0.001450,
	20000=>	  0.001300,
	30000=>	  0.001150,
	50000=>	  0.000960,
	75000=>	  0.000890,
	100000=>  0.000820,
	150000=>  0.000730,
	200000=>  0.000690,
	300000=>  0.000595,
	500000=>  0.000555,
	750000=>  0.000470,
	1000000=> 0.000425,
	1500000=> 0.000390,
	2000000=> 0.000345,
	2500000=> 0.000310,
	3000000=> 0.000290,	
	5000000=> 0.000230,
	10000000=> 0.000210,
	50000000=> 0.000190


	);
	
	/**
	 * min/max values of cost related to the followers
	 *
	 * @var array
	 **/
	private $costLimits = array(
		"1000"=>array(1,5),
		"3000"=>array(3,20),
		"5000"=>array(5,30),
		"10000"=>array(8,90),
		"15000"=>array(10,100),
		"20000"=>array(20,110),
		"25000"=>array(25,125),
		"30000"=>array(27,140),
		"35000"=>array(30,150),
		"40000"=>array(40,160),
		"50000"=>array(40,170),
		"60000"=>array(45,180),
		"70000"=>array(50,210),
		"85000"=>array(70,250),
		"110000"=>array(85,275),
		"130000"=>array(110,300),
		"150000"=>array(130,320),
		"180000"=>array(150,320),
		"200000"=>array(180,350),
		"500000"=>array(200,1000)
	);
	
	/**
	 * calculation multipliers
	 *
	 * @var array
	 **/
	private $multipliers = array(
		"LOCAL"=>array(
			"FRQ" => 0.3 ,
			"RTPT"=> 0.3 ,
			"GTP" => 0.4
		),
		"REMOTE"=>array(
			"ARTCpRTT" => 0.2,
			"GTP" => 0.2 , 
			"maxRT" => 0.3,
			"RTP"	=> 0.3
		)
	);
	
	
	/**
	 * bonus multipliers for low OS rates
	 *
	 * @var array
	 **/
	private $osBonusRate = array(
		10000=>1.2,
		20000=>1.15,
		30000=>1.1,
		// 50000=>1.05, //getting critical from here to below
		// 70000=>1.1,
	);
	
	/**
	 * constructor
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function __construct()
	{
		$this->league = array_keys($this->remoteCosts); 
	}
	
	/**
	 * average calculator, used for multiple cases. calculates the value related to avg.
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function averager($avg, $value,$multiplier,$max=null,$min=null)
	{
		if(!is_numeric($avg)||$avg<=0) return 1; //this should not happen.
		
		if($value>=$avg){
			$lvalue = 0; //check for bigger numbers.
			if(!is_null($max)){
				$lvalue = ($value-$avg)/$avg;
				$lvalue = $lvalue>$max?$max:$lvalue;
			}
			return 1+$lvalue;

		}
		
		$r = round( 1-(($avg-$value)*$multiplier/$avg),2);//;
		return !is_null($min) && $r<$min ? $min : $r;
			
	}
	
	/**
	 * gtp is only a bonus, so a min of 1 is the calculation result. also a upper limit must be set
	 *
	 * @param int $value 
	 * @param int $avg 
	 * @return void
	 * @author Özgür Köy
	 */
	private function gtpAvg($avg, $value,$multiplier)
	{
		if($value<=$avg) return 1;
		$lvalue = (($value-$avg)/$avg)*$multiplier;

		return round(1+($lvalue>$this->maxGTPValue?$this->maxGTPValue:$lvalue),2);
	}
	
	/**
	 * 
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function getOS($os,$follower)
	{
		$halfMax = $this->OSFollowerRatio/2;
		
		//directly added to returning os. 
		$allowedFollower  = $halfMax * $follower;

		if($os<$follower || $os<=$allowedFollower)   
			return $os;
		
		//the remaining part, can be smaller than or equal to allowedFollower
		$semiAllowedFollower = $os>=$allowedFollower*2?$allowedFollower:$os-$allowedFollower;
		
		//remaining from the allowed and semiallowed
		$checkFollower = $os - ($semiAllowedFollower+$allowedFollower); 

		$c = $this->OSFollowerRatio;
		//we will render the semiAllowedFollower and calculate
		while($c>=$halfMax && $semiAllowedFollower>0){
			//calculate rate and multiply with follower
			$t0 = (($c-$halfMax)/$halfMax) * $follower;

			$allowedFollower 	 += $t0;
			$semiAllowedFollower -= $follower;
			$c--;
		}

		return $allowedFollower + ($checkFollower*$this->excessOSMultiplier);
	}

	/**
	 * rate by 100k, 200k etc
	 *
	 * @return int
	 * @author Özgür Köy
	 **/

	private function getOSRate($os,$follower)
	{
		if($os==0) return 0;
		
		//it is 1 by default, might raise if OS is low.
		$multiplier = 1;
		
		foreach ($this->osBonusRate as $limit => $value) {
			if($os<$limit){
				$multiplier = $value;
				break;
			}
		}

		$osSc = $this->getOS($os,$follower);
		return $multiplier * $osSc;
	}
	
	/**
	 * userId and jsons consisting of rt data(from followers) and rt threshold
	 * after calculation insert to db 
	 *
	 * @return array
	 * @author Özgür Köy
	 **/
	private function calculateUserOS($userId, $rtStats, $threshold)
	{
		$ret = array("originalSum"=>0, "rtSum"=>0);
		$rtLevels = array();
		
		foreach ($rtStats as $level => $mems) {
			$lft = 0;	//with rtrs
			$rft = 0;	//without rtrs

			// fetch all user data
			$originalSum = 0;

			foreach ($mems as $uname => $rtInfo) {
				if(is_object($rtInfo)){
					$lft +=  $rtInfo->followers_count;
					if($rtInfo->tweet_count> 0 ){
						if(($rtInfo->retweet_count / ($rtInfo->tweet_count+$rtInfo->retweet_count)) > $this->maxRTRatioAllowed){							
							// back to old deprecated remaing percentage, murat suggested this as well so me convinced
							$mult0 = 1-($rtInfo->retweet_count / ($rtInfo->tweet_count+$rtInfo->retweet_count)); 
							$rft += $rtInfo->followers_count * $mult0;
						}
						else
							$rft += $rtInfo->followers_count;
					}
				}
				else{
					$lft += $rtInfo;
					$rft += $rtInfo;
				}
			}
			$rtLevels[$level] = $rft;
		}
		$originalSum = 0;
		//calculate original sum. <- dont need to run this in fact
		if(isset($threshold->rtThresholdArray)){
			for ($t0=1; $t0 <= 10; $t0++) { 
				$originalSum += isset($threshold->$k->rtThresholdArray->$t0) ? 
				($t0*10/100) * ($threshold->$k->rtThresholdArray->$t0 - (isset($threshold->$k->rtThresholdArray->{$t0+1})?$threshold->$k->rtThresholdArray->{$t0+1}:0))
				:0 ;

			}
		}
		$ret["originalSumUnrated"] = $originalSum;
		
		$levSum = 0;
		for ($t0=1; $t0 <= 10; $t0++) { 
			$levSum += isset($rtLevels[$t0]) ? 
			($t0*10/100) * ($rtLevels[$t0] - (isset($rtLevels[$t0+1])?$rtLevels[$t0+1]:0))
			:0 ;
		}
		
		$ret["originalSum"] = $levSum;
		
		return $ret;
	}
	
	/**
	 * calculate detailed numbers per user. info json is needed which will have t, rt, maxRT, totalRT etc.
	 * after calculation insert to db
	 *	incoming data is a json consisting of: 
	 * 	- rtStats	-> rt'rs follower and twitter info
	 *	- threshold -> rt'rs array 1-10
	 *	- info -> league ,follower,tweetCount,maxRT,totalRT,RTCount
	 *
	 *
	 *
	 * @return array
	 * @author Özgür Köy
	 **/
	public function calculateUserNumbers($userId,$info)
	{
		global $debugLevel;
		
		if(!isset($userId) || !isset($info) || !is_numeric($userId))
			return false;
			
		$u = new User($userId);
		/*
		if(!(strlen($u->email)>0)){
			if($debugLevel>0) {
				echo "calculateUserNumbers NO USER : $userId\n";
			}
			die("no user");
			return false;
		}
		*/
		
		if(!$u->count || $u->utype != "p"){
			if($debugLevel>0) {
				Tool::echoLog("calculateUserNumbers NO USER : $userId");
			}
			die("no user");
			return false;
		}
		
		$info = json_decode($info);
		$ret = array();
		
		$threshold 	= $info->threshold;
		$rtStats	= $info->rtStats;
		$numbers	= $info->numbers;
		
		$osValues	= $this->calculateUserOS($userId,$rtStats,$threshold);
		
		// check for user league 
	    for ($dl=0; $dl < sizeof($this->league); $dl++) { 
			if($numbers->followers_count < $this->league[$dl]){
				$leagues 			= $this->league[$dl];

				if($numbers->t==0) $numbers->t=1; //no tweets or outta system

				$RTCount 		= $numbers->rt>0?$numbers->rt:1;
				$totalRT		= $numbers->total_rt;
			 	$maxRT			= $numbers->max_rt>0?$numbers->max_rt:1;
				// $maxRT		= 0;
				/*
				// Not sure to use.
				
				for ($rco=1; $rco < 10; $rco++) { 
					if(!isset($data[10+$rco]) || $data[10+$rco]>0){
						$maxRT = $data[10+$rco-1];
						break;
					}
				}*/
				
				
				// calculate values
				$FRQ		= ($numbers->t) / ($this->expectedDailyTweet*$this->dayCount);
				$RTP		= ($RTCount / $numbers->t);
				$RTPT		= ($totalRT / $numbers->t);
				$GTP		= ($RTPT / ($maxRT>$this->greatTweet?$this->greatTweet:$maxRT));
				$ARTCpRTT	= $totalRT / $RTCount;
				
				$ret = array(
					"league"=>$leagues,
					"tweet"=>$numbers->t,
					"rt"=>$numbers->rt,
					"totalRT"=>$numbers->total_rt,
					"follower"=>$numbers->followers_count,
					"FRQ" 	=> $FRQ,
					"RTP" 	=> $RTP,
					"RTPT" 	=> $RTPT,
					"GTP"	=> $GTP,
					"ARC" => $ARTCpRTT,
					"maxRT"  => $maxRT,
					"RTCount"	=> $RTCount,
					"OS" => $osValues["originalSum"]
				);
				
				$tc = new TwitterNumbers();
				$tc->load($u);
				$fields = array(
					"league",
					"tweet",
					"rt",
					"totalRT",
					"follower",
					"FRQ" ,
					"RTP" ,
					"RTPT",
					"GTP" ,
					"ARC" ,
					"FRQAvgd" ,
					"RTPAvgd" ,
					"RTPTAvgd",
					"GTPAvgd" ,
					"ARCAvgd" ,
					"maxRT"  ,
					"RTCount",
					"OS",
					"localCost",
					"remoteCost"
				);
				
				if($tc->id>0){
					//save the current to history.
					$tch = new TwitterNumbersHistory();
					
					foreach ($fields as $field) {
						$tch->$field = $tc->$field;
					}
					$tch->period = date("d-m-Y",time());
					$tch->save();
					$tch->saveUser($u);
				}
				else
					$tc = new TwitterNumbers();
					
				//save to db
				foreach ($fields as $field) {
					if(isset($ret[$field]))
						$tc->$field = $ret[$field];
				}
				$tc->save();
				$tc->saveUser($u);
				
				unset($u);
				
				return $ret;
			}
		}
	}
	
	/**
	 * calculate averages
	 * must be called after calculation method for all users is called.
	 * TODO : calculate on the DB .
	 *
	 * @return array
	 * @author Özgür Köy
	 **/
	public function calculateAverages()
	{

		$tc = new TwitterNumbers();
		$tc->nopop()->load();

		$leagueCollection = $avgs = array();
		
		while ($tc->populate()) {
			if(!isset($leagueCollection[$tc->league])) $leagueCollection[$tc->league] = array();
			
			$leagueCollection[$tc->league][] = array(
				"follower"=>$tc->follower,
				"FRQ" 	=> $tc->FRQ,
				"RTP" 	=> $tc->RTP,
				"RTPT" 	=> $tc->RTPT,
				"GTP"	=> $tc->GTP,
				"ARTCpRTT" => $tc->ARC,
				"maxRT"  => $tc->maxRT,
				"RTCount"	=> $tc->RTCount
			);
		}
		
		foreach ($leagueCollection as $le => $legs) {
			$avg = new TwitterAverages();
			
			$avg->league = $le;
			$avg->load();			
			if($avg->id>0){
				$tah = new TwitterAveragesHistory();
				$tah->league	= $le;
				$tah->period 	= date("d-m-Y",time());
				$tah->follower	= $avg->follower;
				$tah->FRQ 		= $avg->FRQ;
				$tah->RTP 		= $avg->RTP;
				$tah->RTPT 		= $avg->RTPT;
				$tah->GTP		= $avg->GTP;
				$tah->ARTCpRTT 	= $avg->ARC;
				$tah->maxRT= $avg->maxRT;
				$tah->RTCount	= $avg->RTCount;
				$tah->save();
			}
			else{
				$avg = new TwitterAverages();
				$avg->league = $le;
				$avg->period = date("d-m-Y",time());
			}
			
			$biggest	= array();
			$avgs[$le] 	= array();
			$temp0 		= array();

			$temp0 = array(
				"follower"=>0,
				"FRQ" 	=> 0,
				"RTP" 	=> 0,
				"RTPT" 	=> 0,
				"GTP"	=> 0,
				"ARTCpRTT" => 0,
				"maxRT"  => 0,
				"RTCount"	=> 0
			);

			foreach ($legs as $leg) {
				foreach ($leg as $lk => $lv) {
					if(is_numeric($lv)){
						$temp0[$lk] = $temp0[$lk]+$lv;
						// reach for biggest
						if(!isset($biggest[$lk])||$biggest[$lk]<$lv)
							$biggest[$lk] = $lv;
					}
				}
			}
			
			foreach ($temp0 as $t0 => $t1) {
				//exclude biggest ? 
				$t1 -= sizeof($legs)>1 && $this->excludeGreatestFromAverage ? $biggest[$t0] : 0;
				$t1 = $t1/(sizeof($legs)-(sizeof($legs)>1 && $this->excludeGreatestFromAverage?1:0));
				
				$avg->$t0 = $t1;
			}
			
			$avg->save();

			$temp0["count"] = sizeof($legs);

			$avgs[$le] = $temp0;
		}
		
		return $avgs;
	}
	
	/**
	 * get averages per league
	 *
	 * @return TwitterAverages
	 * @author Özgür Köy
	 **/
	public function getAveragesPerLeague($league)
	{
		$avg = new TwitterAverages();
		$avg->league = $league;
		$avg->load();
		
		if($avg->id>0){
			return $avg;
		}
		
		
	}
	/**
	 * get user numbers from db
	 *
	 * @param int $userId 
	 * @return TwitterNumbers
	 * @author Özgür Köy
	 */
	public function getUserNumbers($userId)
	{
		$u = new User($userId);
		
		$tc = new TwitterNumbers();
		$tc->load($u);
		
		return $tc;
	}
	
	/**
	 * calculate user cost, must be called after some average is present.
	 *
	 * @param int $userId 
	 * @param float $min 
	 * @param float $max 
	 * @return array
	 * @author Özgür Köy
	 */
	public function calculateUserCost($userId,$minCostLimit=0,$maxCostLimit=0)
	{
		global $debugLevel;
		if(!isset($userId)) die("no userid");
		
		$uval = $this->getUserNumbers($userId);
		$user = new User($userId);
		
		/*
		if(!(strlen($user->email)>0)){
			if($debugLevel>0) {
				echo "calculateUserCost NO USER : $userId\n";
			}
			
			return false;
		}
		*/
		
		
		if(!$user->count || $user->utype != "p"){
			if($debugLevel>0) {
				Tool::echoLog("calculateUserCost NO USER : $userId");
			}
			die("no user");
			return false;
		}
		
		$u = new TwitterNumbers($user);
		
		if( !($u->id>0) && $debugLevel>0) {
			Tool::echoLog("calculateUserCost NO TWITTER NUMBER FOUND : $userId");
		}
		
		
		$ret = array();
		$leagueAvgs	= $this->getAveragesPerLeague($uval->league);
		
		/*
			LOCAL
		*/
		//frq
		$averageFrq = $leagueAvgs->FRQ;
		$u->FRQAvgd = $ret["FRQScore"]  = $this->averager($averageFrq,$uval->FRQ,$this->multipliers["LOCAL"]["FRQ"],null,$this->minFRQValue);
		
		//RTPT
		$averageRtpt = $leagueAvgs->RTPT;
		$u->RTPTAvgd = $ret["RTPTScore"] = $this->averager($averageRtpt,$uval->RTPT,$this->multipliers["LOCAL"]["RTPT"],$this->maxRTPTValue);
		
		//GTP
		$averageGtp = $leagueAvgs->GTP;
		$ret["LGTPScore"] = $this->gtpAvg($averageGtp,$uval->GTP,$this->multipliers["LOCAL"]["GTP"]);

		//local score
		$localScore = $uval->follower * $ret["LGTPScore"] * $ret["FRQScore"] * $ret["RTPTScore"];
		if($localScore>$uval->follower) $localScore = $uval->follower;


		/*
			REMOTE
		*/
		// RCC Ratio
		$osRate = $this->getOSRate($uval->OS,$uval->follower);

		//GTP
		$averageGTP = $leagueAvgs->GTP;
		$u->GTPAvgd =$ret["RGTPScore"] = $this->gtpAvg($averageGtp,$uval->GTP,$this->multipliers["REMOTE"]["GTP"]);
		
		//ARTCpRTT
		$averageARTCpRTT = $leagueAvgs->ARC;
		$u->ARCAvgd = $ret["ARTCpRTT"] =$this->averager($averageARTCpRTT,$uval->ARC,$this->multipliers["REMOTE"]["ARTCpRTT"],null,$this->minARCValue);
		
		//maxRT
		$averagemaxRT = $leagueAvgs->maxRT;
		$ret["maxRT"] =$this->averager($averagemaxRT,$uval->maxRT,$this->multipliers["REMOTE"]["maxRT"]);
		
		//RTP
		$averageRTP = $leagueAvgs->RTP;
		$u->RTPAvgd =$ret["RTP"] =$this->averager($averageRTP,$uval->RTP,$this->multipliers["REMOTE"]["RTP"],null,$this->minRTPValue);
		

		//remote score
		$remoteScore = $osRate * $ret["RGTPScore"] * $ret["ARTCpRTT"] * $ret["RTP"]/* * $ret["maxRT"] //<-not used. inconsistent */;

		$remoteCost = 1;
		$pos = 0;
		
		//calculate remote cost
		foreach ($this->remoteCosts as $ll=>$lk) {
			if($osRate<$ll){	
				$pos = $ll;
				$remoteCost=$lk;
				break;
			}
		}
		
		$finalScore = ($localScore*$this->followerCost)+($remoteScore*$remoteCost);

		//control cost limits per follower
		foreach ($this->costLimits as $cmax => $cvals) {
			if($uval->follower<$cmax){
				// over the ceiling. lower it.
				if($finalScore>$cvals[1]) {
					$finalScore = $cvals[1]+(rand(-500,500)/1000);
					$u->maxMinOp = "max";
				}
					
				// below the floor. raayz
				if($finalScore<$cvals[0]) {
					$finalScore = $cvals[0]+(rand(-500,500)/1000);
					$u->maxMinOp = "min";
				}
					
				break;
			}
		}
		
		$u->localCost = $localScore * $this->followerCost;
		$u->remoteCost = $remoteCost * $remoteScore;
		
		$u->save();

		$finalScore = $ratedScore = $finalScore<$this->minCostAllowed?$this->minCostAllowed:$finalScore;
		
		if($minCostLimit>0 && $finalScore<$minCostLimit)
			$ratedScore = $minCostLimit + (rand(-4000,4000)/1000);
			
		if($maxCostLimit>0 && $finalScore>$maxCostLimit)
			$ratedScore = $maxCostLimit + (rand(-4000,4000)/1000);
			
		return array("cost"=>$ratedScore, "originalCost"=>$finalScore);
	}
	
} // END class Calc
?>