<?php

class EuroMessage
{
	/**
	* euro.message webservice URL
	*
	* @var string
	**/
	public static $serviceUrl="http://max.euromsg.com/emwsml/";
	/**
	* euro.message webservice username
	*
	* @var string
	**/
	public static $username="admingle_max_wsuser";

	/**
	* euro.message webservice password
	*
	* @var string
	**/
	public static $password="@dM1nglep@$$";

	/**
	* euro.message webservice ticket for all operations
	*
	* @var string
	**/
	private $ticket=null;

	/**
	* Constructor
	*
	* @return void
	**/
	function __construct() {
		global $debugLevel;
		if ($this->ticket==null){
			try {
				$memcache = new Memcache;
				$memcache->connect('localhost', 11211);

				if (!($this->ticket = $memcache->get('ServiceTicket'))) {
					self::Login();
				}else{
					/*
					if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
						Tool::echoLog("ServiceTicket readed from memcache");
					}
					*/
				}
				$memcache->close();
				unset($memcache);
			} catch(Exception $ex){
				if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
					Tool::echoLog("ServiceTicket can not read from memcache");
				}
				self::Login();
			}
		}
    }

	/**
	* Login to euro.message webservice and create a ticket
	*
	* @return void
	**/
	private function Login(){
		global $debugLevel;
		$wsLogin = new SoapClient(self::$serviceUrl . "auth.asmx?wsdl");
		$this->ticket = $wsLogin->Login((object)array("Username"=>self::$username,"Password"=>self::$password))->LoginResult->ServiceTicket;
		try {
			$memcache = new Memcache;
			$memcache->connect('localhost', 11211);
			$memcache->set('ServiceTicket', $this->ticket, MEMCACHE_COMPRESSED, 86400);
			if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
				Tool::echoLog("ServiceTicket wrote to memcache : " . $this->ticket);
			}
			$memcache->close();
			unset($memcache);
		} catch(Exception $ex){ }
		unset ($wsLogin);
	}

	/**
	* Check transactional mail status over euro.message webservice
	*
	* @return PostID of the sent mail
	**/
	public function CheckTMail($PostID){
		global $debugLevel;
		$wsPost = new SoapClient(self::$serviceUrl . "post.asmx?wsdl");

		$checkRequest = new EMCheckRequest();
		$checkRequest->ServiceTicket = $this->ticket;
		$checkRequest->PostID = $PostID;

		$rPost = $wsPost->GetPostResult($checkRequest)->GetPostResultResult;
		if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
			Tool::echoLog("euro.message webservice result: ");
			print_r($rPost);
		}

		if (intval($rPost->Code) != 0){
			if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
				Tool::echoLog("Trying again");
				print_r($rPost);
			}
			self::Login();
			$checkRequest->ServiceTicket = $this->ticket;
			if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
				Tool::echoLog("request body: ");
				print_r($checkRequest);
			}
			$rPost = $wsPost->GetPostResult($checkRequest)->GetPostResultResult;
			if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
				Tool::echoLog("euro.message webservice result: ");
				print_r($rPost);
			}
		}
		unset($wsPost,$checkRequest);

		return $rPost;
	}

	/**
	* Send transactional mail over euro.message webservice
	*
	* @return PostID of the sent mail
	**/
	public function SendTMail($to,$subject,$body,$singleMail=true){
		global $debugLevel;
		$wsPost = new SoapClient(self::$serviceUrl . "post.asmx?wsdl");

		$emailRequest = new EMPostRequest();
		$emailRequest->ServiceTicket = $this->ticket;
		$emailRequest->ToEmailAddress = $to;
		$emailRequest->Subject = $subject;
		$emailRequest->HtmlBody = $body;

		$rPost = $wsPost->PostHtml($emailRequest)->PostHtmlResult;
		if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
			Tool::echoLog("euro.message webservice result: ");
			print_r($rPost);
		}
		$PostID = -1;

		if (intval($rPost->Code) != 0){
			if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
				Tool::echoLog("Trying again");
				print_r($rPost);
			}
			self::Login();
			$emailRequest->ServiceTicket = $this->ticket;
			$rPost = $wsPost->PostHtml($emailRequest)->PostHtmlResult;
			if (isset($debugLevel) && $debugLevel != null && $debugLevel > 0){
				Tool::echoLog("euro.message webservice result: ");
				print_r($rPost);
			}
		}
		$PostID = (strlen($rPost->PostID)== 32) ? $rPost->PostID : json_decode($rPost);
		unset($wsPost,$emailRequest,$rPost);
		if ($singleMail){
			//self::Logout();
		}
		return $PostID;
	}

	/**
	* Logout from euro.message webservice
	*
	* @return PostID of the sent mail
	**/
	public function Logout(){
		$wsLogin = new SoapClient(self::$serviceUrl . "auth.asmx?wsdl");
		$wsLogin->Logout((object)array("ServiceTicket"=>$this->ticket));
		unset ($wsLogin);
	}


}

class EMPostRequest
{
	/**
	* ServiceTicket
	*
	* @var string
	**/
	public $ServiceTicket;
	/**
	* FromName
	*
	* @var string
	**/
	public $FromName = "adMingle";
	/**
	* FromAddress
	*
	* @var string
	**/
	public $FromAddress = EMAIL_FROM_ADDRESS;
	/**
	* ReplyAddress
	*
	* @var string
	**/
	public $ReplyAddress = EMAIL_NOREPLY_ADDRESS;
	/**
	* Subject
	*
	* @var string
	**/
	public $Subject;
	/**
	* HtmlBody
	*
	* @var string
	**/
	public $HtmlBody;
	/**
	* Charset
	*
	* @var string
	**/
	public $Charset = "UTF-8";
	/**
	* ToName
	*
	* @var string
	**/
	public $ToName = "";
	/**
	* ToEmailAddress
	*
	* @var string
	**/
	public $ToEmailAddress;
	/**
	* Attachments
	*
	* @var binary
	**/
	public $Attachments;
}



class EMCheckRequest
{
	/**
	* ServiceTicket
	*
	* @var string
	**/
	public $ServiceTicket;
	/**
	* PostID
	*
	* @var string
	**/
	public $PostID;
}
/*
ini_set('error_reporting', E_ALL);
error_reporting(-1);
$debugLevel = 20;
$em = new EuroMessage();
$r = $em->SendTMail("murat@maxiasp.net", "test", "<html><head></head><body>Hello World!</body></html>");
echo $r;
*/
?>
