<?php
/**
* smarty functions class
*/
class UserSmarty
{
	public static function getSessionParams( $params, &$smarty ) {
		$smarty->assign( "_session", $_SESSION );
	}

	public static function haltt($params=null,&$smarty=null){
		exit;
	}

	/**
	 * check if https
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function isSecurePage($params,&$smarty)
	{
		if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=="on"){
			$kf = $smarty->assign("isSecurePage",1);
			return;
		}

		$kf = $smarty->assign("isSecurePage",0);

	}

	/**
	 * check if online
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function isOnline($params,&$smarty)
	{
		if(isset($_SESSION['userId'])){
			$kf = $smarty->assign("isOnline",1);
			return;
		}

		$kf = $smarty->assign("isOnline",0);

	}

	public static function callUserFunction( $params, &$smarty ) {
		call_user_func( array( $params[ "obj" ], $params[ "m" ] ) );
	}

	/**
	 * to upper
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function toUpper($params,&$smarty)
	{
		$str = $params["s"];

		// if($_SESSION['lang'] != "TR") return strtoupper($str);


		$str = str_replace("ğ","Ğ",$str);
		$str = str_replace("ü","Ü",$str);
		$str = str_replace("ş","Ş",$str);
		$str = str_replace("i","İ",$str);
		$str = str_replace("ı","I",$str);
		$str = str_replace("ö","Ö",$str);
		$str = str_replace("ç","Ç",$str);
		return strtoupper($str);

	}

    public static function tr_strtoupper($metin) {
		$metin = strtr($metin, "ığüşiöç", "IĞÜŞİÖÇ");
		return strtoupper($metin);
	}
	/**
	 * ucwords tr
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function ucwordsTR($params,&$smarty)
	{
		$str = $params["s"];

		// $str[0] = strtoupper(strtr($str[0], "ığüşiöç", "IĞÜŞİÖÇ"));
		$str[0] = str_replace("ş","Ş",$str[0]);



		for ($i = 0; $i < strlen($str); $i++) {
			if (($str[$i] == " " || $str[$i] == "(") && isset($str[$i + 1]))
				$str[$i + 1] = self::tr_strtoupper($str[$i + 1]);
		}

		return $str;
	}

	/**
	 * get the array value by step
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function moveArrayStep($params,&$smarty)
	{
		$al = sizeof($params["a"]);
		if($al<=$params["key"]) $params["key"] = $al;


		$r0 = reset($params["a"]);



		for ($za=1; $za < $params["key"]; $za++) {
			$r0 = next($params["a"]);
		}

        $smarty->assign("lastArrVal",$r0);

	}

	/**
	 * get form token
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function getFormToken( $params, &$smarty ) {
		$token = JFORM::getFormToken( $params[ "f" ] );

		return '<input type="hidden" name="t0ken" value="'.$token.'" id="t0ken">';
	}

	public static function timestampToDate($params, &$smarty) {
		if(strlen($params['d'])==0)
			return "";
		if(isset($params['l'])) {
			$l = $params['l'];
			if(strtolower($l) == 'tr') {
				return date('d/m/Y', $params['d']);
			}
			elseif ( strtolower($l) == 'en' ) {
				return date('m/d/Y', $params['d']);
			}
			else
				return date($l, $params['d']);

		}
		else {
			return date('d/m/Y H:i', $params['d']);
		}
	}

	public static function timestampToDateWithTime($params, &$smarty) {
		if(isset($params['l'])) {
			$l = strtolower($params['l']);
			if($l == 'tr') {
				return date('d/m/Y (H:i)', $params['d']);
			} elseif($l == 'en') {
				return date('m/d/Y (H:i)', $params['d']);
			}
		} else {
			return date('d/m/Y (H:i)', $params['d']);
		}
	}

	public static function rndNumber($params, &$smarty){
		extract($params);

		$random_number = null;

		if (isset($step)){
			$vals = array();
			for ($i=$in;$i<$out;$i+=$step){
				$vals[] = $i;
			}
			$random_number = $vals[array_rand($vals)];
		} else {
			srand((double) microtime() * 1000000);

			$random_number = rand($in, $out);
		}
		if (isset($assign)) {
			$smarty->assign($assign, $random_number);
		}
		else {
			return $random_number;
		}
	}
	
	/**
	 * Returns the formatted currency for current active country
	 * 
	 * @param type $params
	 * @param type $smarty
	 * @return string
	 * 
	 * @author Murat
	 */
	public static function getFormattedCurrency($params, &$smarty){
		return Format::getFormattedCurrency($params["a"]);
	}

	/**
	 * currency format
	 *
	 * @return string
	 * @author Murat
	 **/
	public static function cformat($params,&$smarty)
	{
		return Format::getFormattedCurrency(floatval($params["n"]),true,isset($params["d"])?$params["d"]:2);
	}

	/**
	 * number format
	 *
	 * @return string
	 * @author Murat
	 **/
	public static function nformat($params,&$smarty)
	{
		return Format::NumberFormat(floatval($params["n"]),isset($params["d"])?$params["d"]:2);
	}

    /**
     * DateTime Format
     *
     * @return string
     * @author Murat
     */
    public static function dformat($params,&$smarty){
            
            $dateFormat = isset($params["f"])?$params["f"]:Format::getDateTimeFormat();

            return date($dateFormat,$params["t"]);
        }

	/**
	 * @param        $params
	 * @param Smarty $smarty
	 * @author Özgür Köy
	 */
	public static function createObjectFromName( $params, Smarty &$smarty ) {
		$object = ucfirst( $params[ "object" ] );
		$o = new $object();
		unset( $object );
		$smarty->assign_by_ref( $params[ "name" ], $o );
	}

	/**
	 * @param        $params
	 * @param Smarty $smarty
	 * @author Özgür Köy
	 */
	public static function runThroughFunction( $params, Smarty &$smarty ) {
		$smarty->assign($params["var"], self::$params[ "function" ]( $params, $smarty ));
	}

	public static function replaceGlobals( $params, Smarty &$smarty ) {
		return Format::replaceGlobals( $params[ "s" ] );
	}

	public static function pubDateFormat( $params, Smarty &$smarty ){
		return date(Setting::getValue("dateFormat"),(is_numeric($params["d"]) ? $params["d"] : strtotime($params["d"])));
	}

	public static function pubTimeFormat( $params, Smarty &$smarty ){
		return date(Setting::getValue("timeFormat"),(is_numeric($params["d"]) ? $params["d"] : strtotime($params["d"])));
	}

	public static function pubDateTimeFormat( $params, Smarty &$smarty ){
		return date(Setting::getValue("dateTimeFormat"),(is_numeric($params["d"]) ? $params["d"] : strtotime($params["d"])));
	}

	/**
	 * format String
	 *
	 * @return string
	 * @author Sam
	 **/
	public static function strformat($params,&$smarty)
	{
		return vsprintf($params["s"], array($params["v"]));
		//return JUtil::getFormattedCurrency(JUtil::NumberFormat($params["n"],isset($params["d"])?$params["d"]:2));
	}

	/**
	 * clear text from special html chars
	 *
	 * @return string
	 * @author Murat
	 **/
	public static function chtml($params,&$smarty)
	{
		return htmlspecialchars($params["t"]);
	}

	public static function addTag( $params, &$smarty ) {
		//language replace
		$params["i"] = preg_replace_callback( '|\[\[(.+?)\]\]|',
			function ( $ma ) use ($smarty){
				if ( isset( $smarty->_tpl_vars[ "words" ][ "cpl_".$ma[1] ] ) ){
					return $smarty->_tpl_vars[ "words" ][ "cpl_".$ma[1] ];}
				else
					return $ma[ 1 ];
			},
			$params[ "i" ]
		);

		preg_match_all( "~<.+?>~smx", $params[ "i" ], $outer );

		foreach ( $outer as $out ) {
			$inner         = $out;
			$params[ "c" ] = preg_replace( "|[,.]|", " ", $params[ "c" ] );
			$inner         = preg_replace( "~<(select|input|span|div|form|label)(.+?)>~smx", '<$1 $2 ' . $params[ "type" ] . '="' . $params[ "c" ] . '">', $inner );
			$params[ "i" ] = str_replace( $out, $inner, $params[ "i" ] );
		}

		return $params[ "i" ];
	}

	public static function readMenu($params, &$smarty ){
		$m = DSmenu::menuList();
		$smarty->assign( "menus", $m );
	}
	public static function readPerms( $params = null, &$smarty = null ) {
		if ( isset( $_SESSION[ "DS_userId" ] ) ) {

			list( $pers, $perms ) = DSpermission::getPermissions();

			$_SESSION[ "DSPerms" ] = $perms;
			$smarty->assign( "isRoot", $_SESSION[ "DS_isRoot" ] );
			$smarty->assign( "userRoles", $_SESSION[ "DS_roles" ] );
			$smarty->assign( "userPerms", $pers );
			$smarty->assign( "perms", $perms );
		}
	}

	public static function booleanYes($params, &$smarty ){
		return $params["v"]==1?"yes":"no";
	}

	public static function formBox( $params, Smarty &$smarty ) {

		$smarty->assign('__label',$params["label"]);
		if(isset($params["name"]))
			$smarty->assign('__name',$params["name"]);
		else
			$smarty->assign('__name',null);

		if ( isset( $params[ "blockCustomAttr" ] ) )
			$smarty->assign( '__blockCustomAttr', $params[ "blockCustomAttr" ] );
		else
			$smarty->assign( '__blockCustomAttr', null );

		if(isset($params["value"]))
			$smarty->assign('__value',$params["value"]);
		else
			$smarty->assign('__value',null);

		if(isset($params["uploadPath"]))
			$smarty->assign('__uploadPath',$params["uploadPath"]);
		else
			$smarty->assign('__uploadPath',null);

		if(isset($params["info"]))
			$smarty->assign('__info',$params["info"]);
		else
			$smarty->assign('__info',null);

		if(isset($params["class"]))
			$smarty->assign('__class',$params["class"]);
		else
			$smarty->assign('__class',null);

		if(isset($params["vkey"])){
			$vkey = explode( ",", $params[ "vkey" ] );
			$smarty->assign( '__vkey', $vkey );
		}
		else
			$smarty->assign('__vkey',null);

		if(isset($params["options"]))
			$smarty->assign('__options',$params["options"]);
		else
			$smarty->assign('__options',null);

		if(isset($params["disabled"]))
			$smarty->assign('__disabled',$params["disabled"]);
		else
			$smarty->assign('__disabled',null);

		if(isset($params["charLimit"]))
			$smarty->assign('__charLimit',$params["charLimit"]);
		else
			$smarty->assign('__charLimit',null);

		if(isset($params["defaultValue"]))
			$smarty->assign('__defaultValue',$params["defaultValue"]);
		else
			$smarty->assign('__defaultValue',null);

		if(isset($params["src"]))
			$smarty->assign('__src',$params["src"]);
		else
			$smarty->assign('__src',null);

		if(isset($params["attached"]))
			$smarty->assign('__attached',$params["attached"]);
		else
			$smarty->assign('__attached',null);

		if(isset($params["nameAsArray"]))
			$smarty->assign('__nameAsArray',true);
		else
			$smarty->assign('__nameAsArray',false);

		if(isset($params["icon"]))
			$smarty->assign('__inputIcon',$params["icon"]);
		else
			$smarty->assign('__inputIcon',null);

		if(isset($params["color"]))
			$smarty->assign('__color',$params["color"]);
		else
			$smarty->assign('__color',null);

		if(isset($params["id"]))
			$smarty->assign('__inputID',$params["id"]);
		elseif(isset($params["name"]))
			$smarty->assign('__inputID',$params["name"]);
		else
			$smarty->assign('__inputID',null);

		if(isset($params["customAttribute"]) && isset($params["attributeValue"])){
			$smarty->assign('__customAttribute',$params["customAttribute"]);
			$smarty->assign('__attributeValue',$params["attributeValue"]);
		} else {
			$smarty->assign('__customAttribute',null);
			$smarty->assign('__attributeValue',null);
		}

		if(isset($params["attributes"]))
			$smarty->assign('__attributes',explode(",",$params["attributes"]));
		else
			$smarty->assign('__attributes',null);

		if(isset($params["multi"]))
			$smarty->assign('__multi',$params["multi"]);
		else
			$smarty->assign('__multi',null);

		if(isset($params["mask"]))
			$smarty->assign('__mask',$params["mask"]);
		else
			$smarty->assign('__mask',null);

		if(isset($params["labels"])){
			$smarty->assign('__labels',$params["labels"]);
		} else {
			$smarty->assign('__labels',null);
		}

		if(isset($params["updateBtnID"])){
			$smarty->assign('__updateBtnID',$params["updateBtnID"]);
		} else {
			$smarty->assign('__updateBtnID',null);
		}

		return $smarty->fetch('necessary/formPieces/_'.$params["type"].'.tpl');
	}

	public static function breadCrumb( $params, Smarty &$smarty ) {
		$left  = isset( $params[ "left" ] ) ? (array)explode( ";", $params[ "left" ] ) : array();
		$right = isset( $params[ "right" ] ) ? (array)explode( ",", $params[ "right" ] ) : array();

		foreach ( $left as $l0 => &$l1 )
			$l1 = explode( ",", $l1 );


		$smarty->assign( 'left', $left );
		$smarty->assign( 'right', $right );

		return $smarty->fetch( 'DS/_breadCrumb.tpl' );

	}
	public static function yesNo( $params, Smarty &$smarty ) {
		return $params["a"]==1?"yes":"no";
	}

	public static function timeAgo( $params, Smarty &$smarty ) {
		$tm = $params[ "time" ];
		$rcs = 0 ;

		$cur_tm = time(); $dif = $cur_tm-$tm;
		$pds = array('second','minute','hour','day','week','month','year','decade');
		$lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
		for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);

		$no = floor($no); if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
		if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
		return $x;

	}

	public static function previewBug( $params, Smarty &$smarty ) {
//		var_dump( $params[ "detail" ] );
//		echo preg_replace("/(^>.+^)([^>])/uiUsm", "<div class=\"preview\">\n$1\n</div>$2", $params['detail']);
//		exit;
		$params[ "detail" ] .= PHP_EOL . " "; #PATCH FOR COMMENTS
		$params[ "detail" ] = preg_replace( "/(^>.+)^([^>])/uiUsm", "<div class=\"bugPreview\">\n$1\n</div>$2", $params[ 'detail' ] . PHP_EOL );
		$params[ "detail" ] = preg_replace( "/(http(?:s)?:\/\/[^\\s]+)/uism", "<a href=\"$1\" target='_blank'>$1</a>", $params[ "detail" ] );
		$params[ "detail" ] = preg_replace( "~(\n){3,}~s", "\n", $params[ "detail" ] ); #
		return nl2br( $params[ "detail" ] );
	}

	public static function bugEmailDetail( $params, Smarty &$smarty ) {
//		var_dump( $params[ "detail" ] );
//		echo preg_replace("/(^>.+^)([^>])/uiUsm", "<div class=\"preview\">\n$1\n</div>$2", $params['detail']);
//		exit;
		$params[ "detail" ] .= PHP_EOL . " "; #PATCH FOR COMMENTS
		$params[ "detail" ] = preg_replace( "/(^>.+)^([^>])/uiUsm", "$2", $params[ 'detail' ] . PHP_EOL );
		$params[ "detail" ] = preg_replace( "/(http(?:s)?:\/\/[^\\s]+)/uism", "<a href=\"$1\" target='_blank'>$1</a>", $params[ "detail" ] );
		$params[ "detail" ] = preg_replace( "~(\n){3,}~s", "\n", $params[ "detail" ] ); #
		$params[ "detail" ] = preg_replace( "/\\[image:.+?\\]/uism", "", $params[ "detail" ] ); #inline image markers
		return nl2br( $params[ "detail" ] );
	}

}
