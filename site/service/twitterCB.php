<?php
require_once '../def/constants.php';
require_once $__DP.'core/run/exec.php';

if(!isset($_SESSION[ "adTwitterRequest" ])){
	JUtil::jredirect( "dashboard", true );}

#TODO : DO INVITE JOBS?
if (ENABLE_INVITE == 1 || ENABLE_INVITE == 2) { // just invite mode
	if(isset($_SESSION['oauth_status']) && $_SESSION['oauth_status'] == 'denied') {
		$_SESSION['twitterStatus'] = "denied";
	}elseif ($result) {
            // new user added
            $_SESSION['twitterStatus'] = "newUser";
            
        }else {
            // Exist User 
            $_SESSION['twitterStatus'] = "existUser";
        }
        
        JUtil::jredirect('invite');
        die();
}

$tw = new SnTwitter();

#twitter data is set here to object
if ( isset( $_REQUEST[ 'denied' ] ) || $tw->callback( $_GET[ "oauth_token" ], $_GET[ "oauth_verifier" ] ) !== true ) {
	unset( $_SESSION[ "adTwitterRequest" ] );
	$_SESSION[ "adSocialError" ] = "connectionProblem";
}
else {
	unset( $_SESSION[ "adTwitterRequest" ] );
	if ( !isset( $_SESSION[ "assignSN" ] ) )
		$_SESSION[ "assignSN" ] = array();

	if ( isset( $_SESSION[ "adPublisherId" ] ) ) {
		#adding
		$twResponse = $tw->addAccount( $tw->verifiedTokens[ "oauth_token" ], $tw->verifiedTokens[ "oauth_token_secret" ] );
		if ( is_bool( $twResponse ) ) {
			// cant add user
			$_SESSION[ "adSocialError" ] = "socialAccountConnectedToAnotherUser";
		}
		else {
			$avatar                              = SnTwitter::getAvatarImage( $tw->savedAvatar );
			$_SESSION[ "assignSN" ][ "twitter" ] = array( false, null, $avatar );

			$p = new Publisher( $_SESSION[ "adPublisherId" ] );
			$p->addSnTwitter( $twResponse );
			unset( $p );
		}
	}
	elseif ( !is_null( $tw->verifiedTokens ) ) {
		#login
		$twResponse = $tw->login( $tw->twitterInfo->id, $tw->verifiedTokens[ "oauth_token" ], $tw->verifiedTokens[ "oauth_token_secret" ] );
		$avatar = SnTwitter::getAvatarImage( $tw->savedAvatar );

		if ( !is_bool($twResponse) && $twResponse->publisher_snTwitter && $twResponse->publisher_snTwitter->gotValue ) {
			$_SESSION[ "assignSN" ][ "twitter" ] = array( false, null, $avatar );

			JUtil::jredirect( "dashboard", true ); #goood
		}
		else {
			$_SESSION[ "assignSN" ][ "twitter" ] = array( $tw->savedId, $tw->savedUsername, $avatar );

			JUtil::jredirect( "signup-publisher", true );
		}

	}
	else{
		JUtil::jredirect( "signup-publisher", true ); #shieeet
	}
}
unset( $_SESSION[ "oauth_token" ], $_SESSION[ "oauth_token_secret" ] );
JUtil::jredirect( "social" );