<?php
require_once '../def/constants.php';
require_once $__DP . 'core/run/exec.php';
$i = new SnInstagram();
if ( isset( $_GET[ "code" ] ) === true && $i->getAccessToken( $_GET[ "code" ] ) === true && ( $info = $i->getInfo() ) !== false ) {
	if ( !isset( $_SESSION[ "assignSN" ] ) )
		$_SESSION[ "assignSN" ] = array();

	if ( !isset( $_SESSION[ "adPublisherId" ] ) ) {
		#try login
		$loginCheck = $i->login( $i->iid, $info );
		if ( $loginCheck === false ) {
			$avatar                         = SnTwitter::getAvatarImage( $i->savedAvatar );
			$_SESSION[ "assignSN" ][ "Instagram" ] = array( $i->savedId, null, $avatar );
		}

		JUtil::jredirect( $loginCheck !== false ? "dashboard" : "signup-publisher", true ); #new user with no Instagram connection
	}

	$p = new Publisher( $_SESSION[ "adPublisherId" ] );

	#now we are trying to connect
	$accountReggd                   = $i->addAccount( $i->iid, $info );
	$avatar                         = SnTwitter::getAvatarImage( $i->savedAvatar );
	$_SESSION[ "assignSN" ][ "Instagram" ] = array( false, null, $avatar );

	if ( $accountReggd == false ) {
		$_SESSION[ "adSocialError" ] = "connectionProblem";
		JUtil::jredirect( "dashboard", true ); #some problem
	}

	if ( is_null( $i->publisher_snInstagram ) ) {
		#we have a logged in user with an unregistered VK account
		$p->addSnInstagram( $i );
		unset( $p );
	}
	else if ( $i->publisher_snInstagram->id != $_SESSION[ "adPublisherId" ] ) {
		$_SESSION[ "adSocialError" ] = "socialAccountConnectedToAnotherUser";
		#can't add dude
	}
}
else{
	$_SESSION[ "adSocialError" ] = "connectionProblem";
}
JUtil::jredirect( "social" );