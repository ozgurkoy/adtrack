<?php

require_once '../../def/constants.php';
require_once $__DP.'core/run/exec.php';
require_once $__DP.'/site/service/facebook/facebook.php';

$f = new SnFacebook();

if(isset($_GET['error_code'])){
	if(isset($_SESSION["adPublisherId"])){
		$p = new Publisher( $_SESSION[ "adPublisherId" ] );
		$p->writeHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "Facebook login failed", UserHistory::ENUM_MADEBY_USER, serialize( $_GET ) );

		$_SESSION["adSocialError"] = "connectionProblem";

		JUtil::jredirect( "social" );
	}
	else{
		JUtil::jredirect( "signup-publisher", true ); #some problem
	}
}
else{
	if ( !isset( $_SESSION[ "assignSN" ] ) )
		$_SESSION[ "assignSN" ] = array();

	$facebookData      = $f->getUserInfoFromToken();
	if ( isset( $_SESSION[ "adPublisherId" ] ) ) {
		#connect
		$fbResponse = SnFacebook::addFacebookAccount( $facebookData["id"], $facebookData["token"] );

		if ( is_bool( $fbResponse ) )
			$_SESSION[ "adSocialError" ] = "socialAccountConnectedToAnotherUser";
		else {
			$p = new Publisher( $_SESSION[ "adPublisherId" ] );
			$p->addSnFacebook( $fbResponse );

			$avatar                               = SnFacebook::getAvatarImage( SnFacebook::$savedFBId );
			$_SESSION[ "assignSN" ][ "facebook" ] = array( false, null, $avatar );

			unset( $p );
		}

	}
	else {
		#login
		$fbResponse = SnFacebook::loginFacebookAccount( $facebookData["id"], $facebookData["token"] );
		$avatar = SnFacebook::getAvatarImage( SnFacebook::$savedFBId );

		if ( is_bool( $fbResponse ) ) {
			$_SESSION[ "assignSN" ][ "facebook" ] = array( SnFacebook::$savedId, SnFacebook::$savedUsername, $avatar );

			JUtil::jredirect( "signup-publisher", true );
		}
		else{
			$_SESSION[ "assignSN" ][ "facebook" ] = array( false, null, $avatar );

			JUtil::jredirect( "dashboard", true ); #some problem
		}
	}

}
JUtil::jredirect( "social" );


?>