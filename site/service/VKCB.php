<?php
require_once '../def/constants.php';
require_once $__DP . 'core/run/exec.php';
$v = new SnVK();

if ( isset( $_GET[ "code" ] ) === true && $v->getAccessToken( $_GET[ "code" ] ) === true && ( $info = $v->getInfo() ) !== false ) {
	if ( !isset( $_SESSION[ "assignSN" ] ) )
		$_SESSION[ "assignSN" ] = array();

	if ( !isset( $_SESSION[ "adPublisherId" ] ) ) {
		#try login
		$loginCheck = $v->login( $v->vkid, $info );
		if ( $loginCheck === false ) {
			$avatar                         = SnTwitter::getAvatarImage( $v->savedAvatar );
			$_SESSION[ "assignSN" ][ "VK" ] = array( $v->savedId, null, $avatar );
		}

		JUtil::jredirect( $loginCheck !== false ? "dashboard" : "signup-publisher", true ); #new user with no VK connection
	}

	$p = new Publisher( $_SESSION[ "adPublisherId" ] );

	#now we are trying to connect
	$accountReggd                   = $v->addAccount( $v->vkid, $info );
	$avatar                         = SnTwitter::getAvatarImage( $v->savedAvatar );
	$_SESSION[ "assignSN" ][ "VK" ] = array( false, null, $avatar );

	if ( $accountReggd == false ) {
		$_SESSION[ "adSocialError" ] = "connectionProblem";
		JUtil::jredirect( "dashboard", true ); #some problem
	}

	if ( is_null( $v->publisher_snVK ) ) {
		#we have a logged in user with an unregistered VK account
		$p->addSnVK( $v );
		unset( $p );
	}
	else if ( $v->publisher_snVK->id != $_SESSION[ "adPublisherId" ] ) {
		$_SESSION[ "adSocialError" ] = "socialAccountConnectedToAnotherUser";
		#can't add dude
	}
}
else
	$_SESSION[ "adSocialError" ] = "connectionProblem";

JUtil::jredirect( "social" );