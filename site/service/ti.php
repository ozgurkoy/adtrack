<?php
session_start();
include "../../core/helper/JUtil.php";
include "../../site/model/Encoding.php";
error_reporting(E_ALL);
ini_set("display_errors",1);

$s = $_GET[ "s" ];
$string = JUtil::getDecrypted( $s );
// mb_internal_encoding('utf-8');
// mb_http_output('utf-8');


// Set the content-type
header('Content-Type: image/png');

// Create the image
$im = imagecreatetruecolor(400, 30);

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
imagefilledrectangle($im, 0, 0, 399, 29, $white);

putenv('GDFONTPATH=' . realpath('.'));
// The text to draw
// $text = 'Testing...';
// Replace path by your own font path
$font = 'Arial.ttf';

// Add some shadow to the text
// imagettftext($im, 20, 0, 11, 21, $grey, $font, $string);

// Add the text
imagettftext($im, 12, 0, 10, 20, $black, $font, $string);

// Using imagepng() results in clearer text compared with imagejpeg()
imagepng($im);
imagedestroy($im);