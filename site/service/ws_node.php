<?php
header("Content-Type:text/plain");

/**
 * Indents a flat JSON string to make it more human-readable.
 *
 * @param string $json The original JSON string to process.
 *
 * @return string Indented version of the original JSON string.
 */
function jsonIndent($json) {

	$result      = '';
	$pos         = 0;
	$strLen      = strlen($json);
	$indentStr   = '  ';
	$newLine     = "\n";
	$prevChar    = '';
	$outOfQuotes = true;

	for ($i=0; $i<=$strLen; $i++) {

		// Grab the next character in the string.
		$char = substr($json, $i, 1);

		// Are we inside a quoted string?
		if ($char == '"' && $prevChar != '\\') {
			$outOfQuotes = !$outOfQuotes;

			// If this character is the end of an element,
			// output a new line and indent the next line.
		} else if(($char == '}' || $char == ']') && $outOfQuotes) {
			$result .= $newLine;
			$pos --;
			for ($j=0; $j<$pos; $j++) {
				$result .= $indentStr;
			}
		}

		// Add the character to the result string.
		$result .= $char;

		// If the last character was the beginning of an element,
		// output a new line and indent the next line.
		if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
			$result .= $newLine;
			if ($char == '{' || $char == '[') {
				$pos ++;
			}

			for ($j = 0; $j < $pos; $j++) {
				$result .= $indentStr;
			}
		}

		$prevChar = $char;
	}

	return $result;
}

$userData = array("username" => "string","password" => "string");
//$userData = array("username" => "hardc0der","password" => "123456");

require_once('/Users/ozgurkoy/Apache/admingle1.5/site/model/RestReq.php');

$request = new RestReq("http://n.admingle.com/wsv2/user/login", "POST",
'{
  "username": "' . $userData["username"] . '",
  "password": "' . $userData["password"] . '",
  "device": {
    "device_name": "string",
    "device_Platform": "string",
    "device_UUID": "string",
    "device_Model": "string",
    "device_Version": "string",
    "device_Token": "string",
    "device_Manufacturer": "string"
  }
}'
,"raw","application/json");

$request->execute();

$token = urlencode(json_decode($request->responseBody)->Token);

$request = new RestReq("http://n.admingle.com/wsv2/campaign/camps?token=$token", "POST",
	'{
	  "campStatus": "o",
	  "orderBy": "date",
	  "page": "1"
	}'
	,"raw","application/json");

$request->execute();

$response = json_decode($request->responseBody);

echo jsonIndent(json_encode($response)) . PHP_EOL . PHP_EOL . PHP_EOL;



$request = new RestReq("http://n.admingle.com/wsv2/campaign/campDetail?token=$token", "POST",
	'{
  "value": "18402a7481b78975fce0d54af9702e69"
}'
	,"raw","application/json");

$request->execute();

$response = json_decode($request->responseBody);

echo jsonIndent(json_encode($response)) . PHP_EOL . PHP_EOL . PHP_EOL;

//exit;
/*
$request = new RestReq("http://n.admingle.com/wsv2/campaign/campAccept?token=$token", "POST",
	'{
  "campID": "104d4109023de4288825d0bbd6201caf",
  "sendDate": "-",
  "sendTime": "-",
  "setCustomMessage": "-",
  "customMessage": "-",
  "messageId": "-"
}'
	,"raw","application/json");*/

$request = new RestReq("http://n.admingle.com/wsv2/campaign/campAccept?token=$token", "POST",
	'{
  "campID": "9425845f736684cb20c17962ef1498fc",
  "sendDate": "2014-01-23",
  "sendTime": "10:30",
  "setCustomMessage": "N",
  "customMessage": "N",
  "messageId": "615"
}'
	,"raw","application/json");

$request->execute();

$response = json_decode($request->responseBody);

echo jsonIndent(json_encode($response)) . PHP_EOL . PHP_EOL . PHP_EOL;

//echo '<pre>' . print_r($request, true) . '</pre>';
?>