<?php

foreach ($_POST as $key => $value) {
	$_POST[$key] = mb_convert_encoding($value, 'UTF-8', 'iso-8859-9');
}

if(isset($_POST['txn_id'])) {
	$txn_id = $_POST['txn_id'];
	$log = new LogPaypal();
	$log->txnId = $txn_id;
	$log->load();
	$log->responseDate = time();
	$log->response = serialize($_POST);
	$log->save();

	if(strtolower($_POST['payment_status']) != 'completed') {
		JLog::log('cri', 'Paypal uncompleted payment : ' . serialize($_POST));
		die();
	} else {
		// budur
		$receiverEmail = $_POST['receiver_email'];
		$outAmount = $_POST['mc_gross'];
		$user = new User();
		$user->paypalEmail = $receiverEmail;
		$user->load();
		if(!$user->id) {
			JLog::log('cri', 'Paypal Masspay IPN wrong user:' . $receiverEmail);
			die();
		}

		if($user->utype == 'a') {
			// no refund to advertiser
			JLog::log('cri', 'Paypal payment to advertiser' . serialize($_POST) . '!!!' . serialize($user));
			die();
		} else {
			$ma = new UserMoneyAction();
			$ma->user = $user->id;
			$ma->amount = $outAmount * (-1);
			$ma->paymentType = 'p';
			$ma->txnId = '---';
			$ma->load();
			if(!$ma->id) {
				JLog::log('cri', 'Paypal IPN did not match:' . $txn_id);
				die();
			}
			$ma->txnId = $txn_id;
			$ma->lastActionDate = time();
			$ma->save();
		}

		unset($user, $ma);

	}

}

/*
 * TEK ÖDEMEYE GELEN 3 cevap (mass payment)
 *
-------------------------------------------------------------------------------------------------
    [txn_type] => masspay
    [payment_gross_1] =>
    [payment_date] => 06:42:16 Mar 23, 2012 PDT
    [last_name] => Koçak
    [mc_fee_1] => 0.00
    [masspay_txn_id_1] => 4KH697140F1311010
    [receiver_email_1] => personal@admingle.com
    [residence_country] => TR
    [verify_sign] => A7DW3VkKiMYdulURGso47RJeLva.AN0LM7AQhpRfBfzcZAfCYeG4znys
    [payer_status] => verified
    [test_ipn] => 1
    [payer_email] => sirket@admingle.com
    [first_name] => Ergün
    [payment_fee_1] =>
    [payer_id] => 92HRNRSZAFKG6
    [payer_business_name] => adMingle
    [payment_status] => Processed
    [status_1] => Completed
    [mc_gross_1] => 12.00
    [charset] => windows-1254
    [notify_version] => 3.4
    [mc_currency_1] => TRY
    [unique_id_1] =>
    [ipn_track_id] => 86a078d21b7e2

---------------- bu dikkate alınır çünkü txn_id var ------------------------------

    [transaction_subject] => adMingle payment
    [txn_type] => send_money
    [payment_date] => 06:42:17 Mar 23, 2012 PDT
    [last_name] => Koçak
    [residence_country] => TR
    [payment_gross] =>
    [mc_currency] => TRY
    [business] => personal@admingle.com
    [payment_type] => instant
    [protection_eligibility] => Ineligible
    [verify_sign] => AP.6Uu2dgrfJ3.TEkXB1sKDSPysHApqjOcqST4CkxBlhroTOTeq5GDq8
    [payer_status] => verified
    [test_ipn] => 1
    [payer_email] => sirket@admingle.com
    [txn_id] => 8YE47257RH872232G
    [receiver_email] => personal@admingle.com
    [first_name] => Ergün
    [payer_id] => 92HRNRSZAFKG6
    [receiver_id] => 96LMWBE2R4DES
    [payer_business_name] => adMingle
    [payment_status] => Completed
    [mc_gross] => 12.00
    [charset] => windows-1254
    [notify_version] => 3.4
    [ipn_track_id] => 2d4b1b5edcd66
------------------------------------------------------------------------------------------------
    [txn_type] => masspay
    [payment_gross_1] =>
    [payment_date] => 06:42:23 Mar 23, 2012 PDT
    [last_name] => Koçak
    [mc_fee_1] => 0.00
    [masspay_txn_id_1] => 4KH697140F1311010
    [receiver_email_1] => personal@admingle.com
    [residence_country] => TR
    [verify_sign] => AJGNZ4CfKBZXDrzG0vCoTfw2mZHWA9rLtrw.EC8ode1znpkXuQfC5eLv
    [payer_status] => verified
    [test_ipn] => 1
    [payer_email] => sirket@admingle.com
    [first_name] => Ergün
    [payment_fee_1] =>
    [payer_id] => 92HRNRSZAFKG6
    [payer_business_name] => adMingle
    [payment_status] => Completed
    [status_1] => Completed
    [mc_gross_1] => 12.00
    [charset] => windows-1254
    [notify_version] => 3.4
    [mc_currency_1] => TRY
    [unique_id_1] =>
    [ipn_track_id] => 86a078d21b7e2
-------------------------------------------------------------------------------------------------
 */