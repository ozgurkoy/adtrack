<?php

$txn_id = $_POST['txn_id'];
$log = new LogPaypal();
$log->responseDate = time();
$log->response = serialize($_POST);
$log->txnId = $txn_id;
$log->save();

$payment_status = $_POST['payment_status'];
if(strtolower($payment_status) != 'completed') {
	JLog::log('cri', 'Paypal uncompleted payment : ' . serialize($_POST));
	die();
}

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
	$value = urlencode(stripslashes($value));
	$req .= "&{$key}={$value}";
}

unset($log);
$log = new LogPaypal();
$log->requestDate = time();
$log->request = 'ssl://'.PAYPAL_VALIDATE_URL.'/'.$req;
$log->txnId = $txn_id;
$log->save();

// post back to PayPal system to validate
$header  = "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
$fp = fsockopen('ssl://'.PAYPAL_VALIDATE_URL, 443, $errno, $errstr, 30);


// assign posted variables to local variables
$item_name = array_key_exists('item_name', $_POST) ? $_POST['item_name'] : '';
$item_number = array_key_exists('item_number', $_POST) ? $_POST['item_number'] : '';
$payment_amount = array_key_exists('mc_gross', $_POST) ? $_POST['mc_gross'] : '';
$payment_currency = array_key_exists('mc_currency', $_POST) ? $_POST['mc_currency'] : '';
$receiver_email = array_key_exists('receiver_email', $_POST) ? $_POST['receiver_email'] : '';
$payer_email = array_key_exists('payer_email', $_POST) ? $_POST['payer_email'] : '';

if (!$fp) {
	// HTTP ERROR
	JLog::log('cri', 'Paypal Validation http error:('.$errno.') '.$errstr);
	die();
} else {
	fputs ($fp, $header . $req);

	while (!feof($fp)) {
		$res = fgets ($fp, 1024);
		$log->response .= $res;
		if (strcmp ($res, "VERIFIED") == 0) {
			// NP
		} elseif (strcmp ($res, "INVALID") == 0) {
			// log for manual investigation
			$log->responseDate = time();
			$log->save();
			JLog::log('cri', 'Paypal Validation error' . $res);
			die();
		}
	}
	$log->responseDate = time();
	$log->save();
	fclose ($fp);
}
unset($log);

$userId = intval($_REQUEST['custom']);
$user = new User($userId);
$user->load();

if(!$user->id) {
	JLog::log('cri', 'Paypal IPN wrong user:' . $userId);
	die();
}

$incomingAmount = $payment_amount;

if($user->utype == 'a') {
	$company = $user->loadCompany();

	$preAmount = $company->recalculateBalance();
	$postAmount = $preAmount + $incomingAmount;

	$ma = new CompanyMoneyAction();
	$ma->customClause = "receiptInfo='{$txn_id}'";
	$ma->load();
	if($ma->id) {
		JLog::log('cri', 'Paypal recurring payment:' . $txn_id);
		die();
	}
	$ma->txnId = $txn_id;
	$ma->amount = $incomingAmount;
	$ma->amountAfter = $postAmount;
	$ma->paymentType = 'p';
	$ma->paymentDate = time();
	$ma->lastActionDate = time();
	$ma->state = 'a';
	$ma->company = $company->id;
	$ma->save();
} else {
	$preAmount = $user->recalculateBalance();
	$postAmount = $preAmount + $incomingAmount;

	$ma = new UserMoneyAction();
	$ma->customClause = "receiptInfo='{$txn_id}'";
	$ma->load();
	if($ma->id) {
		JLog::log('cri', 'Paypal recurring payment:' . $txn_id);
		die();
	}
	$ma->txnId = $txn_id;
	$ma->amount = $incomingAmount;
	$ma->amountAfter = $postAmount;
	$ma->paymentType = 'p';
	$ma->paymentDate = time();
	$ma->lastActionDate = time();
	$ma->state = 'a';
	$ma->user = $user->id;
	$ma->save();
}

unset($company, $user, $ma);

// check that receiver_email is your Primary PayPal email
// check that payment_amount/payment_currency are correct