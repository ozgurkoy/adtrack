<?php
require_once '../def/constants.php';

require_once $__DP.'core/run/exec.php';

if(array_key_exists('custom', $_REQUEST)) {
	require_once 'paypalNotifyCBNormal.php';
} else {
	require_once 'paypalNotifyCBMass.php';
}