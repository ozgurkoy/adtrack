<?php

// TODO : Logging !

require_once '../def/constants.php';

require_once $__DP.'core/run/exec.php';

if (ENABLE_MODULE_YOUTUBE!=1) {
	header("Location:" . $_S . "dashboard");
	exit;
}


if(!isset($_SESSION['adId']) || !isset($_SESSION['adType']) || $_SESSION['adType']!="p"){
	Tool::setLastError("timeOut","sessionTimeOut","sgeneral->dashboard");
	header("Location: ".$_S.LOGIN_PATH);
	exit;
}


// TODO : check username for duplicates !

if(isset($_REQUEST["token"])){
	$u 		= new User($_SESSION['adId']);
	$u->loadYoutube();
	if(is_object($u->youtube) && $u->youtube->id>0){
		$_SESSION['youtubeError'] = "alreadyconnected";
		// header("Location:".$_S."dashboard#youLink");
		header("Location:".$_S."editProfile#youtubeCon");
		exit;
	}
	
	$you 	= new Youtube();
	$ptoken = $you->persistentToken($_REQUEST['token']);

	if($ptoken!==false){
		$you->token = $ptoken;
		$info = $you->getInfo();
		
		if($info==false){
			header("Location:".$_S."editProfile#youtubeCon");
			exit;
		}
		$username = $info["username"];
		
		if($you->checkUsername($username)==true){
			$_SESSION['youtubeError'] = "prevconnected";
		}
		else{
			
			$you->username = $username;
			$you->feed = $info["feed"];
			$you->name = $info["name"];
			
			$you->lastLogin = date("Y-m-d H:i",time());
			$you->save();
		
			$u->saveYoutube($you);
		}
	}
	
}
// header("Location:".$_S."dashboard#youLink");
header("Location:".$_S."editProfile#youtubeCon");


?>