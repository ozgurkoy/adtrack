<?php
require '../def/constants.php';
require $__DP . 'core/run/exec.php';
$x = new SnXING();

if ( $x->processResponse() === true && $x->getInfo() === true ) {
	if ( !isset( $_SESSION[ "assignSN" ] ) )
		$_SESSION[ "assignSN" ] = array();


	if ( !isset( $_SESSION[ "adPublisherId" ] ) ) {
		#try login
		$loginCheck = $x->login( $x->XINGUser, $x->accessToken );
//		print_r( $x->savedAvatar );exit;

		if ( $loginCheck === false ) {
			$avatar                         = SnTwitter::getAvatarImage( $x->savedAvatar );
			$_SESSION[ "assignSN" ][ "XING" ] = array( $x->savedId, null, $avatar );
		}

		JUtil::jredirect( $loginCheck !== false ? "dashboard" : "signup-publisher", true ); #new user with no XING connection
	}

	$p = new Publisher( $_SESSION[ "adPublisherId" ] );

	#now we are trying to connect
	$accountReggd                   = $x->addAccount( $x->XINGUser, $x->accessToken );
	$avatar                         = SnTwitter::getAvatarImage( $x->savedAvatar );
	$_SESSION[ "assignSN" ][ "XING" ] = array( false, null, $avatar );

	if ( $accountReggd == false ) {
		$_SESSION[ "adSocialError" ] = "connectionProblem";
		JUtil::jredirect( "dashboard", true ); #some problem
	}

	if ( is_null( $x->publisher_snXING ) ) {
		#we have a logged in user with an unregistered XING account
		$p->addSnXING( $x );
		unset( $p );
	}
	else if ( $x->publisher_snXING->id != $_SESSION[ "adPublisherId" ] ) {
		$_SESSION[ "adSocialError" ] = "socialAccountConnectedToAnotherUser";
		#can't add dude
	}
}
else
	$_SESSION[ "adSocialError" ] = "connectionProblem";

JUtil::jredirect( "social" );