<?php
require_once '../def/constants.php';
require_once $__DP . 'core/run/exec.php';
$g = new SnGoogle();

if ( Google::authenticate( $_GET[ "code" ] ) === true ) {
	$token      = json_decode( Google::getToken(), true );
	$googleInfo = $g->checkLoginInfo( $token );

	if ( !isset( $_SESSION[ "assignSN" ] ) )
		$_SESSION[ "assignSN" ] = array();

	if ( !is_bool( $googleInfo ) ) {
		if ( !isset( $_SESSION[ "adPublisherId" ] ) ) {
			#try login
			$loginCheck = $g->login( $token, $googleInfo );
			if($loginCheck === false ){
				$avatar = SnTwitter::getAvatarImage( $g->savedAvatar );
				$_SESSION[ "assignSN" ][ "google" ] = array($g->savedId, null, $avatar);
			}
			JUtil::jredirect( $loginCheck !== false ? "dashboard" : "signup-publisher", true ); #new user with no google connection
		}

		$p = new Publisher( $_SESSION[ "adPublisherId" ] );

		#now we are trying to connect
		$accountReggd = $g->addAccount( $token, $googleInfo );
		$avatar = SnTwitter::getAvatarImage( $g->savedAvatar );
		$_SESSION[ "assignSN" ][ "google" ] = array( false, null, $avatar );

		if ( $accountReggd == false  ){
			$_SESSION["adSocialError"] = "connectionProblem";
			JUtil::jredirect( "dashboard", true ); #some problem
		}

		if ( is_null( $g->publisher_snGoogle ) ) {
			#we have a logged in user with an unregistered google plus account
			$p->addSnGoogle( $g );
			unset( $p );
		}
		else if ( $g->publisher_snGoogle->id != $_SESSION[ "adPublisherId" ] ) {
				$_SESSION["adSocialError"] = "socialAccountConnectedToAnotherUser";
				#can't add dude
		}
	}
}
else
	$_SESSION["adSocialError"] = "connectionProblem";

JUtil::jredirect( "social" );