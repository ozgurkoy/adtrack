
<?php
/**
 * home controller
 *
 * @package admingle
 **/
class Home_controller extends JController
{
	/**
	 * @author Özgür Köy
	 * home page
	 */
	public function display() {

//		echo "<PRE>";
//		$b = new BugHistory(46);
//		$b->sendInformEmails();
//		MailAction::init();
//		exit;


//		JUtil::jredirect( "darkside" );
		$this->checkRoot();
		JT::init();

		if( isset($_SESSION[ "adRedirectAfter" ]) && strpos($_SESSION[ "adRedirectAfter" ],"ds_xhr")===false ){; # he might have tried to connect before.
			$r = $_SESSION[ "adRedirectAfter" ];
			unset( $_SESSION[ "adRedirectAfter" ] );

			JUtil::jredirect( $r, true);
		}
		if(!isset($_SESSION[ "DS_isRoot" ]) || $_SESSION[ "DS_isRoot" ]!=1)
			JUtil::jredirect( "ds_bug", true);

		$d = new DSNotification();
//		JPDO::$debugQuery=true;
		$d->orderBy( "id DESC" )->load(array("nots"=>$_SESSION["DS_userId"]));
		#echo "<PRE>";
		JT::assignByRef( "nots", $d );
		echo JT::pfetch( "DS_home" );

	}

	/**
	 * @JCTL("mailActionized")
	 */
	public function mailAction() {
		echo "<PRE>";
		MailAction::init();
	}
} // END class Home_controller