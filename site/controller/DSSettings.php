<?php
/**
 * Class DSSettings_controller
 * @JCTLNS("darkside")
 */
class DSSettings_controller extends JController{

	//******************************************* Campaign **************************************************
	/**
	 * @JCTL({"ds_campaignSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function campaignSettingsList() {

		JT::init();

		JT::assign( "_job", "campaignSettings" );

		echo JT::pfetch( "DS_form_settingsCampaign" );
	}

	/**
	 * @JCTL({"ds_campaignSettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function campaignSettingsAction() {
		JFORM::formResponse( "campaignSettings" );

		//$_POST[ "id" ]


		Setting::setValue("cpcCampaignMinimumPrice",$_POST["cpcCampaignMinimumPrice"]);
		Setting::setValue("cpcCampaignMinStartDate",$_POST["cpcCampaignMinStartDate"]);
		Setting::setValue("cpcCampaignMinDays",$_POST["cpcCampaignMinDays"]);
		Setting::setValue("cpcCampaignMaxDays",$_POST["cpcCampaignMaxDays"]);
		Setting::setValue("cpcCampaignMaxDays",$_POST["cpcCampaignMaxDays"]);
		Setting::setValue("campaignMinStartDate",$_POST["campaignMinStartDate"]);
		Setting::setValue("campaignMinDays",$_POST["campaignMinDays"]);
		Setting::setValue("campaignMaxDays",$_POST["campaignMaxDays"]);
		Setting::setValue("campaignMinimumPrice",$_POST["campaignMinimumPrice"]);
		Setting::setValue("ENABLE_MODULE_CPC",$_POST["ENABLE_MODULE_CPC"]);
		Setting::setValue("ENABLE_MODULE_AWARENESS",$_POST["ENABLE_MODULE_AWARENESS"]);
		Setting::setValue("campaignHideCPCLink",isset($_POST["campaignHideCPCLink"])?1:0);





		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}
	//******************************************* Campaign **************************************************

	//******************************************* Email **************************************************

	/**
	 * @JCTL({"ds_emailSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function emailSettingsList() {

		JT::init();

		JT::assign( "_job", "emailSettings" );

		echo JT::pfetch( "DS_form_settingsEmail" );
	}

	/**
	 * @JCTL({"ds_emailSettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function emailSettingsAction() {
		JFORM::formResponse( "emailSettings" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("EMAIL_FROM_ADDRESS",$_POST["EMAIL_FROM_ADDRESS"]);
		Setting::setValue("EMAIL_FROM_NAME",$_POST["EMAIL_FROM_NAME"]);
		Setting::setValue("EMAIL_NOREPLY_ADDRESS",$_POST["EMAIL_NOREPLY_ADDRESS"]);
		Setting::setValue("EMAIL_MANDRILL_API_URL",$_POST["EMAIL_MANDRILL_API_URL"]);
		Setting::setValue("EMAIL_MANDRILL_KEY",$_POST["EMAIL_MANDRILL_KEY"]);
		Setting::setValue("REPORT_RECIPIENTS",$_POST["REPORT_RECIPIENTS"]);


		$set = array();
		$set["info"] = $_POST["info"];
		$set["pay"] = $_POST["pay"];
		$set["sales"] = $_POST["sales"];
		$set["support"] = $_POST["support"];
		$set["help"] = $_POST["help"];
		LocalConfig::SaveList($set);
		LocalConfig::CommitChanges();

		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	//******************************************* Email **************************************************

	//******************************************* General **************************************************

	/**
	 * @JCTL({"ds_generalSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function generalSettingsList() {

		JT::init();

		// Site Mode
		$siteMode = array(0=>"open",1=>"invite only",2=>"invite and register");
		JT::assign( "siteModeArr", $siteMode );
		JT::assign( "siteMode", ENABLE_INVITE );

		JT::assign( "_job", "generalSettings" );

		echo JT::pfetch( "DS_form_settingsGeneral" );
	}

	/**
	 * @JCTL({"ds_generalSettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function generalSettingsAction() {
		JFORM::formResponse( "generalSettings" );

		// Change invite mode
		if($_POST["sitemode"] != ENABLE_INVITE){
			$changes = array();
			$changes["ENABLE_INVITE"] = $_POST["sitemode"];
			LocalConfig::SaveList($changes);
			LocalConfig::CommitChanges();
		}

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("ENABLE_MOBILE",isset($_POST["ENABLE_MOBILE"])?1:0);
		Setting::setValue("IPHONE_URL",$_POST["IPHONE_URL"]);
		Setting::setValue("IPHONE_APP_ID",$_POST["IPHONE_APP_ID"]);
		Setting::setValue("ANDROID_URL",$_POST["ANDROID_URL"]);
		Setting::setValue("ANDROID_APP_ID",$_POST["ANDROID_APP_ID"]);
		Setting::setValue("ZENDESK",isset($_POST["ZENDESK"])?1:0);
		Setting::setValue("ZENDESK_CODE",$_POST["ZENDESK_CODE"]);
		Setting::setValue("ZENDESK_API",$_POST["ZENDESK_API"]);
		Setting::setValue("GOOGLE_ANALYTICS",$_POST["GOOGLE_ANALYTICS"]);
		Setting::setValue("ANALYTICS_CODE",$_POST["ANALYTICS_CODE"]);
		Setting::setValue("ENABLE_STORE",isset($_POST["ENABLE_STORE"])?1:0);
		Setting::setValue("FACEBOOK_LIKE_BOX",isset($_POST["FACEBOOK_LIKE_BOX"])?$_POST["FACEBOOK_LIKE_BOX"]:null);

		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}
	//******************************************* General **************************************************

	//******************************************* Home Page **************************************************
	/**
	 * @JCTL({"ds_homePageSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function homePageSettingsList() {

		JT::init();

		JT::assign( "_job", "homePageSettings" );
		JT::assign("visibleOptions",array(1=>"Yes",0=>"No"));

		echo JT::pfetch( "DS_form_settingsHomePage" );
	}

	/**
	 * @JCTL({"ds_homePageSettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function homePageSettingsAction() {
		JFORM::formResponse( "homePageSettings" );

		// Country

		Setting::setValue("BLOG_PAGE",$_POST["blog"]);
		Setting::setValue("TWITTER_USER",isset($_POST["twitterUser"])?$_POST["twitterUser"]:"");
		Setting::setValue("LINKEDIN_PAGE",isset($_POST["linkedin"])?$_POST["linkedin"]:"");
		Setting::setValue("YOUTUBE_PAGE",isset($_POST["youtube"])?$_POST["youtube"]:"");
		Setting::setValue("GOOGLEPLUS_PAGE",isset($_POST["googlePlus"])?$_POST["googlePlus"]:"");
		Setting::setValue("VK_PAGE",isset($_POST["vk"])?$_POST["vk"]:"");
		Setting::setValue("INSTAGRAM_PAGE",isset($_POST["instagram"])?$_POST["instagram"]:"");
		Setting::setValue("FACEBOOK_PAGE",isset($_POST["facebook"])?$_POST["facebook"]:"");
		Setting::setValue("HOME_ADDRESS",$_POST["HOME_ADDRESS"]);
		Setting::setValue("HOME_EMAILS",$_POST["HOME_EMAILS"]);
		Setting::setValue("HOME_PHONE",$_POST["HOME_PHONE"]);
		Setting::setValue("SHOW_PUBLISHER_VIDEO",$_POST["SHOW_PUBLISHER_VIDEO"]);
		Setting::setValue("SHOW_ADVERTISER_VIDEO",$_POST["SHOW_ADVERTISER_VIDEO"]);
		Setting::setValue("HOME_MAP_SHOW",$_POST["HOME_MAP_SHOW"]);
		Setting::setValue("HOME_MAP_URL",$_POST["HOME_MAP_URL"]);
		Setting::setValue("SHOW_COOKIE_WARNING",$_POST["SHOW_COOKIE_WARNING"]);

		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}
	//******************************************* Home Page **************************************************

	//******************************************* Local **************************************************
	/**
	 * @JCTL({"ds_localSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function localSettingsList() {

		// Country List
		$cArray= Country::loadAll();

		//Default Language
		$lArray = Lang::loadAll();

		//Currency
		$crArray = Currency::loadAll();

		// THOUSAND SEPERATOR

		// DECIMAL SEPERATOR

		// CURRENCY SYMBOL AFTER
		$currencyLocation = array(0=>"Left $5",1=>"Right 5$");

		// Date Format

		// Time Format


		JT::init();
		JT::assign( "cArray", $cArray );
		JT::assign( "cCode", COUNTRY_CODE );
		JT::assign( "lang", $lArray );
		JT::assign( "currency", $crArray );
		JT::assign( "currencyLoc", $currencyLocation );

		JT::assign( "_job", "localSettings" );

		echo JT::pfetch( "DS_form_settingsLocal" );
	}

	/**
	 * @JCTL({"ds_localSettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function localSettingsAction() {
		JFORM::formResponse( "localSettings" );

		//$_POST[ "id" ]

		// Country
		if($_POST["country"] != COUNTRY_CODE){
			$changes = array();
			$changes["COUNTRY_CODE"] = $_POST["country"];
			LocalConfig::SaveList($changes);
			LocalConfig::CommitChanges();
		}

		// Settings
		Setting::setValue("DEFAULT_LANGUAGE",$_POST["language"]);
		Setting::setValue("NUMBER_FORMAT_THOUSAND_SEPERATOR",$_POST["thousand"]);
		Setting::setValue("NUMBER_FORMAT_DECIMAL_SEPERATOR",$_POST["decimal"]);
		Setting::setValue("dateFormat",$_POST["dateFormat"]);
		Setting::setValue("timeFormat",$_POST["timeFormat"]);
		Setting::setValue("jsDateFormat",$_POST["jsDateFormat"]);
		Setting::setValue("CURRENCY_SYMBOL",Currency::getSymbol($_POST["currency"]));
		Setting::setValue("CURRENCY_CODE",$_POST["currency"]);
		Setting::setValue("CURRENCY_SYMBOL_AFTER",$_POST["currencySymbol"]);
		Setting::setValue("PHONE_MASK",$_POST["phoneMask"]);




		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	//******************************************* Local **************************************************

	//******************************************* Other **************************************************
	/**
	 * @JCTL({"ds_otherSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function otherSettingsList() {

		JT::init();

		JT::assign( "_job", "otherSettings" );

		echo JT::pfetch( "DS_form_settingsOther" );
	}

	/**
	 * @JCTL({"ds_otherSettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function otherSettingsAction() {
		JFORM::formResponse( "otherSettings" );

		// Country

		Setting::setValue("ADVANCED_DEBUG_MODE",$_POST["ADVANCED_DEBUG_MODE"]);
		Setting::setValue("PUSH_KEY_PASSPHRASE",$_POST["PUSH_KEY_PASSPHRASE"]);
		Setting::setValue("PUSH_GATEWAY_ADDRESS",$_POST["PUSH_GATEWAY_ADDRESS"]);
		Setting::setValue("TWEET_MAX_DAY",$_POST["TWEET_MAX_DAY"]);
		Setting::setValue("MAX_CAMP_ID_BEFORE_CPC",$_POST["MAX_CAMP_ID_BEFORE_CPC"]);

		Setting::setValue("MAIL_DEBUG_MODE",$_POST["MAIL_DEBUG_MODE"]);
		Setting::setValue("campaignImageMaxWidth",$_POST["campaignImageMaxWidth"]);
		Setting::setValue("campaignImageMaxHeight",$_POST["campaignImageMaxHeight"]);
		Setting::setValue("campaignCharLimitWithLink",$_POST["campaignCharLimitWithLink"]);
		Setting::setValue("campaignCharLimitWithoutLink",$_POST["campaignCharLimitWithoutLink"]);

		Setting::setValue("campListItemCount",$_POST["campListItemCount"]);
		Setting::setValue("validImageTypes",$_POST["validImageTypes"]);
		Setting::setValue("maxImageFileSize",$_POST["maxImageFileSize"]);
		Setting::setValue("publisherProfileImageMaxWidth",$_POST["publisherProfileImageMaxWidth"]);
		Setting::setValue("publisherProfileImageMaxHeight",$_POST["publisherProfileImageMaxHeight"]);

		Setting::setValue("publisherBackgroundImageMaxHeight",$_POST["publisherBackgroundImageMaxHeight"]);
		Setting::setValue("publisherBackgroundImageMaxWidth",$_POST["publisherBackgroundImageMaxWidth"]);

		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}
	//******************************************* Other **************************************************

	//******************************************* Security **************************************************
	/**
	 * @JCTL({"ds_securitySettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function securitySettingsList() {

		JT::init();

		JT::assign( "_job", "securitySettings" );

		echo JT::pfetch( "DS_form_settingsSecurity" );
	}

	/**
	 * @JCTL({"ds_securitySettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function securitySettingsAction() {
		JFORM::formResponse( "securitySettings" );
		//$_POST[ "id" ]
		// Settings
		Setting::setValue("EMAIL_FROM_ADDRESS",$_POST["EMAIL_FROM_ADDRESS"]);
		Setting::setValue("EMAIL_NOREPLY_ADDRESS",$_POST["EMAIL_NOREPLY_ADDRESS"]);

		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_securitySettingsIPAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function securitySettingsIPAction() {
		JFORM::formResponse( "securityIPSettings" );

		$set = array();
		$set["info"] = $_POST["info"];
		$set["pay"] = $_POST["pay"];
		$set["sales"] = $_POST["sales"];
		$set["support"] = $_POST["support"];
		LocalConfig::SaveList($set);
		LocalConfig::CommitChanges();

		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}
	//******************************************* Security **************************************************

	//******************************************* Services **************************************************
	/**
	 * @JCTL({"ds_serviceSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function serviceSettingsList() {

		JT::init();

		JT::assign( "_job", "serviceSettings" );

		$SLSDomainOptions = Setting::getValue("SHORTDOMAIN_OPTIONS");
		JT::assign( "SLSDomainOptions", (!is_null($SLSDomainOptions) ? implode("\r\n",json_decode($SLSDomainOptions,true)) : null) );

		echo JT::pfetch( "DS_form_settingsServices" );
	}

	/**
	 * @JCTL({"ds_serviceSettingsAction"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsAction() {
		JFORM::formResponse( "serviceSettings" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("SL_SERVICE_URL",$_POST["SLSAPI"]);
		Setting::setValue("SL_COUNTRY_CODE",$_POST["SLSCountry"]);
		Setting::setValue("SL_PUSH_ADDRESS",$_POST["SLSPush"]);
		Setting::setValue("SL_CAMPAIGN_END",$_POST["SLSCampEnd"]);
		Setting::setValue("SHORTDOMAIN",$_POST["SLSDomain"]);
		Setting::setValue("CALCULATION_API_URL",$_POST["calcApi"]);
		Setting::setValue("LANGMAN_API_URL",$_POST["langApi"]);
		Setting::setValue("TG_SERVICE_URL",$_POST["tgApi"]);

		$SLSDomainOptions = array();
		if (isset($_POST["SLSDomainOptions"])){
			$tSLSDomainOptions = explode("\n",trim($_POST["SLSDomainOptions"]));
			foreach ($tSLSDomainOptions as $o){
				$o = trim($o);
				if (Validate::validateURL($o)) $SLSDomainOptions[] = $o;
			}
		}
		if (!count($SLSDomainOptions)) $SLSDomainOptions = array($_POST["SLSDomain"]);
		Setting::setValue("SHORTDOMAIN_OPTIONS",json_encode($SLSDomainOptions));


		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}
	//******************************************* Services **************************************************

	//******************************************* Social Networks **************************************************
	/**
	 * @JCTL({"ds_socialNetworksSettings"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function socialNetworksSettingsList() {

		JT::init();

		$d = Setting::getValue("SHARE_OPTIONS");
		if(is_null($d)){
			$d = array();
		}else{
			$d = json_decode($d);
		}



		JT::assign( "_job", "socialNetworksSettings" );
		JT::assign( "SHARE_OPTIONS", $d );
		echo JT::pfetch( "DS_form_settingsSocialNetworks" );
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsKeys"})
	 * @JCTLPre(func="checkPerms", args={'localSettings','list'})
	 * @JCTLPost(func="logOperation", args={'Local Settings'})
	 */
	public function socialNetworksSettingsKeysList() {

		JT::init();

		JT::assign( "_job", "socialNetworksSettings" );
		echo JT::pfetch( "DS_form_settingsSocialNetworksKeys" );
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_facebook"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActionfacebook() {
		JFORM::formResponse( "socialNetworksSettings"."facebook" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("FB_APP_ID",$_POST["FB_APP_ID"]);
		Setting::setValue("FB_APP_SECRET",$_POST["FB_APP_SECRET"]);


		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_twitter"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActiontwitter() {
		JFORM::formResponse( "socialNetworksSettings"."twitter" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("CONSUMER_KEY",$_POST["CONSUMER_KEY"]);
		Setting::setValue("CONSUMER_SECRET",$_POST["CONSUMER_SECRET"]);
		Setting::setValue("OAUTH_CALLBACK",$_POST["OAUTH_CALLBACK"]);


		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_gplus"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActiongplus() {
		JFORM::formResponse( "socialNetworksSettings"."gplus" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("GOOGLE_APP_NAME",$_POST["GOOGLE_APP_NAME"]);
		Setting::setValue("GOOGLE_CLIENT_ID",$_POST["GOOGLE_CLIENT_ID"]);
		Setting::setValue("GOOGLE_CLIENT_SECRET",$_POST["GOOGLE_CLIENT_SECRET"]);
		Setting::setValue("GOOGLE_DEVELOPER_KEY",$_POST["GOOGLE_DEVELOPER_KEY"]);
		Setting::setValue("GOOGLE_REDIRECT_URL",$_POST["GOOGLE_REDIRECT_URL"]);


		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_vk"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActionvk() {
		JFORM::formResponse( "socialNetworksSettings"."vk" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("VK_CLIENT_ID",$_POST["VK_CLIENT_ID"]);
		Setting::setValue("VK_PERMISSIONS",$_POST["VK_PERMISSIONS"]);
		Setting::setValue("VK_SECRET",$_POST["VK_SECRET"]);
		Setting::setValue("VK_CALLBACK_URL",$_POST["VK_CALLBACK_URL"]);



		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_xing"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActionxing() {
		JFORM::formResponse( "socialNetworksSettings"."xing" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("XING_KEY",$_POST["XING_KEY"]);
		Setting::setValue("XING_SECRET",$_POST["XING_SECRET"]);
		Setting::setValue("XING_CALLBACK_URL",$_POST["XING_CALLBACK_URL"]);
		

		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_instagram"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActioninstagram() {
		JFORM::formResponse( "socialNetworksSettings"."instagram" );

		//$_POST[ "id" ]
		// Settings
		Setting::setValue("INSTAGRAM_CLIENT_ID",$_POST["INSTAGRAM_CLIENT_ID"]);
		Setting::setValue("INSTAGRAM_CLIENT_SECRET",$_POST["INSTAGRAM_CLIENT_SECRET"]);
		Setting::setValue("INSTAGRAM_CALLBACK",$_POST["INSTAGRAM_CALLBACK"]);


		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_login"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActionlogin() {
		JFORM::formResponse( "socialNetworksSettings"."login" );

		//$_POST[ "id" ]
		// Settings

		if (isset($_POST["TWITTER_LOGIN"]))
			Setting::setValue("TWITTER_LOGIN",1);
		else
			Setting::setValue("TWITTER_LOGIN",0);

		if (isset($_POST["GOOGLE_LOGIN"]))
			Setting::setValue("GOOGLE_LOGIN",1);
		else
			Setting::setValue("GOOGLE_LOGIN",0);

		if (isset($_POST["FB_LOGIN"]))
			Setting::setValue("FB_LOGIN",1);
		else
			Setting::setValue("FB_LOGIN",0);

		if (isset($_POST["XING_LOGIN"]))
			Setting::setValue("XING_LOGIN",1);
		else
			Setting::setValue("XING_LOGIN",0);

		if (isset($_POST["VK_LOGIN"]))
			Setting::setValue("VK_LOGIN",1);
		else
			Setting::setValue("VK_LOGIN",0);

		if (isset($_POST["INSTAGRAM_LOGIN"]))
			Setting::setValue("INSTAGRAM_LOGIN",1);
		else
			Setting::setValue("INSTAGRAM_LOGIN",0);


		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}

	/**
	 * @JCTL({"ds_socialNetworksSettingsAction_sharing"})
	 * @JCTLPre(func="checkPerms",args={'localSettings','edit'})
	 * @JCTLPost(func="logOperation", args={'localSettings A/E'})
	 */
	public function serviceSettingsActionsharing() {
		JFORM::formResponse( "socialNetworksSettings"."sharing" );

		//$_POST[ "id" ]
		// Settings


		$d = null;
		if(isset($_POST["share"]))
			$d = json_encode($_POST["share"]);

//		print_r($d);
//		print_r(json_decode($d));

		Setting::setValue("SHARE_OPTIONS",$d);
		/*extra action, joins etc*/

		JUtil::customCScript( "message", array("good","Saved"),false ); #exited here
	}
	//******************************************* Social Networks **************************************************

}