<?php
/**
 * Class DSCore_controller
 */
class DSBug_controller extends JController{

	public $perPage = 10;
	/**
	 * @JCTL("ds_branch")
	 * @JCTLPre(func="checkRoot")
	 */
	public function branch() {
		$a = new Branch();
		$a->load();

		JT::init();
		JT::assign( "_items", $a );

		echo JT::pfetch( "DS_branch" );
	}

	/**
	 * @JCTL({"ds_addBranch"})
	 * @JCTLPre(func="checkRoot")
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function addBranch( $id = null ) {
		$a = new Branch( $id );
		JT::init();

		JT::assign( "_item", $a );
		JT::assign( "_job", "branch" );
		echo JT::pfetch( "DS_form_branch" );
	}
	/**
	 * @JCTL({"ds_branchAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function branchAction() {
		JFORM::formResponse( "branch" );

		$a                 = new Branch( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		self::_assignPostToObject( $a );
		$a->save();

		unset( $a );
		JUtil::jredirect( "ds_branch" );
	}


	/**
	 * @JCTL({"ds_delBranch"})
	 * @JCTLPre(func="checkRoot")
	 * @param $id
	 * @author Özgür Köy
	 */
	public function delBranch( $id ) {
		$a = new Branch( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "ds_branch" );
	}


	/**
	 * @JCTL("ds_project")
	 * @JCTLPre(func="checkRoot")
	 */
	public function project() {
		$a = new Project();
		$a->load();

		JT::init();
		JT::assign( "_items", $a );

		echo JT::pfetch( "DS_project" );
	}


	/**
	 * @JCTL({"ds_addProject"})
	 * @JCTLPre(func="checkRoot")
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function addProject( $id = null ) {
		$a = new Project( $id );
		JT::init();

		JT::assign( "_item", $a );
		JT::assign( "_job", "project" );
		echo JT::pfetch( "DS_form_project" );
	}
	/**
	 * @JCTL({"ds_projectAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function projectAction() {
		JFORM::formResponse( "project" );

		$a                 = new Project( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		self::_assignPostToObject( $a );
		$a->isDev = isset( $_POST[ "isDev" ] );
		$a->save();

		unset( $a );
		JUtil::jredirect( "ds_project" );
	}


	/**
	 * @JCTL({"ds_delProject"})
	 * @JCTLPre(func="checkRoot")
	 * @param $id
	 * @author Özgür Köy
	 */
	public function delProject( $id ) {
		$a = new Project( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "ds_project" );
	}


	/**
	 * @JCTL("ds_tag")
	 * @JCTLPre(func="checkRoot")
	 */
	public function tag() {
		$a = new Tags();
		$a->load();

		JT::init();
		JT::assign( "_items", $a );

		echo JT::pfetch( "DS_tag" );
	}

	/**
	 * @JCTL({"ds_addTag"})
	 * @JCTLPre(func="checkRoot")
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function addTag( $id = null ) {
		$a = new Tags( $id );

		JT::init();

		JT::assign( "_item", $a );
		JT::assign( "_job", "tag" );
		echo JT::pfetch( "DS_form_tag" );
	}
	/**
	 * @JCTL({"ds_tagAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function tagAction() {
		JFORM::formResponse( "tag" );

		$a                 = new Tags( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		self::_assignPostToObject( $a );
		$a->save();

		unset( $a );
		JUtil::jredirect( "ds_tag" );
	}


	/**
	 * @JCTL({"ds_delTag"})
	 * @JCTLPre(func="checkRoot")
	 * @param $id
	 * @author Özgür Köy
	 */
	public function delTag( $id ) {
		$a = new Tags( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "ds_tag" );
	}


	/**
	 * @JCTL("ds_bugDetail")
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugDetail($id) {
		DSpermission::getPermissions();

		$a = new Bug(intval($id));
		if($a->gotValue!== true)
			die( "bug not found" );

		$a->loadHistory( null, 0, 150, "jdate DESC", false );

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);

		JT::init();
		JT::assign( "id", $id );
		JT::assign( "bug", $a );
		JT::assign( "action", "detail" );
		JT::assign( "list", $a->history );
		JT::assign( "typeLabel", $isRegularUser===true?"Issue":($a->bugType>0?ucfirst($a->bugType_options[$a->bugType]):"Issue") );

		echo JT::pfetch( "DS_bugDetail" );
	}

	/**
	 * @JCTL("ds_bugAttachments")
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugAtts($id) {
		DSpermission::getPermissions();

		$f = new File();
		$f->orderBy("id DESC")->load(array("bugFiles"=>array("history"=>$id)));
		$bug = new Bug( $id );

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);

		JT::init();
		JT::assign( "id", $id );
		JT::assign( "action", "att" );
		JT::assign( "list", $f );
		JT::assign( "bug", $bug );
		JT::assign( "typeLabel", $isRegularUser===true?"Issue":($bug->bugType>0?ucfirst($bug->bugType_options[$bug->bugType]):"Issue") );

		echo JT::pfetch( "DS_bugAtts" );
	}
	/**
	 * @JCTL("ds_bugFollower")
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugFollowers($id=null) {
		DSpermission::getPermissions();

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);
		$bug = new Bug($id);
		$ff = $bug->loadFollower();
		$bug = new Bug( $id );

		JT::init();
		JT::assign( "id", $id );
		JT::assign( "action", "foll" );
		JT::assign( "_items", $ff );
		JT::assign( "bug", $bug );
		JT::assign( "typeLabel", $isRegularUser===true?"Issue":($bug->bugType>0?ucfirst($bug->bugType_options[$bug->bugType]):"Issue") );

		echo JT::pfetch( "DS_bugFollower" );
	}

	/**
	 * @JCTL({"ds_delFollower"})
	 * @JCTLPre(func="checkRoot")
	 * @param $id
	 * @author Özgür Köy
	 */
	public function delFollower( $id,$fid ) {
		$a = new Bug( $id );
		$a->deleteFollower($fid);

		unset( $a );
		JUtil::customCScript("goBugs",array($id));
	}
	/**
	 * @JCTL("ds_assignBug")
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugAssign($id) {
		DSpermission::getPermissions();

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);
		$b = new Bug($id);
		$b->loadAssignee();
		JT::init();
		JT::assign( "id", $id );
		JT::assign( "users", DSuser::loadRootUsers() );
		JT::assign( "action", "ass" );
		JT::assign( "bug", $b );
		JT::assign( "typeLabel", $isRegularUser===true?"Issue":($b->bugType>0?ucfirst($b->bugType_options[$b->bugType]):"Issue") );

		echo JT::pfetch( "DS_bugReassign" );
	}

	/**
	 * @JCTL("ds_bugMerge")
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugMerge($id) {
		DSpermission::getPermissions();

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);

		$b = new Bug($id);
		$b->loadAssignee();
		JT::init();
		JT::assign( "id", $id );
		JT::assign( "bugs", Bug::loadActiveBugs( array("id"=>"&ne ".$id) ) );
		JT::assign( "action", "merge" );
		JT::assign( "bug", $b );
		JT::assign( "typeLabel", $isRegularUser===true?"Issue":($b->bugType>0?ucfirst($b->bugType_options[$b->bugType]):"Issue") );

		echo JT::pfetch( "DS_bugMerge" );
	}

	/**
	 * @JCTL({"ds_assignBugAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugAssignAction($id) {
		JFORM::formResponse( "assignBug".$id );
		$_POST[ "user" ] = $_SESSION[ "DS_userId" ];
		$b = new Bug( $id );
		$b->reassign( $_POST );
		JUtil::customCScript("goBugs",array($id));
	}

	/**
	 * @JCTL({"ds_bugMergeAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugMergeAction($id) {
		JFORM::formResponse( "mergeBug".$id );
		$_POST[ "user" ] = $_SESSION[ "DS_userId" ];
		$b = new Bug( $id );
		$r = $b->mergeTo( $_SESSION[ "DS_userId" ], $_POST[ "mergeId" ] );

		if ( $r !== false )
			JUtil::jredirect( "ds_bug?bugId=" . $_POST["mergeId"] );
	}

	/**
	 * @JCTL("ds_closeBug")
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugClose($id) {
		DSpermission::getPermissions();

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);
		$b = new Bug($id);
		JT::init();
		JT::assign( "bug", $b );
		JT::assign( "id", $id );
		JT::assign( "action", "close" );
		JT::assign( "typeLabel", $isRegularUser===true?"Issue":($b->bugType>0?ucfirst($b->bugType_options[$b->bugType]):"Issue") );

		echo JT::pfetch( "DS_bugClose" );
	}

	/**
	 * @JCTL("ds_bugDel")
	 * @JCTLPre(func="checkRoot")
	 */
	public function delDaBug( $id ) {
		DSpermission::getPermissions();

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);

		$b             = new Bug( $id );
		JT::init();
		JT::assign( "bug", $b );
		JT::assign( "id", $id );
		JT::assign( "action", "del" );

		JT::assign( "typeLabel", $isRegularUser === true ? "Issue" : ( $b->bugType > 0 ? ucfirst( $b->bugType_options[ $b->bugType ] ) : "Issue" ) );

		echo JT::pfetch( "DS_bugDel" );
	}

	/**
	 * @JCTL({"ds_closeBugAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugCloseAction($id) {
		JFORM::formResponse( "closeBug".$id );
		$_POST[ "user" ] = $_SESSION[ "DS_userId" ];
		$b = new Bug( $id );
		$b->close( $_POST );
		JUtil::customCScript("destroyBug",array($id));
	}

	/**
	 * @param int    $me
	 * @param int $type
	 * @JCTL("ds_bug")
	 * @JCTLPre(func="checkRoot")
	 * @author Özgür Köy
	 */
	public function bug( $type=1, $me = 0 ) {

		DSpermission::getPermissions();

		$s = (array)JUtil::getSearchParameter();
		$orders = array(
			"id",
			"title",
			"status",
			"priority",
			"jdate",
			"lastOperation"
		);

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);

		if ( $me == 0 && $isRegularUser ) {
			$me = 1;
		}

		$a = new Bug();

		$filter = array();
		if(!isset($s["ob"]))
			$s[ "ob" ] = "5";
		if(!isset($s["od"]))
			$s[ "od" ] = "1";

		if(isset($s["priority"]) && strlen($s["priority"])>0)
			$a->where("priority",intval($s[ "priority" ]));



		if(isset($s["branch"]) ){
			if($s["branch"]>0)
				$a->where("branch",intval($s[ "branch" ]));
			elseif($s["branch"]==-1)
				$a->where("branch","IS NULL");
		}
		if(isset($s["project"]) ){
			if($s["project"]>0)
				$a->where("project",intval($s[ "project" ]));
			elseif($s["project"]==-1)
				$a->where("project","IS NULL");

		}

		if ( isset( $s[ "assignee" ] ) ) {
			if ( $s[ "assignee" ] > 0 )
				$a->where("assignee",intval($s[ "assignee" ]));
			elseif ( $s[ "assignee" ] == -1 )
				$a->where("assigned",0);
		};

		if(isset($s["title"]) && strlen($s["title"])>0){
			$titleSet = true;
			if(substr($s[ "title" ],0,1)=="#"){
				$s[ "bugId" ] = substr( $s[ "title" ], 1, 100 );
			}
			else {
				$a->where( JPP::O(
					array(
						"title"   => "LIKE %" . $s[ "title" ] . "%",
						"history" => array( "details" => "LIKE %" . $s[ "title" ] . "%" )
					)
				) );

			}
		}
		JT::init();

		if ( isset( $s[ "bugId" ] ) ) {
			$s = array( "bugId" => intval( $s[ "bugId" ] ), "ob" => $s[ "ob" ], "od" => $s[ "od" ] );

			if( Bug::checkIsFollower( $s[ "bugId" ], $_SESSION[ "DS_userId" ] ) === true || $_SESSION["DS_isRoot"] == 1  )
				$a->clearFilter()->where("id" , intval( $s[ "bugId" ] ));
		}
		else{
			if ( isset( $s[ "status" ] ) && strlen( $s[ "status" ] ) > 0 && array_key_exists( $s[ "status" ], $a->status_options ) )
				$a->where("status", intval( $s[ "status" ] ));
			elseif(!isset($titleSet))
				$a->where("status", "&ne " . Bug::ENUM_STATUS_CLOSED);

			if ( !is_null( $type ) && $type > 0 && $type != 99  && $isRegularUser !== true )
				$a->where("bugType",$type);

		}

		if ( $type == 99 && $isRegularUser !== true ) {
			$a->where("assignee" , $_SESSION[ "DS_userId" ]);
			unset( $filter[ "bugType" ] );
			JT::assign( "customLabel", "Assigned to me" );
		}


		if($me!=0)
			$a->where("follower",$_SESSION[ "DS_userId" ]);

//		$filter = JPP::A($filter);
//		print_r( $filter);

//		print_r( $q->indexes );
//		print_r( $q->clauseValues );
//		print_r( JPP::$index );
//		exit;
		$b = new Branch();
		$b->load();
		$project = new Project();
		$project->load();
		$u = new DSuser();
		$u->load();
		if(!isset($s["page"]))
			$s["page"] = 0 ;
//		print_r( $filter );
//		JPDO::debugQuery();
		$a->count( "id", "cid" );
//		JPDO::debugQuery(false);
//		exit;
		$maxCount = $a->cid;
//exit;
//		$a = new Bug();
		//TODO dynamic limiting

//		JPDO::debugQuery();
//		JPDO::debugQuery();

		$a->useDistinct()->orderBy($orders[$s["ob"]]." ".($s["od"]==1?"DESC":"ASC"))->limit($this->perPage*$s["page"],$this->perPage)
			->load();
//		JPDO::debugQuery(false); exit;
		if(isset($s["bugId"])){
			$type = $a->bugType;
		}

		JT::assign( "S", $s );
		JT::assign( "od", $s["od"]==1?0:1);
		JT::assign( "ob", $s[ "ob" ] );
		JT::assign( "type", $type );

		JT::assign( "typeLabel", $isRegularUser===true||$type==99?"Issue":($type>0?ucfirst($a->bugType_options[$type]):"Issue") );

		JT::assign( "me", $me );
		JT::assign( "user", $u );
		JT::assign( "branch", $b );
		JT::assign( "project", $project );
		JT::assign( "bug", $a );
		JT::assign( "page", $s["page"] );
		JT::assign( "perPage", $this->perPage );
//		JPDO::debugQuery(false);

		JT::assign( "_items", $a );
//echo  JUtil::combineKeyValueArrayToString( $s, array( "page" ) ) ;exit;
		JT::assign( "nextPage",  $this->perPage * ($s[ "page" ]+1) < $maxCount );
		JT::assign( "QS", JUtil::combineKeyValueArrayToString( $s, array( "page","1" ) ) );
		JT::assign( "QSOrder", JUtil::combineKeyValueArrayToString( $s, array( "page","1","ob","od" ) ) );
		if ( isset( $s[ "onlyTable" ] ) !== true )
			echo JT::pfetch( "DS_bug" );
		else
			echo JT::pfetch( "DS_bugTable" );
	}

	/**
	 * @param string $type
	 * @JCTL("ds_task")
	 * @JCTLPre(func="checkRoot")
	 * @author Özgür Köy
	 */
	public function task( $type=null ) {
		return $this->bug( Bug::ENUM_BUGTYPE_TASK );
	}
	/**
	 * @param string $type
	 * @JCTL("ds_enhancement")
	 * @JCTLPre(func="checkRoot")
	 * @author Özgür Köy
	 */
	public function enhancement( $type=null ) {
		return $this->bug( Bug::ENUM_BUGTYPE_QUESTION );
	}
	/**
	 * @JCTL({"ds_addBug"})
	 * @JCTLPre(func="checkRoot")
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function addBug( $type = 1, $id = null ) {
		DSpermission::getPermissions();

		$a = new Bug( $id );
		if ( array_key_exists( $type, $a->bugType_options ) !== true ){
			$type = Bug::ENUM_BUGTYPE_QUESTION;

		}

		$a->collectTags();
		JT::init();
		$u = new DSuser( $_SESSION[ "DS_userId" ] );
		if ( $u->isRoot == 1 ) {
			$p        = new Project();
			$projects = $p->load();
		}
		else
			$projects = $u->loadProjects();

		$isRegularUser = !(
			isset( $_SESSION[ "DS_permissions" ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ "bug" ] ) &&
			$_SESSION[ "permissions" ][ "bug" ] & $_SESSION[ "DSPerms" ][ "bug" ][ "bugAdmin" ]
		);

		JT::assign( "bug", $a );
		JT::assign( "type", $type );
		JT::assign( "id", $id );
		JT::assign( "projects", $projects );
		JT::assign( "tags", Tags::getAllTags() );
		JT::assign( "action", "edit" );
		JT::assign( "users", DSUser::loadRootUsers() );
		JT::assign( "typeLabel", $isRegularUser === true ? "Issue" : ucfirst( $a->bugType_options[ $type ] ) );

		JT::assign( "_job", "bug" );

		echo JT::pfetch( "DS_form_bug" . ( $a->gotValue === true ? "Edit" : "" ) );
	}
	/**
	 * @JCTL({"ds_bugAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function bugAction() {
		JFORM::formResponse( isset( $_POST[ "id" ])?"editBug":"bug" );
		$_POST[ "user" ] = $_SESSION[ "DS_userId" ];

		if ( isset( $_POST[ "id" ] ) ) {
			//edit
			$a = new Bug( $_POST[ "id" ] );
			$a->edit( $_POST );
			JUtil::customCScript( "goBugs", array( $_POST[ "id" ] ) );

			unset( $a );
		}
		else {
			Bug::create( $_POST );
			JUtil::jredirect( "ds_bug/" . $_POST[ "bugType" ] );
		}

	}
	/**
	 * @JCTL({"ds_addDetail"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function detailAction($id) {
		JFORM::formResponse( "bugDetail".$id );
		$_POST[ "user" ] = $_SESSION[ "DS_userId" ];
		$b = new Bug( $id );
		$b->response( $_POST );
		JUtil::customCScript("goBugs",array($id));
	}


	/**
	 * @JCTL({"ds_delBug"})
	 * @JCTLPre(func="checkRoot")
	 * @param $id
	 * @author Özgür Köy
	 */
	public function delBug( $id ) {
		JFORM::formResponse( "delBug".$id );
		if ( isset( $_POST[ "deleteMe" ] ) ) {
			$a    = new Bug( $id );
			$type = $a->bugType;
			$a->delete();

			unset( $a );
			//JUtil::jredirect( "ds_bug/$type" );
			JUtil::customCScript( "destroyBug", array( $id ) );
		}
	}

	/**
	 * @JCTL("ds_category")
	 * @JCTLPre(func="checkRoot")
	 */
	public function category() {
		$a = new Category();
		$a->load();

		JT::init();
		JT::assign( "_items", $a );

		echo JT::pfetch( "DS_category" );
	}

	/**
	 * @JCTL({"ds_addCategory"})
	 * @JCTLPre(func="checkRoot")
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function addCategory( $id = null ) {
		$a = new Category( $id );

		JT::init();

		JT::assign( "_item", $a );
		JT::assign( "_job", "category" );
		echo JT::pfetch( "DS_form_category" );
	}
	/**
	 * @JCTL({"ds_categoryAction"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function categoryAction() {
		JFORM::formResponse( "category" );

		$a                 = new Category( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		self::_assignPostToObject( $a );
		$a->save();

		unset( $a );
		JUtil::jredirect( "ds_category" );
	}


	/**
	 * @JCTL({"ds_delCategory"})
	 * @JCTLPre(func="checkRoot")
	 * @param $id
	 * @author Özgür Köy
	 */
	public function delCategory( $id ) {
		$a = new Category( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "ds_category" );
	}

}