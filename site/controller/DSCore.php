<?php
/**
 * Class DSCore_controller
 */
class DSCore_controller extends JController{
	/**
	 * @JCTL("darkside")
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'viewed homepage'})
	 */
	public function home() {
		JT::init();

		if( isset($_SESSION[ "adRedirectAfter" ]) && strpos($_SESSION[ "adRedirectAfter" ],"ds_xhr")===false ){; # he might have tried to connect before.
			$r = $_SESSION[ "adRedirectAfter" ];
			unset( $_SESSION[ "adRedirectAfter" ] );

			JUtil::jredirect( $r, true);
		}

		$d = new DSNotification();
//		JPDO::$debugQuery=true;
		$d->orderBy( "id DESC" )->load(array("nots"=>$_SESSION["DS_userId"]));
		#echo "<PRE>";

		JT::assignByRef( "nots", $d );
		echo JT::pfetch( "DS_home" );
	}

	/**
	 * @JCTL({"ds_menus"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'viewed menus'})
	 */
	public function menus(){
		$k = DSmenu::menuList();

		$a = new DSmenu();

		JT::init();
		JT::assign( "menus", $a );

		JT::assign( "menuList", $k );
		echo JT::pfetch( "DS_menus" );
	}

	/**
	 * @JCTL("ds_moveUpMenu")
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'moved the menu up'})
	 */
	public function moveUpMenu($id=null) {
		$m = new DSmenu( $id );
		$m->moveUpTheMenu();

		JUtil::jredirect( "ds_menus" );
	}

	/**
	 * @JCTL({"ds_addMenu","ds_editMenu"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'added new DSmenu'})
	 */
	public function addMenu($id=null) {
		$k = DSmenu::menuList(null,0,$id);

		$a = new DSmenu( $id );
		$a->loadMenuRoles();
		$a->loadDSmenu();
		$m = new DSmenu();
		$m->load($a->gotValue?array("id"=>"NOT IN ".$a->id):null);

		JT::init();
		JT::assign( "menus", $a );

		$filter = $a->menuRoles && $a->menuRoles->gotValue ? array( "id" => "NOT IN " . join( ",", $a->menuRoles->_ids ) ) : null;
		$l = new DSrole();
		$l->load( $filter );


		JT::init();
		JT::assign( "menu", $a );
		JT::assign( "menus", $m );
		JT::assign( "roles", $l );
		JT::assign( "menuList", $k );

		echo JT::pfetch( "DS_form_menu" );
	}

	/**
	 * @JCTL({"ds_menuAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'added/edited menu'})
	 */
	public function menuAction() {
		JFORM::formResponse( "menu" );

		$a = new DSmenu( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );

		$a->isActive = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
		$a->label    = $_POST[ "label" ];
		$a->css      = $_POST[ "css" ];
		$a->link     = $_POST[ "link" ];
		$a->subMenus = $_POST[ "subMenus" ];

		$a->save();

		if ( $_POST[ "topMenu" ] == 0 ) $_POST[ "topMenu" ] = null;

		if ( $a->gotValue !== true || ( $a->topMenu ) != $_POST[ "topMenu" ] ) { #changed top menu, change order
			$max       = DSmenu::findMaxOrder( $_POST[ "topMenu" ] );
			$a->morder = $max;
			$a->save();
		}

		if ( $a->gotValue ) {
			$a->deleteDSmenu();
		}

		if ( !is_null( $_POST[ "topMenu" ] ) ) {
			$a->saveDSmenu( $_POST[ "topMenu" ] );
		}

		if ( $a->gotValue )
			$a->deleteAllMenuRoles();

		if ( isset( $_POST[ "menuRoles" ] ) )
			$a->saveMenuRoles( $_POST[ "menuRoles" ] );

		unset( $a );
		JUtil::jredirect( "ds_menus" );
	}

	/**
	 * @JCTL({"ds_delMenu"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'deleted menu'})
	 */
	public function delMenu( $id ) {
		$a = new DSmenu( $id );
//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "ds_menus" );
	}

	/**
	 * @JCTL({"ds_checkLogin"})
	 * @JCTLPost(func="logOperation", args={'actions'})
	 */
	public function checkLogin() {
		$u = new DSuser();
		JFORM::formResponse( "login", $u );
		if ( $u->gotValue ) {
			$u->loadSettings();

			$_SESSION[ "DS_permissions" ] = $u->permissions;
			$_SESSION[ "DS_roles" ]       = $u->userRoles;
			$_SESSION[ "DS_userId" ]      = $u->id;
			$_SESSION[ "DS_authLevel" ]   = $u->isRoot ? array( 0 ) : $u->authLevel;
			$_SESSION[ "DS_username" ]    = $u->username;
			$_SESSION[ "DS_isRoot" ]      = $u->isRoot;


		}

		if (!empty($_SESSION["LAST_DS_URL"]) && strlen($_SESSION["LAST_DS_URL"])){
			JUtil::jredirect( $_SESSION["LAST_DS_URL"] );
		} else {
			JUtil::jredirect("/");
		}
	}

	/**
	 * @JCTL({"ds_actions"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'actions'})
	 */
	public function actions() {
		$a = new DSaction();
		$a->load();

		JT::init();
		JT::assign( "actions", $a );
		echo JT::pfetch( "DS_actions" );
	}

	/**
	 * @JCTL({"ds_addAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'action N'})
	 */
	public function addAction( $id = null ) {
		$a = new DSaction( $id );
		JT::init();
		JT::assign( "action", $a );
		echo JT::pfetch( "DS_form_action" );
	}

	/**
	 * @JCTL({"ds_actionAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'action A/E'})
	 */
	public function actionAction() {
		JFORM::formResponse( "action" );

		$a        = new DSaction( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->label = $_POST[ "label" ];
		$a->tasks = $_POST[ "tasks" ];
		$a->save();


		unset( $a );
		JUtil::jredirect( "ds_actions" );
	}

	/**
	 * @JCTL({"ds_delAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'action D'})
	 */
	public function delAction( $id ) {
		$a = new DSaction( $id );
		$a->delete();

		unset( $a );
		JUtil::jredirect( "ds_actions" );
	}

	/*roles*/
	/**
	 * @JCTL({"ds_roles"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'roles L'})
	 */
	public function roles() {
		$a = new DSrole();
		$a->load();

		JT::init();
		JT::assign( "roles", $a );

		echo JT::pfetch( "DS_roles" );
	}

	/**
	 * @JCTL({"ds_addRole"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'roles N'})
	 */
	public function addRole( $id = null ) {
		$a = new DSrole( $id );

		JT::init();
		JT::assign( "role", $a );
		echo JT::pfetch( "DS_form_role" );
	}
	/**
	 * @JCTL({"ds_roleAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'roles A/E'})
	 */
	public function roleAction() {
		JFORM::formResponse( "role" );

		$a                 = new DSrole( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		$a->label          = $_POST[ "label" ];
		$a->short          = $_POST[ "short" ];
		$a->isActive       = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
		$a->defaultEnabled = isset( $_POST[ "defaultEnabled" ] ) ? $_POST[ "defaultEnabled" ] : 0;
		$a->authLevel      = $_POST[ "authLevel" ];
		$a->save();

		unset( $a );
		JUtil::jredirect( "ds_roles" );
	}

	/**
	 * @JCTL({"ds_delRole"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'roles L'})
	 */
	public function delRole( $id ) {
		$a = new DSrole( $id );

		$a->delete();
//		$a->active = 0;

		unset( $a );
		JUtil::jredirect( "ds_roles" );
	}

	/**
	 * @JCTL({"ds_roleMod"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'rolemod L'})
	 */
	public function roleMod( $id ) {
		$r = new DSrole( $id );
		$a = new DSaction();
		$a->load();

		JT::init();
		JT::assign( "actions", $a );
		JT::assign( "role", $r );

		echo JT::pfetch( "DS_form_roleMod" );
	}

	/**
	 * @JCTL({"ds_roleModAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'rolemod A/E'})
	 */
	public function roleModAction() {
		$r = new DSrole( $_POST[ "id" ] );
		$a = new DSaction();
		$a->load();

		if ( $a->gotValue ) {
			$r->deleteAllPermissions();
			do {
				$p = new DSpermission();
				$p->load( array( "action" => $a->id, "permissions" => $r->id ) );

				$t0 = 0;
				if ( isset( $_POST[ "d_" . $a->id ] ) ) {
					foreach ( $_POST[ "d_" . $a->id ] as $bit ) {
						$t0 = $t0 | $bit;
					}
				}

				$p->bits = $t0;
				$p->ukey = $r->label . "_" . $a->label;
				$p->save();


				$p->saveAction( $a->id );

				$r->savePermissions( $p->id );
			} while ( $a->populate() );
		}
		$u = new DSuser( $_SESSION[ "DS_userId" ] );
		$u->loadSettings();

		$_SESSION[ "DS_permissions" ] = $u->permissions;
		$_SESSION[ "DS_roles" ]       = $u->userRoles;
		unset( $_SESSION[ "permissions" ] );

		unset( $u );
		JUtil::jredirect( "ds_roles" );

//		print_r( $_POST );
	}

	/*users*/
	/**
	 * @JCTL({"ds_users"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'users L'})
	 */
	public function users() {
		$a = new DSuser();
		$a->load();

		JT::init();
		JT::assign( "users", $a );
		echo JT::pfetch( "DS_users" );
	}

	/**
	 * @JCTL({"ds_addUser"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'users N'})
	 */
	public function addUser( $id = null ) {
		$a = new DSuser( $id );
		$a->loadRoles();
		$a->collectTags();

		$filter = $a->roles && $a->roles->gotValue ? array( "id" => "NOT IN " . join( ",", $a->roles->_ids ) ) : null;

		$l = new DSrole();
		$l->load( $filter );

		JT::init();
		JT::assign( "tags", Tags::getAllTags() );
		JT::assign( "roles", $l );
		JT::assign( "user", $a );
		echo JT::pfetch( "DS_form_user" );
	}
	/**
	 * @JCTL({"ds_profile"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'profile'})
	 */
	public function profile() {
		$a = new DSuser( $_SESSION["DS_userId"] );

		JT::init();
		JT::assign( "user", $a );
		echo JT::pfetch( "DS_form_profile" );
	}

	/**
	 * @JCTL({"ds_profileAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'profile E'})
	 */
	public function profileAction() {
		JFORM::formResponse( "profile" );

		$a        = new DSuser( $_SESSION[ "DS_userId" ] );
		$a->email = $_POST[ "email" ];

		if ( isset( $_POST[ "password" ] ) && strlen( $_POST[ "password" ] ) > 0 )
			$a->password = md5( $_POST[ "password" ] );

		$a->save();


		unset( $a );
		JUtil::jredirect( "/" );
	}

	/**
	 * @JCTL({"ds_userAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'users A/E'})
	 */
	public function userAction() {
//		var_dump( $_POST );exit;
		//JPDO::$debugQuery = true;
		$a = new DSuser( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );
		JFORM::formResponse( "user", $a );

		$a->username    = $_POST[ "username" ];
		$a->email       = $_POST[ "email" ];
		$a->aliasEmails = $_POST[ "aliasEmails" ];
		$a->isRoot      = isset( $_POST[ "isRoot" ] ) ? $_POST[ "isRoot" ] : 0;
		$a->isActive    = isset( $_POST[ "isActive" ] ) ? $_POST[ "isActive" ] : 0;
//		$a->authLevel = $_POST[ "authLevel" ];

		if ( isset( $_POST[ "password" ] ) && strlen( $_POST[ "password" ] ) > 0 )
			$a->password = md5( $_POST[ "password" ] );

		$a->save();

		if ( $a->countRoles()>0 )
			$a->deleteAllRoles();

		$a->saveRoles( $_POST[ "roles" ] );

		$a->deleteAllUserTags();
		if(isset($_POST["userTags"]) && strlen($_POST["userTags"])>0){
			Tags::parseTagsFromParameter( $_POST[ "userTags" ] );

			$t = new Tags();
			$t->load( array( "name" => $_POST[ "userTags" ] ) );
			$a->saveUserTags( $t );
		}
		$a->deleteAllProjects();
		$a->saveProjects( $_POST[ "projects" ] );
		unset( $a );
		JUtil::jredirect( "ds_users" );
	}

	/**
	 * @JCTL({"ds_logout"})
	 * @JCTLPre(func="logOperation", args={'user LO'})
	 */
	public function logout( ){
//		session_destroy();
		if(isset($_SESSION["DS_userId"])){
		unset( $_SESSION[ "DS_userId" ] );
		unset( $_SESSION[ "DS_isRoot" ] );
		unset( $_SESSION[ "DS_authLevel" ] );
		unset( $_SESSION[ "DS_roles" ] );
		unset( $_SESSION[ "permissions" ] );
		unset( $_SESSION[ "DS_permissions" ] );
		}
		JUtil::jredirect( "/" );
	}

	/**
	 * @JCTL({"ds_delUser"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'users D'})
	 */
	public function delUser( $id ) {
		$a = new DSuser( $id>1?$id:null );

//		$a->active = 0;
		$a->delete();

		unset( $a );
		JUtil::jredirect( "ds_users" );
	}

	/**
	 * @JCTL({"ds_xhr"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'xhr'})
	 */
	public function xhr( $operation ) {
		$args = func_get_args();

		if ( $operation == "object" ) {
			$oname = ucfirst( $args[ 1 ] );
			$a     = new $oname();
			$a->populateOnce( false )->nopop()->load( array( $args[ 2 ] => $args[ 4 ] ) );
			$ret = array();
			while ( $a->populate() ) {
				$ret[ ] = array( $a->id, $a->$args[ 3 ] );
			}

			echo json_encode( $ret );
			unset( $ret, $a, $oname );
		}
		elseif ( $operation == "loadLandingPages" ) {
			$advId = $args[ 1 ];
			$a = new CplCampaign();

			$a->orderBy( "jdate DESC" )
				->populateOnce( false )
				->leftJoin("campaign_cplCampaign")
				->nopop()
				->load( array( "cplCampaign_advertiser" => $advId, "campaign_cplCampaign" => array( "id" => "IS NULL" ) ) );
			$ret = array();

			while ( $a->populate() ) {
				$ret[ ] = array( $a->id, $a->name, $a->preHash );
			}

			echo json_encode( $ret );
			unset( $ret, $a );
		}
		elseif ( $operation == "landingPageDetails" ) {
			$lid = $args[ 1 ];
			$a = new CplCampaign( $lid );
			if ( $a->gotValue !== true ) {

				exit;
			}
			$ret = array(
				"preHash"=>$a->preHash
			);

			while ( $a->populate() ) {
				$ret[ ] = array( $a->id, $a->name, $a->preHash );
			}

			echo json_encode( $ret );
			unset( $ret, $a );
		}
		elseif ( $operation == "word" ) {
			$w = new LangWord();
			$w->nopop()->limit( 0, 20 )->load( array( "wordType" => "word", "wordCode" => "LIKE %" . trim( $args[ 1 ] ) . "%" ) );

			$ret = array();
			while ( $w->populate() ) {
				$ret[ ] = array( "value" => $w->wordCode, "id" => $w->id );
			}
			echo json_encode( $ret );

		}
		elseif ( $operation == "cplWord" ) {
			$w = new LangWord();
			$w->limit( 0, 20 )->nopop()->load( array( "wordCode" => "LIKE %cpl_" . trim( $args[ 1 ] ) . "%","categoryHex"=>1341 ) );

			$ret = array();
			while ( $w->populate() ) {
				$ret[ ] = array( "value" => str_replace("cpl_","",$w->wordCode), "id" => $w->id );
			}
			echo json_encode( $ret );
		}
		elseif( $operation == "getCplRemoteClickCount"){
			 echo Campaign::getRedClickCount( $args[ 1 ] );
		}
		elseif( $operation == "getRemoteServiceDetails"){
			$id = isset($args[1])?$args[1]:null;
			$a = new AffiliateProviders( $id );

			if($a->gotValue){
				echo json_encode(
					array(
						"gotApi"=>$a->apiSupport,
						"details"=>$a->details,
						"tpSupport"=>$a->trackingPixelSupport,
						"tpUrl"=>$a->trackingPixelUrl,
						"tpInfo"=>$a->trackingPixelInformation,
						"hash"=>$a->hashCode
					)
				);

				exit;
			}
		}
		elseif( $operation == "getCplRemoteHashClickCount"){
			$a = new CplRemoteCampaignUser($args[ 1 ]);
			if($a->gotValue && strlen($a->shortUrl)>0){
				$count = $a->getClickCount( true );

				echo $count;
			}

		}
		elseif( $operation == "layoutProps"){
			$c = new CplLayout($args[1]);
			echo $c->options;
		}
		elseif( $operation == "refreshDS"){
			echo 1;
		}
		elseif( $operation == "getCPLRemoteManualCount"){
			$a = new CplRemoteCampaignUser( $args[ 1 ] );
			echo $a->currentCount > 0 ? $a->currentCount : 0;
		}
		elseif( $operation == "updateCPLRemoteManualStats"){
			$a = new Campaign( $args[ 1 ] );
			$a->loadCplRemoteCampaign();
			echo 'Current Count : ' . $a->campaign_cplRemoteCampaign->getActiveLeadCount() . ' / ' . $a->campaign_cplRemoteCampaign->maxLeadLimit;
		}
		elseif( $operation == "updateCPLRemoteManualCount"){
			$a         = new CplRemoteCampaignUser( $args[ 1 ] );
			$r = $a->updateLeadAmount( $args[ 2 ] );
			unset( $a );
			echo $r;
		}
		elseif( $operation == "cplNumberBox" ){
			$id = $args[ 1 ];

			$a = new CplCampaign( $id );
			$numbers = $a->countNumbers();

			JT::init();
			JT::assign( "numbers", $numbers );
			JT::assign( "maxLeadLimit", $a->maxLeadLimit );
			unset( $a );
			echo JT::pfetch( "DS_cplInfoBox" );

		}
	}

	/**
	 * @JCTL({"ds_backend"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'xhr'})
	 */
	public function backend() {
		JT::init();
		JT::assign( "_job", "backend" );
		echo JT::pfetch( "DS_backend" );
	}

	/**
	 * @JCTL({"ds_backendDetail"})
	 * @JCTLPre(func="checkRoot")
	 */
	public function backendDetail($id) {
		$a = new Backend($id);
		$c = $a->getDetails();
		JT::init();
		JT::assignByRef( "obj", $a );
		JT::assignByRef( "content", $c );
		JT::assign( "_job", "backend" );
		echo JT::pfetch( "DS_backendDetail" );
	}

	/**
	 * @JCTL({"ds_backendList"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'advertisers L'})
	 */
	public function backendList() {
		$s        = JUtil::getSearchParameter();
		$sortings = array( "jdate", "job", "label","status" );

		$searchParams = json_decode( html_entity_decode( str_replace( '\"', '"', $s[ "sSearch" ] ) ), true );

		$filter = array();
		if ( isset( $s[ "sSearch_0" ] ) && strlen( $s[ "sSearch_0" ] ) > 0 )
			$filter[ "_custom" ] = "lower(`job`) LIKE lower('%" . $s[ "sSearch_0" ] . "%')";

		if ( isset( $s[ "sSearch_1" ] ) && strlen( $s[ "sSearch_1" ] ) > 0 )
			$filter[ "_custom" ] = "lower(`label`) LIKE lower('%" . $s[ "sSearch_1" ] . "%')";

		if ( isset( $s[ "sSearch" ] ) && strlen( $s[ "sSearch" ] ) > 0 )
			$filter[ "_custom" ] = "lower(`job`) LIKE lower('%" . $s[ "sSearch" ] . "%') OR lower(`label`) LIKE lower('%" . $s[ "sSearch" ] . "%')";

		$filter[ "_custom" ] = "`parentBackend` IS NULL " . ( isset( $filter[ "_custom" ] ) ? " AND (" . $filter[ "_custom" ] . ")" : "" );


		$a = new Backend();
		$a->count( "id", "cid", $filter );
		$count = $a->gotValue ? $a->cid : 0;

		$a = new Backend();
		$a->orderBy( $sortings[ $s[ "iSortCol_0" ] ] . " " . $s[ "sSortDir_0" ] )
			->limit( $s[ "iDisplayStart" ], $s[ "iDisplayLength" ]==-1?200:$s[ "iDisplayLength" ] );
		$a->nopop()->load($filter);

		$response = array(
			"sEcho"                => $s[ "sEcho" ],
			"iTotalRecords"        => $count,
			"iTotalDisplayRecords" => $count,
			"aaData"               => array()
		);

		while($a->populate()){
			$response[ "aaData" ][ ] = array(
				date("d-m-Y H:i",$a->jdate),
				$a->job,
				$a->label,
				$a->status_options[$a->status],
				$a->countChildren(),
				null,
				"rowId"=>$a->id
			);
		}
		unset( $a );
		echo json_encode( $response );
	}


	/**
	 * @JCTL({"ds_backendPreview"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'pending job L'})
	 */
	public function backendPending( $code=null ){
		if ( is_null( $code ) ) {
			JUtil::jredirect( "/ds_backendList" );
		}

		$b = Backend::loadByCode( $code );

		if($b === false){
			die( "Backend job not found" );
		}

		JT::init();
		JT::assignByRef( "obj", $b );
		echo JT::pfetch("DS_backendPreview");
	}

	/**
	 * @JCTL({"ds_backendStatus"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'pending job L'})
	 */
	public function backendStatus( $code=null ){
		if ( is_null( $code ) ) {
			echo json_encode( array( "status" => "error" ) );
			exit;
		}

		$b = Backend::loadByCode( $code );

		if($b === false){
			echo json_encode( array( "status" => "error" ) );
			exit;
		}

		if( $b->status == Backend::ENUM_STATUS_WORKING ){
			echo json_encode( array( "status" => "still" ) );
		}
		elseif($b->status == Backend::ENUM_STATUS_COMPLETE){
			echo json_encode( array( "status" => "good", "url"=>$b->redirectURL."/".$b->returnValue ) );
		}
		elseif($b->status == Backend::ENUM_STATUS_ERROR || $b->status == Backend::ENUM_STATUS_KILLED){
			echo json_encode( array( "status" => "error" ) );
		}
	}

	/**
	 * @JCTL({"ds_deldsNotification"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'notifications D'})
	 */
	public function delNotification( $id ) {
		$a = new DSNotificationEmails( $id );

//		$a->active = 0;
		$a->delete();
		unset( $a );
		JUtil::jredirect( "ds_dsNotifications" );
	}


	/**
	 * @JCTL({"ds_dsAddNotificationAction"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'notifications A/E'})
	 */
	public function notificationAction() {

		$a = new DSNotificationEmails( isset( $_POST[ "id" ] ) ? $_POST[ "id" ] : null );

		$a->name     = $_POST[ "name" ];
		$a->email     = $_POST[ "email" ];

		$a->save();

		unset( $a );
		JUtil::jredirect( "ds_dsNotifications" );
	}



	/**
	 * @JCTL({"ds_dsAddNotification"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'notifications N'})
	 */
	public function addNotification( $id = null ) {
		$a = new DSNotificationEmails( $id );

		JT::init();
		JT::assign( "_item", $a );
		JT::assign( "_job", "dsAddNotification" );
		echo JT::pfetch( "DS_form_dsNotification" );
	}


	/*notifications*/
	/**
	 * @JCTL({"ds_dsNotifications"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'notifications L'})
	 */
	public function notifications() {
		$a = new DSNotificationEmails();
		$a->load();

		JT::init();
		JT::assign( "users", $a );
		echo JT::pfetch( "DS_dsNotifications" );
	}

	/**
 	 * @JCTL({"ds_generalForm"})
	 * @JCTLPre(func="checkRoot")
	 * @JCTLPost(func="logOperation", args={'general L'})
	 * @param      $obj
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function generalForm( $obj, $id=null ) {
		JT::init();
		JT::assign( "object", $obj );
		JT::assign( "id", $id );
		JT::assign("options", array("email"=>"email","mobile"=>"mobile","dashboard"=>"dashboard",));

		echo JT::pfetch( "DS_form_generalForm" );
	}


}