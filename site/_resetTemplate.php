<?php
function delTree($dir) {
   $files = array_diff(scandir($dir), array('.','..')); 
    foreach ($files as $file) { 
      (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file"); 
    } 
    return rmdir($dir); 
}

delTree("../temp/templateCompiled");
delTree("../temp/templateCache");
mkdir("../temp/templateCompiled",0777);
mkdir("../temp/templateCache",0777);

?>