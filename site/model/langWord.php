<?php
/**
 * langWord real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/langWord.php';

class LangWord extends LangWord_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * get langwords by type and optionally by language
	 *
	 * @return array
	 * @author Murat
	 * @author Özgür Köy
	 */
	public static function getLangWords($wordType, $langShort, $wordCode=null){
		$_wordTexts = null;
		$cacheKey = 'langWord_' . $langShort .'_' . $wordType.(is_null($wordCode)?"":"_".$wordCode);

		if ( ( $_wordTexts = JCache::read( $cacheKey ) ) === false ) {
			$langwords = new LangWord();
			$langwords->populateOnce( false );

			$filter = array(
				"wordType"     => $wordType,
				"langShortDef" => $langShort
			);

			if ( !is_null( $wordCode ) ) {
				$filter[ "wordCode" ] = $wordCode;
				//$filter[ "_custom" ] = 'lower("wordCode")=lower('. JPDO::quote( $wordCode ) . ')' ;
			}

			$langwords->load( $filter );

			do {
				$_wordTexts[ $langwords->wordCode ] = Format::replaceGlobals( $langwords->wordText );
			} while ( $langwords->populate() );

			JCache::writeDirect( $cacheKey, $_wordTexts );

		}

		return $_wordTexts;
	}

	/**
	 *
	 * check wordCode validity
	 *
	 * @return bool
	 * @author murat
	 **/
	public static function checkWordCode($wc)
	{
		if (strlen($wc)>0 && !is_numeric(substr($wc,0,1)) && preg_match('/^[a-z0-9\d_]{1,200}$/i', $wc)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Create history record for LangWord
	 *
	 * @return void
	 * @author Murat
	 */
	public function createHistory(){
		JPDO::connect( JCache::read('JDSN.MDB') );
		$sql = "
			INSERT INTO langWordHistory(jdate,langShort,wordType,wordCode,wordtext,createEnv,createDate,updateDate,updateEnv)
			SELECT jdate,langShort,wordType,wordCode,wordtext,createEnv,createDate,updateDate,updateEnv
			FROM langWord
			WHERE wordType = '{$this->wordType}' AND wordCode = '{$this->wordCode}'
		";
		JPDO::executeQuery($sql);
	}

	/**
	 * Compare the given langWords array with database
	 *
	 * @return void
	 * @author Murat
	 */
	public static function compareLangWords($langWordsFromFile){
		$sql = "
				SELECT
					langShort,
					wordType,
					wordCode,
					wordText
				FROM
					langWord
		";
		JPDO::connect(JCache::read('JDSN.MDB'));
		$wordTexts = JPDO::executeQuery($sql);
		$lw = array();
		if (count($wordTexts)){
			foreach($wordTexts as $wt){
				$lw[$wt["langShort"]][$wt["wordType"]][$wt["wordCode"]] = $wt["wordText"];
			}
		}

		foreach ($langWordsFromFile as $langShort => $wordTypeContent) {
			$lang = new Lang();
			$lang->load(array("langShort"=>$langShort));
			if (!$lang->gotValue){
				$lang->langShort = $langShort;
				$lang->save();
			}
			foreach ($wordTypeContent as $wordType => $wordContent) {
				foreach ($wordContent as $wordCode => $wordText) {
					if (array_key_exists($langShort,$lw) && array_key_exists($wordType,$lw[$langShort]) && array_key_exists($wordCode,$lw[$langShort][$wordType])){
						if ($wordText->wordText != $lw[$langShort][$wordType][$wordCode]){
							Tool::echoLog("different word found. lets update : $langShort | $wordType | $wordCode<br>");
							//Tool::echoLog("old word: " . $wordText->wordText);
							//Tool::echoLog("new word: " . $lw[$langShort][$wordType][$wordCode]);
							$langWord = new LangWord();
							/*
							$langWord->langShort = $langShort;
							$langWord->wordType = $wordType;
							$langWord->wordCode = $wordCode;
							$langWord->load();
							*/
							$langWord->load(array(
								"langShort" => $langShort,
								"wordType" => $wordType,
								"wordCode" => $wordCode
							));

							$langWord->wordText = $wordText->wordText;
							$langWord->createDate = $wordText->createDate;
							$langWord->createEnv = $wordText->createEnv;
							$langWord->updateDate = $wordText->updateDate;
							$langWord->updateEnv = $wordText->updateEnv;
							$langWord->createHistory(); // Create history record before updating it
							$langWord->save();
						}
					} else {
						Tool::echoLog("word not found in database, let's insert it : $langShort | $wordType | $wordCode<br>");
						//echo $langShort . "--" . $wordType . "--" . $wordCode . "<br>\n";
						//die("<pre>".print_r($lw,true));
						$langWord = new LangWord();
						$langWord->lang_langWord = $lang->id;
						$langWord->langShort = $langShort;
						$langWord->wordType = $wordType;
						$langWord->wordCode = $wordCode;
						$langWord->wordText = $wordText->wordText;
						$langWord->createDate = $wordText->createDate;
						$langWord->createEnv = $wordText->createEnv;
						$langWord->updateDate = $wordText->updateDate;
						$langWord->updateEnv = $wordText->updateEnv;
						$langWord->save();
					}
				}
			}
		}
		unset($wordTexts,$lw,$langWordsFromFile);

		$cantConnectToMemcache = false;
		$memcache = new Memcache;
		$memcache->connect(MEMCACHE_SERVER, MEMCACHE_PORT) or $cantConnectToMemcache = true;

		if (!$cantConnectToMemcache){
			$memcache->flush();
			$memcache->close();
		}
		Tool::echoLog("lang word compare finished successfully<br /><hr />\n");
	}

	/**
	 * Build and save category hex from language system
	 * @param $categories
	 * @return bool
	 * @author Özgür Köy
	 */
	public function buildCategoryHex( $categories ) {
		$a = explode( ",", $categories );
		$e = 0;

		if ( sizeof( $a ) > 0 ) {
			$e = 0;

			foreach ( $a as $a0 ) {
//				$a0 = strlen($a0)>6?substr($a0,0,5).substr($a0,-1).strlen($a0):$a0;
//				$a0 = hexdec( JUtil::strToHex( trim( $a0 ) ) );
				$a0 = self::strToInt( $a0 );
				$e  = ( $e == 0 ? $a0 : ( $e | $a0 ) );
			}
			//$e = dechex( $e );
		}

		$this->categoryHex = $e;
		$this->save();
		unset( $e );

		return true;
	}

	/**
	 * load categories by language and categories if needed
	 *
	 * @param       $lang
	 * @param array $cats
	 * @return array
	 * @author Özgür Köy
	 */
	public static function collectWords( $lang, $cats = array(), $and = false ) {
		return JCache::read( "langCollection_" . $lang . "_" . implode( "_", $cats ), function () use ( $lang, $cats, $and ) {
			$filter = array( "langShortDef" => $lang );
			$catz = array();
			foreach ( $cats as $arg ) {
				if ( strlen( trim( $arg ) ) == 0 ) continue;
				$e = LangWord::strToInt( $arg );
				$catz[] = ("(`categoryHex`" . ( $and ? "&" : "|" ) . "{$e}=`categoryHex`)");
			}
			$filter[ "_custom" ] = "(`categoryHex`>0) AND ( ".implode(" OR ", $catz)." )";

//			print_r( $filter );exit;

			$l      = new LangWord();

//			JPDO::$debugQuery=true;
			$l->populateOnce(false)->nopop()->load( $filter );
//			JPDO::$debugQuery=false;
			$ret = array();
			while ( $l->populate() ) {
				$ret[ $l->wordCode ] = Format::replaceGlobals( $l->wordText );
			}
			JCache::writeDirect( "langCollection_" . $lang . "_" . implode( "_", $cats ), $ret );
			return $ret;
		} );

	}

	public static function collectWordArray( $lang, $words = array() ) {
		return JCache::read( "langWordCollection_" . $lang . "_" . implode( "_", $words ), function () use ( $lang, $words ) {
			$filter = array( "langShortDef" => $lang, "wordCode"=>array($words) );

			$l      = new LangWord();
			$l->populateOnce(false)->nopop()->load( $filter );
			$ret = array();

			while ( $l->populate() )
				$ret[ $l->wordCode ] = Format::replaceGlobals( $l->wordText );

			JCache::writeDirect( "langWordCollection_" . $lang . "_" . implode( "_", $words ), $ret );
			return $ret;
		} );
	}

	public static function strToInt( $str){
		$r = 0;
		for($i = 0 ;$i<strlen($str);$i++){
			$c0 = substr( $str, $i, 1 );
			if($i%2==0) $c0 = strtoupper( $c0 );
			$r += ord( $c0 );
			if($i%2==0) $r += ord( "_" ) + ( $i * 17 );

		}

		return $r * strlen( $str );
	}
}

