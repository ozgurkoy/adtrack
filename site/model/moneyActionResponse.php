<?php
/**
 * @SWG\Model
 */
class MoneyActionResponse
{

	/**
	 * @var int
	 * @SWG\Property
	 */
	public $id;


	/**
	 * @var string
	 * @SWG\Property
	 */
	public $label;

	/**
	 * @var string
	 * @SWG\Property(description="'PENDING':1 'APPROVED':2 'CANCELLED':3")
	 */
	public $state;

	/**
	 * @var int
	 * @SWG\Property(description="Unix time, must format according to user's local setting")
	 *
	 */
	public $unixTime;


	/**
	 * @var int
	 * @SWG\Property
	 */
	public $amount;

	/**
	 * @var int
	 * @SWG\Property
	 */
	public $amountAfter;

}
