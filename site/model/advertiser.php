<?php
/**
 * advertiser real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/advertiser.php';

class Advertiser extends Advertiser_base
{


	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Send Notification Mail when advertiser register to the site
	 * @author Sam Ben Yakar
	 */
	public function alertHq() {
		global $_S,$SYSTEM_EMAILS;

		$this->loadAdvertiser_advContact();
		$this->advertiser_advContact->loadUserEmail();
		$subject = "New Advertiser Registered - ".date("d/m/y H:i",time());
		$body = "<html><head></head><body>".
				"Company Name: " . $this->companyName. " <br/>" .
				"Contact  Name: " . $this->advertiser_advContact->name. " <br/>" .
				"Mobile Phone: " . $this->advertiser_advContact->contactGsm. " <br/>" .
				"Office Phone: " . $this->advertiser_advContact->contactTel. " <br/>" .
				"E-mail: " . $this->advertiser_advContact->contactUserEmail->email. " <br/>" .
				"Profile DS Link: " . JUtil::siteAddress()."ds_addAdvertiser/".$this->id ." <br/>" .
				"</body></html>";

		Mail::createManual($SYSTEM_EMAILS["sales"],$subject,$body);
	}

	/**
	 * @param $data
	 * @return $this
	 * @author Özgür Köy - modified
	 */
	public function updateProfile( $data ) {
		JPDO::beginTransaction();

		try {
			#country validation fix
			if ( !is_null( $data ) ) {
				//TODO : setup for multiple advertiser contacts ?
				$con = $this->loadAdvertiser_advContact();

				$con->contactGsm = $data[ "gsmNumber" ];
				$con->name       = $data[ "name" ];
				$con->title      = in_array($data[ "title" ],$con->title_options)?$data[ "title" ]:1;

				if ( isset( $data[ "email" ] ) ) {
					$oldEmail        = $this->user_advertiser->getDefaultEmail();
					$oldEmail->email = $data[ "email" ];

					$oldEmail->save();
				}

				if ( isset( $data[ "newPassword" ] ) && strlen( $data[ "newPassword" ] ) > 0 ) {
					if ( !is_object( $this->user_advertiser ) )
						$this->loadUser();

					$this->user_advertiser->pass = md5( $data[ "newPassword" ] );
					$this->user_advertiser->save();
				}

				$con->save();
			}

			if(!is_object($this->user_advertiser))
				$this->loadUser();

			$this->user_advertiser->fillSession();

			JPDO::commit();
		}
		catch ( JError $j ) {
			JPDO::rollback();

			return false;
		}

		return $this;
	}


	/**
	 * signup advertiser
	 * @param           $userData
	 * @param UserEmail $um
	 * @return Advertiser
	 * @author Özgür Köy
	 */
	public static function signup( $userData, UserEmail $um=null )
	{
		$a = new Advertiser();
		$a->webSite            = $userData[ "website" ];
		$a->companyName        = $userData[ "company" ];
		$a->address            = $userData[ "address" ];
		$a->country_advertiser = $userData[ "country" ];
		if(isset($userData["city"]))
			$a->city_advertiser    = $userData[ "city" ];
		$a->save();

		if(isset($userData["name"])){
			$ac               = new AdvContact();
			$ac->name         = $_POST[ "name" ];
			$ac->contactGsm   = $_POST[ "gsmCode" ].$_POST[ "gsmNumber" ];
			$ac->contactTel   = $_POST[ "phone" ];
			$ac->title        = $_POST[ "title" ];
			$ac->save();

			$ac->saveUserEmail( $um );

			$a->saveAdvertiser_advContact( $ac );

			unset( $ac );
		}

		// Notify HQ
		$a->alertHq();

		return $a;
	}

	/**
	 * Get all active advertisers
	 *
	 * @return Advertiser
	 * @author Murat
	 */
	public static function getAll(){
		$a = new Advertiser();
		$a->populateOnce( false )->load(array("user_advertiser"=>array("status"=>User::ENUM_STATUS_ACTIVE)));
		return $a;
	}

	/**
	 * advertiser current balance
	 *
	 * @author ozgur
	 * @return float
	 */
	public function currentBalance()
	{
		$rev = new AdvertiserMoneyAction();
		#JPDO::$debugQuery=true;
		$rev->having("`TOPLAM` IS NOT NULL")->sum( "amount", "TOPLAM", array( "advertiser_advertiserMoneyAction" => $this->id,
		                                                                    "state" => AdvertiserMoneyAction::ENUM_STATE_APPROVED ) );
		if ( $rev->gotValue ) {
			$sum = $rev->TOPLAM;
		}
		else
			$sum = 0;

		unset( $rev );
		return $sum;
	}

	public function getFirstContactInfo() {
		$ret = array("name"=>"-","surname"=>"-","email"=>"-","phone"=>"-");

		if($this->gotValue){
			$this->loadAdvertiser_advContact();
			if ( $this->advertiser_advContact && $this->advertiser_advContact->gotValue ) {
				$this->advertiser_advContact->loadUserEmail();
				$ret = array(
					"name"=>$this->advertiser_advContact->name,
					"email"=>$this->advertiser_advContact->contactUserEmail->email,
					"phone"=>$this->advertiser_advContact->contactTel
				);
			}
		}

		return $ret;
	}

	public static function generateUsernameFromCompName( $company ) {
		return substr(JUtil::normalize($company,true),0,9) . "_" . strtolower(JUtil::randomString( "4" ));
	}

}

