<?php
/**
 * cplForm real class - firstly generated on 26-03-2014 12:46, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplForm.php';

class CplForm extends CplForm_base
{
	const WITH_PREVIEW = 100;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function buildForm() {
		$fields = $this->getFields();

		foreach ( $fields as $field ) {
			$f = new Field();
//			$field->
		}

	}

	/**
	 * get fields of the form
	 *
	 * @param bool $withPreview
	 * @return array
	 * @author Özgür Köy
	 */
	public function getFields( $withPreview = false ) {
		$fcol = array();
		$fields = $this->loadFields(null,0,100,"forder ASC");
		if ( $fields->gotValue ) {
			do {
				$fields->parseOptions();
				$fields->loadField();

				if ( $withPreview == CplForm::WITH_PREVIEW )
					$preview = $fields->field->buildPreview( $fields->label, $fields->options, $fields->pfield );
				else
					$preview = null;


				$fcol[ CplLayout::norm($fields->label) ] = array(
					"label"       => $fields->label,
					"info"        => $fields->info,
					"field"       => $fields->field->type,
					"def"         => $fields->pfield,
					"options"     => $fields->options,
					"isMandatory" => $fields->isMandatory,
					"regex"       => $fields->field->regex,
					"isUnique"    => $fields->isUnique,
					"noLabel"     => $fields->field->noLabel,
					"preview"     => $preview
				);

			} while ( $fields->populate() );
		}
		unset( $fields );
		return $fcol;
	}
}

