<?php
/**
 * mailHistory real class - firstly generated on 10-11-2014 15:01, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/mailHistory.php';

class MailHistory extends MailHistory_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	public static function log( $title, $text=""  ) {
		$h = new MailHistory();
		$h->action = $title;
		$h->content = $text;
		$h->save();
	}
}

