<?php
	/**
	* companyBrand real class for Jeelet - firstly generated on 09-03-2012 17:05, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/companyBrand.php");

	class CompanyBrand extends CompanyBrand_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}
		
		/**
		 * check the campaign brand that exists
		 * 
		 * @return bool
		 * @author murat
		 */
		 public static function checkUnique($companyId, $brandName, $brandID = null){
		 	$cb = new CompanyBrand();
			$cb->company = $companyId;
			$cb->title = $brandName;
			$cb->load();
			if ($cb->count && $brandID != $cb->id){
				unset($cb);
				return true;
			} else {
				unset($cb);
				return false;
			}
		 }
	}

	?>