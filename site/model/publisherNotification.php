<?php
/**
 * publisherNotification real class - firstly generated on 21-12-2013 13:39, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherNotification.php';

class PublisherNotification extends PublisherNotification_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	/**
	 * @param Publisher $publisher
	 * @author Özgür Köy
	 */
	public static function activateAllNotifications( Publisher &$publisher ) {
		$n = new PublisherNotification();
		$n->nopop()->load( array( "isActive"=>1 ) );
		while($n->populate()){
			$n->savePublisherofNotifications( $publisher );
		}
	}
}

