<?php
/**
 * Campaign Validation Class
 *
 * @package default
 * @author Murat
 **/
class ValidateCampaign extends Validate
{
	const TEST_PUBLISHER = -99;

	/**
	 * Campaign validation error codes
	 * @var array
	 */
	public static $errorCodes = array(
		301 => "Undefined campaign race",
		302 => "Invalid start date",
		303 => "Invalid end date",
		304 => "Brandname already used",
		305 => "Campaign min. start date did not reached",
		306 => "Campaign max. day exceeded",
		307 => "Campaign min. day insufficient",
		308 => "Campaign max. day should be greater than the min. day",
		309 => "Campaign not found",
		310 => "Campaign offer action taken before",
		311 => "Custom message empty",
		312 => "Custom message character count is over limit",
		313 => "Invalid campaign message selected",
		314 => "Invalid send date",
		315 => "Invalid send time",
		316 => "Tweet send time should be in range",
		317 => "Tweet send time should be after current time",
		318 => "Late to take action",
		319 => "Budget ended",
		320 => "Unknown state to take action",
		321 => "Invalid camp list type",
		322 => "Invalid orderBy type",
		323 => "Invalid page number",
		324 => "Invalid status",
		325 => "Campaign status can't set to back",
		326 => "Age range should be selected",
		327 => "Invalid age range selected",
		328 => "Invalid gender",
		329 => "Invalid interest",
		330 => "Invalid link",
		331 => "You should upload an campaign preview image",
		332 => "Invalid preview image file",
		333 => "Preview image file size should be maximum : ",
		334 => "Campaign message length exceeded the limit",
		335 => "Value should be numeric",
		336 => "Invalid publish date. Date should be greater than today and less then campaign end date.",
		337 => "Effect campaigns should publish to publishers before campaign start date.",
		338 => "Campaign offer is not approved or campaign already published",
		339 => "Max. click count should be greater then the current max. click count.",
		340 => "Max. lead count should be greater then the current max. lead count.",
		341 => "Link must have a [code] parameter in it for replacement",
		342 => "Value required",
		343 => "Rev percentage must be between 10-90"
	);

	/**
	 * error fields for validation
	 * @var array
	 */
	public static $errorFields = array(
		301 => "race",
		302 => "startDate",
		303 => "endDate",
		304 => "newBrand",
		305 => "startDate",
		306 => "startDate",
		307 => "endDate",
		308 => "endDate",
		309 => "race",
		311 => "customMessage",
		314 => "sendDate",
		315 => "sendTime",
		316 => "sendDate",
		317 => "sendDate",
		326 => "agerange",
		327 => "agerange",
		328 => "sex",
		329 => "interests",
		330 => "link",
		331 => "uploadfile",
		332 => "uploadfile",
		336 => "publishDate",
		320 => "publishDate",
		341 => "link",
	);

	public static $errorLangDefs = array(
		310 => "reActOnCampaign",
		311 => "manErr",
		314 => "manErr",
		315 => "chooseValidHour",
		316 => "chooseBetweenStartEnd",
		317 => "chooseFutureDate",
		318 => "lateToJoinToCampaign",
		319 => "budgetEndedOnCampaign",
		320 => "notTargettedOnCampaign",
	);


	/**
	 * Valid campaign races
	 *
	 * @var array
	 */
	public static $validCampaignRaces = array(
		Campaign::ENUM_RACE_TWITTER,
		Campaign::ENUM_RACE_CPC,
		Campaign::ENUM_RACE_CPL,
		Campaign::ENUM_RACE_CPL_REMOTE,
		Campaign::ENUM_RACE_CPA_AFF
	);

	/**
	 * Valid camp list types
	 *
	 * @var array
	 */
	public static $validCampListTypes = array("o", "a", "f", "r");

	/**
	 * Valid list orderby strings
	 *
	 * @var array
	 */
	public static $validOrderBys = array("date","title","state","race");

	/**
	 * Valid age ranges
	 *
	 * @var array
	 */
	public static $validAgeRanges = array(
		"13-17"=>"13-17",
		"18-24"=>"18-24",
		"25-34"=>"25-34",
		"35-44"=>"35-44",
		"45-54"=>"45-54",
		"55+"=>"55+"
	);

	/**
	 * Valid gender types
	 *
	 * @var array
	 */
	public static $validGenders = array(
		CampaignFilter::ENUM_SEX_MALE => "Male",
		CampaignFilter::ENUM_SEX_FEMALE => "Female",
		CampaignFilter::ENUM_SEX_BOTH => "Both"
	);

	/**
	 * Valid preview image extensions
	 *
	 * @var array
	 */
	public static $validImageExtensions = array("gif", "jpeg", "jpg", "png");

	/**
	 * Valid preview image file mime type
	 *
	 * @var array
	 */
	public static $validImageTypes = array(
		"image/gif",
		"image/jpeg",
		"image/jpg",
		"image/pjpeg",
		"image/x-png",
		"image/png"
	);

	/**
	 * Valid preview image maximum file size (in bytes)
	 *
	 * @var float
	 */
	public static $maxImageFileSize =  10000000;

	/**
	 * Check campaign first step parameter
	 *
	 * @param array $params
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateStep1( &$params, &$r = null){
		//Create validation response
		$r = new ValidationResponse();

		//Define mandatory fields for params array
		$mFields = array("race");

		//Check params is array and all mandatory fields were set
		self::mandatoryCheck($params,$mFields,$r);

		//If $params is array and all mandatory fields were set, lets check the proper values
		if (empty($r->errorCode)){
			if (!in_array($params["race"],self::$validCampaignRaces)){
				$r->errorCode = 301;
			}
		}

		//If there's an error, let set valid false and give the errorDetail
		if ( !empty( $r->errorCode ) ) {
			$r->valid = false;

			if ( empty( $r->errorDetail ) ) $r->errorDetail = self::$errorCodes[ $r->errorCode ];
		}

		return self::response($r);
	}

	/**
	 * Check campaign second step parameter
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateStep2( &$params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("race","title","startDate","endDate","brand");

		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			//die(print_r($params));
			if (!in_array($params["race"],self::$validCampaignRaces)){
				$r->errorCode = 301;
			} elseif (!Validate::Date($params["startDate"],"Y-m-d")){
				$r->errorCode = 302;
			} elseif (!Validate::Date($params["endDate"],"Y-m-d")){
				$r->errorCode = 303;
			} elseif ($params["brand"] == -1){

				self::mandatoryCheck($params,array("advertiserId","brandName"),$r);
				if (empty($r->errorCode)){

					if (AdvertiserBrand::checkUnique($params["advertiserId"],$params["brandName"]))
						$r->errorCode = 304;
				}
			}
			if (empty($r->errorCode)) {
				$start_date = date_parse_from_format('Y-m-d', $params["startDate"]);
				$end_date = date_parse_from_format('Y-m-d', $params["endDate"]);

				$start_date = new DateTime($start_date["year"] . "-" . $start_date["month"] . "-" . $start_date["day"]);
				$end_date = new DateTime($end_date["year"] . "-" . $end_date["month"] . "-" . $end_date["day"]);

				$datediff = date_diff(new DateTime(date('Y-m-d')), $start_date);
				$dd = $datediff->y * 360 + $datediff->m*30 + $datediff->d;

				$setting = new Setting();
				if ($params["race"]==Campaign::ENUM_RACE_CPC){
					$campaignMaxDays      = Setting::getValue( "cpcCampaignMaxDays" );
					$campaignMinDays      = Setting::getValue( "cpcCampaignMinDays" );
					$campaignMinStartDate = Setting::getValue( "cpcCampaignMinStartDate" );
				}
				elseif ($params["race"]==Campaign::ENUM_RACE_CPL){
					$campaignMaxDays      = Setting::getValue( "cplCampaignMaxDays" );
					$campaignMinDays      = Setting::getValue( "cplCampaignMinDays" );
					$campaignMinStartDate = Setting::getValue( "cplCampaignMinStartDate" );
				}
				elseif ($params["race"]==Campaign::ENUM_RACE_CPL_REMOTE){
					$campaignMaxDays      = Setting::getValue( "cplCampaignMaxDays" );
					$campaignMinDays      = Setting::getValue( "cplCampaignMinDays" );
					$campaignMinStartDate = Setting::getValue( "cplCampaignMinStartDate" );
				}
				elseif ($params["race"]==Campaign::ENUM_RACE_CPA_AFF){
					$campaignMaxDays      = Setting::getValue( "cpaAffCampaignMaxDays" );
					$campaignMinDays      = Setting::getValue( "cpaAffCampaignMinDays" );
					$campaignMinStartDate = Setting::getValue( "cpaAffCampaignMinStartDate" );
				}
				else {
					$campaignMaxDays      = Setting::getValue( "campaignMaxDays" );
					$campaignMinDays      = Setting::getValue( "campaignMinDays" );
					$campaignMinStartDate = Setting::getValue( "campaignMinStartDate" );
				}
				$campaignMaxDays = intval($campaignMaxDays) - 1;

				$state = null;

				if (!empty($params["campaign"])){
					if (in_array($params["campaign"]->state,array(Campaign::ENUM_STATE_PROGRESSING,Campaign::ENUM_STATE_FINISHED)))
						$state = "a";
				}

				if (
					is_null($state) &&
					(
						($params["race"]==Campaign::ENUM_RACE_TWITTER && $dd < 1) ||
						$datediff->invert ||
						(empty($params["fromDarkside"]) && $dd < $campaignMinStartDate)
					)
				) {
					$r->errorCode = 305;
					$r->errorParams = array("campaignMinStartDate" => $campaignMinStartDate);
				}

				$datediff = date_diff($start_date, $end_date);
				$dd = $datediff->y * 360 + $datediff->m*30 + $datediff->d;

				if (empty($params["fromDarkside"]) && $dd > $campaignMaxDays) {
					$r->errorCode = 306;
					$r->errorParams = array("campaignMaxDays" => $campaignMaxDays);
				}
				elseif (empty($params["fromDarkside"]) && $dd < $campaignMinDays) {
					$r->errorCode = 307;
					$r->errorParams = array("$campaignMinDays" => $campaignMinDays);
				}
				elseif ($dd < 0 || $datediff->invert) {
					$r->errorCode = 308;
				}
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Check campaign third step parameter
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateStep3( &$params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("agerange","sex","interests");

		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			if ( !is_array( $params[ "agerange" ] ) || count( $params[ "agerange" ] ) < 1 ) {
				$r->errorCode = 326;
			}
			else {
				foreach ( $params[ "agerange" ] as $agerange ) {
					if ( !in_array( $agerange, self::$validAgeRanges ) ) {
						$r->errorCode = 327;
						break;
					}
				}
				if ( empty( $r->errorCode ) ) {
					if ( !in_array( $params[ "sex" ], array_keys( self::$validGenders ) ) ) {
						$r->errorCode = 328;
					}
					else {
						$trend = new Trend();
						$trend->load( array( "id" => $params[ "interests" ] ) );
						if ( !$trend->gotValue ) {
							$r->errorCode = 329;
						}
					}
				}
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Check campaign fourth step parameter
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateStep4( &$params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("message","race");

		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode) && ($params["race"] == Campaign::ENUM_RACE_CPC || !empty($params["campLink"]))){
			$mFields = array("SLDomain","link","campDetail","campNotes");

			self::mandatoryCheck($params,$mFields,$r);

			if (empty($r->errorCode)){

				if (!self::validateURL($params["link"])){
					$r->errorCode = 330;
				}
				else if (!self::validateURL($params["SLDomain"])){
					$r->errorCode = 330;
				}
				else {
					//self::validateImage($params,false,$r);
					if (empty($params["previewFileExist"]) && !isset($_SESSION["tempCampPicture"])){
						$r->errorCode = 332;
					}
				}
			}
		}

		if (
			empty( $r->errorCode ) &&
			in_array( $params[ "race" ], array( Campaign::ENUM_RACE_CPL, Campaign::ENUM_RACE_CPL_REMOTE, Campaign::ENUM_RACE_CPA_AFF ) )
		) {
			$mFields = array("SLDomain", "campDetail", "campNotes" );

			if ( in_array( $params[ "race" ], array( Campaign::ENUM_RACE_CPL_REMOTE, Campaign::ENUM_RACE_CPA_AFF ) ) && !self::validateURL( $params[ "link" ] ) )
				$r->errorCode = 330;

			if (!self::validateURL($params["SLDomain"])){
				$r->errorCode = 330;
			}

			if ( empty( $r->errorCode ) )
				self::mandatoryCheck( $params, $mFields, $r );

			if ( empty( $r->errorCode ) ) {
				//self::validateImage($params,false,$r);
				if ( empty( $params[ "previewFileExist" ] ) && !isset( $_SESSION[ "tempCampPicture" ] ) ) {
					$r->errorCode = 332;
				}
			}
		}

		if (empty($r->errorCode)){
			$maxChar = (!empty($params["link"]) ? Setting::getValue("campaignCharLimitWithLink") : Setting::getValue("campaignCharLimitWithoutLink"));
			for ( $i = 1; $i <= count($params[ "message" ]); $i++ ) {
				if ( !empty( $params[ "message" ][ "$i" ] ) && mb_strlen( $params[ "message" ][ "$i" ] ) > $maxChar ) {
					$r->errorCode  = 334;
					$r->errorField = "message[$i]";
				}
			}
		}

		if ( !empty( $r->errorCode ) ) {
			$r->valid = false;
			if ( empty( $r->errorDetail ) ) $r->errorDetail = self::$errorCodes[ $r->errorCode ];
		}

		return self::response($r);
	}

	/**
	 * Check campaign fifth step parameter
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateStep5( &$params, &$r = null){
		$r = new ValidationResponse();
		if ( $params[ "race" ] == Campaign::ENUM_RACE_CPA_AFF )
			$params["budget"] = 0;

		$mFields = array("commissionRate","budget","race");

		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			if ( $params[ "race" ] == Campaign::ENUM_RACE_CPC ) {
				$mFields = array( "cpcPrice", "maxClick" );

				self::mandatoryCheck( $params, $mFields, $r );
				if ( empty( $r->errorCode ) ) {
					$mFields = array( "commissionRate", "budget", "cpcPrice", "maxClick", "maxAnonRefCount", "keepRedirecting" );
					foreach ( $mFields as $field ) {
						if ( !is_numeric( $params[ $field ] ) ) {
							$r->errorCode  = 335;
							$r->errorField = $field;
							break;
						}
					}
				}
			}
			elseif ( $params[ "race" ] == Campaign::ENUM_RACE_CPL ) {
				$mFields = array( "perLead", "maxLeadLimit" );

				self::mandatoryCheck( $params, $mFields, $r );
				if ( empty( $r->errorCode ) ) {
					$mFields = array( "commissionRate", "budget", "perLead", "maxLeadLimit", "maxOverlappingNegative", "layout" );
					foreach ( $mFields as $field ) {
						if ( !is_numeric( $params[ $field ] ) ) {
							$r->errorCode  = 335;
							$r->errorField = $field;
							break;
						}
					}
				}
			}
			elseif ( $params[ "race" ] == Campaign::ENUM_RACE_CPL_REMOTE ) {
				$mFields = array( "perLead", "maxLeadLimit","hashCode" );

				self::mandatoryCheck( $params, $mFields, $r );
				if ( empty( $r->errorCode ) ) {
					$mFields = array( "commissionRate", "budget", "perLead", "maxLeadLimit", "opType", "affService" );
					foreach ( $mFields as $field ) {
						if ( !is_numeric( $params[ $field ] ) ) {
							$r->errorCode  = 335;
							$r->errorField = $field;
							break;
						}
					}

				}
			}
			elseif ( $params[ "race" ] == Campaign::ENUM_RACE_CPA_AFF ) {
				$mFields = array( "revPerc", "commissionInfo","hashCode", "opType", "affService" );

				self::mandatoryCheck( $params, $mFields, $r );
				if ( empty( $r->errorCode ) ) {
					$mFields = array( "revPerc", "affService" );
					foreach ( $mFields as $field ) {
						if ( !is_numeric( $params[ $field ] ) ) {
							$r->errorCode  = 335;
							$r->errorField = $field;
							break;
						}
					}

				}

				if (empty($r->errorCode)){
					if ($params["revPerc"]<10 || $params["revPerc"]>90){
						$r->errorCode = 343;
						$r->errorField = "revPerc";
					}
				}
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Check campaign image
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return void|bool|ValidationResponse
	 */
	public static function validateImage($params, $returnResponse = false, &$r = null){
		if (is_null($r)) $r = new ValidationResponse();

		if (empty($params["previewFileExist"]) && empty($_FILES["previewImage"])){
			$r->errorCode = 331;
		}
		else {
			if (empty($params["previewFileExist"]) || (!empty($params["previewFileExist"]) && isset($_FILES["previewImage"]) && strlen($_FILES["previewImage"]["name"]))){
				$temp = explode(".", $_FILES["previewImage"]["name"]);
				if ( count( $temp ) < 2 ) {
					$r->errorCode = 332;
				} else {
					$extension = strtolower(end($temp));
					if (!in_array($extension,self::$validImageExtensions) || !in_array($_FILES["previewImage"]["type"],self::$validImageTypes)){
						$r->errorCode = 332;
					} else if ($_FILES["previewImage"]["size"] > self::$maxImageFileSize){
						$r->errorCode = 333;
						$r->errorDetail = self::$errorCodes[333] . self::$maxImageFileSize . " bytes";
					}
				}
			}
		}

		if ($returnResponse){
			if (!empty($r->errorCode)){
				$r->valid = false;
				if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
			}

			return self::response($r);
		}
	}

	/**
	 * Validate image binary
	 *
	 * @param $params
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateImageBinary($params){
		//Create validation response
		$r = new ValidationResponse();

		$base64 = $params['image'];

		$fileType = null;

		if (substr($base64, 0, 5) == 'data:') {
			$base64 = preg_replace('#^data:image/[^;]+;base64,#', '', $base64);
			$base64 = base64_decode($base64);
			$fileType = self::getImageMimeType($base64);
		}
		else {
			$base64 = strtok($base64, '?');
			list($height, $width, $type) = getimagesize($base64);
			if ($type == 1){
				$fileType = "gif";
			}else if ($type == 2){
				$fileType = "jpg";
			}else if ($type == 3) {
				$fileType = "png";
			}
		}

		if (is_null($fileType) || !in_array($fileType,explode(",",Setting::getValue("validImageTypes"))))
			$r->errorCode = 332;

		//If there's an error, let set valid false and give the errorDetail
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Validate CPC campaign max click count
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function cpcClickCount($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("maxClick","maxAnonRefCount");
		self::mandatoryCheck($params,$mFields,$r);
		if (empty($r->errorCode)){
			foreach ($mFields as $field){
				if (!is_numeric($params[$field])){
					$r->errorCode = 335;
					$r->errorField = $field;
					break;
				}
			}
			if (empty($r->errorCode)){
				if ($params["campaign"]->campaign_campaignCPC->maxClick > $params["maxClick"]){
					$r->errorCode = 339;
					$r->errorField = "maxClick";
				}
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Validate CPL campaign
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function cplInfo($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("maxLeadLimit","maxOverlappingNegative");
		self::mandatoryCheck($params,$mFields,$r);
		if (empty($r->errorCode)){
			foreach ($mFields as $field){
				if (!is_numeric($params[$field])){
					$r->errorCode = 335;
					$r->errorField = $field;
					break;
				}
			}

			if (empty($r->errorCode)){
				if ($params["campaign"]->campaign_cplCampaign->maxLeadLimit > $params["maxLeadLimit"]){
					$r->errorCode = 339;
					$r->errorField = "maxLeadLimit";
				}
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}
	/**
	 * Validate CPL.remote campaign
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function cplRemoteInfo($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array( "maxLeadLimit", "hashCode" );
		self::mandatoryCheck($params,$mFields,$r);
		unset( $mFields[ 2 ] ); //hashcode

		if (empty($r->errorCode)){
			foreach ($mFields as $field){
				if (!is_numeric($params[$field])){
					$r->errorCode = 335;
					$r->errorField = $field;
					break;
				}
			}

			if (empty($r->errorCode)){
				if ($params["campaign"]->campaign_cplRemoteCampaign->maxLeadLimit > $params["maxLeadLimit"]){
					$r->errorCode = 339;
					$r->errorField = "maxLeadLimit";
				}
			}

		}
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}
	/**
	 * Validate CPA.aff campaign
	 *
	 * @param array $params
	 * @param null $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function cpaAffInfo($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array( "commissionInfo", "revPerc", "hashCode" );
		self::mandatoryCheck($params,$mFields,$r);
		unset( $mFields[ 0 ], $mFields[ 2 ] );

		if (empty($r->errorCode)){
			foreach ($mFields as $field){
				if (!is_numeric($params[$field])){
					$r->errorCode = 335;
					$r->errorField = $field;
					break;
				}
			}

			if (empty($r->errorCode)){
				if ($params["revPerc"]<10 || $params["revPerc"]>90){
					$r->errorCode = 343;
					$r->errorField = "revPerc";
				}
			}

		}
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Check the given user id and campaign id is real
	 *
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function campaignPublisher($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("publicID","pid");

		//test purpose
		if($params["pid"] == self::TEST_PUBLISHER)
			return true;

		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			$cp = new CampaignPublisher();
			$cp->load(
				array(
					"campaign_campaignPublisher"=> array( "publicID"=>$params["publicID"]),
					"publisher_campaignPublisher"=>$params["pid"]
				)
			);

			if (!$cp->gotValue || $cp->state == CampaignPublisher::ENUM_STATE_IDLE){
				$r->errorCode = 309;
			}
			unset($cp);
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Check the given user id and campaign id is real
	 * @author Özgür Köy
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function campaignAdvertiser($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("publicID","aid");

		self::mandatoryCheck($params,$mFields,$r);

		if ( empty( $r->errorCode ) ) {
			$cp = new Campaign();
			$cp->load(
				array( "publicID" => $params[ "publicID" ], "advertiser" => $params[ "aid" ] )
			);

			if ( !$cp->gotValue || $cp->state == CampaignPublisher::ENUM_STATE_IDLE ) {
				$r->errorCode = 309;
			}
			unset( $cp );
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Check the given parameters is valid for accept campaign
	 *
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function pubAcceptCampaign($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("campID","pid","sendDate","sendTime","setCustomMessage","messageId");
		$cp = null;

		self::mandatoryCheck($params,$mFields,$r);

		if (empty($params["dateFormatString"])) $params["dateFormatString"] = "Y-m-d";
		if (empty($params["timeFormatString"])) $params["timeFormatString"] = "H:i";

		if (empty($r->errorCode)){
			$cp = new CampaignPublisher();
			$cp->load(array("campaign_campaignPublisher"=> array("publicID"=>$params["campID"]),"publisher_campaignPublisher"=>$params["pid"]));
			if (!$cp->gotValue || $cp->state == CampaignPublisher::ENUM_STATE_IDLE){
				$r->errorCode = 309;
			} elseif ($cp->state != CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION){
				$r->errorCode = 310;
			} else {
				$charLimit = Setting::getValue("campaignCharLimitWithoutLink");
				$cp->loadCampaign();
				if ($cp->campaign_campaignPublisher->race == Campaign::ENUM_RACE_TWITTER){
					$cp->campaign_campaignPublisher->loadCampaignClick();
					if ($cp->campaign_campaignPublisher->campaign_campaignClick->gotValue && parent::imgLink($cp->campaign_campaignPublisher->campaign_campaignClick->link)){
						$charLimit = Setting::getValue("campaignCharLimitWithLink");
					}
					if ($params["setCustomMessage"] == "Y" && empty($params["customMessage"])){
						$r->errorCode = 311;
					} elseif ($params["setCustomMessage"] == "Y" && mb_strlen($params["customMessage"]) > $charLimit){
						$r->errorCode = 312;
					} else {
						if ($params["setCustomMessage"] != "Y"){
							$cp->campaign_campaignPublisher->loadCampaign_campaignMessage(array(
								"id" => $params["messageId"],
								"campaign_campaignMessage" => $cp->campaign_campaignPublisher->id
							));
						}
						if ($params["setCustomMessage"] != "Y" && !$cp->campaign_campaignPublisher->campaign_campaignMessage->gotValue){
							$r->errorCode = 313;
						} elseif (!parent::Date($params["sendDate"],$params["dateFormatString"])){
							$r->errorCode = 314;
						} elseif (!parent::Date($params["sendTime"],$params["timeFormatString"])){
							$r->errorCode = 315;
						} else {
							$tsStartDate = strtotime($cp->campaign_campaignPublisher->startDate . " 00:00:00");
							$tsEndDate = strtotime($cp->campaign_campaignPublisher->endDate . " 23:59:59");
							//$tsSendDate = strtotime($params["sendDate"] . " " . $params["sendTime"]);
							$tsSendDate = date_parse_from_format($params["dateFormatString"],$params["sendDate"]);
							$tsSendDate = strtotime($tsSendDate["year"] . "-" . $tsSendDate["month"] . "-" . $tsSendDate["day"] . " " . $params["sendTime"]);

							if ($tsSendDate > $tsEndDate || $tsSendDate < $tsStartDate){
								$r->errorCode = 316;
							} elseif ($cp->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPC && $tsSendDate < time()){
								$r->errorCode = 317;
							} elseif (date("H",$tsSendDate) < 9 || !in_array(date("i",$tsSendDate),array(0,15,30,45))){
								$r->errorCode = 315;
							} else {
								$cpa = $cp->canUserAccept();
								if ($cpa !== true){
									switch($cpa){
										case CampaignPublisher::ENUM_STATE_LATE_TO_JOIN:
											$r->errorCode = 318;
											break;
										case CampaignPublisher::ENUM_STATE_BUDGET_ENDED:
											$r->errorCode = 319;
											break;
										default:
											$r->errorCode = 320;
											break;
									}
								}
							}
						}
					}
				}
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		} else {
			$r->returnParams = array("cp"=>$cp);
		}

		return self::response($r);
	}

	/**
	 * Check the given parameters is valid for accept campaign
	 *
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function pubDenyCampaign($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("campID","reasonId");
		$cp = null;
		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			$cp = new CampaignPublisher();
			$cp->load(array("campaign_campaignPublisher"=> array("publicID"=>$params["campID"]),"publisher_campaignPublisher"=>$params["pid"]));
			if (!$cp->gotValue || $cp->state == CampaignPublisher::ENUM_STATE_IDLE){
				$r->errorCode = 309;
			} elseif ($cp->state != CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION){
				$r->errorCode = 310;
			} else {
				$cpa = $cp->canUserAccept();
				if ($cpa !== true){
					switch($cpa){
						case CampaignPublisher::ENUM_STATE_LATE_TO_JOIN:
							$r->errorCode = 318;
							break;
						case CampaignPublisher::ENUM_STATE_BUDGET_ENDED:
							$r->errorCode = 319;
							break;
						default:
							$r->errorCode = 320;
							break;
					}
				}
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		} else {
			$r->returnParams = array("cp"=>$cp);
		}

		return self::response($r);
	}


	/**
	 * Check the given parameters is valid for campaign list
	 *
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function pubCampList($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("orderBy","page");
		$cp = null;
		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			if (!in_array($params["orderBy"],self::$validOrderBys)){
				$r->errorCode = 322;
			} elseif (!is_numeric($params["page"]) || intval($params["page"]) < 0){
				$r->errorCode = 323;
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		} else {
			$r->returnParams = array("cp"=>$cp);
		}

		return self::response($r);
	}

	/**
	 * Check the given parameters is valid for campaign list
	 *
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function pubCamps($params, &$r = null){
		$r = new ValidationResponse();
		$mFields = array("campStatus","orderBy","page");
		$cp = null;
		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			if (!in_array($params["campStatus"],self::$validCampListTypes)){
				$r->errorCode = 321;
			} elseif (!in_array($params["orderBy"],self::$validOrderBys)){
				$r->errorCode = 322;
			} elseif (!is_numeric($params["page"]) || intval($params["page"]) < 0){
				$r->errorCode = 323;
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		} else {
			$r->returnParams = array("cp"=>$cp);
		}

		return self::response($r);
	}

	/**
	 * Validate Change Campaign status Request (Temporary for swagger)
	 *
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */
	public static function advChangeCampaignStatus($params,&$r = null){
		$r = new ValidationResponse();
		$mFields = array("publicID","status");
		$c = null;
		self::mandatoryCheck($params,$mFields,$r);

		if (empty($r->errorCode)){
			$c = new Campaign();
			$c->load(array(
				"publicID" => $params["publicID"]
			));
			if (!$c->gotValue){
				$r->errorCode = 309;
			} elseif(!in_array($params["status"],array(Campaign::ENUM_STATE_PENDING_PROGRESS,Campaign::ENUM_STATE_PROGRESSING,Campaign::ENUM_STATE_FINISHED))) {
				$r->errorCode = 324;
			} elseif ($c->state > $params["status"]) {
				$r->errorCode = 325;
			} else {

			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		} else {
			$r->returnParams = array("c"=>$c);
		}

		return self::response($r);
	}

	/**
	 * Validate the publish date options
	 *
	 * @param array $params
	 * @param null|ValidationResponse $r
	 *
	 * @return bool|ValidationResponse
	 */public static function publishCampaign($params,&$r = null){
	$r = new ValidationResponse();
	$mFields = array("id","publishOption","publishDate","publishTime");
	$c = null;
	self::mandatoryCheck($params,$mFields,$r);

	if (empty($r->errorCode)){
		$c = new Campaign($params["id"]);

		if (!$c->gotValue){
			$r->errorCode = 309;
		} elseif ($c->state != Campaign::ENUM_STATE_OFFER_APPROVED){
			$r->errorCode = 338;
		} else {
			$startDate = strtotime($c->startDate . " 00:00:00");
			$endDate = strtotime($c->endDate . " 23:59:59");
			$publishDate = strtotime($_POST["publishDate"] . " " . $_POST["publishTime"]);
			if ($publishDate < (time()-100) || $publishDate > $endDate){
				$r->errorCode = 336;
			} elseif ($c->race == Campaign::ENUM_RACE_TWITTER && $publishDate >= $startDate){
				$r->errorCode = 337;
			}
		}
	}

	if (!empty($r->errorCode)){
		$r->valid = false;
		if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
	} else {
		$r->returnParams = array("c"=>$c);
	}

	return self::response($r);
}
}
