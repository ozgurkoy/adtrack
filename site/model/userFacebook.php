<?php
/**
 * userFacebook real class - firstly generated on 15-07-2013 12:29, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/userFacebook.php';
require_once $__DP.'/site/service/facebook/facebook.php';

class UserFacebook extends UserFacebook_base
{
    
        private $config = null;
        private $FacebookObj;
    
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
            
            if(is_null($this->FacebookObj))
                $this->FacebookObj = new Facebook($this->FacebookConfig());	
            
            parent::__construct($id);    
	}
        
        
 
        
        public function FacebookConfig() {

            if(is_null($this->config))
            {
                $this->config = array(
                    'appId' => FB_APP_ID,
                    'secret' => FB_APP_SECRET,
                    'fileUpload' => false);
            }
            
            JLog::log("fb", "Get FacebookConfig - ".  print_r($this->config,true)); 
            return $this->config;
        }
        
        public function GetAccesTokenForUser($code) {

            JLog::log("fb", "GetAccesTokenForUser Code - ".$code); 
            if(isset($code)){
                $facebook = $this->FacebookObj;
                $user_id = $facebook->getUser();
                
                if(!$facebook->getAccessToken())
                {
                    //$my_url = $GLBS["constants"]["siteAddress"];
	                $my_url = JUtil::siteAddress();

                    $token_url = "https://graph.facebook.com/oauth/access_token?"
                    . "client_id=" . self::$config['appId'] . "&redirect_uri=" . urlencode($my_url)
                    . "&client_secret=" . self::$config['secret'] . "&code=" . $code;

                    $response = file_get_contents($token_url);
                    $params = null;
                    parse_str($response, $params);
                    $new_access_token = $params['access_token'];


                    $facebook->setAccessToken($new_access_token);    
                }
                
                $this->facebookID = $user_id;
                $this->load();

                $this->AccessToken = $facebook->getAccessToken();
                $this->save();
            }
            
            return $this;
        }
        
        /*
         * Create sesstion, connect to user
         * Access from login
         */
        private function signin($redirectPage) {
		global $__DP,$_S;
                unset($_SESSION["FBredirectPage"]);
                
                JLog::log("fb", "Facebook sign In Redirect - ".$redirectPage);
                // We Have User ID The Access token
                
                // Invite 
                // 1. sign up - 
                // 2. Sign IN
                    // No User
                    // Has User

                // Invite Mode Handle
                if($redirectPage == "invite")
                {
                    
                    $this->updateInfo();
                    $this->user = null;
                    $this->save();
                    
                    $invite = new Invite();
                    $invite->AddFacebookUser($this->facebookID, $this->AccessToken);
                    $_SESSION['facebookStatus'] = "added";
                    
                    
                    //$_SESSION['facebookEmail']
                    
                    header("Location: ".$_S."/".$redirectPage); 
                    exit();
                }
                
                
                //Check if ther is loged in with adMingleUserID
                if(isset($_SESSION["adId"]))
                {
                    // From Sign up step 3 or update profile
                    
                    if(is_null($this->user))
                    {
                        // Refresh Facebook Date
                        $this->updateInfo();

                        // Fill Session With Facebook Data
                        $this->fillSession();

                        // Has adMingle User - sign up / update profile
                        $this->user = $_SESSION["adId"]; 
                        $this->save();
                        $this->loadUser();
                        $this->user->hasFacebook = 1;
                        $this->user->save();
                        $this->user->fillSession();
                        $this->user->actLang();

                    }
                    else
                    {
                        if($_SESSION["adId"] != $this->user)
                        {
                            Tool::setLastError("user","FacebookAccountUserByAnotherUser","userFacebook->signin");
                        }
                        else {
                            $this->loadUser();
                            $this->user->fillSession();
                            $this->user->actLang();
                        }
                    }
                    
                    header("Location: ".$_S."/".$redirectPage); 
                    exit();
                }
                else
                {
                    // Main Page / Login / Sign up
                    
                    // Refresh Facebook Date
                    $this->updateInfo();

                    // Fill Session With Facebook Data
                    $this->fillSession();
                    
                    
                    // Hasn't adMingl User in session
                    // Check if ther is any adMIngle account
                    if($this->user)
                    {
                        // Has admingle account
                        $this->loadUser();
                        $this->user->fillSession();
                        $this->user->actLang();
                        header("Location: ".$_S."/".$redirectPage); 
                    }
                    else 
                    {
                        // No user, Go to SignUp Page
                        header("Location: ".$_S."/"."sign-up-pub"); 
                    }
                    exit();
                    
                }
	}

	/**
	 * archive the current and update current facebook Basic public info
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function updateInfo()
	{
            $facebook = $this->FacebookObj;
            
            $user_profile = $facebook->api('/me','GET');
            //die( "<pre>" . print_r($user_profile,true). "</pre>");;
            
            JLog::log("fb", print_r($user_profile,true));
            
            
            $this->username = isset($user_profile["username"])?$user_profile["username"]:null;
            $this->profileLink = $user_profile["link"];
            $this->name = $user_profile["name"];
            $this->location = isset($user_profile["location"])?$user_profile["location"]["name"]:null;
            $this->gender = $user_profile["gender"];
            $this->locale = $user_profile["locale"];
            $this->verified = isset($user_profile["verified"])?$user_profile["verified"]:false;
            $this->updated_time = $user_profile["updated_time"];
            
            $this->save();
            
            
            //echo "<pre>" ;
            //print_r($user_profile);
            //echo "</pre>";
	}
        
    public function getUserEmail()
	{
            $facebook = $this->FacebookObj;
            
            $user_profile = $facebook->api('/me?fields=email');
            
            return isset($user_profile["email"])?$user_profile["email"]:null;
            
	}

		/***
         * 
         * redirect_uri: the url to go to after a successful login
         * scope: comma separated list of requested extended perms
         */
        public function getLoginUrl($scope=NULL,$redirect_uri=NULL) {
            
            global $_S;
            // redirect_uri: the url to go to after a successful login
            // scope: comma separated list of requested extended perms

            if(!$redirect_uri)
            {
                $redirect_uri = $_S.'/site/service/facebook/fbCB.php';
            }
            if(!$scope)
            {
                $scope = 'email,user_about_me,user_birthday,user_hometown,user_likes,user_location,read_stream';
            }
            
            
           $loginParams = array("redirect_uri"=> $redirect_uri,"scope"=> $scope);
           $url =$this->FacebookObj->getLoginUrl($loginParams);
           
           JLog::log("fb", "login URL:". $url);
           header("Location: ".$url); 
           exit();
                       

        }
        
        
        public function logout() {
            $this->FacebookObj->clearAllPersistentData();
        }
        
        public function loginError($error,$code) {
		global $__S;
                $redirectPage = "";
                if(isset($_SESSION["fb_redirectPage"]))
                {
                    $redirectPage = $_SESSION["fb_redirectPage"];
                }
                
                JLog::log("fb", "Facebook  Login Error Code: ".$code ." Error: ".$error."Redirect - ".$redirectPage);
                $_SESSION['facebookStatus'] = "error";
        
                header("Location: ".$_S."/".$redirectPage); 
                exit();
        }
        
        /**
	 * login by username etc.
	 * first - First time doing login
	 * @return mixed
	 * @author Sam Ben Yakar
	 **/
	public function login($redirectPage="dashboard",$first=false) {
		global $__DP;
                //$this->FacebookObj->clearAllPersistentData();
                JLog::log("fb", "Facebook  Login Reditect - ".$redirectPage);
                
                if(isset($_SESSION["fb_redirectPage"]))
                {
                    $redirectPage = $_SESSION["fb_redirectPage"];
                }else{
                    $_SESSION["fb_redirectPage"] = $redirectPage;
                }
                
                if($first)
                    return $this->getLoginUrl();
                
                $facebook = $this->FacebookObj;
                $user_id = $facebook->getUser();
                JLog::log("fb", "Facebook  Login user_id - ".$user_id);

                if($user_id != '0') {

                    // We have a user ID, so the user has open session in facebook
                    try {

                      // Checking if we have valid token   

                      $this->facebookID = $user_id;
                      $this->load();
                      
                      if($this->count && !is_null($this->user))
                      {
                          // User Has adMingle account And token
                                                    
                          $app_access_token = $this->getAppAccessToken();
                          
                          //input_token: the access token you want to get information about
                          //access_token: your [app access token][10] or a valid user access tœoken from a developer of the app.
                          $tokenReq = "https://graph.facebook.com/debug_token?input_token=".$this->AccessToken."&access_token=".$app_access_token;
                          
                          //$loginParams = array("input_token"=> $this->AccessToken,"access_token"=> $AdminToken,"method"=> "GET");
                          
                          
                          JLog::log("fb", 'Create tokenCheck - '.$tokenReq); 
                          //$tokenResponse = $facebook->api($tokenReq,"GET");
                          //$tokenResponse = json_encode($tokenResponse);
                          $tokenResponse = null;
                           $response = file_get_contents($tokenReq);
                           $tokenResponse = json_decode($response);

                           //parse_str($response, $tokenResponse);
                           //$new_access_token = $params['access_token'];

                          
                          
                          JLog::log("fb", 'tokenResponse'.print_r($tokenResponse,true)); 
                          //print_r($tokenResponse);
                          
                          
                          
                          if($tokenResponse->data->is_valid)
                          {
                              $facebook->setAccessToken($this->AccessToken);
                              $this->TokenValidDate = $tokenResponse->data->expires_at;
                              $this->save();
                              $this->signin($redirectPage);       
                          }
                          else
                          {
                          
                              // Token not Valid
                              return $this->getLoginUrl();
                          }

                          
                      }else 
                          {
                          // No User Record 
                          
                            //Check for token in the sseion
                            if($facebook->getAccessToken() == "")
                            {
                                // Get new Token
                                return $this->getLoginUrl();
                            }

                          // Has Token
                          // Create new Facebook User
                          // Add 60 Days to Valid Token 
                          $this->TokenValidDate = time()+(60*60*24*60);
                          $this->save();
                          $this->signin($redirectPage);
                          
                      }

                    } catch(FacebookApiException $e) {
                      // If the user is logged out, you can have a 
                      // user ID even though the access token is invalid.
                      // In this case, we'll get an exception, so we'll
                      // just ask the user to login again here.
                      error_log($e->getType());
                      JLog::log("fb", 'error - '.$e->getMessage()); 
                      print_r($e);
                      exit();
//return $this->getLoginUrl();
                    }   
                }
                else
                {                    
                    // If User is not valid or not register 
                    // it will redirect to Facebook login

                    return $this->getLoginUrl();
                }
	}
        /**
	 * Fills session variables of Facebook
	 */
	public function fillSession() {
		
		$_SESSION['fbid'] 		= $this->facebookID;
		$_SESSION['fbimg'] 		= $this->GetProfileimg();
		$_SESSION['fbUsername'] 	= is_null($this->username)?$this->username:$this->name;
		$_SESSION['fbNameSurname']     = $this->name;
	}
        
        public function GetProfileimg($type=null,$width=null,$height=null) {
		//?width=40&height=60
                //?type=square small normal large
           

            $url = "https://graph.facebook.com/".$this->facebookID."/picture?";
            
            if(!is_null($type))
                $url .= "&type=".$type;
            
            
            if(!is_null($width))
                $url .= "&width=".$width;
            
            
            if(!is_null($height))
                $url .= "&height=".$height;
            
            return $url;

	}
        
        /*
         * Get all post Data 
         * return json
         */
        public function GetPost($postID) {
		
            $facebook = $this->FacebookObj;
            
            $user_post = $facebook->api('/'.$postID);
            
            return $user_post;

	}
        
        protected function getAppAccessToken()
        {
            $config = $this->FacebookConfig();
            
            $token_url = "https://graph.facebook.com/oauth/access_token?"
            . "client_id=" . $config['appId'] . "&grant_type=client_credentials"
            . "&client_secret=" . $config['secret'];

            $response = file_get_contents($token_url);
            $params = null;
            parse_str($response, $params);
            $new_access_token = $params['access_token'];
            return $new_access_token;
        }
        

}

