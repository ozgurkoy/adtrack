<?php
/**
 * spamMail real class - firstly generated on 11-07-2012 15:02, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/spamMail.php';

class SpamMail extends SpamMail_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Send the User a Notification to board about the spam message
	 * @param User $user
	 */
	public function informUserSpamMail( User &$user ) {
		// Check if user language is loaded
		if ( !is_object( $user->lang ) )
			$user->loadLang();

		$lang = $user->lang;

		// Load the Messages
		$informMessage = LangMessage::getLangMessages( $lang->langShortDef, "spamMailInformation" );

		$informTitle = $informMessage[ "title" ];
		$userName    = "";

		// Get user name to display on the message
		if ( $user->utype == User::ENUM_UTYPE_PUBLISHER ) {
			if ( !is_object( $user->user_publisher ) )
				$user->loadPublisher();
			$userName = $user->user_publisher->getFullName();
		}
		elseif ( $user->utype == User::ENUM_UTYPE_ADVERTISER ) {
			if ( !is_object( $user->user_advertiser ) )
				$user->loadAdvertiser();

			$userName = $user->user_advertiser->companyName;
		}

		// generate the message
		$messageVariables = array( "user" => $userName );

		// Saving copy in message template
		$msgTemp = TemplateMessage::saveTempalateMessage( $informMessage[ "message" ], $informMessage[ "title" ] );
		UserMessage::sendUserMessage( $user->id, $msgTemp, $spamMsgCode, UserMessage::ENUM_MSGTYPE_SYSTEM, $messageVariables );

		// save that user informed
		$this->informed = 1;
		$this->save();
		unset( $h, $informMessage, $informTitle, $informMes, $_sysMessages, $messageVariables, $msgTemp );

	}

}

