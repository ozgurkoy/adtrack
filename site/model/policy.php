<?php
/**
 * policy real class - firstly generated on 16-02-2014 14:05, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/policy.php';

class Policy extends Policy_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * @param $name
	 * @param $langShortDef
	 * @return string
	 * @author Özgür Köy
	 */
	public static function getPolicy( $name, $langShortDef ) {
		$p = new Policy();
		$p->limit( 0, 1 )->orderBy( "jdate DESC" )->load( array( "policyUrlName" => $name, "policyLanguage" => array( "langShortDef" => $langShortDef ) ) );
		$c = ($p->gotValue?$p->content:"");

		unset( $p );

		return $c;
	}

	public static $getDefaultPolicyTypes = array("publisheragrement"=>"Publisher Agreement",
												 "advertiseragrement"=>"Advertiser Agreement",
												 "policy"=>"site Policy",
												 "privacy"=>"site Policy",
												 "termsofservice"=>"Terms Of Service",
												 "cookiepolicy"=>"Cookie Policy");



}

