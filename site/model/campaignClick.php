<?php
/**
 * campaignClick real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignClick.php';

class CampaignClick extends CampaignClick_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Get campaign referrer stats
	 *
	 * @return array|null
	 * @author Murat
	 */
	public function getReferrerStats(){
		if (!is_object($this->campaign_campaignClick)) $this->loadCampaign();
		if ($this->campaign_campaignClick->checkForSLS()){
			if ($this->campaign_campaignClick->id <= Setting::getValue("MAX_CAMP_ID_BEFORE_CPC")){
				return $this->campaign_campaignClick->getOldCampaignReferrersByDomain();
			} else {
				$data = null;

				$data = Tool::callSLService("referrerStats",array(
					"params"=>  json_encode(array(
						"campaignHash"=>$this->linkHash
					))));

				if (!JUtil::isProduction()){
					$params = JUtil::getSearchParameter();
					if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
						Tool::echoLog("referrerStats : " . print_r($data,true));
					}
				}

				$domains = Format::parseReferrer($data);

				if (!JUtil::isProduction()){
					$params = JUtil::getSearchParameter();
					if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
						Tool::echoLog("referrerStats processed : " . print_r($domains,true));
					}
				}

				return $domains;
			}
		} else {
			return null;
		}
	}

	/**
	 * Update campaign link click stats
	 *
	 * @return void
	 * @author Murat
	 */
	public function updateCampaignLinkStats(){
		if (!is_object($this->campaign_campaignClick)) $this->loadCampaign();
		if ($this->campaign_campaignClick->checkForSLS()){
			$data = null;

			$data = Tool::callSLService("campaignStats",array(
				"params"=>  json_encode(array(
					"campaignHash"=>$this->linkHash,
					"lastUpdate"=>null
				))
			));

			if (!JUtil::isProduction()){
				$params = JUtil::getSearchParameter();
				if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
					Tool::echoLog("campaignStats : " . print_r($data,true));
				}
			}

			CampaignClickStat::bulkAddUpdateCCS($data);
			$this->load($this->id); //Load after click update, because we update the click stats with bulk update sql
		}
	}

	/**
	 * get campaign link stats
	 *
	 * @return array
	 * @author Murat
	 */
	public function getCampaignLinkStats(){
		$data = array(
			"clickDates" => array(),
			"calculatedClick" => array(),
			"suspectedClick" => array(),
			"totalClick" => array(),
			"totalClickSum" => 0,
			"calculatedClickSum" => 0,
			"suspectedClickSum" => 0
		);

		if (!is_object($this->campaign_campaignClick)) $this->loadCampaign();
		if ($this->campaign_campaignClick->checkForSLS()){
			if ($this->id <= Setting::getValue("MAX_CAMP_ID_BEFORE_CPC")){
				$ccount = $this->campaign_campaignClick->getOldCampaignClicks();
				$clickCounts = array();
				$clickDates = array();
				$tempArray = array();
				$totClick = 0;

				foreach ($ccount as $ck => $cc) {
					$clickDates[] = $ck;
					$clickCounts[] = ($cc);
					$totClick += $cc;
				}
				foreach ($clickDates as $key => $clickDate) {
					$tempArray[$key] = date('d.m.Y', strtotime($clickDate));
				}

				if (!count($clickCounts)){
					$clickCounts = array(0);
					$tempArray = array(0);
				}
				$data = array(
					"clickDates" => $tempArray,
					"totalClick" => $clickCounts,
					"totalClickSum" => $totClick
				);

			} else {
				$this->updateCampaignLinkStats();
				$ccs = new CampaignClickStat();
				$ccs->load(array("linkHash" => $this->linkHash));
				if ($ccs->gotValue){
					$dates = array();
					do{
						$data["clickDates"][] = date("Y-m-d",$ccs->clickDate);
						$data["calculatedClick"][] = $ccs->calculatedClick;
						$data["suspectedClick"][] = $ccs->suspectedClick;
						$data["totalClick"][] = $ccs->totalClick;
						$data["totalClickSum"] += $ccs->totalClick;
						$data["calculatedClickSum"] += $ccs->calculatedClick;
						$data["suspectedClickSum"] += $ccs->suspectedClick;
					} while($ccs->populate());
				}
			}
		}
		return $data;
	}

	/**
	 * Get campaign click stats for per country
	 *
	 * @return array
	 * @author Murat
	 */
	public function getCountryStats(){

		$result = array();

		if (!is_object($this->campaign_campaignClick)) $this->loadCampaign();
		if ($this->campaign_campaignClick->checkForSLS()){
			if ($this->id <= Setting::getValue("MAX_CAMP_ID_BEFORE_CPC")){
				$result = $this->campaign_campaignClick->getOldCampaignCountryStats();
			} else {
				$data = null;
				$data = Tool::callSLService("countryStats",array(
					"params"=>  json_encode(array(
						"campaignHash"=>$this->linkHash
					))));

				if (!JUtil::isProduction()){
					$params = JUtil::getSearchParameter();
					if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
						Tool::echoLog("countryStats : " . print_r($data,true));
					}
				}

				$result = array();

				if (is_array($data) && count($data)){
					foreach ($data as $item){
						$result[$item->_id] = $item->count;
					}
				}

				if(!count($result)) {
					$result[Setting::getValue("DEFAULT_LANGUAGE")] = 1;
				}
				return $result;
			}
		}

		return $result;
	}

	/**
	 * Get campaign referrer details
	 *
	 * @return array
	 * @author Murat
	 */
	public function getReferrerDetails($min=null,$limit=null,$domain="t.co"){
		if (is_null($min)) $min = Setting::getValue("REFERRER_FRAUD_MIN_LIMIT");
		if (strlen($min)<1) $min = 1;
		$data = Tool::callSLService("detailedReferrerStats",array("params"=>  json_encode(array(
			"campaignHash" => $this->linkHash,
			"domain" => $domain,
			"min" => intval($min),
			"limit" => is_null($limit) ? 10000000 : intval($limit)
		))));

		if (!JUtil::isProduction()){
			$params = JUtil::getSearchParameter();
			if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
				Tool::echoLog("detailedReferrerStats - fraudAvoidSystem : " . print_r($data,true));
			}
		}

		return $data;
	}

	/**
	 * Get Campaign Blocked Referrers
	 *
	 * @return array
	 * @author Murat
	 */
	public function getBlockedReferrers(){
		//Get blocked referrers
		$data = Tool::callSLService("getCampaignCursed",array("params"=>  json_encode(array(
			"campaignHash" => $this->linkHash,
		))));

		if (!JUtil::isProduction()){
			$params = JUtil::getSearchParameter();
			if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
				Tool::echoLog("getCampaignCursed: " . print_r($data,true));
			}
		}

		$blockedReferrers = array();
		if (is_array($data) && count($data)){
			foreach($data as $r){
				$blockedReferrers[$r->ref] = $r->cleared;
			}
		}

		unset($data);

		return $blockedReferrers;
	}

	/**
	 * Get available short link domains
	 *
	 * @return array
	 * @author Murat
	 */
	public static function getSLDomains(){
		$SLDomains = Setting::getValue("SHORTDOMAIN_OPTIONS");
		if (is_null($SLDomains)) $SLDomains = json_encode(array(Setting::getValue("SHORTDOMAIN")));
		return json_decode($SLDomains,true);
	}

	/**
	 * Create copy of preview image for campaign copy
	 *
	 * @return array
	 * @author Murat
	 */
	public function copyPreviewImage(){
		global $__DP;
		$uploadPath = "site/layout/images/campaign/";

		// image name
		$name = "_temp_" . JUtil::randomString(20) . "." . substr($this->previewFileName,-3);

		// location to save cropped image
		$url = $__DP.$uploadPath.$name;

		copy($uploadPath.$this->previewFileName,$url);

		$filesize = filesize( $url );

		//$fileName = $name . substr($url,-3);
		return array(
			"fullPath" => $url,
			"size"=>$filesize,
			"fileName" => $name
		);
	}

}

