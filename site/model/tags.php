<?php
/**
 * tags real class - firstly generated on 26-10-2014 20:29, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/tags.php';

class Tags extends Tags_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}
	public static function getAllTags() {
		$t = new Tags();
		$t->orderBy( "name ASC" )->populateOnce(false)->load();
		$ts = array();

		if($t->gotValue){
			do {
				$ts[ ] = $t->name;
			} while ( $t->populate() );

		}

		unset( $t );
		return $ts;
	}


	public static function parseTagsFromParameter( &$tagg ) {
		$tagg = preg_replace( "/,\\s+$/ui", "", $tagg );
		$tagg = preg_replace( "/,(\\s+)?/uism", "|", $tagg );
		$tagg = strtolower( $tagg );

		$tagg = explode( "|", $tagg );
		array_walk( $tagg, array( "JUtil", "quoteString" ) );
		$notExisting = (array)JPDO::executeQuery(
			"SELECT * FROM (SELECT unnest(ARRAY[" . ( implode( ",", $tagg ) ) . "]) as name) AS E
						WHERE E.name not IN

						(
						select name from \"devV15_main\".\"tags\"
						)"
		);

		//add not existing tags.
		foreach ( $notExisting as $not ) {
			if(empty($not[ "name" ]))
				continue;
			$t       = new Tags();
			$t->name = $not[ "name" ];
			$t->save();
		}

		array_walk( $tagg, array( "JUtil", "stripQuotes" ) );

	}
}

