<?php
/**
 * campaignBlockedPublisher real class - firstly generated on 09-01-2014 14:07, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignBlockedPublisher.php';

class CampaignBlockedPublisher extends CampaignBlockedPublisher_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Get a campaign all blocked publishers
	 *
	 * @param int $campaignId
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public static function getList($campaignId){
		$cbp = new CampaignBlockedPublisher();
		$cbp->with(
			array("campaignCPCMessage_campaignBlockedPublisher", CampaignBlockedPublisher::DO_LEFT_JOIN)
		);
		$cbp->leftJoin("campaign_campaignCPCMessage")->load(array(
			"campaignCPCMessage_campaignBlockedPublisher" => array(
				"campaign_campaignCPCMessage" => $campaignId
			)
		));

		$data = array();

		if ($cbp->gotValue){
			do {
				$cbp->campaignCPCMessage_campaignBlockedPublisher->loadPublisher_campaignCPCMessage()->loadUser();
				$data[] = array(
					"publisherId" => $cbp->campaignCPCMessage_campaignBlockedPublisher->publisher_campaignCPCMessage->id,
					"username" => $cbp->campaignCPCMessage_campaignBlockedPublisher->publisher_campaignCPCMessage->user_publisher->username,
					"linkHash" => $cbp->campaignCPCMessage_campaignBlockedPublisher->linkHash,
					"killHistory" => $cbp->killHistory,
					"count" => $cbp->count,
					"date" => $cbp->jdate
				);
			} while($cbp->populate());
		}
		unset($cbp);
		return $data;
	}
}

