<?php
/**
 * menu real class - firstly generated on 19-11-2013 16:16, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/DSmenu.php';

class DSmenu extends DSmenu_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function findMaxOrder( $topMenu){
		$m = new DSmenu();
		$m->max("morder","maxOrder",array("_custom"=>$topMenu>0?"`topMenu`=$topMenu":"`topMenu` IS NULL")); #is numeric for root check
		$ret = isset($m->maxOrder)? (int)$m->maxOrder+1:1;
		unset( $m );

		return $ret;
	}

	public function moveUpTheMenu(){
		$topMenu = is_object( $this->topMenu ) ? $this->topMenu->id : $this->topMenu;

		$m = new DSmenu();
		$m->limit(0,1)->orderBy("morder DESC")->load( array("_custom"=>is_numeric($topMenu)?"`topMenu`=$topMenu":"`topMenu` IS NULL", "id"=>"NOT IN {$this->id}", "morder"=>"&le {$this->morder}")); #is numeric for root check
		if ( $m->gotValue ) {
			$t0           = $m->morder;
			if($t0 == $this->morder){ # something might be off..
				$this->morder = DSmenu::findMaxOrder( $topMenu );
			}
			else{
				$m->morder    = $this->morder;
				$this->morder = $t0;

				$m->save();
			}
			$this->save();
		}
		unset( $m );

	}

	public function listRoles() {
		$this->loadMenuRoles();
		$cats = array();

		if($this->menuRoles && $this->menuRoles->gotValue){
			do {
				$cats[ $this->menuRoles->id ] = $this->menuRoles->label;
			} while ( $this->menuRoles->populate() );

		}

		return $cats;
	}

	public static function menuList( $parent = null, $level = 0, $nid = null ) {
		$menus = array();
		$m     = new DSmenu();
		$m->nopop()->orderBy( "morder ASC" )->load( array( "_custom" => "`topMenu` " . ( is_null( $parent ) ? "IS NULL" : "=$parent" ) . ( is_null( $nid ) ? "" : " AND id<>" . $nid ) ) );

		$first = true;
		while ( $m->populate() ) {
//			continue;
			$roles           = $m->listRoles();
			$menus[ $m->id ] = array(
				"subMenus" => explode(",",$m->subMenus),
				"link"     => $m->link,
				"css"      => $m->css,
				"label"    => $m->label,
				"level"    => $level,
				"parent"   => $m->topMenu,
				"order"    => $m->morder,
				"first"    => $first,
				"isActive" => $m->isActive,
				"roles"    => implode( ",", array_values( $roles ) ),
				"roleIds"  => array_keys( $roles )
			);

			$first = false;
			$t0    = (array)self::menuList( $m->id, $level + 1, $nid );

			$menus = $menus + $t0;
		}
		unset( $m );

		return $menus;
	}
}

