<?php
/**
 * lang real class for Jeelet - firstly generated on 01-02-2011 18:58, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/lang.php';
require_once $__DP.'/site/lib/RestRequest.php';

class Lang extends Lang_base
{
	const WITH_FLAGS = 1;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null)
	{
		parent::__construct($id);
	}

	public  $_wordsCount;

	public function  calcWordsCount()
	{
		$this->_wordsCount = "100";
	}

	/**
	 * load by shortname
	 *
	 * @return mixed
	 **/
	public function loadByShort($short=null)
	{
		$this->load(array("langShort"=>$short));
	}


	/**
	 * @param null $short
	 * @return bool|JModel
	 * @author Özgür Köy
	 */
	public function loadByShortDef($short=null)
	{
		return $this->load(array("langShortDef"=>$short));
	}


	/**
	 * switch lang
	 *
	 * @return bool
	 **/
	public static function switchLang($ls)
	{
		if(is_null($ls) || strlen($ls)<1) return;

		$l = new Lang();
		$l->loadByShort($ls);
		// print_r($_SERVER);
		if($l->id>0){
			$_SESSION['lang'] = $ls;
			unset($l);
			return true;
		}
		return false;
	}

	/**
	 * @return Lang|null
	 * @author Özgür Köy
	 */
	public static function getDefaultLanguage()
	{
		$l = new Lang();
		$l->loadByShortDef(Setting::getValue("DEFAULT_LANGUAGE"));

		if($l->id>0){
			return $l;
		}

		return null;
	}

	/*
	 * Count the number of active languages for language switcher
	 * @author Sam
	 * @return int
	 */
	public static function activeCount()
	{
		$l = new Lang();
		$l->count("id","lCount",array("active"=>1));

		$c= $l->lCount;
		unset($l);

		return $c;
	}

	/*
	 * Create array of the active Languages with display name and css value
	 * @author Sam
	 * @return array
	 */
	public static function activeArray()
	{
		$l = new Lang();
		$l->load(array("active"=>1));

		$larr = array();

		do{
			$fields = array();
			$fields["display"] = $l->langName;
			$fields["css"] = $l->flag;
			$larr[$l->langShortDef] = $fields;

		}while($l->populate());

		return $larr;
	}

	public static function loadAll( $withFlags = false ) {
		$l = new Lang();

		$l->populateOnce( false )->orderBy( "langName" )->load( array( "active" => 1 ) );
		$thiss = array();

		if ( sizeof( $l->_ids ) > 0 ) {
			do {
				$thiss[ $l->langShortDef ] = $withFlags == Lang::WITH_FLAGS ? $l->flag : ( $l->langName . " " . $l->langShortDef );
			} while ( $l->populate() );
		}

		unset( $l );

		return $thiss;
	}


	/**
	 * Get changed LangWords from Language System
	 *
	 * $send lang to get full lang
	 * @return string
	 * @author Sam
	 * @author Ozgur - modified
	 */
	public static function getSyncWithCore( $lang ) {
		global $__DP;

		//$lastUpdate =time();
		$lastUpdate = 1;
		$langs      = array();
		$langsID    = array();

		$l = new Lang();
		if ( $lang == 0 ) {
			$l->load( array( "active" => 1 ) );
		}
		else {
			$l->load( array( "active" => 1, "id" => $lang ) );
		}

		//flush jcache
		JCache::flush();

		//flush smarty cache
		JT::init();
		JT::clearAllCache();

		if ( $l->gotValue ) {
			if ( $lang == 0 ) {
				do {
					$langs[ ]                 = $l->langShortDef;
					$langsID[ $l->langShortDef ] = $l->id;

					// Get the last update date
					$lastUpdate = $lastUpdate == 1 || $lastUpdate > $l->lastUpdate ? $l->lastUpdate : $lastUpdate;
				} while ( $l->populate() );

				unset( $lw, $l );
			}
			else {
				$langs[ ]                 = $l->langShortDef;
				$langsID[ $l->langShortDef ] = $l->id;
			}

			//print_r(array("data"=>json_encode(array("time"=>$lastUpdate,"langs"=>$langs,"isProduction"=>JUtil::isProduction()))));exit;

			$request = new cRestRequest(
				Setting::getValue( "LANGMAN_API_URL" ),
				"POST",
				array(
					"data" => json_encode(
						array(
							"time" => $lastUpdate,
						    "langs" => $langs,
						    "isProduction" => JUtil::isProduction() ) )
					),
				"json"
			);
			$request->execute();

			$data = json_decode( $request->responseBody );
			if ( empty( $data ) || !is_array( $data->entry ) ) {
				return json_encode( array( "error" => 1, "data" => $request->responseBody ) );
			}
			else {
				$r = self::parseCoreResult( $data, $langsID );

				if ( $r[ "wordsCount" ] > 0 ) {
					// TODO: Reset cache
				}

				$t = array( "error" => 0, "wordsCount" => $r[ "wordsCount" ] );
				//$t = array_merge( $t, $r );

				return json_encode( $t );
			}
		}
		else {
			return json_encode( array( "error" => 1, "data" => "No Language Selected" ) );
		}

	}

	public static function parseCoreResult($data,$langs){
		$cnt = 0;
		$log = array();
		$lastUpdate = array();

		if ( count( $data->entry ) ) {
			JPDO::beginTransaction();
			foreach ( $data->entry as $entry ) {
				$wordCode = null;
				$wordText = null;
				if ( !isset( $lastUpdate[ $entry->languageDef ] ) ) {
					$lastUpdate[ $entry->languageDef ] = $entry->lastUpdate;
				}

				$lastUpdate[ $entry->languageDef ] = $lastUpdate[ $entry->languageDef ] > $entry->lastUpdate ? $lastUpdate[ $entry->languageDef ] : $entry->lastUpdate;
				if ( $entry->type == "mailMessage" ) {
					$msg = new LangMessage();
					$msg->load( array( "messageCode" => $entry->name, "langShortDef" => $entry->languageDef ) );
					$msg->lang_langMessage = $langs[ $entry->languageDef ];
					$msg->messageCode      = $entry->name;
					$msg->langShort        = $entry->language;
					$msg->langShortDef     = $entry->languageDef;
					$msg->messageTitle     = $entry->content;
					$msg->messageBody      = $entry->bodyNative;
					$msg->footerCode       = $entry->footer;
					$msg->mail             = $entry->isMail;
					$msg->dashbaord        = $entry->isDashboard;
					$msg->pushNotification = $entry->isPush;
					$msg->updateDate       = $entry->lastUpdate;
					$msg->save();
					$msg->createHistory();
					unset( $msg );
				}
				else {
					$wrd = new LangWord();
					$wrd->load( array( "wordCode" => $entry->name, "langShortDef" => $entry->languageDef ) );
					$wrd->lang_langWord = $langs[ $entry->languageDef ];
					$wrd->wordCode      = $entry->name;
					$wrd->langShort     = $entry->language;
					$wrd->langShortDef  = $entry->languageDef;
					$wrd->wordText      = $entry->content;
					$wrd->wordType      = $entry->type;
					$wrd->updateDate    = $entry->lastUpdate;
					$wrd->save();


					#create category index
					$wrd->buildCategoryHex( $entry->categories );

					unset( $wrd );
				}
				$log[ ] = array( "language" => $entry->languageDef, "type" => $entry->type, "wordCode" => $entry->name );
				$cnt++;
			}

			foreach ( $lastUpdate as $key => $time ) {
				$lang             = new Lang( $langs[ $key ] );
				$lang->lastUpdate = $time;
				$lang->save();
			}
			JPDO::commit();

			return array( "wordsCount" => $cnt, "lastUpdate" => $lastUpdate, "words" => $log );
		}
		else {
			return array( "wordsCount" => 0 );
		}
	}
}
