<?php
/**
 * currency real class - firstly generated on 10-12-2013 12:50, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/currency.php';

class Currency extends Currency_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * load all countries and return
	 *
	 * @return array
	 * @author Sam
	 **/
	public static function loadAll()
	{
		$c = new Currency();
		$c->populateOnce(false)->orderBy("name")->load(array("active"=>1));
		$thiss = array();

		if(sizeof($c->_ids)>0){
			do {
				$thiss[$c->code] = $c->name ." ".$c->code ." - ".$c->symbol;
			}while ($c->populate());
		}

		unset($c);
		return $thiss;

	}

	/*
	 * Return the symbol of the given code
	 * if no symbol found return the code name
	 */
	public static function getSymbol($code)
	{
		$curr = new Currency();
		$curr->load(array("code"=>$code));

		if($curr->gotValue){
			if(is_null($curr->symbol)){
				return $curr->code;
			}else{
				return $curr->symbol;
			}

		}else{
			// TODO: Error Symbol not found
			return "NO Currency";
		}



	}

}

