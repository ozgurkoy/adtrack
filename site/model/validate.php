<?php
/**
 * Base Validation Class
 *  Generic validation functions should be here
 *
 * @package default
 * @author Murat
 **/
class Validate
{
	/**
	 * Date format (for date only)
	 * @var string
	 */
	public static $dateFormat = "d.m.Y";

	public static $errorFields = array();

	public static $errorLangDefs = array();

	public static function getDefaltValues(){
		self::$dateFormat = Setting::getValue("dateFormat");
	}

	/**
	 * If validation success it returns true (as bool),
	 * @param ValidationResponse
	 */
	protected static function response(ValidationResponse &$r){
		if ( $r->valid && empty( $r->returnParams ) )
			return true;
		else {
			if ( isset( static::$errorFields[ $r->errorCode ] ) && empty( $r->errorField ) ) {
				$r->errorField = static::$errorFields[ $r->errorCode ];
			}
			if ( $r->errorCode == 88 ) {
				$r->errorDetail = "This field is mandatory";
			}

			if ( isset( static::$errorLangDefs[ $r->errorCode ] ) ) {
				$r->errorLangDef = static::$errorLangDefs[ $r->errorCode ];

			}

			return $r;
		}
	}

	/**
	 * Check given parameter as array and for mandatory fields
	 * @param array
	 */
	protected static function mandatoryCheck($params, $mFields, ValidationResponse &$vr){
		if (!is_array($params)){
			$vr->errorCode = 99;
		} else {
			foreach($mFields as $f){
				if (!isset($params[$f]) || ((is_array($params[$f]) && count($params[$f]) == 0) || (!is_array($params[$f]) && strlen($params[$f]) == 0))){
					$vr->errorField = $f;
					$vr->errorCode = 88;
					$vr->errorDetail = $f;
				}
			}
		}
	}

	/**
	 * Validate email
	 *
	 * @param $email
	 *
	 * @return bool
	 */
	public static function validateEmail($email) {

		// First, we check that there's one @ symbol, and that the lengths are right
		if (!preg_match('/^[^@]{1,64}@[^@]{1,255}$/', $email)) {
			// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
			return false;
		}
		// Split it into sections to make life easier
		$email_array = explode("@", $email);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) {
			if (!preg_match("/^(([A-Za-z0-9!#$%&'*+\/=?^_`{|}~-][A-Za-z0-9!#$%&'*+\/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$/", $local_array[$i])) {
				return false;
			}
		}
		if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) {
				return false; // Not enough parts to domain
			}
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if (!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$/", $domain_array[$i])) {
					return false;
				}
			}
		}

		return true;
	}

	/* check username validity
	*
	* @return bool
	* @author Özgür Köy
	**/
	public static function checkUsername($username='null')
	{
		if (preg_match('/^[a-z0-9\.\-\d_]{5,15}$/i', $username)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * check username validity
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function checkPassword($pass)
	{
		if (preg_match('/^.{6,20}$/i', $pass)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check that given date is true or false
	 *
	 * @param string $d
	 */
	public static function Date($d,$f=null){
		$format = empty($f) ? self::$dateFormat : $f;
		$date = date_parse_from_format($format,$d);
		return ($date["error_count"] == 0 && $date["warning_count"] == 0);
	}

	/**
	 * val url
	 *
	 * @return bool
	 **/
	public static function validateURL($url)
	{
		return preg_match('|^http(s)?\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,12}(/\S*)?$|i', $url);
	}


	public static function checkValidCountry($countryCode)
	{
		return is_numeric($countryCode);
	}

	public static function checkValidCity($countryCode,$cityCode)
	{
		return is_numeric($countryCode) && is_numeric($cityCode);
	}

	public static function checkValidSomething($countryCode,$cityCode)
	{
		return true;
	}

	public static function isValidDate($date,$format="yyyy-mm-dd")
	{
		return true;
	}

	public static function isValidTimeStamp($timestamp)
	{
		return ((string) (int) $timestamp === $timestamp)
		&& ($timestamp <= PHP_INT_MAX)
		&& ($timestamp >= ~PHP_INT_MAX);
	}

	/**
	 * Validate phone number
	 *
	 * @return boolean
	 */
	public static function validatePhone($phone,$mandatory=false){
		$validPhone = true;

		if (strlen($phone)>0 && $phone != "-"){
			if (strpos($phone,"-") === false){
				$validPhone = false;
			} else {
				$gsmNumber = explode("-", $phone);
				if (count($gsmNumber) != 2){
					$validPhone = false;
				}else{
					if (COUNTRY_CODE == 213) //Turkey
					{
						if(!is_numeric($gsmNumber[0])) {
							$validPhone = false;
						} elseif(!is_numeric($gsmNumber[1])) {
							$validPhone = false;
						}

						if (strlen($gsmNumber[0])<3){
							$validPhone = false;
						} elseif(strlen($gsmNumber[1])<7) {
							$validPhone = false;
						} elseif(strlen($gsmNumber[0]) > 4 || strlen($gsmNumber[1]) > 7)  {
							$validPhone = false;
						}
					}
					elseif (COUNTRY_CODE == 220) // United Kingdom
					{
						$phone = str_replace("-", "", $phone);
						$intError = null;
						$strError = null;
						$validPhone = self::checkUKTelephone($phone,$intError,$strError);
					}
					elseif (COUNTRY_CODE == 80) // Germany
					{
						if(!is_numeric($gsmNumber[0])) {
							$validPhone = false;
						} elseif(!is_numeric($gsmNumber[1])) {
							$validPhone = false;
						}

						if (strlen($gsmNumber[0])<3){
							$validPhone = false;
						} elseif(strlen($gsmNumber[1])<6) {
							$validPhone = false;
						} elseif(strlen($gsmNumber[0]) > 4 || strlen($gsmNumber[1]) > 7)  {
							$validPhone = false;
						}
					}
				}
			}
		} elseif ($mandatory && (strlen($phone) == 0 && $phone == "-")){
			$validPhone = false;
		}

		return $validPhone;
	}

	public static function checkUKTelephone (&$strTelephoneNumber, &$intError, &$strError) {

		// Copy the parameter and strip out the spaces
		$strTelephoneNumberCopy = str_replace (' ', '', $strTelephoneNumber);

		// Convert into a string and check that we were provided with something
		if (empty($strTelephoneNumberCopy)) {
			$intError = 1;
			$strError = 'Telephone number not provided';
			return false;
		}

		// Don't allow country codes to be included (assumes a leading "+")
		if (preg_match('/^(\+)[\s]*(.*)$/',$strTelephoneNumberCopy)) {
			$intError = 2;
			$strError = 'UK telephone number without the country code, please';
			return false;
		}

		// Remove hyphens - they are not part of a telephone number
		$strTelephoneNumberCopy = str_replace ('-', '', $strTelephoneNumberCopy);

		// Now check that all the characters are digits
		if (!preg_match('/^[0-9]{10,11}$/',$strTelephoneNumberCopy)) {
			$intError = 3;
			$strError = 'UK telephone numbers should contain 10 or 11 digits';
			return false;
		}

		// Now check that the first digit is 0
		if (!preg_match('/^0[0-9]{9,10}$/',$strTelephoneNumberCopy)) {
			$intError = 4;
			$strError = 'The telephone number should start with a 0';
			return false;
		}

		// Check the string against the numbers allocated for dramas

		// Expression for numbers allocated to dramas

		$tnexp[0] =  '/^(0113|0114|0115|0116|0117|0118|0121|0131|0141|0151|0161)(4960)[0-9]{3}$/';
		$tnexp[1] =  '/^02079460[0-9]{3}$/';
		$tnexp[2] =  '/^01914980[0-9]{3}$/';
		$tnexp[3] =  '/^02890180[0-9]{3}$/';
		$tnexp[4] =  '/^02920180[0-9]{3}$/';
		$tnexp[5] =  '/^01632960[0-9]{3}$/';
		$tnexp[6] =  '/^07700900[0-9]{3}$/';
		$tnexp[7] =  '/^08081570[0-9]{3}$/';
		$tnexp[8] =  '/^09098790[0-9]{3}$/';
		$tnexp[9] =  '/^03069990[0-9]{3}$/';

		foreach ($tnexp as $regexp) {
			if (preg_match($regexp,$strTelephoneNumberCopy, $matches)) {
				$intError = 5;
				$strError = 'The telephone number is either invalid or inappropriate';
				return false;
			}
		}

		// Finally, check that the telephone number is appropriate.
		if (!preg_match('/^(01|02|03|05|070|071|072|073|074|075|07624|077|078|079)[0-9]+$/',$strTelephoneNumberCopy)) {
			$intError = 5;
			$strError = 'The telephone number is either invalid or inappropriate';
			return false;
		}

		// Seems to be valid - return the stripped telephone number
		$strTelephoneNumber = $strTelephoneNumberCopy;
		$intError = 0;
		$strError = '';
		return true;
	}


	/* check for unique User Name
	*
	* @return bool false - exist , true - unique
	* @author Sam
	**/
	public static function checkUniqueUserName($value, $notMatching=0)
	{
		$user = new User();
		$user->load(array("username"=>$value,"status"=>"&ne ".User::ENUM_STATUS_DEACTIVATED, "id"=>"NOT IN ".$notMatching));

		if($user->gotValue)
			return false;

		return true;
	}

	/**
	 * check for unique Email is unique
	 *
	 * @return bool false - exist , true - unique
	 * @author Sam
	 * @author ozgur
	 **/
	public static function checkUniqueEmail( $value, User $user = null ) {
		$email  = new UserEmail();
		$filter = array( "email" => $value, "deleted" => "&ne 1" );

		if ( !is_null( $user ) )
			$filter[ "user_userEmail" ] = array( "id" => "&ne " . $user->id );

		$email->load( $filter );
		$r = $email->gotValue ? false : true;
		unset( $filter, $email );

		return $r;
	}

	/**
	 * Check the campaign link as a twitter valid img domain
	 *
	 * @return boolean
	 * @author Murat
	 */
	public static function imgLink($link){
		$validDomains = array(
			"http://pic.twitter.com",
			"http://yfrog.com",
			"http://twitpic.com",
			"http://twitvid.com",
			"https://pic.twitter.com",
			"https://yfrog.com",
			"https://twitpic.com",
			"https://twitvid.com",
			"http://youtube.com",
			"https://youtube.com",
			"http://youtu.be",
			"https://youtu.be",
			"http://www.youtube.com",
			"https://www.youtube.com",
			"http://www.youtu.be",
			"https://www.youtu.be",
			"http://goo.gl");
		$r = false;

		foreach($validDomains as $validDomain){
			if (substr(trim(strtolower($link)), 0, strlen($validDomain)) == $validDomain){
				$r = true;
			}
		}

		unset($validDomains);
		return $r;
	}

	public static function getBytesFromHexString($hexdata)
	{
		for($count = 0; $count < strlen($hexdata); $count+=2)
			$bytes[] = chr(hexdec(substr($hexdata, $count, 2)));

		return implode($bytes);
	}

	public static function getImageMimeType($imagedata)
	{
		$imagemimetypes = array(
			"jpg" => "FFD8",
			"png" => "89504E470D0A1A0A",
			"gif" => "474946",
			"bmp" => "424D",
			"tiff" => "4949",
			"tiff" => "4D4D"
		);

		foreach ($imagemimetypes as $mime => $hexbytes)
		{
			$bytes = self::getBytesFromHexString($hexbytes);
			if (substr($imagedata, 0, strlen($bytes)) == $bytes)
				return $mime;
		}

		return null;
	}
}

class ValidationResponse {
	/**
	 * Validation result
	 *
	 * @var bool
	 */
	public $valid = true;

	/**
	 * If validation false, the error code should be on that parameter
	 *  Valid validation codes for:
	 *      General     : 100-199
	 *      User        : 200-299
	 *      Campaign    : 300-399
	 *      Finance     : 400-499
	 * @var int
	 */
	public $errorCode = null;

	/**
	 * If any detail for that validation, this property should be include that detail
	 * @var string
	 */
	public $errorDetail = null;

	/**
	 * Field related to error
	 * @var string
	 */
	public $errorField = null;

	/**
	 * Language def of the error, for js.
	 * @var string
	 */
	public $errorLangDef = null;

	/**
	 * If error message needs to contain parameters, this property will be an array of those params
	 * @var array
	 */
	public $errorParams;

	/**
	 * If validation pass and there's object(s) to return, this property will be an array of those params
	 * @var array
	 */
	public $returnParams;
}