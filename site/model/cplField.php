<?php
/**
 * cplField real class - firstly generated on 26-03-2014 12:46, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplField.php';

class

CplField extends CplField_base
{
	public $htmlParts   = array();
	public $htmlBegin   = "";
	public $htmlEnd     = "";
	public $preview     = "";

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct( $id = null ) {
		parent::__construct( $id );
	}

	public static function loadByName( $name ) {
		$f = new CplField();
		$f->load( array( "type" => $name ) );

		return $f;
	}

	public function parseHtml() {
		preg_match( "|(<\w.+?>)(</\w.+>)|", $this->html, $mat );

		if ( sizeof( $mat ) > 0 )
			$this->htmlParts = array( $mat[ 1 ], $mat[ 2 ] );
		else
			$this->htmlParts = array( null, null );
	}

	public function buildPreview( $name, $values = array(), $defaultValue = null ) {
		$fieldName = CplLayout::norm( $name );
		$preview = array();
		$this->parseHtml();
		if ( is_null( $values ) ) $values = array();

		if ( strlen( $this->subHtml ) > 0 ) {
			if ( $name == "pfield" )
				$this->htmlBegin = str_replace( "{name}", $fieldName, $this->htmlParts[ 0 ] );
			else
				$this->htmlBegin = str_replace( "{name}", $fieldName, $this->htmlParts[ 0 ] );
			$preview[ ]      = $this->htmlBegin;
			foreach ( $values as $value ) {
				$ops = $this->subHtml;

				if ( trim( $defaultValue ) == trim( $value ) )
					$ops = str_replace( "@sel", "checked selected", $ops );
				else
					$ops = str_replace( "@sel", "", $ops );

				if ( $name == "pfield" ) {
					$ops = str_replace( "{value}", $value, $ops );
					$ops = str_replace( "{name}", $fieldName, $ops );
				}
				else {
//					$ops = str_replace( "{value}", '[[' . $value . ']]' . " ", $ops );
					//text first
					$ops = preg_replace("~(?<![\"'])(\{value\})~si",'[[' . trim($value) . ']]',$ops);
					//now the value
					$ops = preg_replace("~(\{value\})(?=['\"])~si", trim($value),$ops);

					$ops = str_replace( "{name}", $fieldName, $ops );
				}

				$preview[ ] = $ops;
				$options[ ] = $ops;
			}

			$this->htmlEnd = $this->htmlParts[ 1 ];
			$preview[ ]    = $this->htmlEnd;
		}
		else {
			$html = $this->html;

			if ( $name == "pfield" )
				$html = $this->htmlBegin = str_replace( "{name}", $fieldName, $html );
			else
				$html = $this->htmlBegin = str_replace( "{name}", $fieldName, $html );

			if ( $this->noLabel == 1 )
				$html = $this->htmlBegin = str_replace(
					"{value}",
					$name=="pfield" ?
						"Field Label":
						'[['.$name.']]',
					$html );
			else{
				if ( $name == "pfield" )
					$html = $this->htmlBegin = str_replace( "{value}", (strlen($defaultValue)>0?$defaultValue:$defaultValue), $html );
				else{
					$html = preg_replace("~(\{value\})(?=['\"])~si",(strlen($defaultValue)>0?'[['.$defaultValue.']]':$defaultValue),$html);
//					$html = $this->htmlBegin = str_replace( "{value}", (strlen($defaultValue)>0?'[['.$defaultValue.']]':$defaultValue), $html );
				}
			}
			$preview[ ] = $html;
		}

		return $this->preview = implode( "\n", $preview );
	}
}