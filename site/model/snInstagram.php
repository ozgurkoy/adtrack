<?php
/**
 * snInstagram real class - firstly generated on 10-10-2014 12:43, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/snInstagram.php';

class SnInstagram extends SnInstagram_base
{
	public $instagramObject;
	public $savedId=null;
	public $savedAvatar=false;
	public $accessToken = null;
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
		$this->instagramObject = new Instagram(array(
			'client_id' => Setting::getValue( "INSTAGRAM_CLIENT_ID" ),
			'client_secret' => Setting::getValue( "INSTAGRAM_CLIENT_SECRET" ),
			'grant_type' => 'authorization_code',
			'redirect_uri' => Setting::getValue( "INSTAGRAM_CALLBACK" ),
		));
	}

	/**
	 * Get login URL
	 *
	 * @return string
	 * @author Murat
	 */
	public function getLoginURL(){
		return $this->instagramObject->getAuthorizationUrl();
	}

	/**
	 * Add account
	 *
	 * @return bool|SnGoogle
	 */
	public function addAccount( $instagramId, $info )
	{
		// check if it's a new user
		$token = $this->token;

		$this->load( array( "iid" => $instagramId) );

		if ( !$this->gotValue || is_null( $this->publisher_snInstagram ) ) {
			$this->vkid        = $instagramId;
			$this->token       = $token;

			return $this->updateInfo( $info );
		}

		$this->loadPublisher();

		#this is an after fix. so that api works good.
		if(!$this->publisher_snInstagram || $this->publisher_snInstagram->gotValue === false )
			$this->publisher_snInstagram = null;

		return true;
	}

	/**
	 * @param null $instagramId
	 * @return bool
	 * @author Murat
	 */
	public function getInfo( ) {
		#does not require permission
		$this->instagramObject->setAccessToken($this->token);
		$user = json_decode($this->instagramObject->getUser($this->iid),true);
		if ( is_array( $user ) && isset( $user["meta"] ) && $user["meta"]["code"] == 200 ) {
			//print_r($user);exit;
			return $user["data"];
		}
		else {
			JLog::log( "social", "Instagram Problem : can't fetch info id:" . $this->iid );

			return false;
		}
	}

	/**
	 * @param object $info
	 * @return SnInstagram
	 */
	public function updateInfo( $info, $iid=null ) {
		$this->username       = isset( $info["username"] ) ? $info[ "username" ] : "";
		$this->bio     = isset( $info[ "bio" ] ) ? $info[ "bio" ] : "";
		$this->website        = isset( $info[ "website" ] ) ? $info[ "website" ] : "";
		$this->profilePicture  = isset( $info[ "profile_picture" ] ) ? $info[ "profile_picture" ] : "";
		$this->displayName       = isset( $info[ "full_name" ] ) ? $info[ "full_name" ] : null;
		$this->mediaCount    = isset( $info[ "counts" ] ) ? intval( $info[ "counts" ][ "media" ] ) : null;
		$this->followedBy    = isset( $info[ "counts" ] ) ? intval( $info[ "counts" ][ "followed_by" ] ) : null;
		$this->follows    = isset( $info[ "counts" ] ) ? intval( $info[ "counts" ][ "follows" ] ) : null;
		$this->updatedTime = time();
		if(!is_null($iid))
			$this->iid = $iid;

		$r                 = $this->save();
		$this->savedId     = $this->id;
		$this->savedAvatar = $this->profilePicture;

		return $r;
	}

	public function checkLoginInfo( $userId,$accessToken )
	{
		$this->instagramObject->setAccessToken($accessToken);

		if ($this->instagramObject->getCurrentUser()){
			$this->load(array(
				"iid" => $userId,
			));
			if ( $this->gotValue ) {
				$this->accessToken = $accessToken;
				$this->save();
				return true;
			} else {
				return false;
			}

		}
		else {
			JLog::log( "social", "Instagram Problem : can't fetch info ." . $this->token );

			return false;
		}
	}

	/**
	 * @param $code
	 * @return bool
	 * @author Murat
	 */
	public function getAccessToken( $code ) {
		try {
			$token = $this->instagramObject->getAccessToken( $code );
		}
		catch ( Exception $ex ) {
			//var_dump($J);exit;
			return false;
		}

		$this->iid        = $this->instagramObject->getCurrentUser()->id;
		$this->token       = $token;
		$this->save();

		return true;
	}
	/**
	 * @param $iid
	 * @param $info
	 * @return bool|SnInstagram
	 * @author Murat
	 */
	public function login( $iid, $info )
	{
		$token = $this->token;

		$this->load( array(   "_custom"=>"`iid`= '$iid' AND `publisher_snInstagram` IS NOT NULL"));
		#acc.token does not expire
		if ( $this->gotValue ) {
			$this->token       = $token;

			$this->loadPublisher()
				->writeHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "Login by Instagram", UserHistory::ENUM_MADEBY_USER )
				->loadUser()
				->login( null, null, $this->publisher_snInstagram->user_publisher->id );

			return $this->updateInfo( $info, $iid );
		}
		else{
			//New User
			$this->load( array( "iid" => $iid ) );

			$this->token       = $token;

			$this->updateInfo( $info,$iid );
			return false;
		}

	}

}

