<?php
class MailAction{

	private static $unreadLabel = "Label_1";
	private static $readLabel = "Label_2";
	private static $unknownLabel = "Label_5";
	private static $selfAddress = "track@admingle.com";
	private static $service = null;
	private static $fileIds = array();
	private static $gotContent = false; #specifically for text only messages.
	private static $attIds = array();
	private static $bodyParts = array();
	private static $client = null;


	public static function init() {
		global $__DP;
		include $__DP . "site/lib/Google/autoload.php";
		self::$client = new Google_Client();
		self::$client->setClientId( Setting::getValue( "TRACKMAIL_APPID" ) );
		self::$client->setClientSecret( Setting::getValue( "TRACKMAIL_KEY" ) );
		self::$client->addScope( "https://mail.google.com/" );
		self::$client->setAccessType( "offline" );
		//$client->setRedirectUri($redirect_uri);

		self::$service = new Google_Service_Gmail( self::$client );

		$token = self::processToken( self::$client );
		if ( strlen( $token ) > 5 ) {
			self::$client->setAccessToken( $token );
			self::processMessages();
		}
	}

	private static function getMessage($service, $userId, $messageId) {
		try {
			$message = $service->users_messages->get($userId, $messageId);
			// print 'Message with ID: ' . $message->getId() . ' retrieved.';
			return $message;
		} catch (Exception $e) {
			print 'An error occurred: ' . $e->getMessage();
		}
	}

	/**
	 * Modify the Labels a Message is associated with.
	 *
	 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
	 * @param  string $userId User's email address. The special value 'me'
	 * can be used to indicate the authenticated user.
	 * @param  string $messageId ID of Message to modify.
	 * @param  array $labelsToAdd Array of Labels to add.
	 * @param  array $labelsToRemove Array of Labels to remove.
	 * @return Google_Service_Gmail_Message Modified Message.
	 */
	private static function modifyMessage($service, $userId, $messageId, $labelsToAdd, $labelsToRemove) {
		$mods = new Google_Service_Gmail_ModifyMessageRequest();
		$mods->setAddLabelIds($labelsToAdd);
		$mods->setRemoveLabelIds($labelsToRemove);
		try {
			$message = $service->users_messages->modify($userId, $messageId, $mods);
			MailId::saveMailId( $messageId );

			print 'Message with ID: ' . $messageId . ' successfully modified.';

			return $message;
		} catch (Exception $e) {
			print 'An error occurred: ' . $e->getMessage();
		}
	}

	/**
	 * @param  Google_Service_Gmail $service Authorized Gmail API instance.
	 * @param  string $userId User's email address. The special value 'me'
	 * @return array
	 * @author Özgür Köy
	 */
	private static function listLabels($service, $userId) {
		$labels = array();

		try {
			$labelsResponse = $service->users_labels->listUsersLabels($userId);

			if ($labelsResponse->getLabels()) {
				$labels = array_merge($labels, $labelsResponse->getLabels());
			}

			foreach ($labels as $label) {
				print 'Label with ID: ' . $label->getId() . '<br/>';
			}
		} catch (Excetion $e) {
			print 'An error occurred: ' . $e->getMessage();
		}

		return $labels;
	}


	private static function processToken($client){
//		$token = self::getNewToken( $client );

		if ( !is_null( ( $at = Setting::getValue( "TRACKMAIL_ACCESS_TOKEN" ) ) ) && strlen( $at ) < 10 ) {
			$token = self::getNewToken( $client );
		}
		else
			$token = $at;

		$t = json_decode( $token, true );
		if ( intval( $t[ "created" ] ) + intval( $t[ "expires_in" ] ) < time() - 180 ){
			$token = self::getNewToken( $client );
		}

		return $token;
	}

	private static function getNewToken() {
		echo Setting::getValue( "TRACKMAIL_REFRESH_TOKEN" );
		MailHistory::log( "Getting Refresh Token" );

		try {
			self::$client->refreshToken( Setting::getValue( "TRACKMAIL_REFRESH_TOKEN" ) );
			$at = self::$client->getAccessToken();
			Setting::setValue( "TRACKMAIL_ACCESS_TOKEN", $at );
			MailHistory::log( "New Refresh Token", $at );

			return $at;
		}
		catch ( Google_Auth_Exception $e ) {

			MailHistory::log( "Error getting refresh token", print_r( $e , true));

			die( 'error' );

			//TODO ERROR HANDLING
		}

	}
	private static function fixBase64(&$str, $binary=false){
		$str = str_replace("_", "/",$str);
		$str = str_replace("-", "+",$str);
		$str = base64_decode($str);
		if($binary === false)
			$str = preg_replace("~(\s+?\n){3,}~s","\n\n",$str); #
	}

	private static function parseParts( $messageId, $part ) {
		$subparts = $part->getParts();
		$size     = $part->getBody()->getSize();
		$mimeType = $part->getMimeType();

		if ( !is_null( $filename = $part->getFilename() ) && strlen( $filename ) > 0 && $size > 0 ) {
			$attId    = $part->getBody()->getAttachmentId();
			self::$attIds[$attId]   = array( $filename, $mimeType );
		}
		elseif(stripos($mimeType,"text/plain")!==false){
			self::$gotContent = true;
			$b = $part->getBody()->getData();
			self::fixBase64( $b );
			if(!isset(self::$bodyParts[ "text" ]))
				self::$bodyParts[ "text" ] = "";
			self::stripTags( $b );
			self::$bodyParts[ "text" ] .= $b;
		}
		elseif(stripos($mimeType,"text/html")!==false){
			$b = $part->getBody()->getData();
			self::fixBase64( $b );
			self::stripTags( $b );

			self::$bodyParts[ ] = $b;
		}
		elseif ( sizeof( $subparts ) > 0 ) {
			for ( $pq = 0; $pq < sizeof( $subparts ); $pq++ ) {
				self::parseParts( $messageId, $part->getParts()[ $pq ] );
			}
		}
	}

	private static function saveFiles( $messageId ) {
		if(sizeof(self::$attIds)>0){
			foreach ( self::$attIds as $attId=>$file ) {
				$attach   = self::$service->users_messages_attachments->get( 'me', $messageId, $attId )->getData();
				self::fixBase64( $attach, true );
				self::$fileIds[ ] = File::putFile( $attach, $file[0], $file[1] );
			}
		}

	}


	private static function stripTags(&$content){
		$content = preg_replace("~<style.+?</style>~uismx", "", $content);
		$content = preg_replace("~<blockquote.+?</blockquote>~uismx", "", $content);
		$content = preg_replace("~<script.+?</script>~uismx", "", $content);
		$content = preg_replace("~<(?:(/div|/p|br|br/|br /))>~uim", "\n", $content);
		$content = preg_replace("~</[^>]+>~uim", " ", $content);
		$content = preg_replace("/&nbsp;/uim", " ", $content);
		$content = strip_tags( $content );
	}

	/**
	 *
	 * @param  string $userId User's email address. The special value 'me'
	 * @return array
	 * @author Özgür Köy
	 */
	private static function processMessages($userId='me') {
		$pageToken = NULL;
		$messages = array();
		$opt_param = array();
		do {
			try {
				if ($pageToken) {
					$opt_param['pageToken'] = $pageToken;
				}
				$opt_param["labelIds"] = self::$unreadLabel;

				$messagesResponse = self::$service->users_messages->listUsersMessages($userId, $opt_param);
				if ($messagesResponse->getMessages()) {
					$messages = array_merge($messages, $messagesResponse->getMessages());
					$pageToken = $messagesResponse->getNextPageToken();
				}
			} catch (Exception $e) {
				print 'An error occurred: ' . $e->getMessage();
			}
		} while ($pageToken);

		foreach ($messages as $message) {
			self::resetObject();

			$messageId = $message->getId();

			$threadId = $message->getThreadId();

			$m    = self::getMessage( self::$service, $userId, $messageId );
			$p    = $m->getPayload();
			$parts = (array)$p->getParts();

			$h       = $p->getHeaders();
			if(sizeof($parts)==0){
				$body=$p->getBody()->getData() ;
				self::fixBase64($body);
//				echo $body;

				self::stripTags($body);
//				echo $body;
				if(strlen(trim($body))<3)
					continue;
			}
			else{
				for ($ps=0; $ps < sizeof($parts); $ps++)
					self::parseParts( $messageId, $parts[ $ps ] );

//				print_r( self::$bodyParts );
//				exit;
				if(self::$gotContent === true ){
					#remove \r s so that regex works easier
					self::$bodyParts[ "text" ] = preg_replace("~\r~uim","",self::$bodyParts[ "text" ]);
//					echo self::$bodyParts[ "text" ];

//					echo "<hr>";
					/*for
					>
					>
					*/
					preg_match_all( "~\n(?:^>(?:.+)?$\n){3,}((?:.|\n|\s)+)~uim", self::$bodyParts[ "text" ], $mats, PREG_SET_ORDER );
					if(
						sizeof( $mats ) > 0 &&
						(
							strlen(trim($mats[0][1]))>1 ||
							preg_match("~^\s+?>~i",self::$bodyParts[ "text" ],$mss)>0
						)
					){
						#first line is > OR the there's a "not >" after the
						self::$bodyParts[ "text" ] = preg_replace("~^(>|\s)+~uim","",self::$bodyParts[ "text" ]);
					}
					else{
//						self::$bodyParts[ "text" ] = preg_replace("~\\n\\n(^>(.+)?$\\n){3,}(>.+)?(.|\\n|\\s)+~ium", "\n", self::$bodyParts[ "text" ] );
						self::$bodyParts[ "text" ] = preg_replace("~\\n(^>(.+)?$\\n){3,}(>.+)?(.|\\n|\\s)+~ium", "\n", self::$bodyParts[ "text" ] );
					}
					self::$bodyParts[ "text" ] = preg_replace( '~\n.+?wrote:~uim', "\n", self::$bodyParts[ "text" ] );

					/*for
					Da:  "adMingle.track" <info@e.admingle.com>
					Risposta:  <track@admingle.com>
					Data:  sabato 31 gennaio 2015 13:58
					*/
					self::$bodyParts[ "text" ] = preg_replace( '~(?:^[\\w-_]+?:.+?$\n){4,}(.|\n)+~uim', "\n", self::$bodyParts[ "text" ] );
					self::$bodyParts = array(self::$bodyParts[ "text" ]);
//					unset( self::$bodyParts[ "text" ] );
				}
				else{
					unset( self::$bodyParts[ "text" ] );
//					echo "NOT REMOVING TEXT";
				}

				$body = implode( " ".PHP_EOL, self::$bodyParts );
			}

			self::fixBody( $body );
//			echo $body;
//			exit;
			$link = "";
			preg_match( "|http(?:s)?://[\!\?&\^_@\+=\:\,\w\.\-\~\%/\$\#\(\)\{\}\|;]+|uism", $body , $matches );
			if ( sizeof( $matches ) > 0 && stripos( $matches[ 0 ], SERVER_NAME ) === false )
				$link = $matches[ 0 ];

			$subject = "";
			$from    = "";
			$ccs     = array();
			foreach ( $h as $h0 ) {
				$name = $h0->getName();
				// echo $name;
				if ( strtolower( $name ) == "subject" ) {
					$subject = $h0->getValue();
				}

				if ( strtolower( $name ) == "cc" ) {
					$ccs[] = $h0->getValue();
				}

				if ( strtolower( $name ) == "to" ) {
					$ccs[] = $h0->getValue();
				}

				if ( strtolower( $name ) == "from" ) {
					// preg_match("/<?([\\d\\w].+?@[^>]+?)>?$/uiUm", $h0->getValue(),$matches);
					preg_match("/<([\\d\\w].+?@[^>]+?)>/uiUm", $h0->getValue(),$matches);
					if(is_array($matches) && isset($matches[1]))
						$from = trim( strtolower( $matches[ 1 ] ) );
					else{
						preg_match("/^(?:\\s+)?([\\d\\w].+?@[^>]+?)$/uiUm", $h0->getValue(),$matches);
						if(is_array($matches) && isset($matches[1]))
							$from = trim( strtolower( $matches[ 1 ] ) );

					}

					echo ">>>>>>>".$from."<<<<<<<<<";

				}

			}

			if ( MailId::mailIdExists( $messageId ) !== false ){
				echo "skipping $messageId subject: $subject".PHP_EOL ;
				continue;
			}

			//echo $from . PHP_EOL;
//			echo ">".$from."<";
			if ( $from != "" && ( $user = DSuser::loadByEmail( $from ) ) !== false ) {
				self::saveFiles( $messageId );
				echo $from . " " . $subject . PHP_EOL;
				preg_match( "/\\[#(\\d+)\\]/u", $subject, $matches );
				$b = new Bug();

				$params = array(
					"threadId" => $threadId,
					"title"    => $subject,
					"link"     => $link,
					"fileIds"  => self::$fileIds,
					"user"     => $user->id,
					"details"  => $body
				);

				MailHistory::log( "Processing new mail", print_r( $params, true ) );

//				print_r( $params );
//				continue;
//				print_r( $matches );
				if ( isset( $matches[ 1 ] ) && is_numeric( $matches[ 1 ] ) ) {
					$bugId = $matches[ 1 ];
					#editing
					$b->load( $bugId );
				}
				else{
					$bugThread = Bug::loadByThread( $threadId );
//					var_dump( $bugThread );
					if($bugThread !== false){
						echo "Thread Found ".PHP_EOL;
						$b = $bugThread;
					}
				}

				if( sizeof(($extraFollowers = self::processCC( $ccs )))>0 )
					$params["extraFollowers"] = $extraFollowers;

				if ( $b->gotValue ) {
					//remove unnecessary
					unset( $params[ "link" ] );
					$response = $b->response( $params );
				}
				else {
					$response = Bug::create( $params );
				}

				if ( $response !== false ) {
					self::modifyMessage( self::$service, $userId, $messageId, array( self::$readLabel ), array( self::$unreadLabel ) );
				}

			}
			else {
//				self::modifyMessage( self::$service, $userId, $messageId, array( self::$unknownLabel ), array( self::$unreadLabel ) );

				//TODO ERROR LOG
			}
		}
	}

	private static function fixBody( &$body ) {
		$body = preg_replace( "/<!DOCTYPE.+?<\/html>/uism", "", $body );
	}

	private static function processCC( $ccs ) {
		$ccs = implode( ",", $ccs );
		$ret = array();
		$e0  = explode( ",", $ccs );
		foreach ( $e0 as $e1 ) {
			preg_match( "/<(.+?@.+?)>/uism", $e1, $matches );
			if ( isset( $matches[ 1 ] ) ) {
				$adr = trim( strtolower( $matches[ 1 ] ) );

				if ( $adr != self::$selfAddress && $adr != "" && ( $user = DSuser::loadByEmail( $adr ) ) !== false ) {
					$ret[ ] = $user->id;
				}
			}
		}

		return $ret;
	}

	private static function resetObject() {
		self::$gotContent = false;
		self::$fileIds    = array();
		self::$attIds     = array();
		self::$bodyParts  = array();
	}
}