<?php
/**
 * Tool class
 *
 * @package default
 * @author tiger
 **/
class Tool
{
	/**
	 * min shorturl length
	 *
	 * @var int
	 **/
	public static $minLength = 4;

	/**
	 * max shorturl length
	 *
	 * @var int
	 **/
	public static $maxLength = 8;

	/**
	 * url chars
	 *
	 * @var string
	 **/
	public static $urlChars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";

	private static function conn() {
		JPDO::connect( JCache::read('JDSN.MDB') );
	}

	public static function getUserSummary() {
		global $__site;
		$cantConnectToMemcache = false;
		$memcache = new Memcache;
		$memcache->connect(MEMCACHE_SERVER, MEMCACHE_PORT) or $cantConnectToMemcache = true;
		$userStats = null;

		if ($cantConnectToMemcache || !($userStats = $memcache->get($__site.'_UserStats'))) {

			$sqlStr = "
			SELECT
				(SELECT COUNT(*) TOPLAM FROM user u WHERE u.utype = 'p' AND u.active = 1) member,
				(SELECT COUNT(*) TOPLAM FROM user u WHERE u.utype = 'p' AND u.ptype = 'c' AND u.active = 1) celebrity,
				(SELECT COUNT(*) TOPLAM FROM user u WHERE u.utype = 'p' AND u.ptype = 't' AND u.active = 1) trendsetter,
				(Select SUM(follower) FROM twitterInfo) follower
			";

			JPDO::connect( JCache::read('JDSN.MDB') );
			$res = JPDO::executeQuery($sqlStr);

			$userStats =	(isset($res[0])
									?
										array("follower"=>$res[0]['follower'],
											"member"=>$res[0]['member'],
											"celebrity"=>$res[0]['celebrity'],
											"trendsetter"=>$res[0]['trendsetter']
										)
									:
										array("follower"=>0,
											"member"=>0,
											"celebrity"=>0,
											"trendsetter"=>0
										));
			$memcache->set($__site.'_UserStats', $userStats, MEMCACHE_COMPRESSED, 10);
		}
		if (!$cantConnectToMemcache) $memcache->close();
		unset($memcache);

		return $userStats;
	}

	public static function getTotalCostOfUsersFromDb($userArray) {
		if(sizeof($userArray)==0) return 0;

		$sql = "
			SELECT
				SUM(ti.`customCost`*u.`msgcnt`) cos
			FROM
				`twitterInfo` ti
				INNER JOIN `user` u ON ti.user = u.id
			WHERE
				ti.user IN(".implode(',', array_values($userArray)).")
		";

		$res = JPDO::executeQuery($sql);
		return isset($res[0]) ? round($res[0]['cos'], 2) : 0;
	}

	/**
	 * checks session if the current user is a looged in publisher
	 * if not redirects to home
	 *
	 * @global type $_S
	 */
	public static function checkLoginPublisher() {
		global $_S;
		if(!isset($_SESSION['adId']) || !isset($_SESSION['adType']) || $_SESSION['adType']!='p') {
			header('Location: '.$_S.LOGIN_PATH);
			exit;
		}
	}

	public static function getCountryNameFromShortCode($shortCode) {
		$sql = "
			SELECT
				name
			FROM
				country
			WHERE
				shortName='{$shortCode}'
		";
		JPDO::connect( JCache::read('JDSN.MDB') );
		$results = JPDO::executeQuery($sql);
		if(count($results)) {
			return $results[0]['name'];
		} else {
			return JUtil::getLangWord("unknown");
		}
	}

	public static function checkAdminLogin($checkfor = ""){
		global $_S;
		if(!isset($_SESSION['admingleAX'])){
			Tool::setLastError("admlf",$checkfor);
			header("Location:".$_S."darkside");
			exit;
		}
	}

	public static function setLastError($errorType,$errorCode,$errDetail=null){
		$error = json_encode(Array("errorType" => $errorType, "errorCode" => $errorCode, "errDetail" => $errDetail));
		JLog::log($errorType,$errorCode);
		if(!isset($_SESSION['lang'])){
			if(!isset($_SESSION)) session_start();
			$_SESSION['lang'] = Setting::getValue("DEFAULT_LANGUAGE");
		}
		$_SESSION["LAST_ERROR"] = $error;
		//die($_SESSION["LAST_ERROR"]);
	}

	public static function checkDate($d){
		$r = date_parse_from_format("d/m/Y",$d);
		if ($r["warning_count"] > 0 || $r["error_count"] > 0)
			return false;
		else
			return true;
	}

	public static function echoLog($msg,$appName = null){
		echo date("Y.m.d H:i:s") . " => " . (is_null($appName) ? "" : $appName . " => ") . $msg . PHP_EOL;
	}

	/**
	 * Alert administrators for appropriate message
	 *
	 * @return void
	 **/
	public static function alertToAdmins($msg){
		if (strlen($msg)>140){
			mail::q("murat@admingle.com","[adMingleAlert]","<html><head></head><body>".$msg."</body></html>");
			$msg = "adMingleAlert! Check your mail";
		}
		global $__DP;
		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');
		$connection = new TwitterOAuth("Ij1hpJuraStnIkCT1HFmw",
										"DRO5ThfNZdW31kIpXYHa15nvSdofegUhg8gm5GMh9o",
										"279130274-jweQQyToUy4Mt6V5044ukWDMRz36UboX67MasHGR",
										"D7CD3uJbujULomDE6Rw7JkEzbVeX8tNmKwDRyXnXLk");

		$connection->post('direct_messages/new', array('screen_name' => "hardc0der", "text" => $msg));

	}

	/**
	 * Get a formatted string for given number
	 *
	 * @return string
	 */
	public static function getFormattedNumber($n){
		$n = intval($n);
		if (strlen($n) == 4){
			return $n;
		}elseif (strlen($n)<4) {
			return str_repeat("0", (4-strlen($n))) . $n;
		}elseif (strlen($n)<6){
			$val = (1+intval(substr($n,0,(strlen($n)-3)))) . "k";
			return str_repeat("0", (4-strlen($val))) . $val;
		}else{
			$val = (1+intval(substr($n,0,(strlen($n)-6)))) . "m";
			return str_repeat("0", (4-strlen($val))) . $val;
		}
	}

	public static function convertTR2EN($text) {
		$search = array('Ç','ç','Ğ','ğ','ı','İ','Ö','ö','Ş','ş','Ü','ü');
		$replace = array('c','c','g','g','i','i','o','o','s','s','u','u');
		$new_text = str_replace($search,$replace,$text);
		return $new_text;
	}

	public static function checkAccess(){
		global $__DP;

		$myFile = "$__DP/temp/accessLog.txt";
		$fh = fopen($myFile, 'a') or die("can't open file");
		$stringData = time() . " => access from " . PHP_SAPI . " \n";
		fwrite($fh, $stringData);
		fclose($fh);
	}
	
	/**
	 * Check new admin panel is activated
	 * 
	 * @return void
	 * @author Murat
	 */
	public static function checkNewAdminPanel($newPanel=null){
		global $_newPanel;
		if ((SERVER_NAME != "t.admingle.com" && SERVER_NAME != "s1th.admingle.com") || (defined("NEW_PANEL") && !NEW_PANEL)){
			return false;
			$_newPanel = null;
			$_SESSION["newPanel"] = "old";
		}
		if ($newPanel == "old") {
			$_newPanel = null;
			$_SESSION["newPanel"] = "old";
		}elseif ($newPanel == "new" || (isset($_SESSION["newPanel"]) && $_SESSION["newPanel"] = "new")){
			$_newPanel = "wl_";
			if (!isset($_SESSION["newPanel"])){
				$_SESSION["newPanel"] = "new";
			}
		}
		//die("newPanel value: " . $_newPanel);
	}
        
        
        public static function IsMobileDevice(){      
                $reg_Mobile = "(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge\ |maemo|midp|mmp|netfront|opera\ m(ob|in)i|palm(\ os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows\ (ce|phone)|xda|xiino";
                $reg_Mobilev2 = "/android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i";
                $reg_Mobilev3 = "/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i";
                
               
                $useragent=$_SERVER['HTTP_USER_AGENT'];
                if(preg_match($reg_Mobilev2,$useragent)||preg_match($reg_Mobilev3,substr($useragent,0,4)))
                {
                    return true;
                }
                else
                {
                    
                    
                    return false;
                }
        }
        
        /**
	 * Check if the user is amobile user
	 * 
	 * @return true - goto mobile site
         * @return false - goto desktop site
	 * @author sam
	 */
	public static function GoToMobileSite(){
            
            if((isset($_SESSION["is_mobile"]) && $_SESSION["is_mobile"] == 0) || 
                    (isset($_SESSION["use_desktopSite"]) && $_SESSION["use_desktopSite"] == 1)){
                return false;
                }else if((isset($_SESSION["is_mobile"]) && $_SESSION["is_mobile"] == 1)){
                return true;
            }
            else{
                // Check if the user using mobile device
                //RewriteCond %{HTTP_USER_AGENT} "android|blackberry|googlebot-mobile|iemobile|ipad|iphone|ipod|opera mobile|palmos|webos" [NC]

               
                $useragent=$_SERVER['HTTP_USER_AGENT'];
                if(Tool::IsMobileDevice())
                {
                    
                    $_SESSION["is_mobile"] = 1;
                    return true;
                }
                else
                {
                    
                    $_SESSION["is_mobile"] = 0;
                    return false;
                }
                
            }
            
            //$_COOKIE["use_desktopSite"];
            
            
	}
        
        /**
	 * Mobile user that want to change to desktop site
	 * 
	 * @return void
	 * @author sam
	 */
	public static function GoToDesktopSite(){
		
            
            $_SESSION["use_desktopSite"] = 1;
            
            // Maybe set cookie for a short period 
            //$_COOKIE["use_desktopSite"];
                
	}
        
    /**
	 * Mobile user that want to change to desktop site
	 * 
	 * @return void
	 * @author sam
	 */
	public static function RedirectToPreferdSite($_desktopsite,$_mobileSite){
                
            $_query = JUtil::getSearchParameter();
                if (isset($_query['desktop']))
                {
                    if($_query['desktop'] == 1) {
                        Tool::GoToDesktopSite();
                            return $_desktopsite;
                        }else
                        {
                            $_SESSION["use_desktopSite"] = 0;
                        }
                    if($_query['desktop'] == 0) {
                            return $_mobileSite;
                    }    
                }
                
            
                if(Tool::GoToMobileSite())
                {
                    return $_mobileSite;
                }else
                {
                    return $_desktopsite;
                }    
	
	}
	
	/**
	 * Add newly registered users to active CPC campaigns
	 * 
	 * @return int
	 * @author Murat
	 */
	public static function addNewUserToActiveCampaigns($uid){
		
		JPDO::beginTransaction();
		$campaign = new Campaign();
		$campaign->customClause = "race='c' AND state IN ('pp','p') AND endDate >= DATE(NOW())";
		$campaign->nopop()->load();
		$campCount = 0;
		while($campaign->populate()){
			$trends = array();
			$t = $campaign->loadTrends();
			do {
				$trends[$t->id] = "ok";
			} while ($t->populate());
			//die("trends " . print_r($trends));
			$result = Tool::getUserCountsFiltered(array(
				"gender"	=> $campaign->sex,
				"city"		=> $campaign->city,
				"country"	=> $campaign->country,
				"agerange"	=> $campaign->ageRange,
				"trends"	=> $trends,
				"uid"		=> $uid
			),$campaign->race);
			if ($result["cid"]){
				//echo "Checking for the campaign# {$campaign->id} \n";
				$u = new User($uid);
				$cu = new CampaignUser();
				$cu->campaign = $campaign->id;
				$cu->user = $uid;
				$cu->msgcnt = $u->msgcnt;
				$cu->load();
				//var_dump($cu);
				//exit;
				if (!$cu->count){
					//echo "Adding user {$uid} to {$campaign->id} \n";
					$sql = "
						SELECT
							th.id AS thID,
							kh.id AS khID,
							kh.cost,
							kh.customCost,
							IFNULL(th.follower,0) AS follower,
							IFNULL(kh.trueReach,0) AS trueReach
						FROM
							twitterInfo ti
							LEFT JOIN twitterHistory th ON ti.id = th.twitterInfo
							LEFT JOIN twitterInfoKlout tik ON ti.id = tik.twitterInfo
							LEFT JOIN kloutHistory kh ON tik.id = kh.twitterInfoKlout
						WHERE
							ti.user = {$uid}
						ORDER BY
							kh.id DESC,
							th.id DESC
						LIMIT 0,1
					";
					JPDO::connect( JCache::read('JDSN.MDB') );
					$r = JPDO::executeQuery($sql);
					
					$cu->campaign = $campaign->id;
					$cu->user = $uid;
					$cu->msgcnt = $u->msgcnt;
					$cu->state = "w";
					if (count($r)){
						$cu->twitterHistory = $r[0]["thID"];
						$cu->kloutHistory = $r[0]["khID"];
						$cu->cost = ($r[0]['customCost'] > 0) ? $r[0]['customCost'] : ($r[0]['cost'] > 0 ? $r[0]['cost'] : 0);
					} else {
						$cu->twitterHistory = null;
						$cu->kloutHistory = null;
						$cu->cost = 0;
					}
					$cu->availableDate = $campaign->activationDate;
					$cu->emailed = 0;
					$cu->isReserved = 0;
					$cu->cpcFinishWarning = 0;
					$cu->save();
					
					$campaign->publisher++;
					if (count($r)){
						if ($r[0]["follower"] > 0) $campaign->follower += $r[0]["follower"];
						if ($r[0]["trueReach"] > 0) $campaign->trueReach += $r[0]["trueReach"];
					}
					$campaign->save();
					$campCount++;
					//echo "User added to {$campaign->id} \n";
				}
			}
		}
		JPDO::commit();
		
		return $campCount;
	}


	/**
	 * Create configuration file from DB
	 *
	 * @return string
	 * @author Murat
	 */
	public static function generateConfigfile(){
		global $__DP;
		$con = JCache::read( 'JDSN.MDB' );
		JPDO::connect($con);
		$rows = JPDO::executeQuery("SELECT * FROM `".$con["schema"]."`.`localConfig` ORDER BY `category`, `name`");

		ob_start();

		echo '<?php' . "\n";
		if (is_array($rows) && sizeof($rows)>0){
			$categories = array();
			foreach($rows as $s){
				if (!isset($categories[$s["category"]])){
					$categories[$s["category"]] = true;
					echo '
/********************************************************************************************************************************************
*
* '. $s["category"] .'
*
********************************************************************************************************************************************/

';
					if ($s["category"] == "IP Restriction"){
						echo '$ADMINGLE_IPS = array();' . "\n\n";
					} elseif ($s["category"] == "IP Restriction"){
						echo '$SYSTEM_EMAILS = array();' . "\n\n";
					}
				}
				if (strlen($s["description"])){
					$s["description"] = str_replace("\r\n","\n ",$s["description"]);
					if (strpos($s["description"],"\n")){
						$s["description"] = str_replace("\n","\n* ",$s["description"]);
						echo '
/*
* ' . $s["description"] . '
*/
';
					}else{
						echo '// ' . $s["description"] . "\n";
					}
				}
				switch($s["category"]){
					case "General Constants":
						echo '$GLBS["constants"]["' . $s["name"] . '"] = ' . (!is_numeric($s["value"]) ? '"' . $s["value"] . '"' : $s["value"]) . ';' . "\n\n";
						break;
					case "IP Restriction":
						echo '$ADMINGLE_IPS[] = "' . $s["value"] . '";' . "\n\n";
						break;
					case "System Emails":
						echo '$SYSTEM_EMAILS["' . $s["name"] . '"] = "' . $s["value"] . '";' . "\n\n";
						break;					case "General Settings":
					case "Paypal":
					case "Twitter Settings":
					default:
						echo 'define("' . $s["name"] . '",' . (!is_numeric($s["value"]) ? '"' . $s["value"] . '"' : $s["value"]) .');' . "\n\n";
						break;
				}
			}
		}
		$localConfigFile = ob_get_contents();
		ob_end_clean();
		//die($localConfigFile);
		$localConfigFileName = $__DP."site/def/localConfig.php";
		$fp = fopen($localConfigFileName, 'w+');
		fwrite($fp, $localConfigFile);
		fclose($fp);

//		chmod($localConfigFileName, 0777);

		return true;
	}

	/**
	 * Call Short Link Service
	 *
	 * @return object
	 * @author Murat
	 */
	public static function callSLService($serviceNode,$params,$method = "POST"){
		global $__DP;

		$url = "http://" . Setting::getValue("SL_SERVICE_URL") . "/";
		$requestDate = time();
		$request = new RestReq($url . $serviceNode, $method, $params,"json");
		$request->execute();
		//echo '<pre>' . print_r($request, true) . '</pre>';
		//exit;
		//print_r($request);

		/*
		$log = new LogWebservice();
		$log->requestDate = $requestDate;
		$log->request = json_encode(array(
			"URL" => $url . $serviceNode,
			"Params" => json_encode($params)
		));
		$log->response = $request->responseBody;
		$log->save();
		unset($log);
		*/

		//print_r($data);
		//die($data["responseInfo:protected"]["http_code"]);
		//exit;
		$data = json_decode($request->responseBody);
		if (isset($data->error) && $data->error != "no clicks found"){ // || empty($data)){
			JLOG::log("slsAPI", "URL : {$url} \n
								 ServiceNode: {$serviceNode} \n
								 Method: {$method} \n
								 Params: " . serialize($params) . " \n
								 API result: " . serialize($data) . " \n",2);
		}
		return $data;
	}

	public static function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}

		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
	}

	public static function arrayToObject($d) {
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call
			*/
			return (object) array_map(__FUNCTION__, $d);
		}
		else {
			// Return object
			return $d;
		}
	}

	public static function getFileExtension($file_name) {
		return substr(strrchr($file_name,'.'),1);
	}


	/**
	 * Call TweetGator Service
	 *
	 * @return object
	 * @author Murat
	 */
	public static function callTGService($serviceNode,$params,$method = "POST"){
		global $__DP;
		$request = new RestReq(Setting::getValue("TG_SERVICE_URL") . "/api/gator/" . $serviceNode, $method, $params,"json");
		$request->execute();
		//echo '<pre>' . print_r($request, true) . '</pre>';
		//exit;
		$data = json_decode($request->responseBody);
		if (empty($data)){
			JLOG::log("tgAPI", "Response is empty: \n
								 URL : http://" . Setting::getValue("TG_SERVICE_URL") . "/ \n
								 ServiceNode: {$serviceNode} \n
								 Method: {$method} \n
								 Params: " . serialize($params) . " \n
								 API result: " . $request->responseBody . " \n");
		} elseif (isset($data->responseCode) && $data->responseCode != 200){
			JLOG::log("tgAPI", "URL : http://" . Setting::getValue("TG_SERVICE_URL") . "/ \n
								 ServiceNode: {$serviceNode} \n
								 Method: {$method} \n
								 Params: " . serialize($params) . " \n
								 API result: " . serialize($data) . " \n");
		}
		$data = is_object($data) && !empty($data->responseBody) ? $data->responseBody : $data;
		return $data;
	}

}
