<?php
/**
 * cplCampaign real class - firstly generated on 26-03-2014 12:46, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplCampaign.php';

class CplCampaign extends CplCampaign_base
{
	public static $landingsPath = "files/modules/CPL/landings";
	public $landingFolder = null;
	public $landingRelPath = null;
	public static $maxFieldLength = 500;

	const ERROR_IN_FORM        = -1;
	const GOOD_FORM            =  1;
	const CAMPAIGN_NOT_FOUND   = -4;
	const CAMPAIGN_NOT_STARTED = -5;
	const CAMPAIGN_NOT_ACTIVE  = -2;
	const CAMPAIGN_UNDERINVEST = -6;
	const TOO_MANY_NEGATIVES   = -3;
	const LOAD_ALL_VALUES      = 500;

	const FIELD_ARRAY          = 1;
	const FIELD_TEXT           = 0;

	const IGNORE_PUBLISHER = true;
	const USE_TOKEN = true;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	public static function isCampaignRunning( $code, CplCampaign &$c=null ) {
		$r = true;

		if ( !is_null( $code ) ) { //we might receive a CplCampaign object
			$c = new CplCampaign();
			$c->loadByCode( $code );
		}

		$c->loadCampaign();

		$endDate   = strtotime( $c->campaign_cplCampaign->endDate ) + ( 24 * 3600 );


		if ( $c->gotValue !== true )
			$r = CplCampaign::CAMPAIGN_NOT_FOUND;
		elseif ( $c->state == CplCampaign::ENUM_STATE_ENDED || $c->state != CplCampaign::ENUM_STATE_STABILE || $endDate <= time() ){
			$r = CplCampaign::CAMPAIGN_NOT_ACTIVE;}
		elseif ( !( $c->startDate <= time() ) )
			$r = CplCampaign::CAMPAIGN_NOT_STARTED;
		elseif ( $c->maxOverlappingNegative > 0 && $c->currentOverlappingNegative > $c->maxOverlappingNegative ) {
			$r = CplCampaign::TOO_MANY_NEGATIVES;
		}


		//can't unset $c

		return $r;
	}


	/**
	 * saving the details of the layout to campaign
	 *
	 * @param $options
	 * @param $filesArray
	 * @author Özgür Köy
	 */
	public function	attachLayout( $options, $filesArray, $fields ) {
//		print_r( $filesArray );exit;
		//fields first
		$exports = array();
		$finished = array();

		foreach ( $options as $field => $value ) {
			if ( is_array( $value ) ) {
				//repeat seq
				foreach ( $value as $lowerField => $lowerFieldArray ) {
					foreach ( $lowerFieldArray as $v0 => $lowerFieldValue ) {
						if ( !isset( $exports[ $field ] ) )
							$exports[ $field ] = array();
						$exports[ $field ][ $v0 ][ $lowerField ] = $lowerFieldValue;
					}
				}

				//check for files array
				if ( isset( $filesArray[ "name" ][ $field ] ) && is_array( $filesArray[ "name" ][ $field ] ) ) {

					foreach ( $filesArray[ "name" ][ $field ] as $ufield => $ufieldArray ) {
						foreach ( $ufieldArray as $v0 => $lowerFieldValue ) {
							if ( !isset( $filesArray[ "tmp_name" ][ $field ][ $ufield ][ $v0 ] ) || !( strlen( $filesArray[ "tmp_name" ][ $field ][ $ufield ][ $v0 ] ) > 0 ) )
								continue;

							if ( !isset( $exports[ $field ] ) )
								$exports[ $field ] = array();

							//file exists in files seq
							$newFile                             = $this->saveFile(
								$this->code,
								array(
									"name"     => $filesArray[ "name" ][ $field ][ $ufield ][ $v0 ],
									"tmp_name" => $filesArray[ "tmp_name" ][ $field ][ $ufield ][ $v0 ]
								)
							);
							$finished[ ] = $field . "_" . $ufield;
							$exports[ $field ][ $v0 ][ $ufield ] = $newFile;
						}
					}
				}


			}
			else {
				//regular dandik field
				$exports[ $field ] = $value;
			}
		}

		//now render the files array
		if ( isset( $filesArray[ "name" ] ) ) {

			$f0 = json_decode( $fields, true );
			$f0 = $f0["repeats"];

			foreach ( $filesArray[ "name" ] as $field => $fileV ) {
				if ( !is_array( $fileV ) ) {
					if (
						!isset( $filesArray[ "tmp_name" ][ $field ] ) ||
						!( strlen( $filesArray[ "tmp_name" ][ $field ] ) > 0 )
					)
						continue;

					$newFile           = $this->saveFile(
						$this->code,
						array(
							"name"     => $filesArray[ "name" ][ $field ],
							"tmp_name" => $filesArray[ "tmp_name" ][ $field ]
						)
					);
					echo $newFile;
					$exports[ $field ] = $newFile;

				}
				else{
					//for repeats , though task here.
//					continue;
					foreach ( $f0 as $field0=>$fieldDetails ) {
						$fields0 = $fieldDetails[ "fields" ];
						if(sizeof($fields0)>1)  // this has been processed above.
							continue;

						foreach ( $fields0 as $fieldName => $fieldInfo ) {
							if($fieldInfo["type"] == "file"){
								if($field0 == $field){
									$c0 = 0  ;
									foreach ( $fileV as $fieldName0 => $files ) {
										foreach ( $files as $fi=>$f3 ) {
											if(in_array($field0."_".$fieldName0, $finished))
												continue;

											$newFile                             = $this->saveFile(
												$this->code,
												array(
													"name"     => $filesArray[ "name" ][ $field0 ][ $fieldName0 ][ $fi ],
													"tmp_name" => $filesArray[ "tmp_name" ][ $field0 ][ $fieldName0 ][ $fi ]
												)
											);
											$exports[ $field0 ][ $fi ][ $fieldName0 ] = $newFile;

										}

									}

								}
			}
		}

					}
//					echo $field.PHP_EOL;
				}
			}
		}
//		echo "___";
//		print_r( $exports );
//		echo "___";
//
//		exit;
		$this->details = json_encode( $exports, JSON_HEX_QUOT );
		$this->save();
	}

	/**
	 * excel report
	 *
	 * @param null $forceLang
	 * @author Özgür Köy
	 */
	public function getExcelReport($forceLang = null) {
		$this->loadCampaign();

		if ( !( $this->campaign_cplCampaign && $this->campaign_cplCampaign->gotValue ) ){
			JLog::log( "cri", "Cpl campaign export problem:" . serialize( $this ) );
			die( "Problem with excel export" );
		}

		if(is_null($forceLang))
			$lang = $this->campaign_cplCampaign->loadAdvertiser()->loadUser()->loadLang()->langShortDef;
		else
			$lang = $forceLang;

		$leads = $this->getLeadCounts();

		$words = LangWord::collectWordArray( $lang, array( "campaignDetails","leadReport", "title", "validLeadCount", "totalLeadCount", "fields", "values", "date" ) );

		$fields = $this->loadForm()->getFields();
		$labelWords = array();
		Format::getDateTimeFormated( 123 ); //php excel fucks it below.

		foreach ( $fields as $d0) {
			$d0 = $d0[ "label" ];
			$label = LangWord::collectWordArray( $lang, array( $d0 ) );

			if(isset($label[$d0]))
				$labelWords[ $d0 ] = $label[ $d0 ];
			else
				$labelWords[ $d0 ] = $d0;
		}

		$eb = new ExcelBuilder();
		$eb->createHeaderRow($words["campaignDetails"],sizeof($labelWords));

		$eb->createRow($words["date"],date("Y-m-d H:i"))
			->createRow($words["title"],$this->campaign_cplCampaign->title)
			->createRow($words["totalLeadCount"],$leads["total"])
			->createRow($words["validLeadCount"],$leads["valid"])
			->createEmptyRow();

		$eb->createEmptyRow();
		$eb->createHeaderRow($words["values"],sizeof($labelWords));

		$eb->addColumn( $words["date"], ExcelBuilder::SET_BOLD );
		foreach( $labelWords as $lab ){
			$eb->addColumn( $lab, ExcelBuilder::SET_BOLD );
		}
		$leads = CplCampaign::listLeads( $this->code, CplCampaign::LOAD_ALL_VALUES, CplCamp::ENUM_STATE_APPROVED );
		$eb->createEmptyRow();

		if ( $leads->gotValue ) {
			do {
				$eb->addColumn( Format::getDateTimeFormated( $leads->date ) );

				$details = json_decode( $leads->details, JSON_HEX_QUOT );
				$rowData = array();

				foreach ( $details as $d1 )
					$eb->addColumn( $d1 );
				$eb->createEmptyRow();

			} while ( $leads->populate() );

		}

		$eb->finalizeDoc();
		$eb->getBinary("adMingleLead");
	}

	/**
	 * save phase #1, save details create folders etc.
	 * @param $params
	 * @author Özgür Köy
	 */
	public function prepareLayout( $params, $files=array() ) {
		global $__DP;

		$oldHash                      = $this->preHash;
		$this->form                   = $params[ "form" ];
		$this->cplCampaign_advertiser = $params[ "cplCampaign_advertiser" ];
		$this->layout                 = $params[ "layout" ];
		$this->footerText             = $params[ "footerText" ];
		$this->name                   = $params[ "name" ];
		$this->endMessage             = $params[ "endMessage" ];
		$this->preHash                = $params[ "preHash" ];

		$f = new CplForm( $this->form );
		$f = $f->getFields( );
		$fields = array();

		foreach ( $f as $f0 )
			$fields[ ] = CplLayout::norm( $f0[ "label" ] );


		if($this->gotValue !== true){
			$this->code = JUtil::randomString( 12 );
		}

		$this->save();
		$l                    = strtoupper( substr( $this->code, 0, 1 ) );
		$this->landingFolder  = $__DP . CplCampaign::$landingsPath . "/" . $l . "/" . $this->code . "/";
		$this->landingRelPath = CplCampaign::$landingsPath . "/" . $l . "/" . $this->code . "/";

		if ( $oldHash != $params[ "preHash" ] )
			$this->getCode( null, $params[ "preHash" ] );

		if($this->gotValue !== true){
			mkdir( $this->landingFolder, 0775, true );
			mkdir( $this->landingFolder . "files" );
			mkdir( $this->landingFolder . "templateCache" );

			CplCampaign::createCampTable( $this->code, $fields );
		}

		if(sizeof($files)>0 && strlen($files["favicon"]["tmp_name"])>0){
			$path = $this->saveFile(null,$files["favicon"],$this->landingFolder . "files","___favicon.png");
			$this->favicon = $path;
			$this->save();
		}
	}

	/**
	 * save the incoming package
	 *
	 * @param $code
	 * @param $uploadArray
	 * @return string
	 * @author Özgür Köy
	 */
	private function saveFile( $code, $uploadArray, $customPath=null, $filename=null ) {
		global $__DP;

		if(is_null($filename)){
			$e0       = explode( ".", $uploadArray[ "name" ] );
			$filename = JUtil::randomString( 15 ) . "." . array_pop( $e0 );
		}

		if(is_null($customPath))
			$filepath = $this->landingFolder."files/".$filename;
		else
			$filepath = $customPath . "/" . $filename;

		move_uploaded_file( $uploadArray[ "tmp_name" ], $filepath );
		return "/".$this->landingRelPath."files/".$filename;
	}

	/**
	 * gather data for template processing
	 *
	 * @return array
	 * @author Özgür Köy
	 */
	public function prepare() {
		//TODO : add caching here

		$form   = $this->loadForm()->getFields( true );
		$fields = json_decode( $this->details, true );

		return array( "form" => $form, "fields" => $fields, "code" => $this->loadLayout()->code );
	}

	/**
	 * @param $code
	 * @return $this
	 * @author Özgür Köy
	 */
	public function loadByCode( $code, $owner=null ) {
		$f = array( "code" => $code );
		if(!is_null($owner))
			$f[ "cplCampaign_advertiser" ] = $owner;
		$this->load( $f );
		return $this;
	}

	/**
	 * get code for publisher
	 *
	 * @param Publisher $publisher
	 * @param null      $code
	 * @return CplCampaignUser
	 * @author Özgür Köy
	 */
	public function getCode( Publisher $publisher = null, $code = null ) {
		if ( is_null( $code ) )
			$code = JUtil::randomString( 4 );

		$cpl = new CplCampaignUser();
		if ( !is_null( $publisher ) ) {
			$cpl->load( array( "cplCampaignUser_publisher" => $publisher , "cplCampaign_cplCampaignUser" => $this ) );
		}

		if ( $cpl->gotValue !== true || is_null( $publisher ) ) {
			$cpl       = new CplCampaignUser();
			$cpl->code = $code;
			$cpl->save();
			$cpl->saveCplCampaign( $this );

			if ( !is_null( $publisher ) )
				$cpl->saveCplCampaignUser_publisher( $publisher );

		}

		return $cpl;
	}

	public static function cloneCampaign( $id, $newName, $newHash ) {
		global $__DP;
		$base = new CplCampaign( $id );
		$f    = $base->loadForm()->getFields();
		$oldCode = $base->code;
		$newCode   = JUtil::randomString( 12 );
		$new       = new CplCampaign();
		$new->code = $newCode;
		$new->save();
		$newId = $new->id;
		unset( $new );

		$fields = array();
		foreach ( $f as $f0 )
			$fields[ ] = CplLayout::norm( $f0[ "label" ] );


		//we now clone it.
		CplCampaign::createCampTable( $newCode, $fields );
		$base->getCode( null, $newHash );

		$l                    = strtoupper( substr( $newCode, 0, 1 ) );
		$lOld                 = strtoupper( substr( $oldCode, 0, 1 ) );

		$oldLandingFolder  = $__DP . CplCampaign::$landingsPath . "/" . $lOld . "/" . $oldCode . "/";
		$landingFolder     = $__DP . CplCampaign::$landingsPath . "/" . $l . "/" . $newCode . "/";

		mkdir( $landingFolder, 0775, true );
		mkdir( $landingFolder . "templateCache" );

		self::recurse_copy( $oldLandingFolder . "files", $landingFolder . "files" );

		$base->code                 = $newCode;
		$base->name                 = $newName;
		$base->id                   = $newId;
		$base->campaign_cplCampaign = null;
		$base->preHash              = $newHash;
//		$base->details              = preg_replace( "|".$lOld."\\/".$oldCode."|si", $l. "\\/" . $newCode, $base->details );
		$base->details              = str_replace( $lOld."\/".$oldCode, $l. "\/" . $newCode, $base->details );
		$base->save();

		return $base;
	}

	/**
	 * create campaign table
	 *
	 * @param $code
	 * @author Özgür Köy
	 */
	public static function createCampTable( $code, $fields=array() ) {
		//campaign records
		$table = "CPL_" . $code . "_cplCamp";
		if (DB_ENGINE == "MYSQL"){
			$sqls = array(
				"CREATE TABLE IF NOT EXISTS `{$table}` (
					 `id` int(11) NOT NULL AUTO_INCREMENT,
					`jdate` INT NULL ,
					`cplCamp_publisher` INT NULL ,
					`date` INT NULL,
					`state` INT NULL,
					`reason` INT NULL,
					`publicID` text NULL,
					`details` text NULL,
					`info` text NULL,
					`token` text NULL,
					`shortUrl` text NULL,
					`invalidReason` text NULL,
					`approved` float NULL,
					PRIMARY KEY (  `id` ) ,
					INDEX (  `date`,`cplCamp_publisher` )
					) ENGINE = INNODB;
				"
			);
		}
		elseif (DB_ENGINE == "PGSQL"){
			$dbConfig = JCache::read( 'JDSN.CAMP0' );
			$table = '"' . $dbConfig["schema"] . '"."' . $table . '"';
			$sqls = array(
					'CREATE TABLE IF NOT EXISTS ' . $table . ' (
						"id" Serial NOT NULL,
						"jdate" NUMERIC,
						"cplCamp_publisher" NUMERIC,
						"date" INT NULL,
						"state" SMALLINT DEFAULT 1::SMALLINT,
						"reason" SMALLINT DEFAULT 0::SMALLINT,
						"details" JSON,
						"publicID" CHARACTER VARYING( 40 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"token" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"shortUrl" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"invalidReason" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"info" JSON,
						"approved" SMALLINT DEFAULT 0::SMALLINT,
					 PRIMARY KEY ( "id" )
					 );',
					 'CREATE INDEX ON ' . $table.' ( "publicID" );',
					 'CREATE INDEX ON ' . $table.' ( "state" );',
					 'CREATE INDEX ON ' . $table.' ( "reason" );',
					 'CREATE INDEX ON ' . $table.' ( "token" );',
					 'CREATE INDEX ON ' . $table.' ( "shortUrl" );',
					 'CREATE INDEX ON ' . $table.' ( "jdate" );',
					 'CREATE INDEX ON ' . $table.' ( "cplCamp_publisher" );'
			);
			//fields index
			foreach ( $fields as $field )
				$sqls[] = 'CREATE INDEX ON ' . $table.' ( ("details"->>\''.$field.'\') );';
		}

		JPDO::executeQueryBatch( $sqls );

		//campaign views
		$table = "CPL_" . $code . "_cplViews";
		if (DB_ENGINE == "MYSQL"){
			$sqls = array(
					"CREATE TABLE IF NOT EXISTS `{$table}` (
					 `id` int(11) NOT NULL AUTO_INCREMENT,
					`jdate` INT NULL ,
					`cplCampView_publisher` INT NULL ,
					`date` INT NULL,
					`details` text NULL,
					`token` text NULL,
					`referrer` text NULL,
					`ip` text NULL,
					`country` text NULL,
					`cplCampaignUser` INT NULL,
					PRIMARY KEY (  `id` ) ,
					INDEX (  `date`,`cplCampView_publisher` )
					) ENGINE = INNODB;
					"
			);
		}
		elseif (DB_ENGINE == "PGSQL"){
			$dbConfig = JCache::read( 'JDSN.CAMP0' );
			$table = '"' . $dbConfig["schema"] . '"."' . $table . '"';
			$sqls = array(
					'CREATE TABLE IF NOT EXISTS ' . $table . ' (
						"id" Serial NOT NULL,
						"jdate" NUMERIC,
						"cplCampView_publisher" NUMERIC,
						"date" INT NULL,
						"cplCampaignUser" INT NULL,
						"token" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"referrer" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"ip" CHARACTER VARYING( 40 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"country" CHARACTER VARYING( 40 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"details" JSON,
					 PRIMARY KEY ( "id" )
					 );
					 ',
					 'CREATE INDEX ON ' . $table.' ( "cplCampView_publisher" );',
					 'CREATE INDEX ON ' . $table.' ( "token" );',
					 'CREATE INDEX ON ' . $table.' ( "referrer" );',
					 'CREATE INDEX ON ' . $table.' ( "country" );'
			);

		}

		JPDO::executeQueryBatch($sqls);

		//campaign history
		$table = "CPL_" . $code . "_cplHistory";
		if (DB_ENGINE == "MYSQL"){
			$sqls = array(
				"CREATE TABLE IF NOT EXISTS `{$table}` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`jdate` INT NULL ,
					`details` text NULL,
					`action` text NULL,
					`madeBy` INT NULL ,
					PRIMARY KEY (  `id` ) ,
					INDEX (  `date` )
					) ENGINE = INNODB;
					"
				);
		}
		elseif (DB_ENGINE == "PGSQL"){
			$dbConfig = JCache::read( 'JDSN.CAMP0' );
			$table = '"' . $dbConfig["schema"] . '"."' . $table . '"';
			$sqls = array(
					'CREATE TABLE IF NOT EXISTS ' . $table . ' (
						"id" Serial NOT NULL,
						"jdate" NUMERIC,
						"madeBy" NUMERIC,
						"action" TEXT COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"details" TEXT COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
					 PRIMARY KEY ( "id" )
					 ); '
			);
		}

		JPDO::executeQueryBatch($sqls);

		//campaign tokens
		$table = "CPL_" . $code . "_cplTokens";
		if (DB_ENGINE == "MYSQL"){
			$sqls = array(
				"CREATE TABLE IF NOT EXISTS `{$table}` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`jdate` INT NULL ,
					`expires` INT NULL ,
					`used` INT NULL DEFAULT 0,
					`token` text NULL,
					`viewToken` text NULL,
					PRIMARY KEY (  `id` ) ,
					INDEX (  `date`,`cplCampView_publisher` )
					) ENGINE = INNODB;
					"
			);
		}
		elseif (DB_ENGINE == "PGSQL"){
			$dbConfig = JCache::read( 'JDSN.CAMP0' );
			$table = '"' . $dbConfig["schema"] . '"."' . $table . '"';
			$sqls = array(
					'CREATE TABLE IF NOT EXISTS ' . $table . ' (
						"id" Serial NOT NULL,
						"jdate" NUMERIC,
						"expires" NUMERIC,
						"used" SMALLINT DEFAULT (0)::SMALLINT,
						"token" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"viewToken" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
					 PRIMARY KEY ( "id" )
					 );',
					 'CREATE INDEX ON ' . $table.' ( "token" );',
					 'CREATE INDEX ON ' . $table.' ( "viewToken" );',
					 'CREATE INDEX ON ' . $table.' ( "expires" );'
			);
		}

		JPDO::executeQueryBatch($sqls);

		//campaign ips
		$table = "CPL_" . $code . "_cplIps";

		if (DB_ENGINE == "MYSQL"){
			$sqls = array(
				"CREATE TABLE IF NOT EXISTS `{$table}` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`jdate` INT NULL ,
					`ip` text NULL ,
					PRIMARY KEY (  `id` ) ,
					INDEX (  `date`,`cplIps_publisher` )
				) ENGINE = INNODB; "
			);
		}
		elseif (DB_ENGINE == "PGSQL"){
			$dbConfig = JCache::read( 'JDSN.CAMP0' );
			$table = '"' . $dbConfig["schema"] . '"."' . $table . '"';
			$sqls = array(
				' CREATE TABLE IF NOT EXISTS ' . $table . ' (
						"id" Serial NOT NULL,
						"jdate" NUMERIC,
						"ip" CHARACTER VARYING( 40 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
					 PRIMARY KEY ( "id" )
				);
				',
				'CREATE INDEX ON ' . $table.' ( "ip" );',

			);
		}

		JPDO::executeQueryBatch($sqls);
	}

	/**
	 * check if campaign is active and good to go
	 *
	 * @return bool
	 * @author Özgür Köy
	 */
	public function getClickStatus() {
		if ( $this->state != CplCampaign::ENUM_STATE_STABILE ) {
			$this->loadCampaign();

			$startDate = strtotime( $this->campaign_cplCampaign->startDate );
			$endDate   = strtotime( $this->campaign_cplCampaign->endDate ) + ( 24 * 3600 );

			unset( $this->campaign_cplCampaign );

			if ( $startDate > time() )
				return CplCamp::ENUM_STATE_EARLY;
			elseif ( $endDate < time() || $this->state == CplCampaign::ENUM_STATE_ENDED )
				return CplCamp::ENUM_STATE_LATE;
			else
				return CplCamp::ENUM_STATE_PENDING;
		}

		//if ( $this->state == CplCampaign::ENUM_STATE_STABILE && $this->startDate < time() && $this->endDate > time() )
		//	return CplCamp::ENUM_STATE_APPROVED;

		return CplCamp::ENUM_STATE_PENDING;
	}

	/**
	 * save visit
	 *
	 * @param Publisher $publisher
	 * @param           $server
	 * @return string
	 * @author Özgür Köy
	 */
	public function saveVisit( &$server, $ccuId = null ) {
		$ip = "UNKNOWN";

		if ( !empty( $server[ 'HTTP_X_FORWARDED_FOR' ] ) ) {
			$ip = $server[ 'HTTP_X_FORWARDED_FOR' ];
		}
		elseif ( !empty( $server[ 'REMOTE_ADDR' ] ) ) {
			$ip = $server[ 'REMOTE_ADDR' ];
		}

		$cv = new CplCampView();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplViews" );
		$cv->cplCampaignUser = $ccuId;
		$cv->date            = time();
		$cv->referrer        = isset( $server[ "HTTP_REFERER" ] ) ? $server[ "HTTP_REFERER" ] : "";
		$cv->ip              = CplCampaign::getIp();
		$cv->country         = Ip2country::findCountry( $ip );
		$cv->details         = json_encode( $server );
		$cv->token           = $token = JUtil::randomString( 30 );
		$cv->save();

		unset( $cv );

		return $token;
	}


	/**
	 * @param Publisher $publisher
	 * @param           $server
	 * @param           $options
	 * @author Özgür Köy
	 */
	public function savePForm( Publisher $publisher, $server, $options, $overRideState = null, $invalidReason=null ) {
		$state = is_null( $overRideState ) ? $this->getClickStatus() : $overRideState;

		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );
		$cv->cplCamp_publisher = $publisher->id;
		$cv->publicID          = JUtil::randomString( 30 );
		$cv->info              = $server;
		$cv->state             = $state;
		$cv->reason            = $invalidReason;
		//$cv->date              = time();
		$cv->details           = json_encode( $options );
		$cv->info              = json_encode( $server );
		$cv->save();

		if ( $invalidReason == CplCamp::ENUM_REASON_GOOD ) {
			//good so save the ip.
			$cv = new CplCampIps();
			$cv->overrideTableName( "CPL_" . $this->code . "_cplIps" );
			$cv->ip                = CplCampaign::getIp();
			$cv->save();
		}
	}

	/**
	 * check if field is unique
	 *
	 * @param $field
	 * @param $value
	 * @return bool
	 * @author Özgür Köy
	 */
	public function checkFieldUnique( $field, $value ){
		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );

		// $cv->load( array("_custom"=>"`details`->>'$field' ILIKE '" . str_replace("'","",json_encode($value)) . "'") );
		$val0 = preg_replace("|[\"']|s","",json_encode($value));

		$cv->load( array("_custom"=>"`details`->>'$field' ILIKE '" . $val0 . "'") );
		$u = $cv->gotValue;
		unset( $cv );

		return !$u;
	}

	/**
	 * check field quality, 3 repeating characters and keyboard goodness
	 *
	 * @param $val
	 * @return bool
	 * @author Özgür Köy
	 */
	public function checkFieldQuality( $val ) {
		$val = preg_replace( "|[\t\n]|", "", $val );

		$val0 = explode( " ", $val );
		$bad  = "qwertyuiopasdfghjklzxcvbnm1234567890";
		$good = true;

		foreach ( $val0 as $val1 ) {
			$val2 = str_split( $val1, 4 );

			foreach ( $val2 as $val3 ) {
				if(!(strlen(trim($val3))>0))
					continue;
				elseif (
					preg_match( "|(.)\1{3,}|", $val ) > 0 ||
					( strpos( $bad, strtolower( $val3 ) ) !== false && strlen( $val3 ) > 2 ) ||
					strlen( $val3 ) > 25
				) {
					$good = false;
					break;
				}
			}

		}

		return $good;

	}

	/**
	 * approve lead
	 *
	 * @param        $id
	 * @param string $madeBy
	 * @param bool   $override
	 * @return bool
	 * @author Özgür Köy
	 */
	public function approveLead( $id, $madeBy = CplCampHistory::ENUM_MADEBY_USER, $override = true ) {
		if($this->state != CplCampaign::ENUM_STATE_STABILE)
			return false;

		JPDO::beginTransaction();

		try {
			$cv = new CplCamp();
			$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );
			$cv->load( array( "publicID" => $id ) );

			if ( $cv->state == CplCamp::ENUM_STATE_PENDING || $override ) {
				$cv->date = time();
				$cv->state = CplCamp::ENUM_STATE_APPROVED;
				$cv->save();
			}

			unset( $cv );

			if( $madeBy == CplCampHistory::ENUM_MADEBY_USER ){
				$this->currentOverlappingNegative = 0 ;
				$this->save();
			}

			$this->checkCompletitionStatus();
			CplCampHistory::saveHistory( $this->code, "approved lead", $madeBy );
			JPDO::commit();
		}
		catch ( JError $j ) {
			JPDO::rollback();
			return false;
		}

		return true;
	}

	/**
	 * deny lead
	 *
	 * @param        $id
	 * @param string $reason
	 * @param string $madeBy
	 * @param bool   $override
	 * @return bool|int
	 * @author Özgür Köy
	 */
	public function denyLead( $id, $reason = CplCamp::ENUM_REASON_BAD_DATA, $madeBy = CplCampHistory::ENUM_MADEBY_USER, $override = true ) {
		if($this->state != CplCampaign::ENUM_STATE_STABILE)
			return false;

		JPDO::beginTransaction();
		try {
			$cv = new CplCamp();
			$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );
			$cv->load( array( "publicID" => $id ) );

			if(!array_key_exists(strval($reason), $cv->reason_options))
				$reason = CplCamp::ENUM_REASON_BAD_DATA;

			if ( $cv->state == CplCamp::ENUM_STATE_PENDING || $override ) {
				$cv->date   = time();
				$cv->state  = CplCamp::ENUM_STATE_INVALID;
				$cv->reason = $reason;
				$cv->save();
			}

			if( $madeBy == CplCampHistory::ENUM_MADEBY_USER ){
				$this->currentOverlappingNegative = intval( $this->currentOverlappingNegative ) + 1;
				$this->save();
			}

			//TODO : add here too?
			CplBadEntry::addString( $cv->details );
			CplCampHistory::saveHistory( $this->code, "denied lead", $madeBy );
			unset( $cv );

			JPDO::commit();
		}
		catch ( JError $j ) {
			JPDO::rollback();
			return false;
		}

		if(( $madeBy == CplCampHistory::ENUM_MADEBY_USER ) &&
			( $this->maxOverlappingNegative > 0 ) &&
			( $this->currentOverlappingNegative > $this->maxOverlappingNegative )
		){
			$this->block();
			return CplCampaign::TOO_MANY_NEGATIVES;
		}
		else
			return true;

	}

	public function countNumbers( $only=array() ) {
		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );

		if(sizeof($only)>0)
			$only = array( "state" => $only );

		$cv->multipleGroupOperation(true)
			->nopop()
			->groupBy( array(array("state","state")) )
			->count( "id", "cid" )->load( $only );

		$r = array();

		while($cv->populate())
			$r[ strtolower($cv->state_options[ $cv->state ]) ] = array($cv->cid, $cv->state);

		return $r;
	}

	/**
	 * get list of pending leads
	 *
	 * @param int $per
	 * @return CplCamp
	 * @author Özgür Köy
	 */
	public static function listLeads( $code, $per = 3, $type = 1, $startId=null ) {
		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $code . "_cplCamp" );
		$cv->orderBy( "jdate ASC" )->limit( 0, $per );

		if($per == CplCampaign::LOAD_ALL_VALUES)
			$cv->populateOnce( false );

		$f = array( "state" => ( $type ) );
		if(!is_null($startId))
			$f[ "id" ] = "&gt " . $startId;
		$cv->load( $f );
		return $cv;
	}

	/**
	 * @return string
	 * @author Özgür Köy
	 */
	public function checkCompletitionStatus() {
		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );

		if ( $this->state == CplCampaign::ENUM_STATE_STABILE ) {
			if ( $cv->approvedLeadCount() >= $this->maxLeadLimit ) {
				CplCampHistory::saveHistory( $this->code, "campaignFinished", CplCampHistory::ENUM_MADEBY_SYSTEM );
				Campaign::endCampaign( $this->loadCampaign()->id );
			}
		}
		unset( $cv );

		return $this->state;
	}

	public function getLeadCounts( $publisherId = null ) {
		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );

		$ret = array(
			"valid"   => $cv->approvedLeadCount( $publisherId ),
			"pending" => $cv->pendingLeadCount( $publisherId ),
			"total"   => $cv->totalLeadCount( $publisherId ) );
		unset( $cv );

		return $ret;
	}

	public function getViewCount() {
		$cv = new CplCampView();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplViews" );
		$cv->count( "id", "cid" );
		$ret = intval($cv->cid);

		unset( $cv );

		return $ret;
	}

	/**
	 * @author Özgür Köy
	 */
	public function campaignStart() {
		$this->state = CplCampaign::ENUM_STATE_STABILE;
		$this->save();

		CplCampHistory::saveHistory( $this->code, "campaign started", CplCampHistory::ENUM_MADEBY_SYSTEM );
	}

	/**
	 * @author Özgür Köy
	 */
	public function campaignComplete( &$leads ) {
		$this->state = CplCampaign::ENUM_STATE_ENDED;
		$this->save(); // we make it here so that it wont happen again.

		$this->loadCampaign();

		$p = Backend::createSupervisionTask( "finalize CPL Campaign #".$this->campaign_cplCampaign->id, "finalizeCPLCampaign" );
		if($p !== false){
			$fin = $p->addChildTask(
				"finalization",
				"finalizeCPLCampaign",
				array('cplCampaign'=>$this,'leads'=>$leads)
			);

			if($fin !== false){
				$fin->addChildTask(
					"sending emails:" . $this->campaign_cplCampaign->id . " name:" . $this->campaign_cplCampaign->title,
					"sendCampaignFinishingMails",
					array( "campaignId" => $this->campaign_cplCampaign->id )
				);
			}

			$p->runTask();
		}
	}

	/**
	 * payment to publishers
	 *
	 * @param Campaign $campaign
	 * @author Özgür Köy
	 */
	public function payToPublishers( Campaign &$campaign ) {
		//load users
		$users = $this->loadCplCampaign_cplCampaignUser( array( "isActive" => 1, "cplCampaignUser_publisher"=>array("id"=>"IS NOT NULL") ), 0, 50, null, false );
		if ( $users->gotValue ) {
			do {
				$publisher  = $users->loadCplCampaignUser_publisher();
				$revenue    = $publisher->loadPublisher_publisherRevenue(
					array(
						"campaign_publisherRevenue"=>$campaign->id,
						"publisher_publisherRevenue"=>$publisher->id
					)
				);
				//get valid leads
				$leads   = $this->getLeadCounts( $publisher->id );
				print_r( $leads );
				$actives = $leads[ "valid" ];
				echo "active (".$publisher->id.") :" . $actives.PHP_EOL;


				echo $revenue->amount = $this->perLead * $actives;
				echo PHP_EOL;
				$revenue->status = PublisherRevenue::ENUM_STATUS_APPROVED;
				$revenue->approveDate = time();
				$revenue->save();
			} while ( $users->populate() );
		}
	}

	/**
	 * refunds for cpl campaign
	 *
	 * @param Campaign $campaign
	 * @param          $leads
	 * @author Özgür Köy
	 */
	public function checkRefunds( Campaign &$campaign, &$leads ){
		if(!is_object($campaign->campaign_cplCampaign))
			$campaign->loadCplCampaign();

		if ( $campaign->campaign_cplCampaign->maxLeadLimit > $leads["valid"] ) {
			$refundCount  = $campaign->campaign_cplCampaign->maxLeadLimit - $leads["valid"];
			$refundAmount = ( $refundCount * $campaign->campaign_cplCampaign->perLead * $campaign->multiplier );

			Tool::echoLog( "Refunded lead count : " . $refundCount );
			AdvertiserMoneyAction::create(
				$campaign->advertiser,
				$campaign->id,
				$refundAmount,
				'cpl campaign refund: ' . $campaign->title,
				null
			);

			$campaign->writeHistory(
				CampaignHistory::ENUM_ACTIONID_FRAUD_CONTROL,
				"Refunded unused lead value amount: " . $refundAmount,
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				$refundAmount
			);
		}
		else {
			Tool::echoLog( "No leads to refund" );

			$campaign->writeHistory(
				CampaignHistory::ENUM_ACTIONID_FRAUD_CONTROL,
				"No refund needed for CPL campaign",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);
		}
	}

	/**
	 * invalidate pending leads
	 *
	 * @author Özgür Köy
	 */
	public function invalidatePendingLeads() {
		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );
		$cv->count( "id", "cid", array( "state" => CplCamp::ENUM_STATE_PENDING ) );
		$count = $cv->cid;

		Tool::echoLog( "Invalidated lead count: " . $count );
		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );
		$cv->update(
			array("state"=>CplCamp::ENUM_STATE_PENDING),
			array(
				"state"=>CplCamp::ENUM_STATE_INVALID,
			    "reason"=>CplCamp::ENUM_REASON_CAMPAIGN_ENDED
			)
		);

		if(!is_object($this->campaign_cplCampaign))
			$this->loadCampaign();

		$this->campaign_cplCampaign->writeHistory(
			CampaignHistory::ENUM_ACTIONID_CAMPAIGN_UPDATE,
			$count." leads invalidated due to campaign end",
			CampaignHistory::ENUM_MADEBY_SYSTEM,
			$count
		);
	}

	/**
	 * validate form response
	 *
	 * @param $code
	 * @param $viewToken
	 * @param $xtoken
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validateResponse( $code, $viewToken, $xtoken, $useToken = false ) {
		$c0           = CplCampaignUser::loadCampaignByCode( $code, CplCampaign::IGNORE_PUBLISHER );
		$c            = $c0[ "campaign" ];
		$campaignCode = $c->code;

		$t = new CplCampToken();
		$t->overrideTableName( "CPL_" . $campaignCode . "_cplTokens" );

		$t->load(
			array(
				"used"      => "&ne 1",
				"viewToken" => $viewToken,
				"token"     => $xtoken,
				"expires"   => "&lt " . ( time() + CplCampToken::$tokenLife )
			)
		);

		if ( $t->gotValue ) {
			if ( $useToken ) {
				//valid response so we ll be checking for ip limit
				$t->used = 1;
				$t->save();

				if( $c->checkIpLimit() === true ){
					CplCampHistory::saveHistory( $campaignCode, "token valid & used. id:" . $t->id, CplCampHistory::ENUM_MADEBY_USER );
				}
				else{
					CplCampHistory::saveHistory( $campaignCode, "token valid but ip limit active" . $t->id, CplCampHistory::ENUM_MADEBY_USER );

					return CplCamp::ENUM_REASON_IP_LIMIT;
				}
			}

			unset( $c, $c0 );

			return true;
		}
		else {
			CplCampHistory::saveHistory( $campaignCode, "token invalid token:{$viewToken} xtoken:{$xtoken} code:{$code}", CplCampHistory::ENUM_MADEBY_USER );
			unset( $c, $c0 );

			return false;
		}
	}

	/**
	 * ip limit control
	 *
	 * @return bool
	 * @author Özgür Köy
	 */
	public function checkIpLimit() {
		$cv = new CplCampIps();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplIps" );
		$cv->count(
			"id",
			"cid",
			array( "ip" => CplCampaign::getIp(), "jdate" => "&ge " . (time() - 3600) )
		);

		$mcid = $cv->cid;

		unset( $cv );

		if ( $mcid > $this->maxPerHourPerIp )
			return false;
		else
			return true;
	}

	/**
	 * receive form data
	 *
	 * @param $code
	 * @param $post
	 * @return array|int
	 * @author Özgür Köy
	 */
	public static function receiveSubmission( $code, &$post ) {
		$c0 = CplCampaignUser::loadCampaignByCode( $code );
		$c  = $c0[ "campaign" ];
		$p  = $c0[ "publisher" ];

		$values  = $c->prepare();
		$errors  = array();
		$details = array();

		if (
			CplCampaign::validateResponse( $post[ "code" ], $post[ "token" ], $post[ "xtoken" ] ) === true &&
			isset( $values[ "form" ] ) &&
			sizeof( $values[ "form" ] ) > 0
		)
			{
			$state         = CplCamp::ENUM_STATE_PENDING;
			$invalidReason = CplCamp::ENUM_REASON_GOOD;
			$gotError      = false;


			if ( ( $nstate = self::isCampaignRunning( null, $c ) ) !== true ) {
				switch ( $nstate ) {
					case CplCampaign::CAMPAIGN_NOT_FOUND:
						$state         = CplCamp::ENUM_STATE_INVALID;
						$invalidReason = CplCamp::ENUM_REASON_SYSTEM_CHOICE;
						break;

					case CplCampaign::CAMPAIGN_NOT_STARTED:
						$state         = CplCamp::ENUM_STATE_EARLY;
						$invalidReason = CplCamp::ENUM_REASON_CAMPAIGN_NOT_STARTED;
						break;

					case CplCampaign::CAMPAIGN_NOT_ACTIVE:
						$state         = CplCamp::ENUM_STATE_LATE;
						$invalidReason = CplCamp::ENUM_REASON_CAMPAIGN_ENDED;
						break;

					default:
						//nothing to set , move on . too many etc is not related here.
						break;
				};
			}


			if ( in_array( $state, array( CplCamp::ENUM_STATE_PENDING, CplCamp::ENUM_STATE_LATE ) ) ) {
				foreach ( $values[ "form" ] as $ff ) {
					$la        = CplLayout::norm( $ff[ "label" ] );
					$options   = $ff[ "options" ];
					//might
					if ( !isset( $post[ $la ] ) ) {
//						if(is_array($options))
							$post[ $la ] = "";

					}

					$fieldType = intval( is_array( $post[ $la ] ) );
					$pval      = (array) $post[ $la ];
					$direkt    = false;

					//test the values
					if ( !is_array( $options ) && $fieldType == self::FIELD_ARRAY ) {
						//it can't be an array
						$pval = array(reset( $pval ));
					}
					elseif ( is_array( $options ) ) {

						//we must validate multichoices
						if ( $fieldType == self::FIELD_ARRAY ) {
							$pval0 = array();
							foreach ( $pval as $ppv ) {
								$ppv = trim( $ppv );
								if ( in_array( $ppv, $options ) )
									$pval0[ ] = $ppv;
							}
							if(sizeof($pval0)==0)
								$pval0 = array( "" );
							$pval = $pval0;
							$direkt = true;
						}
						elseif ( $fieldType == self::FIELD_TEXT ) {
							$pval0 = trim(reset($pval));

							//not a valid option
							if ( !in_array( $pval0, $options ) )
								$pval = array("");

							unset( $pval0 );
						}


					}

					$pval = implode( ", ", $pval );


					if (
						!isset( $pval ) ||
						(
							$ff[ "isMandatory" ] == 1 &&
							!(
								( $fieldType === 0 && strlen( trim( $pval ) ) > 0 )
								||
								( $fieldType === 1 && sizeof( $pval ) > 0 )
							)
						)
					) {
						$errors[ $la ] = "m";
					}
					elseif ( $direkt == true ) {
						$details[ $la ] = $pval;
					}
					else {
						$pval = substr( trim( $pval ), 0, self::$maxFieldLength );
						$pval = $pval === false ? "" : $pval;

						//good?
						//check for regex if exists
						if ( strlen( $ff[ "regex" ] ) > 0 && preg_match( "|" . $ff[ "regex" ] . "|s", $pval, $matches ) != 1 ) {
							$gotError = "i";
						}

						//check uniqueness
						if ( $ff[ "isUnique" ] == 1 && $c->checkFieldUnique( $la, $pval ) != true ) {
							$invalidReason = CplCamp::ENUM_REASON_NOT_UNIQUE;
							$state         = CplCamp::ENUM_STATE_INVALID;
						}

						//dictionary
						if ( CplBadWords::checkWordExists( $pval ) === true ) {
							$invalidReason = CplCamp::ENUM_REASON_DICTIONARY;
							$state         = CplCamp::ENUM_STATE_INVALID;
						}

						//il qualite
						if ( $c->checkFieldQuality( $pval ) !== true ) {
							$invalidReason = CplCamp::ENUM_REASON_FIELD_QUALITY;
							$state         = CplCamp::ENUM_STATE_INVALID;
						}

						if ( $gotError !== false )
							$errors[ $la ] = $gotError;
						else
							$details[ $la ] = $pval;
					}
				}
			}


			if ( sizeof( $errors ) == 0 && sizeof( $details ) > 0 ) {
				//respond good.
				if ( ( $newReason = CplCampaign::validateResponse( $post[ "code" ], $post[ "token" ], $post[ "xtoken" ], CplCampaign::USE_TOKEN ) ) !== true ) {
					//might have been ip limited
					$state         = CplCamp::ENUM_STATE_INVALID;
					$invalidReason = $newReason;
				}
				$c->savePForm( $p, $_SERVER, $details, $state, $invalidReason );

				unset( $c, $p );

				return self::GOOD_FORM;
			}
			else {

				unset( $c, $p );

				return $errors;
			}
		}
		else {
			unset( $c, $p );

			return self::ERROR_IN_FORM;
		}

	}

	/**
	 * lead stats
	 *
	 * @param null $publisherId
	 * @return array
	 * @author Özgür Köy
	 */
	public function getLeadStats( $publisherId = null ) {
		//filtering
		if ( !is_null( $publisherId ) )
			$filter = array( "cplCamp_publisher" => $publisherId );
		else
			$filter = null;


		$cv = new CplCamp();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplCamp" );
		$cv->multipleGroupOperation( true )
			->count( "id", "cid", $filter )
			->groupBy( array( array( "to_char(to_timestamp(`jdate`), 'YYYY-MM-DD')", "timos" ), array( "state", "state" ) ) )
			->rawOrderBy( "to_char(to_timestamp(`jdate`), 'YYYY-MM-DD') ASC" )
			->nopop()
			->load();
		$ret = array( "valid" => 0, "pending" => 0, "total" => 0 );
		while ( $cv->populate() ) {
			if ( !isset( $ret[ $cv->timos ] ) )
				$ret[ $cv->timos ] = array( "total" => 0, "valid" => 0, "pending" => 0, "invalid" => 0 );

			if ( $cv->state == CplCamp::ENUM_STATE_APPROVED )
				$ret[ $cv->timos ][ "valid" ] += $cv->cid;
			elseif ( $cv->state == CplCamp::ENUM_STATE_PENDING )
				$ret[ $cv->timos ][ "pending" ] += $cv->cid;
			else
				$ret[ $cv->timos ][ "invalid" ] += $cv->cid;

			$ret[ $cv->timos ][ "total" ] += $cv->cid;
		}

		unset( $cv );

		return $ret;
	}

	public function getViewStats() {
		$cv = new CplCampView();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplViews" );

		$cv->multipleGroupOperation( true )
			->count("id","cid")
			->groupBy( array( array("to_char(to_timestamp(`jdate`), 'YYYY-MM-DD')","timos") ) )
			->rawOrderBy("to_char(to_timestamp(`jdate`), 'YYYY-MM-DD') ASC")
			->nopop()
			->load();
		$ret = array();
		while($cv->populate()){
			$ret[ $cv->timos ] = $cv->cid;
		}

		unset( $cv );

		return $ret;
	}

	public function getReferrerStats( $trimDomains = true ) {
		$cv = new CplCampView();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplViews" );

		$cv->multipleGroupOperation( true )
			->count("id","cid")
			->groupBy( array( array("referrer","refz") ) )
			->rawOrderBy("cid DESC")
			->nopop()
			->load();

		$ret = array();
		while($cv->populate()){
			if ( $trimDomains ) {
				$cv->refz = explode( "/", str_replace( "http://", "", strtolower( $cv->refz ) ) );
				$cv->refz = array_shift( $cv->refz );
			}
			$ret[ $cv->refz ] = $cv->cid;
		}

		unset( $cv );

		return $ret;
	}

	public function getCountryStats( ) {
		$cv = new CplCampView();
		$cv->overrideTableName( "CPL_" . $this->code . "_cplViews" );

		$cv->multipleGroupOperation( true )
			->count("id","cid")
			->groupBy( array( array("country","cnt") ) )
			->rawOrderBy("cid DESC")
			->nopop()
			->load();

		$ret = array();
		while($cv->populate()){
			$ret[ $cv->cnt ] = $cv->cid;
		}

		unset( $cv );

		return $ret;
	}

	public function clearCPLCampaignRelations( ) {
//		$this->deleteAllCplCampaign_cplCampaignUser();
		$dbConfig = JCache::read( 'JDSN.MDB' );
		JPDO::executeQuery( 'DELETE FROM "' . $dbConfig[ "schema" ] . '"."cplCampaignUser" WHERE "cplCampaignUser_publisher" IS NOT NULL AND "cplCampaign_cplCampaignUser" =' . $this->id );

		$c = new Campaign();
		$c->update( array( "campaign_cplCampaign" => $this->id ), array( "campaign_cplCampaign" => null ) );

		$dbConfig = JCache::read( 'JDSN.CAMP0' );
		JPDO::connect(	$dbConfig );

		JPDO::executeQuery( 'DELETE FROM "'.$dbConfig["schema"].'"."CPL_' . $this->code . '_cplViews";' );
		JPDO::executeQuery( 'DELETE FROM "'.$dbConfig["schema"].'"."CPL_' . $this->code . '_cplCamp";' );
		JPDO::executeQuery( 'DELETE FROM "'.$dbConfig["schema"].'"."CPL_' . $this->code . '_cplTokens";' );
		JPDO::executeQuery( 'DELETE FROM "'.$dbConfig["schema"].'"."CPL_' . $this->code . '_cplHistory";' );
	}

	public static function getIp(){
		if ( !empty( $server[ 'HTTP_X_FORWARDED_FOR' ] ) ) {
			$ip = $server[ 'HTTP_X_FORWARDED_FOR' ];
		}
		elseif ( !empty( $server[ 'REMOTE_ADDR' ] ) ) {
			$ip = $server[ 'REMOTE_ADDR' ];
		}
		else
			$ip = null;
//		$ip = '78.186.62.81';
		return $ip;
	}

	public function unblock( Campaign &$campaign ) {
		$this->currentOverlappingNegative = 0;
		$this->save();

		$campaign->writeHistory(
			CampaignHistory::ENUM_ACTIONID_CAMPAIGN_UPDATE,
			"CPL Validation unblocked",
			CampaignHistory::ENUM_MADEBY_ADMIN,
			$this->id
		);

		$campaign->loadAdvertiser()->loadUser()->sendSysMessage(
			"V2_cplCampaignUnblocked",
			array( "campName" => $campaign->title ),
			array( "name" => $campaign->advertiser->companyName, "campName"=>$campaign->title )
		);
	}

	public function block() {
		if(!is_object($this->campaign_cplCampaign))
			$this->loadCampaign();

		$this->campaign_cplCampaign->writeHistory(
			CampaignHistory::ENUM_ACTIONID_CAMPAIGN_UPDATE,
			"CPL Validation blocked, too many overlapping negatives",
			CampaignHistory::ENUM_MADEBY_SYSTEM,
			$this->id
		);

		$this->campaign_cplCampaign->loadAdvertiser()->loadUser()->sendSysMessage(
			"V2_cplCampaignBlocked",
			array( "campName" => $this->campaign_cplCampaign->title ),
			array( "name" => $this->campaign_cplCampaign->advertiser->companyName, "campName"=>$this->campaign_cplCampaign->title )
		);

		DSNotification::addNotification(
			$this->campaign_cplCampaign,
			"Advertiser locked approval",
			"Current status : {$this->currentOverlappingNegative}/{$this->maxOverlappingNegative}",
			DSNotification::ENUM_TYPE_CHECKOUT,
			"ds_campaignCplReview/".$this->campaign_cplCampaign->id
		);
	}

	/**
	 * get short url for cpl campaigns
	 *
	 * @param $publisherId
	 * @param $campaignID
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function getShortURL( $publisherId, $campaignID ) {
		$c = new Campaign();
		$c->load( array( "publicID" => $campaignID ) );

		if($c->gotValue){
			$c->loadCplCampaign();
			$c->campaign_cplCampaign->loadCplCampaign_cplCampaignUser(array("cplCampaignUser_publisher"=>$publisherId));
			$cu = &$c->campaign_cplCampaign->cplCampaign_cplCampaignUser;

			if (
				$cu &&
				$cu->gotValue
			) {
				if ( strlen( $cu->shortUrl ) > 0 )
					return $cu->shortUrl;

//				$shortUrl     = CplCampaignUser::getShortLink( $c->campaign_cplCampaign->preHash . "/" . $cu->code );
				$shortUrl     = Campaign::getRedLink(
					$campaignID,
					self::getCplBaseUrl( $c->campaign_cplCampaign->preHash . "/" . $cu->code ),
					null,
					array(
						"title"       => $c->title,
						"description" => $c->campaign_cplCampaign->campDetail,
						"image"       => $c->getPreviewImageLink(),
						"shortUrl"    => $c->campaign_cplCampaign->SLDomain
					)
				);

				$cu->shortUrl = $shortUrl;
				$cu->save();

				if ( $shortUrl !== false )
					return $shortUrl;
			}
		}
		JLog::log( "cri", "Something went wrong with get cpl short url" );

		return false;
	}

	public static function getCplBaseUrl( $hash ) {
		return Setting::getValue( "CPL_LANDING_DOMAIN" ).(substr(Setting::getValue( "CPL_LANDING_DOMAIN" ),-1)=="/"?"":"/").$hash;
	}

	public static function recurse_copy($src,$dst) {
		$dir = opendir($src);
		@mkdir($dst);
		while(false !== ( $file = readdir($dir)) ) {
			if (( $file != '.' ) && ( $file != '..' )) {
				if ( is_dir($src . '/' . $file) ) {
					self::recurse_copy($src . '/' . $file,$dst . '/' . $file);
				}
				else {
					copy($src . '/' . $file,$dst . '/' . $file);
				}
			}
		}
		closedir($dir);
	}


	/**
	 * Create copy of preview image for campaign copy
	 *
	 * @return array
	 * @author Murat
	 */
	public function copyPreviewImage(){
		global $__DP;
		$uploadPath = "site/layout/images/campaign/";

		// image name
		$name = "_temp_" . JUtil::randomString(20) . "." . substr($this->previewFileName,-3);

		// location to save cropped image
		$url = $__DP.$uploadPath.$name;

		copy($uploadPath.$this->previewFileName,$url);

		$filesize = filesize( $url );

		//$fileName = $name . substr($url,-3);
		return array(
			"fullPath" => $url,
			"size"=>$filesize,
			"fileName" => $name
		);
	}

}