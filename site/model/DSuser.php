<?php
/**
 * dsuser real class - firstly generated on 22-11-2013 14:28, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/DSuser.php';

class DSuser extends DSuser_base
{
	public $tagz  = null;

	public $permissions = array();

	public $authLevel = array();

	public $userRoles = array();

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct( $id = null ) {
		parent::__construct( $id );
	}

	public function login( $username, $password ) {
		$this->load(
			array(
				"username" => $username,
				"password" => md5( $password ))
		);

		return $this->gotValue;
	}

	//	TODO:cache role permissions on db
	public function loadSettings() {
		/*roles*/
		$roles = $this->loadRoles();
		if ( $roles ) {
			do {
				$this->userRoles[ ] = $roles->id;

				if ( !in_array($roles->authLevel,$this->authLevel)  )
					$this->authLevel[] = $roles->authLevel;

				$perms = $roles->loadPermissions();
				if ( $perms && $perms->gotValue ) {
					do {
						$action = $perms->loadAction();
						if(!$action || $action->gotValue===false)
							continue;

						if ( !isset( $this->permissions[ $action->label ] ) )
							$this->permissions[ $action->label ] = 0;
						$this->permissions[ $action->label ] |= $perms->bits;

					} while ( $perms->populate() );
				}

			} while ( $roles->populate() );
		}


	}

	public static function record($userId, $action){

		$ipAddress = $_SERVER['REMOTE_ADDR'];
		if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
			$ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
		}

		$h = new DSuserHistory();
		$h->action = $action."POST:".print_r($_POST,true)."|GET:".print_r($_GET,true);
		$h->ip = $ipAddress;
		$h->save();
//		JPDO::$debugQuery = true;
		$h->saveDSuser( $userId );
//exit;
		unset( $h );
	}

	public static function loadByEmail( $email ) {
		$d = new DSuser();
		$d->load(array("email"=>"LIKE ".$email));
		if ( $d->gotValue === false ) {
			$d->load( array( "_custom" => "`aliasEmails`||'
' SIMILAR TO '%" . $email . "\n?\r?%'" ) );
			var_dump( $d->gotValue );
		}
		return $d->gotValue?$d:false;
	}

	public static function loadByUsername( $username ) {
		$d = new DSuser();
		$d->load(array("username"=>"LIKE ".$username));

		return $d->gotValue?$d:false;
	}

	/**
	 * @param array $filter
	 * @return DSuser
	 * @author Özgür Köy
	 */
	public static function loadRootUsers($filter=array()) {
		$d = new DSuser();
		$filter["isRoot"] = 1;
		$d->load($filter);
		return $d;
	}

	public function collectTags( $returnArray=false) {
		$b = $this->loadUserTags();
		$t = array();
		if($b && $b->gotValue){
			do {
				$t[ ] = $b->name;
			} while ( $b->populate() );

		}
		if($returnArray!==false)
			return $t;

		$this->tagz = implode( ", ", $t );
	}
}

