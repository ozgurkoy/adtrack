<?php
/**
 * cplBadEntry real class - firstly generated on 29-05-2014 11:21, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplBadEntry.php';

class CplBadEntry extends CplBadEntry_base
{

	public static $whiteList = array("gmail.com","yahoo.com","hotmail.com","live.com");

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function addString( $details ) {
		$details = json_decode( $details );

		foreach ( $details as $dval ) {
			$dval = preg_replace( "|[\s\t\n\r@]|", " ", $dval );
			$s    = explode( " ", $dval );
			foreach ( $s as $s0 ) {
				$s0 = strtolower( trim( $s0 ) );
				if ( !( strlen( $s0 ) > 2 ) || in_array( $s0, CplBadEntry::$whiteList ) )
					continue;

				$b               = new CplBadEntry();
				$b->string       = $s0;
				JPDO::$hideError = true;
				$b->save();
				JPDO::$hideError = false;
			}
		}
	}

	public function addToDictionary() {
		if ( $this->gotValue !== true ) {
			return false;
		}

		JPDO::$hideError=true;
		$b         = new CplBadWords();
		$b->string = $this->string;
		$b->save();

		JPDO::$hideError=false;
		unset( $b );

		$this->filtered = 1;
		$this->save();
	}

	public function ignoreWord() {
		if ( $this->gotValue !== true ) {
			return false;
		}

		$this->filtered = 2;
		$this->save();
	}

}

