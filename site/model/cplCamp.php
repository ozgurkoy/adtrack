<?php
/**
 * cplCamp real class - firstly generated on 04-04-2014 12:04, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplCamp.php';

class CplCamp extends CplCamp_base
{

	/*TODO: SINGLE METHOD FOR ALL STATS */
	public $code = null;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function approvedLeadCount( $publisherId = null ) {
		$this->resetObject();
//		JPDO::debugQuery();
		$this->count(
			"id",
			"cid",
			array( "state" => CplCamp::ENUM_STATE_APPROVED, "cplCamp_publisher" => $publisherId )
		);

//		JPDO::debugQuery( false );

		if ( $this->cid )
			return $this->cid;

		return 0;
	}

	public function pendingLeadCount( $publisherId = null ) {
		$this->resetObject();
		$this->count( "id", "cid", array( "state" => CplCamp::ENUM_STATE_PENDING, "cplCamp_publisher" => $publisherId ) );

		if ( $this->cid )
			return $this->cid;

		return 0;
	}

	public function totalLeadCount( $publisherId = null ) {
		$this->resetObject();
		$filter = array();
		if ( !is_null( $publisherId ) )
			$filter = array( "cplCamp_publisher" => $publisherId );

		$this->count( "id", "cid", $filter );

		if ( $this->cid )
			return $this->cid;

		return 0;
	}


}

