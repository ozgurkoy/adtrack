<?php
/**
 * category real class - firstly generated on 27-10-2014 10:57, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/category.php';

class Category extends Category_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function loadByName( $name ) {
		$c = new Category();
		$c->load( array( "name" => "LIKE " . trim($name) ) );

		return $c->gotValue ? $c : false;

	}
}

