<?php
/**
 * failedRMQJobs real class - firstly generated on 02-01-2014 15:06, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/FailedRMQJobs.php';

class FailedRMQJobs extends FailedRMQJobs_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Create FailedRMQJob
	 *
	 * @param string $msg
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function create($messageHash,$msg){
		$frmqj = new FailedRMQJobs();
		$frmqj->load(array("messageHash"=>$messageHash));
		if (!$frmqj->gotValue){
			$frmqj->messageHash = $messageHash;
			$frmqj->message = $msg;
			$frmqj->failCount = 0;
		} else {
			$frmqj->failCount++;
		}
		$frmqj->save();
	}

}

