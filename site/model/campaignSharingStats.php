<?php
/**
 * campaignSharingStats real class - firstly generated on 12-02-2014 00:00, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignSharingStats.php';

class CampaignSharingStats extends CampaignSharingStats_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function log($campaignID,$publisherID,$type,$msgId,$msg){
		$m = new CampaignSharingStats();
		$m->campaignSharingStats_campaign = $campaignID;
		$m->campaignSharingStats_publisher = $publisherID;
		$m->shareType = $type;
		$m->shareMessageID = $msgId;
		$m->sharedMessage = $msg;
		$m->save();

		unset($m);
	}

}

