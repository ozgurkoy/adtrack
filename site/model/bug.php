<?php
/**
 * bug real class - firstly generated on 26-10-2014 20:28, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/bug.php';

class Bug extends Bug_base
{
	public $tagz  = null;

	private $mailActions = array(
		"CLOSE",
		"ASSIGN"
	);
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	public static function create( array $params ) {
		JPDO::beginTransaction();
		try {
			$params[ "operation" ] = BugHistory::ENUM_OPERATION_OPEN;
			$u                     = new DSuser( $params[ "user" ] );
			$projects              = $u->loadProjects();

			if ( $u->gotValue && sizeof( $projects->_ids ) == 1 )
				$params[ "project" ] = $projects->id;

			unset( $u, $projects );

			$b               = new Bug();
			$b->mailThreadId = isset( $params[ "threadId" ] ) ? $params[ "threadId" ] : "";
			$b->title        = $params[ "title" ];
			$b->priority     = isset( $params[ "priority" ] ) && array_key_exists( $params[ "priority" ], $b->priority_options ) ? $params[ "priority" ] : Bug::ENUM_PRIORITY_LOW;
			$b->startDate    = isset( $params[ "startDate" ] ) ? $params[ "startDate" ] : null;
			$b->bugType      = isset( $params[ "bugType" ] ) && array_key_exists( $params[ "bugType" ], $b->bugType_options ) ? $params[ "bugType" ] : Bug::ENUM_BUGTYPE_QUESTION;
			$b->link         = $params[ "link" ];
			$b->status       = isset( $params[ "assignee" ] ) && $params[ "assignee" ] > 0 ? Bug::ENUM_STATUS_OPEN : Bug::ENUM_STATUS_PENDING;
			$b->save();

			$b->saveOpener( $params[ "user" ] );
			$b->updateLastOperation( $params[ "user" ] );

			$b->saveFollower( array( $params[ "user" ] ) );

			if(isset($params["extraFollowers"]))
				$b->saveFollower( $params[ "extraFollowers" ] );


			if ( isset( $params[ "branch" ] ) )
				$b->saveBranch( $params[ "branch" ] );

			if ( isset( $params[ "category" ] ) )
				$b->saveBcategory( $params[ "category" ] );

			if ( isset( $params[ "project" ] ) && $params[ "project" ] > 0 )
				$b->saveProject( $params[ "project" ] );

			if ( isset( $params[ "assignee" ] ) ) {
				$b->saveAssignee( $params[ "assignee" ] );
				$b->saveFollower( array( $params[ "assignee" ] ) );
			}

			$b->saveFollower( DSuser::loadRootUsers() );

			//process mail actions, now
			$b->checkMailAction( $params, true );

			$b->newHistory( $params );

			$b->deleteAllBugTags();
			if ( isset( $params[ "tags" ] ) && strlen( $params[ "tags" ] ) > 0 ) {
				Tags::parseTagsFromParameter( $params[ "tags" ] );

				$t = new Tags();
				$t->load( array( "name" => $params[ "tags" ] ) );
				$b->saveBugTags( $t );
			}

			JPDO::commit();
		}
		catch ( JError $j ) {
			print_r( $j );

			return false;
		}

		return true;
	}

	public function historyCnt() {
		$this->hc = intval($this->countHistory());
	}

	private function checkMailAction( &$params, $noEmails=false ){
		//commands
		$ouser = new DSuser( $params[ "user" ] );
		$params[ "details" ] = PHP_EOL . $params[ "details" ]; #patch for mentions on the start

		if ( $ouser->gotValue && preg_match_all( "/\\{\\{(\\w+)?:?(.+)?\\}\\}/uim", $params[ "details" ], $coms, PREG_SET_ORDER ) !== false ) {
			if($noEmails !== false || stripos($params["details"],'{{NOEMAIL}}')!==false )
				$params["noEmails"] = 1;
			foreach ( $coms as $com ) {
				$comFound = false;
				$comError = "";
				$ma       = strtoupper($com[ 1 ]);
				$raw      = $com[ 0 ];

				if ( $ma == "CLOSE" ) {
					$comFound            = true;
					$params[ "doClose" ] = 1;
				}
				elseif ( $ma == "ASSIGN" ) {
					if ( isset( $com[ 2 ] ) ) {
						//find user
						if( trim( $com[ 2 ] )=="me")
							$u = new DSuser( $params[ "user" ] );
						else
							$u = DSuser::loadRootUsers( array( "username" => "LIKE " . trim( $com[ 2 ] ) ) );

						if ( $u->gotValue === true ) {
							$comFound = true;

							$params[ "assignee" ] = $u->id;
							$this->reassign( $params, true );
						}
						else
							$comError = "user not found or not a root : " . $com[ 2 ];
					}
					else
						$comError = "User definition required after ASSIGN. {{ASSIGN:username}}";
				}
				elseif ( $ma == "TYPE" ) {
					if ( isset( $com[ 2 ] ) ) {
						$newType = array_search( strtolower( $com[ 2 ] ), $this->bugType_options );

						if ( $newType !== false ) {
							$comFound = true;
							unset( $params[ "title" ] );
							$params[ "bugType" ] = $newType;
							$this->edit( $params, true );
						}
						else
							$comError = "type not found : " . $com[ 2 ];
					}
					else
						$comError = "Issue type required after TYPE. {{TYPE:" . implode( "|", $this->bugType_options ) . "}}";
				}
				elseif ( $ma == "CATEGORY" ) {
					if ( isset( $com[ 2 ] ) ) {
						if ( ($c=Category::loadByName($com[2]))!==false ) {
							$comFound = true;
							unset( $params[ "title" ] );
							$params[ "category" ] = $c->id;
							$this->edit( $params, true );
						}
						else
							$comError = "category not found : " . $com[ 2 ];
					}
					else
						$comError = "Category required after CATEGORY. {{CATEGORY:sls|website}}";
				}
				elseif ( $ma == "PROJECT" ) {
					if ( isset( $com[ 2 ] ) ) {
						if ( ($c=Project::loadByName($com[2]))!==false ) {
							$comFound = true;
							unset( $params[ "title" ] );
							$params[ "project" ] = $c->id;
							$this->edit( $params, true );
						}
						else
							$comError = "project not found : " . $com[ 2 ];
					}
					else
						$comError = "Project required after PROJECT. {{PROJECT:SLS|track}}";
				}
				elseif ( $ma == "PRIORITY" ) {
					if ( isset( $com[ 2 ] ) ) {
						$newPrio = array_search( strtolower( $com[ 2 ] ), $this->priority_options );

						if ( $newPrio !== false ) {
							$comFound = true;

							$params[ "priority" ] = $newPrio;
							unset( $params[ "title" ] );
							$this->edit( $params, true );
						}
						else
							$comError = "priority not found : " . $com[ 2 ];
					}
					else
						$comError = "Priority required after PRIORITY. {{PRIORITY:" . implode( "|", $this->priority_options ) . "}}";
				}
				elseif ( $ma == "UNFOLLOW" ) {
					$this->deleteFollower( $params[ "user" ] );
					$params[ "skipSelfFollower" ] = 1;
					$comFound = true;
				}
				elseif ( $ma == "NOEMAIL" ) { #not used any more
					$params[ "noEmails" ] = 1;
					$comFound = true;
				}

				if ( $comFound === false ) {

					MailHistory::log( "Command not found $ma","($raw) by " . $ouser->username.PHP_EOL.$comError );

					$u = new DSuser( $params[ "user" ] );
					if ( $u->isRoot == 1 ) {
						$r = Mail::sendMail( $u->email, "Admingle.Track - Command Error", nl2br( "
						Hello {$u->username},
						Your last command was not good  :
						> " . $raw . " "
							. ( strlen( $comError ) > 0 ? "
						Error Detail :
						" . $comError : "" )
						) );
					}
					unset( $u );

				}
				else
					MailHistory::log( "Command found $ma","($raw) by " . $ouser->username );

			}

			//if it's coming from create and noemail command is not present then remove noemail flag
			if ( $noEmails !== false && stripos( $params[ "details" ], '{{NOEMAIL}}' ) === false )
				unset( $params[ "noEmails" ] );

			//remove commands
			$params[ "details" ] = preg_replace( "/\\{\\{.+?\\}\\}/uim", "", $params[ "details" ] );
			unset( $params[ "isRoot" ] );
		}

		//mentions
		if ( preg_match_all( "/[^\\w]@([^\\s\\.]+)/uim", $params[ "details" ], $umatches, PREG_SET_ORDER ) !== false ) {
			foreach ( $umatches as $ma ) {
				$un = trim( $ma[ 1 ] );

				if ( ( $u = DSuser::loadByUsername( $un ) ) !== false ) {
					$this->saveFollower( $u->id );
					$params[ "details" ] = preg_replace( "/[^\\w]@" . $un . "/uism", " (at)".$un, $params[ "details" ] );
				}
				else {
					$u = new DSuser( $params[ "user" ] );
					Mail::sendMail( $u->email, "Admingle.Track - Mention Error", nl2br( "
					Hello {$u->username},
					Your last mention was not good  :
					> " . $un . ""
					) );
				}
			}
		}
		return true;
	}

	public function response( $params ) {
		JPDO::beginTransaction();
		try {
			$this->checkMailAction( $params );

			if ( $this->status == Bug::ENUM_STATUS_CLOSED )
				return $this->reopen( $params, true );
			elseif ( $this->status != Bug::ENUM_STATUS_CLOSED && isset( $params[ "doClose" ] ) ) {
				return $this->close( $params );
			}

//			$params[ "operation" ] = isset($params["doClose"])?BugHistory::ENUM_OPERATION_CLOSE:BugHistory::ENUM_OPERATION_RESPONSE;
			$params[ "operation" ] = BugHistory::ENUM_OPERATION_RESPONSE;

			$this->save();
			$this->updateLastOperation( $params[ "user" ] );

			if(isset($params["extraFollowers"]))
				$this->saveFollower( $params[ "extraFollowers" ] );

			if ( !isset( $params[ "skipSelfFollower" ] ) )
				$this->saveFollower( $params[ "user" ] );

			$this->newHistory( $params );

			JPDO::commit();

		}
		catch ( JError $j ) {
			return false;
		}

		return true;
	}

	public function reassign( $params, $inlineOperation = false ) {
		JPDO::beginTransaction();
		try {
			$this->loadAssignee();
			$d = new DSuser( $params[ "assignee" ] );
			if (
				( $this->assignee && $this->assignee->gotValue && $d->id != $this->assignee->id )
				||
				$this->assignee === false
				||
				$this->assignee->gotValue !== true
			) {
				$params[ "operation" ] = BugHistory::ENUM_OPERATION_ASSIGNMENT;
				$params[ "details" ]   = "Assigned " . ( strlen( $this->assignee->username ) > 0 ? " from " . $this->assignee->username : "" ) . " to " . $d->username;
				$this->status          = Bug::ENUM_STATUS_OPEN;
				//$this->lastOperation   = time();

				$this->assigned        = 1;
				$this->save();
				$this->updateLastOperation( $params[ "user" ] );
				$this->deleteAllAssignee();

				$this->saveAssignee( $params[ "assignee" ] );
				$this->saveFollower( array( $params[ "assignee" ] ) );

				$this->newHistory( $params );
			}
			if ( $inlineOperation )
				return true;

			JPDO::commit();

		}
		catch ( JError $j ) {
			return false;
		}

		return true;
	}

	public function close( $params, $inlineOperation=false ) {
		JPDO::beginTransaction();

		if ( $this->status == Bug::ENUM_STATUS_CLOSED )
			return true;  #we ve been there
		try {
			$this->status        = Bug::ENUM_STATUS_CLOSED;

			$params[ "operation" ] = BugHistory::ENUM_OPERATION_CLOSE;
			$params[ "details" ]   = $params[ "details" ] . "
			ISSUE CLOSED";

			$this->save();
			$this->updateLastOperation( $params[ "user" ] );
			$this->newHistory( $params );

			if($inlineOperation)
				return true;

			JPDO::commit();

		}
		catch ( JError $j ) {
			return false;
		}

		return true;
	}

	public function reopen( $params, $inlineOperation=false ) {
		JPDO::beginTransaction();
		try {
			$this->lastOperation = time();
			$this->status = Bug::ENUM_STATUS_OPEN;
			$params[ "operation" ] = BugHistory::ENUM_OPERATION_REOPEN;
			$params[ "details" ] = "ISSUE REOPENED
			".$params[ "details" ];

			$this->save();
			$this->updateLastOperation( $params[ "user" ] );
			$this->saveFollower(array($params[ "user" ]));

			$this->newHistory( $params );

			if($inlineOperation)
				return true;


			JPDO::commit();

		}
		catch ( JError $j ) {
			return false;
		}

		return true;
	}

	public function edit( $params, $inlineOperation=false ) {
		JPDO::beginTransaction();
		try {
			$params[ "operation" ] = BugHistory::ENUM_OPERATION_EDIT;
			$params[ "details" ] = array();
			if(isset($params[ "title" ]) && $this->title != $params[ "title" ]){
				$params[ "details" ][] = "Changed title to " . $params[ "title" ];
				$this->title = $params[ "title" ];
			}

			if(isset($params[ "priority" ]) && $this->priority != $params[ "priority" ]){
				$params[ "details" ][] = "Changed priority to " . $this->priority_options[$params[ "priority" ]];
				$this->priority      = $params[ "priority" ];
			}
			if(isset($params[ "link" ]))
				$this->link          = $params[ "link" ];

			$this->lastOperation = time();

			$this->loadBranch();
			if ( isset( $params[ "branch" ] ) && $this->branch->id != $params[ "branch" ] ) {
				$bb                     = new Branch( $params[ "branch" ] );
				$params[ "details" ][ ] = "Changed branch to " . $bb->name;
				unset( $bb );

				$this->deleteBranch();
				$this->saveBranch( $params[ "branch" ] );
			}

			$this->loadBcategory();
			if(isset($params[ "category" ]) && $this->bcategory->id != $params[ "category" ]){
				$bb = new Category( $params[ "category" ] );
				$params[ "details" ][] = "Changed category to " . $bb->name ;
				unset( $bb );

				$this->deleteAllBcategory();
				$this->saveBcategory( $params[ "category" ] );
			}

			$this->loadProject();
			if(isset($params[ "project" ]) && $this->project->id != $params[ "project" ]){
				$bb = new Project( $params[ "project" ] );
				$params[ "details" ][ ] = "Changed project to " . $bb->name;
				unset( $bb );

				$this->saveProject( $params[ "project" ] );
			}

			if(isset($params[ "startDate" ]) && $params["startDate"] != $this->startDate){
				$params[ "details" ][ ] = "Changed start date to " . $params[ "startDate" ];
				$this->startDate     = $params[ "startDate" ];
			}

			if (isset($params[ "bugType" ]) &&  $params[ "bugType" ] != $this->bugType ) {
				$params[ "details" ][ ] = "Changed type to " . $this->bugType_options[ $params[ "bugType" ] ];
				$this->bugType          = $params[ "bugType" ];
			}

			$this->save();
			$this->updateLastOperation( $params[ "user" ] );

			if ( isset( $params[ "tags" ] ) ) {
				$oldTags = $this->collectTags( true );
				$this->deleteAllBugTags();

				if ( strlen( $params[ "tags" ] ) > 0 ) {
					Tags::parseTagsFromParameter( $params[ "tags" ] );

					$t = new Tags();
					$t->load( array( "name" => $params[ "tags" ] ) );
					$this->saveBugTags( $t );

					$diff1 = array_diff( $params[ "tags" ], $oldTags );
					$diff2 = array_diff( $oldTags, $params[ "tags" ] );

					if ( sizeof( $diff1 ) > 0 || sizeof( $diff2 ) > 0 || sizeof( $oldTags ) != sizeof( $params[ "tags" ] ) )
						$params[ "details" ][ ] = "Tags changed to " . implode( ", ", $params[ "tags" ] );
				}
			}
			if ( isset( $params[ "details" ] ) )
				$params[ "details" ] = implode( "\n", $params[ "details" ] );

			$params[ "rootOnly" ] = 1;

			$this->newHistory( $params, true );

			if ( !isset( $params[ "skipSelfFollower" ] ) )
				$this->saveFollower( array( $params[ "user" ] ) );

			if($inlineOperation)
				return true;

			JPDO::commit();

			return true;
		}
		catch ( JError $j ) {
			return false;
		}

		return true;
	}

	public function newHistory( $params, $onlyRoots=false, $inlineOperation=true , $noFileOperation=false ) {

		$params[ "details" ]   = trim( $params[ "details" ] );
//		if ( !( strlen( $params[ "details" ] ) > 0 ) ) {
//			return true;
//		}

		if ( !($this->id > 0)  )
			return false;


		$h            = new BugHistory();
		if(isset($params["title"]))
			$h->title     = $params[ "title" ];

		if(isset($params["details"]))
			$h->details   = $params[ "details" ];

		$h->operation = isset( $params[ "operation" ]) ? $params[ "operation" ] : BugHistory::ENUM_OPERATION_RESPONSE;
		$h->bugUser   = $params[ "user" ];
		$h->rootOnly  = isset( $params[ "rootOnly" ] ) ? $params[ "rootOnly" ] : 0;
		$h->save();

		$this->saveHistory( $h );

		if ( $noFileOperation === false ) {
			if ( isset( $params[ "fileIds" ] ) && sizeof( $params[ "fileIds" ] ) > 0 )
				$files = $params[ "fileIds" ];
			else
				$files = File::newFile( "file" );

			if ( sizeof( $files ) > 0 )
				$h->saveBugFiles( $files );
		}

		//$h->sendInformEmails();
		if ( !isset( $params[ "noEmails" ] ) ) {
			$b = new Backend();
			$b->task(
				"Info Emails",
				"sendEmails", array(
					"bugId"      => $h->id,
					"onlyRoots"  => $onlyRoots,
					"skipUser"   => $params[ "user" ] )
			);
			unset( $b );
		}
		if(isset($params["relatedBug"]))
			$h->saveRelatedBug( $params[ "relatedBug" ] );

		if($inlineOperation)
			return true;

//			Backend::task( "send ticket mails" );
		return true;

	}

	/**
	 * @param bool $returnArray
	 * @author Özgür Köy
	 */
	public function collectTags( $returnArray=false) {
		$b = $this->loadBugTags();
		$t = array();
		if($b && $b->gotValue){
			do {
				$t[ ] = $b->name;
			} while ( $b->populate() );

		}
		if($returnArray!==false)
			return $t;

		$this->tagz = implode( ", ", $t );
	}

	public static function checkIsFollower( $bugId, $userId ) {
		$b = new Bug($bugId);
		$follower = $b->loadFollower($userId);
		$is = $follower && $follower->gotValue;
		unset( $b, $follower );

		return $is;
	}

	public function mergeTo( $userId, $id ) {
		if ( $id == $this->id )
			return false;

		$b = new Bug( $id );
		JPDO::beginTransaction();
		try {
			//merge folls
			$this->loadFollower( null, 0, 50, null, false );
			if ( $this->follower && $this->follower->gotValue ) {
				do {
					$b->saveFollower( $this->follower->id );
				} while ( $this->follower->populate() );

			}
			$this->merged = $b->id;
			$this->save();

			//merge history , or dont
			/*			$this->loadHistory(null, 0, 50, null, false);
						if($this->history && $this->history->gotValue){
							do {
								if($this->history->rootOnly != 1){
									$nh = $this->history->cloneMe();
									$b->saveHistory( $nh->id );
								}

							} while ( $this->history->populate() );
						}
			*/

			$b->newHistory(
				array(
					"user" => $userId,
					"relatedBug" => $this->id,
					"details" => "Issue #" . $this->id . " merged into this issue"
				)
			);

			$this->close(
				array(
					"noEmails"=>1,
					"user" => $userId,
					"relatedBug" => $id,
					"details" => "Issue Merged into #" . $b->id
				)
			);

			$b->updateLastOperation( $userId );
			$this->updateLastOperation( $userId,1 );
			unset( $b );

			JPDO::commit();
		}
		catch ( JError $j ) {

			return false;
		}
	}

	public static function loadActiveBugs( $filter = array() ) {
		$b = new Bug();
		$filter += array( "status" => array( Bug::ENUM_STATUS_OPEN, Bug::ENUM_STATUS_PENDING ) );
		$b->orderBy("jdate DESC")->load( $filter );

		return $b;
	}

	public function updateLastOperation( $userId, $timeDiff=0 ) {
		$this->lastOperation = time()+$timeDiff;
		$this->deleteAllLastOperator();
		$this->saveLastOperator( $userId );
		$this->save();
	}

	public static function loadByThread( $tid ) {
		if(empty($tid))
			return false;
		$o = new Bug();
		$o->load( array("mailThreadId"=>$tid) );
		return $v = $o->gotValue?$o:false;
	}
}

