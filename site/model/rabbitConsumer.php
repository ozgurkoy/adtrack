<?php
/**
 * rabbitmq consumer class
 *
 * @package default
 * @author murat
 **/
class RabbitConsumer
{
	public $connection;
	public $channel;
	public $lastRequestResult;
	private $rejectedHashes = array();

	public function init(){
		self::clog("Starting consumer...");
	}

	public static function clog($msg){
		global $debugLevel;
		if($debugLevel > 0){
			if (strlen($msg)>0)
				$msg = date("Y-m-d H:i:s", time()) . (isset($_SERVER["SUPERVISOR_PROCESS_NAME"]) ? " | " . $_SERVER["SUPERVISOR_PROCESS_NAME"] : "") . " => " . $msg . "\n";

				echo $msg;
			}

	}
	
	/**
	 * Mail Send
	 * 
	 * @return void
	 * @author Murat
	 */
	public function sendMail($msg,$m){
		$this->clog("processing email message : " . $m->id);
		
		try {
			if (Mail::send($m->id)){
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
				$this->clog("processed email message : " . $m->id);
			} else {
				//$this->rejectMessage($msg);
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
				$this->clog("exception occured in email message : " . $m->id);
			}
		} catch (Exception $ex) {
			$this->rejectMessage($msg);
			$this->clog("exception occured in email message : " . $m->id);
		}
	}
	
	/**
	 * Publisher twitter information update
	 * 
	 * @return void
	 * @author Murat
	 */
	public function updatePublisherTwitterInfo( $msg, $m ) {
		global $debugLevel;
		$this->clog( "processing twitter update message : " . $m->id );

		try {
			$r = PublisherUpdate::singleTwitterUpdateProcess($m->id);
			if (is_bool($r)){
				$this->clog("Publisher Twitter Update Success for #" . $m->id);
			} else {
				$this->clog("Publisher Twitter Update Error for #" . $m->id . ". Error: " . $r);
			}
			$msg->delivery_info[ 'channel' ]->basic_ack( $msg->delivery_info[ 'delivery_tag' ] );
			$this->clog( "processed twitter update message : " . $m->id );
		}
		catch ( Exception $ex ) {
			$this->rejectMessage($msg);
			$this->clog( "exception occured in twitter update message : " . $m->id );
		}
	}

	/**
	 * Publisher twitter TrueReach score update
	 *
	 * @return void
	 * @author Murat
	 */
	public function updatePublisherTrueReach( $msg, $m ) {
		global $debugLevel;
		$this->clog( "processing TrueReach update message : " . $m->id );

		try {
			$r = PublisherUpdate::singleCalculateProcess($m->id);
			if (is_bool($r)){
				$this->clog("TrueReach Update Success for #" . $m->id);
			} else {
				$this->clog("TrueReach Update Error for #" . $m->id . ". Error: " . $r);
			}
			$msg->delivery_info[ 'channel' ]->basic_ack( $msg->delivery_info[ 'delivery_tag' ] );
			$this->clog( "processed TrueReach update message : " . $m->id );
		}
		catch ( Exception $ex ) {
			$this->rejectMessage($msg);
			$this->clog( "exception occured in TrueReach update message : " . $m->id );
		}
	}

	/**
	 * Process mail hook
	 *
	 * @param string $msg
	 * @param string $m
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public function mailHook($msg, $m) {
		$this->clog("processing mail hook message : " . $m->id);

		try {
			MailHook::process($m->id);
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			$this->clog("processed mail hook message : " . $m->id);
		} catch (Exception $ex) {
			$this->rejectMessage($msg);
			$this->clog("exception occured in email message : " . $m->id);
		}
	}
	
	/**
	 * send push message to Apple Servers
	 * 
	 * @return void
	 * @author Murat
	 */
	public function sendPushMessage($msg,$m){

		$this->clog("processing Send Push message : " . $m->id);

		try {
			PushMobileMessage::send($m->id);
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			$this->clog("Send Push message : " . $m->id);
		} catch (Exception $ex) {
			$this->rejectMessage($msg);
			$this->clog("exception occurred in push message : " . $m->id);
		}
	}
	
	/**
	 * Process cron message
	 * 
	 * @return void
	 * @author Murat
	 */
	public function processCronMessage($msg,$m){
        global $debugLevel;
		$this->clog("processing cron message : " . $msg->body);

		$type           = $m->type;
		$cronId         = $m->id;
		$campaignId     = $m->campaign;
		$publisherId    = $m->publisher;
		$msgIndex       = $m->msgindex;
		$additionalData = $m->additionalData;
		$acted = false;
		try {
			Cron::startProcess($cronId);
			switch($type){
				case Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL:
				case Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_TRENDSETTER:
					if (!Campaign::sendAvailableEmailsNew($m->campaign,$m->type)){
						$acted = true;
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						Cron::failProcess($cronId);
						$this->clog("Campaign sendAvailableEmails failed ");
					}
					break;
				case Cron::ENUM_TYPE_CAMPAIGN_START:
					Campaign::startCampaign($campaignId);
					break;
				case Cron::ENUM_TYPE_CAMPAIGN_END:
					if (!Campaign::endCampaign($campaignId)){
						$acted = true;
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						Cron::failProcess($cronId);
						$this->clog("Campaign ending failed ");
					}
					break;
				case Cron::ENUM_TYPE_TWEET_SEND:
					if (CampaignTwitterMessage::sendTweet($campaignId,$publisherId,$msgIndex) === false){
						Cron::resetProcess($cronId);
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						$acted = true;
					}
					break;
				case Cron::ENUM_TYPE_TWEET_CHECK:
					CampaignTwitterMessage::checkTweet($campaignId,$publisherId,$msgIndex);
					break;
				case Cron::ENUM_TYPE_CAMPAIGN_FINISH_ALERT:
					if (!Campaign::sendFinishingMail($campaignId)){
						$acted = true;
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						Cron::failProcess($cronId);
						$this->clog("Campaign sendFinishingMail failed ");
					}
					break;
				case Cron::ENUM_TYPE_CAMPAIGN_BUDGET_CHECK:
					if (!Campaign::checkBudget($campaignId)){
						$acted = true;
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						//Cron marked as waiting, so we don't need to update again.
					}
					break;
				case Cron::ENUM_TYPE_GET_PUBLISHER_CLICK_STATS:
					if (Campaign::getPublisherClickStats($campaignId,$publisherId,$msgIndex,$additionalData) === false){
						$acted = true;
						Cron::failProcess($cronId);
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						unset($cum);
					}
					break;
				case Cron::ENUM_TYPE_LAST_FRAUD_CHECK:
					if (Campaign::lastFraudControl($campaignId) === false){
						$acted = true;
						Cron::failProcess($cronId);
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						unset($cum);
					}
					break;
				case Cron::ENUM_TYPE_CPL_REMOTE_FINISH:
					if (Campaign::endCplRemoteCampaign($campaignId) === false){
						$acted = true;
						Cron::failProcess($cronId);
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						unset($cum);
					}
					break;
				case Cron::ENUM_TYPE_CPA_AFF_FINISH:
					if (Campaign::endCpaAffCampaign($campaignId) === false){
						$acted = true;
						Cron::failProcess($cronId);
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						unset($cum);
					}
					break;
				default:
					$this->clog("unknown queue message received: " . json_encode($msg->body));
					break;
			}

			if (!$acted){
				Cron::endProcess($cronId);
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			}
			$this->clog("processed cron message : " . $msg->body);
		} catch (Exception $ex) {
			try {
				Cron::failProcess($m->id);
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag'],true);
				$this->clog("error occured on cron message : " . $msg->body . " Error: " . print_r($ex,true));
			} catch (Exception $ex2) {
				try {
					$this->rejectMessage($msg);
					$this->clog("error occurred on cron message : " . $msg->body . " Error2: " . print_r($ex2,true));
				} catch (Exception $ex3) {
					$this->clog("error occurred on cron message : " . $msg->body . " Error3: " . print_r($ex3,true));
				}
			}
		}
	}

	/**
	 * Reject the RabbitMQ Message to que
	 *
	 * @param object $msg
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	private function rejectMessage($msg){
		$hash = md5($msg->body);

		if (!isset($this->rejectedHashes[$hash])) $this->rejectedHashes[$hash] = -1;
		$this->rejectedHashes[$hash]++;

		if ($this->rejectedHashes[$hash] < 10){
			$msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'],true);
		} else {
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag'],true);
			try{
				FailedRMQJobs::create($hash,$msg->body);
			} catch(Exception $ex){
				JLog::log("rmq","RabbitMQ Job Failed: ". $msg->body);
			}
		}
	}

	/**
	 * Check campaign fraud actions
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public function fraudControl($msg,$m){
		global $debugLevel;
		$this->clog("processing fraud control : " . $msg->body);
		try {
			if ($m->type == "fraudControl"){
				$c = new Campaign($m->id);
				$c->loadCampaignClick();
				$data = $c->campaign_campaignClick->getReferrerDetails();
				if (is_array($data)){
					foreach($data as $ref){
						$this->channel->basic_publish(new AMQPMessage(json_encode(Array(
							"type" => "fraudAnalyze",
							"ref" => $ref->hash
						)), array('content_type' => 'text/plain', 'delivery_mode' => 2)), RMQ_EXCHANGE, RMQ_QUEUE_FRAUD_CONTROL);
						$this->clog("Referrer '{$ref->hash}' pushed to RabbitMQ for TweetGator");
					}
				} else {
					$this->clog("No referrer data found. SLS API Response: " . print_r($data,true));
				}
				unset($data);
			} elseif ($m->type == "fraudAnalyze"){
				$data = Tool::callTGService("search",array("data"=>  json_encode(array("q" => $m->ref))));
				$this->clog("Referrer '{$m->ref}' pushed to TweetGator, TG service response : " . print_r($data,true));
			}
			$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			$this->clog("processed fraud control : " . $msg->body);
		} catch (Exception $ex) {
			try {
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag'],true);
				$this->clog("error occured on fraud control : " . $msg->body . " Error: " . print_r($ex,true));
			} catch (Exception $ex2) {
				try {
					$this->rejectMessage($msg);
					JLog::log("rabbitmq",  serialize($ex));
					$this->clog("error occured on cron message : " . $msg->body . " Error2: " . print_r($ex2,true));
				} catch (Exception $ex3) {
					$this->clog("error occured on cron message : " . $msg->body . " Error3: " . print_r($ex3,true));
				}
			}
		}
	}

	/**
	 * Process RabbitMQ message
	 *
	 * @param object $msg
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public function processQueue($msg) {
		JLog::$writeToFile = true;
		$m = json_decode($msg->body);
		if (isset($m->type)){
			switch($m->type){
				case "email":
					$this->sendMail($msg,$m);
					break;
				case "updateTwitter":
					$this->updatePublisherTwitterInfo($msg,$m);
					break;
				case "updateTrueReach":
					$this->updatePublisherTrueReach($msg,$m);
					break;
				case "push":
					$this->sendPushMessage($msg,$m);
					break;
				case "mailHook":
					$this->mailHook($msg,$m);
					break;
				case "fraudControl":
				case "fraudAnalyze":
					$this->fraudControl($msg,$m);
					break;
				default:
					$this->processCronMessage($msg, $m);
					break;
			}
		} else {
			die("message type did not set");
		}
		JLog::$writeToFile = false;
	}
}