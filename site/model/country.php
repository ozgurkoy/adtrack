<?php
	/**
	* country real class for Jeelet - firstly generated on 11-10-2011 11:44, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/country.php");

	class Country extends Country_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}
		
		/**
		 * load all countries and return 
		 *
		 * @return array
		 * @author Özgür Köy
		 **/
		public static function loadAll( $returnObject=false )
		{
			$c = new Country();
			$c->populateOnce(false)->orderBy("name")->load(array("active"=>1));
			if($returnObject)
				return $c;

			$thiss = array();

			if(sizeof($c->_ids)>0){
				do {
					$thiss[$c->id] = $c->name;
				}while ($c->populate());
			}

			unset($c);
			return $thiss;
			
		}



	}

	?>