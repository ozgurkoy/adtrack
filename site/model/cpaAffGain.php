<?php
/**
 * cpaAffGain real class - firstly generated on 08-09-2014 15:50, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cpaAffGain.php';

class CpaAffGain extends CpaAffGain_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function loadByUId( $uid ) {
		$this->load( array( "uid" => $uid ) );
		return $this;
	}

}

