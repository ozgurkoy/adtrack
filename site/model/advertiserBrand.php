<?php
/**
 * advertiserBrand real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/advertiserBrand.php';

class AdvertiserBrand extends AdvertiserBrand_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * check the campaign brand that exists
	 *
	 * @return bool
	 * @author murat
	 */
	public static function checkUnique($advertiserId, $brandName, $brandID = null){
		$ar = new AdvertiserBrand();
		$ar->load(array("advertiser_advertiserBrand"=>$advertiserId,"name"=> $brandName));

		if ($ar->gotValue && $brandID != $ar->id){
			unset($ar);
			return true;
		} else {
			unset($ar);
			return false;
		}
	}

	/**
	 * Get advertiser brands
	 *
	 * @return AdvertiserBrand
	 * @author Murat
	 */
	public static function allBrands($advertiser,$returnType="array"){
		$ar = new AdvertiserBrand();
		$ar->populateOnce( false )->load(array("advertiser_advertiserBrand"=>$advertiser));

		if ($returnType == "array"){
			$brands = array();
			if ($ar->gotValue){
				do{
					$brands[$ar->id] = $ar->name;
				} while($ar->populate());
			}
			return $brands;
		} else {
			return $ar;
		}
	}

	/**
	 * Get all active brands as object
	 *
	 * @return Trend
	 * @author Murat
	 */
	public static function getAll(){
		$t = new Trend();
		$t->populateOnce( false )->load(array("active"=>1));
		return $t;
	}
}

