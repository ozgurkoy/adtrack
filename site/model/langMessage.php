<?php
/**
 * langMessage real class - firstly generated on 25-09-2013 16:24, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/langMessage.php';

class LangMessage extends LangMessage_base
{

	/**
	 * available footer types
	 * @var array
	 */
	public static  $footerTypes = array( "both", "publisher", "advertiser" );

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function clearCache($langShort=null){
		global $__site;
		$memcacheKey = $__site.'_langMessage_' . $langShort;
		$cantConnectToMemcache = false;
		$memcache = new Memcache;
		$memcache->connect(MEMCACHE_SERVER, MEMCACHE_PORT) or $cantConnectToMemcache = true;
		$memcache->delete($memcacheKey);
		if (!$cantConnectToMemcache) $memcache->close();
		unset($memcache);

	}

	/**
	 * @param $langShort
	 * @param $wordCode
	 * @return array|bool|null
	 * @author Murat
	 * @author Özgür Köy
	 */
	public static function getLangMessages( $langShort, $wordCode ) {
		$cacheKey = "langMessage_" . $langShort . "_" . $wordCode;

		if ( ( $_wordTexts = JCache::read( $cacheKey ) ) === false ) {
			$langMessages = new LangMessage();
			$langMessages->load(
				array(
					"langShortDef" => $langShort,
					"messageCode"  => $wordCode
				)
			);

			if ( $langMessages->gotValue !== true ) { #return to default language.
				$langMessages = new LangMessage();
				$langMessages->load(
					array(
						"langShortDef" => Setting::getValue( "DEFAULT_LANGUAGE" ),
						"messageCode"  => $wordCode
					)
				);

				if ( $langMessages->gotValue !== true ) { #well, nothing mate, sorry
					//Message code not found
					JLog::log( "sys", "Invalid message request . " . $wordCode );

					return false;
				}
			}


			$msg = $langMessages->messageBody;

			if ( strpos( $msg, "{footer." ) !== false ) {
				foreach ( self::$footerTypes as $tt ) {
					if ( !empty( $langwords[ "footer.$tt" ] ) ) {
						$langwords = LangWord::getLangWords( "mailFooter", $langShort, "footer.$tt" );
						$msg       = str_replace( "{footer.$tt}", $langwords[ "footer.$tt" ], $msg );
					}
					else
						$msg = str_replace( "{footer.$tt}", "", $msg ); #could not be found in definitions, so remove from the template
				}
			}
			$t0 = array(
				"message"          => Format::replaceGlobals( $msg ),
				"title"            => Format::replaceGlobals( $langMessages->messageTitle ),
				"email"            => $langMessages->mail,
				"pushNotification" => $langMessages->pushNotification,
				"dashboard"        => $langMessages->dashbaord,
				"lastHistoryId"    => $langMessages->lastHistoryId
			);
			JCache::writeDirect( $cacheKey, $t0 );
			$_wordTexts = $t0;

			unset( $t0 );

			unset( $langwords, $langMessages );
		}

		return $_wordTexts;
	}

	/**
	 * Create history for LangMessage
	 *
	 * @return void
	 * @author Murat
	 */
	public function createHistory(){
		$lmh = new LangMessageHistory();
		foreach ($lmh->_fields as $f){
			if ($f != "id" && $f != "jdate"){
//				if(isset($lmh->$f))
					$lmh->$f = $this->$f;
			}
		}
		$lmh->langMessage_langMessageHistory = $this->id;
		$lmh->save();
		$this->lastHistoryId = $lmh->id;
		$this->save();
	}
}

