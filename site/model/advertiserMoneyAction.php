<?php
/**
 * advertiserMoneyAction real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/advertiserMoneyAction.php';

class AdvertiserMoneyAction extends AdvertiserMoneyAction_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Crete money action for advertiser
	 *
	 * @return void
	 * @author Murat
	 */
	public static function create($advertiser,$campaign,$amount,$info=null,$publisher=null,$state=AdvertiserMoneyAction::ENUM_STATE_APPROVED,$paymentType=AdvertiserMoneyAction::ENUM_PAYMENTTYPE_CASH){
		if (!is_object($advertiser)){
			$advertiser = new Advertiser($advertiser);
			if (!$advertiser->gotValue) return;
		}

		$ama = new AdvertiserMoneyAction();
		$ama->advertiser_advertiserMoneyAction = $advertiser;
		$ama->campaign_advertiserMoneyAction = $campaign;
		$ama->amount = $amount;
		$ama->amountAfter = $advertiser->currentBalance() + $ama->amount;
		$ama->state = $state;
		$ama->lastActionDate = time();
		$ama->info = $info;
		$ama->publisher = $publisher;
		$ama->paymentType = $paymentType;
		$ama->save();
		unset($amc);
	}
}

