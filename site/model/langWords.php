<?php
/**
 * langWords real class - firstly generated on 24-10-2012 14:17, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/langWords.php';

class LangWords extends LangWords_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}
	
	/**
	 * get langwords by type and optionally by language
	 * 
	 * @return array
	 * @author Murat
	 */
	 public static function getLangWords($wordType,$langShort=null){
		global $__site;
	 	$cantConnectToMemcache = false;
		$memcache = new Memcache;
		$memcache->connect(MEMCACHE_SERVER, MEMCACHE_PORT) or $cantConnectToMemcache = true;
		$_wordTexts = null;

		if ($cantConnectToMemcache || !($_wordTexts = $memcache->get($__site.'_langWords_' . $langShort . '_' . $wordType))) {
			//echo "getting from db";
			/*
			$lw = new LangWords();
			if ($wordType == "mailMessage"){
				$lw->customClause = "wordType IN ('mailMessage','mailFooter')";
			} else {
				$lw->wordType = $wordType;
				if (!is_null($langShort)){
					$lw->langShort = $langShort;
				}
			}
			
			$lw->orderBy = "wordType";
			$lw->nopop()->load(null,0,1000000);
			
			while($lw->populate()){
				$text = JUtil::replaceGlobals($lw->wordText);
				if ($lw->wordType=="mailMessage"){
					$_wordTexts[$lw->langShort][$lw->wordCode] = (array)json_decode($text);
					foreach(array("both","publisher","advertiser") as $tt){
						$_wordTexts[$lw->langShort][$lw->wordCode]["message"] = str_replace("{footer.$tt}", $_wordTexts[$lw->langShort]["footer.$tt"], $_wordTexts[$lw->langShort][$lw->wordCode]["message"]);
					}
				} else 
				if (!is_null($langShort) && $wordType != "mailMessage"){
					$_wordTexts[$lw->wordCode] = $text;
				} else {
					$_wordTexts[$lw->langShort][$lw->wordCode] = $text;
				}
			}
			unset($lw);
			$memcache->set($__site."_langWords_" . $langShort . '_' . $wordType, $_wordTexts, MEMCACHE_COMPRESSED, 300);
			*/
			
			$sql = "SELECT langShort, wordType, wordCode, wordText FROM langWords WHERE ";
			if ($wordType == "mailMessage"){
				$sql .= "wordType IN ('mailMessage','mailFooter')";
			} else {
				$sql .= "wordType = '$wordType'";
				if (!is_null($langShort)){
					$sql .= " AND langShort = '$langShort'";
				}
			}
			$sql .= " ORDER BY wordType";
			JPDO::connect( JCache::read('JDSN.MDB') );
			$words = JPDO::executeQuery($sql);
			
			if (count($words)){
				foreach($words as $word){
					$text = $word["wordText"];
					if ($word["wordType"]=="mailMessage"){
						$_wordTexts[$word["langShort"]][$word["wordCode"]] = (array)json_decode($text);
						foreach(array("both","publisher","advertiser") as $tt){
							$_wordTexts[$word["langShort"]][$word["wordCode"]]["message"] = Format::replaceGlobals(str_replace("{footer.$tt}", $_wordTexts[$word["langShort"]]["footer.$tt"], $_wordTexts[$word["langShort"]][$word["wordCode"]]["message"]));
							$_wordTexts[$word["langShort"]][$word["wordCode"]]["title"] = Format::replaceGlobals($_wordTexts[$word["langShort"]][$word["wordCode"]]["title"]);
						}
					} elseif (!is_null($langShort) && $wordType != "mailMessage"){
						$_wordTexts[$word["wordCode"]] = Format::replaceGlobals($text);
					} else {
						$_wordTexts[$word["langShort"]][$word["wordCode"]] = Format::replaceGlobals($text);
					}
				}
			}
			unset($words);
			$memcache->set($__site."_langWords_" . $langShort . '_' . $wordType, $_wordTexts, MEMCACHE_COMPRESSED, 300);
			
			/*
			try {
				if (defined('ADVANCED_DEBUG_MODE') && ADVANCED_DEBUG_MODE == "ON"){
					$myFile = DOCUMENT_ROOT.'errors/langWordLog.log';
					$fh = fopen($myFile, 'a');

					$fContent = date("Y-m-d H:i:s") . " => Langword loaded from DB. Cached langword count: " .  count($_wordTexts) . ". Cache Key: langWords_" . $langShort . '_' . $wordType . PHP_EOL;

					fwrite($fh, PHP_EOL . utf8_encode($fContent));
					fclose($fh);
					//die("dur yolcu");
				}
			} catch (Exception $e) {
				// continue;
			}
			*/
		} else {
			/*
			try {
				if (defined('ADVANCED_DEBUG_MODE') && ADVANCED_DEBUG_MODE == "ON"){
					$myFile = DOCUMENT_ROOT.'errors/langWordLog.log';
					$fh = fopen($myFile, 'a');

					$fContent = date("Y-m-d H:i:s") . " => Langword loaded from Cache. Loaded langword count: " .  count($_wordTexts) . ". Cache Key: langWords_" . $langShort . '_' . $wordType . PHP_EOL;

					fwrite($fh, PHP_EOL . utf8_encode($fContent));
					fclose($fh);
					//die("dur yolcu");
				}
			} catch (Exception $e) {
				// continue;
			}
			*/
		}
		if (!$cantConnectToMemcache) $memcache->close();
		unset($memcache);
		return $_wordTexts;
	 }

	/**
	 * check wordCode validity
	 *
	 * @return bool
	 * @author murat
	 **/
	public static function checkWordCode($wc)
	{
		if (strlen($wc)>0 && !is_numeric(substr($wc,0,1)) && preg_match('/^[a-z0-9\d_]{1,200}$/i', $wc)) {
		    return true;
		} else {
		    return false;
		}
	}
	
	/**
	 * Create history record for LangWord
	 * 
	 * @return void
	 * @author Murat 
	 */
	public function createHistory(){
		JPDO::connect( JCache::read('mysqJDSN') );
		$sql = "
			INSERT INTO langWordHistory(status,lang,jdate,langShort,wordType,wordCode,wordtext,createEnv,createDate,updateDate,updateEnv)
			SELECT status,lang,jdate,langShort,wordType,wordCode,wordtext,createEnv,createDate,updateDate,updateEnv
			FROM langWords
			WHERE wordType = '{$this->wordType}' AND wordCode = '{$this->wordCode}'
		";
		JPDO::executeQuery($sql);
	}
	 
	 /**
	  * Compare the given langWords array with database
	  * 
	  * @return void
	  * @author Murat
	  */
	 public static function compareLangWords($langWordsFromFile){
		 $sql = "
				SELECT
					langShort,
					wordType,
					wordCode,
					wordText
				FROM
					langWords
		";
		JPDO::connect(JCache::read('mysqlDSN.MDB'));
		$wordTexts = JPDO::executeQuery($sql);
		$lw = array();
		foreach($wordTexts as $wt){
			$lw[$wt["langShort"]][$wt["wordType"]][$wt["wordCode"]] = $wt["wordText"];
		}

		foreach ($langWordsFromFile as $langShort => $wordTypeContent) {
			foreach ($wordTypeContent as $wordType => $wordContent) {
				foreach ($wordContent as $wordCode => $wordText) {
					if (isset($lw[$langShort][$wordType][$wordCode])){
						if ($wordText->wordText != $lw[$langShort][$wordType][$wordCode]){
							Tool::echoLog("different word found. lets update : $langShort | $wordType | $wordCode<br>");
							$langWord = new LangWords();
							$langWord->langShort = $langShort;
							$langWord->wordType = $wordType;
							$langWord->wordCode = $wordCode;
							$langWord->load();
							$langWord->wordText = $wordText->wordText;
							$langWord->createDate = $wordText->createDate;
							$langWord->createEnv = $wordText->createEnv;
							$langWord->updateDate = $wordText->updateDate;
							$langWord->updateEnv = $wordText->updateEnv;
							$langWord->createHistory(); // Create history record before updating it
							$langWord->save();
						}
					} else {
						Tool::echoLog("word not found in database, let's insert it : $langShort | $wordType | $wordCode<br>");
						//echo $langShort . "--" . $wordType . "--" . $wordCode . "<br>\n";
						//die("<pre>".print_r($lw,true));
						$langWord = new LangWords();
						$langWord->langShort = $langShort;
						$langWord->wordType = $wordType;
						$langWord->wordCode = $wordCode;
						$langWord->wordText = $wordText->wordText;
						$langWord->createDate = $wordText->createDate;
						$langWord->createEnv = $wordText->createEnv;
						$langWord->updateDate = $wordText->updateDate;
						$langWord->updateEnv = $wordText->updateEnv;
						$langWord->save();
					}
				}
			}
		}
		unset($wordTexts,$lw,$langWordsFromFile);
		
		$cantConnectToMemcache = false;
		$memcache = new Memcache;
		$memcache->connect(MEMCACHE_SERVER, MEMCACHE_PORT) or $cantConnectToMemcache = true;

		if (!$cantConnectToMemcache){
			$memcache->flush();
			$memcache->close();
		}
		Tool::echoLog("lang word compare finished successfully<br /><hr />\n");
	 }

}

