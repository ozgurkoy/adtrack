<?php
	/**
	* twitterInfo real class for Jeelet - firstly generated on 30-09-2011 00:32, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/twitterInfo.php");

	class TwitterInfo extends TwitterInfo_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		public function getKloutTrueReach() {
			if($this->id > 0) {
				$kloutInfo = new TwitterInfoKlout();
				$kloutInfo->customClause = 'twitterInfo='.$this->id;
				$kloutInfo->load();
				return $kloutInfo->trueReach;
			} else {
				return 0;
			}
		}
		
		/**
		 * Create a TwitterHistory record from object
		 * 
		 * @return TwitterHistory
		 * @author Murat
		 */
		public function createHistory(){
			$th = new TwitterHistory();

			$th->timg 	= $this->timg;
			$th->name 			= $this->name;
			$th->screenname 	= $this->screenname;
			$th->location 		= $this->location;
			$th->following 		= $this->following;
			$th->follower 		= $this->follower;
			$th->statusCount 	= $this->statusCount;
			$th->url 			= $this->url;
			$th->description	= $this->description;
			$th->verified 		= $this->verified;
			//$th->listed_count 	= $this->listed_count;
			$th->protected 		= $this->protected;
			$th->lang 			= isset($this->lang)?$this->lang:"";
			$th->user           = $this->user;
			$th->save();
			
			return $th;
		}

		/**
		 * Load TwitterInfoKlout record of publisher
		 *
		 * @return TwitterInfoKlout
		 * @author Murat
		 */
		public function loadTwitterInfoKlout(){
			$tik = new TwitterInfoKlout();
			$tik->twitterInfo = $this->id;
			$tik->load();
			if (!$tik->count){
				$tik->twitterInfo = $this->id;
				$tik->save();
			}
			return $tik;
		}

	}
