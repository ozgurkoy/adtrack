<?php
	/**
	* headliner real class for Jeelet - firstly generated on 12-10-2011 13:43, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/headliner.php");

	class Headliner extends Headliner_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * Get HeadLines Formatted for content
		 *
		 * @return array
		 * @author Murat Hoşver
		 */
		public function getHeadLinesFormatted(){
			$he = new Headliner();
			$he->active = "1";
			$he->orderBy("jorder ASC")->nopop()->load();
			$hez = array();

			while($he->populate()){
				$he->loadImage();
				$t0 = explode("\n",$he->headere);
				$hez[] = array($t0,$he->content,$he->image[0],$he->html);
			}
			return $hez;
		}
	}

	?>