<?php
/**
 * publisherMoneyAction real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherMoneyAction.php';

class PublisherMoneyAction extends PublisherMoneyAction_base
{
	const NOT_ENOUGH_FUNDS = "notEnoughFunds";

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * @param Publisher $publisher
	 * @param           $params
	 * @return bool
	 * @author Özgür Köy - modified
	 */
	public static function addNewMoneyAction( Publisher &$publisher, $params ) {
		JPDO::beginTransaction();
		try {
			$balance = $publisher->recalculateWithdrawableBalance();
			if(!isset($params["paymentOptionId"]))
				$params["paymentOptionId"] = null;

			$defaultTaxRate = Setting::getValue("DEFAULT_PUBLISHER_TAX_RATE");

			$money                 = new PublisherMoneyAction();
			$money->amount         = $params[ "amount" ] * -1;
			$money->amountAfter    = $balance + $money->amount;
			$money->requestDate    = time();
			$money->status         = PublisherMoneyAction::ENUM_STATUS_PENDING;
			$money->lastUpdate     = time();
			$money->actionType     = PublisherMoneyAction::ENUM_ACTIONTYPE_USER_WITHDRAWAL;
			$money->taxRate     = !is_null($defaultTaxRate) ? $defaultTaxRate : 0;
			if (isset( $params[ "note" ] )){
				$money->note           = $params[ "note" ];
			} else {
				if (!is_object($publisher->user_publisher)) $publisher->loadUser();
				if ($publisher->user_publisher->gotValue)
					$money->note = "userwithdrawal-" . $publisher->user_publisher->username;
			}
			if ( !is_null( $params[ "paymentOptionId" ] ) ) {
				$money->paymentOption  = $params[ "paymentOptionId" ];
				$option                = new PublisherPaymentOption( $params[ "paymentOptionId" ] );
				$money->withdrawOption = $option->publisherWithdrawOption;
			}

			if ( $money->save() !== false ) {
				if ( $publisher->savePublisher_publisherMoneyAction( $money->id ) !== false ){
					$publisher->writeHistory(
						UserHistory::ENUM_ACTIONID_REQUEST_WITHDRAWAL,
						(isset($params["note"])?$params["note"]:"Request withdrawal"),
						UserHistory::ENUM_MADEBY_USER,
						"Amount:{$params["amount"]} Option:{$params["paymentOptionId"]}"
					);
					global $SYSTEM_EMAILS;
					if (!is_object($publisher->user_publisher)) $publisher->loadUser();
					Mail::createManual($SYSTEM_EMAILS["pay"],"adMingle New Payment Request - ".date("d/m/y H:i",time()),"<html><head></head><body>Name surname: <b>" . $publisher->getFullName(). " (" . $publisher->user_publisher->username . ")</b><br />Amount: <b>" . Format::getFormattedCurrency($params[ "amount" ]) . "</b></body></html>");
				}
			}
			else
				throw new JError( "can't save money action" . $publisher->id . "params:" . serialize( $params ) );

			JPDO::commit();
		}
		catch ( JError $j ) {
			JPDO::rollback();

			return false;
		}

		return true;
	}

	/**
	 * @param Publisher $publisher
	 * @param           $cost
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function checkPurchase( Publisher $publisher, $storeCategoryId ){
		$woOption = PublisherWithdrawOption::loadByName( "product" );

		#problem with wdo
		if ( $woOption->gotValue !== true ){
			JLog::log( "cri", "Can't find withdrawoption `product`" );
			return false;
		}

		$c = new StoreCategory( $storeCategoryId );

		if($c->containsProducts != 1){
			JLog::log( "cri", "Category does not contain containsProducts({$storeCategoryId})" );
			return false;
		}

		$balance  = $publisher->recalculateWithdrawableBalance();

		if ( $c->price > $balance )
			return self::NOT_ENOUGH_FUNDS;

		return true;
	}

	/**
	 * @param Publisher    $publisher
	 * @param StoreProduct $product
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function purchase( Publisher &$publisher, StoreProduct $product ) {
		$product->loadStoreCategory();

		$woOption              = PublisherWithdrawOption::loadByName( "product" );
		$balance               = $publisher->recalculateWithdrawableBalance();
		$brand                 = StoreCategory::findBrand( $product->products->id );
		$money                 = new PublisherMoneyAction();
		$money->amount         = $product->products->price * -1;
		$money->amountAfter    = $balance + $money->amount;
		$money->requestDate    = time();
		$money->lastUpdate     = time();
		$money->status         = PublisherMoneyAction::ENUM_STATUS_APPROVED;
		$money->note           = $brand->name . " / " . $product->products->name . " - " . $product->code;
		$money->withdrawOption = $woOption->id;
		$money->actionType     = PublisherMoneyAction::ENUM_ACTIONTYPE_USER_COUPON;

		if ( $money->save() !== false ) {
			$publisher->savePublisher_publisherMoneyAction( $money->id );

			return true;
		}
		else {
			return false;
		}
	}

	public function publisherWithdrewAmount($publisherID)
	{
		$results = $this->having("`TOPLAM` IS NOT NULL")->sum( "amount", "TOPLAM", array( "publisher_publisherMoneyAction" => $publisherID,
			"status" => array( PublisherMoneyAction::ENUM_STATUS_APPROVED, PublisherMoneyAction::ENUM_STATUS_PENDING, PublisherMoneyAction::ENUM_STATUS_TRANSFERRING, PublisherMoneyAction::ENUM_STATUS_DOCUMENTS_NEEDED ) ) );
		if ( $this->gotValue ) {
			$sum = $results->TOPLAM;
		}
		else {
			$sum = 0;
		}

		return $sum;
	}

	//
	public function publisherPendingBalance($publisherID)
	{
		$results = $this->having("`TOPLAM` IS NOT NULL")->sum( "amount", "TOPLAM", array( "publisher_publisherMoneyAction" => $publisherID,
			"status" => array( PublisherMoneyAction::ENUM_STATUS_PENDING, PublisherMoneyAction::ENUM_STATUS_TRANSFERRING, PublisherMoneyAction::ENUM_STATUS_DOCUMENTS_NEEDED  ) ) );
		if ( $this->gotValue ) {
			$sum = $results->TOPLAM;
		}
		else {
			$sum = 0;
		}

		return $sum;
	}

	/**
	 * Create excel report for selected filter
	 * 
	 * @return void
	 * @author Murat
	 */
	public function renderExcelReport(){
		$data = array("hasTax"=>false,"rows"=>array());
		do{
			$this->paymentOption->loadBank();
			$row = array(
				"amount" => $this->amount * -1,
				"bank" => $this->paymentOption->bank->bankName,
				"info" => $this->note,
				"gsm" => $this->publisher_publisherMoneyAction->gsmNumberCode . " " . $this->publisher_publisherMoneyAction->gsmNumber,
				"address" => $this->publisher_publisherMoneyAction->address,
				"tax" => 0
			);
			$details = "";
			if ($this->status == PublisherMoneyAction::ENUM_STATUS_APPROVED){
				$details = json_decode($this->paymentOptionHistory);
				if (!count($details)){
					$details = $this->paymentOptionHistory;
					$details = explode("<br/>",$details);
				}
			} else {
				$details = json_decode($this->paymentOption->details);
			}
			$row["accountHolder"] = !empty($details->accountHolder) ? $details->accountHolder : null;
			$account = "";
			if (is_object($details) || is_array($details)){
				foreach($details as $k=>$v){
					if ($k != "accountHolder"){
						if ($k != "accountHolder" && !empty($v)){
							if (is_numeric($k))
								$account .= $v . "\r\n";
							else
								$account .= $k . " : " . $v . "\r\n";
						}
					}
				}
			}
			$row["account"] = $account;
			if (!is_null($this->taxRate) && $this->taxRate > 0){
				$rate = $this->taxRate / 100;
				$row["fullAmount"] = $row["amount"];
				$row["tax"] = $row["amount"] * $rate;
				$row["amount"] = $row["fullAmount"] - $row["tax"];
				if (!$data["hasTax"]) $data["hasTax"] = true;
			}
			$data["rows"][] = $row;
		} while($this->populate());
		$eb = new ExcelBuilder();

		$eb->specialExcelExportForPMA($data);
		$eb->getBinary("PublisherMoneyActionsExport");
	}

	/**
	 * Create excel report for selected filter
	 *
	 * @return void
	 * @author Murat
	 */
	public function renderPaypalReport(){
		$data = array("hasTax"=>false,"rows"=>array());
		do{
			$this->paymentOption->loadBank();
			$row = array(
				"amount" => $this->amount*-1,
				"info" => $this->note,
				"gsm" => $this->publisher_publisherMoneyAction->gsmNumberCode . " " . $this->publisher_publisherMoneyAction->gsmNumber,
				"address" => $this->publisher_publisherMoneyAction->address,
				"tax" => 0
			);
			$details = "";
			if ($this->status == PublisherMoneyAction::ENUM_STATUS_APPROVED){
				$details = json_decode($this->paymentOptionHistory);
				if (!count($details)){
					$details = $this->paymentOptionHistory;
					$details = explode("<br/>",$details);
				}
			} else {
				$details = json_decode($this->paymentOption->details);
			}
			$row["accountHolder"] = !empty($details->accountHolder) ? $details->accountHolder : null;
			$account = "";
			if (is_object($details) || is_array($details)){
				foreach($details as $k=>$v){
					if ($k != "accountHolder" && !empty($v)){
						if (is_numeric($k))
							$account .= $v . "\r\n";
						else
							$account .= $k . " : " . $v . "\r\n";
					}
				}
			}
			$row["account"] = $account;
			if (!is_null($this->taxRate) && $this->taxRate > 0){
				$rate = $this->taxRate / 100;
				$row["fullAmount"] = $row["amount"];
				$row["tax"] = $row["amount"] * $rate;
				$row["amount"] = $row["fullAmount"] - $row["tax"];
				if (!$data["hasTax"]) $data["hasTax"] = true;
			}
			$data["rows"][] = $row;
		} while($this->populate());
		//print_r($data);
		//exit;
		$eb = new ExcelBuilder();

		$eb->specialPaypalExportForPMA($data);
		$eb->getBinary("PublisherMoneyActionsExport");
	}

	public function completeChars($string,$length,$char){
		$nameChars = strlen(utf8_decode($string));
		$a = $length - $nameChars;
		$returnS = "";
		for ($i = 1; $i <= $a; $i++) {
			$returnS = $returnS .$char;
		}

		return $returnS;
	}

	/**
	 * Create Masab file - israeli payment system
	 *
	 * @return void
	 * @author Sam
	 */
	public function renderMasabReport(){
		// Generate File

		$filename = "Masab_".date('Ymd-His').'.masab';


		$PayerId="96408";
		$counter = 1;
		$institution_name = "96408208";
		$serialNUmber = substr("0001".$counter,-3);

		$filePath = "files/masab/".$filename;
		$fp = fopen($filePath, 'w');

		//Generate file first row
		fwrite($fp, 'K');
		// Company name
		fwrite($fp, $institution_name);
		// curncy
		fwrite($fp, '00');
		// Payment Date
		fwrite($fp, date("ymd"));
		// Filter
		fwrite($fp, '0');
		// document serial number
		fwrite($fp,$serialNUmber);
		// Filter
		fwrite($fp, '0');
		// Created Date
		fwrite($fp, date("ymd"));
		// Payer ID
		fwrite($fp, $PayerId);
		// Filter
		fwrite($fp, '000000');
		// Company name
		fwrite($fp, 'adMingle                      ');
		// Filter
		fwrite($fp, '                                                        ');
		// End file
		fwrite($fp, 'KOT');
		fwrite($fp,"\r\n");

		$totalMoney = 0;
		$rowCount = 0;

		do{
			$rowCount++;
			$this->paymentOption->loadBank();
			// Generate Transfer rows

			fwrite($fp, '1');
			fwrite($fp, $institution_name);
			fwrite($fp, '00');
			fwrite($fp, '000000');
			// bank code
			fwrite($fp, $this->paymentOption->bank->bankCode);
			// branch
			$bankData = json_decode($this->paymentOption->details);
			fwrite($fp,$this->completeChars($bankData->sort_code,3,'0'));
			fwrite($fp, $bankData->sort_code);
			// account type
			fwrite($fp, '0000');
			// account number
			fwrite($fp,$this->completeChars($bankData->accountnumber,9,'0'));
			fwrite($fp, $bankData->accountnumber);
			// filter
			fwrite($fp, '0');
			// Publisher id number
			fwrite($fp,$this->completeChars($this->publisher_publisherMoneyAction->identityNo,9,'0'));
			fwrite($fp, $this->publisher_publisherMoneyAction->identityNo);

			// publisher name 16 chars
			$accountName = $this->publisher_publisherMoneyAction->identityNo;// !empty($bankData->accountHolder) ? $bankData->accountHolder : $this->paymentOption->displayName;
			fwrite($fp,$this->completeChars($accountName,16,' '));
			fwrite($fp, $accountName);

			// amount
			$amt = -1 * $this->amount;
			if (!is_null($this->taxRate) && $this->taxRate > 0){
				$rate = $this->taxRate / 100;
				$amt = $amt * (1-$rate);
			}

			$numberDisplay = number_format($amt, 2, '', '');
			fwrite($fp,$this->completeChars($numberDisplay,13,'0'));
			fwrite($fp, $numberDisplay);
			$totalMoney = $totalMoney+$amt;

			// publisher ID
			$this->publisher_publisherMoneyAction->loadUser();
			$publisherID = $this->publisher_publisherMoneyAction->id."_".$this->publisher_publisherMoneyAction->user_publisher->username;
			fwrite($fp,$this->completeChars($publisherID,20,'0'));
			fwrite($fp, $publisherID);

			// payment dates
			fwrite($fp, '00000000');

			fwrite($fp, '000');

			//type
			fwrite($fp, '006');

			// filter
			fwrite($fp, '000000000000000000');

			fwrite($fp, '  ');
			fwrite($fp,"\r\n");

		} while($this->populate());

		// Generate total

		fwrite($fp, '5');
		fwrite($fp,$institution_name);
		fwrite($fp, '00');
		fwrite($fp, date("ymd"));
		fwrite($fp, '0');
		fwrite($fp, $serialNUmber);
		// total
		$totalNumberDisplay = number_format($totalMoney, 2, '', '');
		fwrite($fp,$this->completeChars($totalNumberDisplay,15,'0'));
		fwrite($fp,$totalNumberDisplay);

		//filter
		fwrite($fp, '000000000000000');

		// total records
		fwrite($fp,$this->completeChars($rowCount,7,'0'));
		fwrite($fp, $rowCount);//$rowCount
		fwrite($fp, '0000000');
		fwrite($fp, '                                                               ');
		fwrite($fp,"\r\n");

		// Send the file

		fclose($fp);

		if (file_exists($filePath)) {

			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
			header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
			header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
			header("Content-Transfer-Encoding: binary\n");
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Length: ' . filesize($filePath));
			ob_clean();
			flush();
			readfile($filePath);
			exit;
		}

	}

	/**
	 * Get related campaigns for this publisher money action
	 *
	 * @return array
	 * @author Murat
	 */
	public function getRelatedCampaigns(){
		$startDate = 0;

		$pma = new PublisherMoneyAction();
		$pma->orderBy("id DESC")->load(array("id" => "&lt " . $this->id,"status" => PublisherRevenue::ENUM_STATUS_APPROVED,"publisher_publisherMoneyAction"=>$this->publisher_publisherMoneyAction));
		if ($pma->gotValue) $startDate = $pma->requestDate;

		//die($this->id . " - " .$pma->id);

		$pr = new PublisherRevenue();
		//JPDO::debugQuery(true);
		$pr->populateOnce(false)->with("campaign_publisherRevenue")->orderBy("id ASC")->load(array("status" => PublisherRevenue::ENUM_STATUS_APPROVED,"approveDate"=>"BETWEEN {$startDate},{$this->requestDate}","publisher_publisherRevenue"=>$this->publisher_publisherMoneyAction));
		//exit;
		$campaigns = array();
		if ($pr->gotValue){
			do {
				$campaigns[] = array(
					"cid" => $pr->campaign_publisherRevenue->id,
					"title" => $pr->campaign_publisherRevenue->title,
					"revenue" => $pr->amount,
					"approveDate" => $pr->approveDate
				);
			} while($pr->populate());
		}
		return $campaigns;
	}

	/**
	 * Excel report for all data
	 *
	 * @return void
	 * @author Murat
	 */
	public function renderExcelReportAll(){
		$data = array();
		$taxRate = Setting::getValue("DEFAULT_PUBLISHER_TAX_RATE");
		if ($this->gotValue) {
			do {
				$row = array(
					"Payment ID"       => $this->id,
					"Paymet Type"      => $this->paymentOption->publisherWithdrawOption->name,
					"Username"         => $this->publisher_publisherMoneyAction->user_publisher->username,
					"Name"             => $this->publisher_publisherMoneyAction->name,
					"Surname"          => $this->publisher_publisherMoneyAction->surname,
					"Request Date"     => date("Y-m-d H:i", $this->jdate),
					"Publisher ID"     => $this->publisher_publisherMoneyAction->identityNo,
					"Parent Approval" => date("Y", $this->publisher_publisherMoneyAction->bday) > (date("Y") - 18) ? "Yes" : ($this->publisher_publisherMoneyAction->hasParentApproval ? "Yes" : "No"),
					"Full Amount"      => round($this->amount, 3) * -1
				);
				if (!empty($taxRate) && $taxRate) {
					$row["Tax Rate"]   = $this->taxRate;
					$row["Tax Amount"] = round($this->amount * ($this->taxRate / 100), 3);
					$row["Net Value"]  = $row["Full Amount"] - $row["Tax Amount"];
				}
				if ($this->status == PublisherMoneyAction::ENUM_STATUS_APPROVED) {
					$row["Details"] = $this->paymentOptionHistory;
				} else {
					$details = "";
					foreach (json_decode($this->paymentOption->details,true) as $k=>$v)
						$details .= $k . " : " . $v . "\r\n";
					$row["Details"] = trim($details);
				}
				if (strtolower($this->paymentOption->publisherWithdrawOption->name) == "bank"){
					$row["Bank Name"] = $this->paymentOption->bank->bankName;
				} elseif (strtolower($this->paymentOption->publisherWithdrawOption->name) == "paypal"){
					$row["Bank Name"] = "PayPal";
				}
				$data[] = $row;
			} while ($this->populate());
			//print_r($data);
			//exit;
			$eb = new ExcelBuilder();
			$eb->convertArrayToExcelFile($data,"PublisherMoneyActionReportAll");
		}
	}





}

