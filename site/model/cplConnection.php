<?php
/**
 * cplConnection real class - firstly generated on 26-03-2014 12:46, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplConnection.php';

class CplConnection extends CplConnection_base
{
	public $pfield      = null;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function findMaxOrder($id) {
		$c = new CplConnection();
		$c->max("forder","fmax", array("fields"=>$id) );

		return $c->fmax;
	}

	public function parseOptions() {
		if ( $this->gotValue ) {
			$e             = json_decode( $this->options );
			$this->pfield  = $e->pfield;
			$this->options = $e->options;
		}
	}

}

