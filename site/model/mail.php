<?php
/**
 * mail real class - firstly generated on 10-12-2013 14:30, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/mail.php';

class Mail extends Mail_base
{
	/**
	 * @param null $id
	 */
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * send to rabbitmq queue
	 *
	 * @return void
	 * @author Murat Hosver
	 **/
	public static function sendToRabbitMQ($withoutCheck=false)
	{
		global $debugLevel;
		$filter = array(
			"sentDate" => "IS NULL",
			"state" => array(Mail::ENUM_STATE_WAITING,Mail::ENUM_STATE_ERROR),
			"postid" => "IS NULL",
			"_custom" => "`cronStart` IS NULL OR `cronStart` < " . (time()-600)
		);

		$cronStart = time();
		$iterationLabel = date("Y-m-d H:i:s",$cronStart);

		$m = new Mail();
		$m->update($filter,array("cronStart"=>$cronStart));
		$cnt = JPDO::count();
		if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => Waiting mail count: " . $cnt);
		$filter = array("cronStart"=>$cronStart);

		if ($cnt > 100){ //If the waiting mail count is greater than 100 then process them on backend
			if($debugLevel > 0) Tool::echoLog("Waiting mail count is greater than 100: " . $cnt);

			$per = 500;
			$cid = 0;
			$label = "Date:".$iterationLabel." Cnt:".$cnt;
			$job = "sendToRabbitMQ";
			$b = Backend::createSupervisionTask( $label, $job );

			if($debugLevel > 0) Tool::echoLog("Backend process created: " . $label . " for cronStart: " . $cronStart);

			while($cid<$cnt){
				$mm = new Mail();
				$mm->limit( $cid, 1 )->load( $filter );
				if (!$mm->gotValue)
					break;
				$b->addChildTask(
					$label."-Time:".time(),
					$job,
					array( "startId" => $mm->id, "cronStart" => $cronStart, "count"=>$per )
				);

				if($debugLevel > 0) Tool::echoLog("Backend child created: " . $mm->id . "-" . $per . " under " . $label);
				$cid += $per;
			}
			$b->runTask();
		} else {
			$m = new Mail();
			$m->limitCount = 10000;
			$m->orderBy("reqDate")->populateOnce(false)->load($filter);

			if ($m->gotValue){
				$rmq = new RabbitMQ();
				$rmq->declareQueue(RMQ_QUEUE_EMAIL);

				$mcount = 0;
				do {
					if($debugLevel > 0) Tool::echoLog("{$iterationLabel} | {$mcount} => email state of " . $m->id . " is " . $m->state_options[$m->state]);

					$m->state = Mail::ENUM_STATE_QUEUED;
					$m->save();

					$msg_body = json_encode(array("id" => $m->id,"type" => "email"));
					$rmq->publishMessage($msg_body,RMQ_QUEUE_EMAIL);

					if($debugLevel > 0) Tool::echoLog("{$iterationLabel} | {$mcount} => email queued: " . $m->id);
					$mcount++;
				}while($m->populate());

				if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => $mcount email(s) queued");
				$rmq->close();
			} else {
				if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => No mail found");
			}
			unset($m);
		}

		if (!$withoutCheck){
			Tool::echoLog("Checking mail hooks");
			$filter = array(
				"state" => MailHook::ENUM_STATE_RECEIVED,
				"_custom" => "`cronStart` IS NULL OR `cronStart` < " . (time()-600)
			);
			$mh = new MailHook();
			$mh->update($filter,array("cronStart"=>$cronStart));

			$counter = new MailHook();
			$counter->count("id","cnt",$filter);
			if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => Waiting mailHook count: " . JPDO::count());

			$filter = array("cronStart"=>$cronStart);

			$mh = new MailHook();
			$mh->populateOnce(false)->load($filter);
			if ($mh->gotValue){
				$rmq = new RabbitMQ();
				$rmq->declareQueue(RMQ_QUEUE_HOOK);
				$mcount = 0;
				$ids = array();
				do{
					$mh->state = MailHook::ENUM_STATE_QUEUED;
					$mh->save();

					$msg_body = json_encode(array("id" => $mh->id,"type" => "mailHook"));
					$rmq->publishMessage($msg_body,RMQ_QUEUE_HOOK);
					if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => email hook queued: " . $mh->id);
					$mcount++;
				} while($mh->populate());
				if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => $mcount email hook(s) queued");
			} else {
				if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => No mail hook found");
			}
			unset($mh);
		}
	}

	/**
	 * Create new mail record
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function create( $user, $to, $titleVariables, $messageVariables, $langMessageHistoryID, $campaign=null, $imageSrc=null ) {
		$m                          = new Mail();
		$m->user_mail               = $user;
		$m->receipent               = $to;
		$m->titleVariables          = json_encode( $titleVariables );
		$m->messageVariables        = json_encode( $messageVariables );
		$m->langMessageHistory_mail = $langMessageHistoryID;
		$m->fromTemplate            = 1;
		$m->state                   = Mail::ENUM_STATE_WAITING;
		$m->tryCount                = 0;
		$m->clickCount              = 0;
		$m->reqDate                 = time();
		$m->mail_campaign           = $campaign;
		$m->imageSrc                = $imageSrc;
		$m->save();
	}

	/**
	 * Create mail from scratch
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $content
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function createManual($to,$subject,$content){
		$m = new Mail();
		$m->user_mail = null;
		$m->receipent = $to;
		$m->subject = $subject;
		$m->message = $content;
		$m->fromTemplate = 0;
		$m->state = Mail::ENUM_STATE_WAITING;
		$m->tryCount = 0;
		$m->clickCount = 0;
		$m->reqDate = time();
		$m->save();
	}

	/**
	 * Send mail function
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $content
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public static function sendMail($to,$subject,$content){
		$mail = array(
			"key" => Setting::getValue("EMAIL_MANDRILL_KEY"),
			"message" => array(
				"subject" => $subject,
				"html" => $content,
				"from_email" => Setting::getValue("EMAIL_FROM_ADDRESS"),
				"from_name" => Setting::getValue("EMAIL_FROM_NAME"),
				"to" => array(
					array(
						"email" => $to,
						"type" => "to"
					)
				),
				"headers" => array(
					"Reply-To" => Setting::getValue("EMAIL_NOREPLY_ADDRESS")
				),
				"track_opens" => (Setting::getValue("EMAIL_TRACK_READ") == "Y" ? true : false),
				"track_clicks" => (Setting::getValue("EMAIL_TRACK_CLICK") == "Y" ? true : false)
			)
		);

		$request = new RestReq(Setting::getValue("EMAIL_MANDRILL_API_URL") . "messages/send.json", "POST", json_encode($mail),"raw");
		$request->execute();
		//echo '<pre>' . print_r($request, true) . '</pre>';
		//exit;
		$data = json_decode($request->responseBody);

		$result = array(
			"sent" => false,
			"reason" => null
		);

		if (!is_array($data) && !is_object($data)){
			$result["reason"] = "InternalServerError";
		} else {
			if (is_array($data)) $data = $data[0];
			switch($data->status){
				case "queued":
				case "sent":
					$result = array(
						"sent" => true,
						"_id" => $data->_id
					);
					break;
				case "invalid":
					$result = array(
						"sent" => false,
						"reason" => "invalid",
						"data" => $data
					);
					break;
				case "rejected":
					$result = array(
						"sent" => false,
						"reason" => "rejected",
						"data" => $data
					);
					break;
				default:
					$result = array(
						"sent" => false,
						"reason" => "UnhandledError",
						"data" => $data
					);
			}
		}

		unset($mail,$request,$data);

		return $result;
	}

	/**
	 * Send mail from mail table
	 *
	 * @param int $id
	 */
	public static function send($id){
		$m = new Mail($id);
		$response = false;
		global $debugLevel;
		if ($m->gotValue){
			//if (1==1){
			if ($m->state == Mail::ENUM_STATE_QUEUED && is_null($m->postid)){
				$m->state = Mail::ENUM_STATE_PROCESSING;
				$m->save();

				$subject = $m->subject;
				$body = $m->message;

				if ($m->fromTemplate){
					/*
					$m->loadLangMessageHistory_mail();
					$subject = Format::replaceStringParam(json_decode($m->titleVariables),$m->langMessageHistory_mail->messageTitle);
					$body = Format::replaceStringParam(json_decode($m->messageVariables),$m->langMessageHistory_mail->messageBody);

					$m->langMessageHistory_mail->loadLangMessage_langMessageHistory()->loadLang();

					JT::init();
					JT::assign( "words", LangWord::collectWords($m->langMessageHistory_mail->langMessage_langMessageHistory->langShortDef, array( "Mail" ) ) );

					if (!in_array($m->langMessageHistory_mail->messageCode,array("deactivationInformationMail","V2_unsubscribeMail"))){
						$unsubscribeCode = json_encode(array("email"=>$m->receipent,"salt"=>JUtil::randomString(5)));
						if ($debugLevel >= 100) Tool::echoLog("original unsubscribeCode : $unsubscribeCode");
						$unsubscribeCode = JUtil::hardEncrypt($unsubscribeCode);
						if ($debugLevel >= 100) Tool::echoLog("encrypted unsubscribeCode : $unsubscribeCode");
						JT::assign("unsubscribeCode",$unsubscribeCode);
					}

					JT::assign("viewLink",JUtil::hardEncrypt(json_encode(array("id"=>$m->id,"salt"=>JUtil::randomString(5)))));
					JT::assign("title",$subject);
					JT::assign("content",nl2br($body));
					JT::assign("footer",is_null($m->langMessageHistory_mail->footerCode) ? "footer.both" : $m->langMessageHistory_mail->footerCode);
					$rtl = "";
					if($m->langMessageHistory_mail->langMessage_langMessageHistory->loadLang()->RTL == 1 ) $rtl = "RTL";
					if (strlen($m->imageSrc) > 0) JT::assign("imageSrc",$m->imageSrc);
					$body = JT::pfetch("mail_new{$rtl}");
					if ($debugLevel >= 100) Tool::echoLog($body);
					*/
					$body = $m->renderMailHTML($subject);
					//die($body);
				}
				$result = Mail::sendMail($m->receipent,$subject,$body);
				$m->tryCount++;
				if ($result["sent"] === true){
					$m->state = Mail::ENUM_STATE_SUCCESSFUL;
					$m->postid = $result["_id"];
					$m->sentDate = time();
					$m->save();
					$response = true;
					if ($debugLevel > 0) Tool::echoLog("#{$id} ok: " . json_encode($result));
				} else {
					if ($debugLevel > 0) Tool::echoLog("#{$id} nok: " . json_encode($result));
					switch($result["reason"]){
						case "invalid":
						case "rejected":
							$m->errorData = json_encode($result["data"]);
							$m->state = Mail::ENUM_STATE_CANCELED;
							$m->save();
							UserEmail::markAsInvalid($m->receipent);
							$response = true;
							break;
						case "UnhandledError":
						default:
							$m->errorData = json_encode($result["data"]);
							if ($m->tryCount <= 10){
								$m->state = Mail::ENUM_STATE_ERROR;
								$m->cronStart = null;
							} else {
								$m->state = Mail::ENUM_STATE_CANCELED;
							}
							$m->save();
							$response = true;
							break;
					}
				}
			} else {
				if ($debugLevel > 0) Tool::echoLog("#{$id} this mail sent before. Current state: " . $m->state_options[$m->state] . " postid: " . $m->postid);
				$response = true;
			}
		}

		return $response;
	}

	/**
	 * Load mail row with postid
	 *
	 * @param string $postid
	 *
	 * @return Mail|null
	 *
	 * @author Murat
	 */
	public static function loadWithId($postid){
		$m = new Mail();
		$m->load(array("postid" => $postid));
		if ($m->gotValue){
			return $m;
		} else {
			return null;
		}
	}

	/**
	 * Change mail read state
	 *
	 * @param string $postid
	 * @param int $state
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function changeReadState($postid,$state){
		$dbConfig = JCache::read( 'JDSN.MDB' );
		$table = '"' . $dbConfig["schema"] . '"."';
		$sql = 'UPDATE ' . $table . 'mail" SET "readStateChange"=' . time() . ',"readState"=' . $state . ' WHERE "mail"."postid"=\'' . $postid . '\'';
		JPDO::executeQuery($sql);
	}

	/**
	 * Count click for mail
	 *
	 * @param string $postid
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function countClick($postid){
		$m = self::loadWithId($postid);
		if (!is_null($m)){
			if (is_null($m->clickCount)) $m->clickCount = 0;
			$m->clickCount++;
			$m->save();
		}
		unset($m);
	}

	/**
	 * Render mail body html
	 *
	 * @return string
	 * @author Murat
	 */
	public function renderMailHTML(&$subject = null, $withUnsubscribe = true, $withHTMLLink = true){
		global $debugLevel;

		$this->loadLangMessageHistory_mail();
		$subject = Format::replaceStringParam(json_decode($this->titleVariables),$this->langMessageHistory_mail->messageTitle);
		$body = Format::replaceStringParam(json_decode($this->messageVariables),$this->langMessageHistory_mail->messageBody);

		$this->langMessageHistory_mail->loadLangMessage_langMessageHistory()->loadLang();

		JT::init();
		JT::assign( "words", LangWord::collectWords($this->langMessageHistory_mail->langMessage_langMessageHistory->langShortDef, array( "Mail" ) ) );

		if ($withUnsubscribe && !in_array($this->langMessageHistory_mail->messageCode,array("deactivationInformationMail","V2_unsubscribeMail"))){
			$unsubscribeCode = json_encode(array("email"=>$this->receipent,"salt"=>JUtil::randomString(5)));
			if ($debugLevel >= 100) Tool::echoLog("original unsubscribeCode : $unsubscribeCode");
			$unsubscribeCode = JUtil::hardEncrypt($unsubscribeCode);
			if ($debugLevel >= 100) Tool::echoLog("encrypted unsubscribeCode : $unsubscribeCode");
			JT::assign("unsubscribeCode",$unsubscribeCode);
		}

		if ($withHTMLLink) JT::assign("viewLink",JUtil::hardEncrypt(json_encode(array("id"=>$this->id,"salt"=>JUtil::randomString(5)))));
		JT::assign("title",$subject);
		JT::assign("content",nl2br($body));
		JT::assign("footer",is_null($this->langMessageHistory_mail->footerCode) ? "footer.both" : $this->langMessageHistory_mail->footerCode);
		$rtl = "";
		if($this->langMessageHistory_mail->langMessage_langMessageHistory->loadLang()->RTL == 1 ) $rtl = "RTL";
		if (strlen($this->imageSrc) > 0) JT::assign("imageSrc",$this->imageSrc);
		$body = JT::pfetch("mail_new{$rtl}");
		if ($debugLevel >= 100) Tool::echoLog($body);

		return $body;
	}


}

