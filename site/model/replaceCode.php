<?php
	/**
	* replaceCode real class for Jeelet - firstly generated on 22-11-2011 13:20, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/replaceCode.php");

	class ReplaceCode extends ReplaceCode_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * Kill old codes for the user's specific type
		 * @param User $user
		 * @param      $type
		 * @author Özgür Köy
		 */
		public static function killOld( User &$user, $type ){
			$r = new ReplaceCode();
			$r->update( array( "user_replaceCode"=>$user->id, "type"=>$type ), array("status"=>ReplaceCode::ENUM_STATUS_CANCELLED) );

			unset( $r );
		}

		/**
		 * Kill old codes for the user's specific type
		 * @param User $user
		 * @param      $type
		 * @author Özgür Köy
		 */
		public static function usedCode( User &$user, $code ){
			$r = new ReplaceCode();
			$r->update( array( "user_replaceCode"=>$user->id, "repCode"=>$code ), array("status"=>ReplaceCode::ENUM_STATUS_USED) );

			unset( $r );
		}


		/**
		 * Get code obj if valid
		 *
		 * @param $type
		 * @param $code
		 * @return bool|ReplaceCode
		 * @author Özgür Köy
		 */
		public static function isCodeValid( $type, $code ) {
			$r = new ReplaceCode();

			$r->with("user_replaceCode")->load( array( "type"=>$type, "repCode"=>$code, "status"=> ReplaceCode::ENUM_STATUS_PENDING, "expiredDate"=>"&ge ".time()) );
			return $r->gotValue ? $r : false;
		}

		/**
		 * @param User $user
		 * @param      $type
		 * @return string
		 * @author Özgür Köy
		 */
		public static function getCode( User &$user, $type ) {
			#kill old
			ReplaceCode::killOld( $user, $type );

			$c = new ReplaceCode();
			$c->user_replaceCode = $user->id;
			$c->repCode          = JUtil::randomString( 10 );
			$c->repDate          = time();
			$c->expiredDate      = time() + (7*24*3600);
			$c->type             = $type;
			if($c->save() !== false){
				return $c->repCode;
			}

		}
	}
?>