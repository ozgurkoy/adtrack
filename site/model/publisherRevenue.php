<?php
/**
 * publisherRevenue real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherRevenue.php';

class PublisherRevenue extends PublisherRevenue_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * load by custom ref id
	 *
	 * @param $cid
	 * @return $this
	 * @author Özgür Köy
	 */
	public function loadByCustomId( $cid ) {
		$this->load(
			array( "customId" => $cid )
		);

		return $this;

	}

}

