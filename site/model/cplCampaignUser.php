<?php
/**
 * cplCampaignUser real class - firstly generated on 04-04-2014 12:04, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplCampaignUser.php';

class CplCampaignUser extends CplCampaignUser_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function isCodeUnique( $code ) {
		$c = new CplCampaignUser();
		$c->load( array( "code" => $code ) );
		$r = $c->gotValue;
		unset( $c );
		return !$r;
	}

	public static function loadByCampaignHash( $preHash, $code = null ) {
		if ( is_null( $code ) || !( strlen( trim( $code ) ) > 0 ) )
			$c = CplCampaignUser::loadByProperty( "code", $preHash );
		else
			$c = CplCampaignUser::loadByProperty( "code", $code );

		if ( $c->gotValue ) {
			$ret = array(
				"campaign"  => $c->loadCplCampaign(),
				"ccu"       => $c->id
			);

			unset( $c );

			return $ret;
		}
		else
			return false;

	}

	/**
	 * load by property
	 *
	 * @param $property
	 * @param $value
	 * @author Özgür Köy
	 */
	public static function loadByProperty( $property, $value ) {
		//TODO : apply cache here
		$c = new CplCampaignUser();
		$c->load( array( $property => $value ) );

		return $c;
	}

	/**
	 * load campaign by code
	 *
	 * @param $code
	 * @return bool|cplCampaign
	 * @author Özgür Köy
	 */
	public static function loadCampaignByCode( $code, $onlyCampaign = false ) {
		$c = new CplCampaignUser();
		$c->load( array( "code" => $code ) );
		if ( $c->gotValue ) {
			if ( $onlyCampaign === false )
				$p = $c->loadCplCampaignUser_publisher();
			else
				$p = null;

			$ret = array( "campaign" => $c->loadCplCampaign(), "publisher" => $p===false?null:$p, "ccu"=>$c->id );
			unset( $c );

			return $ret;
		}
		else
			return false;
	}


/*TODO:GET RID OF THIS
	moved to campaign*/
	public static function getShortLink( $hash ) {
		$url = Tool::callSLService(
			"shortenURL",
			array(
				"params" => json_encode(
					array(
						"url" => Setting::getValue( "CPL_LANDING_DOMAIN" ).(substr(Setting::getValue( "CPL_LANDING_DOMAIN" ),-1)=="/"?"":"/"). $hash
					)
				)
			)
		);

		if ( is_object($url) && isset($url->code) && is_string( $url->code ) )
			//todo@Özgür:Implement this
			return Setting::getValue( "SHORTDOMAIN" ) . "/" . $url->code;
		else
			return false;
	}
}

