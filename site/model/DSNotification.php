<?php
/**
 * DSNotification real class - firstly generated on 23-05-2014 19:25, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/DSNotification.php';

class DSNotification extends DSNotification_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * add notification
	 *
	 * @param      $user
	 * @param      $title
	 * @param      $note
	 * @param null $link
	 * @param bool $doEmail
	 * @author Özgür Köy
	 */
	public static function addNotification( $userId, $title, $note, $type=DSNotification::ENUM_TYPE_CHECKOUT, $link = null, $doEmail = true ) {
		global $_S;

		$d        = new DSNotification();
		$d->title = $title;
		$d->note  = $note;
		$d->link  = $link;
		$d->type  = $type;
		$d->save();

		$u = new DSuser($userId);
		$u->saveNots( $d );

	}

}

