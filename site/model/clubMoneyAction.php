<?php
/**
 * clubMoneyAction real class - firstly generated on 17-04-2014 11:58, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/clubMoneyAction.php';

class ClubMoneyAction extends ClubMoneyAction_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * create cma from publishermoneyaction
	 *
	 * @param PublisherMoneyAction $pma
	 * @return void
	 * @author Murat
	 */
	public static function createFromPMA(PublisherMoneyAction $pma){
		$c = new Club($pma->publisher_publisherMoneyAction->ownerClub);
		if (!$c->gotValue) return;

		$pma->publisher_publisherMoneyAction->loadUser();

		$cma = new ClubMoneyAction();

		$cma->load(array(
			"publisherMoneyAction_clubMoneyAction" => $pma->id
		));
		if (!$cma->gotValue){
			$cma->actionType = ClubMoneyAction::ENUM_ACTIONTYPE_INCOME;
			$cma->paymentDate = $pma->paymentDate;
			$cma->publisherMoneyAction_clubMoneyAction = $pma->id;
			$cma->note = $pma->publisher_publisherMoneyAction->user_publisher->username;
			$cma->club_clubMoneyAction = $c->id;
		}
		$cma->amount = ($pma->amount * $c->commissionRate / 100)*(-1);
		$cma->save();
	}

	/**
	 * Overwrite save function to createHistory
	 */
	public function save(){
		if ($this->gotValue){
			$cmah = new ClubMoneyActionHistory();
			$fields = array("actionType","paymentDate","accountDetail","amount","note","receiptInfo","deleted");
			foreach ($fields as $f){
				$cmah->$f = $this->$f;
			}
			$cmah->clubMoneyActionHistories = $this->id;
			$cmah->save();
		}
		parent::save();
	}

}

