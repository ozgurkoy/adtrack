<?php
/**
 * cplBadWords real class - firstly generated on 29-05-2014 11:21, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplBadWords.php';

class CplBadWords extends CplBadWords_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * check word exists in db
	 *
	 * @param $word
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function checkWordExists( $string ) {
		$string = explode( " ", preg_replace( "|[\t\n]|", "", $string ) );
		$words  = array();

		foreach ( $string as $str ) {
			$words[] = trim( strtolower( $str ) );
		}

		$c = new CplBadWords();
		$c->load( array( "string" => $words ) );
		$r = $c->gotValue;

		return $r;
	}

}

