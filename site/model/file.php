<?php
/**
 * file real class - firstly generated on 26-10-2014 20:28, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/file.php';

class File extends File_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function newFile( $name ) {
//		$_FILES['userfile']['name'][0]
		global $__DP;
		$ids = array();
		if ( !isset( $_FILES[ $name ] ) )
			return $ids;
		foreach ( $_FILES[ $name ][ "name" ] as $fi => $f1 ) {
			if ( strlen( $_FILES[ $name ][ "name" ][ $fi ] ) == 0 )
				continue;

			$newName = JUtil::randomString( 5 ) . JUtil::normalize($_FILES[ $name ][ "name" ][ $fi ]);
			$newPath = $__DP . "files/" . $newName;

			move_uploaded_file( $_FILES[ $name ][ "tmp_name" ][ $fi ], $newPath );
			$ids[ ] = self::placeFile( $newName, $_FILES[ $name ][ "type" ][ $fi ] );
		}

		return $ids;

	}

	public static function putFile( &$data, $name, $type ) {
		global $__DP;
		$newName = JUtil::randomString( 5 ) . JUtil::normalize($name);

		#google blob parts
		if( !(preg_match("/.+?\\.[a-z]{2,}+$/uiUm", $newName)>0) ){
			$newName .= ".png";
		}

		$newPath = $__DP . "files/" . $newName;

		if( file_put_contents( $newPath, $data ) !== false ){
			return self::placeFile( $newName, $type );
		}

		return false;
	}

	public static function placeFile( $name, $type ) {
		$f           = new File();
		$f->filename = $name;
		$f->fileType = $type;
		$f->save();

		return $f->id;
	}
}

