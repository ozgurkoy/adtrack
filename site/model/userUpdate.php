<?php
/**
 * userUpdate real class for Jeelet - firstly generated on 27-02-2012 10:24, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 * @author Özgür Köy
 **/
include($__DP."/site/model/base/userUpdate.php");

class UserUpdate extends UserUpdate_base
{

	public function __construct($id=null)
	{
		parent::__construct($id);
	}

	/**
	 * Marks row as added to queue
	 *
	 * @param integer $userId
	 * @param string $kredStatus
	 */
	public static function updateKredStatus($userId,$kredStatus) {
		if(JPDO::connect( JCache::read('JDSN.MDB') )) {
			$sql = "UPDATE `userUpdate` SET `kredUpdateDate` = UNIX_TIMESTAMP(), `kredStatus`='$kredStatus' WHERE `user`=$userId";
			JPDO::executeQuery($sql);
		}
	}

}
