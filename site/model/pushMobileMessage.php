<?php
/**
 * pushMobileMessage real class - firstly generated on 08-01-2014 23:13, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/pushMobileMessage.php';

class PushMobileMessage extends PushMobileMessage_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	/**
	 * Create a push notification queue message
	 *
	 * @param User $usr
	 * @param array|null $HeaderVariables
	 * @param array|null $messageVariables
	 * @param int|null $langMessageHistoryID
	 * @param int|null $campaign
	 * @param string|null $customTitle
	 * @param string|null $customMessage
	 * @return void
	 * @auhor Murat
	 */
	public static function create(User &$usr, $HeaderVariables=null,$messageVariables=null, $langMessageHistoryID=null, $campaign=null,$customTitle=null,$customMessage=null){
		//$alert, $type, $did
		$publisher = is_object($usr->user_publisher) && $usr->user_publisher->gotValue ? $usr->user_publisher : $usr->loadPublisher();
		if(is_object($publisher)){
			//$publisher = new Publisher();
			//$publisher->id;

			$devices = new PublisherMobileDevice();
			//JPDO::debugQuery(true);
			$devices->load(array("publisher_publisherMobileDevice"=>$publisher->id,
								 "status"=>PublisherMobileDevice::ENUM_STATUS_ACTIVE));
			//exit;
			do{
				if(!empty($devices->pushKey)){
					$push = new PushMobileMessage();
					$push->publisherMobileDevice_pushMobileMessage = $devices;
					$push->state = PushMobileMessage::ENUM_STATE_WAITING;
					$push->tryCount = 0;
					$push->sendTime = time();
					$push->lastUpdate = time();
					$push->langMessageHistory_pushMobileMessage = $langMessageHistoryID;
					$push->pushMobileMessage_campaign = $campaign;
					$push->titleVariables          = is_array($HeaderVariables) ? json_encode( $HeaderVariables ) : null;
					$push->messageVariables        = is_array($messageVariables) ? json_encode( $messageVariables ) : null;
					$push->customMessageTitle = $customTitle;
					$push->customMessage = $customMessage;

					/*
					$message = new LangMessageHistory($langMessageHistoryID);

					$type = "m";
					$itemId = $sysmsg;
					if(is_null($campaign)){
						// todo: Sup types
						$type = "c";
						$itemId = $campaign;
						$push->pushMobileMessage_campaign = $campaign;
					}

					$payloadParams = array(
						"header" => "header",//Format::replaceStringParam($HeaderVariables,$message->messageTitle),
						"body" => "body",//Format::replaceStringParam($messageVariables,$message->messageTitle),
						"type" => $type,
						"itemId" => $itemId
					);
					$push->payload = self::getPayload($devices->devicePlatform,$payloadParams);
					*/

					// Check device Type
					if($devices->type == PublisherMobileDevice::ENUM_TYPE_ANDROID)
					{
						$push->type = PushMobileMessage::ENUM_TYPE_ANDROID;
					}elseif($devices->type == PublisherMobileDevice::ENUM_TYPE_IOS){
						$push->type = PushMobileMessage::ENUM_TYPE_IOS;
					}else{
						JLog::log("push","unknown mobile platform ".print_r($devices,true),1);
					}

					// Check notification type
					if($campaign != null && $devices->enableNotification){
						$push->save();

					}elseif($devices->enableMessagesNotification){

						$push->save();
					}

					unset($push);
				}

			}while($devices->populate());
		}
	}

	private static function getPayload($deviceType,$payloadParams){

		$payload = "";

		switch($deviceType){
			case PushMobileMessage::ENUM_TYPE_ANDROID:
				/*
				$payload = json_encode(array(
					"header" => $payloadParams["header"],
					"type" => $payloadParams["type"],
					"itemId" => $payloadParams["itemId"]
				));
				*/
				$payload = array(
					"title" => $payloadParams["header"],
					"data" => array(
						"type" => $payloadParams["type"],
						"cid" => $payloadParams["itemId"]
					)
				);
				break;
			case PushMobileMessage::ENUM_TYPE_IOS:
				/*
				$payload = json_encode(array(
					"aps" => array("alert"=>$payloadParams["header"],
									"header"=>"OK",
									"sound"=>"default"),
					"type" => $payloadParams["type"],
					"itemId" => $payloadParams["itemId"]
				));
				*/
				if (mb_strlen($payloadParams["header"]) > 190) $payloadParams["header"] = mb_substr($payloadParams["header"],0,190) . "...";
				$payload = json_encode(array(
					"aps" => array(
						"alert" => $payloadParams["header"],
						"sound" => "default"
					),
					"type" => $payloadParams["type"],
					"cid" => $payloadParams["itemId"]
				));
				break;
			default:
				JLog::log("push","unknown mobile platform ".$deviceType,1);
				break;

		}

		return $payload;

	}


	/**
	 * Marks row as given state
	 *
	 * @param state PushMobileMessage::ENUM_STATE
	 */
	public static function changeState($id,$state) {
		$push = new PushMobileMessage($id);
		if($push->gotValue){

			//$push->state = PushMobileMessage::ENUM_STATE_CANCELLED;
			$push->state = $state;
			$push->save();

		}else{
			JLog::log("push","Updating state Failed for ID: ".$id,1);
		}
	}


	public static function send($id){

		$pmm = new PushMobileMessage();
		//$pmm->with("publisherMobileDevice_pushMobileMessage")->load(array($id));
		$pmm->with(
			array(
				"publisherMobileDevice_pushMobileMessage",
				Publisher::NO_LEFT_JOIN
			),
			array(
				"pushMobileMessage_campaign",
				Publisher::DO_LEFT_JOIN
			)
		)->load(array($id));
		if($pmm->gotValue){

			$response = false;
			$type = "m";
			$itemId = null;
			if(is_object($pmm->pushMobileMessage_campaign) && $pmm->pushMobileMessage_campaign->gotValue){
				$type = "c";
				$itemId = $pmm->pushMobileMessage_campaign->publicID;
			}

			$payloadParams = array(
				"header" => $pmm->getTitle(),//"header",//Format::replaceStringParam($HeaderVariables,$message->messageTitle),
				"body" => $pmm->getMessage(),//"body",//Format::replaceStringParam($messageVariables,$message->messageTitle),
				"type" => $type,
				"itemId" => $itemId
			);
			$payload = self::getPayload($pmm->type,$payloadParams);

			if($pmm->type==PushMobileMessage::ENUM_TYPE_ANDROID){
				$response = $pmm->pushAndroidMessage($pmm->publisherMobileDevice_pushMobileMessage->pushKey,$payload);
			}elseif($pmm->type==PushMobileMessage::ENUM_TYPE_IOS){
				$response = $pmm->pushAppleMessage($pmm->publisherMobileDevice_pushMobileMessage->pushKey,$payload);
			}else{
				die("Unknown message type");
			}

			$pmm->tryCount++;
			$pmm->lastUpdate= time();
			if($response){
				$pmm->sentTime = time();
				$pmm->state = PushMobileMessage::ENUM_STATE_SUCCESSFUL;
			}else{
				$pmm->state = PushMobileMessage::ENUM_STATE_ERROR;
			}
			$pmm->save();
			Tool::echoLog("Message sent for for $id");
		}else{
			JLog::log("push","PushMobileMessage not found $id",1);
			Tool::echoLog("No message found for $id");
		}

	}


	public function getMessage()
	{
		if (!empty($this->customMessage)){
			return $this->customMessage;
		} else {
			$message = "";

			if(!is_object($this->langMessageHistory_pushMobileMessage))
				$this->loadLangMessageHistory_pushMobileMessage();

			if (is_object($this->langMessageHistory_pushMobileMessage) && $this->langMessageHistory_pushMobileMessage->gotValue){
				if(!empty($this->messageVariables))
				{
					$messageVariables = json_decode($this->messageVariables,true);
					$message = Format::replaceStringParam($messageVariables,$this->langMessageHistory_pushMobileMessage->messageBody);
				}else{
					$message = $this->langMessageHistory_pushMobileMessage->messageBody;
				}

				return $message;
			} else {
				return "";
			}

		}

	}

	public function getTitle()
	{
		if (!empty($this->customMessageTitle)){
			return $this->customMessageTitle;
		} else {
			$_title = "";

			if(!is_object($this->langMessageHistory_pushMobileMessage))
				$this->loadLangMessageHistory_pushMobileMessage();

			if(isset($this->titleVariables))
			{
				$messageVariables = json_decode($this->titleVariables,true);
				$_title = Format::replaceStringParam($messageVariables,$this->langMessageHistory_pushMobileMessage->messageTitle);
			}elseif (!is_null($this->langMessageHistory_pushMobileMessage)){
				$_title = $this->langMessageHistory_pushMobileMessage->messageTitle;
			} else {
				$_title = $this->customMessageTitle;
			}
			return $_title;
		}
	}

	/**
	 * send to rabbitmq queue
	 *
	 * @return void
	 * @author Murat Hosver
	 **/
	public static function sendToRabbitMQ()
	{
		global $debugLevel;

		$filter = array(
			"sentTime" => "IS NULL",
			"state" => array(PushMobileMessage::ENUM_STATE_ERROR,PushMobileMessage::ENUM_STATE_WAITING),
			"tryCount" => "&lt 10"
		);

		$cronStart = time();
		$iterationLabel = date("Y-m-d H:i:s",$cronStart);

		$m = new PushMobileMessage();
		$m->update($filter,array("cronStart"=>$cronStart));
		$cnt = JPDO::count();

		if($debugLevel > 0) Tool::echoLog("{$iterationLabel} => Waiting push message count: " . $cnt);
		$filter = array("cronStart"=>$cronStart);

		$m = new PushMobileMessage();
		$m->orderBy("sendTime")->populateOnce(false)->load($filter);

		if ($m->gotValue){
			$rmq = new RabbitMQ();
			$rmq->declareQueue(RMQ_QUEUE_PUSH);

			$mcount = 0;
			$ids = array();
			do {
				$msg_body = json_encode(array("id" => $m->id,"type" => "push"));
				$rmq->publishMessage($msg_body,RMQ_QUEUE_PUSH);


				$m->state = PushMobileMessage::ENUM_STATE_ADDED_TO_QUEUE;
				$m->save();

				if($debugLevel > 0) Tool::echoLog("push queued: " . $m->id);
				$mcount++;
			}while($m->populate());

			if($debugLevel > 0) Tool::echoLog("$mcount Push(s) queued");
			$rmq->close();
		} else {
			if($debugLevel > 0) Tool::echoLog("No push Messages found");
		}

		unset($m);
	}


	/**
	 * send push message to Google Servers
	 *
	 * @return bool
	 * @author Sam
	 */
	public static function pushAndroidMessage($pushKey,$payload){
		global $debugLevel;
		/*
		 * $pushKey = "APA91bHvQks1fLPd1NPlS_MC9mBrzCkJthgN2ns9_T6MUBmLM8XmVsdlg6Zuh8ks8ci_kqZsgJvf_yCq02sTjJQIAZWlAzw0PB6FEuLR7IxxiLPOiFPJ0F-4ooPSZ4JTkUKQltp413UPTNELg_uIU9H4yW2FaTaZHw";


		$payload = json_encode($payload);

		$payload = json_encode(array(
			"header" => "header",
			"body" => "body",
			"type" => "C",
			"itemId" => 1
		));

		print_r($payload);
		*/

		//$payload = json_encode($payload);
		$androidKey = Setting::getValue("PUSH_KEY_ANDROID");
		//$androidKey ="AIzaSyCfwSZ0Car8ONZRITVMHHKJ8_i6zsQGKYk";
		$android = new PushAndroid($androidKey);
		$res= $android->notify(array($pushKey),$payload);
		//print_r($res);
		$res = json_decode($res->output);
		if($res->success == 1){
			return true;
		}else{
			if ($debugLevel) Tool::echoLog("Error on android notification ".print_r($res,true));
			JLog::log("push","Error on android notification ".print_r($res,true),2);
			return false;
		}
	}


	/**
	 * send push message to Apple Servers
	 *
	 * @return bool
	 * @author Sam
	 */
	private function pushAppleMessage($pushKey,$payload){
		global $__DP, $debugLevel;

		try {
			$request = new RestReq("http://185.14.186.139/push/push.php", 'POST', array(
				"payload" => $payload,
				"token" => $pushKey
			),"json");
			$request->execute();
			$response = json_decode($request->responseBody);

			if (is_object($response) && isset($response->state) && $response->state === true){
				if ($debugLevel) Tool::echoLog('Message successfully delivered to APNS server');
				if ($debugLevel) Tool::echoLog("Log:" . $response->log);
				return true;
			} else {
				if ($debugLevel) Tool::echoLog('Message not delivered. Log:' . $request->responseBody);
				if ($debugLevel && isset($response->log)) Tool::echoLog("Log:" . $response->log);
				JLog::log("push",'Message not delivered. Log:' . $request->responseBody);
				return false;
			}

			$ctx = stream_context_create();
			stream_context_set_option($ctx, 'ssl', 'local_cert', $__DP.'site/def/'.Setting::getValue("PUSH_KEY_FILENAME"));
			stream_context_set_option($ctx, 'ssl', 'passphrase',Setting::getValue("PUSH_KEY_PASSPHRASE"));

			// Create the IOS payload
			/*
			 *
			 * $payload = json_encode(array(
				"aps" => array("alert"=>$AndroidPushRequest->data->header,
							   "header"=>"New Campaign",
							   "sound"=>"default"),
				"type" => $AndroidPushRequest->data->type,
				"itemId" => $AndroidPushRequest->data->itemId
			));
			 *
			 *  {
				  "aps": {
				    "alert": "Yeni duyuru: Kampanya kat\u0131l\u0131m\u0131 ba\u015far\u0131 ile tamamland\u0131!",
				    "sound": "default"
				  },
				  "type": "dd",
				  "did": "151815"
				}


			$iosPayload = array(
				"aps"=>array(
					"sound"=> "default",
					"alert" => $payload->header. " - ".$payload->body
				),
				"type"=>$payload->type,
				"itemId"=>$payload->itemId
			);
			*/

			// Open a connection to the APNS server
			$err = null;
			$errstr = null;
			if ($debugLevel) Tool::echoLog("connecting to APNS server");
			$fp = stream_socket_client(Setting::getValue("PUSH_GATEWAY_ADDRESS"), $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

			if (!$fp){
				$errs = "Failed to connect APNS " . Setting::getValue("PUSH_GATEWAY_ADDRESS") . " error: $err $errstr";
				if ($debugLevel) Tool::echoLog($errs);
				JLog::log("push",$errs);
				//Mark push queue row as error
				//PushQueue::changeState($m->id,"e");
				return false;


			} else {
				JLog::log("push","Connected to APNS");
				//$iosPayload = json_encode($payload);

				/*
				$payload = json_encode(array(
					"aps" => array("alert"=>"testm","header"=>"New Campaign","sound"=>"default"),
					"type" => "c",
					"itemId" => null
				));
				*/

				$message = chr(0) . pack('n', 32) . pack('H*', $pushKey) . pack('n', strlen($payload)) . $payload;

				// Send it to the server
				$result = fwrite($fp, $message, strlen($message));

				// Close the connection to the server
				fclose($fp);

				if (!$result){
					if ($debugLevel) Tool::echoLog('Message not delivered to APNS server');
					JLog::log("push",'Message not delivered to APNS server');
					//Mark push queue row as error
					return false;
				} else {
					if ($debugLevel) Tool::echoLog('Message successfully delivered to APNS server');
					JLog::log("push",'Message successfully delivered to APNS server');

					//Mark push queue row as success
					return true;
				}

			}
			//$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			//$this->clog("processed push message : " . $m->id);
		} catch (Exception $ex) {
			if ($debugLevel) Tool::echoLog("exception occured in push message : " . print_r($ex,true));
			JLog::log("push","exception occured in push message : " . print_r($ex,true));
			return false;
		}
	}

}

