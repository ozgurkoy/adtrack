<?php
	/**
	* userRevenue real class for Jeelet - firstly generated on 14-02-2012 16:19, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/userRevenue.php");

	class UserRevenue extends UserRevenue_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}
	}

	?>