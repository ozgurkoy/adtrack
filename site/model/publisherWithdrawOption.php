<?php
/**
 * publisherWithdrawOption real class - firstly generated on 03-10-2013 17:58, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherWithdrawOption.php';

class PublisherWithdrawOption extends PublisherWithdrawOption_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * @param $name
	 * @return bool|PublisherWithdrawOption
	 * @author Özgür Köy
	 */
	public static function loadByName( $name ){
		$wo = new PublisherWithdrawOption();
		$wo->load( array( "name" => $name ) );
		$ret =  $wo->gotValue ? $wo : false;

		return $ret;
	}

}

