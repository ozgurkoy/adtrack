<?php
	/**
	* twitterQ real class for Jeelet - firstly generated on 23-02-2012 10:48, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/twitterQ.php");

	class TwitterQ extends TwitterQ_base
	{
		/**
		 * check period for twitter messages
		 *
		 * @var int
		 **/
		private $checkPeriod=259000; // 3 days

		/**
		 * max try count for twitter check
		 *
		 * @var string
		 **/
		private $maxTryCount = 10;

		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * q twitter message
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function q(&$user,$message,$date,&$rev,&$camp)
		{
			$this->message = $message;
			$this->goDate = $date;
			$this->tryCount = 0;
			$this->currentStatus = 'p';
			$this->user = $user->id;
			$this->campaign = $camp->id;
			$this->revenue = $rev->id;
			$this->save();
		}


		/**
		 * check messages
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function checkMessages()
		{
			global $__DP;
			require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');
			$this->customClause = "goneDate <= ".(time()+$this->checkPeriod)." AND tryCount <= ".$this->maxTryCount." AND (checkDate IS NULL OR checkDate=0) AND currentStatus='s'"; //
			$this->nopop()->load(null,0,20);

			while($this->populate()){
				$u = $this->loadUser();
				$cred = $u->loadTwitter();

				$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $cred->twKey, $cred->twKeySecret);
				$postResult = $connection->get('statuses/show', array('id' => $this->messageId));

				if(isset($postResult->error)){
					if(strpos($postResult->error,"No status found")!==false){
						//deleted
						$this->currentStatus = 'd';
						$this->tryCount = 0;
						$this->save();

						$u->saveHistory("DeletedT:".$this->message);
						$camp->tweetDeleted($u);
					}
					elseif(strpos($postResult->error,"Not found")!==false){
    					//user deleted/deactivated
						$this->currentStatus = 'd';
						$this->tryCount = 0;
						$this->save();

						$u->saveHistory("UserDeleted!");
						$camp = $this->loadCampaign();
						$camp->tweetDeleted($u);
					}
				}
				else if(isset($postResult->errors)){
					//api error
					JLog::log("twi","Twitter check error".serialize($postResult)."|||".serialize($_SESSION));
					$this->currentStatus = 'e';
					$this->tryCount = $this->tryCount+1;
					$this->save();

					$e = new ApiError();
					$e->error = serialize($postResult->errors);
					$e->type = 'tc';
					$e->save();
					$e->saveTwitterQ($this);

					unset($e);
				}
				else{
					$this->checkDate = time();
					$this->messageId = $postResult->id;
					$u->saveHistory("CheckedT:".$this->message);
					$this->save();

					//approve rev.
					$rev = $this->loadRevenue();
					$rev->state = 'c';
					$rev->save();

					$this->loadUser();
					$camp = $this->loadCampaign();

					//TODO rtcount returns false from API.
					$rtCount = 0;

					$camp->tweetChecked($u,$rtCount);

					//remove queue response after logging
					unset($camp,$rev,$u);

					$this->delete();
				}
			}

		}

		/**
		 * check and finish if needed
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function checkActiveCampaigns()
		{
			$c = new Campaign();
			$c->customClause = "state = 'p' AND endDate<'".date("Y-m-d",time())."'";
			$c->nopop()->load();

			while ($c->populate()) {
				$c->endCampaign();
			}

		}

		/**
		 * post , TODO : Check for more errors and index them
		 *
		 * @return bool
		 * @author Özgür Köy
		 **/
		public function launchMessages()
		{
	   		global $__DP;

			require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');
			$this->customClause = "goDate <= ".time()." AND tryCount <= ".$this->maxTryCount." AND currentStatus IN('p','e')"; //IN('p','e')
			$this->nopop()->load(null,0,20);

			while($this->populate()){
				$u = $this->loadUser();
				$cred = $u->loadTwitter();

				$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $cred->twKey, $cred->twKeySecret);
				$postResult = $connection->post('statuses/update', array('status' => JUtil::randomString(15)."".$this->message));

				if(isset($postResult->error)){
					if(strpos($postResult->error,"Could not authenticate")!==false){
						$this->currentStatus = 'e';
						$this->tryCount = $this->tryCount+1;
						$this->save();
					}
					else{
					// 	Status is a duplicate.
				   	}
				}
				else if(isset($postResult->errors)){
					//api error
					JLog::log("twi","Twitter check error".serialize($postResult)."|||".serialize($_SESSION));
					$this->currentStatus = 'e';
					$this->tryCount = $this->tryCount+1;
					$this->save();

					$e = new ApiError();
					$e->error = serialize($postResult->errors);
					$e->type = 'tp';
					$e->save();
					$e->saveTwitterQ($this);

					unset($e);
				}
				else{
					$this->currentStatus = 's';
					$this->tryCount = 0; //reset try count
					$this->goneDate = time();
					$this->messageId = $postResult->id;
					$u->saveHistory("SentT:".$this->message);
					$this->save();

					$camp = $this->loadCampaign();
					$camp->tweetSent($u);

					unset($camp);

				}

			}


		}

}

?>