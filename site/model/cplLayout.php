<?php
/**
 * cplLayout real class - firstly generated on 28-03-2014 18:33, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplLayout.php';

class CplLayout extends CplLayout_base
{
	public static $regulars = array();
	public static $formInfo = array();
	public static $repeated = array();
	public static $tempCode = array();

	public static $layoutPath   = "files/modules/CPL/layouts";
	public static $actionPath   = "/CPLFormSubmission/";

	const ERROR_IN_FILE          = -1;
	const NO_FORMS_FOUND         = -2;
	const NO_FORM_PART_FOUND     = -3;
	const NO_FORM_FIELD_FOUND    = -4;
	const NO_FORM_LABEL_FOUND    = -5;
	const REPEAT_TAGS_DONT_MATCH = -6;
	const DUPLICATE_REPEAT_TAG   = -7;
	const DUPLICATE_FIELD_TAG    = -8;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * process template
	 *
	 * @param $f
	 * @return mixed|String
	 * @author Özgür Köy
	 * @throws JError
	 */
	public static function processTemplate( $f ) {
		global $__DP;
		include_once $__DP . "site/lib/phpQuery/phpQuery-onefile.php";
		self::$repeated = array();

		$replacements = array();
		//stop error throws
		libxml_use_internal_errors(true);

		//repeat groups
		$rpp = phpQuery::newDocumentHTML( $f );
		$f = $rpp->htmlOuter();
		preg_match_all( "|<!--repeat@(.+?)-->(.+?)<!--end-repeat@(.+?)-->|smi", $f, $repeats );
		if ( sizeof( $repeats ) == 4 ) {
			$phrases = array_shift( $repeats );
			foreach ( $repeats[ 0 ] as $rk => $repeatTag ) {
				//check for max repeat count
				if(strpos($repeatTag,":")!==false ){
					$x0 = explode( ":", $repeatTag );
					if ( !isset( $x0[ 1 ] ) )
						return array( "error" => "Repeat tag's repeat count is not placed ($repeatTag)" );

					$maxCount = intval( $x0[ 1 ] );
					$repeatTag = $x0[ 0 ];
				}
				else
					$maxCount = 0 ;

				$vrepeatTag = CplLayout::norm( $repeatTag);
				if ( CplLayout::norm( $repeats[ 2 ][ $rk ]) != $vrepeatTag )
					return array("error"=>"Non match, beginning({$repeatTag}) and ending({$repeats[ 2 ][$rk]}) tags don't match" );

				if ( isset( self::$repeated[ $vrepeatTag ] ) )
					return array("error"=>"Repeat tag already created : " . $repeatTag );

				self::$repeated[ $vrepeatTag ] = array(
					"tag"      => $repeatTag,
					"label"    => $repeatTag,
					"maxCount" => $maxCount,
					"html"     => $phrases[ $rk ],
					"fields"   => array()
				);

				$code       = JUtil::randomString();
				$f          = str_replace( $phrases[ $rk ], "<!--" . $code . "-->", $f );
				$repeatPart = phpQuery::newDocumentHTML( $repeats[ 1 ][ $rk ] );
				$container  = $repeatPart->find( '*[data-jtemplate]' );

				$t0 = $phrases[ $rk ];

				foreach ( $container as $cc ) {
					$d0          = pq( $cc );
					$el0         = $d0->elements[ 0 ];
					$attr        = explode( ';', $d0->attr( "data-jtemplate" ) );
					$varOnly     = $d0->attr( "data-jtemplate-varonly" );
					$effect      = $attr[ 0 ];
					$name        = $attr[ 1 ];
					$type        = $attr[ 2 ];
					$description = isset( $attr[ 3 ] ) ? $attr[ 3 ] : '';
					$vname       = CplLayout::norm( $name );

					preg_match( "|<.+?>|", $d0->htmlOuter(), $outer );

					$inner = $outer[ 0 ];
					if ( isset( self::$repeated[ $vrepeatTag ][ "fields" ][ $vname ] ) && $varOnly != 1 )
						return array("error"=> $name . " is already within the '" . $repeatTag . "' loop. Can't add it again" );

					self::processRegular( $cc, $t0, "", "jrep." . CplLayout::norm( $name), $vname );


					if ( $varOnly != 1 ) {
						self::$repeated[ $vrepeatTag ][ "fields" ][ $vname ] = array(
							"label"  => $name,
							"effect" => $effect,
							"type"   => $type,
							"description"   => $description,
							"tag"    => $el0->nodeName
						);
					}
				}

				$replacements[ $code ] = $t0;
			}
		}

		//we got rid of the repeats, now regular fields
		$rpp = phpQuery::newDocumentHTML( $f );
		$container  = $rpp->find( '*[data-jtemplate]' );

		foreach ( $container as $cc ) {
			$d0     = pq( $cc );


			// if this is only a variable from another place
			if ( $d0->attr( "data-jtemplate-varonly" ) && $d0->attr( "data-jtemplate-varonly" ) == 1 )
				continue;

			$parts = explode("|",$d0->attr( "data-jtemplate" ));
			foreach ( $parts as $part ) {
				$attr        = explode( ';', $part );
				$effect      = $attr[ 0 ];
				$name        = $attr[ 1 ];
				$type        = $attr[ 2 ];
				$description = isset( $attr[ 3 ] ) ? $attr[ 3 ] : '';
				$vname       = CplLayout::norm( $name );

				$r0     = self::processRegular( $cc, $f, "", "data." . CplLayout::norm( $name), $vname );

				self::$regulars[ $vname ] = $r0[ "data." . CplLayout::norm( $name) ];
			}
			//replace with
			$o0 = $d0->htmlOuter();
			$d0->addClass( "joverlay" );
			$f = str_replace( $o0, $d0->htmlOuter(), $f );

		}

		//put back the replacements
		foreach ( $replacements as $code => $repp )
			$f = str_replace( "<!--" . $code . "-->", $repp, $f );

		//form processing
		/*
		data-jtemplateFormFieldClass
		data-jtemplateFormField
		data-jtemplateFormLabel
		data-jtemplateFormPart
		data-jtemplateForm
		*/
		$rpp = phpQuery::newDocumentHTML( $f );
		$container  = $rpp->find( '*[data-jtemplateform]' );

		if ( sizeof( $container ) == 0 )
			throw new JError( self::NO_FORMS_FOUND );
		else {
			$d0 = pq( $container[ 0 ] );
			preg_match( "|<.+?>|", $d0->htmlOuter(), $outer );
			self::$formInfo[ "form" ] = $outer[ 0 ];
		}

		$container  = $rpp->find( '*[data-jtemplateformpart]' );
		if ( sizeof( $container ) == 0 )
			throw new JError( self::NO_FORM_PART_FOUND );
		else {
			$d0 = pq( $container[ 0 ] );
			preg_match( "|<.+?>|", $d0->htmlOuter(), $outer );
			self::$formInfo[ "part" ] = $outer[ 0 ];
		}
		#if(sizeof($container)==0)
			#throw new JError(self::NO_FORM_LABEL_FOUND);
		$container  = $rpp->find( '*[data-jtemplateformlabel]' );
		if ( sizeof( $container ) == 1 ) {
			$d0 = pq( $container[ 0 ] );
			preg_match( "|<.+?>|", $d0->htmlOuter(), $outer );
			self::$formInfo[ "label" ] = $outer[ 0 ];
		}

		$container  = $rpp->find( '*[data-jtemplateformfield]' );
		if ( sizeof( $container ) == 0 )
			throw new JError( self::NO_FORM_FIELD_FOUND );
		else {
			$d0 = pq( $container[ 0 ] );
			preg_match( "|<.+?>|", $d0->htmlOuter(), $outer );
			self::$formInfo[ "field" ] = $outer[ 0 ];
		}

		$container  = $rpp->find( '*[data-jtemplateformfieldclass]' );
		if ( sizeof( $container ) > 0 ) {
			$d0 = pq( $container[ 0 ] );
			preg_match( "|<.+?>|", $d0->htmlOuter(), $outer );
			self::$formInfo[ "fieldClass" ] = $outer[ 0 ];
		}

		return $f;
	}

	/**
	 * process regular field
	 *
	 * @param DOMElement $cc
	 * @param            $f
	 * @param string     $prefix
	 * @param string     $override
	 * @return array
	 * @author Özgür Köy
	 */
	public static function processRegular( DOMElement $cc, &$f, $prefix = "", $override = "", $nameSpecific = null ) {
//		echo "
//
//		PROCESS
//
//		";
		$d0      = pq( $cc );
		$el0     = $d0->elements[ 0 ];
		$parts   = explode( "|", $d0->attr( "data-jtemplate" ) );
		$newAtts = array();
		$arrs    = array();
		$outerHtml = $d0->htmlOuter();
		$attsFixed = false;

		foreach ( $parts as $part ) {

			$attr        = explode( ';', $part );
			$effect      = $attr[ 0 ];
			$name        = $attr[ 1 ];
			$type        = $attr[ 2 ];
			$description = isset( $attr[ 3 ] ) ? $attr[ 3 ] : '';

			$vname = CplLayout::norm( $name );


			$codeRep = $override == "" ? $vname : $override;
			$unique  = JUtil::randomString( 10 );

			if ( $attsFixed === false ) {
				$parts1 = explode( "|", $d0->attr( "data-jtemplate" ) );
				$newAt0 = array();

				foreach ( $parts1 as $part0 ) {
					$attr0  = explode( ';', $part0 );
					$vname0 = CplLayout::norm( $attr0[ 1 ] );

					if ( !is_null( $nameSpecific ) && $nameSpecific == $vname0 ) {
						$codeRep   = $override == "" ? $vname : $override;
						$newAt0[ ] = $attr0[ 0 ] . ';' . $codeRep . ';' . $attr0[ 2 ];
					}
					else
						$newAt0[ ] = $attr0[ 0 ] . ';' . $vname0 . ';' . $attr0[ 2 ];

				}

				$d0->attr( "data-jtemplate", implode( "|", $newAt0 ) );

				$attsFixed = true;

			}

			if ( !is_null( $nameSpecific ) && $nameSpecific != $vname )
				continue;

			$outerHtml2 = $d0->htmlOuter();

			$code    = "<!--COND:" . ( $prefix == "" ? "" : "$prefix." ) . $codeRep . "-->";
			$endCode = "<!--END-COND-->";

			if ( isset( self::$regulars[ $codeRep ] ) )
				return array( "error" => "Field '" . $codeRep . "' is already defined, you can't do that." );

			preg_match( "|<.+?>|", $outerHtml, $outer );
			$inner = $outer[ 0 ];

			$arrs[ $codeRep ] = array(
				"type"        => $type,
				"effect"      => $effect,
				"code"        => $code,
				"label"       => $name,
				"description" => $description,
				"tag"         => $el0->nodeName,
			);

			$f = str_replace( $outerHtml, $code . $outerHtml2 . $endCode, $f );

		}

		return $arrs;
	}

	/**
	 * save to folder
	 *
	 * @param      $code
	 * @param      $name
	 * @param null $id
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function saveTemplate( $code, $name, $id=null ) {
		global $__DP;
		$folder = $__DP . self::$layoutPath . "/" . $code;
		self::delTree( $folder );

		//saved to folder
		@mkdir( $folder, 0775, true );
		rename( sys_get_temp_dir() . "/cpl/" . $code, $folder );

		$f = file_get_contents( $folder . "/index.html" );

		//read template and get variables
		self::processTemplate( $f );

		$c          = new CplLayout( strlen( $id ) > 0 ? $id : null );
		$c->name    = $name;
		$c->code    = $code;
		$c->options = json_encode(
			array(
				"regulars" => self::$regulars,
				"repeats"  => self::$repeated,
				"form"     => self::$formInfo
			), JSON_HEX_QUOT
		);

		$c->save();

		return true;
	}

	/**
	 * @param      $path
	 * @param      $name
	 * @param null $code
	 * @return array|int|string
	 * @author Özgür Köy
	 */
	public static function receiveTemplate( $path, $name, $code= null ){
		if ( is_null( $code ) )
			$code = JUtil::randomString( 10 );

		$zip        = new ZipArchive;
		$res        = $zip->open( $path );
		$folderName = sys_get_temp_dir() . "/cpl/" . $code;

		self::delTree( $folderName );

		mkdir($folderName, 0777, true);

		if ($res === TRUE) {
			self::$tempCode = $code;
			$zip->extractTo($folderName);
			$zip->close();
			if( !file_exists($folderName . "/index.html"))
				return self::ERROR_IN_FILE;

			$f = file_get_contents( $folderName . "/index.html" );
			$error = null;
			try{
				$r = self::processTemplate( $f );
				if(is_array($r) && isset($r["error"]))
					$error = $r[ "error" ];

			}
			catch(JError $j){
				return $j->getMessage();
			}

			return array(
				"error"    => $error,
				"code"     => $code,
				"name"     => $name,
				"repeats"  => self::$repeated,
				"regulars" => self::$regulars,
				"form"     => self::$formInfo
			);

		}
		else
			return "";
	}

	/**
	 * convert html to template
	 *
	 * @param $code
	 * @return array
	 * @author Özgür Köy
	 */
	public static function convertToTemplate( $code ) {
		global $__DP;
		$folder = $__DP . self::$layoutPath . "/" . $code;

		$layout = new CplLayout();
		$layout->load( array( "code"=>$code ) );

		$f = file_get_contents( $folder . "/index.html" );
		$f = self::processTemplate( $f );

		//replace script and style literals
		$f = preg_replace( "|<script(.*?)>(.*?)</script>|ms", "<script $1>{literal}$2{/literal}</script>", $f );
		$f = preg_replace( "|<style(.*?)>(.*?)</style>|ms", "<style $1>{literal}$2{/literal}</style>", $f );

		$tfile = phpQuery::newDocumentHTML( $f );
		$container  = $tfile->find( '*[data-jtemplate]' );
		$topCss = array();
		foreach ( $container as $cc ) {
			$d0          = pq( $cc );
			$el0         = $d0->elements[ 0 ];
			$parts       = explode( '|', $d0->attr( "data-jtemplate" ) );
			foreach ( $parts as $part ) {
				$attr        = explode( ';', $part );
				$effect      = $attr[ 0 ];
				$name        = $attr[ 1 ];
				$type        = $attr[ 2 ];
				$description = isset( $attr[ 3 ] ) ? $attr[ 3 ] : '';

				$e0 = explode( ":", $effect );
				if ( sizeof( $e0 ) > 1 ) {
					$attribute = $e0[ 0 ];
					$attValue  = $e0[ 1 ];
				}
				else {
					$attribute = $effect;
					$attValue  = "%";
				}

				//body's attrs can't be set
				if($el0->nodeName=="body" && $attribute=="style" ){
					if(!isset($topCss[$el0->nodeName])){
						$s0 = explode( "=", $attValue );
						if(sizeof($s0)!=2)
							return array("error"=>"Wrong args for body" );
						$topCss[ $el0->nodeName ][ trim( $s0[ 0 ] ) ] = str_replace( "%", "{\$data.".CplLayout::norm( $name)."}",trim( $s0[ 1 ] ));
					}
				}
				else{
					$cont = "{assign var=tw0 value=\"cpl_`$".$name."`\"}{if isset(\$words.\$tw0)}{\$words.\$tw0}{else}{\$".$name."|nl2br}{/if}";
//				$d0->html( str_replace( "%", "{\$".$name."}", $attValue ) );
					if($attribute == "html")
						$d0->html( str_replace( "%", $cont, $attValue ) );
					elseif(in_array($attribute,array("value"))){
						$d0->attr( $attribute, str_replace( "%", $cont, str_replace( "=", ":", $attValue ) ) ); // =: for css
					}
					else{
						$d0->attr( $attribute, str_replace( "%", "{\${$name}}", str_replace("=",":",$attValue) ) ); // =: for css
					}
				}
			}

		}
//		exit;
		$f = $tfile->htmlOuter();

		//forms
		/*
		data-jtemplateformfieldclass
		data-jtemplateformfield
		data-jtemplateformlabel
		data-jtemplateformpart
		data-jtemplateform
		*/
		$tfile     = phpQuery::newDocumentHTML( $f );
		$container = $tfile->find( '*[data-jtemplateform]' );
		$d0        = pq( $container[ 0 ] );
		$d0->attr( "id", "JLTF_SU" );
		$d0->attr( "method", "POST" );

		$d0->attr( "action", self::$actionPath . '{$code}' );
		$d0->attr( "target", "alCPL" );
		$d0->append( '<input type="hidden" name="token" value="{$token}">' );
		$d0->append( '<input type="hidden" name="code" value="{$code}">' );

		$f         = $tfile->htmlOuter();
		$tfile     = phpQuery::newDocumentHTML( $f );

		$container = $tfile->find( '*[data-jtemplateformpart]' );
		$d0        = pq( $container[ 0 ] );
//		$d0->attr( "pid", 'j_formFPP_{$formKey}' );
		$touter    = $d0->htmlOuter();
//		echo $touter;exit;

		$tfout      = phpQuery::newDocumentHTML( $touter );
		$element    = $tfout->find( '*[data-jtemplateformlabel]' );
		$d1         = pq( $element[ 0 ] );
		$d1->html('
		{if strlen(trim($formItem.info)) gt 0}
		<a href="javascript:;" data-toggle="modal" data-target="#jInfoModal_{$formItem}">
		{/if}
		{if isset($words[$formItem.label])}
			{$words[$formItem.label]}
		{else}
			{$formItem.label}
		{/if}

		{if strlen(trim($formItem.info)) gt 0}
		</a>
		{/if}
		');
		$labelHtml = $d1->htmlOuter();

		$element    = $tfout->find( '*[data-jtemplateformfield]' );
		$d1         = pq( $element[ 0 ] );

		$d1->attr( "id", 'j_formLp_{$formKey}' );

		if ( $d1->attr( "data-jtemplateformfieldclass" ) && strlen( $d1->attr( "data-jtemplateformfieldclass" ) ) > 0 )
			$d1->html( '{addTag i=$formItem.preview type="class" c="' . $d1->attr( "data-jtemplateformfieldclass" ) . '"}' );
		else
			$d1->html( '{$formItem.field}' );


		$container = $tfout->find( '*[data-jtemplateformpart]' );
		$d0        = pq( $container[ 0 ] );
		$d0->attr( "id", 'j_formFPP_{$formKey}' );

		$toutx     = $tfout->htmlOuter();

		$f = str_replace( $touter, '
			{if isset($jForm) && sizeof($jForm)>0}
			{foreach from=$jForm key=formKey item=formItem}'.
				$toutx.
			'{/foreach}
			{/if}',
			$f
		);

		$f = str_replace( $labelHtml, '
			{if $formItem.noLabel != 1}'.
				$labelHtml.
			'{/if}',
			$f
		);

		//replacements
		$f = preg_replace( '|<link rel="(!?shortcut )?icon".+?/?>|i', '', $f );
		$f = preg_replace( '|<title>.+?</title>|i', '', $f );
		//remove previous metas and other stuff
		$f = preg_replace( "|<link rel.+?shortcut.+?>|i", "\n", $f );
		$f = preg_replace("|<meta.+?name=\"description\".+?>|i","\n", $f);
		$f = preg_replace("|<meta.+?name=\"author\".+?>|i","\n", $f);

		$f = preg_replace( "|<!--end-repeat.+?-->|", "\n{/if}{/foreach}\n{/if}\n", $f );
		$f = preg_replace( "|<!--END-COND-->|", "\n{/if}\n", $f );
		$f = preg_replace( "|<!--JCBLCK|", "{ ", $f );
		$f = preg_replace( "|EJCBLCK-->|", " }", $f );


		//pre condiditions
		$f = preg_replace_callback( "|<!--COND:(.+?)-->|",
			function ( $cond ) {
				if ( strpos( $cond[ 1 ], "data." ) === false && strpos( $cond[ 1 ], "jrep." ) === false )
					$cond[ 1 ] = "data." . $cond[ 1 ];

				return "\n{if isset($" . CplLayout::norm( $cond[ 1 ] ) . ") && strlen($" . CplLayout::norm( $cond[ 1 ] ) . ")>0}\n";
			},
			$f
		);

		//variables
		$f = preg_replace_callback( "|<!--repeat@(.+?)-->|", function($matches){
				if(strpos($matches[1],":")!==false ){
					$x0 = explode( ":", $matches[1] );

					$maxCount = intval( $x0[ 1 ] );
					$matches[1] = $x0[ 0 ];
				}
				else
					$maxCount = 1000;

				return "\n{if isset(\$data.".CplLayout::norm($matches[1]).")}\n{foreach from=\$data.".CplLayout::norm($matches[1])." key=jcounter item=jrep}{if \$jcounter<=".($maxCount-1)."}\n";
			},
			$f
		);

		if(sizeof($topCss)>0){
			$topCss = self::prepareTopCss( $topCss );
			$f = str_replace( '</head>', $topCss . '</head>', $f );
		}



		//base tag and others
		$f = str_replace( '<head>', '<head>
		<!--
		REMOVE THIS
		<script>
		document.write(\'<base href="http://\'+(location.href.replace(\'http://\',\'\').split(\'/\'))[0]+\'/'.self::$layoutPath.'/'.$code.'/">\');
		</script>
		-->
		<base href="/'.self::$layoutPath.'/'.$code.'/">
		<script src="/langJS" type="text/javascript"></script>
		{if isset($favicon) &&  strlen($favicon)>0}
		<link rel="shortcut icon" sizes="16x16 32x32 48x48 64x64" href="{$favicon}">
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon}">
		<!--[if IE]><link rel="shortcut icon" href="/favicon.ico"><![endif]-->
		{/if}
		<title>{$title}</title>
		<meta property="og:title" content="{$title}" />
		<meta property="og:description" content="{$description}" />
		<meta property="og:image" content="{$domain}/site/layout/images/campaign/{$image}" />
		', $f );


		//script additions
		$f = str_replace( '</body>',
		($layout->includeBootstrap==1?'
		<!--bootstrap-->
		<link href="/site/layout/CPL/style/bootstrap-theme.min.css" rel="stylesheet" />
		<link href="/site/layout/CPL/style/bootstrap.min.css" rel="stylesheet" />
		<script src="/site/layout/CPL/js/bootstrap.min.js"></script>
		<!--end bootstrap-->
		':'').'

		<script>
		var layoutPosition = \''.strtolower($layout->errorPosition).'\';
		var finalMessage   = \'{$endMessage}\';
		</script>
		<link href="/site/layout/CPL/style/admingle-cpl.css" rel="stylesheet" />
		<link href="/site/layout/CPL/style/avground.css" rel="stylesheet" />
		<script src="/site/layout/CPL/js/main.js"></script>
		<script src="/site/layout/CPL/js/jquery.avgrund.min.js"></script>
		<iframe name="alCPL" style="display:none"></iframe>
		{if isset($langs)}
		<script>
		var langs = [];
		var actLang = "{$actLang}";
		{foreach from=$langs key=short item=flag}
		langs.push([\'{$short}\',\'{$flag}\']);
		{/foreach}
		</script>
		{/if}
		{if isset($footerText)}
		<!--footer-->
		{$footerText}
		<!--end footer-->
		{/if}
		{if isset($jForm) && sizeof($jForm)>0}
		{foreach from=$jForm key=formKey item=formItem}
		{if strlen(trim($formItem.info)) > 0}
		<div class="modal fade" id="jInfoModal_{$formItem}" tabindex="-1" role="dialog" aria-labelledby="jInfoModal_{$formItem}" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-body">
		      {$formItem.info|nl2br}
		      </div>
		    </div>
		  </div>
		</div>

		{/if}
		{/foreach}
		{/if}
		</body>
		', $f );

		self::entityFix($f);
		self::srcFix( $f, self::$layoutPath . '/' . $code );

//		remove guide tags
		$f = preg_replace( '|data-jtemplate.+?".+?"|', '', $f );

		#echo $folder . "/index.tpl";exit;
		file_put_contents( $folder . "/index.tpl" , $f);
	}

	/**
	 * del folder, rec
	 *
	 * @param $dir
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function delTree( $dir ) {
		if(!is_dir($dir)) return false;

		$files = array_diff( scandir( $dir ), array( '.', '..' ) );
		foreach ( $files as $file ) {
			( is_dir( "$dir/$file" ) ) ? self::delTree( "$dir/$file" ) : unlink( "$dir/$file" );
		}

		return rmdir( $dir );
	}

	/**
	 * fix form entities
	 *
	 * @param $f
	 * @author Özgür Köy
	 */
	public static  function entityFix( &$f ){
		$f = str_replace( "%7D", "}", $f );
		$f = str_replace( "%7B%24", "{\$", $f );
		$f = preg_replace( "|\r|", "", $f );
		$f = preg_replace("|\{(.+?)\%20(.+?)\}|","{\$1_\$2}",$f);
	}

	public static function srcFix( &$f, $layoutPath ) {
		$f = preg_replace_callback( '~<(\w+)\s(href|src)=[\'"](.*?)[\'"]~si', function ( $e ) use ( $layoutPath ) {
			if (
				stripos( $e[ 3 ], "http:" ) === false &&
				stripos( $e[ 3 ], "javascript:" ) === false &&
				!in_array( substr($e[3], 0, 1), array("#","/","{"))
			) {
				return "<".$e[1]." ".$e[2].'="{$domain}/' . $layoutPath . '/' . $e[ 3 ].'"';
			}
			else{

				return "<".$e[1]." ".$e[2].'='. '"'.(strtolower($e[1])=="iframe"?"http://":"").$e[ 3 ].'"';}
		}, $f );
	}

	/**
	 * top css for template
	 *
	 * @param $topCss
	 * @return string
	 * @author Özgür Köy
	 */
	public static function prepareTopCss( $topCss ) {
		$c = '<style>';
		foreach ( $topCss as $tag => $tagValues ) {
			$c .= $tag . '{literal}{{/literal}';
			foreach ( $tagValues as $t0 => $t1 ) {
				preg_match( "|\{(.+?)\}|", $t1, $varMatch );
				$c .= '{if isset(' . $varMatch[1] . ')}
				' . $t0 . ':' . $t1 . ';
				{/if}
				';
			}

			$c .= '{literal}}{/literal}
			';
		}
		$c .= '</style>';

		return $c;
	}

	public static function norm( $str ) {
		return JUtil::normalize( $str, true, "_" );
	}

}

