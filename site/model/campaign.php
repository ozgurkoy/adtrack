<?php
/**
 * campaign real class for Jeelet - firstly generated on 24-05-2011 11:41, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 * @author  Özgür Köy
 **/
include($__DP . "/site/model/base/campaign.php");

class Campaign extends Campaign_base
{
	/**
	 * genders
	 *
	 * @var string
	 **/
	public static $genders = array(
		"m"   => "male",
		"f"   => "female",
		"all" => "all"
	);

	/**
	 * dsns for campaign
	 *
	 * @var string
	 **/
	private $dsns = array("CA" => "CAMP0");

	/**
	 * active dsn for new campaigns
	 *
	 * @var string
	 **/
	private static $activeDSN = "CA";

	/**
	 * age ranges
	 *
	 * @var array
	 **/
	public static $ages = array(
		"13-17"  => 0,
		"18-24"  => 0,
		"25-34"  => 0,
		"35-49"  => 0,
		"50-59"  => 0,
		"60-69"  => 0,
		"70-100" => 0
	);

	/**
	 * lang values for type.
	 *
	 * @var array
	 **/
	public static $typesLang = array(
		"sc" => "standardcampaignwithcelebrities",
		"s"  => "standardcampaign",
		"c"  => "celebritiesonlycampaign"
	);

	/**
	 * lang values for states
	 *
	 * @var array
	 **/
	public static $statesLang = array(
		"i"  => "campaignstate-initial",
		"a"  => "campaignstate-accepted",
		"d"  => "campaignstate-declined",
		"o"  => "campaignstate-offerwaiting",
		"od" => "campaignstate-offerdeclined",
		"oa" => "campaignstate-offerapproved",
		"wp" => "campaignstate-waitingpayment",
		"pp" => "campaignstate-pendingprogress",
		"p"  => "campaignstate-progressing",
		"f"  => "campaignstate-finished",
	);

	/**
	 * hq email
	 *
	 * @var string
	 **/
	private $hqEmail = "info@admingle.com";

	/**
	 * Campaign preview image upload path
	 *
	 * @var string
	 */
	public $uploadPath = "site/layout/images/campaign/";


	/**
	 * constructor for the class
	 *
	 * @return void
	 * @author Murat
	 **/
	public function __construct($id = null)
	{
		global $SYSTEM_EMAILS;
		parent::__construct($id);
		$this->hqEmail = $SYSTEM_EMAILS["info"];
	}

	/**
	 * connect to dsn
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function conn()
	{
		$dsn = $this->dsns[self::$activeDSN];

		JPDO::connect( JCache::read('JDSN.'.$dsn) );
	}

	/**
	 * returns remaining budget
	 *
	 * @return float
	 * @author Özgür Köy
	 **/
	public function getLatestBudget() {
		$cb = new campaignBudget();
		$cb->overrideTableName($this->campCode . "_budget");
		$cb->orderBy("id DESC");
		$cb->limit(0,1);
		$cb->load();
		if ($cb->gotValue){
			return $cb->budgetLeft;
		} else {
			return 0;
		}
	}

	public function revert() {
		if($this->gotValue && $this->state < Campaign::ENUM_STATE_PENDING_PROGRESS){
			$this->state = Campaign::ENUM_STATE_INITIAL;
			$this->save();

			$this->writeHistory( CampaignHistory::ENUM_ACTIONID_CAMPAIGN_UPDATE, "Campaign state reverted", CampaignHistory::ENUM_MADEBY_ADMIN );
		}
	}

	/**
	 * Write action to campaign history
	 *
	 * @return void
	 * @author Murat
	 **/
	public function writeHistory($actionId,$actionText,$madeBy,$actionData="")
	{
		$h = new CampaignHistory();
		$h->date = time();
		$h->actionId = $actionId;
		$h->actionText = $actionText;
		$h->actionData = (is_array($actionData) ? json_encode($actionData) : $actionData);
		$h->madeBy = $madeBy;
		$h->campaign_campaignHistory = $this->id;
		$h->save();
		unset($h);
	}

	/**
	 * Campaign Detail for Publisher
	 * @param array $params Array of required parameters
	 *
	 * @return array
	 * @author Murat
	 */
	public static function pubCampDetail($params){
		$cp = new CampaignPublisher();
		$r = null;
		//$cp->with("campaign_campaignPublisher,publisher_campaignPublisher");
		$cp->with(
			array(
				"publisher_campaignPublisher",
				Publisher::DO_LEFT_JOIN
			),
			array(
				"campaign_campaignPublisher",
				Campaign::DO_LEFT_JOIN,
				array(
					array(
						"brand",
						User::DO_LEFT_JOIN
					),
					array(
						"campaign_campaignClick",
						User::DO_LEFT_JOIN
					),
				)
			)
		);
		$loadFilter = array(
				"campaign_campaignPublisher"=> array("publicID"=>$params["publicID"])
			);

		if ( $params[ "pid" ] != ValidateCampaign::TEST_PUBLISHER )
			$loadFilter[ "publisher_campaignPublisher" ] = $params[ "pid" ];

		$cp->load(
			$loadFilter
		);

		if ($cp->gotValue){
			$r = array();
			$r["cpcLink"] = null;

			//$cp->loadCampaign();
			//$cp->loadPublisher();

			if ($cp->state == CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION){
				$cp->canUserAccept();
			}

			$cp->campaign_campaignPublisher->loadCampaign_campaignMessage(null,0,50,"RANDOM");
			//$cp->campaign_campaignPublisher->loadCampaignClick();
			//$cp->campaign_campaignPublisher->loadBrand();
			switch ( $cp->campaign_campaignPublisher->race ) {
				case Campaign::ENUM_RACE_CPC:
					$cp->campaign_campaignPublisher->loadCampaign_campaignCPC();
					$r[ "actionCost" ] = $cp->campaign_campaignPublisher->campaign_campaignCPC->cpcCost;
					break;
				case Campaign::ENUM_RACE_CPL:
					$cp->campaign_campaignPublisher->loadCplCampaign();
					$r[ "actionCost" ]  = $cp->campaign_campaignPublisher->campaign_cplCampaign->perLead;
					$r[ "leadPreview" ] = Setting::getValue( "CPL_LANDING_DOMAIN" ) . "/" . $cp->campaign_campaignPublisher->campaign_cplCampaign->preHash;
					break;
				case Campaign::ENUM_RACE_CPL_REMOTE:
					$cp->campaign_campaignPublisher->loadCplRemoteCampaign();
					$r[ "actionCost" ]  = $cp->campaign_campaignPublisher->campaign_cplRemoteCampaign->perLead;
					$r[ "leadPreview" ] = $cp->campaign_campaignPublisher->campaign_cplRemoteCampaign->preparePreviewLink();
					break;
				case Campaign::ENUM_RACE_CPA_AFF:
					$cp->campaign_campaignPublisher->loadCpaAffCampaign();
					$r[ "commissionInfo" ]  = $cp->campaign_campaignPublisher->campaign_cpaAff->commissionInfo;
					$r[ "actionCost" ] = 0;
					$r[ "leadPreview" ] = $cp->campaign_campaignPublisher->campaign_cpaAff->preparePreviewLink();

					break;
				case Campaign::ENUM_RACE_TWITTER:
					$cp->campaign_campaignPublisher->loadCampaign_campaignTwitter();
					$r[ "actionCost" ] = 0;
					break;
			}

			if ($cp->state == CampaignPublisher::ENUM_STATE_APPROVED){
				switch($cp->campaign_campaignPublisher->race){
					case Campaign::ENUM_RACE_CPC:
						$cp->campaign_campaignPublisher->loadCampaign_campaignCPCMessage(array(
							"campaign_campaignCPCMessage" => $cp->campaign_campaignPublisher->id,
							"publisher_campaignCPCMessage" => $cp->publisher_campaignPublisher->id
						));
						if (is_null($cp->campaign_campaignPublisher->campaign_campaignCPCMessage->calculatedClick)) $cp->campaign_campaignPublisher->campaign_campaignCPCMessage->calculatedClick = 0;
						if (is_null($cp->campaign_campaignPublisher->campaign_campaignCPCMessage->totalClick)) $cp->campaign_campaignPublisher->campaign_campaignCPCMessage->totalClick = 0;
						break;
					case Campaign::ENUM_RACE_CPL:
						$cp->campaign_campaignPublisher->loadCplCampaign();
						$cplUser = $cp->campaign_campaignPublisher->campaign_cplCampaign->loadCplCampaign_cplCampaignUser(
							array(
								"cplCampaignUser_publisher"   => $cp->publisher_campaignPublisher,
								"cplCampaign_cplCampaignUser" => $cp->campaign_campaignPublisher->campaign_cplCampaign
							)
						);

						$r[ "perLead" ]  = $cp->campaign_campaignPublisher->campaign_cplCampaign->perLead;

						if($cplUser && $cplUser->gotValue){
							if ( $cplUser && $cplUser->gotValue && strlen( $cplUser->shortUrl ) > 0 )
								$r[ "cplLink" ] = $cplUser->shortUrl;
							else {
								$r[ "noCplLink" ] = true;
								$r[ "cplLink" ]   = Setting::getValue( "CPL_LANDING_DOMAIN" ) . "/" . $cplUser->code;
							}
							$r[ "leads" ]   = $cp->campaign_campaignPublisher->campaign_cplCampaign->getLeadCounts( $cp->publisher_campaignPublisher->id );
						}
						else{
							//something is wrong.
							$r[ "cplLink" ] = "-";
							$r[ "leads" ]   = array( "valid" => 0 );
						}

						break;
					case Campaign::ENUM_RACE_CPL_REMOTE:
						$cp->campaign_campaignPublisher->loadCplRemoteCampaign();
						$cplUser = $cp->campaign_campaignPublisher->campaign_cplRemoteCampaign->loadCplRemoteCampaign_cplRemoteCampaignUser(
							array(
								"cplRemoteCampaignUser_publisher"         => $cp->publisher_campaignPublisher,
								"cplRemoteCampaign_cplRemoteCampaignUser" => $cp->campaign_campaignPublisher->campaign_cplCampaign
							)
						);

						$r[ "perLead" ]  = $cp->campaign_campaignPublisher->campaign_cplRemoteCampaign->perLead;

						if ( $cplUser && $cplUser->gotValue ) {
							if ( $cplUser && $cplUser->gotValue && strlen( $cplUser->shortUrl ) > 0 )
								$r[ "cplLink" ] = $cplUser->shortUrl;
							else {
								$r[ "noCplLink" ] = true;
								$r[ "cplLink" ]   = str_replace("[code]",$cplUser->code,$cp->campaign_campaignPublisher->campaign_cplRemoteCampaign->preparePreviewLink());
							}
							$vleads = $cp->campaign_campaignPublisher->campaign_cplRemoteCampaign->getActiveLeadCount($cp->publisher_campaignPublisher->id);
							$r[ "leads" ] = array(
								"valid" => $vleads,
								"total" => intval($cplUser->pixelCount) + $vleads
							);
						}
						else {
							//something is wrong.
							//TODO : RUN AN ALERT HERE
							DSNotification::addNotification( $cp->campaign_campaignPublisher, "CPL User not found", "Campaigns User not found on publisher campaign detail page", DSNotification::ENUM_TYPE_ERROR );

							$r[ "cplLink" ] = "-";
							$r[ "leads" ]   = array( "valid" => 0, "total"=>0 );
						}

						break;
					case Campaign::ENUM_RACE_CPA_AFF:
						$cp->campaign_campaignPublisher->loadCpaAffCampaign();
						$cpaUser = $cp->campaign_campaignPublisher->campaign_cpaAff->loadCpaAffCampaign_cpaAffCampaignUser(
							array(
								"cpaAffCampaignUser_publisher"         => $cp->publisher_campaignPublisher,
								"cpaAffCampaign_cpaAffCampaignUser" => $cp->campaign_campaignPublisher->campaign_cpaAff
							)
						);
						if ( $cpaUser && $cpaUser->gotValue ) {
							if ( $cpaUser && $cpaUser->gotValue && strlen( $cpaUser->shortUrl ) > 0 )
								$r[ "cpaLink" ] = $cpaUser->shortUrl;
							else {
								$r[ "noCpaLink" ] = true;
								$r[ "cpaLink" ]   = str_replace( "[code]", $cpaUser->code, $cp->campaign_campaignPublisher->campaign_cpaAff->preparePreviewLink() );
							}

							$lstat              = $cpaUser->getLeadStatus();
							$gains              = $cpaUser->getGains();
							$r[ "leads" ]       = $lstat;
							$r[ "gains" ]       = $gains;
							$r[ "gainHistory" ] = $cpaUser->loadGain( null, 0, 50, "id DESC", false );
						}
						else {
							//something is wrong.
							$r[ "cpaLink" ] = "-";
							$r[ "leads" ]   = array( "APPROVED" => 0, "PENDING" => 0 );
						}

						break;
					case Campaign::ENUM_RACE_TWITTER:
						$cp->campaign_campaignPublisher->loadCampaign_campaignTwitterMessage(array(
							"campaign_campaignTwitterMessage"  => $cp->campaign_campaignPublisher->id,
							"publisher_campaignTwitterMessage" => $cp->publisher_campaignPublisher->id
						));
						$cp->loadSnTwitterHistory();
						if (is_null($cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->totalClick)) $cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->totalClick = 0;
						break;
				}
			}

			$r["campaignPublisher"] = $cp;

			if ($cp->state == CampaignPublisher::ENUM_STATE_APPROVED  && $cp->campaign_campaignPublisher->checkForSLS()){
				$r["refStats"] = $cp->getLinkStats(true);
				$r["clickStats"] = array();
				$ucs = new UserClickStat();
				switch($cp->campaign_campaignPublisher->race){
					case Campaign::ENUM_RACE_CPC:
						//load campaignCPCMessage again before the click stats probably updated
						$cp->campaign_campaignPublisher->loadCampaign_campaignCPCMessage(
							array(
								"campaign_campaignCPCMessage"  => $cp->campaign_campaignPublisher->id,
								"publisher_campaignCPCMessage" => $cp->publisher_campaignPublisher->id
							)
						);

						$ucs->populateOnce(false)->load(
							array(
								"linkHash"=>$cp->campaign_campaignPublisher->campaign_campaignCPCMessage->linkHash
							)
						);

						//$r["cpcLink"] = Setting::getValue("SHORTDOMAIN") . $cp->campaign_campaignPublisher->campaign_campaignCPCMessage->linkHash;
						$r["cpcLink"] = $cp->campaign_campaignPublisher->getShortLinkDomain() . $cp->campaign_campaignPublisher->campaign_campaignCPCMessage->linkHash;
						break;
					case Campaign::ENUM_RACE_TWITTER:
						if (strlen($cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->linkHash)) $ucs->populateOnce(false)->load(array("linkHash"=>$cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->linkHash));
						break;
				}
				if ($ucs->gotValue){
					do{
						$r["clickStats"][$ucs->clickDate] = array(
							"calculatedClick" => $ucs->calculatedClick,
							"suspectedClick" => $ucs->suspectedClick,
							"totalClick" => $ucs->totalClick
						);
					} while($ucs->populate());
				}
				$r["charLimit"] = Setting::getValue("campaignCharLimitWithLink");
				$r["hasCountableLink"] = true;
			}
			else {
				$r["refStats"] = null;
				$r["hasCountableLink"] = false;
				if (strlen($cp->campaign_campaignPublisher->campaign_campaignClick->link)){
					$r["charLimit"] = Setting::getValue("campaignCharLimitWithLink");
				} else {
					$r["charLimit"] = Setting::getValue("campaignCharLimitWithoutLink");
				}
			}

		}
		return $r;
	}

	/**
	 * Campaign accept for publisher
	 * @param array $params Array of required parameters
	 * @param CampaignPublisher $cp
	 *
	 * @return array
	 * @author Murat
	 */
	public static function pubAcceptCamp($params,CampaignPublisher $cp){
		global $_S;

		JPDO::beginTransaction();
		try {
			$cost = 0;
			$noRevenue=false;
			if (!is_object($cp->publisher_campaignPublisher)){
				$cp->loadPublisher();
			}
			if (!is_object($cp->publisher_campaignPublisher->user_publisher)){
				$cp->publisher_campaignPublisher->loadUser();
			}
			switch($cp->campaign_campaignPublisher->race){
				case Campaign::ENUM_RACE_TWITTER:
					//Reserve budget
					$cp->campaign_campaignPublisher->budgetAction($cp->cost*(-1)*$cp->campaign_campaignPublisher->multiplier, $cp->publisher_campaignPublisher->id, "User ({$cp->publisher_campaignPublisher->user_publisher->username}) Added");

					$ctm = new CampaignTwitterMessage();
					//Load for double check, if the record exists, we should update it
					$ctm->load(array(
						"campaign_campaignTwitterMessage" => $cp->campaign_campaignPublisher->id,
						"publisher_campaignTwitterMessage" => $cp->publisher_campaignPublisher->id
					));
					if (!$ctm->gotValue){
						$ctm->campaign_campaignTwitterMessage = $cp->campaign_campaignPublisher->id;
						$ctm->publisher_campaignTwitterMessage = $cp->publisher_campaignPublisher->id;
					}

					$tsSendDate = date_parse_from_format($params["dateFormatString"],$params["sendDate"]);
					$tsSendDate = strtotime($tsSendDate["year"] . "-" . $tsSendDate["month"] . "-" . $tsSendDate["day"] . " " . $params["sendTime"]);

					$ctm->state = $params["setCustomMessage"] == "Y" ? CampaignTwitterMessage::ENUM_STATE_WAITING_ADMIN_APPROVE : CampaignTwitterMessage::ENUM_STATE_WAITING;
					$ctm->message = ($params["setCustomMessage"] == "Y" ? $params["customMessage"] : $cp->campaign_campaignPublisher->campaign_campaignMessage->message) . $cp->createShortLink($ctm) . " #ad";
					$ctm->toSendDate = $tsSendDate;
					$ctm->isCustomMessage = ($params["setCustomMessage"] == "Y");
					$ctm->msgindex = 0;
					$ctm->tryCount = 0;
					$ctm->save();

					if ($params["setCustomMessage"] != "Y"){
						$ctm->createCronForTweetSend();
					} else {
						global $_S,$SYSTEM_EMAILS;
						$subject = "[adMingle] Publisher custom effect campaign message approval";
						$message = "Publisher named '" . $cp->publisher_campaignPublisher->user_publisher->username . "' entered a custom message for '" . $cp->campaign_campaignPublisher->title . "' campaign. Click <a href='{$_S}/ds_campaignCustomMessages'>here</a> to review it.";
						Mail::createManual($SYSTEM_EMAILS["info"],$subject,$message);
					}

					$cost = $cp->cost;

					$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
						'V2_acceptedCampaign',
						array("name"=>$cp->campaign_campaignPublisher->title),
						array(
							"name" => $cp->publisher_campaignPublisher->getFullName(),
							"campName" => $cp->campaign_campaignPublisher->title,
							"publishDate" => date('d/m/Y', $ctm->toSendDate),
							"publishTime" => date('H:i', $ctm->toSendDate),
							"campDetail" => $_S."myCampDetail/".$cp->campaign_campaignPublisher->publicID
						),
						$cp->campaign_campaignPublisher->id,
						null,
						null,
						$cp->campaign_campaignPublisher->getPreviewImageLink()
					);

					break;
				case Campaign::ENUM_RACE_CPC:
					$ccm = new CampaignCPCMessage();
					$ccm->load(array(
						"campaign_campaignCPCMessage" => $cp->campaign_campaignPublisher->id,
						"publisher_campaignCPCMessage" => $cp->publisher_campaignPublisher->id
					));
					if (!$ccm->gotValue){
						$ccm->campaign_campaignCPCMessage = $cp->campaign_campaignPublisher->id;
						$ccm->publisher_campaignCPCMessage = $cp->publisher_campaignPublisher->id;
					}
					$cp->createShortLink($ccm);
					$ccm->finishingWarningSent = 0;
					$ccm->finishedWarningSent = 0;
					$ccm->isBlocked = 0;
					$ccm->hasBlockedReferrer = 0;
					$ccm->save();

					if (!is_object($cp->campaign_campaignPublisher->campaign_campaignCPC))
						$cp->campaign_campaignPublisher->loadCampaign_campaignCPC();

					$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
						'V2_acceptedCPCCampaign',
						array("campName"=>$cp->campaign_campaignPublisher->title),
						array(
							"name" => $cp->publisher_campaignPublisher->getFullName(),
							"campName" => $cp->campaign_campaignPublisher->title,
							"clickRevenue" => Format::getFormattedCurrency($cp->campaign_campaignPublisher->campaign_campaignCPC->cpcCost),
							"campDetail" => $_S."myCampDetail/".$cp->campaign_campaignPublisher->publicID
						),
						$cp->campaign_campaignPublisher->id,
						null,
						null,
						$cp->campaign_campaignPublisher->getPreviewImageLink()
					);

					break;
				case Campaign::ENUM_RACE_CPL:
					$cpl        = $cp->campaign_campaignPublisher->loadCplCampaign();
					$ccu        = $cpl->getCode( $cp->publisher_campaignPublisher );

					//TODO : Make this async.
					#$shortUrl = CplCampaignUser::getShortLink( $cpl->preHash . "/" . $ccu->code );
					#$ccu->shortUrl = $shortUrl; // lazy load.

					$ccu->isActive = 1;
					$ccu->cplCampaign_cplCampaignUser = $cpl->id;
					$ccu->save();

					$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
						'V2_acceptedCPLCampaign',
						array( "campName" => $cp->campaign_campaignPublisher->title ),
						array(
							"name"         => $cp->publisher_campaignPublisher->getFullName(),
							"campName"     => $cp->campaign_campaignPublisher->title,
							"clickRevenue" => Format::getFormattedCurrency( $cpl->perLead ),
							"campDetail"   => $_S . "myCampDetail/" . $cp->campaign_campaignPublisher->publicID
						),
						$cp->campaign_campaignPublisher->id,
						null,
						null,
						$cp->campaign_campaignPublisher->getPreviewImageLink()
					);
					$noRevenue = true;

					break;
				case Campaign::ENUM_RACE_CPL_REMOTE:
					$cpl        = $cp->campaign_campaignPublisher->loadCplRemoteCampaign();
					$ccu        = $cpl->getCode( $cp->publisher_campaignPublisher );

					//TODO : Make this async.
					#$shortUrl = CplCampaignUser::getShortLink( $cpl->preHash . "/" . $ccu->code );
					#$ccu->shortUrl = $shortUrl; // lazy load.

					$ccu->isActive = 1;
					$ccu->cplRemoteCampaign_cplRemoteCampaignUser = $cpl->id;
					$ccu->save();

					$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
						'V2_acceptedCPLCampaign',
						array( "campName" => $cp->campaign_campaignPublisher->title ),
						array(
							"name"         => $cp->publisher_campaignPublisher->getFullName(),
							"campName"     => $cp->campaign_campaignPublisher->title,
							"clickRevenue" => Format::getFormattedCurrency( $cpl->perLead ),
							"campDetail"   => $_S . "myCampDetail/" . $cp->campaign_campaignPublisher->publicID
						),
						$cp->campaign_campaignPublisher->id,
						null,
						null,
						$cp->campaign_campaignPublisher->getPreviewImageLink()
					);

					break;
				case Campaign::ENUM_RACE_CPA_AFF:
					$cpa        = $cp->campaign_campaignPublisher->loadCpaAffCampaign();
					$ccu        = $cpa->getCode( $cp->publisher_campaignPublisher );

					$ccu->isActive = 1;
					$ccu->cpaAffCampaign_cpaAffCampaignUser = $cpa->id;
					$ccu->save();

					$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
						'V2_acceptedCPAAffCampaign',
						array( "campName" => $cp->campaign_campaignPublisher->title ),
						array(
							"name"         => $cp->publisher_campaignPublisher->getFullName(),
							"campName"     => $cp->campaign_campaignPublisher->title,
							"campDetail"   => $_S . "myCampDetail/" . $cp->campaign_campaignPublisher->publicID
						),
						$cp->campaign_campaignPublisher->id,
						null,
						null,
						$cp->campaign_campaignPublisher->getPreviewImageLink()
					);

					break;
				default:
					die("Unknown campaign");
					break;
			}

			if($noRevenue === false){
			$pr = new PublisherRevenue();
				$pr->load(array(
					"campaign_publisherRevenue" => $cp->campaign_campaignPublisher->id,
					"publisher_publisherRevenue" => $cp->publisher_campaignPublisher->id
				));
				if (!$pr->gotValue){
					$pr->campaign_publisherRevenue = $cp->campaign_campaignPublisher->id;
					$pr->publisher_publisherRevenue = $cp->publisher_campaignPublisher->id;
				}

				$pr->status = PublisherRevenue::ENUM_STATUS_WAITING;
				$pr->amount = $cost;
				$pr->msgindex = 0;
				$pr->save();
			}

			$cp->changeStatus(CampaignPublisher::ENUM_STATE_APPROVED);

			$cp->campaign_campaignPublisher->writeHistory(CampaignHistory::ENUM_ACTIONID_CAMPAIGN_APPROVE,"Approved by [{$cp->publisher_campaignPublisher->user_publisher->username}]",CampaignHistory::ENUM_MADEBY_USER,array("cpid"=>$cp->id));
			$cp->publisher_campaignPublisher->writeHistory(UserHistory::ENUM_ACTIONID_CAMPAIGN_APPROVE,"[" . $cp->campaign_campaignPublisher->title . "] campaign approved",UserHistory::ENUM_MADEBY_USER,array("cpid"=>$cp->id));

			JPDO::commit();
			return true;
		} catch (Exception $ex){
			JPDO::rollback();
			JLOG::log("cri","Exception occured while accepting campaign #cpid: {$cp->id} : " . serialize($ex));
			return false;
		}
	}

	/**
	 * Campaign deny for publisher
	 * @param array $params Array of required parameters
	 * @param CampaignPublisher $cp
	 *
	 * @return array
	 * @author Murat
	 */
	public static function pubDenyCamp($params,CampaignPublisher $cp){
		JPDO::beginTransaction();
		try {
			$cost = 0;
			if (!is_object($cp->publisher_campaignPublisher)){
				$cp->loadPublisher();
			}

			$cp->changeStatus(CampaignPublisher::ENUM_STATE_REJECTED);

			$cdr = new CampaignDenyReason();
			$cdr->reasonId = $params["reasonId"];
			$cdr->reasonText = $params["reasonText"];
			$cdr->campaignPublisher_campaignDenyReason = $cp->id;
			$cdr->save();

			$cp->campaign_campaignPublisher->writeHistory(CampaignHistory::ENUM_ACTIONID_CAMPAIGN_DENY,"Denied by [{$cp->publisher_campaignPublisher->user_publisher->username}]",CampaignHistory::ENUM_MADEBY_USER,array("cpid"=>$cp->id));
			$cp->publisher_campaignPublisher->writeHistory(UserHistory::ENUM_ACTIONID_CAMPAIGN_DENY,"[" . $cp->campaign_campaignPublisher->title . "] campaign denied",UserHistory::ENUM_MADEBY_USER,array("cpid"=>$cp->id));

			JPDO::commit();
			return true;
		} catch (Exception $ex){
			JPDO::rollback();
			JLOG::log("cri","Exception occured while denying campaign #cpid: {$cp->id} : " . serialize($ex));
			return false;
		}
	}

	/**
	 * Campaign list for publisher
	 *
	 * @param array $params Array of required parameters
	 *
	 * @return array
	 * @author Murat
	 */
	public static function pubCampList($params){
		$cp = new CampaignPublisher();
		$cp->with("campaign_campaignPublisher");

		$loadParams = $cp->createPublisherFilter($params["campStatus"],$params["pid"]);
		$cp->count("id","cnt",$loadParams);

		$r = array("recordCount" => $cp->cnt, "camps" => array(), "currentPage" => 0, "campListItemCount" => 0, "pageCount" => 1);

		$params["page"] = intval($params["page"]);
		if ($params["page"] < 1) $params["page"] = 1;

		$r["currentPage"] = $params["page"];

		$params["page"]--;
		$campListItemCount = Setting::getValue("campListItemCount");
		if (empty($campListItemCount)) $campListItemCount = 10;
		$r["campListItemCount"] = $campListItemCount;

		if ($r["recordCount"] > $r["campListItemCount"]){
			$r["pageCount"] = ceil($r["recordCount"] / $r["campListItemCount"]);
		}
		$cp = new CampaignPublisher();
		$cp->orderBy("id DESC")->with("campaign_campaignPublisher")->limit($params["page"]*$campListItemCount,$campListItemCount)->load($loadParams);
		if ($cp->gotValue){
			do {
				if ( $cp->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPC ) {
					$cp->campaign_campaignPublisher->loadCampaign_campaignCPC();
					$cost = $cp->campaign_campaignPublisher->campaign_campaignCPC->cpcCost;
				}
				elseif ( $cp->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPL ) {
					$cp->campaign_campaignPublisher->loadCplCampaign();
					if(is_object($cp->campaign_campaignPublisher->campaign_cplCampaign))
						$cost = $cp->campaign_campaignPublisher->campaign_cplCampaign->perLead;
					else
						$cost = 0 ;
				}
				elseif ( $cp->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPL_REMOTE ) {
					$cp->campaign_campaignPublisher->loadCplRemoteCampaign();
					if(is_object($cp->campaign_campaignPublisher->campaign_cplRemoteCampaign))
						$cost = $cp->campaign_campaignPublisher->campaign_cplRemoteCampaign->perLead;
					else
						$cost = 0 ;
				}
				elseif ( $cp->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPA_AFF ) {
					$cp->campaign_campaignPublisher->loadCpaAffCampaign();
					if(is_object($cp->campaign_campaignPublisher->campaign_cpaAff))
						$cost = 0; //TODO: WHAT WILL BE ?
					else
						$cost = 0 ;
				}
				elseif ( $cp->campaign_campaignPublisher->race == Campaign::ENUM_RACE_TWITTER ) {
					$cp->campaign_campaignPublisher->loadCampaign_campaignTwitter();
					$cost = $cp->cost;
				}
				else {
					$cost = "Free";
				}

				$r["camps"][] = array(
					"publicID" => $cp->campaign_campaignPublisher->publicID,
					"race" => $cp->campaign_campaignPublisher->race,
					"title" => $cp->campaign_campaignPublisher->title,
					"date" => $cp->campaign_campaignPublisher->startDate,
					"endDate" => $cp->campaign_campaignPublisher->endDate,
					"availableDate" => $cp->campaign_campaignPublisher->startDate,
					"status" => $cp->campaign_campaignPublisher->state,
					"publisherStatus" => $cp->state,
					"price" => $cost
				);
			} while($cp->populate());
		}

		unset($cp);

		return $r;
	}

	/**
	 * Campaign list for advertiser
	 *
	 * @param array $params Array of required parameters
	 *
	 * @return array
	 * @author Murat
	 */
	public static function advertiserCampList( &$params ) {
		$c      = new Campaign();
		$states = array();

		switch($params["campStatus"]){
			case "o":
				$states = array(
					Campaign::ENUM_STATE_INITIAL,
					Campaign::ENUM_STATE_ACCEPTED,
					Campaign::ENUM_STATE_OFFER_WAITING,
					Campaign::ENUM_STATE_OFFER_APPROVED,
					Campaign::ENUM_STATE_WAITING_PAYMENT
				);
				break;
			case "a":
				$states = array(
					Campaign::ENUM_STATE_PENDING_PROGRESS,
					Campaign::ENUM_STATE_PROGRESSING
				);
				break;
			case "f":
				$states = array(
					Campaign::ENUM_STATE_FINISHED
				);
				break;
			case "r":
				$states = array(
					Campaign::ENUM_STATE_DECLINED,
					Campaign::ENUM_STATE_OFFER_DECLINED
				);
				break;
		}

		$filter = array( "advertiser" => $params[ "advertiser" ], "state" => $states );

		$c->count( "id", "cnt", $filter );

		$r = array( "stateOptions"=> $c->state_options, "recordCount" => $c->cnt, "camps" => array(), "currentPage" => 0, "campListItemCount" => 0, "pageCount" => 1 );

		$params[ "page" ] = intval( $params[ "page" ] );
		if ( $params[ "page" ] < 1 )
			$params[ "page" ] = 1;

		$r[ "currentPage" ] = $params[ "page" ];

		$params[ "page" ]--;
		$campListItemCount = Setting::getValue( "campListItemCount" );

		if ( empty( $campListItemCount ) ) $campListItemCount = 10;

		$r[ "campListItemCount" ] = $campListItemCount;

		if ( $r[ "recordCount" ] > $r[ "campListItemCount" ] ) {
			$r[ "pageCount" ] = ceil( $r[ "recordCount" ] / $r[ "campListItemCount" ] );
		}
		$c = new Campaign();
		$c->orderBy( "id DESC" )->limit( $params[ "page" ] * $campListItemCount, $campListItemCount )->load( $filter );

		if ( $c->gotValue ) {
			do {
				$r[ "camps" ][ ] = array(
					"publicID"      => $c->publicID,
					"race"          => $c->race,
					"title"         => $c->title,
					"date"          => $c->startDate,
					"endDate"       => $c->endDate,
					"availableDate" => $c->startDate,
					"status"        => $c->state,
					"budget"        => $c->budget
				);
			} while ( $c->populate() );
		}

		unset( $cp );

		return $r;
	}

	public static function getCampaignStateLabel( $status ) {
		$r = null;
		switch($status){

			case Campaign::ENUM_STATE_INITIAL:
			case Campaign::ENUM_STATE_ACCEPTED:
			case Campaign::ENUM_STATE_OFFER_WAITING:
			case Campaign::ENUM_STATE_OFFER_APPROVED:
			case Campaign::ENUM_STATE_WAITING_PAYMENT:
				$r = "o";
				break;

			case Campaign::ENUM_STATE_PENDING_PROGRESS:
			case Campaign::ENUM_STATE_PROGRESSING:
				$r =  "a";
				break;

			case Campaign::ENUM_STATE_FINISHED:
				$r =  "f";
				break;

			case Campaign::ENUM_STATE_DECLINED:
			case Campaign::ENUM_STATE_OFFER_DECLINED:
				$r =  "r";
				break;
		}

		return $r;

	}

	/**
	 * Create campaign first step
	 *
	 * @param array $params Array of required parameters
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function createStep1($params){
		$c = new Campaign();
		if (!empty($params["campaign"])){
			$c = $params["campaign"];
		}
		$c->advertiser = $params["advertiserId"];
		$c->race = $params["race"];
		$c->campCode = date("YmdHi");
		$c->save();
		if (empty($params["campaign"])){
			$c->campCode .= "_" . $c->id;
			$c->publicID = md5($c->campCode);
			$c->save();
		}
		//Create history record for that action
		$c->writeHistory(
			CampaignHistory::ENUM_ACTIONID_CAMPAIGN_CREATED,
			"Campaign created",
			CampaignHistory::ENUM_MADEBY_ADMIN,
			array("admin"=>isset($_SESSION["DS_userId"])?$_SESSION["DS_userId"]:null)
		);

		return $c;
	}

	/**
	 * Create campaign second step
	 *
	 * @param array $params Array of required parameters
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function createStep2($params){
//		$c = new Campaign();
		$c = $params["campaign"];
		$c->title = $params["title"];
		$c->startDate = $params["startDate"];
		$c->endDate = $params["endDate"];

		if ($params["brand"] == -1){
			$brand = new AdvertiserBrand();
			$brand->advertiser_advertiserBrand = $params["advertiserId"];
			$brand->name = $params["brandName"];
			$brand->status = AdvertiserBrand::ENUM_STATUS_ACTIVE;
			$brand->save();
			$c->brand = $brand->id;
			unset($brand);
		} else {
			$c->brand = $params["brand"];
		}
		$c->save();

		return $c;
	}

	/**
	 * Create campaign third step
	 *
	 * @param array $params Array of required parameters
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function createStep3($params){
//		$c = new Campaign();
		$c = $params["campaign"];
		$c->loadCampaign_campaignFilter();

		if (!$c->campaign_campaignFilter->gotValue){
			$c->campaign_campaignFilter->advertiser_campaignFilter = $params[ "advertiserId" ];
			$c->campaign_campaignFilter->campaign_campaignFilter   = $c->id;
		}
		$c->campaign_campaignFilter->age       = $params[ "age" ];
		$c->campaign_campaignFilter->sex       = $params[ "sex" ];
		$c->campaign_campaignFilter->noAutoAdd = 0;
		$c->campaign_campaignFilter->save();

		$trend = new Trend();
		$trend->load(array("id"=>$params["interests"]));
		$c->campaign_campaignFilter->deleteAllTrends_campaignFilter();
		$c->campaign_campaignFilter->saveTrends_campaignFilter($trend);

		$c->campaign_campaignFilter->deleteAllIncClub_campaignFilter();
		if (!empty($params["incClubs"]) && count($params["incClubs"])){
			$incClubs = new Club();
			$incClubs->load(array("id"=>$params["incClubs"]));
			$c->campaign_campaignFilter->saveIncClub_campaignFilter($incClubs);
		}

		$c->campaign_campaignFilter->deleteAllExcClub_campaignFilter();
		if (!empty($params["excClubs"]) && count($params["excClubs"])){
			$excClubs = new Club();
			$excClubs->load(array("id"=>$params["excClubs"]));
			$c->campaign_campaignFilter->saveExcClub_campaignFilter($excClubs);
		}
		$c->campaign_campaignFilter->deleteAllExcCampaign_campaignFilter();
		if (!empty($params["excludedCampaign"]) && count($params["excludedCampaign"])){
			$excludedCampaign = new Campaign();
			$excludedCampaign->load(array("id"=>$params["excludedCampaign"]));
			$c->campaign_campaignFilter->saveExcCampaign_campaignFilter($excludedCampaign);
		}
		$c->campaign_campaignFilter->deleteAllCity_campaignFilter();
		if (!empty($params["city"]) && count($params["city"])){
			$city = new City();
			$city->populateOnce(false)->load(array("id"=>$params["city"]));
			$c->campaign_campaignFilter->saveCity_campaignFilter($city);
		}
		return $c;
	}

	/**
	 * Create campaign fourth step
	 *
	 * @param array $params Array of required parameters
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function createStep4($params){
//		$c = new Campaign();
		$c = $params["campaign"];

		if ( $params["race"] == Campaign::ENUM_RACE_CPL ){
			$c->loadCplCampaign();
			if ( !is_object( $c->campaign_cplCampaign ) || !$c->campaign_cplCampaign->gotValue ) {
				$c->campaign_cplCampaign                       = new CplCampaign( $params[ "layout" ] );
				$c->campaign_cplCampaign->campaign_cplCampaign = $c->id;
			}
			elseif ( $c->campaign_cplCampaign->id != $params[ "layout" ] ) { // layout changed
				$ppf = $c->campaign_cplCampaign->previewFileName;
				$c->deleteCplCampaign(); // delete the layout
				$c->campaign_cplCampaign = new CplCampaign( $params[ "layout" ] );
				$c->campaign_cplCampaign->previewFileName = $ppf;  // getting the previous preview file.
			}

			if (!empty($params["previewFile"]) && !empty($params["previewFile"]["name"]))
				$c->campaign_cplCampaign->previewFileName = $c->savePreviewImage( $params );

			$c->campaign_cplCampaign->campDetail             = $params[ "campDetail" ];
			$c->campaign_cplCampaign->SLDomain               = $params["SLDomain"];

			if (mb_substr( $c->campaign_cplCampaign->campDetail, mb_strlen($c->campaign_cplCampaign->campDetail,"UTF-8")-4, 4, "UTF-8" ) != " #ad"){
				if (strlen($c->campaign_cplCampaign->campDetail) > 146){
					$c->campaign_cplCampaign->campDetail = mb_substr($c->campaign_cplCampaign->campDetail,0,146, "UTF-8");
//					$c->campaign_cplCampaign->campDetail = substr($c->campaign_cplCampaign->campDetail,0,146);
				}
				$c->campaign_cplCampaign->campDetail .= " #ad";
			}

			$c->campaign_cplCampaign->save();

		}
		elseif ( $params["race"] == Campaign::ENUM_RACE_CPL_REMOTE ){
			$c->loadCplRemoteCampaign();
			if ( !is_object( $c->campaign_cplRemoteCampaign ) || !$c->campaign_cplRemoteCampaign->gotValue ) {
				$c->campaign_cplRemoteCampaign                             = new CplRemoteCampaign();
				$c->campaign_cplRemoteCampaign->campaign_cplRemoteCampaign = $c->id;
			}



			if (!empty($params["previewFile"]) && !empty($params["previewFile"]["name"]))
				$c->campaign_cplRemoteCampaign->previewFileName = $c->savePreviewImage( $params );

			$c->campaign_cplRemoteCampaign->campDetail             = $params[ "campDetail" ];
			$c->campaign_cplRemoteCampaign->link                   = $params["link"];
			$c->campaign_cplRemoteCampaign->SLDomain               = $params["SLDomain"];
			$c->campaign_cplRemoteCampaign->affService             = $params[ "affService" ];
			$c->campaign_cplRemoteCampaign->opType                 = $params[ "opType" ];

			if (mb_substr( $c->campaign_cplRemoteCampaign->campDetail, mb_strlen($c->campaign_cplRemoteCampaign->campDetail,"UTF-8")-4, 4, "UTF-8" ) != " #ad"){
				if (strlen($c->campaign_cplRemoteCampaign->campDetail) > 146){
					$c->campaign_cplRemoteCampaign->campDetail = mb_substr($c->campaign_cplRemoteCampaign->campDetail,0,146, "UTF-8");
				}
				$c->campaign_cplRemoteCampaign->campDetail .= " #ad";
			}

			$c->campaign_cplRemoteCampaign->save();

		}
		elseif ( $params["race"] == Campaign::ENUM_RACE_CPA_AFF ){
			$c->loadCpaAffCampaign();
			if ( !is_object( $c->campaign_cpaAff ) || !$c->campaign_cpaAff->gotValue ) {
				$c->campaign_cpaAff                             = new CpaAffCampaign();
				$c->campaign_cpaAff->campaign_cpaAff = $c->id;
			}

			if (!empty($params["previewFile"]) && !empty($params["previewFile"]["name"]))
				$c->campaign_cpaAff->previewFileName = $c->savePreviewImage( $params );

			$c->campaign_cpaAff->campDetail     = $params[ "campDetail" ];
			$c->campaign_cpaAff->commissionInfo = $params[ "commissionInfo" ];
			$c->campaign_cpaAff->hashCode       = $params[ "hashCode" ];
			$c->campaign_cpaAff->cpaAffService  = $params[ "affService" ];
			$c->campaign_cpaAff->opType         = $params[ "opType" ];
			$c->campaign_cpaAff->revPerc        = $params[ "revPerc" ];
			$c->campaign_cpaAff->link           = $params[ "link" ];
			$c->campaign_cpaAff->SLDomain       = $params[ "SLDomain" ];

			if (mb_substr( $c->campaign_cpaAff->campDetail, mb_strlen($c->campaign_cpaAff->campDetail,"UTF-8")-4, 4, "UTF-8" ) != " #ad"){
				if (strlen($c->campaign_cpaAff->campDetail) > 146){
					$c->campaign_cpaAff->campDetail = mb_substr($c->campaign_cpaAff->campDetail,0,146, "UTF-8");
				}
				$c->campaign_cpaAff->campDetail .= " #ad";

			}

			$c->campaign_cpaAff->save();

		}
		else{
			$c->loadCampaignClick();

			if (!$c->campaign_campaignClick->gotValue){
				$c->campaign_campaignClick->campaign_campaignClick = $c->id;
			}

			if ( !empty( $params[ "previewFile" ] ) && !empty( $params[ "previewFile" ][ "name" ] ) )
				$c->campaign_campaignClick->previewFileName = $c->savePreviewImage( $params );


			$c->campaign_campaignClick->link = $params["link"];
			$c->campaign_campaignClick->SLDomain = $params["SLDomain"];
			$c->campaign_campaignClick->hashCode = $params["hashCode"];
			$c->campaign_campaignClick->campDetail = $params["campDetail"];
			$c->campaign_campaignClick->calculatedClick = 0;
			$c->campaign_campaignClick->suspectedClick = 0;
			$c->campaign_campaignClick->totalClick = 0;

			if (mb_substr( $c->campaign_campaignClick->campDetail, mb_strlen($c->campaign_campaignClick->campDetail,"UTF-8")-4, 4, "UTF-8" ) != " #ad"){
				if (strlen($c->campaign_campaignClick->campDetail) > 146){
//					$c->campaign_campaignClick->campDetail = substr($c->campaign_campaignClick->campDetail,0,146);
					$c->campaign_campaignClick->campDetail = mb_substr($c->campaign_campaignClick->campDetail,0,146, "UTF-8");
				}
				$c->campaign_campaignClick->campDetail .= " #ad";
			}
// CONSIDER BELOW IF SOMETHING FAILS
//			$c->campaign_campaignClick->campDetail = str_replace("\0", "", $c->campaign_campaignClick->campDetail);
//			$c->campaign_campaignClick->campDetail = utf8_encode( $c->campaign_campaignClick->campDetail );

//			iconv_set_encoding("internal_encoding", "UTF-8");
//			iconv_set_encoding("output_encoding", "UTF-8");
			$c->campaign_campaignClick->save();
		}

		$c->keywords = $params["keywords"];
		$c->notes = $params["campNotes"];
		$c->save();

		$c->deleteAllCampaign_campaignMessage();
		foreach ( $params[ "message" ] as $key => $message ) {
			if ( strlen( trim( $message ) ) ) {
				$cm                           = new CampaignMessage();
				$cm->campaign_campaignMessage = $c->id;
				$cm->message                  = $message;
				$cm->save();
				//JPDO::debugQuery(true);
			}
		}

		return $c;
	}

	/**
	 * Create campaign fifth step
	 *
	 * @param array $params Array of required parameters
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function createStep5($params){
//		$c = new Campaign();
		$c = $params["campaign"];
		$c->deleteAllCampaign_campaignPublisher();
		//Tool::echoLog("Old users deleted from campaign");
		$counts = $c->collectUsers();
		//Tool::echoLog("New users collected for campaign");
		$c->state = $params["state"];
		$c->commissionRate = $params["commissionRate"];
		switch ($c->race){
			case Campaign::ENUM_RACE_CPC:
				$c->loadCampaign_campaignCPC();
				if (!$c->campaign_campaignCPC->gotValue){
					$c->campaign_campaignCPC->campaign_campaignCPC = $c->id;
				}
				$c->campaign_campaignCPC->extendMailSent = 0;
				$c->campaign_campaignCPC->finishingAlertSent = 0;
				$c->campaign_campaignCPC->maxClick = $params["maxClick"];
				$c->campaign_campaignCPC->cpcCost = $params["cpcPrice"];
				$c->campaign_campaignCPC->perAdvertiserClick = $params[ "perAdvertiserClick" ];
				$c->campaign_campaignCPC->maxAnonRefCount = $params["maxAnonRefCount"];
				$c->campaign_campaignCPC->keepRedirecting = $params["keepRedirecting"];
				$c->multiplier                               = number_format( ( $params[ "perAdvertiserClick" ] / $params[ "cpcPrice" ] )/** ( 100 / ( 100 - $c->commissionRate ) )*/, 2 );

				$initBudget    = $c->campaign_campaignCPC->maxClick * $c->multiplier * $c->campaign_campaignCPC->cpcCost;
				$c->commission = floor(round( $initBudget * $c->commissionRate / 100, 2 )); // reverse calculation
				$c->budget     = floor($initBudget - $c->commission); // absoute math to support old code.

				$c->campaign_campaignCPC->save();

				break;
			case Campaign::ENUM_RACE_CPL:
				$c->loadCplCampaign();
				if (!$c->campaign_cplCampaign->gotValue){
					$c->campaign_cplCampaign->campaign_cplCampaign = $c->id;
				}

				$c->campaign_cplCampaign->totalBudget            = $params[ "budget" ];
				$c->campaign_cplCampaign->maxLeadLimit           = $params[ "maxLeadLimit" ];
				$c->campaign_cplCampaign->maxOverlappingNegative = $params[ "maxOverlappingNegative" ];
				$c->campaign_cplCampaign->maxPerHourPerIp        = $params[ "maxPerHourPerIp" ];
				$c->campaign_cplCampaign->perLead                = $params[ "perLead" ];
				$c->multiplier                                   = Setting::getValue( "CPL_MULTIPLIER" );
				$c->budget                                       = $c->campaign_cplCampaign->maxLeadLimit * $c->multiplier * $c->campaign_cplCampaign->perLead;
				$c->commission                                   = round($c->budget * ( ( 100 / ( 100 - $c->commissionRate ) ) - 1 ),2); // reverse calculation

				$c->campaign_cplCampaign->save();

				break;
			case Campaign::ENUM_RACE_CPL_REMOTE:
				$c->loadCplRemoteCampaign();
				if (!$c->campaign_cplRemoteCampaign->gotValue){
					$c->campaign_cplRemoteCampaign->campaign_cplRemoteCampaign = $c->id;
				}

				$c->campaign_cplRemoteCampaign->totalBudget       = $params[ "budget" ];
				$c->campaign_cplRemoteCampaign->maxLeadLimit      = $params[ "maxLeadLimit" ];
				$c->campaign_cplRemoteCampaign->perLead           = $params[ "perLead" ];
				$c->campaign_cplRemoteCampaign->perAdvertiserLead = $params[ "perAdvertiserLead" ];
				$c->campaign_cplRemoteCampaign->hashCode          = $params[ "hashCode" ];
				$c->commissionRate                                = 0;
				$c->multiplier                                    = 1;
				$c->budget                                        = $c->campaign_cplRemoteCampaign->maxLeadLimit * $c->campaign_cplRemoteCampaign->perAdvertiserLead;
				$c->commission                                    = 0;

				$c->campaign_cplRemoteCampaign->save();

				break;
			case Campaign::ENUM_RACE_CPA_AFF:
				$c->loadCpaAffCampaign();
				if (!$c->campaign_cpaAff->gotValue){
					$c->campaign_cpaAff->campaign_cpaAff = $c->id;
				}

				$c->commissionRate                                = 0;
				$c->multiplier                                    = 1;
				$c->budget                                        = 0;
				$c->commission                                    = 0;

				$c->campaign_cpaAff->save();

				break;
			case Campaign::ENUM_RACE_TWITTER:
				$c->multiplier = Setting::getValue("PROFIT_MULTIPLIER");
				$c->budget = $params["budget"] * ((100 - $c->commissionRate) / 100);

				$c->loadCampaign_campaignTwitter();
				if (!$c->campaign_campaignTwitter->gotValue){
					$c->campaign_campaignTwitter->campaign_campaignTwitter = $c->id;
				}
				$c->campaign_campaignTwitter->campType = CampaignTwitter::ENUM_CAMPTYPE_STANDART;
				$c->campaign_campaignTwitter->celebCost = 0;
				$c->campaign_campaignTwitter->influencer = $counts["influence"];
				$c->campaign_campaignTwitter->follower = $counts["follower"];
				$c->campaign_campaignTwitter->celebCost = $counts["celebCost"];
				$c->campaign_campaignTwitter->maxCost = $counts["cost"] * $c->multiplier * (100 / (100-$c->commissionRate));
				$c->commission                           = round($c->budget * ( ( 100 / ( 100 - $c->commissionRate ) ) - 1 ),2); // reverse calculation

				$c->campaign_campaignTwitter->save();

				break;
		}

		$c->publisherCount = $counts["count"];
		$c->extraBudget = 0;

		if(isset($params["share"]))
			$c->shareOptions = json_encode($params["share"]);

		/*
		echo "budget: " . PHP_EOL;
		var_dump($c->budget);
		echo PHP_EOL;

		echo "commissionRate: " . PHP_EOL;
		var_dump($c->commissionRate);
		echo PHP_EOL;

		echo "commission: " . PHP_EOL;
		var_dump($c->budget);
		echo PHP_EOL;


		die("budget : " . $c->budget . " | commissionRate: " . $c->commissionRate . " | commission: " . $c->commission);
		*/

		$c->save();

		return $c;
	}

	/**
	 * Get filtered users or get count of filtered users
	 *
	 * @param bool $onlyStats If this parameter set to true, this function only return counts
	 * @param bool $celebrityOnly If this parameter set to true, this function only return celebrities
	 * @param int $spesificPublisherId This parameter can only called by Publisher->addToActiveCampaigns method
	 *
	 * @return Publisher
	 * @author Murat
	 */
	public function getUsers($onlyStats = false,$celebrityOnly=false,$spesificPublisherId=null){

		if (!is_object($this->campaign_campaignFilter)){
			$this->loadCampaign_campaignFilter();
		}

		if (!is_object($this->
			campaign_campaignFilter->
			trends_campaignFilter)){
			$this->campaign_campaignFilter->loadTrends_campaignFilter();
		}
		if (!is_object($this->campaign_campaignFilter->incClub_campaignFilter)){
			$this->campaign_campaignFilter->loadIncClub_campaignFilter();
		}
		if (!is_object($this->campaign_campaignFilter->excClub_campaignFilter)){
			$this->campaign_campaignFilter->loadExcClub_campaignFilter();
		}
		if (!is_object($this->campaign_campaignFilter->excCampaign_campaignFilter)){
			$this->campaign_campaignFilter->loadExcCampaign_campaignFilter();
		}
		if (!is_object($this->campaign_campaignFilter->city_campaignFilter)){
			$this->campaign_campaignFilter->loadCity_campaignFilter();
		}

		return Publisher::filterPublishers(
			$onlyStats,
			$this->race,
			explode(",",$this->campaign_campaignFilter->age),
			$this->campaign_campaignFilter->sex,
			$this->campaign_campaignFilter->trends_campaignFilter->_ids,
			$celebrityOnly,
			$this->campaign_campaignFilter->incClub_campaignFilter->_ids,
			$this->campaign_campaignFilter->excClub_campaignFilter->_ids,
			$spesificPublisherId,
			$this->campaign_campaignFilter->excCampaign_campaignFilter->_ids,
			$this->campaign_campaignFilter->city_campaignFilter->_ids
		);
	}

	/**
	 * Insert the publishers for campaign
	 *
	 * @param bool $controlBeforeAdd This parameter should be true if $spesificPublisherID is not null
	 * @param int $spesificPublisherId This parameter can only called by Publisher->addToActiveCampaigns method
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public function collectUsers($controlBeforeAdd=false,$spesificPublisherId=null){
		$publisher = $this->getUsers(false,false,$spesificPublisherId);
		$r = array(
			"count" => 0,
			"influence" => 0,
			"follower" => 0,
			"cost" => 0,
			"celebCost" => 0,
			"readyCount" => 0
		);
		if ($publisher->gotValue){
			//Tool::echoLog("Query count before do: " . JPDO::$queryCount);
			do {
				$cp = new CampaignPublisher();
				$addUser = true;
				if ($controlBeforeAdd){
					$cp->load(array(
						"campaign_campaignPublisher" => $this->id,
						"publisher_campaignPublisher" => $publisher->id
					));
					if ($cp->gotValue) {
						$addUser = false;
					} else {
						$cp = new CampaignPublisher();
					}
				}
				if ($addUser){
					$cp->campaign_campaignPublisher = $this->id;
					$cp->publisher_campaignPublisher = $publisher->id;
					$cp->emailed = 0;
					$cp->msgcnt = 1;
					$cp->isReserved = 0;
					$cp->hasTwitter = $publisher->hasTwitter;
					$cp->hasFacebook = $publisher->hasFacebook;
					//if ($publisher->getLastTwitterHistoryID()){
					//	$cp->snTwitterHistory = $publisher->publisher_snTwitter->snTwitter_snTwitterHistory->id;
					//}
					if ($this->race == Campaign::ENUM_RACE_TWITTER){
					$th = $publisher->getLatestTwitterData();
					if (!is_null($th)){
						$cp->snTwitterHistory = $th["lastID"];
					}
					}
					//Tool::echoLog("th for #{$publisher->id}: " . print_r($th,true));
					switch ($this->race){
						case Campaign::ENUM_RACE_CPL:
						case Campaign::ENUM_RACE_CPL_REMOTE:
						case Campaign::ENUM_RACE_CPA_AFF:
						case Campaign::ENUM_RACE_CPC:
							$cp->cost = 0;
							break;
						case Campaign::ENUM_RACE_TWITTER:
							/*
							$cp->cost = $publisher->getLastCost();
							if (!is_null($cp->snTwitterHistory)){
								$r["influence"] += $publisher->publisher_snTwitter->snTwitter_snTwitterHistory->trueReach;
								$r["follower"] += $publisher->publisher_snTwitter->snTwitter_snTwitterHistory->follower;
								if ($publisher->type == Publisher::ENUM_TYPE_CELEBRITY)
									$r["celebCost"] += $publisher->getLastCost();
								else
									$r["cost"] += $publisher->getLastCost();
							}
							*/
							if (!is_null($th)){
								$cp->cost = $th["cost"];
								$r["influence"] += $th["influence"];
								$r["follower"] += $th["follower"];
								if ($publisher->type == Publisher::ENUM_TYPE_CELEBRITY)
									$r["celebCost"] += $th["cost"];
								else
									$r["cost"] += $th["cost"];
							}
							break;
						default:
							$cp->cost = 0;
							break;
					}
					if (!is_null($this->activationDate)){
						$cp->availableDate = $this->activationDate;
					}
					if (!is_null($this->activationDate) && $this->activationDate <= time()){
						$cp->state = CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION;
						$r["readyCount"]++;
					} else {
						$cp->state = CampaignPublisher::ENUM_STATE_IDLE;
					}
					$cp->save();
					$r["count"]++;
				}
				unset($cp);
				//if ($r["count"] == 100) break;
			} while ($publisher->populate());
		}
		return $r;
	}

	/**
	 * Create campaign link collections on short link service
	 *
	 * @return bool
	 * @author Murat
	 */
	public function createSLCollections(){
		$result = false;

		if ($this->checkForSLS()){
			if (!is_object($this->brand)){
				$this->loadBrand();
			}
			if ($this->race == Campaign::ENUM_RACE_CPC && !is_object($this->campaign_campaignCPC)){
				$this->loadCampaign_campaignCPC();
			}

			$slsParams = array(
				"params"=>  json_encode(array(
					"url"=>Format::parametizer($this->campaign_campaignClick->link),
					"campId"=>$this->id,
					"active"=>"1",
					"startDate"=> strtotime($this->startDate.' 00:00:00'),
					"endDate"=>strtotime($this->endDate.' 23:59:59'),
					"country"=>Setting::getValue("SL_COUNTRY_CODE"),
					"maxCount"=>($this->race == Campaign::ENUM_RACE_CPC ? $this->campaign_campaignCPC->maxClick : 0),
					"baseDomain"=>false,
					"doFilter"=>($this->race == Campaign::ENUM_RACE_CPC ? true : false),
					"env"=>ENVIRONMENT_CODE,
					"urlBase"=>$this->getShortLinkDomain(), //Setting::getValue("SHORTDOMAIN"),
					"urlPush"=>Setting::getValue("SL_PUSH_ADDRESS"),
					"urlEnd"=>Setting::getValue("SL_CAMPAIGN_END"),
					"title"=>Format::parametizer($this->title),
					"description"=>Format::parametizer($this->campaign_campaignClick->campDetail),
					"screen_name"=>Setting::getValue("TWITTER_USER"),
					"brand"=>Format::parametizer($this->brand->name),
					"image"=>JUtil::siteAddress() . "site/layout/images/campaign/" . $this->campaign_campaignClick->previewFileName,
					"maxAnonRefCount"=>($this->race == Campaign::ENUM_RACE_CPC ? $this->campaign_campaignCPC->maxAnonRefCount : 0),
					"keepRedirecting"=>($this->race == Campaign::ENUM_RACE_CPC ? $this->campaign_campaignCPC->keepRedirecting : 0),
					"hashCode"=>$this->campaign_campaignClick->hashCode
				))
			);

			JLog::log("slsAPI","Creating Campaign to SLS".print_r($slsParams,true));
			$response = Tool::callSLService("createCampaign",$slsParams);

			if (isset($response->code)){
				$this->campaign_campaignClick->linkHash = $response->code;
				$this->campaign_campaignClick->save();
				$result = true;
			}else{
				JLog::log("slsAPI","Campaign create error ".print_r($response,true));
				$result = false;
			}
			unset($response);
		} else {
			$result = true;
		}

		return $result;
	}

	/**
	 * Get campaign races
	 *
	 * @return array
	 * @author Murat
	 */
	public static function getCampaignRaces(){
		return array(
			Campaign::ENUM_RACE_TWITTER => "Effect",
			Campaign::ENUM_RACE_CPC => "Click",
			Campaign::ENUM_RACE_CPL => "CPL",
			Campaign::ENUM_RACE_CPL_REMOTE => "CPL.Affiliate",
			Campaign::ENUM_RACE_CPA_AFF => "CPA.Affiliate"
		);
	}

	/**
	 * Get effect campaign types
	 *
	 * @return array
	 * @author Murat
	 */
	public static function getEffectCampaignTypes(){
		return array(
			CampaignTwitter::ENUM_CAMPTYPE_BOTH => "Both",
			CampaignTwitter::ENUM_CAMPTYPE_STANDART => "Standart",
			CampaignTwitter::ENUM_CAMPTYPE_CELEBRITY => "Celebrity"
		);
	}

	/**
	 * Get genders
	 *
	 * @return array
	 * @author Murat
	 */
	public static function getGenders(){
		return ValidateCampaign::$validGenders;
	}

	/**
	 * Get age ranges
	 *
	 * @return array
	 * @author Murat
	 */
	public static function getAgeRanges(){
		return ValidateCampaign::$validAgeRanges;
	}

	/**
	 * Load campaign with all details
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function loadWithAllDetail($id){
		$c = new Campaign();

		if(is_numeric($id))
			$c->load( array( "id" => $id ) );
		elseif(!is_null($id))
			$c->load ( array( "publicID" => $id ) );
		else
			$c = new Campaign();

		$c->gotClick = false;

		if ($c->gotValue){
			$c->loadAdvertiser();
			$c->loadBrand();
			$c->loadCampaign_campaignFilter();
			$c->loadCampaignClick();
			$c->loadCampaign_campaignMessage();
			$c->campaign_campaignFilter->loadTrends_campaignFilter();
			$c->campaign_campaignFilter->loadIncClub_campaignFilter();
			$c->campaign_campaignFilter->loadExcClub_campaignFilter();
			$c->campaign_campaignFilter->loadExcCampaign_campaignFilter();
			$c->campaign_campaignFilter->loadCity_campaignFilter(null,0,1000,null,false);
			switch ($c->race){
				case Campaign::ENUM_RACE_TWITTER:
					$c->loadCampaign_campaignTwitter();
					break;
				case Campaign::ENUM_RACE_CPC:
					$c->gotClick = true;
					$c->loadCampaign_campaignCPC();
					break;
				case Campaign::ENUM_RACE_CPL:
					$c->gotClick = true;
					$c->loadCplCampaign();
					break;
				case Campaign::ENUM_RACE_CPL_REMOTE:
					$c->gotClick = true;
					$c->loadCplRemoteCampaign();
					break;
				case Campaign::ENUM_RACE_CPA_AFF:
					$c->gotClick = true;
					$c->loadCpaAffCampaign();
					$c->campaign_cpaAff->loadCpaAffService();
					break;
			}
		}
		return $c;
	}

	/**
	 * Change campaign state
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function changeState($params){
		$c = new Campaign($params["id"]);

		if ($c->gotValue){
			$oState = $c->state;

			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_CAMPAIGN_STATE_CHANGE,
				"Campaign change request from " . $c->state_options[$oState] . " to " . $c->state_options[$params["state"]],
				CampaignHistory::ENUM_MADEBY_ADMIN,
				array("admin"=>isset($_SESSION["DS_userId"])?$_SESSION["DS_userId"]:null)
			);

			if (($params["state"] == Campaign::ENUM_STATE_ACCEPTED || $params["state"] == Campaign::ENUM_STATE_DECLINED) && $c->state == Campaign::ENUM_STATE_INITIAL){
				$c->state = $params["state"];
				if ($params["state"] == Campaign::ENUM_STATE_DECLINED){
					$c->denyReason = $params["denyReason"];
				}
				$c->save();
				return $c;
			} elseif ($params["state"] == Campaign::ENUM_STATE_INITIAL && ($c->state == Campaign::ENUM_STATE_DECLINED || $c->state == Campaign::ENUM_STATE_OFFER_DECLINED)){
				$c->state = Campaign::ENUM_STATE_INITIAL;
				$c->save();
				return $c;
			} elseif ($params["state"] == Campaign::ENUM_STATE_OFFER_WAITING && $c->state == Campaign::ENUM_STATE_ACCEPTED){
				$c->state = Campaign::ENUM_STATE_OFFER_WAITING;
				$c->extraBudget = (!empty($params["extraBudget"]) && is_float($params["extraBudget"]) ? $params["extraBudget"] : 0);
				//todo:@Murat:send a mail to advertiser
				$c->save();
				return $c;
			} elseif (($params["state"] == Campaign::ENUM_STATE_OFFER_APPROVED || $params["state"] == Campaign::ENUM_STATE_OFFER_DECLINED) && $c->state == Campaign::ENUM_STATE_OFFER_WAITING){
				$c->state = $params["state"];
				$c->save();
				return $c;
			}
		}
		unset($c);
		return false;
	}

	/**
	 * Calculate the campaign total cost
	 *
	 * @return float
	 * @author Murat
	 */
	public function calculateTotalCost($withCommission=true){
		return $this->budget + $this->extraBudget + ($withCommission ? $this->commission : 0);
	}

	/**
	 * Publish campaign
	 *
	 * @return Campaign
	 * @author Murat
	 */
	public static function publishToPublishers($params){
		$c = Campaign::loadWithAllDetail($params["id"]);

		JPDO::beginTransaction();

		$result = $c->createSLCollections();

		if ($result){
			$c->state = Campaign::ENUM_STATE_PENDING_PROGRESS;
			$c->activationDate = strtotime($params["publishDate"] . " " . $params["publishTime"]);
			$c->save();

			//Block Money
			AdvertiserMoneyAction::create(
				$c->advertiser,
				$c->id,
				($c->calculateTotalCost() * (-1)),
				"Campaign Publish: " . $c->title
			);

			//Create campaign budget table
			$c->conn();

			//check if table exists
			$sql = "";
			$table = $c->campCode . "_budget";
			if (DB_ENGINE == "MYSQL"){
				$sql = "CREATE TABLE IF NOT EXISTS `{$table}` (
					 `id` int(11) NOT NULL AUTO_INCREMENT,
					`jdate` INT NULL ,
					`user` INT NULL ,
					`info` VARCHAR( 200 ) NULL,
					`actionTime` int(11) NULL,
					`budgetBefore` float NULL,
					`cost` float NOT NULL,
					`budgetLeft` float NULL,
					PRIMARY KEY (  `id` ) ,
					INDEX (  `actionTime`,`user` )
					) ENGINE = INNODB;
					";
			} elseif (DB_ENGINE == "PGSQL"){
				$dbConfig = JCache::read( 'JDSN.CAMP0' );
				$table = '"' . $dbConfig["schema"] . '"."' . $table;
				$sql = '
					CREATE TABLE IF NOT EXISTS ' . $table . '" (
						"id" Serial NOT NULL,
						"jdate" NUMERIC,
						"user" NUMERIC,
						"info" CHARACTER VARYING( 400 ) COLLATE "' . $dbConfig["schema"] . '"."' . JPDO::getLocaleString() . '",
						"actionTime" NUMERIC,
						"budgetBefore" NUMERIC,
						"cost" NUMERIC,
						"budgetLeft" NUMERIC,
					 PRIMARY KEY ( "id" )
					 );
				';
			}

			JPDO::executeQuery($sql);


			//Initialize the budget
			$c->budgetAction($c->calculateTotalCost(false), null, "initial");
			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_BUDGET_INITIALIZE,
				"Initial budget(" . $c->budget . ") entered",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);

			//Reserve budget for reserved users
			if ($c->race == Campaign::ENUM_RACE_TWITTER){

				$cp = new CampaignPublisher();
				$cp->with("publisher_campaignPublisher");
				$cp->populateOnce(false)->load(array("campaign_campaignPublisher"=>$c->id,"isReserved"=>1));

				if($cp->gotValue){
					do{
						$cp->publisher_campaignPublisher->loadUser();
						$c->budgetAction($cp->cost*(-1)*$c->multiplier, $cp->publisher_campaignPublisher->id, "User ({$cp->publisher_campaignPublisher->user_publisher->username}) Added");
					} while($cp->populate());
				}
			}

			//Create a budget check for effect campaigns
			if ($c->race == Campaign::ENUM_RACE_TWITTER){
				$c->setCronForBudget(strtotime($c->activationDate) + 60);
			}

			//register cronjob for campaign start date
			Cron::create(
				Cron::ENUM_TYPE_CAMPAIGN_START,
				strtotime($c->startDate.' 00:00:00'),
				null,
				$c->id
			);

			//register cronjob for campaign end date
			Cron::create(
				Cron::ENUM_TYPE_CAMPAIGN_END,
				strtotime($c->endDate.' 23:59:59'),
				null,
				$c->id
			);

			$normalActivationDate = $c->activationDate + (60*60*Setting::getValue("publisherCampaignDisplayHourDifference"));
			//Publish job for normal users
			Cron::create(
				Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL,
				$normalActivationDate,
				null,
				$c->id
			);


			//Ask to normal publishers
			$cp = new CampaignPublisher();
			$cp->update(array("campaign_campaignPublisher"=>$c->id,"publisher_campaignPublisher"=>array("type"=>Publisher::ENUM_TYPE_REGULAR)),array("state"=>CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION,"availableDate"=>$normalActivationDate));

			//Publish job for trendsetters and celebrities
			Cron::create(
				Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_TRENDSETTER,
				$c->activationDate,
				null,
				$c->id
			);


			//Ask to trendsetters and celebrities
			$cp = new CampaignPublisher();
			$cp->update(array("campaign_campaignPublisher"=>$c->id,"publisher_campaignPublisher"=>array("type"=>Publisher::ENUM_TYPE_TRENDSETTER . " &or " . Publisher::ENUM_TYPE_CELEBRITY)),array("state"=>CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION,"availableDate"=>$c->activationDate));


			//Create history record for that action
			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_CAMPAIGN_PUBLISH,
				"[" . $c->title . "] campaign published for " . $params["publishDate"] . " " . $params["publishTime"],
				CampaignHistory::ENUM_MADEBY_ADMIN,
				array("admin"=>isset($_SESSION["DS_userId"])?$_SESSION["DS_userId"]:null)
			);

			unset($normalActivationDate);


			JPDO::commit();
			return $c;
		} else {
			JPDO::rollback();
			return false;
		}
	}

	/**
	 * set cron job for campaign budget finished
	 *
	 * @return void
	 * @author Sam
	 */
	public function setCronForBudget($targetDate=null) {
		global $debugLevel;

		$c = new Cron();
		$c->load(array(
			"type" => Cron::ENUM_TYPE_CAMPAIGN_BUDGET_CHECK,
			"campaign_cron" => $this->id
		));
		if ($c->gotValue){
			$c->targetDate = $targetDate;
			$c->state = Cron::ENUM_STATE_WAITING;
			$c->executeCount++;
			$c->save();
			if ($debugLevel > 0){
				Tool::echoLog("BudgetCheck cron job updated for campaign #{$this->id}");
			}
		} else { //If there's a job for budget check, update the target date again

			Cron::create(
				Cron::ENUM_TYPE_CAMPAIGN_BUDGET_CHECK,
				(is_null($targetDate) ? time() : $targetDate),
				null,
				$this->id
			);
			if ($debugLevel > 0){
				Tool::echoLog("BudgetCheck cron job created for campaign #{$this->id}");
			}
		}
		unset($c);
	}

	/**
	 * budget action
	 *
	 * @return void
	 * @author Murat
	 **/
	public function	budgetAction($cost, $publisherId, $info=null)
	{
		$this->conn();
		$latest = $this->getLatestBudget();
		$budgetLeft = $latest + $cost;

		//$sql = JPDO::executeQuery("INSERT INTO `".$this->campCode."_budget`(`user`,`info`,`cost`,`budgetLeft`,`budgetBefore`,`actionTime`) VALUES(?,?,?,?,?,?)",
		//	array(0,(is_null($publisherId)?0:$publisherId),$info,$cost,$budgetLeft,$latest,time()));

		$cb = new CampaignBudget();
		$cb->overrideTableName($this->campCode . "_budget");
		$cb->user = $publisherId;
		$cb->info = $info;
		$cb->budgetBefore = $latest;
		$cb->cost = $cost;
		$cb->budgetLeft = $budgetLeft;
		$cb->actionTime = time();
		$cb->save();
	}

	/**
	 * Update campaign SL data
	 *
	 * @return bool
	 * @author Murat
	 */
	public function updateSLData($startDate=null,$expireDate=null,$maxCount=null,$image=null){
		if (!is_object($this->campaign_campaignClick)){
			$this->loadCampaignClick();
		}
		if ($this->campaign_campaignClick->gotValue){
			$params = array("hash"=>$this->campaign_campaignClick->linkHash);
			if (!is_null($startDate)) $params["startDate"] = $startDate;
			if (!is_null($expireDate)) $params["expireDate"] = $expireDate;
			if (!is_null($maxCount)) $params["maxCount"] = $maxCount;
			if (!is_null($image)) $params["image"] = $image;
			//die(print_r($params,true));
			$response = Tool::callSLService("updateCampaign", array("params"=>  json_encode($params)));
			//die(print_r($response,true));
			if (is_object($response) && isset($response->update) && $response->update == "successful")
				return true;
			else
				return false;

		} else {
			return false;
		}

	}


	/**
	 * Sends campaign available emails to normal publishers
	 *
	 * @param int $campaignId
	 * @param bool $utype
	 *
	 * @return bool
	 *
	 * @author Murat
	 */
	public static function sendAvailableEmailsNew($campaignId,$utype) {
		global $debugLevel, $_S;
		//JPDO::beginTransaction();
		$filter = array(
			"emailed" => 0,
			"availableDate" => "&le " . (time() + 60),
			"campaign_campaignPublisher" => array(
				"id" => $campaignId,
				"_custom" => "
				`campaign_campaignPublisher`.`race` IN (" . Campaign::ENUM_RACE_CPC . "," . Campaign::ENUM_RACE_CPL . "," . Campaign::ENUM_RACE_CPL_REMOTE. "," . Campaign::ENUM_RACE_CPA_AFF . ")
							OR (
								`campaign_campaignPublisher`.`race` NOT IN
								(" . Campaign::ENUM_RACE_CPC . "," . Campaign::ENUM_RACE_CPL. "," . Campaign::ENUM_RACE_CPL_REMOTE. "," . Campaign::ENUM_RACE_CPA_AFF . ")
								AND `campaign_campaignPublisher`.`startDate` >= NOW()
							)"
			),
			"publisher_campaignPublisher" => array(
				"type" => ($utype == Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL ? Publisher::ENUM_TYPE_REGULAR : array(Publisher::ENUM_TYPE_TRENDSETTER,Publisher::ENUM_TYPE_CELEBRITY)),
				"user_publisher" => array(
					"status" => User::ENUM_STATUS_ACTIVE
				)
			)
		);


		$cp = new CampaignPublisher();
		$cp->with(
			array(
				"publisher_campaignPublisher",
				Publisher::NO_LEFT_JOIN,
				array(
					array(
						"user_publisher",
						User::NO_LEFT_JOIN,
						array(
							array(
								"lang",
								Lang::DO_LEFT_JOIN
							)
						)
					)
				)
			),
			array(
				"campaign_campaignPublisher",
				Campaign::NO_LEFT_JOIN
			)
		);
		$cp->limitCount = 1000;
		$cp->count( "id", "cid", $filter );
		$count = $cp->cid;

		if($count > 0){
			$per = 500;
			$cid = 0;
			$label = "sendEmail_".$campaignId;
			$job = "sendAvailableEmails";
			$b = Backend::createSupervisionTask( $label, $job );

			while($cid<$count){
				$cp = new CampaignPublisher();
				$cp->limit( $cid, 1 )->load( $filter );
				if($cp->gotValue !== true)
					break;

				$b->addChildTask(
					$label.time(),
					$job,
					array( "startId" => $cp->id, "utype" => $utype, "campaignId" => $campaignId, "count"=>$per )
				);


				$cid += $per;
			}
			$b->runTask();
		}
		else
			Tool::echoLog(' (' . $campaignId . ') no publisher found for that campaign');

		//JPDO::commit();
		return true;

	}



	/**
	 * Sends campaign available emails to normal publishers
	 *
	 * @param int $campaignId
	 * @param bool $utype
	 *
	 * @return bool
	 *
	 * @author Murat
	 */
	public static function sendAvailableEmails($campaignId,$utype) {
		global $debugLevel, $_S;

		//JPDO::beginTransaction();
		try{
			$filter = array(
				"emailed" => 0,
				"availableDate" => "&le " . (time() + 60),
				"campaign_campaignPublisher" => array(
					"id" => $campaignId,
					"_custom" => "`campaign_campaignPublisher`.`race` <> " . Campaign::ENUM_RACE_TWITTER . " OR
					(`campaign_campaignPublisher`.`race` = " . Campaign::ENUM_RACE_TWITTER . " AND `campaign_campaignPublisher`.`startDate` >= NOW())"
				),
				"publisher_campaignPublisher" => array(
					"type" => ($utype == Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL ? Publisher::ENUM_TYPE_REGULAR : array(Publisher::ENUM_TYPE_TRENDSETTER,Publisher::ENUM_TYPE_CELEBRITY)),
					"user_publisher" => array(
						"status" => User::ENUM_STATUS_ACTIVE
					)
				)
			);
			$cp = new CampaignPublisher();
			//JPDO::debugQuery(true);
			//$cp->with("publisher_campaignPublisher","campaign_campaignPublisher");
			$cp->with(
				array(
					"publisher_campaignPublisher",
					Publisher::NO_LEFT_JOIN,
					array(
						array(
							"user_publisher",
							User::NO_LEFT_JOIN,
							array(
								array(
									"lang",
									Lang::DO_LEFT_JOIN
								)
							)
						)
					)
				),
				array(
					"campaign_campaignPublisher",
					Campaign::NO_LEFT_JOIN
				)
			);
			$cp->limitCount = 1000;
			$cp->populateOnce(false)->load($filter);
			$c = null;
			if ($cp->gotValue){
				//print_r($cp);
				//exit;
				//echo "dump : ";
				//var_dump($cp->publisher_campaignPublisher->user_publisher);
				//echo PHP_EOL;
				$time = time();
				$c = $cp->campaign_campaignPublisher;
				$ids = array();
				$cnt = 0;
				$cron = new Cron();
				$previewImage = $c->getPreviewImageLink();
				do {
					$cnt++;
					//$cp->publisher_campaignPublisher->loadUser();
					$cp->publisher_campaignPublisher->user_publisher->user_publisher = $cp->publisher_campaignPublisher; // to avoid load publisher under user object
					$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
						'V2_campaignAvailable',
						array("campName" => $cp->campaign_campaignPublisher->title),
						array(
							"name" => $cp->publisher_campaignPublisher->getFullName(),
							"campName" => $cp->campaign_campaignPublisher->title,
							"publishDate" => Format::getDateTimeFormated($time) ,//date('d/m/Y (H:i)', $time),
							"campDetail" => $_S."myCampDetail/".$cp->campaign_campaignPublisher->publicID
						),
						$cp->campaign_campaignPublisher->id,
						null,
						null,
						$previewImage
					);
					/*
					$cp->emailed = 1;
					$cp->save();
					*/
					$ids[] = $cp->id;

					if($debugLevel > 0) {
						Tool::echoLog($cnt . "# - " . $cp->publisher_campaignPublisher->user_publisher->username . ' (' . $cp->campaign_campaignPublisher->title . ') email scheduled to send');
					}
				} while($cp->populate());
				$cp = new CampaignPublisher();
				$cp->update(array("id"=>$ids),array("emailed"=>1));
				//Mail::sendToRabbitMQ(true);

				$c->writeHistory(
					CampaignHistory::ENUM_ACTIONID_PUBLISH_MAIL_SENT,
					"Campaign publish mails sent to " . (!empty($cron->type_options[$utype]) ? $cron->type_options[$utype] : $utype),
					CampaignHistory::ENUM_MADEBY_SYSTEM,
					null
				);
				if($debugLevel > 0) {
					Tool::echoLog(' (' . $c->title . ') publish mail(s) scheduled to send');
				}
			} else {
				if($debugLevel > 0) {
					Tool::echoLog(' (' . $campaignId . ') no publisher found for that campaign');
				}
			}
			//JPDO::commit();
			return true;
		}catch(JError $ex){
			JLog::log("cri","Error on sendAvailableEmails : " . print_r($ex,true));
			//JPDO::rollback();
			return false;
		}
	}

	/**
	 * Start campaign
	 *
	 * @param int $campId
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function startCampaign($campId){
		$emailTemplates = array(
			Campaign::ENUM_RACE_CPC        => "V2_cpcCampaignStarted",
			Campaign::ENUM_RACE_CPL        => "V2_cplCampaignStarted",
			Campaign::ENUM_RACE_CPL_REMOTE => "V2_cplCampaignStarted",
			Campaign::ENUM_RACE_CPA_AFF    => "V2_cpaAffCampaignStarted",
			Campaign::ENUM_RACE_TWITTER    => "V2_campaignStarted",
		);
		JPDO::beginTransaction();

		$c = new Campaign($campId);
		$c->state = Campaign::ENUM_STATE_PROGRESSING;
		$c->save();
		$c->loadAdvertiser()->loadUser();
		$dateFormat = Setting::getValue("dateFormat");

		$c->advertiser->user_advertiser->sendSysMessage(
			$emailTemplates[$c->race],
			array("campName" => $c->title),
			array(
				"name"      => $c->advertiser->companyName,
				"campName"  => $c->title,
				"startDate" => date( $dateFormat, strtotime( $c->startDate ) ),
				"endDate"   => date( $dateFormat, strtotime( $c->endDate ) )
			),
			$c->id,
			null,
			null,
			$c->getPreviewImageLink()
		);

		$c->writeHistory(
			CampaignHistory::ENUM_ACTIONID_CAMPAIGN_STARTED,
			"Campaign started.",
			CampaignHistory::ENUM_MADEBY_SYSTEM,
			null
		);

		if ($c->race == Campaign::ENUM_RACE_TWITTER){
			//Mark the asked publishers as late
			$cp = new CampaignPublisher();
			$cp->update(array(
				"campaign_campaignPublisher"=>$c->id,
				"state"=>CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION
			),array(
				"state"=>CampaignPublisher::ENUM_STATE_LATE_TO_JOIN
			));


			$amount = $c->getLatestBudget();

			AdvertiserMoneyAction::create(
				$c->advertiser,
				$c,
				$amount,
				'campaign unattended budget - '.$c->id.' - '.$c->title,
				null
			);

			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_UNATTENDED_PUBLISHER_BUDGET_REFUND,
				"Not attended budget (" . Format::getFormattedCurrency($amount) . ") refunded",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);
		}
		elseif ($c->race == Campaign::ENUM_RACE_CPL){

			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_CAMPAIGN_STARTED,
				"CPL Campaign started",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);

			$cpl = $c->loadCplCampaign();
			$cpl->campaignStart();
		}
		elseif ($c->race == Campaign::ENUM_RACE_CPL_REMOTE){

			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_CAMPAIGN_STARTED,
				"CPL.REMOTE Campaign started",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);

			$cpl = $c->loadCplRemoteCampaign();
			$cpl->campaignStart();
		}
		elseif ($c->race == Campaign::ENUM_RACE_CPA_AFF){

			$c->writeHistory(
				CampaignHistory::ENUM_ACTIONID_CAMPAIGN_STARTED,
				"CPA.AFF Campaign started",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);

			$cpa = $c->loadCpaAffCampaign();
			$cpa->campaignStart();
		}

		JPDO::commit();

		DSNotification::addNotification( $c, "Campaign Started", $c->title, DSNotification::ENUM_TYPE_GOOD );
	}

	/**
	 * End campaign
	 *
	 * @param int $campId
	 *
	 * @return bool
	 *
	 * @author Murat
	 */
	public static function endCampaign($campId){
		global $_S, $debugLevel;

		$campaign = new Campaign($campId);
		$campaign->loadAdvertiser()->loadUser();

		JPDO::beginTransaction();

		try {

			$campaign->realEndTime = time();
			$campaign->state = Campaign::ENUM_STATE_FINISHED;
			$campaign->save();

			$campaign->writeHistory(
				CampaignHistory::ENUM_ACTIONID_CAMPAIGN_ENDED,
				"Campaign ended",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);

			$campaign->loadCampaignClick();

			if (is_object($campaign->campaign_campaignClick) && $campaign->campaign_campaignClick->gotValue){
				$data = $campaign->campaign_campaignClick->getCampaignLinkStats();
				$campaign->campaign_campaignClick->totalClickSnapshot = $data["totalClickSum"];
				if (isset($data["suspectedClickSum"])) $campaign->campaign_campaignClick->suspectedClickSnapshot = $data["suspectedClickSum"];
				$campaign->campaign_campaignClick->save();
			}

			$campaign->loadPublishersClickStats(true,true);

			$dateFormat = Setting::getValue("dateFormat");
			if ($campaign->race == Campaign::ENUM_RACE_CPC){
				$campaign->advertiser->user_advertiser->sendSysMessage(
					'V2_cpcCampaignEnded',
					array("campName"=>$campaign->title),
					array(
						"name"=>$campaign->advertiser->companyName,
						"campName"=>$campaign->title,
						"endDate"=>date($dateFormat),
						"totalClick"=>$campaign->campaign_campaignClick->totalClick,
						"reportDetail"=>$_S."myReportDetail/".$campaign->id
					),
					$campaign->id,
					null,
					null,
					$campaign->getPreviewImageLink()
				);
			}
			elseif ($campaign->race == Campaign::ENUM_RACE_CPL){
				//ending must be checked before here.
				$cpl = $campaign->loadCplCampaign();

				if ( is_object( $cpl ) && $cpl->gotValue ) {
					$leads = $cpl->getLeadCounts();

					//end cpl campaign
					$campaign->campaign_cplCampaign->campaignComplete( $leads );

					$campaign->loadAdvertiser()->loadUser()->sendSysMessage(
						'V2_cplCampaignEnded',
						array( "campName" => $campaign->title ),
						array(
							"name"         => $campaign->advertiser->companyName,
							"campName"     => $campaign->title,
							"endDate"      => date( $dateFormat ),
							"totalLead"    => $leads[ "valid" ],
							"reportDetail" => $_S . "myReportDetail/" . $campaign->id
						),
						$campaign->id,
						null,
						null,
						$campaign->getPreviewImageLink()
					);
				}
			}
			elseif ($campaign->race == Campaign::ENUM_RACE_CPL_REMOTE){
				//ending must be checked before here.
				$cpl = $campaign->loadCplRemoteCampaign();

				if ( is_object( $cpl ) && $cpl->gotValue ) {
					$leads = $cpl->getActiveLeadCount();

					$campaign->loadAdvertiser()->loadUser()->sendSysMessage(
						'V2_cplCampaignEnded',
						array( "campName" => $campaign->title ),
						array(
							"name"         => $campaign->advertiser->companyName,
							"campName"     => $campaign->title,
							"endDate"      => date( $dateFormat ),
							"totalLead"    => $leads,
							"reportDetail" => $_S . "myReportDetail/" . $campaign->id
						),
						$campaign->id,
						null,
						null,
						$campaign->getPreviewImageLink()
					);
				}
			}
			elseif ($campaign->race == Campaign::ENUM_RACE_CPA_AFF){
				//ending must be checked before here.
				$cpa = $campaign->loadCpaAffCampaign();

				if ( is_object( $cpa ) && $cpa->gotValue ) {
					$campaign->loadAdvertiser()->loadUser()->sendSysMessage(
						'V2_cpaAffCampaignEnded',
						array( "campName" => $campaign->title ),
						array(
							"name"         => $campaign->advertiser->companyName,
							"campName"     => $campaign->title,
							"endDate"      => date( $dateFormat ),
							"reportDetail" => $_S . "myReportDetail/" . $campaign->id
						),
						$campaign->id,
						null,
						null,
						$campaign->getPreviewImageLink()
					);
				}
			}
			else {
				$campaign->loadCampaign_campaignTwitter();
				$summary = $campaign->campaign_campaignTwitter->getSummaryValues();
				$campaign->advertiser->user_advertiser->sendSysMessage(
					'V2_campaignEnded',
					array("campName"=>$campaign->title),
					array(
						"name"=>$campaign->advertiser->companyName,
						"campName"=>$campaign->title,
						"endDate"=>date($dateFormat),
						"totalTrueReach"=>$summary["influence"],
						"reportDetail"=>$_S."myReportDetail/".$campaign->id
					),
					$campaign->id,
					null,
					null,
					$campaign->getPreviewImageLink()
				);
			}

			if ($campaign->race == Campaign::ENUM_RACE_CPC){
				Cron::create(
					Cron::ENUM_TYPE_LAST_FRAUD_CHECK,
					time() + (72 * 60 * 60), //Todo: Change it To variable
					null,
					$campaign->id
				);
			}
			if ($campaign->race == Campaign::ENUM_RACE_CPL_REMOTE){
				Cron::create(
					Cron::ENUM_TYPE_CPL_REMOTE_FINISH,
					time() + (24 * 60 * 60 * 30), //Todo: Change it To variable
					null,
					$campaign->id
				);
			}
			if ($campaign->race == Campaign::ENUM_RACE_CPA_AFF){
				Cron::create(
					Cron::ENUM_TYPE_CPA_AFF_FINISH,
					time() + (24 * 60 * 60 * 30), //Todo: Change it To variable
					null,
					$campaign->id
				);
			}

			$cp = new CampaignPublisher();
			$cp->update(
				array(
					"campaign_campaignPublisher"=>$campaign->id,
					"state"=>CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION
				),
				array(
					"state"=>CampaignPublisher::ENUM_STATE_LATE_TO_JOIN
				)
			);
			$cron = new Cron();
			$cron->update(
				array(
					"campaign_cron"=>$campaign->id,
					"state"=>Cron::ENUM_STATE_WAITING,
					"type"=>array(Cron::ENUM_TYPE_TWEET_SEND,Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL,Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_TRENDSETTER)
				),
				array(
					"state"=>Cron::ENUM_STATE_CANCELLED
				)
			);
			DSNotification::addNotification( $campaign, "Campaign Ended", $campaign->title, DSNotification::ENUM_TYPE_GOOD );

			unset($cp,$cron);

			JPDO::commit();
			if ($debugLevel){
				Tool::echoLog("Campaign ended successfully");
			}
			return true;
		} catch(Exception $e){
			JPDO::rollback();
			JLog::log("cri", "Campaign end failed. Error: " . print_r($e,true));

			DSNotification::addNotification( $campaign, "Campaign End Problem", $campaign->title, DSNotification::ENUM_TYPE_ERROR );

			unset($campaign);
			return false;
		}
	}

	/**
	 * end cpl remote campaign
	 *
	 * @param $campaignId
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function endCplRemoteCampaign($campaignId) {
		$c = new Campaign($campaignId);
		if($c->gotValue && $c->race == Campaign::ENUM_RACE_CPL_REMOTE){
			$leads = $c->loadCplRemoteCampaign()->getActiveLeadCount();

			//end cpl campaign
			$c->campaign_cplRemoteCampaign->campaignComplete( $leads );

			return true;
		}

		return false;
	}

	/**
	 * TODO: anything?
	 *
	 * @param $campaignId
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function endCpaAffCampaign($campaignId) {
		return true;
	}

	/**
	 * Get all attended users click stats for cpc campaign
	 *
	 * @return void
	 * @author Murat
	 */
	public function loadPublishersClickStats($async=true,$campEnd=false){
		global $debugLevel;
		if ($this->checkForSLS()){
			$cnt = 0;
			$this->loadCampaign_campaignPublisher(array(
				"campaign_campaignPublisher" => $this->id,
				"state" => CampaignPublisher::ENUM_STATE_APPROVED
			),0,1000,null,false);
			if ($this->campaign_campaignPublisher->gotValue){
				do {
					if (!$async){
						$this->campaign_campaignPublisher->getLinkStats(false);
					} else {
						$this->campaign_campaignPublisher->getClickStatsAsync($campEnd);
						$cnt++;
					}
					if ($debugLevel){
						Tool::echoLog("cp#{$this->campaign_campaignPublisher->id} queued for get stats");
					}
					if ($async && $cnt){
						Cron::forcedAddToRabbitMQ("gpcs");
					}
				} while ($this->campaign_campaignPublisher->populate());
			}
		}
	}

	/**
	 * Send campaign ending alert to publishers
	 *
	 * @return bool
	 * @author Murat
	 */
	public static function sendFinishingMail($campaignId){
		global $debugLevel, $_S;

		JPDO::beginTransaction();
		try{
			$campaign = new Campaign( $campaignId );
			if($campaign->race == Campaign::ENUM_RACE_CPC){
				$ccm = new CampaignCPCMessage();
				$ccm->with("publisher_campaignCPCMessage","campaign_campaignCPCMessage");
				$ccm->populateOnce(false)->load(array(
					"campaign_campaignCPCMessage"=>$campaignId,
					"finishingWarningSent" => 0
				));
				if ($ccm->gotValue){
					$ids = array();
					do {
						$ccm->publisher_campaignCPCMessage->loadUser();
						$ccm->publisher_campaignCPCMessage->user_publisher->sendSysMessage(
							'V2_campaignFinishing',
							array("campName"=>$ccm->campaign_campaignCPCMessage->title),
							array(
								"name" => $ccm->publisher_campaignCPCMessage->getFullName(),
								"campName" => $ccm->campaign_campaignCPCMessage->title,
								"campDetail" => $_S."myCampDetail/".$ccm->campaign_campaignCPCMessage->publicID
							),
							$campaignId,
							null,
							null,
							$ccm->campaign_campaignCPCMessage->getPreviewImageLink()
						);
						//$ccm->finishingWarningSent = 1;
						//$ccm->save();
						$ids[] = $ccm->id;

						if($debugLevel > 0) {
							JUtil::echoLog("CPC finishing mail sent to " . $ccm->publisher_campaignCPCMessage->user_publisher->username . " for '" . $ccm->campaign_campaignCPCMessage->title . "' campaign.");
						}
					} while($ccm->populate());
					$ccm = new CampaignCPCMessage();
					$ccm->update(array("id"=>$ids),array("finishingWarningSent"=>1));
				}
			}
			elseif($campaign->race == Campaign::ENUM_RACE_CPL){
				$ccm = new CplCampaignUser();
				$ccm->with(
					array(
						"cplCampaignUser_publisher", CplCampaignUser::DO_LEFT_JOIN, array(
							array(
								"user_publisher", User::NO_LEFT_JOIN
							)
						)
					),
					array(
						"cplCampaign_cplCampaignUser", CplCampaignUser::DO_LEFT_JOIN, array(
							array(
								"campaign_cplCampaign", CplCampaign::DO_LEFT_JOIN
							)
						)
					)
				);
				$ccm->populateOnce( false )->load( array(
					"cplCampaign_cplCampaignUser"=>array(
						"campaign_cplCampaign"=>$campaignId
					)
				) );

				$ids = array();
				$previewImage = $ccm->cplCampaign_cplCampaignUser->campaign_cplCampaign->getPreviewImageLink();
				do {
					$revenue = $ccm->cplCampaignUser_publisher->loadPublisher_publisherRevenue( array("campaign_publisherRevenue"=>$ccm->cplCampaign_cplCampaignUser->campaign_cplCampaign) );

					$ccm->cplCampaignUser_publisher->user_publisher->sendSysMessage(
						'V2_pubCPLCampaignEnd',
						array("campName"=>$ccm->cplCampaign_cplCampaignUser->campaign_cplCampaign->title),
						array(
							"name" => $ccm->cplCampaignUser_publisher->getFullName(),
							"endDate"=>Format::getActualDateTime(),
							"campName" => $ccm->cplCampaign_cplCampaignUser->campaign_cplCampaign->title,
							"revenue"=>$revenue->amount
						),
						$campaignId,
						null,
						$previewImage
					);
					//$ccm->finishingWarningSent = 1;
					//$ccm->save();
					$ids[] = $ccm->id;

					if($debugLevel > 0) {
						JUtil::echoLog("CPL finishing mail sent to " . $ccm->cplCampaignUser_publisher->user_publisher->username . " for '" . $ccm->cplCampaign_cplCampaignUser->campaign_cplCampaign->title . "' campaign.");
					}
				} while($ccm->populate());
				$ccm = new CplCampaignUser();
				$ccm->update(array("id"=>$ids),array("finishingWarningSent"=>1));


			}
			elseif( $campaign->race == Campaign::ENUM_RACE_CPL_REMOTE ){
				$ccm = new CplRemoteCampaignUser();
				$ccm->with(
					array(
						"cplRemoteCampaignUser_publisher", CplCampaignUser::DO_LEFT_JOIN, array(
							array(
								"user_publisher", User::NO_LEFT_JOIN
							)
						)
					),
					array(
						"cplRemoteCampaign_cplRemoteCampaignUser", CplCampaignUser::DO_LEFT_JOIN, array(
							array(
								"campaign_cplRemoteCampaign", CplCampaign::DO_LEFT_JOIN
							)
						)
					)
				);
				$ccm->populateOnce( false )->load( array(
					"cplRemoteCampaign_cplRemoteCampaignUser"=>array(
						"campaign_cplRemoteCampaign"=>$campaignId
					)
				) );

				$ids = array();
				$previewImage = $ccm->cplRemoteCampaign_cplRemoteCampaignUser->campaign_cplRemoteCampaign->getPreviewImageLink();
				do {
					$revenue = $ccm->cplRemoteCampaignUser_publisher->loadPublisher_publisherRevenue(
						array(
							"campaign_publisherRevenue" => $ccm->cplRemoteCampaign_cplRemoteCampaignUser->campaign_cplRemoteCampaign
						)
					);
					$ccm->cplRemoteCampaignUser_publisher->user_publisher->sendSysMessage(
						'V2_pubCPLCampaignEnd',
						array("campName"=>$ccm->cplRemoteCampaign_cplRemoteCampaignUser->campaign_cplRemoteCampaign->title),
						array(
							"name"     => $ccm->cplRemoteCampaignUser_publisher->getFullName(),
							"endDate"  => Format::getActualDateTime(),
							"campName" => $ccm->cplRemoteCampaign_cplRemoteCampaignUser->campaign_cplRemoteCampaign->title,
							"revenue"  => $revenue->amount
						),
						$campaignId,
						null,
						$previewImage
					);
					Tool::echoLog( "Finishing Email sent to " . $ccm->cplRemoteCampaignUser_publisher->user_publisher->username );
					//$ccm->finishingWarningSent = 1;
					//$ccm->save();
					$ids[] = $ccm->id;

					if($debugLevel > 0) {
						JUtil::echoLog("CPL.remote finishing mail sent to " .
							$ccm->cplRemoteCampaignUser_publisher->user_publisher->username .
							" for '" .
							$ccm->cplRemoteCampaign_cplRemoteCampaignUser->campaign_cplRemoteCampaign->title . "' campaign."
						);
					}
				} while($ccm->populate());

				$ccm = new CplRemoteCampaignUser();
				$ccm->update(
					array( "id" => $ids ),
					array( "finishingWarningSent" => 1 )
				);


			}
			elseif( $campaign->race == Campaign::ENUM_RACE_CPA_AFF ){
				$ccm = new CpaAffCampaignUser();
				$ccm->with(
					array(
						"cpaAffCampaignUser_publisher", CpaAffCampaignUser::DO_LEFT_JOIN, array(
							array(
								"user_publisher", User::NO_LEFT_JOIN
							)
						)
					),
					array(
						"cpaAffCampaign_cpaAffCampaignUser", CpaAffCampaignUser::DO_LEFT_JOIN, array(
							array(
								"campaign_cplRemoteCampaign", CpaAffCampaign::DO_LEFT_JOIN
							)
						)
					)
				);
				$ccm->populateOnce( false )->load( array(
					"cpaAffCampaign_cpaAffCampaignUser"=>array(
						"campaign_cpaAff"=>$campaignId
					)
				) );

				$ids = array();
				$previewImage = $ccm->cpaAffCampaign_cpaAffCampaignUser->campaign_cpaAff->getPreviewImageLink();
				do {
					$ccm->cpaAffCampaignUser_publisher->user_publisher->sendSysMessage(
						'V2_pubCpaAffCampaignEnd',
						array("campName"=>$ccm->cpaAffCampaign_cpaAffCampaignUser->campaign_cpaAff->title),
						array(
							"name"     => $ccm->cpaAffCampaignUser_publisher->getFullName(),
							"endDate"  => Format::getActualDateTime(),
							"campName" => $ccm->cpaAffCampaign_cpaAffCampaignUser->campaign_cpaAff->title
						),
						$campaignId,
						null,
						$previewImage
					);
					Tool::echoLog( "Finishing Email sent to " . $ccm->cpaAffCampaignUser_publisher->user_publisher->username );
					//$ccm->finishingWarningSent = 1;
					//$ccm->save();
					$ids[] = $ccm->id;

					if($debugLevel > 0) {
						JUtil::echoLog("CPA.Aff finishing mail sent to " .
							$ccm->cpaAffCampaignUser_publisher->user_publisher->username .
							" for '" .
							$ccm->cpaAffCampaign_cpaAffCampaignUser->campaign_cpaAff->title . "' campaign."
						);
					}
				} while($ccm->populate());

				$ccm = new CpaAffCampaignUser();
				$ccm->update(
					array( "id" => $ids ),
					array( "finishingWarningSent" => 1 )
				);


			}
			JPDO::commit();
			return true;
		} catch(JError $ex){
			JLog::log("cri","Error on sendFinishingMail : " . print_r($ex,true));
			JPDO::rollback();
			return false;
		}

	}

	/**
	 * Check campaign budget
	 *
	 * @param int $campaignId
	 * @return bool
	 * @author Murat
	 */
	public static function checkBudget($campaignId){
		global $debugLevel;
		$result = true;
		$campaign = new Campaign($campaignId);
		if ($campaign->state == Campaign::ENUM_STATE_PENDING_PROGRESS){

			if ($campaign->getLatestBudget() < $campaign->multiplier){
				if ($debugLevel > 0){
					Tool::echoLog("Budget ended for campaign #{$campaign->id}");
				}
				$campaign->writeHistory(
					CampaignHistory::ENUM_ACTIONID_BUDGET_ENDED,
					"Campaign budget ended. Sending mail(s) to waiting user.",
					CampaignHistory::ENUM_MADEBY_SYSTEM,
					null
				);

				$cp = new CampaignPublisher();
				$cp->with("publisher_campaignPublisher","campaign_campaignPublisher");
				$cp->populateOnce(false)->load(array(
					"campaign_campaignPublisher" => $campaignId,
					"state" => CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION
				));

				if ($cp->gotValue){
					$previewImage = $cp->campaign_campaignPublisher->getPreviewImageLink();
					do {
						$cp->publisher_campaignPublisher->loadUser();
						$cp->publisher_campaignPublisher->user_publisher->sendSysMessage(
							"V2_budgetEnded",
							array(),
							array(
								"name" => $cp->publisher_campaignPublisher->getFullName(),
								"campName" => $cp->campaign_campaignPublisher->title,
								"endDate" => Format::getActualDateTime()
							),
							$cp->campaign_campaignPublisher->id,
							null,
							null,
							$previewImage
						);
					} while($cp->populate());
				}
				unset($cp);
			} else {
				$cp = new CampaignPublisher();
				$cp->count("id","cnt",array(
					"campaign_campaignPublisher" => $campaignId,
					"state" => CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION
				));

				if ($cp->gotValue && $cp->cnt > 0){
					if ($debugLevel > 0){
						Tool::echoLog("Budget not ended yet for campaign #{$campaign->id}");
					}
					$campaign->setCronForBudget(time()+50);
					$result = false;
				}else{
					if ($debugLevel > 0){
						Tool::echoLog("Budget not ended yet, however there's no waiting publisher for campaign #{$campaign->id}");
					}
				}
				unset($cp);
			}
		}
		unset($campaign);
		return $result;
	}

	/**
	 * Get publisher click stats from SLS
	 *
	 * @param int $campaignId
	 * @param int $publisherId
	 * @param int $msgIndex
	 * @param null $additionalData
	 *
	 * @return bool
	 *
	 * @author Murat
	 */
	public static function getPublisherClickStats($campaignId,$publisherId,$msgIndex,$additionalData=null){
		$c = new Campaign($campaignId);
		if ($c->gotValue){
			if ($c->race == Campaign::ENUM_RACE_TWITTER){
				$c->loadCampaign_campaignTwitterMessage(array(
					"campaign_campaignTwitterMessage" => $campaignId,
					"publisher_campaignTwitterMessage" => $publisherId,
					"msgindex" => $msgIndex
				));
				if ($c->campaign_campaignTwitterMessage->gotValue){
					return $c->campaign_campaignTwitterMessage->loadCampaignPublisher()->getLinkStats(false,($additionalData == "Y" ? true : false));
				}
			} elseif ($c->race == Campaign::ENUM_RACE_CPC){
				$c->loadCampaign_campaignCPCMessage(array(
					"campaign_campaignCPCMessage" => $campaignId,
					"publisher_campaignCPCMessage" => $publisherId
				));
				if ($c->campaign_campaignCPCMessage->gotValue){
					return $c->campaign_campaignCPCMessage->loadCampaignPublisher()->getLinkStats(false,($additionalData == "Y" ? true : false));
				}
			}
		} else {
			return false;
		}
	}

	/**
	 * Create campaign report data
	 *
	 * @param int $campaignId
	 *
	 * @return array|null
	 *
	 * @author Murat, Özgür Köy
	 */
	public static function reportData($campaignId, $private=false){

		$c = new Campaign($campaignId);
		if ($c->gotValue){
			$rd = array(
				"publisherCount" => 0,
				"gender" => array(
					Publisher::ENUM_GENDER_M => 0,
					Publisher::ENUM_GENDER_F => 0
				),
				"age" => array(
					"13-17" => 0,
					"18-24" => 0,
					"25-34" => 0,
					"35-44" => 0,
					"45-54" => 0,
					"55+" => 0
				),
				"city" => array(),
				"response" => array(
					CampaignPublisher::ENUM_STATE_APPROVED => 0,
					CampaignPublisher::ENUM_STATE_ADMIN_DENIED => 0,
					CampaignPublisher::ENUM_STATE_BUDGET_ENDED => 0,
					CampaignPublisher::ENUM_STATE_IDLE => 0,
					CampaignPublisher::ENUM_STATE_LATE_TO_JOIN => 0,
					CampaignPublisher::ENUM_STATE_REJECTED => 0,
					CampaignPublisher::ENUM_STATE_WAITING_ADMIN_APPROVAL => 0,
					CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION => 0
				),
				"mailStats" => array(
					"sent" => 0,
					"hardBounced" => 0,
					"softBounced" => 0,
					"readMail" => 0,
					"clickedMail" => 0,
					"readRate" => 0,
					"clickRate" => 0
				)
			);

			//Publisher count

			$cp = new CampaignPublisher();
			$cp->count("id","cnt",array("campaign_campaignPublisher"=>$c->id));
			if ($cp->gotValue){
				$rd["publisherCount"] = $cp->cnt;
			}

			// Gender stat data

			$cp = new CampaignPublisher();
			$cp->groupBy(array(array("`publisher_campaignPublisher`.`gender`","gender")))->count(
				"id",
				"cnt",
				array(
					"campaign_campaignPublisher" => $c->id,
					"state" => CampaignPublisher::ENUM_STATE_APPROVED,
					"publisher_campaignPublisher" => array("id" => "&gt 0")
				)
			);

			if ($cp->gotValue){
				do {
					//echo "$cp->gender : $cp->cnt <br>" . PHP_EOL;
					$rd["gender"][$cp->gender] = $cp->cnt;
				} while($cp->populate());
			}

			//print_r($rd["gender"]);
			//exit;

			// Age range stat data

			$cp = new CampaignPublisher();
			$groupString = "";
			if (DB_ENGINE=="MYSQL"){
				$groupString = "
						CASE
				             WHEN YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) <= (YEAR(CURDATE()) - 13) AND YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) >= (YEAR(CURDATE()) - 17) THEN '13-17'
				                WHEN YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) <= (YEAR(CURDATE()) - 18) AND YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) >= (YEAR(CURDATE()) - 24) THEN '18-24'
				                WHEN YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) <= (YEAR(CURDATE()) - 25) AND YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) >= (YEAR(CURDATE()) - 34) THEN '25-34'
				                WHEN YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) <= (YEAR(CURDATE()) - 35) AND YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) >= (YEAR(CURDATE()) - 44) THEN '35-44'
				                WHEN YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) <= (YEAR(CURDATE()) - 45) AND YEAR(FROM_UNIXTIME(publisher_campaignPublisher.bday)) >= (YEAR(CURDATE()) - 54) THEN '45-54'
				                ELSE '55+'
				        END
						";
			} else {
				$groupString = 'CASE
				             WHEN EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) <= (EXTRACT(YEAR FROM CURRENT_DATE) - 13) AND EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) >= (EXTRACT(YEAR FROM CURRENT_DATE) - 17) THEN \'13-17\'
				                WHEN EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) <= (EXTRACT(YEAR FROM CURRENT_DATE) - 18) AND EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) >= (EXTRACT(YEAR FROM CURRENT_DATE) - 24) THEN \'18-24\'
				                WHEN EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) <= (EXTRACT(YEAR FROM CURRENT_DATE) - 25) AND EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) >= (EXTRACT(YEAR FROM CURRENT_DATE) - 34) THEN \'25-34\'
				                WHEN EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) <= (EXTRACT(YEAR FROM CURRENT_DATE) - 35) AND EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) >= (EXTRACT(YEAR FROM CURRENT_DATE) - 44) THEN \'35-44\'
				                WHEN EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) <= (EXTRACT(YEAR FROM CURRENT_DATE) - 45) AND EXTRACT(YEAR FROM to_timestamp(`publisher_campaignPublisher`.`bday`)) >= (EXTRACT(YEAR FROM CURRENT_DATE) - 54) THEN \'45-54\'
				                ELSE \'55+\'
				        END';
			}
			$cp
				->groupBy(array(array($groupString,"ageRange")))
				->count(
					"id",
					"cnt",
					array(
						"campaign_campaignPublisher" => $c->id,
						"state" => CampaignPublisher::ENUM_STATE_APPROVED,
						"publisher_campaignPublisher" => array("id" => "&gt 0")
					)
				);

			if ($cp->gotValue){
				do {
					$rd["age"][$cp->ageRange] = $cp->cnt;;
				} while($cp->populate());
			}

			// City stat data
			$cp = new CampaignPublisher();
			$cp->leftJoin()->orderBy("city.cityName")->groupBy(array(array("city.cityName","cityName")))->count(
				"id",
				"cnt",
				array(
					"campaign_campaignPublisher" => $c->id,
					"state" => CampaignPublisher::ENUM_STATE_APPROVED,
					"publisher_campaignPublisher" => array("city" => array("_custom" => "1=1")) //Small trick for loading the null city records
				)
			);

			if ($cp->gotValue){
				do {
					$rd["city"][$cp->cityName] = $cp->cnt;;
				} while($cp->populate());
			}

			// Response stat data
			$cp = new CampaignPublisher();
			$cp->groupBy(array(array("campaignPublisher.state","state")))->count(
				"id",
				"cnt",
				array(
					"campaign_campaignPublisher" => $c->id
				)
			);

			if ($cp->gotValue){
				do {
					$rd["response"][$cp->state] = $cp->cnt;;
				} while($cp->populate());
			}

			//Effect campaign data

			if ($c->race == Campaign::ENUM_RACE_TWITTER){
				$rd["effectData"] = array(
					"totalTrueReach" => 0,
					"totalFollower" => 0,
					"totalRetweet" => 0,
					"trueReachByDays" => array(),
					"trueReachBySendTime" => array()
				);

				//Sum of TrueReach and follower data

				$cp = new CampaignPublisher();
				$cp
					->multipleGroupOperation(true)
				    ->sum("`snTwitterHistory`.`trueReach`","sTrueReach")
				    ->sum("`snTwitterHistory`.`follower`","sFollower")
				    ->load(
						array(
							"campaign_campaignPublisher" => $c->id,
							"state" => CampaignPublisher::ENUM_STATE_APPROVED,
							"snTwitterHistory" => array("id" => "&gt 0")
						)
					);
				if ($cp->gotValue){
					$rd["effectData"]["totalTrueReach"] = $cp->sTrueReach;
					$rd["effectData"]["totalFollower"] = $cp->sFollower;
				}

				//TrueReach by days
				if (DB_ENGINE=="MYSQL"){
					$groupString = "DATE(FROM_UNIXTIME(`publisher_campaignTwitterMessage`.`toSendDate`))";
				} else {
					$groupString = "to_timestamp(`publisher_campaignTwitterMessage`.`toSendDate`)::date";
				}
				$cp = new CampaignPublisher();
				$cp
					->multipleGroupOperation(true)
					->groupBy(
						array(
							array(
								$groupString,"date"
							)
						)
					)
					->sum(
						"`snTwitterHistory`.`trueReach`",
						"sTrueReach"
					)
					->load(
						array(
							"campaign_campaignPublisher" => $c->id,
							"publisher_campaignPublisher" => array(
								"publisher_campaignTwitterMessage" => array("campaign_campaignTwitterMessage" => $c->id)
							),
							"state" => CampaignPublisher::ENUM_STATE_APPROVED,
							"snTwitterHistory" => array("id" => "&gt 0")
						)
					);
				if ($cp->gotValue){
					do {
						$rd["effectData"]["trueReachByDays"][$cp->date] = $cp->sTrueReach;;
					} while($cp->populate());
				}

				//TrueReach by send times

				$cp = new CampaignPublisher();
				$cp
					->multipleGroupOperation(true)
					->groupBy(
						array(
							array(
								"`publisher_campaignTwitterMessage`.`toSendDate`","date"
							)
						)
					)
					->sum(
						"`snTwitterHistory`.`trueReach`",
						"sTrueReach"
					)
					->load(
						array(
							"campaign_campaignPublisher" => $c->id,
							"publisher_campaignPublisher" => array(
								"publisher_campaignTwitterMessage" => array("campaign_campaignTwitterMessage" => $c->id)
							),
							"state" => CampaignPublisher::ENUM_STATE_APPROVED,
							"snTwitterHistory" => array("id" => "&gt 0")
						)
					);

				if ($cp->gotValue){
					do {
						$rd["effectData"]["trueReachBySendTime"][$cp->date] = $cp->sTrueReach;;
					} while($cp->populate());
				}

				//Total Retweet Data
				$ctm = new CampaignTwitterMessage();
				$ctm->sum("retweetCountOnCheck","sRetweetCountOnCheck",array("campaign_campaignTwitterMessage" => $c->id));
				if ($ctm->gotValue){
					$rd["effectData"]["totalRetweet"] = $ctm->sRetweetCountOnCheck;
				}
			}
			//click related
			if ($c->checkForSLS()){
				$rd["clickData"] = array(
					"referrerDomains" => array(),
					"clickByDay" => array(),
					"countryClicks" => array(),
					"referrerDetails" => array(),
					"suspectedClickSnapshot" => $c->campaign_campaignClick->suspectedClickSnapshot,
					"totalClickSnapshot" => $c->campaign_campaignClick->totalClickSnapshot
				);

				//Referrer stat data
				$rd["clickData"]["referrerDomains"] = $c->campaign_campaignClick->getReferrerStats();

				//Clicks data
				$rd["clickData"]["clickByDay"] = $c->campaign_campaignClick->getCampaignLinkStats();

				//Country click data
				$rd["clickData"]["countryClicks"] = $c->campaign_campaignClick->getCountryStats();

				//Referrer details
				$rd["clickData"]["referrerDetails"] = $c->campaign_campaignClick->getReferrerDetails();

				if ($c->race == Campaign::ENUM_RACE_CPC){
					$rd["clickData"]["blockedPublishers"] = CampaignBlockedPublisher::getList($c->id);
					$c->loadCampaign_campaignCPC();
					$rd["clickData"]["totalCalculatedClick"] = $rd["clickData"]["clickByDay"]["calculatedClickSum"];
					$rd["clickData"]["leftClick"] = $c->campaign_campaignCPC->maxClick - $rd["clickData"]["clickByDay"]["calculatedClickSum"];
					if ($rd["clickData"]["leftClick"] < 0){
						$rd["clickData"]["leftClick"] = 0;
						$rd["clickData"]["totalCalculatedClick"] = $c->campaign_campaignCPC->maxClick;
					}
				}
			}

			if ( $c->race == Campaign::ENUM_RACE_CPL ) {
				if ( !is_object( $c->campaign_cplCampaign ) )
					$c->loadCplCampaign();

				$counts           = $c->campaign_cplCampaign->getLeadCounts();
				$leads            = $c->campaign_cplCampaign->getLeadStats();
				$views            = $c->campaign_cplCampaign->getViewStats();
				$referrers        = $c->campaign_cplCampaign->getReferrerStats();
				$countries        = $c->campaign_cplCampaign->getCountryStats();
				$totalView        = $c->campaign_cplCampaign->getViewCount();

				$rd[ "leadData" ] = array(
					"leadsByDay"   => $leads,
					"viewsByDay"   => $views,
					"referrers"    => $referrers,
					"countries"    => $countries,
					"totalLeads"   => intval( $counts[ "total" ] ),
					"leftLeads"    => $c->campaign_cplCampaign->maxLeadLimit - intval( $counts[ "total" ] ),
					"invalidLeads" => intval( $counts[ "total" ] ) - intval( $counts[ "valid" ] ),
					"validLeads"   => $counts[ "valid" ],
					"totalView"    => $totalView
				);
			}
			elseif ( $c->race == Campaign::ENUM_RACE_CPL_REMOTE ) {
				if ( !is_object( $c->campaign_cplCampaign ) )
					$c->loadCplRemoteCampaign();

				$counts = $c->campaign_cplRemoteCampaign->getActiveLeadCount();

				$rd[ "leadData" ] = array(
					"totalLeads"   => $c->campaign_cplRemoteCampaign->maxLeadLimit,
					"validLeads"   => $counts,
					"clicks"       => 0,
					"leftLeads"    => $c->campaign_cplRemoteCampaign->maxLeadLimit - intval( $counts )
				);
			}
			elseif ( $c->race == Campaign::ENUM_RACE_CPA_AFF ) {
				if ( !is_object( $c->campaign_cpaAff ) )
					$c->loadCpaAffCampaign();

				$amounts  = $c->campaign_cpaAff->getLeadAmounts();
				$numbers  = $c->campaign_cpaAff->getLeadNumbers();

				$rd[ "leadData" ] = array(
					"amounts" => $amounts,
					"numbers" => $numbers
				);
			}

			$m = new Mail();
			$m->groupBy(array(array("`readState`","readState")))->count(
				"id",
				"cnt",
				array(
					"langMessageHistory_mail" => array(
						"langMessage_langMessageHistory" => array(
							"messageCode" => "V2_campaignAvailable"
						)
					),
					"mail_campaign" => $c->id
				)
			);
			$readStates = array();
			if ($m->gotValue){
				do{
					$readStates[$m->readState] = $m->cnt;
				} while($m->populate());
			}

			$m = new Mail();
			$m->count("id","cnt",array(
				"langMessageHistory_mail" => array(
					"langMessage_langMessageHistory" => array(
						"messageCode" => "V2_campaignAvailable"
					)
				),
				"mail_campaign" => $c->id,
				"clickCount" => "&gt 1"
			));

			$totalSent = 0;
			foreach($readStates as $k=>$v){
				$totalSent += $v;
			}
			$rd["mailStats"]["sent"] = $totalSent;
			$rd["mailStats"]["clickedMail"] = $m->cnt;
			if (!empty($readStates[Mail::ENUM_READSTATE_OPENED])) $rd["mailStats"]["readMail"] = $readStates[Mail::ENUM_READSTATE_OPENED];
			if (!empty($readStates[Mail::ENUM_READSTATE_SOFT_BOUNCE])) $rd["mailStats"]["softBounced"] = $readStates[Mail::ENUM_READSTATE_SOFT_BOUNCE];
			if (!empty($readStates[Mail::ENUM_READSTATE_HARD_BOUNCE])) $rd["mailStats"]["hardBounced"] = $readStates[Mail::ENUM_READSTATE_HARD_BOUNCE];
			if ($rd["mailStats"]["sent"] > 0) {
				$rd["mailStats"]["readRate"] = Format::NumberFormat($rd["mailStats"]["readMail"] / $rd["mailStats"]["sent"] * 100) . "%";
				$rd["mailStats"]["clickRate"] = Format::NumberFormat($rd["mailStats"]["clickedMail"] / $rd["mailStats"]["sent"] * 100) . "%";
			}

			//print_r($rd);
			//exit;

			if($private){
				$rd[ "response" ] = array( "accepted" => $rd[ "response" ][ CampaignPublisher::ENUM_STATE_APPROVED ], "total" => $rd[ "publisherCount" ] );
				unset( $rd[ "publisherCount" ], $rd[ "mailStats" ] );
			}

			return $rd;

		} else {
			return null;
		}
	}

	/**
	 * Get old campaign referrers by domain
	 * @Murat: Old code, don't need to edit
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public function getOldCampaignReferrersByDomain() {
		$sql = "
			SELECT
				l.referrer
			FROM
				yourls_log l,
				yourls_url u
			WHERE
				u.campaign = {$this->id}
			AND l.shorturl = u.keyword
			AND l.referrer != 'direct'
		";
		JPDO::connect( JCache::read('JDSN.URL') );
		$results = JPDO::executeQuery($sql);

		$domains = array();
		$total = 0;
		if(count($results)) {
			foreach ($results as $result) {
				$parsed = @parse_url($result['referrer']);
				$parsedHost = isset($parsed['host']) ? $parsed['host'] : '';
				if(strlen($parsedHost)) {
					$parsedHost = str_replace('www.', '', $parsedHost);
					if(!isset($domains[$parsedHost])) {
						$domains[$parsedHost] = 0;
					}
					$domains[$parsedHost]++;
					$total++;
				}
			}
		}
		if(!count($domains)) {
			$domains[SERVER_NAME] = 1;
			$total = 1;
		}
		$domains['total'] = $total;

		return $domains;
	}

	/**
	 * Get old campaign clicks
	 * @Murat: Old code, don't need to edit
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public function getOldCampaignClicks() {
		$sql = "
			SELECT
				l.click_time clicktime
			FROM
				yourls_log l,
				yourls_url u
			WHERE
				u.campaign={$this->id}
				AND u.keyword = l.shorturl
			ORDER BY
				l.click_id ASC
		";
		JPDO::connect( JCache::read('JDSN.URL') );
		$results = JPDO::executeQuery($sql);

		$clicks = array();
		if($results && count($results)) {
			foreach($results as $result) {
				$key = substr($result['clicktime'], 0, 10);
				if(!array_key_exists($key, $clicks)) {
					$clicks[$key] = 1;
				} else {
					$clicks[$key]++;
				}
			}
		}

		return $clicks;
	}

	/**
	 * Get old campaign country stats
	 * @Murat: Old code, don't need to edit
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public function getOldCampaignCountryStats() {
		$sql = "
			SELECT
				l.country_code,
				COUNT(*) AS TOPLAM
			FROM
				yourls_log l,
				yourls_url u
			WHERE
				u.campaign = {$this->id}
			AND l.shorturl = u.keyword
			GROUP BY
				l.country_code
			ORDER BY
				TOPLAM DESC
		";
		JPDO::connect( JCache::read('JDSN.URL'),false,false,true );
		$results = JPDO::executeQuery($sql);

		$countries = array();
		if(count($results)) {
			foreach ($results as $result) {
				$countries[$result['country_code']] = $result['TOPLAM'];
			}
		}
		if(!count($countries)) {
			$countries[Setting::getValue("DEFAULT_LANGUAGE")] = 1;
		}

		return $countries;
	}

	/**
	 * Get Campaign Top Clickers
	 *
	 * @return void
	 * @author Murat
	 */
	public function getCampaignTopClickers($limit=10){
		if ($this->checkForSLS()){
			$data = Tool::callSLService("getTopClickers",array(
				"params"=>  json_encode(array(
					"campaignHash"=>$this->campaign_campaignClick->linkHash,
					"limit"=>$limit
				))));

			if (!JUtil::isProduction()){
				$params = JUtil::getSearchParameter();
				if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
					Tool::echoLog("getCampaignTopClickers for {$this->campaign_campaignClick->linkHash} : " . print_r($data,true));
				}
			}

			if (is_array($data) && count($data)){
				$cum = null;
				$ccm = new CampaignCPCMessage();
				foreach($data as $stat){
					$ccm = new CampaignCPCMessage();
					$ccm->load(array("linkHash"=>$stat->_id));
					if ($ccm->gotValue){
						$ccm->calculatedClick = $stat->value->calculatedClick;
						$ccm->suspectedClick = $stat->value->suspectedClick;
						$ccm->totalClick = $stat->value->totalClick;
						$ccm->lastUpdate = time();
						$ccm->save();
						$ccm->loadCampaignPublisher()->getClickStatsAsync(false);
					}
				}
				unset($ccm);
			}
		}
	}

	/**
	 * Load and check link if it's valid for SLS
	 *
	 * @return bool
	 * @author Murat
	 */
	public function checkForSLS(){
		if (!is_object($this->campaign_campaignClick)) $this->loadCampaignClick();
		if (is_object($this->campaign_campaignClick) && ($this->race == Campaign::ENUM_RACE_CPC || (strlen($this->campaign_campaignClick->link) > 12 && !Validate::imgLink($this->campaign_campaignClick->link)))){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Finish CPC Campaign if it's not finished
	 *
	 * @return void
	 * @author Murat
	 */
	public function finishCPCCampaign(){
		if ($this->gotValue && $this->state == Campaign::ENUM_STATE_PROGRESSING){
			$cron = new Cron();
			$cron->load(array(
				"type" => Cron::ENUM_TYPE_CAMPAIGN_END,
				"campaign_cron" => $this->id
			));
			if (!$cron->gotValue){
				$cron->type = Cron::ENUM_TYPE_CAMPAIGN_END;
				$cron->campaign_cron = $this->id;
			}
			$cron->targetDate = time();
			$cron->save();
			if (date("s",time())<55){
				Cron::forcedAddToRabbitMQ(Cron::ENUM_TYPE_CAMPAIGN_END);
			}
		}
	}


	/**
	 * Last fraud control 72 hours after ended cpc campaign
	 *
	 * @return bool
	 * @author Murat
	 */
	public static function lastFraudControl($campaignId){
		global $debugLevel;

		$campaign = new Campaign($campaignId);
		$campaign->loadCampaignClick();
		$campaign->loadCampaign_campaignCPC();
		JPDO::beginTransaction();
		try{
			if ($debugLevel){
				Tool::echoLog("Getting removed fraud click count");
			}
			$data = $campaign->campaign_campaignClick->getBlockedReferrers();
			$fraudCount = 0;
			if (is_array($data) && count($data)){
				foreach ($data as $r) {
					$fraudCount += $r->cleared;
				}
			}
			$campaign->campaign_campaignClick->fraudClickCount = $fraudCount;
			$campaign->save();
			$campaign->writeHistory(
				CampaignHistory::ENUM_ACTIONID_FRAUD_CONTROL,
				"Removed fraud click count: $fraudCount",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				$fraudCount
			);
			if ($debugLevel){
				Tool::echoLog("Campaign last fraud control executed successfully");
				Tool::echoLog("Updating campaign click stats");
			}
			$campaign->campaign_campaignClick->updateCampaignLinkStats();

			if ($campaign->campaign_campaignCPC->maxClick > $campaign->campaign_campaignClick->calculatedClick){
				$refundCount = $campaign->campaign_campaignCPC->maxClick - $campaign->campaign_campaignClick->calculatedClick;
				if ($debugLevel){
					Tool::echoLog("Giving back the unused click count: " . $refundCount);
				}
				$refundAmount = ($refundCount*$campaign->campaign_campaignCPC->cpcCost*$campaign->multiplier);
				AdvertiserMoneyAction::create(
					$campaign->advertiser,
					$campaign->id,
					$refundAmount,
					'cpc campaign refund: ' . $campaign->title,
					null
				);

				$campaign->writeHistory(
					CampaignHistory::ENUM_ACTIONID_FRAUD_CONTROL,
					"Refunded unused click value amount: " . $refundAmount,
					CampaignHistory::ENUM_MADEBY_SYSTEM,
					$refundAmount
				);
				if ($debugLevel){
					Tool::echoLog("Refunded unused click value amount: {$refundAmount}");
				}
			} else {
				if ($debugLevel){
					Tool::echoLog("All click budget used. So no need to refund any amount");
				}
			}

			if ($debugLevel){
				Tool::echoLog("Getting the publishers last click amount");
			}
			$cp = new CampaignPublisher();
			$cp->populateOnce(false)->load(array(
				"campaign_campaignPublisher" => $campaign->id,
				"state" => CampaignPublisher::ENUM_STATE_APPROVED
			));
			if ($cp->gotValue){
				$cnt=0;
				do{
					$cp->getLinkStats(false);
					$cnt++;
				} while ($cp->populate());
				$campaign->writeHistory(
					CampaignHistory::ENUM_ACTIONID_FRAUD_CONTROL,
					"Publisher click stats updated count: " . $cnt,
					CampaignHistory::ENUM_MADEBY_SYSTEM,
					$cnt
				);
				if ($debugLevel){
					Tool::echoLog("Publisher click stats updated count: " . $cnt);
				}
			} else {
				if ($debugLevel){
					Tool::echoLog("No approved publisher found");
				}
			}

			if ($debugLevel){
				Tool::echoLog("Approving the user revenues");
			}
			$pr = new PublisherRevenue();
			$pr->update(array("campaign_publisherRevenue"=>$campaign->id),array("status"=>PublisherRevenue::ENUM_STATUS_APPROVED,"approveDate"=>time()));

			JPDO::commit();
			return true;
		} catch(Exception $ex){
			JPDO::rollback();
			JLOG::log("cri","Exception occured while last fraud control of campaign #cid: {$campaignId} : " . serialize($ex));
			return false;
		}
	}

	public function getExcelReport(){
		$data = Campaign::reportData($this->id);

		$eb = new ExcelBuilder();
		$eb->createHeaderRow("General Information");

		$eb->createRow("Report Date",date("Y-m-d H:i"))
			->createRow("Campaign Title",$this->title)
			->createRow("Campaign Start Date",$this->startDate)
			->createRow("Campaign End Date",$this->endDate)
			->createRow("Publisher Count",$data["publisherCount"]);

		switch($this->race){
			case Campaign::ENUM_RACE_TWITTER:
				$eb->createRow("Follower", $data["effectData"]["totalFollower"])
					->createRow("Influence", $data["effectData"]["totalTrueReach"])
					->createRow("Retweet", $data["effectData"]["totalRetweet"]);
				break;
			case Campaign::ENUM_RACE_CPC:
				$eb->createRow("Calculated Click", $data["clickData"]["clickByDay"]["calculatedClickSum"])
					->createRow("Suspected Click", $data["clickData"]["clickByDay"]["suspectedClickSum"]);
				break;
			case Campaign::ENUM_RACE_CPL:
				$eb->createRow("Total Leads", $data["leadData"]["totalLeads"])
					->createRow("Valid Leads", $data["leadData"]["validLeads"]);
				break;
			case Campaign::ENUM_RACE_CPL_REMOTE:
				$eb->createRow("Total Leads", $data["leadData"]["totalLeads"])
					->createRow("Valid Leads", $data["leadData"]["validLeads"]);
				break;
			case Campaign::ENUM_RACE_CPA_AFF:
//				$eb->createRow("Total Leads", $data["leadData"]["totalLeads"])
//					->createRow("Valid Leads", $data["leadData"]["validLeads"]);
				break;
		}
		if (isset($data["clickData"])){
			$eb->createRow("Total Click", $data["clickData"]["clickByDay"]["totalClickSum"]);
		}

		$eb->createEmptyRow();
		$eb->createHeaderRow("Gender Distribution");
		$eb->createRow("Male",$data["gender"][1]);
		$eb->createRow("Female",$data["gender"][2]);

		$eb->createEmptyRow();
		$eb->createHeaderRow("Age Distribution");
		$eb->createSubHeaderRow("Age Range","Count");
		foreach($data["age"] as $k=>$v){
			$eb->createRow($k,$v);
		}

		$eb->createEmptyRow();
		$eb->createHeaderRow("Response Distribution");
		$eb->createSubHeaderRow("Status","Count");
		$eb->createRow("Not answered",$data["response"][2]);
		$eb->createRow("Waiting Admin Approval",$data["response"][3]);
		$eb->createRow("Accepted",$data["response"][5]);
		$eb->createRow("Denied",$data["response"][6]);
		$eb->createRow("Late (for time)",$data["response"][7]);
		$eb->createRow("Late (budget end)",$data["response"][8]);

		$eb->createEmptyRow();
		$eb->createHeaderRow("City Distribution");
		$eb->createSubHeaderRow("City","Count");
		foreach($data["city"] as $k=>$v){
			$eb->createRow($k,$v);
		}

		if ($this->race == Campaign::ENUM_RACE_TWITTER){
			$eb->createEmptyRow();
			$eb->createHeaderRow("Influence By Day");
			$eb->createSubHeaderRow("Date","Influence");
			foreach($data["effectData"]["trueReachByDays"] as $k=>$v){
				$eb->createRow($k,$v);
			}
		}

		if (isset($data["clickData"])){
			$eb->createEmptyRow();
			$eb->createHeaderRow("Clicks By Day");
			$eb->createSubHeaderRow("Date","Count");
			switch($this->race){
				case Campaign::ENUM_RACE_TWITTER:
					foreach($data["clickData"]["clickByDay"]["clickDates"] as $k=>$v){
						$eb->createRow($v,$data["clickData"]["clickByDay"]["totalClick"][$k]);
					}
					break;
				case Campaign::ENUM_RACE_CPC:
					foreach($data["clickData"]["clickByDay"]["clickDates"] as $k=>$v){
						$eb->createRow($v,array(
							"Calculated: " . $data["clickData"]["clickByDay"]["calculatedClick"][$k],
							"Suspected: " . $data["clickData"]["clickByDay"]["suspectedClick"][$k],
							"Total: " . $data["clickData"]["clickByDay"]["totalClick"][$k]));
					}
					break;
			}

			$eb->createEmptyRow();
			$eb->createHeaderRow("Click Countries");
			$eb->createSubHeaderRow("Country","Count");
			foreach($data["clickData"]["countryClicks"] as $k=>$v){
				$eb->createRow(strlen($k) ? $k : "Outside of current country",$v);
			}

			$eb->createEmptyRow();
			$eb->createHeaderRow("Referrer Domains");
			$eb->createSubHeaderRow("Referrer","Count");
			foreach($data["clickData"]["referrerDomains"] as $k=>$v){
				$eb->createRow($k,$v);
			}

			$eb->createEmptyRow();
			$eb->createHeaderRow("Referrer Details");
			$eb->createSubHeaderRow("Referrer","Count");
			if (is_array($data["clickData"]["referrerDetails"])){
				foreach($data["clickData"]["referrerDetails"] as $k=>$v){
					$eb->createRow($v->hash,$v->totalCount);
				}
			}

		}
		if(isset($data["leadData"])){
			$eb->createEmptyRow();
			$eb->createHeaderRow("Leads By Day");
			$eb->createSubHeaderRow("Date","Count");

			foreach($data["leadData"]["leadsByDay"]as $k=>$v){
				$eb->createRow($k,array(
					"Approved: " . $v["valid"],
					"Invalid: " . $v["invalid"],
					"Total: " . $v["total"])
				);
			}

			$eb->createHeaderRow("Views By Day");
			$eb->createSubHeaderRow("Date","Count");
			foreach($data["leadData"]["viewsByDay"]as $k=>$v){
				$eb->createRow($k,$v );
			}

			$eb->createHeaderRow("Referrers");
			$eb->createSubHeaderRow("Referrer","Count");
			foreach($data["leadData"]["referrers"]as $k=>$v){
				$eb->createRow($k,$v );
			}
		}

		$eb->finalizeDoc();

		$eb->getBinary();
	}

	/**
	 * Update campaign counts after publisher delete
	 *
	 * @return void
	 * @author Murat
	 */
	public function updateCampaignStats(){
		$cp = new CampaignPublisher();
		switch($this->race){
			case Campaign::ENUM_RACE_TWITTER:
				$cp->multipleGroupOperation(true)
					->sum("`snTwitterHistory`.`trueReach`", "sTrueReach")
					->sum("`snTwitterHistory`.`follower`","sFollower")
					->sum("`snTwitterHistory`.`cost`","sCost")
					->count( "id", "cPublisher" )
					->load(array(
						"snTwitterHistory" => array(
							"_custom" => "1=1"
						),
						"campaign_campaignPublisher" => $this->id
					));
				$this->publisherCount = $cp->cPublisher;
				if (!is_object($this->campaign_campaignTwitter)) $this->loadCampaign_campaignTwitter();
				$this->campaign_campaignTwitter->influencer = $cp->sTrueReach;
				$this->campaign_campaignTwitter->follower = $cp->sFollower;
				$this->campaign_campaignTwitter->maxCost = $cp->sCost;
				$this->campaign_campaignTwitter->save();
				$this->save();
				break;
			case Campaign::ENUM_RACE_CPL_REMOTE:
			case Campaign::ENUM_RACE_CPA_AFF:
			case Campaign::ENUM_RACE_CPL:
			case Campaign::ENUM_RACE_CPC:
				$cp->multipleGroupOperation(true)
					->count( "id", "cPublisher" )
					->load(array(
						"campaign_campaignPublisher" => $this->id
					));
				$this->publisherCount = $cp->cPublisher;
				$this->save();

				break;
		}


	}


	/**
	 * Get reverse commission rate for calculation from bottom to top
	 *
	 * @return float
	 * @author Murat
	 */
	public function getReverseCommissionRate(){
		if (is_null($this->commissionRate) && $this->commissionRate == 0){
			return 1;
		} else {
			return (100/(1-($this->commissionRate/100)))/100;
		}
	}

	/**
	 * Save campaign preview image
	 *
	 * @return string
	 * @author Murat
	 */
	public function savePreviewImage($params){
		global $__DP;
		$tempUploadPath = $__DP.$this->uploadPath;

		$temp = explode(".", $params["previewFile"]["name"]);
		$extension = strtolower(end($temp));

		$fileName = JUtil::randomString(32) . "." . $extension;
		$upFileName = $tempUploadPath.$fileName;
		if (file_exists($upFileName))
		{
			unlink($upFileName);
		}
		copy( $params[ "previewFile" ][ "tmp_name" ], $upFileName );
//		move_uploaded_file($params["previewFile"]["tmp_name"],$upFileName);

		/*
		 * Don't need to resize anymore because of crop
		//resize image
		require_once($__DP . "site/lib/imageResize.php");
		$campaignImageMaxWidth = Setting::getValue("campaignImageMaxWidth");
		$campaignImageMaxHeight = Setting::getValue("campaignImageMaxHeight");

		$image = new resize($tempUploadPath.$fileName);
		//Resize image options: exact, portrait, landscape, auto, crop
		if ($image->width > $campaignImageMaxWidth || $image->height > $campaignImageMaxHeight){
			$image->resizeImage($campaignImageMaxWidth, $campaignImageMaxHeight , "auto");
			$image->saveImage($tempUploadPath.$fileName, 100);
		}
		unset($image,$campaignImageMaxWidth,$campaignImageMaxHeight,$temp,$extension,$upFileName);
		*/

		return $fileName;
	}

	/**
	 * Get campaign list by filter
	 *
	 * @params null $filter
	 * @return array
	 *
	 * @author Murat
	 */
	public static function getCampaigns($filter = null,$order = null){
		if (is_null($filter)) $filter = array("state" => array(Campaign::ENUM_STATE_PENDING_PROGRESS,Campaign::ENUM_STATE_PROGRESSING,Campaign::ENUM_STATE_FINISHED));
		if (is_null($order)) $order = "`id` DESC";
		$c = new Campaign();
		$c->populateOnce(false)->orderBy($order)->load($filter);
		return $c;
	}

	/**
	 * Get campaign preview image if campaign link exist
	 *
	 * @return null|string
	 *
	 * @author Murat
	 */
	public function getPreviewImageLink(){
		$campaignImage = null;
		switch($this->race){
			case Campaign::ENUM_RACE_TWITTER:
			case Campaign::ENUM_RACE_CPC:
				if (!is_object($this->campaign_campaignClick)) $this->loadCampaignClick();
				if ($this->campaign_campaignClick->gotValue) $campaignImage = JUtil::siteAddress() . "site/layout/images/campaign/" . $this->campaign_campaignClick->previewFileName;

				break;

			case Campaign::ENUM_RACE_CPL:
				if(!is_object($this->campaign_cplCampaign)) $this->loadCplCampaign();
				if($this->campaign_cplCampaign && $this->campaign_cplCampaign->gotValue) $campaignImage = JUtil::siteAddress() . "site/layout/images/campaign/" . $this->campaign_cplCampaign->previewFileName;

				break;
			case Campaign::ENUM_RACE_CPL_REMOTE:
				if(!is_object($this->campaign_cplRemoteCampaign))
					$this->loadCplRemoteCampaign();
				if($this->campaign_cplRemoteCampaign && $this->campaign_cplRemoteCampaign->gotValue)
					$campaignImage = JUtil::siteAddress() . "site/layout/images/campaign/" . $this->campaign_cplRemoteCampaign->previewFileName;

				break;
			case Campaign::ENUM_RACE_CPA_AFF:
				if(!is_object($this->campaign_cpaAff))
					$this->loadCpaAffCampaign();
				if($this->campaign_cpaAff && $this->campaign_cpaAff->gotValue)
					$campaignImage = JUtil::siteAddress() . "site/layout/images/campaign/" . $this->campaign_cpaAff->previewFileName;

				break;
		}

		return $campaignImage;
	}


	public function getSubObject() {
		switch($this->race){
			case Campaign::ENUM_RACE_TWITTER:
			case Campaign::ENUM_RACE_CPC:
				$this->loadCampaignClick();
				return $this->campaign_campaignClick;

				break;

			case Campaign::ENUM_RACE_CPL:
				$this->loadCplCampaign();
				$this->campaign_cplCampaign->link = Setting::getValue( "CPL_LANDING_DOMAIN" ) . "/" . $this->campaign_cplCampaign->preHash;
				return $this->campaign_cplCampaign;

				break;
			case Campaign::ENUM_RACE_CPL_REMOTE:
				$this->loadCplRemoteCampaign();
				return $this->campaign_cplRemoteCampaign;

				break;
			case Campaign::ENUM_RACE_CPA_AFF:
				$this->loadCpaAffCampaign();
				return $this->campaign_cpaAff;

				break;
		}
	}

	/**
	 * Upload temporary picture for new/edit campaign
	 *
	 * @param array $file
	 * @return array
	 * @author Murat
	 */
	public static function uploadTempCampPicture($file){
		global $__DP;
		$uploadPath = "site/layout/images/campaign/";

		// image name
		$name = "_temp_" . JUtil::randomString(20) . ".";

		// location to save cropped image
		$url = $__DP.$uploadPath.$name;

		$dst_x = 0;
		$dst_y = 0;

		$src_x = $file['x']; // crop Start x
		$src_y = $file['y']; // crop Srart y

		$src_w = $file['w']; // $src_x + $dst_w
		$src_h = $file['h']; // $src_y + $dst_h

		// set a specific size for the image
		// the default is to grab the width and height from the cropped image.
		$dst_w = 600;
		$dst_h = 315;

		// remove the base64 part
		$base64 = $file['image'];
		$image = null;


		// if URL is a base64 string
		if (substr($base64, 0, 5) == 'data:') {

			// remove data from image
			$base64 = preg_replace('#^data:image/[^;]+;base64,#', '', $base64);

			$base64 = base64_decode($base64);

			// create image from string
			$source = imagecreatefromstring($base64);
			imagealphablending($source, FALSE);
			imagesavealpha($source, TRUE);

			$url .= Validate::getImageMimeType($base64);
		}
		else {

			// strip parameters from URL
			$base64 = strtok($base64, '?');

			list($height, $width, $type) = getimagesize($base64);

			// create image
			if ($type == 1){
				$source = imagecreatefromgif($base64);
				$url .= "gif";
			}else if ($type == 2){
				$source = imagecreatefromjpeg($base64);
				$url .= "jpg";
			}else if ($type == 3) {
				$source = imagecreatefrompng($base64);
				$url .= "png";

				// keep transparent background
				imagealphablending($image, FALSE);
				imagesavealpha($image, TRUE);

			}
			else die();

		}

		// resize image variable
		$image = imagecreatetruecolor($dst_w, $dst_h);

		// process cropping and resizing of image
		imagecopyresampled($image, $source, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

		// save image
		imagejpeg($image, $url, 100);

		$filesize = filesize( $url );

		$fileName = $name . substr($url,-3);
		$_SESSION["tempCampPicture"] = array(
			"fullPath" => $url,
			"size"=>$filesize,
			"fileName" => $fileName
		);

		// return URL
		$validation = array (
			'url'     => JUtil::siteAddress() . $uploadPath . $fileName
		);

		return $validation;
	}

	/**
	 * get click count from sls server for remote campaign
	 *
	 * @param $campPublicId
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function getHashClickCount( $hash ) {
		$url = Tool::callSLService(
			"redirectionStats",
			array(
				"params" => json_encode(
					array(
						"hash" => $hash
					)
				)
			)
		);

		return $url[ 0 ]->total;
	}


	/**
	 * get red link for not sls related campaigns
	 *
	 * @param       $campaignCode
	 * @param       $link
	 * @param       $hash
	 * @param array $details
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function getRedLink( $campaignCode, $link, $hash=null, $details=array() ) {
		if(!is_null($hash))
			$link = str_replace( "[code]", $hash, $link );

		$SD = $details[ "shortUrl" ];
		unset( $details[ "shortUrl" ] );

		$url = Tool::callSLService(
			"shortenURL",
			array(
				"params" => json_encode(
					array(
						"url"        => Format::parametizer( $link ),
						"camp"       => $campaignCode,
						"jsRedirect" => 1,
						"details"    => Format::parametizer(json_encode( $details ))
					)
				)
			)
		);

		if ( is_object($url) && isset($url->code) && is_string( $url->code ) )
			return $SD . $url->code;
		else
			return false;
	}

	/**
	 * get click count from sls server for red onlys
	 *
	 * @param $campPublicId
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function getRedClickCount( $campPublicId ) {
		$url = Tool::callSLService(
			"redirectionStats",
			array(
				"params" => json_encode(
					array(
						"camp"=> $campPublicId
					)
				)
			)
		);
		return $url[0]->total;
	}



	/**
	 * Get shortlink domain if campaign link exist
	 *
	 * @return null|string
	 *
	 * @author Murat
	 */
	public function getShortLinkDomain(){
		$SLDomain = null;
		switch($this->race){
			case Campaign::ENUM_RACE_TWITTER:
			case Campaign::ENUM_RACE_CPC:
				if (!is_object($this->campaign_campaignClick)) $this->loadCampaignClick();
			    if ($this->campaign_campaignClick->gotValue) $SLDomain = $this->campaign_campaignClick->SLDomain;

				break;
		}

		if (is_null($SLDomain) || strlen($SLDomain) == 0) $SLDomain = Setting::getValue("SHORTDOMAIN");

		return $SLDomain;
	}

	/**
	 * constructor for the class
	 *
	 * @return void
	 * @author Murat
	 **/
	public function delete($id = null)
	{
		/*
		 * Model delete function will only set campaign column to null. So we should delete records manually
		 * $this->deleteAdvertiserMoneyAction();
		 * $this->deleteCampaignSharingStats();
		 * $this->deleteDSNotification();
		 * $this->deletePublisherRevenue();
		 * $this->deleteUserMessage();
		 * $this->deletePushMobileMessage();
		 */
		$ama = new AdvertiserMoneyAction();
		$ama->multiDelete(array("campaign_advertiserMoneyAction" => $this->id));
		$css = new CampaignSharingStats();
		$css->multiDelete(array("campaignSharingStats_campaign" => $this->id));
		$dn = new DSNotification();
		$dn->multiDelete(array("dsn_campaign" => $this->id));
		$pr = new PublisherRevenue();
		$pr->multiDelete(array("campaign_publisherRevenue" => $this->id));
		$um = new UserMessage();
		$um->multiDelete(array("campaign" => $this->id));
		$pmm = new PushMobileMessage();
		$pmm->multiDelete(array("pushMobileMessage_campaign" => $this->id));

		//Don't delete cron jobs because they might pushed an waiting response from RabbitMQ
		$cron = new Cron();
		$cron->update(array("campaign_cron"=>$this->id,"state"=>Cron::ENUM_STATE_WAITING),array("state"=>Cron::ENUM_STATE_CANCELLED));

		unset($ama,$css,$dn,$pr,$um,$pmm,$cron);
		parent::delete($id);
	}

	/**
	 * Remove campaign with confirmation
	 *
	 * @return string
	 *
	 * @author Murat
	 */
	public function removeCampaign($params){
		//Check for campaign email job executed
		$check = new Cron();
		$check->count("id","cnt",array(
			"type" => Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL . " &or " . Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_TRENDSETTER,
			"state" => array(Cron::ENUM_STATE_ADDED_TO_QUEUE,Cron::ENUM_STATE_ADDING_TO_QUEUE,Cron::ENUM_STATE_PROCESSING,Cron::ENUM_STATE_SUCCESSFUL),
			"campaign_cron" => $this->id
		));
		if ($check->gotValue && $check->cnt && !$params["emailPass"]){
			return "emailWaiting";
		} else {
			$check = new CampaignPublisher();
			$check->load(array(
				"campaign_campaignPublisher" => $this->id,
				"state" => CampaignPublisher::ENUM_STATE_APPROVED
			));
			if ($check->gotValue && !$params["acceptedPublisherPass"]){
				return "acceptedPublisherPassExists";
			} else {
				JPDO::beginTransaction();
				try{
					$this->delete();
					JPDO::commit();
					return "ok";
				} catch(Exception $ex){
					JPDO::rollback();
					JLog::log("cri","An unknown error occured while deleting campaign. Error: " . serialize($ex));
					return "nok";
				}
			}
		}
	}
}
