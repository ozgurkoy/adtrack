<?php
/**
 * cplCampHistory real class - firstly generated on 08-04-2014 21:10, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplCampHistory.php';

class CplCampHistory extends CplCampHistory_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function saveHistory( $code, $action, $madeBy = CplCampHistory::ENUM_MADEBY_USER ) {
		$cv = new CplCampHistory();
		$cv->overrideTableName( "CPL_" . $code . "_cplHistory" );
		$cv->action  = $action;
		$cv->details = json_encode( $_SERVER );
		$cv->madeBy  = $madeBy;

		$cv->save();

		unset( $cv );
	}


}