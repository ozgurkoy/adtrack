<?php
class sapiUser
{
	protected $base;


	function __construct()
	{
		$this->base = sapiCore::getInstance();
	}

	public function check(){
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$data = $this->base->_request->json;

			if (!isset($this->base->_request->json->type) || !isset($this->base->_request->json->uid)){
				$this->base->sendResponse(406);
			} else {
				switch($this->base->_request->json->type){
					case "email":
					case "facebook":
					case "twitter":
					case "gplus":
					case "vk":
					case "xing":
						echo json_encode(User::check($this->base->_request->json->type,$this->base->_request->json->uid));
						break;
					default:
						$this->base->sendResponse(501);
						break;
				}
			}
		}
	}

	public function register(){
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$data = $this->base->_request->json;

			if (!isset($this->base->_request->json->email) ||
				!isset($this->base->_request->json->name) ||
				!isset($this->base->_request->json->surname) ||
				!isset($this->base->_request->json->clubID) ||
				!isset($this->base->_request->json->loginType) ||
				!isset($this->base->_request->json->mbID
			)){
				$this->base->sendResponse(406);
			} else {
				echo json_encode(User::mbRegister(
					$this->base->_request->json->email,
					$this->base->_request->json->name,
					$this->base->_request->json->surname,
					$this->base->_request->json->clubID,
					$this->base->_request->json->loginType,
					$this->base->_request->json->mbID,
					isset($this->base->_request->json->adMingleID) ? $this->base->_request->json->adMingleID : null
				));
			}
		}
	}

}