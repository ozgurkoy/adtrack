<?php
/**
 * cplCampToken real class - firstly generated on 09-04-2014 12:41, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplCampToken.php';

class CplCampToken extends CplCampToken_base
{

	public static $tokenLife = 40000;
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * get view token
	 *
	 * @param $code
	 * @param $viewToken
	 * @return string
	 * @author Özgür Köy
	 */
	public function getToken( $code, $viewToken ) {
		$token = JUtil::randomString( 30 );
		$this->overrideTableName( "CPL_" . $code . "_cplTokens" );
		$this->viewToken = $viewToken;
		$this->expires = time() + CplCampToken::$tokenLife;
		$this->token = $token;
		$this->save();

		return $token;
	}
}

