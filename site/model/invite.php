<?php
	/**
	* invite real class for Jeelet - firstly generated on 10-01-2012 11:28, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Sam Ben Yakar
	**/
	include($__DP."/site/model/base/invite.php");

	class Invite extends Invite_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Sam Ben Yakar
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}
	
                /*
                 * Add User Facebook details to invite table
                 * * return Saved UserID
                 */
                public function AddFacebookUser($fbID,$fbAccessToken) {

                  $newUser = false;
                  if ($this->loadIviteID())
                    {
                        // User that have Twitter or mail address
                        $this->facebookID = $fbID;
                        
                    }else{
                    
                        $this->facebookID = $fbID;
                        $this->load();

                        if($this->count())
                        {
                            // user Exists , Check for Twitter Post
                            $_SESSION['inviteSendfbPostStatus'] = $this->CheckInviteFacebookPost();
                        }
                        else {
                            $newUser = true;
                            $this->saveLogin();
                        } 
                    }
                    
                    $this->facebookKeys = $fbAccessToken;
                    $facebook = new UserFacebook();
                    $facebook->facebookID = $this->facebookID;
                    $facebook->load();
                    $email=$facebook->getUserEmail();
                    if($this->email == null)
                    {
                       $this->email = $email;
                       $_SESSION["facebookEmail"] = $email;
                    }
                    else if($this->email == $email)
                    {
                       $_SESSION["facebookEmail"] = $email; 
                    }else{
                        unset($_SESSION["facebookEmail"]);
                    }


                    $this->save();


                    $_SESSION['fid'] = $this->facebookID;
                    $_SESSION['inviteid'] = $this->id;
                    return $newUser;
                }
                
                
                /*
                 * Add User Facebook details to invite table
                 * * return Saved UserID
                 */
                public function SaveFacebookPost($fbPostID) {

                  
                  if ($this->loadIviteID())
                    {
                        // User that have Twitter or mail address
                        $facebook = new UserFacebook();
                        $facebook->facebookID = $this->facebookID;
                        $facebook->load();
                        $postData=$facebook->GetPost($fbPostID);
                        
                        $this->facebookPost = json_encode($postData);
                        $this->save();
                        return true;
                    }else{
                  
                        return false;
                    }
                  
                }
                
                /*
                 * Add User twitter login
                 * return true - added , false - exists
                 */
                public function AddTwitterUser($twKey,$twKeySecret,$twData) {
                    // Check for user Email if not valid
                    
                    $newUser = false;
                    
                    if ($this->loadIviteID())
                    {
                        // User that have facebook or mail address
                        if($this->twitterID !=$twData->id)
                        {
                            $this->twitterID = $twData->id;
                            $newUser = true;
                        }else {
                            $_SESSION['inviteSendTweetStatus'] = $this->CheckInviteTweet();
                        }
                        
                    }else{
                    
                        $this->twitterID = $twData->id;
                        $this->load();

                        if($this->count())
                        {
                            // user Exists , Check for Twitter Post
                            $_SESSION['inviteSendTweetStatus'] = $this->CheckInviteTweet();
                        }
                        else {
                            $newUser = true;
                            $this->saveLogin();
                        } 
                    }
                    
                    $this->twitterKeys = json_encode(array("Key"=>$twKey,"Secret"=>$twKeySecret,"Date"=>time()));
                    $this->twitterData = json_encode($twData);

                    $this->save();


                    $_SESSION['tid'] = $this->twitterID;
                    $_SESSION['inviteid'] = $this->id;
                    return $newUser;
                }
                
                /*
                 * Check if the sent tweet is Active or deleted
                 * false - delted / Never sent
                 * true - Active
                 */
                public function CheckInviteTweet() {
                
                    //die(print_r($this->tweetData,true));
                    
                    if($this->tweetData !=null)
                    {
                        $tweetData = json_decode($this->tweetData);
                        

                        return $tweetData->state;
                    }
                    else
                    {
                        return "noTweet";
                    }
                }
                
                public function CheckInviteFacebookPost() {
                
                        return "NULL";
                }
                
                
                
                public static function GetInviteTweetMessage() {
                    global $_S;

                    $url = $_S.' @' . TWITTER_USER;    
                    return JUtil::getLangWord("admingleTwitterPost") . $url;
                }
                
                /*
                 * 
                 */
                public function SendInviteTweet() {
                    global $__DP;
                    if ($this->loadIviteID())
                    {
                        $message = self::GetInviteTweetMessage();
                        
                        require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');
                        
                        $twitterInfo = json_decode($this->twitterKeys);
                        //die(print_r($twitterInfo,true));

                        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $twitterInfo->Key, $twitterInfo->Secret);
                        $logT = Twitter::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $twitterInfo->Key, $twitterInfo->Secret, 'statuses/update?status='.$message);

                        $postResult = $connection->post('statuses/update', array('status' => $message));
                        Twitter::logResponse($logT, $postResult);
                        
                        $tweetStaus = new stdClass();
                        $tweetStaus->tweet = $message;
                        $tweetStaus->postResult = $postResult;
                        
                        $tweetSent = false;
                        if(isset($postResult->error)) {
                            if(strpos($postResult->error, 'Could not authenticate') !== false) {
                                    $tweetStaus->state = 'error';
                                    $tweetStaus->errorMessage = 'Could not authenticate'; 
                            } else {
                                    $tweetStaus->state = 'duplicated';
                                    // 	Status is a duplicate.
                                    $tweetSent =true;
                            }
                        } elseif(isset($postResult->errors)) {
                                //api error
                                JLog::log('twi', 'Twitter, send post error'.serialize($postResult)."|||".serialize($_SESSION));
                                if (isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 89){ //Invalid or expired token
                                        $tweetStaus->state = 'error';
                                        $tweetStaus->errorMessage = 'Invalid or expired token';
                                } elseif (isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 187){ //Status is a duplicate
                                        $tweetStaus->state = 'duplicated';
                                        $tweetStaus->message = 'duplicated';
                                } else {
                                        $tweetStaus->state = 'error';
                                        $tweetStaus->errorMessage = 'Error';
                                }
                                $_SESSION['twitterStatus'] = 6; // sent tweet fail
                        } else {
                                $tweetStaus->state = 'sent';
                                $tweetStaus->tweetId = $postResult->id_str;
                                $tweetStaus->tweetSentDate = time();
                                $tweetStaus->toCheckDate = time() + (72 * 60 * 60);
                                $_SESSION['twitterStatus'] = 5; // sent tweet successfully
                                $tweetSent = true;
                        }
                        
                        $this->tweetData = json_encode($tweetStaus);
                        $this->save();
                        return $tweetSent;
                    }
                    else{
                        $_SESSION['twitterStatus'] = 6; // tryin to send tweet
                        return false;
                    }
                }
                
                
                
                /*
                 * Add user with only Email account
                 * * return Saved UserID
                 */
                public function AddEmailUser($email) {
                    
                    if ($this->loadIviteID())
                    {
                        
                        if($this->email === $email)
                        {
                            return false;
                            
                        }else{
                            $this->email = $email;
                            $this->save();
                        }
                        return true;
                        
                    }  else {
                        
                        $this->email = $email;
                        $this->load();

                        $tid = isset($_SESSION['tid'])?$_SESSION['tid']:null;
                        $fid = isset($_SESSION['tid'])?$_SESSION['fid']:null;


                        if($this->count())
                        {
                            // user Exists , Check for Twitter Post
                            if($tid)
                            {
                                if($tid != $this->twitterID)
                                {
                                    // Problem !!! Twitter user enter registerd mail
                                }

                                if($fid != $this->facebookID)
                                {
                                    // Problem !!! facebook user enter registerd mail
                                }
                            }

                            $_SESSION['emailValidate'] = $this->emailValidate;

                            return false;
                        }
                        else {

                            if($tid)
                            {
                                if($fid)
                                {
                                    $this->facebookID = $fid;
                                }
                                $this->twitterID = $tid;
                                $this->load();
                            }elseif ($fid) {
                                $this->facebookID = $fid;
                                $this->load();
                            }


                            $this->saveLogin();
                            $this->emailValidate = false;
                            $this->save();
                            $this->SendValidateEmail();
                            $_SESSION['inviteid'] = $this->id;
                            return true;
                        }
                    }
                    
                    
                }
                /*
                 * Send Email Validatetion code
                 * * return true if sent
                 */
                public function SendValidateEmail() {
                    
                    
                }
                
                private function loadIviteID() {
                    
                    if(isset($_SESSION['inviteid']))
                    {
                         $this->id = $_SESSION['inviteid'];
                         $this->load();
                         
                         if($this->count)
                             return true;
                    }
                    
                    return false;
                }
                
                /*
                 * Save session values
                 * * return Saved UserID
                 */
                private function saveLogin() {
                    $this->state = "r";
                    $this->lan = $_SESSION['lang'];
                    $this->ip = $_SERVER['REMOTE_ADDR'];
                    $this->referral = $_SESSION['inviteReferral'];
                }
                
        }
        
?>