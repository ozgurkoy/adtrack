<?php
	/**
	* city real class for Jeelet - firstly generated on 11-10-2011 11:44, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/city.php");

	class City extends City_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}
		
		/**
		 * load all cities to array
		 *
		 * @return array
		 * @author Özgür Köy
		 * @author Murat (modify)
		 **/
		public function loadAll($country=null)
		{
			$this->nopop()->orderBy("cityName")->load(is_null($country)?null:array("country"=>$country));
			$thisty = array();

			if(sizeof($this->_ids)>0){
				while ($this->populate()) {
					$thisty[$this->id] = $this->cityName;
				}
			}      
			return $thisty;
		}

		/**
		 * Get all active cities
		 *
		 * @param bool $asObject
		 *
		 * @return City|array
		 *
		 * @author Murat
		 */
		public static function getActiveCities($asObject=true){
			$c = new City();
			$c->populateOnce(false)->load(array("active"=>1));

			if ($asObject) {
				return $c;
			} else {
				$cities = array();
				while($c->populate()) {
					$cities[$c->id] = $c->cityName;
				}
				return $cities;
			}
		}
	}

	?>