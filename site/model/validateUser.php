<?php
/**
 * Campaign Validation Class
 *
 * @package default
 * @author Murat
 **/
class ValidateUser extends Validate
{
	/**
	 * Campaign validation error codes
	 * @var array User        : 200-299
	 */
	public static $errorCodes = array(
		200 => "Params is not an array",
		88 =>  "Mandatory field is missing",
		201 => "password id not valid",
		202 => "user name taken",
		203 => "email name taken",
		204 => "user name bad format",
		205 => "email bad format",
		206 => "image file extension isn't allowed",
		207 => "image file size is too big",
		208 => "Birthdate is wrong",
		209 => "City is wrong",
		210 => "Birthdate is wrong",
		211 => "password id not valid",
		212 => "password is not the same as old",
		213 => "bad phone",
		214 => "email not found",
		215 => "wrong password",
		216 => "invalid lang",
		217 => "invalid Identity Number"
	);

	public static $errorFields = array(
		201=>"password",
		202=>"username",
		203=>"email",
		204=>"username",
		205=>"email",
		206=>"avatarUpload",
		207=>"avatarUpload",
		208=>"dob",
		209=>"country",
		210=>"city",
		211=>"newPassword",
		212=>"password",
		213=>"gsmNumber",
		214=>"forgotEmail",
		215=>"deaccpass",
		217 => "identityNumber",
	);

	public static $errorLangDefs = array(
		88=>"manErr",
		201=>"invalidPassErr",
		202=>"uniUsernameErr",
		203=>"uniEmailErr",
		204=>"invalidUsernameErr",
		205=>"wrongEmailFormatErr",
		206=>"invalidAvatarFileFormat",
		207=>"maxFileSizeExceed",
		208=>"manErr",
		209=>"manErr",
		210=>"manErr",
		211=>"invalidPassErr",
		212=>"wrongOldPassword",
		213=>"invalidPhoneNumber",
		214=>"emailNotFound",
		215=>"wrongOldPassword",
		217 => "invalidIdentityNumber"
	);

	/**
	 * Valid campaign races
	 *
	 * @var array
	 */
	public static $validPublisherTypes = array(Publisher::ENUM_TYPE_REGULAR,Publisher::ENUM_TYPE_TRENDSETTER,Publisher::ENUM_TYPE_CELEBRITY);

	/**
	 * Check Signup first step parameters
	 *
	 * @param $params
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateSignupStep1( $params , $is_mobile = false ) {
		global $__DP;

		require_once($__DP."site/country/".COUNTRY_CODE.".php");
		//Create validation response
		$r = new ValidationResponse();


		$defaultFields = array( "username", "password", "email", "name", "surname", "gsmCode", "gsmNumber" );
		if($is_mobile){
			$defaultFields = array( "username", "password", "email", "name", "surname");
		}
		//Define mandatory fields for params array
		$mFields = isset( $params[ "fields" ] ) ? $params[ "fields" ] : $defaultFields;


		#for advertisers.
		if(!isset($params["username"]))
			$params[ "username" ] = "";

		#new user
		if ( !isset( $params[ "id" ] ) )
			$params[ "id" ] = 0;

		//Check params is array and all mandatory fields were set
		self::mandatoryCheck( $params, $mFields, $r );

		//If $params is array and all mandatory fields were set, lets check the proper values
		if ( empty( $r->errorCode ) ) {
			if ( in_array( "password", $mFields ) && self::checkPassword( $params[ "password" ] )!==true ) {
				$r->errorCode = 201;
			}
			elseif ( !self::checkUsername( $params[ "username" ] ) ) {
				$r->errorCode = 204;
			}
			elseif ( !self::checkUniqueUserName( $params[ "username" ], isset($params["mbUserID"]) ? $params["mbUserID"] : $params[ "id" ] ) ) {
				$r->errorCode = 202;
			}
			elseif (isset($params[ "email" ]) && !self::validateEmail( $params[ "email" ] ) ) {
				$r->errorCode = 205;
			}

			if (isset($params[ "email" ])){
				if ((isset($params["mbUserID"]) && !self::checkUniqueEmail( $params[ "email" ],new User($params["mbUserID"]) )) || (!isset($params["mbUserID"]) && !self::checkUniqueEmail( $params[ "email" ] ))){
					$r->errorCode = 203;
				}
			}
			if(!$is_mobile){
				if ( isset( $params[ "gsmNumber" ] ) && strlen($params[ "gsmNumber" ]) && CountryValidate::validatePhone( $params[ "gsmNumber" ] ) !== true ) {
					$r->errorCode = 213;
				}

				if ( isset( $params[ "gsmCode" ] ) && strlen($params[ "gsmCode" ]) && self::validateCountryCode( $params[ "gsmCode" ] ) !== true ) {
					$r->errorCode = 213;
				}
			}
		}

		//If there's an error, let set valid false and give the errorDetail
		if ( !empty( $r->errorCode ) ) {
			$r->valid = false;
			if ( empty( $r->errorDetail ) ) $r->errorDetail = self::$errorCodes[ $r->errorCode ];
		}

		return self::response( $r );
	}


	public static function validateSignupStep2($params,$is_mobile=false){
		global $__DP;

		require_once($__DP."site/country/".COUNTRY_CODE.".php");
		//Create validation response
		$r = new ValidationResponse();

		$defaultFields = array("bDay","country","interests","gender");
		if($is_mobile){
			$defaultFields = array( "bDay","country","interests","gender", "gsm");
		}

		//Define mandatory fields for params array
		$mFields = isset($params["fields"]) ? $params[ "fields" ] : $defaultFields;

		//Check params is array and all mandatory fields were set
		self::mandatoryCheck($params,$mFields,$r);

		//If $params is array and all mandatory fields were set, lets check the proper values
		if (empty($r->errorCode)){
			if ( isset( $mFields[ "bDay" ] ) && !self::isValidDate( $params[ "bDay" ] ) ) {
				$r->errorCode = 208;
			}
			elseif ( !self::checkValidCountry( $params[ "country" ] ) ) {
				$r->errorCode = 209;
			}
			elseif (isset($params["city"]) && !self::checkValidCity( $params[ "country" ], $params[ "city" ] ) ) {
				$r->errorCode = 210;
			}

			if($is_mobile){
				if ( isset( $params["gsm"][ "gsmNumber" ] ) && strlen($params["gsm"][ "gsmNumber" ]) && CountryValidate::validatePhone( $params["gsm"][ "gsmNumber" ] ) !== true ) {
					$r->errorCode = 213;
				}

				if ( isset( $params["gsm"][ "gsmCode" ] ) && strlen($params["gsm"][ "gsmCode" ]) && self::validateCountryCode( $params["gsm"][ "gsmCode" ] ) !== true ) {
					$r->errorCode = 213;
				}
			}
		}

		//If there's an error, let set valid false and give the errorDetail
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * @param      $params
	 * @param User $user
	 * @author Özgür Köy
	 */
	public static function validatePublisherProfile( $params, User $user ) {
		global $__DP;

		require_once($__DP."site/country/".COUNTRY_CODE.".php");

		$params["bDay"] = $params[ "yob" ] . "/" . $params[ "mob" ] . "/" . $params[ "dob" ];

		$r = new ValidationResponse();
		if ( isset( $params[ "newPassword" ] ) && strlen( $params[ "newPassword" ] )>0 && self::checkPassword( $params[ "newPassword" ] ) !== true ) {
			$r->errorCode = 211;
		}
		elseif ( isset( $params[ "newPassword" ] ) && strlen( $params[ "password" ] )>0 && $user->pass != md5( $params[ "password" ] ) ) {
			$r->errorCode = 212;
		}
		elseif ( isset( $params[ "email" ] ) && !self::validateEmail( $params[ "email" ] ) ) {
			$r->errorCode = 205;
		}
		elseif ( isset( $params[ "email" ] ) && !self::checkUniqueEmail( $params[ "email" ], $user ) ) {
			$r->errorCode = 203;
		}
		elseif ( isset( $params[ "bDay" ] ) && !self::isValidDate( $params[ "bDay" ] ) ) {
			$r->errorCode = 208;
		}
		elseif ( !self::checkValidCountry( $params[ "country" ] ) ) {
			$r->errorCode = 209;
		}
		elseif ( isset( $params[ "city" ] ) && $params["city"]== COUNTRY_CODE && !self::checkValidCity( $params[ "country" ], $params[ "city" ] ) ) {
			$r->errorCode = 210;
		}
		elseif ( isset( $params[ "gsmNumber" ] ) && strlen($params[ "gsmNumber" ]) > 0 && CountryValidate::validatePhone( $params[ "gsmNumber" ] ) !== true ) {
			$r->errorCode = 213;
		}
		elseif ( isset( $params[ "gsmCode" ] ) && self::validateCountryCode( $params[ "gsmCode" ] ) !== true ) {
			$r->errorCode = 213;
		}


		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);


	}

	/**
	 * @param      $params
	 * @param User $user
	 * @author Özgür Köy
	 */
	public static function validateAdvertiserProfile( $params, User $user ) {
		global $__DP;

		require_once($__DP."site/country/".COUNTRY_CODE.".php");

		$r = new ValidationResponse();
		if ( isset( $params[ "newPassword" ] ) && strlen( $params[ "newPassword" ] )>0 && self::checkPassword( $params[ "newPassword" ] ) !== true ) {
			$r->errorCode = 211;
		}
		elseif ( isset( $params[ "newPassword" ] ) && strlen( $params[ "password" ] )>0 && $user->pass != md5( $params[ "password" ] ) ) {
			$r->errorCode = 212;
		}
		elseif ( isset( $params[ "email" ] ) && !self::validateEmail( $params[ "email" ] ) ) {
			$r->errorCode = 205;
		}
		elseif ( isset( $params[ "email" ] ) && !self::checkUniqueEmail( $params[ "email" ], $user ) ) {
			$r->errorCode = 203;
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);


	}


	/**
	 * @param      $params
	 * @param User $user
	 * @author Özgür Köy
	 */
	public static function validatePublisherDeactivate( $params, User $user ) {
		$r = new ValidationResponse();

		if ( isset( $params[ "deaccpass" ] ) && strlen( $params[ "deaccpass" ] )>0 && $user->pass != md5( $params[ "deaccpass" ] ) )
			$r->errorCode = 215;



		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);

	}

	/**
	 * Darkside advertiser validation
	 * @param            $params
	 * @param Advertiser $advertiser
	 * @return bool|ValidationResponse
	 * @author Özgür Köy
	 */
	public static function validateDSAdvertiser( $params, Advertiser &$advertiser ) {
		$params[ "fields" ] = array( "username");

		#either new or editing, if editing must be some value.
		if ( $advertiser->gotValue !== true || strlen($params["password"])>0  )
			$params[ "fields" ][ ] = "password";

		#editing, must be already loaded before this step
		if ( $advertiser->user_advertiser && $advertiser->user_advertiser->gotValue )
			$params[ "id" ] = $advertiser->user_advertiser->id;

		$step1 = self::validateSignupStep1( $params );

		if ( $step1 === true ) {
			$params[ "fields" ] = array( "country", "city" );
			$step2              = self::validateSignupStep2( $params );

			return $step2;
		}

		return $step1;
	}

	/**
	 * @param $email
	 * @return bool|ValidationResponse
	 * @author Özgür Köy
	 */
	public static function validateResetPassword( $params ) {
		$email = $params[ "forgotEmail" ];
		$r     = new ValidationResponse();
		$u     = new User();
		$u->load( array(
			"status"         => User::ENUM_STATUS_ACTIVE,
			"utype"          => User::ENUM_UTYPE_PUBLISHER,
			"user_userEmail" => array( "email" => "LIKE " . $email )
		) );
		if ( $u->gotValue !== true ) {
			$r->valid       = false;
			$r->errorCode   = 214;
			$r->errorDetail = self::$errorCodes[ $r->errorCode ];
		}

		return self::response( $r );
	}

	/**
	 * @param $params
	 * @return bool|ValidationResponse
	 * @author Özgür Köy
	 */
	public static function validateReplacePassword( $params ) {
		$r     = new ValidationResponse();

		if ( self::checkPassword( $params[ "password" ] )!==true ) {
			$r->valid       = false;
			$r->errorCode = 201;
		}

		return self::response( $r );
	}

	/**
	 * Validate image file
	 *
	 * @param $params
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateImageFile($params){
		//Create validation response
		$r = new ValidationResponse();

		if (!in_array(Tool::getFileExtension($params["name"]),explode(",",Setting::getValue("validImageTypes")))){
			$r->errorCode = 206;
		} elseif ($params["size"] > Setting::getValue("maxImageFileSize")){
			$r->errorCode = 207;
		}

		//If there's an error, let set valid false and give the errorDetail
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Validate image binary
	 *
	 * @param $params
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateImageBinary($params){
		//Create validation response
		$r = new ValidationResponse();

		$base64 = $params['image'];

		$fileType = null;

		if (substr($base64, 0, 5) == 'data:') {
			$base64 = preg_replace('#^data:image/[^;]+;base64,#', '', $base64);
			$base64 = base64_decode($base64);
			$fileType = self::getImageMimeType($base64);
		}
		else {
			$base64 = strtok($base64, '?');
			list($height, $width, $type) = getimagesize($base64);
			if ($type == 1){
				$fileType = "gif";
			}else if ($type == 2){
				$fileType = "jpg";
			}else if ($type == 3) {
				$fileType = "png";
			}
		}

		if (is_null($fileType) || !in_array($fileType,explode(",",Setting::getValue("validImageTypes"))))
			$r->errorCode = 206;

		//If there's an error, let set valid false and give the errorDetail
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	public static function validateCountryCode( $code ) {
		$c = new Country();
		$c->load( array( "countryCode" => $code ) );

		$r = $c->gotValue;
		unset( $c );
		return $r;
	}

	/**
	 * Validate contact form data
	 *
	 * @param $params
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateContactUs($params){
		//Create validation response
		$r = new ValidationResponse();

		//Define mandatory fields for params array
		$mFields = array("subject","email","message");

		//Check params is array and all mandatory fields were set
		self::mandatoryCheck($params,$mFields,$r);

		//If $params is array and all mandatory fields were set, lets check the proper values
		if (empty($r->errorCode)){
			if(!self::validateEmail($params["email"])){
				$r->errorCode = 205;
			}
		}

		//If there's an error, let set valid false and give the errorDetail
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}

	/**
	 * Validate lang
	 *
	 * @param $params
	 *
	 * @return bool|ValidationResponse
	 */
	public static function validateLang($params){
		//Create validation response
		$r = new ValidationResponse();

		//Define mandatory fields for params array
		$mFields = array("langId");

		//If $params is array and all mandatory fields were set, lets check the proper values
		if (empty($r->errorCode)){
			$l = new Lang($params["langId"]);
			if (!$l->gotValue){
				$r->errorCode = 216;
			}
		}

		//If there's an error, let set valid false and give the errorDetail
		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}



	/**
	 * Validate Identity Number
	 *
	 * @param $params
	 *
	 * @return bool|ValidationResponse
	 */
		public static function validateIdentity($params){
		global $__DP;

		require_once($__DP."site/country/".COUNTRY_CODE.".php");

		$r = new ValidationResponse();

		$mFields = array( "identityNumber" );

		self::mandatoryCheck( $params, $mFields, $r );

		//If $params is array and all mandatory fields were set, lets check the proper values
		if ( empty( $r->errorCode ) ) {
			if (!CountryValidate::validateIdentity($params["identityNumber"])){
				$r->errorCode = 217;
			}
		}

		if (!empty($r->errorCode)){
			$r->valid = false;
			if (empty($r->errorDetail)) $r->errorDetail = self::$errorCodes[$r->errorCode];
		}

		return self::response($r);
	}
}
