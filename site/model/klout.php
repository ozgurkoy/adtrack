<?php
/**
 * klout class
 *
 * @package default
 * @author Özgür Köy
 **/
class Klout
{
	/**
	 * klout object
	 *
	 * @var string
	 **/
	public static $kobj = NULL;

	/**
	 * score
	 *
	 * @var int
	 **/
	public static $score = 0;

	/**
	 * true reach value
	 *
	 * @var int
	 **/
	public static $trueReach=0;

	/**
	 * network score value
	 *
	 * @var int
	 **/
	public static $network=0;

	/**
	 * amplification value
	 *
	 * @var float
	 **/
	public static $amplification=0;

	/**
	 * ???
	 *
	 * @var string
	 **/
	public static $slope='';

	/**
	 * user classification value
	 *
	 * @var string
	 **/
	public static $kclass='';

	/**
	 * processed response error code
	 * 0: no error
	 * 1: api returned error
	 * 2: could not parse response
	 * 3: no response
	 * @var int
	 **/
	public static $errorCode=0;

	/**
	 * min klout score
	 *
	 * @var float
	 **/
	public static $minKloutScore = 10;

	/**
	 * load by webpage
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function scoreByPage($user)
	{
		/*
		<div id="score">


		    <div class="large-flag kscore ">


		                <img class="kflag" src="images/stroke-flag.png">

		        <span class="value">10</span>
		        <div class="hovercard"></div>
		    </div>

        </div>
		*/
	 	$url = "http://klout.com/$user";

		$xdoc = JUtil::fetchPage($url);
		if(strlen($xdoc)<=150) return self::$minKloutScore;
		//check if it s a redirect
		$red = preg_match('%<meta.+?url.+?content="(.+?)"/>%', $xdoc, $ps);

		$url = trim($ps[1]);
		if($url=="http://klout.com") return self::$minKloutScore;

		$mat = preg_match_all('#<div id="score".+?>.*?<span.+?>(.+?)</span>#is', $xdoc,$ps);
		$score = intval(trim($ps[1][0]));

		if($score>0 || $score<=100) {
			return $score;
		} else {
			JLog::log("cri","KLOUT-WEB-PAGE-ERROR");
			return -1;
		}
	}

	/**
	 * get klout data for requested twitter username
	 *
	 * @return void
	 * @author tiger
	 **/
	public static function loadFromAPI($username, $debugLevel=0)
	{
		self::$errorCode		= 0;
		self::$kobj 			= null;
		self::$score 			= 0;
		self::$trueReach 		= 0;
		self::$network		 	= 0;
		self::$amplification 	= 0;
		self::$slope 			= '';
		self::$kclass 			= '';

		$apiUrl	 		= 'http://api.klout.com/1/users/show.json?users=' . $username . '&key=' . KLOUT_APIKEY;
		$logK = Klout::logRequest($apiUrl);

		$apiResponse 	= JUtil::fetchPage($apiUrl);
		Klout::logResponse($logK, $apiResponse);

		if($debugLevel > 0) {
			//echo 'API URL:'.$apiUrl.PHP_EOL;
			//echo 'Response string:'.$apiResponse.PHP_EOL;
		}

		if(strlen($apiResponse) > 1) {
			self::$kobj = json_decode($apiResponse);

			if($debugLevel > 1) {
				echo 'Response object:'.PHP_EOL;
				print_r(self::$kobj);
				echo PHP_EOL;
			}

			if(is_object(self::$kobj) && self::$kobj->status==200) {
				if(isset(self::$kobj->body->error)) {
					if($debugLevel > 1) JLog::log("api","Klout Api Fail; error returned. Details:".serialize($apiUrl).$apiResponse);
					self::$errorCode = 1;
					return FALSE;
				} else {
					self::$errorCode		= 0;
					self::$score 			= self::$kobj->users[0]->score->kscore;
					self::$trueReach 		= self::$kobj->users[0]->score->true_reach;
					self::$network		 	= self::$kobj->users[0]->score->network_score;
					self::$amplification 	= self::$kobj->users[0]->score->amplification_score;
					self::$slope 			= self::$kobj->users[0]->score->slope;
					self::$kclass 			= self::$kobj->users[0]->score->kclass;
					return TRUE;
				}
			} else {
				if($debugLevel > 1) JLog::log("api","Klout Api Fail; could not parse result. Details:".serialize($apiUrl).$apiResponse);
				self::$errorCode = 2;
				return FALSE;
			}
		} else {
			if($debugLevel > 1) JLog::log("api","Klout Api Fail; no response. Details:".serialize($apiUrl));
			self::$errorCode = 3;
			return FALSE;
		}
	}

	/**
	 * get klout (v2 api) data for requested twitter username
	 *
	 * @return void
	 * @author murat
	 **/
	public static function loadFromAPIV2($username, $debugLevel=0)
	{
		self::$errorCode		= 0;
		self::$kobj 			= null;
		self::$score 			= 0;
		self::$trueReach 		= 0;
		self::$network		 	= 0;
		self::$amplification 	= 0;
		self::$slope 			= '';
		self::$kclass 			= '';

		$apiUrl	 		= 'http://api.klout.com/v2/identity.json/twitter?screenName=' . $username . '&key=' . KLOUT_APIKEY_V2;
		if($debugLevel > 0) {
			echo 'Requesting from:'.$apiUrl.PHP_EOL;
		}
		$logK = Klout::logRequest($apiUrl);

		$apiResponse 	= JUtil::fetchPage($apiUrl);
		Klout::logResponse($logK, $apiResponse);

		if($debugLevel > 0) {
			echo 'API URL:'.$apiUrl.PHP_EOL;
			echo 'Response string:'.$apiResponse.PHP_EOL;
		}

		if(strlen($apiResponse) > 1) {
			self::$kobj = json_decode($apiResponse);

			if($debugLevel > 1) {
				echo 'Response object:'.PHP_EOL;
				print_r(self::$kobj);
				echo PHP_EOL;
			}

			if(is_object(self::$kobj)) {
				if(isset(self::$kobj->body->error)) {
					JLog::log("api","Klout Api Fail; error returned. Details:".serialize($apiUrl).$apiResponse);
					self::$errorCode = 1;
					return FALSE;
				} else {
					$apiUrl	= 'http://api.klout.com/v2/user.json/' . self::$kobj->id . '/influence?key=' . KLOUT_APIKEY_V2;
					$logK = Klout::logRequest($apiUrl);

					$apiResponse 	= JUtil::fetchPage($apiUrl);
					Klout::logResponse($logK, $apiResponse);

					if($debugLevel > 0) {
						echo 'API URL:'.$apiUrl.PHP_EOL;
						echo 'Response string:'.$apiResponse.PHP_EOL;
					}

					if(strlen($apiResponse) > 1) {
						self::$kobj = json_decode($apiResponse);

						if($debugLevel > 1) {
							echo 'Response object:'.PHP_EOL;
							print_r(self::$kobj);
							echo PHP_EOL;
						}

						if(is_object(self::$kobj)) {
							if(isset(self::$kobj->body->error)) {
								JLog::log("api","Klout Api Fail; error returned. Details:".serialize($apiUrl).$apiResponse);
								self::$errorCode = 1;
								return FALSE;
							} else {

								self::$errorCode		= 0;
								self::$score 			= null;
								self::$trueReach 		= self::$kobj->myInfluenceesCount;
								self::$network		 	= null;
								self::$amplification 	= null;
								self::$slope 			= null;
								self::$kclass 			= null;
								return TRUE;
							}
						} else {
							JLog::log("cri","Klout Api Fail; could not parse result. Details:".serialize($apiUrl).$apiResponse);
							self::$errorCode = 2;
							return FALSE;
						}
					}
				}
			} else {
				JLog::log("cri","Klout Api Fail; could not parse result. Details:".serialize($apiUrl).$apiResponse);
				self::$errorCode = 2;
				return FALSE;
			}
		} else {
			JLog::log("cri","Klout Api Fail; no response. Details:".serialize($apiUrl));
			self::$errorCode = 3;
			return FALSE;
		}
	}

	/**
	 *	updates given userid's klout data from API
	 *
	 * @param integer $userId
	 * @param integer $debugLevel
	 * @author tiger
	 */
	public static function updateUserKlout($userId, $debugLevel=0) {
		$user = new User($userId);

		$userUpdate = new UserUpdate();
		$userUpdate->customClause = 'user='.$user->id;
		$userUpdate->load();

		$userUpdate->kloutStatus = 'q';
		$userUpdate->kloutUpdateDate = time();
		$userUpdate->save();

		$twitterInfo = new TwitterInfo();
		$twitterInfo->customClause = 'user='.$user->id;
		$twitterInfo->load();

		if($debugLevel > 0) {
			Tool::echoLog('userId:'.$userId.' username:'.$user->username);
		}

		if(self::loadFromAPI($twitterInfo->screenname, $debugLevel)) {

			if($debugLevel > 0) {
				Tool::echoLog($twitterInfo->screenname.' load from API');
			}

			//update twitter klout data
			$twitterInfoKlout = new TwitterInfoKlout();
			$twitterInfoKlout->customClause = 'twitterInfo='.$twitterInfo->id;
			$twitterInfoKlout->load();
			if($twitterInfoKlout->count == 0) {
				$twitterInfoKlout->twitterInfo = $twitterInfo->id;
				$twitterInfoKlout->save();
				if($debugLevel > 0) {
					Tool::echoLog('Added twitterInfoKlout record');
				}
			}

			if($debugLevel > 1) {
				print_r(self::$kobj);
			}
			$twitterInfoKlout->score = self::$score;
			$twitterInfoKlout->trueReach = self::$trueReach;
			$twitterInfoKlout->network = self::$network;
			$twitterInfoKlout->amplification = self::$amplification;
			$twitterInfoKlout->slope = self::$slope;
			$twitterInfoKlout->kclass = self::$kclass;
			$twitterInfoKlout->save();

			//insert twitter klout history
			$history = new KloutHistory();
			$history->score = self::$score;
			$history->trueReach = self::$trueReach;
			$history->network = self::$network;
			$history->amplification = self::$amplification;
			$history->slope = self::$slope;
			$history->kclass = self::$kclass;
			$history->twitterInfoKlout = $twitterInfoKlout->id;
			$history->user = $twitterInfo->user;
			$history->save();

			//update userUpdate : marks as user updated mükemmel bir şekilde :P
			$userUpdate->kloutStatus = 'y';
			$userUpdate->kloutTryCount = 0;
			$userUpdate->save();
			return TRUE;

		} else {

			if($debugLevel > 0) {
				Tool::echoLog($twitterInfo->screenname.' could not load from API. Error Code: ' . self::$errorCode);
			}

			//update userUpdate : marks as user update failed
			if(self::$errorCode == 1) { // probably no user or processing user or something like so come tomorrow yeğen :D
				$userUpdate->kloutStatus = 'et';
			} elseif(Klout::$errorCode == 2) { // service response error
				$userUpdate->kloutStatus = 'ee';
			} elseif(Klout::$errorCode == 3) { // service down
				$userUpdate->kloutStatus = 'ed';
			} else { // unregistered errors
				$userUpdate->kloutStatus = 'e';
			}

			$userUpdate->kloutTryCount++;
			$userUpdate->save();
			return FALSE;
		}
	}

	/**
	 * Logs request made to klout
	 *
	 * @param string $rUrl
	 * @return LogKlout
	 */
	public static function logRequest($rUrl) {
		$logK = new LogKlout();
		$logK->requestDate = time();
		$logK->request = $rUrl;
		$logK->save();
		return $logK;
	}

	/**
	 * Logs response received from klout
	 *
	 * @param LogKlout $logK
	 * @param type $response
	 */
	public static function logResponse(LogKlout $logK, $response) {
		$logK->response = serialize($response);
		$logK->responseDate = time();
		$logK->save();
	}

} // END class Klout
