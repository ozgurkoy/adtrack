<?php
/**
 * DSNotificationEmails real class - firstly generated on 23-05-2014 19:29, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/DSNotificationEmails.php';

class DSNotificationEmails extends DSNotificationEmails_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function getAll() {
		$e = new DSNotificationEmails();
		$e->populateOnce( false )->nopop()->load();
		$ret = array();
		while($e->populate()){
			$ret[ ] = array($e->email,$e->name);
		}

		unset( $e );

		return $ret;
	}
}

