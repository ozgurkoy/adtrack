<?php
/**
 * snTwitter real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/snTwitter.php';
require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

class SnTwitter extends SnTwitter_base
{
	/**
	 * user info retrieved from twitter api
	 * @var null
	 */
	public $twitterInfo = null;

	/**
	 * filter to check credentials for once.
	 * @var bool
	 */
	public $credentialsVerified = false;

	/**
	 * verified tokens from api
	 * @var null
	 */
	public $verifiedTokens = null;

	/**
	 * Update error code
	 *
	 * @var int
	 */
	public $updateError;

	public $savedId = null;

	public $savedUsername = null;

	public $savedAvatar = false;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Reset error status of twitter user
	 *
	 * @return void
	 * @author Murat
	 */
	public function resetErrorStatus($save=false){
		$this->errorStatus = $this->follower > 0 ? null : "NoFollower";
		$this->unAuthotizeDate = null;
		if ($save) $this->save();
	}

	public static function addTwitterAccount($oauthToken,$oauthTokenSecret){
		$snT = new SnTwitter();
		return $snT->addAccount( $oauthToken, $oauthTokenSecret );
	}

	/**
	 * modified from static version , we need object properties.
	 * @param $oauthToken
	 * @param $oauthTokenSecret
	 * @return SnTwitter|bool
	 * @author Özgür Köy
	 */
	public function addAccount($oauthToken,$oauthTokenSecret) {

		$userData = $this->verify_credentials($oauthToken, $oauthTokenSecret);

		if (is_bool($userData)) {
			return false;
		}
		else {

			$this->load(array("twid"=>$userData->id_str));

			//save user
			if($this->gotValue && !is_null($this->publisher_snTwitter)) {
				return false;
			}

			// new user
			$this->twid = $userData->id_str;
			$this->customCost = 0;
			$this->cost = 0;
			$this->minCost = 0;
			$this->maxCost = 0;
			$this->originalCost = 0;
			//update twitter details
			$this->twKey = $oauthToken;
			$this->twKeySecret = $oauthTokenSecret;
			$this->updateUserData($userData);
			$this->resetErrorStatus();

			return $this;
		}
	}


	public static function loginTwitterAccount($twitterId, $oauthToken, $oauthTokenSecret){
		$snT = new SnTwitter();
		return $snT->login($twitterId, $oauthToken, $oauthTokenSecret);
	}

	/**
	 * modified from static version , we need object properties.
	 * @param $twitterId
	 * @param $oauthToken
	 * @param $oauthTokenSecret
	 * @return SnTwitter|bool
	 * @author Özgür Köy - modified
	 */
	public function login($twitterId, $oauthToken, $oauthTokenSecret, $fromMobile = false){
		$userData = $this->verify_credentials($oauthToken, $oauthTokenSecret);

		if ( is_bool( $userData ) ) {
			return false;
		}
		else {
			$this->load( array( "twid" => $userData->id_str ) );

			if ($fromMobile) $avatar = SnTwitter::getAvatarImage( $this->savedAvatar );

			//save user
			if ( !$this->gotValue ) {
				// new user
				$this->twid         = $userData->id_str;
				$this->customCost   = 0;
				$this->cost         = 0;
				$this->minCost      = 0;
				$this->maxCost      = 0;
				$this->originalCost = 0;
			}
			else{
				$this->loadPublisher();
				if ( $this->publisher_snTwitter && $this->publisher_snTwitter->gotValue ) {
					$this->publisher_snTwitter
						->writeHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "login by twitter", UserHistory::ENUM_MADEBY_USER )
						->loadUser()
						->login( null, null, $this->publisher_snTwitter->user_publisher->id );
				}
				else {
					$this->updateUserData( $userData );

					if ($fromMobile){
						$_SESSION[ "assignSN" ][ "twitter" ] = array( $this->savedId, $this->savedUsername, $avatar );
					}

					return false;
				}
			}


			if ($fromMobile){
				$_SESSION[ "assignSN" ][ "twitter" ] = array( null, null, $avatar );
			}

			//update twitter details
			$this->twKey       = $oauthToken;
			$this->twKeySecret = $oauthTokenSecret;
			$this->updateUserData( $userData );
			$this->resetErrorStatus();

			return $this;
		}
	}


	/**
	 * @param $token
	 * @param $tokenSecret
	 * @return API|bool|mixed
	 */
	public function verify_credentials( $token, $tokenSecret, $verifierToken=null ) {
		//$logT = self::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $token, $tokenSecret, "Validate");
		if ( $this->credentialsVerified === true )
			return $this->twitterInfo;

		$connection = new TwitterOAuth( Setting::getValue( "CONSUMER_KEY" ), Setting::getValue( "CONSUMER_SECRET" ), $token, $tokenSecret );
//		var_dump( $connection );
		//self::logResponse($logT, $connection);
		if ( !is_null( $verifierToken ) )
			$this->verifiedTokens = $connection->getAccessToken( $_GET[ "oauth_verifier" ] );


		$data = $connection->get( 'account/verify_credentials' );

		$connectionState = $this->checkConnection( $connection );
		if ( is_bool( $connectionState ) ) {
			if (!is_object($this->publisher_snTwitter)){
				$this->loadPublisher();
			}
			if(isset($data->error)) {
				if(strpos($data->error,"Not found") !== false || strpos($data->error,"Could not authenticate") !== false) {
					//user deleted/deactivated
					$this->updateError = $data->error;
					if (!is_object($this->publisher_snTwitter)) $this->publisher_snTwitter->unauthorizedFromTwitter();
					return false;
				} else {
					$this->updateError = $data->error;
					return false;
				}
			} elseif(isset($data->errors)) {
				if (isset($data->errors[0]->message) && strpos($data->errors[0]->message,"Invalid or expired token") !== false){
					$this->updateError = $data->errors[0]->message;
					if (!is_object($this->publisher_snTwitter)) $this->publisher_snTwitter->unauthorizedFromTwitter();
					return false;
				} else {
					$this->updateError = $data->errors[0]->message;
					return false;
				}

			} else {
				// return user Data
				$this->credentialsVerified = true;
				$this->twitterInfo         = $data;

				return $data;
			}
		}
		else {
			// Error on connection
			return false;
		}
	}

	/**
	 * Check if the connection is good or have error
	 * @param $connection
	 * @return bool|string
	 */
	private function checkConnection(&$connection){

		switch ($connection->http_code) {
			case 200:
				$good = true;
				break;
			case 401:
				//JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
				$error = 'Twitter Error 401 - Too Many Requests';
				$good = true;
				break;
			case 429:
				//JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
				$error = 'Twitter Error 429 - Too Many Requests';
				$good = false;
				break;
			case 500:
				//JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
				$good = false;
				$error='Twitter Error 500 - Internal Server Error';
				break;
			case 502:
				//JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
				$good = false;
				$error='Twitter Error 502 - Bad Gateway, Twitter is down or being upgraded.';
				break;
			case 503:
				//JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
				$good = false;
				$error='Twitter Error 503 - Service Unavailable, The Twitter servers are up, but overloaded with requests. Try again later';
				break;
			case 504:
				//JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
				$good = false;
				$error='Twitter Error 504 - Gateway timeout, The Twitter servers are up, but the request could not be serviced due to some failure within our stack. Try again later.';
				break;
			default:
				/* Show notification if something went wrong. */
				//JLog::log("cri","Twitter api error: SERVER: \n" . serialize($_SERVER) . "\n Connection: \n" . serialize($connection));
				$good = false;
				$error='Twitter Error , Please contact to site Administrator ';
		}
		
		if($good)
			return true;

		$_SESSION['twitterApiError'] = 1;
		return $error;

	}

	/**
	 * Updates local properties from successful response of twitter account/verify_credentials request
	 *
	 * @param mixed $refill
	 * @author Murat
	 */
	public function updateUserData($refill) {
		//defaults

		//twitter api response to local properties
		$this->timg		        = str_replace("_normal.", ".", $refill->profile_image_url_https);
		$this->name			    = $refill->name;
		$this->screenname		= $refill->screen_name;
		$this->location			= $refill->location;
		$this->following	    = $refill->friends_count;
		$this->follower	        = $refill->followers_count;
		$this->statusCount		= $refill->statuses_count;
		$this->protected		= strlen($refill->protected) ? 1 : 0;
		$this->language			= $refill->lang;
		$this->url				= $refill->url;
		$this->verified			= strlen($refill->verified) ? 1 : 0;
		$this->description		= $refill->description;
		$this->listedCount		= $refill->listed_count;
		$this->lastUpdate       = time();
		if ($this->follower == 0){
			$this->errorStatus = "NoFollower";
		}
		$this->save();

		$this->savedId = $this->id;
		$this->savedUsername = $this->screenname;
		$this->savedAvatar = $this->timg;

		//insert into twitter history
		SnTwitterHistory::copyFromSnTwitter($this);


	}


	/**
	 * get avatar image
	 * @param $path
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function getAvatarImage( $path ) {
		if(!(strlen($path)>10))
			return false;
		$t0 = file_get_contents( $path );
		if(!(strlen($t0)>500))
			return false;
		$filename = JUtil::randomString(10);
		$temp = tempnam(sys_get_temp_dir(), $filename );
		$handle = fopen($temp, "w");

		fwrite( $handle, $t0 );

		rename( $temp, $temp . ".jpg" );

		return $temp . ".jpg";
	}

	/**
	 * get login url for twitter.
	 * @return string
	 * @author Özgür Köy
	 */
	public function getLoginUrl() {
		return "/site/service/twitterRE.php";
	}

	/**
	 * callback check with oauth parameter
	 * @param $oauthToken
	 * @return bool
	 * @author Özgür Köy
	 */
	public function callback( $oauthToken, $verifierToken ) {
		if ( !isset( $_SESSION[ "oauth_token" ] ) || !isset( $_SESSION[ "oauth_token_secret" ] ) )
			return false;

		if ( !isset( $_SESSION[ 'oauth_token' ] ) || ( $_SESSION[ 'oauth_token' ] !== $oauthToken ) ) {
			JLog::log( "sec", "Twitter auth tokens dont match. SERVER: \n" . serialize( $_SERVER ) . "\n SESSION: \n" . serialize( $_SESSION ) );

			return false;
		}

		#we made it so far, we can proceed.
		$tww = $this->verify_credentials( $_SESSION[ "oauth_token" ], $_SESSION[ "oauth_token_secret" ], $verifierToken );

		if ( ( $tww ) !== false )
			return true;
		else
			return false;
	}

	/**
	 * get auth url
	 * @return a|bool
	 * @author Özgür Köy
	 */
	public function getAuthUrl() {
		$connection                       = new TwitterOAuth( Setting::getValue( "CONSUMER_KEY" ), Setting::getValue( "CONSUMER_SECRET" ) );
		$request_token                    = $connection->getRequestToken( Setting::getValue( "OAUTH_CALLBACK" ) );


		$connectionState = $this->checkConnection( $connection );

		if ( is_bool( $connectionState ) ) {
			//Error 89 - Invalid or expired token
			if ( isset( $request_token->errors ) ) {
				JLog::log( "twi", "cant get login url" . serialize( $request_token->errors ) );

				return false;
			}
			else {
				$_SESSION[ "oauth_token" ]        = $token = $request_token[ "oauth_token" ];
				$_SESSION[ "oauth_token_secret" ] = $request_token[ "oauth_token_secret" ];

				// return user Data
				$token = $request_token[ 'oauth_token' ];

				return $connection->getAuthorizeURL( $token );
			}
		}
		else {
			// Error on connection
			return false;
		}
	}

	/**
	 * Update Twitter account data
	 *
	 * @return bool
	 * @author Murat
	 */
	public function updateAccount(){
		$data = $this->verify_credentials($this->twKey,$this->twKeySecret);
		if (is_bool($data)){
			return false;
		} else {
			$this->updateUserData($data);
			if (!is_object($this->publisher_snTwitter)) $this->loadPublisher();
			if (is_object($this->publisher_snTwitter)){
				$this->publisher_snTwitter->writeHistory(
					UserHistory::ENUM_ACTIONID_TWITTER_UPDATE,
					"Publisher twitter information updated",
					UserHistory::ENUM_MADEBY_SYSTEM,
					$this->publisher_snTwitter->id
				);
			}
			return true;
		}
	}

	/**
	 * Update request for TrueReach
	 *
	 * @return bool
	 * @author Murat
	 */
	public function updateTrueReach(){
		global $debugLevel;
		$result = false;
		if ($debugLevel) Tool::echoLog("pushing " . $this->screenname . " to Calculation API with minCost: " . $this->minCost . " minCost: " . $this->maxCost);
		$request = new RestReq(Setting::getValue("CALCULATION_API_URL"), 'POST', array(
			"data"=>  json_encode(array(
				"screen_name"=>$this->screenname,
				"consumer_key"=>$this->twKey,
				"consumer_secret"=>$this->twKeySecret,
				"app_secret"=>Setting::getValue("CONSUMER_KEY"),
				"app_secret_key"=>Setting::getValue("CONSUMER_SECRET"),
				"environment_code"=>ENVIRONMENT_CODE,
				"push_address"=>Jutil::siteAddress() . "wsv2/hook/updatePublisherScore/" . md5($this->id . "-" . $this->twid),
				"minCost"=>$this->minCost,
				"maxCost"=>$this->maxCost
			))
		),"json");
		$request->execute();
		$response = json_decode($request->responseBody);
		if (is_object($response)){
			$result = true;
			if (!is_object($this->publisher_snTwitter)) $this->loadPublisher();
			if (is_object($this->publisher_snTwitter)){
				$this->publisher_snTwitter->writeHistory(
					UserHistory::ENUM_ACTIONID_TRUEREACH_UPDATE,
					"Publisher TrueReach score updated",
					UserHistory::ENUM_MADEBY_SYSTEM,
					$this->publisher_snTwitter->id
				);
			}
			if ($debugLevel) Tool::echoLog("push success for: " . $this->screenname);
		} else {
			if ($debugLevel) Tool::echoLog("push error for: " . $this->screenname . " Calculation API Response: " . $request->responseBody);
		}
		unset($request);
		return $result;
	}

	/**
	 * Update publisher TrueReach and cost data
	 *
	 * @param object $scoreData
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public function updateScoreData($scoreData){
		$multiplier = Setting::getValue("COUNTRY_COST_MULTIPLIER");
		//$this->cost = $scoreData->cost * ($scoreData->cost > Setting::getValue("COUNTRY_MIN_PUBLISHER_COST") ? $multiplier : 1);
		$this->cost = $scoreData->cost * $multiplier;
		if ($scoreData->cost < Setting::getValue("COUNTRY_MIN_PUBLISHER_COST"))
			$this->cost = Setting::getValue("COUNTRY_MIN_PUBLISHER_COST");
		$this->originalCost = $scoreData->originalCost;
		if (!is_null($this->customCost) && $this->customCost > 0){
			$this->trueReach = intval($this->customCost / 0.002);
		} else {
			if (COUNTRY_CODE == 213){
				$calculatedTrueReach = intval(($this->originalCost > $this->cost ? $this->originalCost : $this->cost) / 0.002);
				$this->trueReach = ($calculatedTrueReach > $scoreData->trueReach ? $calculatedTrueReach : $scoreData->trueReach);
				//if ($this->cost > 1.008 && $this->cost >= $this->originalCost && $multiplier < 1){
				//	$this->trueReach = $this->trueReach / (1-$multiplier);
				//}
			} else {
				$this->trueReach = $scoreData->trueReach;
			}
		}
		$this->save();
		$this->loadPublisher();
		if ($this->publisher_snTwitter->gotValue){
			$this->publisher_snTwitter->loadPublisher_publisherUpdate();
			if ($this->publisher_snTwitter->publisher_publisherUpdate->gotValue){
				$this->publisher_snTwitter->publisher_publisherUpdate->calculateStatus = PublisherUpdate::ENUM_CALCULATESTATUS_DONE;
				$this->publisher_snTwitter->publisher_publisherUpdate->calculateDate = time();
				$this->publisher_snTwitter->publisher_publisherUpdate->save();
			}
		}
	}

}

