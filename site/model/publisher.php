<?php
/**
 * publisher real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisher.php';

class Publisher extends Publisher_base
{


	/**
	 * campaign approve email
	 *
	 * @var string
	 **/
	private $caEmail="approver@admingle.com";

	/**
	 * no twitter user found error string
	 *
	 * @var string
	 **/
	public static $errorTwitterNotFound = "errorTwitterNotFound";

	/**
	 * twitter authorization revoked error string
	 *
	 * @var string
	 **/
	public static $errorTwitterCouldNotAuth = "errorTwitterCouldNotAuth";

	/**
	 * @var array
	 */
	public $loginUrls = array();

	/**
	 * const for loading social networks
	 */
	const GET_SOCIAL_AUTH_URLS = true;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Get Publisher Full Name
	 * @return string
	 */
	public function getFullName()
	{
		return $this->name. ' ' .$this->surname;
	}

	public static function signUp( $userData ) {
		$publisher              = new Publisher();
		if (isset($userData[ "mbPublisher" ])) $publisher = $userData[ "mbPublisher" ];
		$publisher->name        = substr($userData["name"],0,255);
 		$publisher->surname     = substr($userData["surname"],0,255);
		$publisher->type        = self::ENUM_TYPE_REGULAR;
		$publisher->hasFacebook = 0;
		$publisher->hasTwitter  = 0;
		$publisher->hasGPlus    = 0;

		if ( isset( $userData[ "gsm" ]) || isset( $userData[ "gsmNumber" ] ) ) {
			$publisher->gsmNumber     = $userData[ "gsmNumber" ];
			$publisher->gsmNumberCode = $userData[ "gsmCode" ];
		}

//		JPDO::$debugQuery = true;
		$publisher->save();

		// Check For Club
		if(isset($_SESSION[ "regClubId" ])){
			$publisher->saveClubs($_SESSION["regClubId"]);
			$publisher->ownerClub = $_SESSION["regClubId"];
			$publisher->save();
			unset($_SESSION["regClubId"]);
		}

//		JPDO::$debugQuery = false;
		return $publisher;
	}

	/**
	 * @return bool
	 * @author Özgür Köy - Modified
	 */
	public function isRegistrationComplete() {
		if ( !is_object( $this->user_publisher ) )
			$this->loadUser();

		return in_array( $this->user_publisher->status, array( User::ENUM_STATUS_NOT_CONFIRMED, User::ENUM_STATUS_ACTIVE ) );
	}


	/**
	 * @param $data
	 * @return $this
	 * @author Özgür Köy - modified
	 */
	public function updateProfile( $data, $snAssignment = null ) {
		JPDO::beginTransaction();

		try {
			#country validation fix
			if ( !is_null( $data ) ) {
				if ( isset( $data[ "gsmNumber" ] ) )
					$data[ "gsm" ] = array( "gsmNumber" => $data[ "gsmNumber" ], "gsmCode" => $data[ "gsmCode" ] );

				if ( isset( $data[ "gsm" ] ) ) {
					$this->gsmNumber     = $data[ "gsm" ][ "gsmNumber" ];
					$this->gsmNumberCode = $data[ "gsm" ][ "gsmCode" ];
				}

				if ( isset( $data[ "bDay" ] ) )
					$this->bday = strtotime( $data[ "bDay" ] );

				if ( isset( $data[ "country" ] ) )
					$this->saveCountry( $data[ "country" ] );

				if ( isset( $data[ "city" ] ) && strlen( $data[ "city" ] ) > 0 )
					$this->saveCity( $data[ "city" ] );

				if ( isset( $data[ "professional" ] ) )
					$this->saveProfessional( new Professional( $data[ "professional" ] ) );

				if ( isset( $data[ "education" ] ) )
					$this->saveEducation( new Education( $data[ "education" ] ) );

				if ( isset( $data[ "address" ] ) )
					$this->address = $data[ "address" ];

				if ( isset( $data[ "interests" ] ) ) {
					$this->deleteAllTrends();
					$this->saveTrends( $data[ "interests" ] );
				}

				if ( isset( $data[ "gender" ] ) )
					$this->gender = $data[ "gender" ];

				if ( isset( $data[ "otherInfo" ] ) )
					$this->otherInfo = $data[ "otherInfo" ];

				if ( isset( $data[ "name" ] ) )
					$this->name = $data[ "name" ];

				if ( isset( $data[ "tags" ] ) )
					$this->tags = $data[ "tags" ];

				if ( isset( $data[ "email" ] ) ) {
					$oldEmail        = $this->user_publisher->getDefaultEmail();
					$oldEmail->email = $data[ "email" ];
					if ( isset( $data[ "unsubscribed" ] ) ) {
						$oldEmail->unsubscribed = $data[ "unsubscribed" ];
					}
					$oldEmail->save();
				}

				if ( isset( $data[ "surname" ] ) )
					$this->surname = $data[ "surname" ];

				if ( isset( $data[ "newPassword" ] ) && strlen( $data[ "newPassword" ] ) > 0 ) {
					if ( !is_object( $this->user_publisher ) )
						$this->loadUser();

					$this->user_publisher->pass = md5( $data[ "newPassword" ] );
					$this->user_publisher->save();
				}
				$this->save();
			}

			if ( !is_null( $snAssignment ) ) {
				foreach ( $snAssignment as $sn => $conds ) {
					$cl = "Sn" . ucfirst( $sn );
					$fn = "add" . $cl;
					if ( $conds[ 0 ] !== false )
						call_user_func_array( array( $this, $fn ), array( new $cl( $conds[ 0 ] ) ) );
					if ( $conds[ 2 ] !== false && !( strlen( $this->profileImage ) > 0 ) )
						$this->changeImage( 1, array( "name" => $conds[ 2 ] ) );

				}
			}
			if(!is_object($this->user_publisher))
				$this->loadUser();

			$this->user_publisher->fillSession();

			JPDO::commit();
		}
		catch ( JError $j ) {
			JPDO::rollback();

			return false;
		}

		return $this;
	}


		/**
	 * Added Event : Social Network Add
	 *
	 * @return void
	 * @author Murat
	 */
	public function addedSNA(){
		$this->hasTwitter = 1;
		$this->save();

		$this->fillSession();

		$sql = "UPDATE campaignUser cu, campaign c SET cu.hasTwitter = 1 WHERE cu.campaign=c.id AND cu.user = {$this->id} AND cu.state = 'w' AND c.endDate >= '" . date("Y-m-d") . "'";
		JPDO::connect(JCache::read('JDSN.MDB'));
		JPDO::executeQuery($sql);
	}

	/**
	 * Deactivate user, if he/she revoke adMingle access to his/her twitter account
	 * @return void|string
	 * @author Murat
	 */
	public function unauthorizedFromTwitter($withoutWarning = false, $approve = true) {
		global $debugLevel;
		$this->loadUser();
		if (!$approve){
			$ctm = new CampaignTwitterMessage();
			$ctm->load(array(
				"publisher_campaignTwitterMessage" => $this->id,
				"state" => array(CampaignTwitterMessage::ENUM_STATE_WAITING,CampaignTwitterMessage::ENUM_STATE_TWEET_SENT)
			));
			if ($ctm->gotValue){
				return "activeCampaignFound";
			}
		}
		if ($debugLevel>0) Tool::echoLog("Starting deactivate user: " . $this->user_publisher->username);


		JPDO::beginTransaction();

		$cp = new CampaignPublisher();
		$cp->update(array(
			"publisher_campaignPublisher"=>$this->id,
			"state"=>array(CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION,CampaignPublisher::ENUM_STATE_WAITING_ADMIN_APPROVAL)
		),array(
			"state"=>CampaignPublisher::ENUM_STATE_REJECTED,
			"actionDate"=>time()
		));
		if ($debugLevel>0) Tool::echoLog("CampaignPublisher records (" . JPDO::count() . ") updated");
		unset($cp);

		$cron = new Cron();
		$cron->update(array(
			"publisher_cron"=>$this->id,
			"state"=>Cron::ENUM_STATE_WAITING,
			"type"=>array(Cron::ENUM_TYPE_TWEET_CHECK,Cron::ENUM_TYPE_TWEET_SEND)
		),array(
			"state"=>Cron::ENUM_STATE_CANCELLED
		));
		if ($debugLevel>0) Tool::echoLog("Cron jobs (" . JPDO::count() . ") canceled.");
		unset($cron);

		$pr = new PublisherRevenue();
		$pr->update(array(
			"publisher_publisherRevenue"=>$this->id,
			"status"=>PublisherRevenue::ENUM_STATUS_WAITING,
			"campaign_publisherRevenue"=>array(
				"race"=>Campaign::ENUM_RACE_TWITTER
			)
		),array(
			"status"=>PublisherRevenue::ENUM_STATUS_CANCELED
		));
		if ($debugLevel>0) Tool::echoLog("PublisherRevenue records (" . JPDO::count() . ") updated.");
		unset($pr);

		$ctm = new CampaignTwitterMessage();
		$ctm->with("campaign_campaignTwitterMessage");
		$ctm->populateOnce(false)->load(array(
			"publisher_campaignTwitterMessage"=>$this->id,
			"_custom"=>"`campaignTwitterMessage`.`state` NOT IN ('" . CampaignTwitterMessage::ENUM_STATE_TWEET_EXIST . "','" . CampaignTwitterMessage::ENUM_STATE_TWEET_DELETED . "')"
		));
		if ($ctm->gotValue){
			do {
				$ctm->campaign_campaignTwitterMessage->loadAdvertiser();
				$ctm->loadCampaignPublisher();
				AdvertiserMoneyAction::create(
					$ctm->campaign_campaignTwitterMessage->advertiser,
					$ctm->campaign_campaignTwitterMessage,
					$ctm->campaignPublisher->cost,
					"user unauthorized:".$this->user_publisher->username,
					$ctm->publisher_campaignTwitterMessage,
					AdvertiserMoneyAction::ENUM_STATE_APPROVED,
					AdvertiserMoneyAction::ENUM_PAYMENTTYPE_REFUND
				);
				$ctm->campaign_campaignTwitterMessage->writeHistory(
					CampaignHistory::ENUM_ACTIONID_UNAUTHORIZE_PUBLISHER_BUDGET_REFUND,
					"Publisher unauthorized from Twitter: " . $this->user_publisher->username,
					CampaignHistory::ENUM_MADEBY_SYSTEM,
					$this->id
				);
				$ctm->state = CampaignTwitterMessage::ENUM_STATE_TWEET_DELETED;
				$ctm->tweetCheckDate = time();
				$ctm->tryCount++;
				$ctm->save();

				if ($debugLevel>0) Tool::echoLog("Publisher unauthorized from campaign: " . $ctm->campaign_campaignTwitterMessage->title . " ctm#: " . $ctm->id);
			} while($ctm->populate());

		}
		$this->loadSnTwitter();
		if (is_object($this->publisher_snTwitter)){
			if (strlen($this->publisher_snTwitter->errorStatus) == 0){

				if (!$withoutWarning) {
					$this->user_publisher->sendSysMessage("V2_UserDeactivated",array(),array("name"=>$this->getFullName()));
				}
				$this->publisher_snTwitter->unAuthotizeDate = time();
				$this->writeHistory(
					UserHistory::ENUM_ACTIONID_TWITTER_UNAUTHORIZE,
					"Publisher unauthorized from Twitter: " . $this->user_publisher->username,
					UserHistory::ENUM_MADEBY_SYSTEM
				);
			}
			$this->publisher_snTwitter->unAuthotizeDate = time();
			$this->publisher_snTwitter->publisher_snTwitter = null;
			$this->publisher_snTwitter->save();
		} 
		else {
			if (!$withoutWarning){
				$this->user_publisher->sendSysMessage("V2_UserDeactivated",array(),array("name"=>$this->getFullName()));
			}
		}

		JPDO::commit();

		if ($debugLevel>0) Tool::echoLog("Deactivating user done : " . $this->user_publisher->username);
	}

	/**
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function unauthorizeFacebook( $id=null, $madeByUser=true ){
		$this->loadPublisher_snFacebook( $id );
		if($this->publisher_snFacebook && $this->publisher_snFacebook->gotValue){
			do {
				$this->publisher_snFacebook->publisher_snFacebook = null;
				$this->publisher_snFacebook->save();

			} while ( $this->publisher_snFacebook->populate() );
		}
		if(is_null($id))
			$this->hasFacebook = 0;

		$this->writeHistory(
			UserHistory::ENUM_ACTIONID_USER_SOCIAL,
			"Publisher unauthorized from Facebook ",
			$madeByUser?UserHistory::ENUM_MADEBY_USER:UserHistory::ENUM_MADEBY_SYSTEM
		);
		$this->save();
	}

	/**
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function unauthorizeVK( $id=null, $madeByUser=true ){
		$this->loadPublisher_snVK( $id );
		if($this->publisher_snVK && $this->publisher_snVK->gotValue){
			do {
				$this->publisher_snVK->publisher_snVK = null;
				$this->publisher_snVK->save();

			} while ( $this->publisher_snVK->populate() );
		}
		if(is_null($id))
			$this->hasVK = 0;

		$this->writeHistory(
			UserHistory::ENUM_ACTIONID_USER_SOCIAL,
			"Publisher unauthorized from VK ",
			$madeByUser?UserHistory::ENUM_MADEBY_USER:UserHistory::ENUM_MADEBY_SYSTEM
		);
		$this->save();
	}

	/**
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function unauthorizeXING( $id=null, $madeByUser=true ){
		$this->loadPublisher_snXING( $id );
		if($this->publisher_snXING && $this->publisher_snXING->gotValue){
			do {
				$this->publisher_snXING->publisher_snXING = null;
				$this->publisher_snXING->save();

			} while ( $this->publisher_snXING->populate() );
		}
		if(is_null($id))
			$this->hasXING = 0;

		$this->writeHistory(
			UserHistory::ENUM_ACTIONID_USER_SOCIAL,
			"Publisher unauthorized from XING ",
			$madeByUser?UserHistory::ENUM_MADEBY_USER:UserHistory::ENUM_MADEBY_SYSTEM
		);
		$this->save();
	}

	/**
	 * @param null $id
	 * @author Murat
	 */
	public function unauthorizeInstagram( $id=null, $madeByUser=true ){
		$this->loadPublisher_snInstagram( $id );
		if($this->publisher_snInstagram && $this->publisher_snInstagram->gotValue){
			do {
				$this->publisher_snInstagram->publisher_snInstagram = null;
				$this->publisher_snInstagram->unAuthorizeDate = time();
				$this->publisher_snInstagram->save();

			} while ( $this->publisher_snInstagram->populate() );
		}
		if(is_null($id))
			$this->hasInstagram = 0;

		$this->writeHistory(
			UserHistory::ENUM_ACTIONID_USER_SOCIAL,
			"Publisher unauthorized from Instagram ",
			$madeByUser?UserHistory::ENUM_MADEBY_USER:UserHistory::ENUM_MADEBY_SYSTEM
		);
		$this->save();
	}

	/**
	 * @param null $id
	 * @author Özgür Köy
	 */
	public function unauthorizeGoogle( $id=null, $madeByUser=true ){
		$this->loadPublisher_snGoogle( $id );
		if($this->publisher_snGoogle && $this->publisher_snGoogle->gotValue){
			do {
				$this->publisher_snGoogle->publisher_snGoogle = null;
				$this->publisher_snGoogle->save();

			} while ( $this->publisher_snGoogle->populate() );
		}
		if(is_null($id))
			$this->hasGPlus = 0;

		$this->writeHistory(
			UserHistory::ENUM_ACTIONID_USER_SOCIAL,
			"Publisher unauthorized from Google ",
			$madeByUser?UserHistory::ENUM_MADEBY_USER:UserHistory::ENUM_MADEBY_SYSTEM
		);
		$this->save();
	}
	
	/**
	 * load available withdraw options
	 * @param bool $onlyWithDrawable
	 * @param null $id
	 * @return bool|JModel
	 * @author Özgür Köy
	 */
	public static function loadAvailableWithDrawOptions( $onlyWithDrawable = true, $filters=null ) {
		$w      = new PublisherWithdrawOption();
		$filter = array( "active" => 1 );
		if ( $onlyWithDrawable === true )
			$filter[ "isWithdrawable" ] = 1;
		if ( !is_null( $filters ) )
			$filter += $filters;

		return $w->load( $filter );
	}

	/**
	 * Load publisher options by name or all
	 * @param null $name
	 * @return publisherPaymentOption
	 * @author Özgür Köy
	 */
	public function loadPaymentOptions( $name = null ) {
		$filter = array( "deleted" => 0 );
		if(!is_null( $name ))
			$filter["publisherWithdrawOption"] = array( "name" => $name );

		return $this->loadPublisher_publisherPaymentOption( $filter );
	}


	/**
	 * Returns users cost; checks spaecialCost if exists special cost is users cost else default cost is returned
	 * @return float
	 */
	public function getLastCost() {
		if(!$this->id) {
			return 0;
		}

		if (!is_object($this->publisher_snTwitter)){
			$this->loadSnTwitter();
		}
		$cost = 0;
		if($this->publisher_snTwitter->customCost > 0) {
			$cost = $this->publisher_snTwitter->customCost;
		} elseif($this->publisher_snTwitter->cost > 0) {
			$cost = $this->publisher_snTwitter->cost;
		} else {
			$cost = 0;
		}

		return $cost;
	}

	/**
	 * Get publishers latest SnTwitterHistory id
	 *
	 * @return int
	 * @author Murat
	 */
	public function getLastTwitterHistoryID(){
		$th = null;
		if (!is_object($this->publisher_snTwitter)){
			$this->loadSnTwitter();
		}
		//return false;
		if ($this->publisher_snTwitter->gotValue){
			if (!is_object($this->publisher_snTwitter->snTwitter_snTwitterHistory)){
				$this->publisher_snTwitter->loadSnTwitter_snTwitterHistory(null,0,1,"id DESC");
			}
			if ($this->publisher_snTwitter->snTwitter_snTwitterHistory->gotValue){
				return true;
			}
		}
		return false;
	}

	/**
	 * Get publishers latest Twitter data
	 *
	 * @return int
	 * @author Murat
	 */
	public function getLatestTwitterData(){
		$th = null;
		if ($this->hasTwitter){
			$snth = new SnTwitterHistory();
			$snth
				->with("snTwitter_snTwitterHistory")
				->orderBy("id DESC")
				->limit(0,1)->load(array(
					"snTwitter_snTwitterHistory" => array(
						"publisher_snTwitter" => $this->id
					)
				));
			if ($snth->gotValue){
				$th = array(
					"lastID" => $snth->id,
					"cost" => 0,
					"follower" => $snth->snTwitter_snTwitterHistory->follower,
					"influence" => $snth->snTwitter_snTwitterHistory->trueReach
				);
				if($snth->snTwitter_snTwitterHistory->customCost > 0) {
					$th["cost"] = $snth->snTwitter_snTwitterHistory->customCost;
				} elseif($snth->snTwitter_snTwitterHistory->cost > 0) {
					$th["cost"] = $snth->snTwitter_snTwitterHistory->cost;
				} else {
					$th["cost"] = 0;
				}
			}
			unset($snth);
		}

		return $th;
	}

	/**
	 * Returns users price with profit; checks spaecialCost if exists uses special cost else default cost is used
	 * @return real
	 */
	public function getLastPrice() {
		return ($this->getLastCost() * PROFIT_MULTIPLIER);
	}


	public function loadTwitter() {
		$ti = new TwitterInfo();
		$ti->customClause = 'user=' . intval($this->id);
		$ti->load();

		return $ti;
	}

	public function loadBankAccounts() {
		$uba = new UserBankAccount();
		$uba->customClause = 'user=' . intval($this->id);
		$uba->load();

		return $uba;
	}

	public function loadUserBankAccounts() {
		return $this->loadBankAccounts();
	}


	public static function getFollower($userId) {
		$twitterInfo = new TwitterInfo();
		$twitterInfo->customClause = 'user='.$userId;
		$twitterInfo->load();

		return $twitterInfo->follower;
	}

	/**
	 * reset error status
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function resetErrorStatus()
	{
		$twi = new TwitterInfo();
		$twi->user = $this->id;
		$twi->load();
		if ($twi->count){
			$twi->errorStatus = null;
			$twi->unAuthotizeDate = null;
			$twi->save();
		}
		unset($twi);
	}

	/**
	 * reset twitter key incase of a problem
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function resetTwitterKey()
	{
		$this->errorStatus = User::$errorTwitterKey;
		$this->save();

		$info = $this->loadTwitter();

		$info->twKey = "";
		$info->twKeySecret = "";
		$info->save();
	}

	/**
	 * load celebs
	 *
	 * @return User
	 * @author Özgür Köy
	 **/
	public function loadCelebs()
	{
		$us = new User();
		$us->utype = 'p';
		$us->ptype = 'c';
		$us->active = 1;
		$us->nopop()->load();
		return $us;
	}

	/**
	 * load money actions
	 * @param int $page
	 * @param     $perPage
	 * @return array
	 * @author Özgür Köy
	 */
	public function loadMoneyActions( $page = 1, $perPage ) {
		$m = new PublisherMoneyAction();
		$m->having( "`count` IS NOT NULL" )->count( "id", "count", array( "publisher_publisherMoneyAction" => $this->id ) );
		$totalCount = $m->count;
		unset( $m );

		$actions = $this->loadPublisher_publisherMoneyAction( array(), $page, $perPage );
		$resp = array();
		if ( $actions && $actions->gotValue ) {
			do {
				$r              = new MoneyActionResponse();
				$r->id          = $actions->id;
				$r->amount      = $actions->amount;
				$r->amountAfter = $actions->amountAfter;
				$r->unixTime    = $actions->lastUpdate;
				$r->state       = $actions->status;



				if ( is_null( $actions->paymentOption ) ) {
					$r->label = $actions->note;
				}
				else {
					$act      = $actions->loadPaymentOption();
					$r->label = $act->displayName;
				}

				array_push( $resp, $r );

				unset( $r );
			} while ( $actions->populate() );
		}

		unset( $actions );
		return array( $totalCount, $resp );

	}

	/**
	 * recalc and save current balance
	 * @sam
	 * @return float
	 **/
	public function recalculateWithdrawableBalance() {
		// Get Total Revenue
		$m = $this->loadPublisher_publisherRevenue();
		if(!$m || $m->gotValue !== true)
			return 0;

		$revenue = $this->totalRevenue();

		$m = $this->loadPublisher_publisherMoneyAction();
		$sum = $m->publisherWithdrewAmount($this->id);

		unset( $m );

		// $sum has minus value
		$this->currentBalance = round($revenue + $sum,3,PHP_ROUND_HALF_DOWN);
		$this->save();

		return $this->currentBalance;

	}

	/*
	 * Get the Total User Approved Revenue
	 * moved from publisher revenue to here by ozgur
	 */
	public function totalRevenue( $campaignId = null ) {
		$filter = array(
			"publisher_publisherRevenue" => $this->id,
			"status"                     => PublisherRevenue::ENUM_STATUS_APPROVED
		);

		if ( is_null( $campaignId ) !== true ) {
			$filter[ "campaign_publisherRevenue" ] = $campaignId;
		}
		$rev = new PublisherRevenue();
		$rev->having( "`TOPLAM` IS NOT NULL" )
			->sum( "amount",
				"TOPLAM",
				$filter
			);

		if ( $rev->gotValue )
			$revenue = $rev->TOPLAM;
		else
			$revenue = 0;

		unset( $rev );

		return $revenue;
	}

	/**
	 * Deactivate publisher
	 * @return bool
	 * @author Özgür Köy
	 */
	public function deactivate() {
		JPDO::beginTransaction();

		try {
			#deactivate user
			if ( $this->loadUser()->deactivate() === false )
				throw new JError( "error deactivating" );

			#balance

			/*$balance = $this->recalculateWithdrawableBalance();
			if ( $balance > 0 ) {
				$action = PublisherMoneyAction::addNewMoneyAction(
					$this->publisher,
					array( "amount" => $balance, "note" => "Deactivation on " . date( "d-m-Y H:i", time() ) )
				);
			}*/

			#social
			$this->unauthorizedFromTwitter( true );
			$this->unauthorizeFacebook( null, false );
			$this->unauthorizeGoogle( null, false );
			$this->unauthorizeVK( null, false );
			$this->unauthorizeXING( null, false );

			JPDO::commit();

		}
		catch ( JError $j ) {
			JPDO::rollback();

			return false;
		}

		return true;
	}

	/**
	 * recalc users waiting balance
	 *@Sam
	 * @return float
	 **/
	public function calculatePendingBalance() {

		$m = $this->loadPublisher_publisherMoneyAction();
		$sum = $m->publisherPendingBalance($this->id);

		unset( $m );

		// Change the display value to Plus, $sum is minus
		return $sum * -1;
	}

	/**
	 * clear conns
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function clearConns()
	{
		$uc = new UserConns();
		$uc->load($this);
		do {
			$uc->delete();
		} while ($uc->populate());
	}

	/**
	 * Create publisher history
	 *
	 * @author Murat
	 */
	public function writeHistory( $actionId, $actionText, $madeBy, $actionData = null ) {
		if ( !is_object( $this->user_publisher ) ) {
			$this->loadUser();
		}
		if ( is_array( $actionData ) ) $actionData = json_encode( $actionData );
		$this->user_publisher->saveHistory( $actionId, $actionText, $madeBy, $actionData );

		return $this;
	}

	/**
	 * Change publisher image
	 *
	 * @return void
	 * @author Murat
	 * @author ozgur - modified
	 */
	public function changeImage($imageType,$file){
		global $__DP;
		$uploadPath = $__DP."site/layout/images/publisher/";
		$extension = Tool::getFileExtension($file["name"]);

		if ($imageType == 1){
			$imageType = "Profile";
		} else {
			$imageType = "Background";
		}

		$fileName = $this->id . "_" . $imageType . "_" . JUtil::randomString(20) . "." . $extension;
		$upFileName = $uploadPath.$fileName;
		if (file_exists($upFileName))
		{
			unlink($upFileName);
		}

		if(isset($file["tmp_name"]))
			move_uploaded_file($file["tmp_name"],$upFileName);
		else{
			rename( $file[ "name" ], $upFileName );
//			@unlink( $file[ "name" ] );
		}
		if (!file_exists($upFileName)) return;

		require_once($__DP . "site/lib/imageResize.php");
		$image = new resize($uploadPath.$fileName);

		$imageMaxWidth = Setting::getValue("publisher" . $imageType . "ImageMaxWidth");
		$imageMaxHeight = Setting::getValue("publisher" . $imageType . "ImageMaxHeight");

		if ($image->width > $imageMaxWidth || $image->height > $imageMaxHeight){
			$image->resizeImage($imageMaxWidth, $imageMaxHeight , "auto");
			$image->saveImage($uploadPath.$fileName, 100);
		}
		if ($imageType == "Profile"){
			if (!empty($this->profileImage) && file_exists($uploadPath.$this->profileImage)){
				unlink($uploadPath.$this->profileImage);
			}
			$this->profileImage = $fileName;
		} else {
			if (!empty($this->backgroundImage) && file_exists($uploadPath.$this->backgroundImage)){
				unlink($uploadPath.$this->backgroundImage);
			}
			$this->backgroundImage = $fileName;
		}
		$this->save();
		unset($imageType,$image, $imageMaxWidth, $imageMaxHeight, $fileName, $upFileName, $uploadPath, $extension);
	}

	/**
	 * Change publisher profile image with crop
	 *
	 * @return array
	 * @author Murat
	 */
	public function chaneImageWithCrop($file){
		global $__DP;
		$uploadPath = "site/layout/images/publisher/";

		// image name
		$name = $this->id . "_Profile_" . JUtil::randomString(20) . ".";

		// location to save cropped image
		$url = $__DP.$uploadPath.$name;

		$dst_x = 0;
		$dst_y = 0;

		$src_x = $file['x']; // crop Start x
		$src_y = $file['y']; // crop Srart y

		$src_w = $file['w']; // $src_x + $dst_w
		$src_h = $file['h']; // $src_y + $dst_h

		// set a specific size for the image
		// the default is to grab the width and height from the cropped image.
		$dst_w = 150;
		$dst_h = 150;

		// remove the base64 part
		$base64 = $file['image'];
		$image = null;


		// if URL is a base64 string
		if (substr($base64, 0, 5) == 'data:') {

			// remove data from image
			$base64 = preg_replace('#^data:image/[^;]+;base64,#', '', $base64);

			$base64 = base64_decode($base64);

			// create image from string
			$source = imagecreatefromstring($base64);
			imagealphablending($source, FALSE);
			imagesavealpha($source, TRUE);

			$url .= Validate::getImageMimeType($base64);
		}
		else {

			// strip parameters from URL
			$base64 = strtok($base64, '?');

			list($height, $width, $type) = getimagesize($base64);

			// create image
			if ($type == 1){
				$source = imagecreatefromgif($base64);
				$url .= "gif";
			}else if ($type == 2){
				$source = imagecreatefromjpeg($base64);
				$url .= "jpg";
			}else if ($type == 3) {
				$source = imagecreatefrompng($base64);
				$url .= "png";

				// keep transparent background
				imagealphablending($image, FALSE);
				imagesavealpha($image, TRUE);

			}
			else die();

		}

		// resize image variable
		$image = imagecreatetruecolor($dst_w, $dst_h);

		// process cropping and resizing of image
		imagecopyresampled($image, $source, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

		// save image
		imagejpeg($image, $url, 100);

		$fileName = $name . substr($url,-3);
		if (!empty($this->profileImage) && file_exists($uploadPath.$this->profileImage)){
			unlink($uploadPath.$this->profileImage);
		}
		$this->profileImage = $fileName;
		$this->save();

		// return URL
		$validation = array (
			'url'     => JUtil::siteAddress() . $uploadPath . $fileName
		);

		if (!is_object($this->user_publisher)) $this->loadUser();
		$this->user_publisher->fillSession();

		return $validation;
	}

	/**
	 * Filter users for given parameters
	 *
	 * @return Publisher
	 * @author Murat
	 */
	public static function filterPublishers($onlyStats = false,$race,$ages,$sex,$interests,$celebrityOnly=false,$incClub=null,$excClub=null,$spesificPublisherId=null,$excCampaign=null,$cities=null){
		global $dps;
		$p = new Publisher();

		$filterParams = array(
		);

		//apply age filter
		if (is_array($ages) && count($ages)){
			$filterParams["bday"] = "";
			foreach($ages as $age){
				$age = trim($age);
				$thisYear = date("Y");
				$minYear = null;
				$maxYear = null;
				switch($age){
					case "0":
					case "13-17":
						$minYear = $thisYear - 13;
						$maxYear = $thisYear - 17;
						break;
					case "1":
					case "18-24":
						$minYear = $thisYear - 18;
						$maxYear = $thisYear - 24;
						break;
					case "2":
					case "25-34":
						$minYear = $thisYear - 25;
						$maxYear = $thisYear - 34;
						break;
					case "3":
					case "35-44":
						$minYear = $thisYear - 35;
						$maxYear = $thisYear - 44;
						break;
					case "4":
					case "45-54":
						$minYear = $thisYear - 45;
						$maxYear = $thisYear - 54;
						break;
					case "5":
					case "55+":
					case "55": //todo:@Murat: check this bug on chrome. it doesn't post the value on checkboxes with +
						$maxYear = $thisYear - 55;
						break;
				}
				if (is_null($minYear)){
					$maxYear = strtotime($maxYear . "-01-01 00:00");
					$filterParams["bday"] .= (!empty($filterParams["bday"]) ? " &or " : "") . "&lt:$maxYear";
				} else {
					$minYear = strtotime($minYear . "-12-31 23:59");
					$maxYear = strtotime($maxYear . "-01-01 00:00");
					$filterParams["bday"] .= (!empty($filterParams["bday"]) ? " &or " : "") . "BETWEEN $maxYear,$minYear";
				}
			}
		}

		$filterParams["user_publisher"] = array(
			"status" => User::ENUM_STATUS_ACTIVE,
			"utype" => User::ENUM_UTYPE_PUBLISHER
		);
		$filterParams["type"] = (!$celebrityOnly ? Publisher::ENUM_TYPE_REGULAR . " &or " . Publisher::ENUM_TYPE_TRENDSETTER : Publisher::ENUM_TYPE_CELEBRITY);
		//apply gender filter
		switch ($sex){
			case CampaignFilter::ENUM_SEX_MALE:
				$filterParams["gender"] = Publisher::ENUM_GENDER_M;
				break;
			case CampaignFilter::ENUM_SEX_FEMALE:
				$filterParams["gender"] = Publisher::ENUM_GENDER_F;
				break;
		}
		//apply interests filter
		if (is_array($interests) && count($interests)){

			$filterParams["trends"] = array("id"=>"0");
			foreach($interests as $trend){
				$filterParams["trends"]["id"] .= " &or " . $trend;
			}
			/**/
			//$filterParams["trends"]["id"] = $interests;
		}
		//apply included clubs filter
		if (is_array($incClub) && count($incClub)){

			$filterParams["clubs"] = array("id"=>"0");
			foreach($incClub as $club){
				$filterParams["clubs"]["id"] .= " &or " . $club;
			}
			/**/
			//$filterParams["clubs"]["id"] = $incClub;
		}
		//apply city filter
		if (is_array($cities) && count($cities)){

			$filterParams["city"] = array("id"=>"0");
			foreach($cities as $city){
				$filterParams["city"]["id"] .= " &or " . $city;
			}
			/**/
			//$filterParams["clubs"]["id"] = $incClub;
		}

		if (!is_null($spesificPublisherId)){
			$filterParams["id"] = $spesificPublisherId;
		}

		//apply filter for effect camp
		if ($race == Campaign::ENUM_RACE_TWITTER){
			$filterParams["hasTwitter"] = true;
			$filterParams["publisher_snTwitter"] = array(
				"errorStatus" => "IS NULL",
				"trueReach" => "&gt:0"
			);
			$p->with("publisher_snTwitter");
		}

		$exFilter = array();
		if (is_array($excClub) && count($excClub)){
			$p->filter(array("clubs"=>array("id"=>$excClub)));

		}

		if (is_array($excCampaign) && count($excCampaign)){
			$p->filter( array( "publisher_campaignPublisher" => array(
				"campaign_campaignPublisher" => $excCampaign,
				"state"                      => CampaignPublisher::ENUM_STATE_APPROVED
			) ) );
		}

		if (count($exFilter)) $p->filter($exFilter);

		if ($onlyStats){
			$r = array("publisher"=>0, "follower"=>0, "influence"=>0, "price"=>0);

			if ($race == Campaign::ENUM_RACE_TWITTER){
				//JPDO::debugQuery(true);
				$p->multipleGroupOperation(true)
					->sum("`publisher_snTwitter`.`trueReach`", "sTrueReach")
					->sum("`publisher_snTwitter`.`follower`","sFollower")
					->sum("`publisher_snTwitter`.`cost`","sCost")
					->count( "id", "cPublisher" )
					->load($filterParams);
				//exit;
				$r["publisher"] = $p->cPublisher;
				$r["follower"] = $p->sFollower;
				$r["influence"] = $p->sTrueReach;
				$r["price"] = $p->sCost * Setting::getValue("PROFIT_MULTIPLIER");
			} else {
				//print_r($filterParams);
				//echo PHP_EOL;
				//JPDO::debugQuery(true);
				$p->multipleGroupOperation(true)
					->count( "id", "cPublisher" )
					->load($filterParams);
				//exit;

				$r["publisher"] = $p->cPublisher;
			}

			return $r;
		} else {
			$p->populateOnce(false)->orderBy("id")->load($filterParams);
			return $p;
		}
	}

	/**
	 * @author Özgür Köy
	 * load sn to object. since they are seperately connected.
	 */
	public function loadSocialNetworks( $getAuths=false ) {
		$this->loadPublisher_snGoogle();
		$this->loadSnTwitter();
		$this->loadPublisher_snFacebook();
		$this->loadPublisher_snVK();
		$this->loadPublisher_snXING();
		$this->loadPublisher_snInstagram();


		if($getAuths !== false){
			if($this->publisher_snFacebook->gotValue !== true)
				$this->loginUrls[ "facebook" ] = $this->publisher_snFacebook->getLoginUrl( null, null, true );

			if($this->publisher_snTwitter->gotValue !== true )
				$this->loginUrls[ "twitter" ] = $this->publisher_snTwitter->getLoginUrl();

			if($this->publisher_snGoogle->gotValue !== true )
				$this->loginUrls[ "google" ] = $this->publisher_snGoogle->getLoginUrl();

			if($this->publisher_snVK->gotValue !== true )
				$this->loginUrls[ "VK" ] = $this->publisher_snVK->getLoginUrl();

			if($this->publisher_snXING->gotValue !== true )
				$this->loginUrls[ "XING" ] = $this->publisher_snXING->getLoginUrl();

			if($this->publisher_snInstagram->gotValue !== true )
				$this->loginUrls[ "instagram" ] = $this->publisher_snInstagram->getLoginURL();
		}
	}


	/**
	 * Had to do it like this, because all methods were non static
	 * @return array
	 * @author Özgür Köy
	 */
	public static function getSNLoginUrls( $onlyUrls=true ) {
		$set = json_decode(Setting::getValue( "SHARE_OPTIONS" ),true);
		if(!is_array($set) || !(sizeof($set)>0) ) return array();
		$urls               = array();
		if ( in_array( "facebook", $set ) || $onlyUrls === true) {
			$f                  = new SnFacebook();
			$urls[ "facebook" ] = $f->getLoginUrl( null, null, true );
		}
		if ( in_array( "twitter", $set ) || $onlyUrls === true ) {
			$t = new SnTwitter();
			$urls[ "twitter" ]  = $t->getLoginUrl();
		}
		if ( in_array( "gplus", $set ) || $onlyUrls === true ) {
			$g = new SnGoogle();
			$urls[ "google" ]   = $g->getLoginUrl();
		}
		if ( in_array( "vk", $set ) || $onlyUrls === true ) {
			$v = new SnVK();
			$urls[ "VK" ]   = $v->getLoginUrl();
		}
		if ( in_array( "xing", $set ) || $onlyUrls === true ) {
			$v = new SnXING();
			$urls[ "XING" ]   = $v->getLoginUrl();
		}
		if ( in_array( "instagram", $set ) || $onlyUrls === true ) {
			$i = new SnInstagram();
			$urls[ "instagram" ]   = $i->getLoginUrl();
		}

		unset( $f, $t, $g, $v );

		return $urls;
	}

	/**
	 * load settings with title and label
	 * @param null $type
	 * @param null $title
	 * @return array
	 * @author Özgür Köy
	 */
	public function loadNotificationSettings( $type=null, $title=null ) {
		$filter = array( "isActive" => 1 );
		if ( !is_null( $type ) )
			$filter[ "type" ] = $type;
		if ( !is_null( $type ) )
			$filter[ "title" ] = $title;

		$this->loadNotifications( $filter );
		$ret = array( "SMS" => array(), "EMAIL" => array() );
		if ( $this->notifications && $this->notifications->gotValue ) {
			do {
				$ret[ $this->notifications->type_options[ $this->notifications->type ] ][ $this->notifications->id ] = array( "label" => $this->notifications->word, "title" => $this->notifications->title );
			} while ( $this->notifications->populate() );

		}

		return $ret;
	}

	/**
	 * Send custom message
	 * @param array $messageTypes
	 * @param       $message
	 * @author Özgür Köy
	 */
	public function sendCustomMessage(array $messageTypes,$title,$message) {
		$this->loadUser();

		if (in_array( "email", $messageTypes )){
			JT::init();
			JT::assign("title",$title);
			JT::assign("content",nl2br($message));
			$body = JT::pfetch("mail_general");
			Mail::createManual($this->user_publisher->getDefaultEmail()->email,$title,$body);
		}
		if (in_array( "dashboard", $messageTypes )){
			UserMessage::createManual($this->user_publisher->id,$title,$message);
		}
		if (in_array( "mobile", $messageTypes )){
			PushMobileMessage::create($this->user_publisher,null,null,null,null,$title,$message);
		}
	}

	/**
	 * reset password
	 * @param null   $password
	 * @param string $operator
	 * @return null|string
	 * @author Özgür Köy
	 */
	public function resetPassword($password=null, $operator=UserHistory::ENUM_MADEBY_ADMIN) {
		$this->loadUser();

		$password = !is_null( $password ) ? $password : JUtil::randomString( 10 );
		$this->user_publisher->pass = md5( $password );
		$this->user_publisher->save();
		$this->user_publisher->saveHistory( UserHistory::ENUM_ACTIONID_PASSWORD_CHANGE, "password changed",$operator, null );

		return $password;
	}

	/**
	 * Add snTwitter account
	 *
	 * @param snTwitter $snt
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public function addSnTwitter($snt){
		$this->hasTwitter = true;

		$this->saveSnTwitter( $snt );
		$this->save();
		$this->writeHistory( UserHistory::ENUM_ACTIONID_USER_SOCIAL, "user connected twitter account", UserHistory::ENUM_MADEBY_USER );
		PublisherUpdate::addUpdate($this);
	}

	/**
	 * @param SnGoogle $google
	 * @author Özgür Köy
	 */
	public function addSnGoogle( SnGoogle $google ) {
		$this->hasGPlus = true;
		$this->save();
		$this->savePublisher_snGoogle( $google );
		$this->writeHistory( UserHistory::ENUM_ACTIONID_USER_SOCIAL, "user connected google account", UserHistory::ENUM_MADEBY_USER );
	}

	/**
	 * @param SnFacebook $facebook
	 * @author Özgür Köy
	 */
	public function addSnFacebook( SnFacebook $facebook ) {
		$this->hasFacebook = true;
		$this->savePublisher_snFacebook( $facebook );
		$this->save();
		$this->writeHistory( UserHistory::ENUM_ACTIONID_USER_SOCIAL, "user connected facebook account", UserHistory::ENUM_MADEBY_USER );
	}
	
	/**
	 * @param SnVK $VK
	 * @author Özgür Köy
	 */
	public function addSnVK( SnVK $VK ) {
		$this->hasVK = true;
		$this->savePublisher_snVK( $VK );
		$this->save();
		$this->writeHistory( UserHistory::ENUM_ACTIONID_USER_SOCIAL, "user connected VK account", UserHistory::ENUM_MADEBY_USER );
	}

	/**
	 * @param SnVK $VK
	 * @author Özgür Köy
	 */
	public function addSnXING( SnXING $X ) {
		$this->hasXING = true;
		$this->savePublisher_snXING( $X );
		$this->save();
		$this->writeHistory( UserHistory::ENUM_ACTIONID_USER_SOCIAL, "user connected XING account", UserHistory::ENUM_MADEBY_USER );
	}

	/**
	 * @param SnInstagram $i
	 * @author Murat
	 */
	public function addSnInstagram( SnInstagram $i ) {
		$this->hasXING = true;
		$this->savePublisher_snInstagram( $i );
		$this->save();
		$this->writeHistory( UserHistory::ENUM_ACTIONID_USER_SOCIAL, "user connected Instagram account", UserHistory::ENUM_MADEBY_USER );
	}

	/**
	 * Get publisher campaign count by state
	 *
	 * @param string $state
	 *
	 * @return int
	 *
	 * @author Murat
	 */
	public function getCampaignCount($state){
		$cp = new CampaignPublisher();
		$loadParams = $cp->createPublisherFilter($state,$this->id);
		$cp->count("id","cnt",$loadParams);
		$cnt = $cp->cnt;
		unset($cp);
		return $cnt;
	}

	/**
	 * Add publisher to active CPC campaigns
	 *
	 * @return int
	 * @author Murat
	 */
	public function addToActiveCampaigns(){
		$count = 0;
		JPDO::beginTransaction();
		try{
			$c = new Campaign();
			$c->populateOnce(false)->with("campaign_campaignFilter")->load(array(
				"race" => array(Campaign::ENUM_RACE_CPC,Campaign::ENUM_RACE_CPL,Campaign::ENUM_RACE_CPL_REMOTE),
				"state" => "&lt " . Campaign::ENUM_STATE_FINISHED,
				"endDate" => "&ge " . date("Y-m-d"),
				"campaign_campaignFilter" => array ("noAutoAdd" => 0)
			));
			if ($c->gotValue){
				do{
					$cnt = $c->collectUsers(true,$this->id);
					$count += $cnt["readyCount"];
				} while($c->populate());
			}
			JPDO::commit();
		} catch(JError $e){
			JPDO::rollback();
		}

		return $count;
	}

	/**
	 * Get publisher count with filter
	 *
	 * @return int
	 * @author Murat
	 */
	public static function getCountWithFilter($filter){
		if (empty($filter["user_publisher"])){
			$filter["user_publisher"] = array(
				"status" => User::ENUM_STATUS_ACTIVE
			);
		}
		$p = new Publisher();
		$p->count("id","cnt",$filter);
		$cnt = $p->cnt;
		unset($p);
		return $cnt;
	}

	/**
	 * Total Social Network count
	 *
	 * @return void
	 * @author Sam
	 **/
	public function socialConnectionCount()
	{
		$conections = 0;
		$conections = $this->hasFacebook + $this->hasGPlus + $this->hasTwitter + $this->hasVK + $this->hasXING;
		return $conections;
	}

}

