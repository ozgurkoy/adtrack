<?php
	/**
	* twitterInfoKlout real class for Jeelet - firstly generated on 28-02-2012 22:08, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/twitterInfoKlout.php");

	class TwitterInfoKlout extends TwitterInfoKlout_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * Update TrueReach data and create history
		 *
		 * @return void
		 * @author Murat
		 */
		public function updateData($trueReach,TwitterInfo $ti){
			//$this->trueReach = $trueReach;
			if (!is_null($ti->customCost) && $ti->customCost > 0){
				$this->trueReach = intval($ti->customCost / 0.002);
			} else {
				$this->trueReach = intval(($ti->originalCost > $ti->cost ? $ti->originalCost : $ti->cost) / 0.002);
			}

			$this->save();

			$history = new KloutHistory();
			$history->trueReach = $this->trueReach;
			$history->twitterInfoKlout = $this->id;
			$history->cost = $ti->cost;
			$history->customCost = $ti->customCost;
			$history->minCost = $ti->minCost;
			$history->maxCost = $ti->maxCost;
			$history->originalCost = $ti->originalCost;
			$history->user = $ti->user;
			$history->save();

			unset($history);
		}
	}

	?>