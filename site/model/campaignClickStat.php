<?php
/**
 * campaignClickStat real class - firstly generated on 02-04-2013 16:01, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignClickStat.php';

class CampaignClickStat extends CampaignClickStat_base
{
	/**
	 * Campaign object
	 * 
	 * @var Campaign
	 */
	public $campaign = null;
	
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}
	
	/**
	 * Add/Update CampaignClickStat
	 * 
	 * @return void
	 * @author Murat
	 */
	public function addUpdateCCS($item, $linkHash){
		$this->load(array(
			"linkHash" => $linkHash,
			"clickDate" => strtotime($item->clickDate)
		));

		if (!$this->gotValue){
			$this->linkHash = $linkHash;
			$this->clickDate = strtotime($item->clickDate);
		}

		$this->calculatedClick = $item->calculatedClick;
		$this->suspectedClick = $item->suspectedClick;
		$this->totalClick = $item->totalClick;
		$this->lastUpdate = time();
		$this->save();
	}
	
	/**
	 * Bulk Add/Update CampaignClickStat
	 * 
	 * @return void
	 * @author Murat
	 */
	public static function bulkAddUpdateCCS($data){
		if (is_array($data) && count($data)){
			$ccs = new CampaignClickStat();
			foreach ($data as $items){
				if (isset($items->clicks) && is_array($items->clicks) && count($items->clicks)){
					foreach ($items->clicks as $click){
						$ccs = new CampaignClickStat();
						$ccs->addUpdateCCS($click,$items->hash);
					}
					$ccs->updateCampStatCounts();
				}
			}
			unset($ccs);
		}
	}
	
	/**
	 * Update campaign click stat counts
	 * 
	 * @return void
	 * @author Murat
	 */
	public function updateCampStatCounts(){
		global $debugLevel;
		if (DB_ENGINE == "MYSQL"){
			$sql = "
				UPDATE
					campaignClick
				SET
					calculatedClick = IFNULL((SELECT SUM(calculatedClick) FROM campaignClickStat WHERE linkHash = campaignClick.linkHash),0),
					suspectedClick = IFNULL((SELECT SUM(suspectedClick) FROM campaignClickStat WHERE linkHash = campaignClick.linkHash),0),
					totalClick = IFNULL((SELECT SUM(totalClick) FROM campaignClickStat WHERE linkHash = campaignClick.linkHash),0)
				WHERE linkHash = '{$this->linkHash}';
			";
		} else {
			$dbConfig = JCache::read( 'JDSN.MDB' );

			$sql = '
			UPDATE
				"'.  $dbConfig["schema"] . '"."campaignClick"
			SET
				"calculatedClick" = COALESCE((SELECT SUM("calculatedClick") FROM "'.  $dbConfig["schema"] . '"."campaignClickStat" WHERE "linkHash" = "campaignClick"."linkHash"),0),
				"suspectedClick" = COALESCE((SELECT SUM("suspectedClick") FROM "'.  $dbConfig["schema"] . '"."campaignClickStat" WHERE "linkHash" = "campaignClick"."linkHash"),0),
				"totalClick" = COALESCE((SELECT SUM("totalClick") FROM "'.  $dbConfig["schema"] . '"."campaignClickStat" WHERE "linkHash" = "campaignClick"."linkHash"),0)
			WHERE "linkHash" = \'' . $this->linkHash . '\';
			';
		}
		
		if(JPDO::connect( JCache::read('JDSN.MDB'),false,true )) {
			JPDO::executeQuery($sql);
			if ($debugLevel){
				Tool::echoLog ("Campaign link stats updated successfully");
				//echo $sql;
			}
		} else {
			throw new Exception("Unable to connect main database");
		}
		
	}

	/**
	 * Load campaign from hash
	 *
	 * @return Campaign|null
	 * @author Murat
	 */
	public function loadCampaign(){
		$cc = new CampaignClick();
		$cc->with("campaign_campaignClick")->load(array("linkHash"=>$this->linkHash));
		if ($cc->gotValue && $cc->campaign_campaignClick->gotValue){
			$this->campaign = $cc->campaign_campaignClick;
		}
		unset($cc);
		return $this->campaign;
	}
}

