<?php
/**
 * userClickStat real class - firstly generated on 01-04-2013 11:47, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/userClickStat.php';

class UserClickStat extends UserClickStat_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}
	
	/**
	 * Add/Update new UserClickStat
	 * 
	 * @return void
	 * @author Murat
	 */
	public function addUpdateUCS($item, $linkHash){
		$this->load(array(
			"linkHash" => $linkHash,
			"clickDate" => strtotime($item->clickDate)
		));

		if (!$this->gotValue){
			$this->linkHash = $linkHash;
			$this->clickDate = strtotime($item->clickDate);
			$this->blacklisted = false;
		}

		$this->calculatedClick = $item->calculatedClick;
		$this->suspectedClick = $item->suspectedClick;
		$this->totalClick = $item->totalClick;
		$this->lastUpdate = time();
		$this->save();
	}
	
	/**
	 * Update link stats
	 * 
	 * @return void
	 * @author Murat
	 */
	public static function updateLinkStats($data){
		$ucs = new UserClickStat();
		$linkHashes = array();
		if (isset($data->clicks) && is_array($data->clicks) && count($data->clicks)){
			foreach ($data->clicks as $item){
				$ucs = new UserClickStat();
				//die(print_r($item));
				$ucs->addUpdateUCS($item,$data->hash);

				$linkHashes[] = $data->hash;
			}
		}
		
		unset($ucs);
		
		$linkHashes = array_unique($linkHashes);
		
		//die(implode("','",$linkHashes));
		
		if (count($linkHashes)){
			self::updateLinkStatTotals(implode("','",$linkHashes));
		}
	}


	/**
	 * Update campaignUserMessage click counts
	 * 
	 * @return void
	 * @author Murat
	 */
	public static function updateLinkStatTotals($linkHashes){
		$dbConfig = JCache::read( 'JDSN.CAMP0' );

		if (DB_ENGINE == "MYSQL"){
			$sql = "
				UPDATE
					campaignCPCMessage ccm
				SET
					ccm.calculatedClick = IFNULL((SELECT SUM(calculatedClick) FROM userClickStat WHERE linkHash = ccm.linkHash),0),
					ccm.suspectedClick = IFNULL((SELECT SUM(suspectedClick) FROM userClickStat WHERE linkHash = ccm.linkHash),0),
					ccm.totalClick = IFNULL((SELECT SUM(totalClick) FROM userClickStat WHERE linkHash = ccm.linkHash),0),
					ccm.lastUpdate = IFNULL((SELECT MAX(lastUpdate) FROM userClickStat WHERE linkHash = ccm.linkHash),".time().")
				WHERE ccm.linkHash IN ('{$linkHashes}');

				UPDATE
					campaignTwitterMessage ctm
				SET
					ctm.totalClick = IFNULL((SELECT SUM(totalClick) FROM userClickStat WHERE linkHash = ctm.linkHash),0),
					ctm.lastUpdate = IFNULL((SELECT MAX(lastUpdate) FROM userClickStat WHERE linkHash = ctm.linkHash),".time().")
				WHERE ctm.linkHash IN ('{$linkHashes}');

				UPDATE
					publisherRevenue pr, campaignCPCMessage ccm, campaign c, campaignCPC cc
				SET
					pr.amount = (ccm.calculatedClick * cc.cpcCost)
				WHERE
					pr.campaign_publisherRevenue = ccm.campaign_campaignCPCMessage
					AND c.id = pr.campaign_publisherRevenue
					AND cc.campaign_campaignCPC = c.id
					AND c.race = 3
					AND pr.publisher_publisherRevenue = ccm.publisher_campaignCPCMessage
					AND ccm.linkHash IN ('{$linkHashes}')
					AND pr.status = 1;

				UPDATE
					publisher p, campaignCPCMessage ccm, campaign c
				SET
					p.currentBalance = (
						IFNULL((SELECT SUM(amount) AS TOPLAM FROM publisherMoneyAction WHERE publisher_publisherMoneyAction=p.id AND status IN (1,2)),0)
						+
						IFNULL((SELECT SUM(amount) AS TOPLAM FROM publisherRevenue WHERE publisher_publisherRevenue=p.id AND status=1),0)
					)
				WHERE
					p.id = ccm.publisher_campaignCPCMessage
					AND c.id = ccm.campaign_campaignCPCMessage
					AND c.race = 3
					AND ccm.linkHash IN ('{$linkHashes}');
			";

			JPDO::connect( JCache::read('JDSN.MDB') );
			JPDO::executeQuery($sql);
		} else {
			$dbConfig = JCache::read( 'JDSN.MDB' );

			$sql = '
			UPDATE
				"'.  $dbConfig["schema"] . '"."campaignCPCMessage" AS ccm
			SET
				"calculatedClick" = (SELECT SUM("calculatedClick") FROM "'.  $dbConfig["schema"] . '"."userClickStat" WHERE "linkHash" = "ccm"."linkHash"),
				"suspectedClick" = (SELECT SUM("suspectedClick") FROM "'.  $dbConfig["schema"] . '"."userClickStat" WHERE "linkHash" = "ccm"."linkHash"),
				"totalClick" = (SELECT SUM("totalClick") FROM "'.  $dbConfig["schema"] . '"."userClickStat" WHERE "linkHash" = "ccm"."linkHash"),
				"lastUpdate" = coalesce((SELECT MAX("lastUpdate") FROM "'.  $dbConfig["schema"] . '"."userClickStat" WHERE "linkHash" = "ccm"."linkHash"),'.time().')
			WHERE "ccm"."linkHash" IN (\'' . $linkHashes . '\');
			';

			JPDO::connect( JCache::read('JDSN.MDB') );
			JPDO::executeQuery($sql);
			$sql = '
			UPDATE
				"'.  $dbConfig["schema"] . '"."campaignTwitterMessage" ctm
			SET
				"totalClick" = (SELECT SUM("totalClick") FROM "'.  $dbConfig["schema"] . '"."userClickStat" WHERE "linkHash" = "ctm"."linkHash"),
				"lastUpdate" = coalesce((SELECT MAX("lastUpdate") FROM "'.  $dbConfig["schema"] . '"."userClickStat" WHERE "linkHash" = "ctm"."linkHash"),'.time().')
			WHERE "ctm"."linkHash" IN (\'' . $linkHashes . '\');
			';

			JPDO::connect( JCache::read('JDSN.MDB') );
			JPDO::executeQuery($sql);
			$sql = '
			UPDATE
					"'.  $dbConfig["schema"] . '"."publisherRevenue" pr
			SET
				amount = coalesce(("ccm"."calculatedClick" * "cc"."cpcCost"),0)
			FROM
				"'.  $dbConfig["schema"] . '"."campaignCPCMessage" ccm
				,"'.  $dbConfig["schema"] . '"."campaign" c
				,"'.  $dbConfig["schema"] . '"."campaignCPC" cc
			WHERE
				"c"."id" = "pr"."campaign_publisherRevenue"
				AND "cc"."campaign_campaignCPC" = "c"."id"
				AND "pr"."campaign_publisherRevenue" = "ccm"."campaign_campaignCPCMessage"
				AND "c"."race" = 3
				AND "pr"."publisher_publisherRevenue" = "ccm"."publisher_campaignCPCMessage"
				AND "ccm"."linkHash" IN (\'' . $linkHashes . '\')
				AND "pr"."status" = 1;
			';

			JPDO::connect( JCache::read('JDSN.MDB') );
			JPDO::executeQuery($sql);
			$sql = '
			UPDATE
					"'.  $dbConfig["schema"] . '"."publisher" p
				SET
					"currentBalance" = (
						coalesce((SELECT SUM("amount") AS TOPLAM FROM "'.  $dbConfig["schema"] . '"."publisherMoneyAction" WHERE "publisher_publisherMoneyAction"="p"."id" AND "status" IN (1,2)),0)
						+
						coalesce((SELECT SUM("amount") AS TOPLAM FROM "'.  $dbConfig["schema"] . '"."publisherRevenue" WHERE "publisher_publisherRevenue"="p"."id" AND "status"=1),0)
					)
			FROM
			"'.  $dbConfig["schema"] . '"."campaignCPCMessage" ccm, 
			"'.  $dbConfig["schema"] . '"."campaign" c
							WHERE
								"p"."id" = "ccm"."publisher_campaignCPCMessage"
								AND "c"."id" = "ccm"."campaign_campaignCPCMessage"
								AND "c"."race" = 3
								AND "ccm"."linkHash" IN (\'' . $linkHashes . '\');
			';
			JPDO::connect( JCache::read('JDSN.MDB') );
			JPDO::executeQuery($sql);
		}
		//die($sql);
	}

}

