<?php
	/**
	* setting real class for Jeelet - firstly generated on 17-02-2012 15:46, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/setting.php");

	class Setting extends Setting_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}
		
		/**
		 * load setting
		 *
		 * @return mixed
		 * @author Özgür Köy
		 **/
		public function loadSetting($s)
		{
			$this->customClause = "name='$s'";
			$this->limit(1);
			$this->load();
			return $this->value;
		}

		/**
		 * get setting
		 *
		 * @return mixed
		 * @author Murat
		 * change on 130113 by Özgür Köy
		 */
		public static function getValue( $key ) {
			$s = null;

			if ( ( $s = JCache::read( "setting_" . $key ) ) === false ) {
				$setting = new Setting();
				$setting->load( array( "name" => $key ) );
				$s = $setting->value;

				unset( $setting );

				JCache::write( "setting_" . $key, $s, 60 );
			}

			return $s;
		}

		public static function setValue($key,$value){

			$setting = new Setting();
			$setting->load(array("name"=>$key));

			if(!$setting->gotValue)
				$setting->name = $key;
			$setting->value = $value;
			$setting->save();
			unset($setting);
			JCache::write("setting_".$key,$value,60);
		}

		/**
		 * Get all settings
		 *
		 * @return array
		 * @author Murat
		 *
		 *  OZGUR TODO : CHANGE TO JCACHE
		 */
		public static function getAllSettings(){
			$cantConnectToMemcache = false;
			$memcache = new Memcache;
			$memcache->connect(MEMCACHE_SERVER, MEMCACHE_PORT) or $cantConnectToMemcache = true;
			$_settings = array();

			if (!JUtil::isProduction() || !($_settings = $memcache->get(ENVIRONMENT_CODE.'_SettingsValues')) || !is_array($_settings) || count($_settings) < 7) {
				$settings = new Setting();
				$settings->populateOnce(false)->load();
				while($settings->populate()){
					$_settings[$settings->name] = $settings->value;
				}
				unset($settings);
				$memcache->set(ENVIRONMENT_CODE."_SettingsValues", $_settings, MEMCACHE_COMPRESSED, 300);
			}
			if (!$cantConnectToMemcache) $memcache->close();
			return $_settings;
		}
	}

	?>