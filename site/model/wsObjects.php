<?php

/**
 */
use Swagger\Annotations as SWG;


class JsonObj
{
    function __construct($json=NULL) {
        if($json)
            $this->setFromJson($json);
    }

	public function getArrayData( $opt = null ) {
		$var = array();
		if ( is_null( $opt ) ) {
			$opt = &$this;
		}

		foreach ( $opt AS $key => $value ) {
			if ( is_object( $value ) )
				$var[ $key ] = $this->getArrayData( $value );
			elseif ( !empty( $value ) )
				$var[ $key ] = $value;
		}

		return $var;
	}

    public function getJsonData(){
        $var = get_object_vars($this);
        foreach($var as &$value){
           if(is_object($value) && method_exists($value,'getJsonData')){
              $value = $value->getJsonData();
           }
        }
        return $var;
     }
     
     public function setFromJson($data) {
        foreach ($data AS $key => $value){
            $this->{$key} = $value;
        }
    }
}

/**
 * @SWG\Model(id="AcceptResponse")
 */
class AcceptResponse extends Jsonobj
{

	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[309,310,311,312,313,314,315,316,317,318,319,320, null]"))
	 */
	public $error;

    /**
     * @var string
     * @SWG\Property
     */
    public $value = "ok";
    
}

/**
 * @SWG\Model(id="DenyResponse")
 */
class DenyResponse extends Jsonobj
{

	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[309,310,318,319,320, null]"))
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $value = "ok";

}

/**
 * @SWG\Model(id="SingleValueReq")
 */
class SingleValueReq extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $value;
    
}

/**
 * @SWG\Model(id="PairValueReq")
 */
class PairValueReq extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $key;
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $value;

}

/**
 * @SWG\Model
 */
class TwitterLoginReq extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $twitter_id;
            
    /**
     * @var string
     * @SWG\Property
     */
    public $oauth_token;
    
    /**
     * @var string
     * @SWG\Property
     */
    public $oauth_token_secret;

	/**
	 * @var DeviceData
	 * @SWG\Property(type="DeviceData")
	 */
	public $device;
    
}

/**
 * @SWG\Model
 */
class FacebookLoginReq extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $facebook_id;
            
    /**
     * @var string
     * @SWG\Property
     */
    public $facebook_token;

	/**
	 * @var DeviceData
	 * @SWG\Property(type="DeviceData")
	 */
	public $device;
    
}


/**
 * @SWG\Model
 * access_token
token_type
expires_in
id_token
created
 */
class GoogleLoginReq extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
	public $access_token;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $refresh_token;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $token_type;
	/**
	 * @var int
	 * @SWG\Property
	 */
	public $expires_in;
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id_token;
	/**
	 * @var int
	 * @SWG\Property
	 */
	public $created;

	/**
	 * @var DeviceData
	 * @SWG\Property(type="DeviceData")
	 */
	public $device;
}

/**
 * @SWG\Model
 */
class InviteReq extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $email;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $country_id;

	/**
	 * @var DeviceData
	 * @SWG\Property(type="DeviceData")
	 */
	public $device;

}

/**
 * @SWG\Model(id="FullMessagesResponse")
 */
class FullMessagesResponse extends Jsonobj
{

	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[321,322,323, null]"))
	 */
	public $error;

    /**
     * @var string
     * @SWG\Property
     */
    public $messageID;

	/**
	 * @var string some desc
	 * @SWG\Property
	 */
	public $type;

	/**
     * @var string some desc
     * @SWG\Property
     */
    public $title;
    /**
     * @var string
     * @SWG\Property
     */
    public $date;
    
    /**
     * @var string
     * @SWG\Property
     */
    public $message;
    
    /**
     * @var string
     * @SWG\Property
     */
    public $campaign;

	/**
	 * @var string some desc
	 * @SWG\Property
	 */
	public $read;
   
}

/**
 * @SWG\Model
 */
class MessageListRequest extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property
	 */
	public $page;
}

/**
 * @SWG\Model
 */
class MessagesResponse extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[321,322,323, null]"))
	 */
	public $error;

	/**
	 * @var int
	 *
	 * @SWG\Property(description="Count of the all campaign that match criteria")
	 */
	public $recordCount;

	/**
	 * @var int
	 *
	 * @SWG\Property(description="Current page")
	 */
	public $currentPage;

    /**
     * @var array
     * @SWG\Property(name="messages",type="Array", items="$ref:MessageResponse")
     */
    public $messages;

}

/**
 * @SWG\Model(id="MessageResponse")
 */
class MessageResponse extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $messageID;

	/**
	 * @var string some desc
	 * @SWG\Property
	 */
	public $type;

	/**
     * @var string some desc
     * @SWG\Property
     */
    public $title;

    /**
     * @var string
     * @SWG\Property
     */
    public $date;
    
    /**
     * @var boolean
     * @SWG\Property(type="string", @SWG\AllowableValues(valueType="LIST",values="['1', '0']"))
     */
    public $read;
   
}

/**
 * @SWG\Model(id="loginReq")
 */
class loginReq extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property(type="string", description="Mandatory, Also can be en email")
     */
    public $username;
    /**
     * @var string some desc
     * @SWG\Property
     */
    public $password;
    /**
     * @var DeviceData
     * @SWG\Property(type="DeviceData")
     */
    public $device;
    
}

/**
 * @SWG\Model(id="DeviceData")
 */
class DeviceData extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $device_name;
    /**
     * @var string some desc
     * @SWG\Property(type="string", @SWG\AllowableValues(valueType="LIST",values="['android', 'ios', 'windows']"))
     */
    public $device_Platform;
    /**
     * @var string
     * @SWG\Property
     */
    public $device_UUID;

    /**
     * @var string
     * @SWG\Property
     */
    public $device_Model;

    /**
     * @var string
     * @SWG\Property
     */
    public $device_Version;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $device_Token;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $device_Manufacturer;
    
}

/**
 * @SWG\Model(id="SNLoginResponse")
 */
class SNLoginResponse extends Jsonobj
{
	/**
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[606,203, null]"))
	 */
	public $error;

    /**
     * @var boolean
     * @SWG\Property(type="boolean",description="if new user send to sign up, else go to home page")
     */
    public $newUser;

	/**
	 * @var boolean
	 * @SWG\Property(type="boolean",description="if user completed registration, true- goto home page, false -go to profile")
	 */
	public $registrationComplete;

    /**
     * @var string some desc
     * @SWG\Property
     */
    public $userToken;

    
}

/**
 * @SWG\Model(id="settingsResponse")
 */
class settingsResponse extends Jsonobj
{
    /**
     * @var boolean
     * @SWG\Property(type="boolean",description="enable or disable campaign notification")
     */
    public $campaignNotification;

	/**
	 * @var boolean
	 * @SWG\Property(type="boolean",description="enable or disable campaign notification")
	 */
	public $messagesNotification;

	/**
     * @var boolean
     * @SWG\Property(type="int",description="change the user selected language, also for mobile app")
     */
    public $language;
    
    
}

/**
 * @SWG\Model
 */
class validateToken extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property(type="string")
	 */
	public $token;
	/**
	 * @var DeviceData
	 * @SWG\Property(type="DeviceData")
	 */
	public $device;
}

/**
 * @SWG\Model
 */
class twitterToken extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property(type="string")
	 */
	public $token;
	/**
	 * @var string
	 * @SWG\Property(type="string")
	 */
	public $tokenSecret;
}

/**
 * @SWG\Model
 */
class facebookToken extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property(type="string")
	 */
	public $facebookID;

	/**
	 * @var string
	 * @SWG\Property(type="string")
	 */
	public $accessToken;
}

/**
 * @SWG\Model
 */
class signUpRequest extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property(required=true)
     */
    public $username;
    
    /**
     * @var string
     * @SWG\Property(required=true)
     */
    public $password;

	/**
	 * @var string
	 * @SWG\Property(required=true)
	 */
	public $email;
	/**
	 * @var string
	 * @SWG\Property(required=true)
	 */
	public $name;

	/**
	 * @var string
	 * @SWG\Property(required=true)
	 */
	public $surname;
	/**
	 * @var DeviceData
	 * @SWG\Property(type="DeviceData")
	 */
	public $device;


}

/**
 * @SWG\Model
 */
class SignUpResponse extends Jsonobj
{
	/**
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[606,203, null]"))
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $Token;
}
/**
 * @SWG\Model
 */
class LoginResponse extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[606,203, null]"))
	 */
	public $error;

    /**
     * @var string
     * @SWG\Property
     */
    public $Token;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $userName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $userNameSurname;

	/**
	 * @var bool
	 * @SWG\Property(type="boolean", description="true - go to dashboard, false - go to profile page and display a message that user need to complete registration "))
	 */
	public $registrationComplete;
    
}

/**
 * @SWG\Model(id="ErrResponse")
 */
class ErrResponse extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property(type="int", description="Error code"))
	 */
	public $errorCode;

	/**
	 * @var string
	 * @SWG\Property(description="Error text (replacement of code)"))
	 */
	public $errorText;

	/**
	 * @var string
	 * @SWG\Property(description="Error detail"))
	 */
	public $errorDetail;

	/**
	 * @var array
	 * @SWG\Property(description="If error message needs to contain parameters, this property will be an array of those params"))
	 */
	public $errorParams;

	public function __construct($errorCode, $errorDetail = null){
		$this->errorCode = $errorCode;
		$this->errorText = RestResponse::responseCodeMessage($errorCode);
		$this->errorDetail = $errorDetail;
	}
}

/**
 * @SWG\Model
 */
class signUpDataRequest extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property(required=false, description="only on update")
	 */
	public $email;

	/**
	 * @var string
	 * @SWG\Property(required=true, description="format: yyyy-mm-dd")
	 */
	public $bDay;

	/**
     * @var int
     * @SWG\Property
     */
    public $country;
    
    /**
     * @var int
     * @SWG\Property
     */
    public $city;
    
    /**
     * @var string
     * @SWG\Property
     */
    public $address;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $name;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $surname;

	/**
	 * @var string
	 * @SWG\Property(type="signUpPhoneValidateRequest")
	 */
	public $gsm;
    
    /**
     * //@var int
     * //@SWG\Property

    //public $education;
    

     * //@var int
     * //@SWG\Property

    //public $professional;
    */

    /**
     * @var array
     * @SWG\Property(type="List")
     */
    public $interests;

	/**
	 * @var string
	 * @SWG\Property(required=true, description="1: Mr, 2: Ms, 3:Dr", @SWG\AllowableValues(valueType="LIST",values="['1', '2', '3']"))
	 */
	//public $title;

	/**
	 * @var string
	 * @SWG\Property(required=true, description="1: Male, 2: Female", @SWG\AllowableValues(valueType="LIST",values="['1', '2']"))
	 */
	public $gender;

	/**
	 * @var string
	 * @SWG\Property
	 */

	//public $otherInfo;
}

/**
 * @SWG\Model
 */
class signUpPhoneValidateRequest extends Jsonobj
{
	/**
	 * @var number
	 * @SWG\Property(required=true, description=" number"))
	 */
	public $gsmNumber;

	/**
	 * @var number
	 * @SWG\Property(required=true, description="country code"))
	 */
	public $gsmCode;

}

/**
 * @SWG\Model
 */
class signUpPhoneValidateResponse extends Jsonobj
{
	/**
	 * @var bool
	 * @SWG\Property(required=true, description="country code - number"))
	 */
	public $saved;

	/**
	 * @var bool
	 * @SWG\Property
	 */
	public $smsSent;

}


/**
 * @SWG\Model
 */
class UserProfileResponse extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $email;
    /**
     * @var string
     * @SWG\Property
     */
    public $username;
    
    /**
     * @var boolean
     * @SWG\Property
     */
    public $fb;
    
    /**
     * @var boolean
     * @SWG\Property
     */
    public $tw;
    
    /**
     * @var boolean
     * @SWG\Property
     */
    public $gp;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $totalRevenue;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $campaigns;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $socialConnection;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $trueReach;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $publisherLevel;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $avatar;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $background;

    /**
     * @var signUpDataRequest
     * @SWG\Property(type="signUpDataRequest")
     */
    public $signUpDataRequest;
     
}

/**
 * @SWG\Model
 */
class CampaignCreateRequest extends Jsonobj{

	/**
	 * @var string
	 * @SWG\Property(description="advertiser ID, can be null")
	 */
	public $advertiserId;

	/**
	 * @var string
	 * @SWG\Property(@SWG\AllowableValues(valueType="LIST",values="{'1':'Effect','3':'CPC'}"))
	 */
	public $race;

	/**
	 * @var string
	 * @SWG\Property(description="Campaign Title")
	 */
	public $title;

	/**
	 * @var string
	 * @SWG\Property(description="Campaign Brand Name")
	 */
	public $brandName;

	/**
	 * @var string
	 * @SWG\Property(description="Campaign Start Date (Format: Y-m-d)")
	 */
	public $startDate;

	/**
	 * @var string
	 * @SWG\Property(description="Campaign End Date (Format: Y-m-d)")
	 */
	public $endDate;

	/**
	 * @var string
	 * @SWG\Property(description="Campaign Link")
	 */
	public $link;

	/**
	 * @var string
	 * @SWG\Property(description="Campaign Notes")
	 */
	public $campNotes;

	/**
	 * @var array
	 * @SWG\Property(description="Campaign Messages",type="Array")
	 */
	public $messages;
}

/**
 * @SWG\Model(id="CampaignCreateResponse")
 */
class CampaignCreateResponse extends Jsonobj
{

	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[301,302,303,304,305,306,307,308, null]"))
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $value = "ok";

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $campaignId;

}

/**
 * @SWG\Model(id="ChangeCampaignStatusRequest")
 */
class ChangeCampaignStatusRequest extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property(description="Campaign ID")
	 */
	public $publicID;

	/**
	 * @var string
	 * @SWG\Property(@SWG\AllowableValues(valueType="LIST",values="{'8':'Pending Progress','9':'Progressing','10':'Finished'}"))
	 */
	public $status;

}

/**
 * @SWG\Model
 */
class CampaignAcceptRequest extends Jsonobj
{
    
    /**
     * @var string
     * @SWG\Property(description="Camp publicID")
     */
    public $campID;

	/**
	 * @var string
	 * @SWG\Property(description="Tweet send date for effect campaigns. Format: Y-m-d")
	 */
	public $sendDate;

	/**
	 * @var string
	 * @SWG\Property(description="Tweet send date for effect campaigns. Format: H:i")
	 */
	public $sendTime;

	/**
	 * @var string
	 * @SWG\Property(description="If publisher set sets his own message, this property should be Y", @SWG\AllowableValues(valueType="LIST",values="['Y', 'N']"))
	 */
	public $setCustomMessage;

	/**
	 * @var string
	 * @SWG\Property(description="Publisher custom message")
	 */
	public $customMessage;

	/**
	 * @var int
	 * @SWG\Property(description="Selected message id")
	 */
	public $messageId;
}

/**
 * @SWG\Model
 */
class CampaignDenyRequest extends Jsonobj
{

	/**
	 * @var string
	 * @SWG\Property(description="Camp publicID")
	 */
	public $campID;

	/**
	 * @var int
	 * @SWG\Property(description="Deny reason ID")
	 */
	public $reasonId;

	/**
	 * @var string
	 * @SWG\Property(description="Free text deny reason")
	 */
	public $reasonText;
}



/**
 * @SWG\Model
 */
class CampaignListRequest extends Jsonobj
{

    /**
     * @var string
     * @SWG\Property(@SWG\AllowableValues(valueType="LIST",values="['o', 'a', 'f', 'r']"))
     */
    public $campStatus;

	/**
	 * @var string
	 * @SWG\Property(@SWG\AllowableValues(valueType="LIST",values="['date', 'title', 'state','race']"))
	 */
	public $orderBy;
    
    /**
     * @var int
     * @SWG\Property
     */
    public $page;
}

/**
 * @SWG\Model
 */
class CampaignAllListRequest extends Jsonobj
{

	/**
	 * @var string
	 * @SWG\Property(@SWG\AllowableValues(valueType="LIST",values="['date', 'title', 'state','race']"))
	 */
	public $orderBy;

	/**
	 * @var int
	 * @SWG\Property
	 */
	public $page;
}


/**
 * @SWG\Model
 */
class CampaignsResponse extends Jsonobj
{

	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[321,322,323, null]"))
	 */
	public $error;

	/**
	 * @var int
	 *
	 * @SWG\Property(description="Count of the all campaign that match criteria")
	 */
	public $recordCount;

	/**
	 * @var int
	 *
	 * @SWG\Property(description="Current page")
	 */
	public $currentPage;
    
    /**
     * @var string
     * @SWG\Property(type="Array", items="$ref:CampaignResponse")
     */
    public $campaigns;
}

/**
 * @SWG\Model
 */
class CampaignResponse extends Jsonobj
{
    
    /**
     * @var string
     * @SWG\Property
     */
    public $id;
    
    
    /**
     * @var string
     * @SWG\Property(@SWG\AllowableValues(valueType="LIST",values="{'1':'Twitter','3':'CPC'}"))
     */
    public $race;
    
    
    /**
     * @var string
     * @SWG\Property
     */
    public $title;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $imgUrl;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $price;
    
    /**
     * @var string
     * @SWG\Property(@SWG\AllowableValues(valueType="LIST",values="{'o':'Waiting Offer','a':'Active','f':'Finished','r':'Other'}"))
     */
    public $status;
    
    /**
     * @var string
     * @SWG\Property
     */
    public $date;
}

/**
 * @SWG\Model
 */
class CampaignDetailResponse extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $id;

	/**
	 * @var string
	 * @SWG\Property(type="string", description="Campaign race", @SWG\AllowableValues(valueType="LIST",values="{'1':'Effect','3':'CPC'}"))))
	 */
	public $race;

    /**
     * @var string
     * @SWG\Property(type="string", description="State of campaign detail",
     * @SWG\AllowableValues(valueType="LIST",values="{'2':'Waiting User Action', '5':'Approved', '6':'Rejected', '7':'Late To Join', '8':'Budget Ended'}"))
     */
    public $state;

	/**
	 * @var string
	 * @SWG\Property(type="string", description="State of campaign",
	 * @SWG\AllowableValues(valueType="LIST",values="{'8':'Waiting To Start', '9':'Campaign Active', '10':'Campaign Ended'}"))
	 */
	public $cState;

    /**
     * @var float
     * @SWG\Property(description="For effect campaign: Revenue per tweet, For CPC campaign: Revenue per click")
     */
    public $revenue;

    /**
     * @var string
     * @SWG\Property(description="Campaign title")
     */
    public $campTitle;

    /**
     * @var string
     * @SWG\Property(description="Campaign link")
     */
    public $campLink;

    /**
     * @var string
     * @SWG\Property(description="Campaign brand name")
     */
    public $brand;

    /**
     * @var string
     * @SWG\Property(description="Campaign start date. Format: Y-m-d H:i")
     */
    public $startDate;

    /**
     * @var string
     * @SWG\Property(description="Campaign end date. Format: Y-m-d H:i")
     */
    public $endDate;


    /**
     * @var string
     * @SWG\Property(description="Campaign notes for publishers that has permission for custom message")
     */
    public $campaignNotes;


    /**
     * @var bool
     * @SWG\Property(description="If publisher has permission for custom message, this proeprty will be true else false")
     */
    public $editMessage;

	/**
	 * @var int
	 * @SWG\Property(description="Calculated click count (For only CPC campaign)")
	 */
	public $calculatedClick;

    /**
     * @var int
     * @SWG\Property(description="Total click count")
     */
    public $totalClick;

    /**
     * @var ClickStats
     * @SWG\Property(type="Array", items="$ref:ClickStats")
     */
    public $clickStats;

    /**
     * @var string
     * @SWG\Property(description="Max character count for the publisher that has permission for custom message")
     */
    public $charLimit;


    /**
     * @var PairValueReq
     * @SWG\Property(type="Array", items="$ref:PairValueReq")
     */
    public $campaignMessages;

    /**
     * @var CampaignTweetMessage
     * @SWG\Property(description="If campaign race is effect and user approved the campaign, this property will be filled otherwise null")
     */
    public $tweetMessage;

	/**
	 * @var string
	 * @SWG\Property(description="Cost per Click for CPC campaigns")
	 */
	public $cpcCost;

	/**
	 * @var strstringng
	 * @SWG\Property(description="Total revenue for CPC campaigns")
	 */
	public $totalRevenue;

	/**
	 * @var string
	 * @SWG\Property(description="Spesific link for publisher")
	 */
	public $cpcLink;


	/**
	 * @var string
	 * @SWG\Property(description="Image Url for Campaign")
	 */
	public $imageLink;


}

/**
 * @SWG\Model
 */
class ClickStats{
	/**
	 * @var int
	 * @SWG\Property(description="Stat date unix timestamp")
	 */
	public $clickDate;

	/**
	 * @var int
	 * @SWG\Property(description="Calculated click count")
	 */
	public $calculatedClick;

	/**
	 * @var int
	 * @SWG\Property(description="Suspected click count")
	 */
	public $suspectedClick;

	/**
	 * @var int
	 * @SWG\Property(description="Total click count")
	 */
	public $totalClick;
}

/**
 * @SWG\Model
 */
class CampaignTweetMessage extends Jsonobj{
	/**
	 * @var string
	 * @SWG\Property(description="Selected campaign message with link and hashtag")
	 */
	public $message;

	/**
	 * @var string
	 * @SWG\Property(description="Tweet send date. before sending Format: Y-m-d H:i")
	 */
	public $sendDate;

	/**
	 * @var string
	 * @SWG\Property(description="Tweet send date. Format: Y-m-d H:i")
	 */
	public $toSendDate;

	/**
	 * @var string
	 * @SWG\Property(type="string", description="State of campaign message",
	 * @SWG\AllowableValues(valueType="LIST",values="{'1':'Waiting', '2':'Tweet Sent', '3':'Tweet Sending', '4':'Tweet Deleted', '5':'Tweet checking', '6':'Tweet Exist', '7':'Waiting Admin Approve', '8':'Status duplicate'}"))
	 */
	public $state;
}
/**
 * @SWG\Model
 */
class TransferDetailsResponse extends Jsonobj
{
    
    /**
     * @var string
     * @SWG\Property
     */
    public $id;
    
    
    /**
     * @var string
     * @SWG\Property
     */
    public $bank;
    
    
    /**
     * @var string
     * @SWG\Property
     */
    public $info;
    
    /**
     * @var string
     * @SWG\Property
     */
    public $amount;
    
    /**
     * @var string
     * @SWG\Property
     */
    public $state;
    
    /**
     * @var Date
     * @SWG\Property
     */
    public $date;
  
}

/**
 * @SWG\Model
 */
class PublisherPaymentOptionsResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $timestamp;

    /**
     * @var string
     * @SWG\Property(type="Array", items="$ref:PublisherPaymentOptionResponse")
     */
    public $publisherWithdrawOptions;
    
}

/**
 * @SWG\Model
 */
class PublisherPaymentOptionResponse extends Jsonobj
{

	/**
	 * @var id
	 * @SWG\Property
	 */
	public $id;


	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $WithdrawOptionID;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $minWithdrawAmount;


	/**
	 * @var string
	 * @SWG\Property
	 */
	public $maxWithdrawAmount;

	/**
	 * @var bool
	 * @SWG\Property
	 */
	public $canWithdraw;

	/**
	 * @var bool
	 * @SWG\Property
	 */
	public $canWithDrawOnlyMax;

}


/**
 * @SWG\Model
 */
class MoneyActionsRequest extends Jsonobj
{

    /**
     * @var int
     * @SWG\Property
     */
    public $page=0;

}
/**
 * @SWG\Model
 */
class MoneyActionsResponse extends Jsonobj
{
    /**
     * @var int
     * @SWG\Property
     */
    public $currentPage;

    /**
     * @var int
     * @SWG\Property
     */
    public $pageCount;

	/**
     * @var int
     * @SWG\Property
     */
    public $recordCount;


    /**
     * @var array
     * @SWG\Property(type="Array", items="$ref:MoneyActionResponse")
     */
    public $moneyActions;
}


/**
 * @SWG\Model
 */
class MoneyActionDetailResponse extends Jsonobj
{

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property(description="'PENDING':1 'APPROVED':2 'CANCELLED':3")
	 */
	public $state;

	/**
     * @var int
     * @SWG\Property
     */
    public $id;

    /**
     * @var string
     * @SWG\Property
     */
    public $displayName;

    /**
     * @var int
     * @SWG\Property
     */
    public $amount;

	/**
     * @var PublisherBillingInfo
     * @SWG\Property
     */
    public $billing;

    /**
     * @var string
     * @SWG\Property(type="JSON", description="details of the account info")
     */
    public $details;

    /**
     * @var int
     * @SWG\Property
     */
    public $unixTime
    ;

}

/**
 * @SWG\Model
 */
class WithdrawOptionsResponse extends Jsonobj
{

    /**
     * @var string
     * @SWG\Property
     */
    public $timestamp;


    /**
     * @var string
     * @SWG\Property(type="Array", items="$ref:WithdrawOptionResponse")
     */
    public $withdrawOptions;

}

/**
 * @SWG\Model
 */
class WithdrawOptionResponse extends Jsonobj
{

    /**
     * @var string
     * @SWG\Property
     */
    public $id;


    /**
     * @var string
     * @SWG\Property
     */
    public $nameCode;

	/**
     * @var string
     * @SWG\Property(description="Json with the fields for each payment option wordcode:validationType, validation - 1:number only 2:email 3:text")
     */
    public $fields;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $type;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $message;

}


/**
 * @SWG\Model(id="FinanceSummaryResponse")
 */
class FinanceSummaryResponse extends Jsonobj
{


	/**
	 * @var int
	 * @SWG\Property
	 */
	public $pendingWithdrawal;


	/**
	 * @var int
	 * @SWG\Property
	 */
	public $currentTotal;

}

/**
 * @SWG\Model(id="PublisherPaymentOptionActionResponse")
 */
class PublisherPaymentOptionActionResponse extends Jsonobj
{

	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null")
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $value = "ok";

}


/**
 * @SWG\Model
 */
class PublisherPaymentOptionActionRequest extends Jsonobj
{
    /**
     * @var int
     * @SWG\Property
     */
    public $id;


    /**
     * @var string
     * @SWG\Property
     */
    public $displayName;

    /**
     * @var int
     * @SWG\Property
     */
    public $publisherWithdrawOptionId;

    /**
     * @var int
     * @SWG\Property
     */
    public $bank;

    /**
     * @var string
     * @SWG\Property
     * {
    "id": "11",
    "displayName": "dsfsf sdf a",
    "publisherWithdrawOptionId": "1",
    "bank": "1",
    "details": "{\"IBAN\":\"TR34 0006 2000 7230 0006 6996 79\"}",
    "billingInfo": {
    "billingName": "aaa",
    "taxOffice": "bb",
    "taxNumber": "ccc",
    "address": "dddd"
    }
    }
     */
    public $details;

    /**
     * @var PublisherBillingInfo
     * @SWG\Property(type="PublisherBillingInfo")
     */
    public $billingInfo;
}

/**
 * @SWG\Model
 */
class PublisherWithdrawalRequest extends Jsonobj
{
    /**
     * @var int
     * @SWG\Property
     */
    public $paymentOptionId;

    /**
     * @var int
     * @SWG\Property
     */
    public $amount;

}

/**
 * @SWG\Model
 */
class PublisherWithdrawalResponse extends Jsonobj
{

	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null")
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $value = "ok";

}

/**
 * @SWG\Model
 */
class PublisherBillingInfo extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $billingName;


    /**
     * @var string
     * @SWG\Property
     */
    public $taxOffice;

    /**
     * @var string
     * @SWG\Property
     */
    public $taxNumber;

    /**
     * @var string
     * @SWG\Property
     */
    public $address;
}



/**
 * @SWG\Model
 */
class PublisherBanksResponse extends Jsonobj
{
     /**
     * @var string
     * @SWG\Property
     */
    public $banks;
 
}
/**
 * @SWG\Model
 */
class PublisherBankResponse extends Jsonobj
{
    /**
     * @var string
     * @SWG\Property
     */
    public $id;
    
    
    /**
     * @var string
     * @SWG\Property
     */
    public $DisplayName;
    
    
    /**
     * @var string
     * @SWG\Property
     */
    public $AccountHolder;

    /**
     * @var string
     * @SWG\Property
     */
    public $bankID;
}

/**
 * @SWG\Model
 */
class CountriesResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $timestamp;


	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:ActiveCountryResponse")
	 */
	public $countries;
}
/**
 * @SWG\Model
 */
class ActiveCountryResponse extends Jsonobj
{

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $iso;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $countryName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $inviteMode;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $countryUrl;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $phone;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $oauth;
}

/**
 * @SWG\Model
 */
class StaticDataResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $timestamp;

	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:InterestsResponse")
	 */
	public $interests;

	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:CityResponse")
	 */
	public $city;

	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:BankResponse")
	 */
	public $bank;


	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:siteSettingsResponse")
	 */
	public $settings;

	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:LanguagesResponse")
	 */
	public $languages;


}
/**
 * @SWG\Model
 */
class LanguagesResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $shortCode;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $shortCodeIso;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $default;
}

/**
 * @SWG\Model
 */
class InterestsResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $active;
}

/**
 * @SWG\Model
 */
class EducationResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $active;
}

/**
 * @SWG\Model
 */
class ProfessionalResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $active;
}

/**
 * @SWG\Model
 */
class CityResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $country;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $active;
}

/**
 * @SWG\Model
 */
class CountryResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $iso;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $active;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $inviteMode;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $countryUrl;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $phone;
}

/**
 * @SWG\Model
 */
class BankResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $displayName;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $active;
}

/**
 * @SWG\Model
 */
class siteSettingsResponse extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $key;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $value;
}

/**
 * @SWG\Model
 */
class AndroidPushRequest extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property(description="Device Push Key")
	 */
	public $registration_id;

	/**
	 * @var string
	 * @SWG\Property(type="AndroidPushRequestData")
	 */
	public $data;
}

/**
 * @SWG\Model
 */
class AndroidPushRequestData extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $header;

	/**
	 * @var string
	 * @SWG\Property(required=true, description="m: User Message, C: Campaign type", @SWG\AllowableValues(valueType="LIST",values="['c', 'm']"))
	 */
	public $type;

	/**
	 * @var string
	 * @SWG\Property(type="string", description="public id for user messages or campaign")
	 */
	public $itemId;

}

/**
 * @SWG\Model
 */
class CouponDepartmentsResponse extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[321,322,323, null]"))
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:CouponDepartmentResponse")
	 */
	public $Departments;
}

/**
 * @SWG\Model
 */
class CouponDepartmentResponse extends Jsonobj
{
	/**
	 * @var int
	 *
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $DisplayName;
}

/**
 * @SWG\Model
 */
class CouponsResponse extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[321,322,323, null]"))
	 */
	public $error;

	/**
	 * @var string
	 * @SWG\Property(type="Array", items="$ref:CouponResponse")
	 */
	public $coupons;
}

/**
 * @SWG\Model
 */
class CouponResponse extends Jsonobj
{
	/**
	 * @var int
	 *
	 * @SWG\Property
	 */
	public $id;

	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $title;


	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $imageUrl;


	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $description;


	/**
	 * @var int
	 *
	 * @SWG\Property
	 */
	public $price;

	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $formattedPrice;
}

/**
 * @SWG\Model
 */
class BuyCouponResponse extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property(type="ErrResponse", description="If there's an error, this property show the error in ErrResponse object; otherwise it will be null", @SWG\AllowableValues(valueType="LIST",values="[321,322,323, null]"))
	 */
	public $error;

	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $couponCode;

}

/**
 * @SWG\Model
 */
class ImageRequest extends Jsonobj
{
	/**
	 * @var int
	 * @SWG\Property(type="string", description="Type Of Image",
	 * @SWG\AllowableValues(valueType="LIST",values="{'1':'Profile', '2':'Background'}"))
	 */
	public $type;

	/**
	 * @var file
	 *
	 * @SWG\Property(type="file")
	 */
	public $file;

}

/**
 * @SWG\Model
 */
class ContactUsRequest extends Jsonobj
{
	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $subject;

	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $email;

	/**
	 * @var string
	 *
	 * @SWG\Property
	 */
	public $message;
}

/**
 * @SWG\Model
 */
class messageActionRequest extends Jsonobj
{
	/**
	 * @var array
	 * @SWG\Property(type="List")
	 */
	public $messagesId;

	/**
	 * @var string
	 * @SWG\Property(required=true, description="r: mark as read, d: delete", @SWG\AllowableValues(valueType="LIST",values="['r', 'd']"))
	 */
	public $action;

}

/**
 * @SWG\Model
 * For testing only
 */
class sendMessageRequest extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $body;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $header;

	/**
	 * @var string
	 * @SWG\Property
	 */
	public $campaign;

}



/**
 * @SWG\Model
 * access_token
token_type
expires_in
id_token
created
 */
class VKLoginReq extends Jsonobj
{
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $access_token;
	/**
	 * @var int
	 * @SWG\Property
	 */
	public $expires_in;
	/**
	 * @var string
	 * @SWG\Property
	 */
	public $user_id;

	/**
	 * @var DeviceData
	 * @SWG\Property(type="DeviceData")
	 */
	public $device;
}