<?php
	/**
	* celebrityPreview real class for Jeelet - firstly generated on 17-02-2012 14:39, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/celebrityPreview.php");

	class CelebrityPreview extends CelebrityPreview_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * Get CelebrityPreview Formatted for content
		 *
		 * @return array
		 * @author Murat Hoşver
		 */
		public function getCelebrityPreviewFormatted(){
			$cp = new CelebrityPreview();
			$cp->customClause = 'status=10';
			$cp->limit(10);
			$cp->nopop()->load();
			$cpa = array();
			while ($cp->populate()) {
				$cpa[] = array(
					'thumb'=>$cp->thumb,
					'facebook'=>$cp->facebook,
					'linkedin'=>$cp->linkedin,
					'twitter'=>$cp->twitter,
					'rss'=>$cp->rss,
					'fullname'=>$cp->fullname,
					'location'=>$cp->location,
					'definition'=>$cp->definition,
					'web'=>$cp->web,
					'followers'=>$cp->followers,
					'influence'=>$cp->influence
				);
			}
			return $cpa;
		}
	}

	?>