<?php
/**
 * club real class - firstly generated on 31-01-2014 00:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/club.php';

class Club extends Club_base
{

	public static $siteImagePathTmp = "site/layout/images/club/tmp/";
	public static $siteImagePath = "site/layout/images/club/";

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function getClub($publicKey,$privateKey){
		$a = new Club();

		$a->limit(0,1)->load(array("clubPublicKey"=>$publicKey,"clubPrivateKey"=>$privateKey,"active"=>1,"enableRegistration"=>1));
		if($a->gotValue){
			return $a;
		}

		return false;

	}

	/**
	 * Get all active clubs
	 *
	 * @param bool $asObject
	 *
	 * @return Club|array
	 *
	 * @author Murat
	 */
	public static function getActiveClubsCount(){
		$c = new Club();
		$c->count("id","cnt",array("active"=>1));
		return $c->cnt;
	}

	/**
	 * Get all active clubs
	 *
	 * @param bool $asObject
	 *
	 * @return Club|array
	 *
	 * @author Murat
	 */
	public static function getActiveClubs($asObject=true){
		$c = new Club();
		$c->populateOnce(false)->load(array("active"=>1));

		if ($asObject) {
			return $c;
		} else {
			$clubs = array();
			while($c->populate()) {
				$clubs[$c->id] = $c->clubName;
			}
			return $clubs;
		}
	}

	/**
	 * Get commissions and payments
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public function getCommissions(){

		$payments = array();
		$finalBalance = 0;

		$this->loadClub_clubMoneyAction();
		if ($this->club_clubMoneyAction->gotValue){
			do {
				if ($this->club_clubMoneyAction->deleted != 1){
					$payments[] = array(
						"id" => $this->club_clubMoneyAction->id,
						"date" => $this->club_clubMoneyAction->paymentDate,
						"actionType" => $this->club_clubMoneyAction->actionType_options[$this->club_clubMoneyAction->actionType],
						"note" => $this->club_clubMoneyAction->note,
						"amount" => $this->club_clubMoneyAction->amount
					);
					$finalBalance += $this->club_clubMoneyAction->amount;
				}
			} while($this->club_clubMoneyAction->populate());
		}
		if (count($payments))
			$payments = Format::array_orderby($payments,"date",SORT_ASC,"actionType",SORT_ASC);

		return array("finalBalance" => $finalBalance,"payments" => $payments);
	}

	/**
	 * Upload temporary picture for new/edit club
	 *
	 * @param array $file
	 * @return array
	 * @author Murat
	 */
	public static function uploadTempPicture($file){
		global $__DP;

		// image name
		$name = "club_" . JUtil::randomString(20) . ".";

		// location to save cropped image
		$url = $__DP.self::$siteImagePathTmp.$name;

		$dst_x = 0;
		$dst_y = 0;

		$src_x = $file['x']; // crop Start x
		$src_y = $file['y']; // crop Start y

		$src_w = $file['w']; // $src_x + $dst_w
		$src_h = $file['h']; // $src_y + $dst_h

		// set a specific size for the image
		// the default is to grab the width and height from the cropped image.
		$dst_w = 570;
		$dst_h = 345;

		// remove the base64 part
		$base64 = $file['image'];
		$image = null;


		// if URL is a base64 string
		if (substr($base64, 0, 5) == 'data:') {

			// remove data from image
			$base64 = preg_replace('#^data:image/[^;]+;base64,#', '', $base64);

			$base64 = base64_decode($base64);

			// create image from string
			$source = imagecreatefromstring($base64);
			imagealphablending($source, FALSE);
			imagesavealpha($source, TRUE);

			$url .= Validate::getImageMimeType($base64);
		}
		else {

			// strip parameters from URL
			$base64 = strtok($base64, '?');

			list($height, $width, $type) = getimagesize($base64);

			// create image
			if ($type == 1){
				$source = imagecreatefromgif($base64);
				$url .= "gif";
			}else if ($type == 2){
				$source = imagecreatefromjpeg($base64);
				$url .= "jpg";
			}else if ($type == 3) {
				$source = imagecreatefrompng($base64);
				$url .= "png";

				// keep transparent background
				imagealphablending($image, FALSE);
				imagesavealpha($image, TRUE);

			}
			else die();

		}

		// resize image variable
		$image = imagecreatetruecolor($dst_w, $dst_h);

		// process cropping and resizing of image
		imagecopyresampled($image, $source, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

		// save image
		imagejpeg($image, $url, 100);

		$fileName = $name . substr($url,-3);
		$_SESSION["tempCampPicture"] = array(
			"fullPath" => $url,
			"fileName" => $fileName
		);

		// return URL
		$validation = array (
			'url'     => JUtil::siteAddress() . self::$siteImagePathTmp . $fileName
		);

		return $validation;
	}

}

