<?php
/**
 * banks real class - firstly generated on 07-03-2013 14:42, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/banks.php';

class Banks extends Banks_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}
	
	public static function getAllBanks(){
		$banks = new Banks();
		$banks->country = COUNTRY_CODE;
		$banks->orderBy = "bankName";
		$banks->nopop()->load(null,0,100000);
		
		$bankNames = array();
		while($banks->populate()){
			$bankNames[$banks->id] = $banks->bankName;
		}
		unset($banks);
		return $bankNames;
	}
	
	public static function getAllBankNames(){
		$banks = new Banks();
		$banks->country = COUNTRY_CODE;
		$banks->orderBy = "bankName";
		$banks->nopop()->load(null,0,100000);
		
		$bankNames = array();
		while($banks->populate()){
			$bankNames[] = $banks->bankName;
		}
		unset($banks);
		return $bankNames;
	}

}

