<?php
/**
 * cron real class for Jeelet - firstly generated on 12-03-2012 19:23, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include($__DP."/site/model/base/cron.php");

class Cron extends Cron_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null)
	{
		parent::__construct($id);
	}

	/**
	 * Marks cronjob as processing
	 *
	 * @param type $cronId
	 */
	public static function startProcess($cronId) {
		$c = new Cron();
		$c->update(
			array("id" => $cronId),
			array(
				"pStartDate" => time(),
				"state" => Cron::ENUM_STATE_PROCESSING
			)
		);
		unset($c);
	}

	/**
	 * Marks cronjob as processing
	 *
	 * @param type $cronId
	 */
	public static function resetProcess($cronId) {
		$c = new Cron($cronId);
		if ($c->gotValue){
			$c->state = Cron::ENUM_STATE_WAITING;
			$c->trycount++;
			$c->pStartDate = null;
			$c->save();
		}
		unset($c);
	}

	/**
	 * Marks cronjob as processed
	 *
	 * @param type $cronId
	 */
	public static function endProcess($cronId) {
		$c = new Cron();
		$c->update(
			array("id" => $cronId),
			array(
				"pEndDate" => time(),
				"state" => Cron::ENUM_STATE_SUCCESSFUL
			)
		);
		unset($c);
	}

	/**
	 * Marks cronjob as failed
	 *
	 * @param type $cronId
	 */
	public static function failProcess($cronId) {
		$c = new Cron($cronId);
		if ($c->gotValue){
			$c->state = Cron::ENUM_STATE_ERROR;
			$c->trycount++;
			$c->pEndDate = time();
			$c->save();
		}
		unset($c);
	}
	
	/**
	 * Manually pass the given type cron jobs to RabbitMQ
	 * 
	 * @return void
	 * @author Murat
	 */
	public static function forcedAddToRabbitMQ($type=null,$id=null){
		global $__DP, $debugLevel;
		JPDO::beginTransaction();
		try{
			$cron = new Cron();
			$filter = array(
				"state" => array(Cron::ENUM_STATE_WAITING,Cron::ENUM_STATE_ERROR),
				"targetDate" => "&le " . time(),
				"trycount" => "&lt 10"
			);
			if (!is_null($type) && in_array($type,$cron->type_options)){
				$filter["type"] = $type;
			}
			if (!is_null($id)){
				$filter["id"] = $id;
			}
			//JPDO::debugQuery(true);
			$counter = new Cron();
			$counter->count("id","cnt",$filter);
			$cnt = 0;
			if ($counter->cnt){
				if($debugLevel > 0) Tool::echoLog("Waiting job count: " . $counter->cnt);
				while($counter->cnt > 0){
					$cron = new Cron();
					$cron->orderBy("targetDate")->populateOnce(false)->load($filter);
					//exit;
					if ($cron->gotValue){
						$rmq = new RabbitMQ();
						$rmq->declareQueue(RMQ_QUEUE_EMAIL);
						$rmq->declareQueue(RMQ_QUEUE_TWITTER);
						$rmq->declareQueue(RMQ_QUEUE_CAMPAIGN);
						do{
							$cron->adding();
							$msg = json_encode(Array("id" => $cron->id, "type" => $cron->type, "campaign" => $cron->campaign_cron, "publisher" => $cron->publisher_cron, "msgindex" => $cron->msgindex, "additionalData" => $cron->additionalData));
							switch($cron->type){
								case Cron::ENUM_TYPE_TWEET_CHECK:
								case Cron::ENUM_TYPE_TWEET_SEND:
									$rmq->publishMessage($msg,RMQ_QUEUE_TWITTER);
									break;
								case Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_NORMAL:
								case Cron::ENUM_TYPE_CAMPAIGN_AVAILABLE_EMAIL_TRENDSETTER:
								case Cron::ENUM_TYPE_CAMPAIGN_START:
								case Cron::ENUM_TYPE_CAMPAIGN_END:
								case Cron::ENUM_TYPE_CAMPAIGN_FINISH_ALERT:
								case Cron::ENUM_TYPE_CAMPAIGN_BUDGET_CHECK:
								case Cron::ENUM_TYPE_GET_PUBLISHER_CLICK_STATS:
								case Cron::ENUM_TYPE_LAST_FRAUD_CHECK:
								case Cron::ENUM_TYPE_CPL_REMOTE_FINISH:
								case Cron::ENUM_TYPE_CPA_AFF_FINISH:
									$rmq->publishMessage($msg,RMQ_QUEUE_CAMPAIGN);
									break;
								default:
									$rmq->publishMessage($msg,RMQ_QUEUE_EMAIL);
									break;
							};
							if($debugLevel > 0) {
								Tool::echoLog("Message published :" . $msg);
							}

							$cron->added();
							$cnt++;
						} while($cron->populate());
					}

					$counter->count("id","cnt",$filter);
				}
				if($debugLevel > 0) {
					Tool::echoLog("Scheduled cron job count: $cnt");
				}
			} else {
				if($debugLevel > 0) {
					Tool::echoLog('No crons to run');
				}
			}
			JPDO::commit();
		} catch(JError $ex){
			JPDO::rollback();
			JLog::log("cri","Error occured on Cron::forcedAddToRabbitMQ : " . print_r($ex,true),0,true);
			Tool::echoLog("Error occured: " . print_r($ex,true));
		}
	}

	/**
	 * Create cron job
	 *
	 * @return void
	 * @author Murat
	 */
	public static function create($type,$targetDate,$user,$campaign,$additionalData=null,$msgindex=0,$state=Cron::ENUM_STATE_WAITING){
		$c = new Cron();
		$c->type = $type;
		$c->state = $state;
		$c->targetDate = $targetDate;
		$c->publisher_cron = $user;
		$c->campaign_cron = $campaign;
		$c->msgindex = $msgindex;
		$c->additionalData = $additionalData;
		$c->trycount = 0;
		$c->executeCount = 0;
		$c->save();
		unset($c);
	}

	/**
	 * Adding cronjob to RabbitMQ Que
	 *
	 * @return void
	 * @author Murat
	 */
	public function adding(){
		$this->state = Cron::ENUM_STATE_ADDING_TO_QUEUE;
		$this->pStartDate = time();
		$this->save();
	}

	/**
	 * Cronjob added to RabbitMQ Que
	 *
	 * @return void
	 * @author Murat
	 */
	public function added(){
		$this->state = Cron::ENUM_STATE_ADDED_TO_QUEUE;
		$this->pStartDate = time();
		$this->save();
	}

	/**
	 * Check active cpc campaign referrers for fraud
	 *
	 * @return void
	 * @author Murat
	 */
	public static function fraudControl(){
		global $__DP, $debugLevel;

		$campaign = new Campaign();
		$campaign->with("campaign_campaignClick")->load(array("race"=>Campaign::ENUM_RACE_CPC,"state"=>Campaign::ENUM_STATE_PROGRESSING));

		if ($campaign->gotValue){

			$rmq = new RabbitMQ();
			$rmq->declareQueue(RMQ_QUEUE_FRAUD_CONTROL);

			do {
				$msg = json_encode(array("type" => "fraudControl", "id" => $campaign->id, "title" => $campaign->title));
				$rmq->publishMessage($msg,RMQ_QUEUE_FRAUD_CONTROL);

				if($debugLevel > 0) {
					Tool::echoLog("'{$campaign->title}' with #{$campaign->id} pushed RabbitMQ to check fraud");
				}
			} while($campaign->populate());
		} else {
			if($debugLevel > 0) {
				Tool::echoLog("No active CPC campaign found");
			}
		}
	}

	/**
	 * Load screenname for TWEET_SENT and TWEET_CHECK jobs
	 *
	 * @return string|null
	 * @author Murat
	 */
	public function getScreenname(){
		$cp = new CampaignPublisher();
		$cp->with("snTwitterHistory")->load(array(
			"publisher_campaignPublisher" => $this->publisher_cron,
			"campaign_campaignPublisher" => $this->campaign_cron
		));
		$screenname = $cp->gotValue ? $cp->snTwitterHistory->screenname : null;
		unset($cp);
		return $screenname;
	}

}
