<?php
	/**
	* contact real class for Jeelet - firstly generated on 11-03-2011 18:03, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/contact.php");

	class Contact extends Contact_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * Create new contact form data
		 *
		 * @return bool|Contact
		 * @author Murat
		 * @author Ozgur - modified
		 */
		public static function createNew($params){
			$c = new Contact();
			if (!empty($params["name"])) $c->cname = $params["name"];
			if (!empty($params["publisher"])) $c->contact_publisher = $params["publisher"];
			$c->cemail = $params["email"];
			if (isset($params["subject"]))
				$c->subject = $params["subject"];
			$c->cmessage = $params["message"];
			$c->ip = $params["ip"];
			return $c->save();

		}
	}

	?>