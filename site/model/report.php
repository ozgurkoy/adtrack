<?php
/**
 * report real class - firstly generated on 20-08-2014 14:30, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/report.php';

class Report extends Report_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}
	/**
	 * Give spesific adMingle report data
	 */
	public function weeklyReportData(){
		$rd = array(
			"usersByStatusWeek" => null,
			"usersByStatusAll" => null,
			"totalTrueReach" => null,
			"1weekBeforeTrueReach" => null,
			"newAdvertiser" => null,
			"totalAdvertiser" => null,
			"newBrand" => null,
			"totalBrand" => null,
			"campaignsByStatus" => null,
			"weeklyPaymentsCount" => null,
			"weeklyPaymentsTotalAmount" => null,
			"totalPaymentsCount" => null,
			"totalPaymentsAmount" => null
		);

		$rd["usersByStatusWeek"] = $this->getPublisherCounts(strtotime("-7 day"),time());
		$rd["usersByStatusAll"] = $this->getPublisherCounts(1,time());
		$rd["totalTrueReach"] = $this->getTrueReachStats(1,time());
		$rd["1weekBeforeTrueReach"] = $this->getTrueReachStats(1,strtotime("-7 day"));
		$rd["newAdvertiser"] = $this->getAdvertiserStats(strtotime("-7 day"),time());
		$rd["totalAdvertiser"] = $this->getAdvertiserStats(1,time());
		$rd["newBrand"] = $this->getBrandStats(strtotime("-7 day"),time());
		$rd["totalBrand"] = $this->getBrandStats(1,time());
		$rd["campaignsByStatus"] = $this->getCampaignStats(strtotime("-7 day"),time());
		$rd["weeklyPaymentsCount"] = $this->getPaymentStatCounts(strtotime("-7 day"),time());
		$rd["weeklyPaymentsTotalAmount"] = $this->getPaymentStats(strtotime("-7 day"),time());
		$rd["totalPaymentsCount"] = $this->getPaymentStatCounts(1,time());
		$rd["totalPaymentsAmount"] = $this->getPaymentStats(1,time());


		return $rd;
	}

	private function getPublisherCounts($startDate,$endDate){
		$rd = array();
		$u = new User();
		$u->groupBy(array(array("status","statusG")))
			->count(
				"id",
				"cnt",
				array(
					"jdate" => "BETWEEN $startDate,$endDate"
				)
			);
		if ($u->gotValue){
			do {
				$rd[$u->status_options[$u->statusG]] = $u->cnt;
			} while($u->populate());
		}
		return $rd;
	}

	private function getTrueReachStats($startDate,$endDate){
		$p = new Publisher();
		$p->with(array(
			"publisher_snTwitter",
			Publisher::NO_LEFT_JOIN
		));
		$p->multipleGroupOperation(true)
			->sum("`publisher_snTwitter`.`trueReach`","sTrueReach")
			->load(
				array(
					"jdate" => "BETWEEN $startDate,$endDate",
					"hasTwitter" => 1
				)
			);
		if ($p->gotValue){
			return intval($p->sTrueReach);
		} else {
			return 0;
		}
	}

	private function getAdvertiserStats($startDate,$endDate){
		$a = new Advertiser();
		$a->count("id","cAdvertiser",
			array(
				"jdate" => "BETWEEN $startDate,$endDate"
			)
		);
		if ($a->gotValue){
			return intval($a->cAdvertiser);
		} else {
			return 0;
		}
	}

	private function getBrandStats($startDate,$endDate){
		$ab = new AdvertiserBrand();
		$ab->count("id","cAdvertiserBrand",
			array(
				"jdate" => "BETWEEN $startDate,$endDate"
			)
		);
		if ($ab->gotValue){
			return intval($ab->cAdvertiserBrand);
		} else {
			return 0;
		}
	}

	private function getCampaignStats($startDate,$endDate){
		$rd = array();
		$c = new Campaign();
		$c->groupBy(array(array("state","stateG")))
			->count(
				"id",
				"cnt",
				array(
					"jdate" => "BETWEEN $startDate,$endDate"
				)
			);
		if ($c->gotValue){
			do {
				$rd[$c->state_options[$c->stateG]] = $c->cnt;
			} while($c->populate());
		}
		return $rd;
	}

	private function getPaymentStats($startDate,$endDate){
		$rd = array();
		$pma = new PublisherMoneyAction();
		$pma->groupBy(array(array("status","statusG")))
			->sum(
				"amount",
				"aAmount",
				array(
					"jdate" => "BETWEEN $startDate,$endDate"
				)
			);
		if ($pma->gotValue){
			do {
				$rd[$pma->status_options[$pma->statusG]] = $pma->aAmount * -1;
			} while($pma->populate());
		}
		return $rd;
	}

	private function getPaymentStatCounts($startDate,$endDate){
		$rd = array();
		$pma = new PublisherMoneyAction();
		$pma->groupBy(array(array("status","statusG")))
			->count(
				"id",
				"cnt",
				array(
					"jdate" => "BETWEEN $startDate,$endDate"
				)
			);
		if ($pma->gotValue){
			do {
				$rd[$pma->status_options[$pma->statusG]] = $pma->cnt;
			} while($pma->populate());
		}
		return $rd;
	}

	public function getReportHTML(){
		JT::init();
		JT::assign("data",json_decode($this->rdata,true));
		JT::assign("startDate",date("d/m/Y",$this->startDate));
		JT::assign("endDate",date("d/m/Y",$this->endDate));
		JT::assign("week",$this->weekNumber);

		return JT::pfetch( "info_report" );
	}
}

