<?php
	/**
	* companyMoneyAction real class for Jeelet - firstly generated on 09-03-2012 18:26, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/companyMoneyAction.php");

	class CompanyMoneyAction extends CompanyMoneyAction_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

	}
