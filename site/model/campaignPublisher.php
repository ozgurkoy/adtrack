<?php
/**
 * campaignPublisher real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignPublisher.php';

class CampaignPublisher extends CampaignPublisher_base
{
	/**
	 * Accepted campaign message link hash
	 * @var string
	 */
	public $linkHash = null;
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function getLinkStats($returnData=true, $campEnd=false, $linkHash = null){
		$rData = null;
		if (!is_null($linkHash)){
			$this->linkHash = $linkHash;
		} else {
			$this->getLinkHash();
		}

		if (!is_null($this->linkHash)){
			$ucs = new UserClickStat();
			$ucs->orderBy("clickDate DESC")->limit(0,1)->load(array(
				"linkHash" => $this->linkHash
			));

			$refData = (isset($_SESSION["SLS_REFDATA_".$this->linkHash]) ? $_SESSION["SLS_REFDATA_".$this->linkHash] : null);
			$rData = array("refData"=>null);

			if (!$returnData || !$ucs->gotValue || (time() - $ucs->lastUpdate) >= 60 || is_null($refData)){
				$data = Tool::callSLService("hashStats",array(
					"params"=>  json_encode(array(
						"linkHash"=>$this->linkHash,
						"lastUpdate" => null //($ucs->gotValue ? strtotime(date("Y-m-d",$ucs->lastUpdate) . " 00:00:00") : null)
					))
				));

				if (!JUtil::isProduction()){
					$params = JUtil::getSearchParameter();
					if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
						Tool::echoLog("hashStats : " . print_r($data, true));
					}
				}

				if (!is_null($data)){
					UserClickStat::updateLinkStats($data);
				}

				$error = null;
				if (isset($data->error) && $data->error != "no clicks found"){
					$error = true;
				}

				if ($campEnd && $this->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPC){
					global $debugLevel;
					$this->campaign_campaignPublisher->campaign_campaignCPCMessage->loadCampaignPublisher();
					$this->campaign_campaignPublisher->campaign_campaignCPCMessage->loadPublisherRevenue();
					$this->campaign_campaignPublisher->campaign_campaignCPCMessage->loadPublisher_campaignCPCMessage()->loadUser();

					if ($this->campaign_campaignPublisher->campaign_campaignCPCMessage->finishedWarningSent == 0){
						if ($debugLevel) Tool::echoLog("Campaign finish mail scheduled to send for : " . $this->campaign_campaignPublisher->campaign_campaignCPCMessage->publisher_campaignCPCMessage->user_publisher->username);
						$this->campaign_campaignPublisher->campaign_campaignCPCMessage->publisher_campaignCPCMessage->user_publisher->sendSysMessage(
							'V2_pubCPCCampaignEnd',
							array(),
							array(
								"name" => $this->campaign_campaignPublisher->campaign_campaignCPCMessage->publisher_campaignCPCMessage->getFullName(),
								"campName" => $this->campaign_campaignPublisher->title,
								"endDate" => Format::getActualDateTime(),
								"revenue" => $this->campaign_campaignPublisher->campaign_campaignCPCMessage->publisherRevenue->amount
							),
							$this->campaign_campaignPublisher->id,
							null,
							null,
							$this->campaign_campaignPublisher->getPreviewImageLink()
						);
						$this->campaign_campaignPublisher->campaign_campaignCPCMessage->finishedWarningSent = 1;
						$this->campaign_campaignPublisher->campaign_campaignCPCMessage->save();
					} else {
						if ($debugLevel) Tool::echoLog("Campaign finish already sent for : " . $this->campaign_campaignPublisher->campaign_campaignCPCMessage->publisher_campaignCPCMessage->user_publisher->username);
					}
				} else {
					//global $debugLevel;
					//if ($debugLevel) Tool::echoLog("Campaign end parameter not set");
				}

				if ($returnData){
					//Referrer data

					$data = $this->getReferrerStats();

					$rData["refData"] = Format::parseReferrer($data);

					$_SESSION["SLS_REFDATA_".$this->linkHash] = $rData["refData"];

					unset($data,$refData);
				} else {
					unset($data);
					if ($error)
						return false;
					else
						return true;
				}
			} else {
				$rData["refData"] = $_SESSION["SLS_REFDATA_".$this->linkHash];
			}
		}
		return $rData;
	}

	/**
	 * Change status with given state
	 *
	 * @return void
	 * @author Murat
	 */
	public function changeStatus($state){
		$this->state = $state;
		$this->actionDate = time();
		$this->save();
	}

	/**
	 * Checks if given user can attend this campaign
	 * - if user is in target people
	 * - if campaign has not started
	 * - if campaign has enough remaining budget to accept user cost for advertiser
	 *
	 * @param User $user
	 * @return boolean if user can accept campaign
	 */
	public function canUserAccept() {
		if (!is_object($this->campaign_campaignPublisher)) $this->loadCampaign();

		if (
			$this->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPC ||
			$this->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPL ||
			$this->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPL_REMOTE ||
			$this->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPA_AFF
		){
			//direct accept because when the campaign ends system closes this pages(muro)

			return true;
		}
		elseif (strtotime($this->campaign_campaignPublisher->endDate . " 23:59:59") <= time()) {
			//Mark as late
			$this->changeStatus(CampaignPublisher::ENUM_STATE_LATE_TO_JOIN);
			return CampaignPublisher::ENUM_STATE_LATE_TO_JOIN;
		}
		else {
			// if campaign is started users can not join anymore
			if($this->campaign_campaignPublisher->startDate <= (date('Y-m-d').' 00:00:00')) {
				//Mark as late
				$this->changeStatus(CampaignPublisher::ENUM_STATE_LATE_TO_JOIN);
				return CampaignPublisher::ENUM_STATE_LATE_TO_JOIN;
			}

			$userCost = $this->cost;
			$userCost *= Setting::getValue("PROFIT_MULTIPLIER");
			$userCost = (float)(string)$userCost;

			// get remaining budget for this campaign
			$remainingBudget = $this->campaign_campaignPublisher->getLatestBudget();

			// compare remaining budget with user cost for advertiser
			if($remainingBudget >= $userCost || $this->isReserved) {
				return true;
			} else {
				//Mark as budget ended
				$this->changeStatus(CampaignPublisher::ENUM_STATE_BUDGET_ENDED);
				return CampaignPublisher::ENUM_STATE_BUDGET_ENDED;
			}
		}
	}

	/**
	 * Get click stats async
	 *
	 * @return void
	 * @author Murat
	 */
	public function getClickStatsAsync($campEnd=false){
		$c = new Cron();
		$c->load(array(
			"type" => Cron::ENUM_TYPE_GET_PUBLISHER_CLICK_STATS,
			"publisher_cron" => $this->publisher_campaignPublisher,
			"campaign_cron" => $this->campaign_campaignPublisher
		));
		if ($c->gotValue){
			$c->targetDate = time();
			$c->state = Cron::ENUM_STATE_WAITING;
			$c->executeCount++;
			if ($c->additionalData != "Y" && $campEnd){
				$c->additionalData = "Y";
			}
			$c->save();
		} else {
			Cron::create(
				Cron::ENUM_TYPE_GET_PUBLISHER_CLICK_STATS,
				time(),
				$this->publisher_campaignPublisher,
				$this->campaign_campaignPublisher,
				($campEnd ? "Y" : null)
			);
		}
		unset($c);
	}

	/**
	 * Create campaign shot link for publisher
	 *
	 * @param CampaignTwitterMessage|CampaignCPCMessage $cum
	 *
	 * @return string
	 * @author Murat
	 */
	public function createShortLink(&$cum){
		if (!is_object($this->campaign_campaignPublisher->campaign_campaignClick)){
			$this->campaign_campaignPublisher->loadCampaignClick();
		}

		$isCountable = (!Validate::imgLink($this->campaign_campaignPublisher->campaign_campaignClick->link) || $this->campaign_campaignPublisher->race == Campaign::ENUM_RACE_CPC);

		if ($isCountable && (!$this->campaign_campaignPublisher->campaign_campaignClick->gotValue || empty($this->campaign_campaignPublisher->campaign_campaignClick->linkHash))){
			return null;
		}

		if (!$isCountable){
			return " " . $this->campaign_campaignPublisher->campaign_campaignClick->link;
		} else {
			$slResponse = Tool::callSLService("createURL",array(
				"params"=>  json_encode(array(
					"campaignHash"=>$this->campaign_campaignPublisher->campaign_campaignClick->linkHash
				))));
			if (empty($slResponse)){
				throw new Exception("Can not connect to link service API");
			} elseif (!empty($slResponse->error)){
				throw new Exception("SLS Error: " . $slResponse->error);
			}
			//die(print_r($slResponse,true));
			$cum->linkHash = $slResponse->code;
			$cum->lastUpdate = time();
			//return ' ' . Setting::getValue("SHORTDOMAIN") . $slResponse->code;
			return ' ' . $this->campaign_campaignPublisher->getShortLinkDomain() . $slResponse->code;
		}
	}

	/**
	 * Assign campaign filter via status
	 *
	 * @param string $status
	 * @param int $publisherId
	 *
	 * @return array
	 *
	 * @author Murat
	 */
	public function createPublisherFilter($status,$publisherId){
		$filter = array("publisher_campaignPublisher" => $publisherId);
		switch($status){
			case "o":
				$filter["state"] = CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION;
				$filter["availableDate"] = "&le " . time();
				break;
			case "a":
				$filter["state"] = CampaignPublisher::ENUM_STATE_APPROVED;
				$filter["campaign_campaignPublisher"] = array("state" => Campaign::ENUM_STATE_PENDING_PROGRESS . ' &or ' . Campaign::ENUM_STATE_PROGRESSING);
				break;
			case "f":
				$filter["state"] = CampaignPublisher::ENUM_STATE_APPROVED;
				$filter["campaign_campaignPublisher"] = array("state" => Campaign::ENUM_STATE_FINISHED);
				break;
			case "r":
				$filter["state"] = CampaignPublisher::ENUM_STATE_REJECTED . ' &or ' . CampaignPublisher::ENUM_STATE_LATE_TO_JOIN . ' &or ' . CampaignPublisher::ENUM_STATE_BUDGET_ENDED;
				break;
		}
		return $filter;
	}

	/**
	 * Load linkHash property
	 *
	 * @return void
	 * @author Murat
	 */
	public function getLinkHash(){
		if (!is_object($this->campaign_campaignPublisher)) $this->loadCampaign();
		if (!is_object($this->publisher_campaignPublisher)) $this->loadPublisher();
		if ($this->campaign_campaignPublisher->gotValue && $this->publisher_campaignPublisher->gotValue){
			switch($this->campaign_campaignPublisher->race){
				case Campaign::ENUM_RACE_CPC:

					if (!is_object($this->campaign_campaignPublisher->campaign_campaignCPCMessage) || !$this->campaign_campaignPublisher->campaign_campaignCPCMessage->gotValue){
						$this->campaign_campaignPublisher->loadCampaign_campaignCPCMessage(array(
							"campaign_campaignCPCMessage" => $this->campaign_campaignPublisher->id,
							"publisher_campaignCPCMessage" => $this->publisher_campaignPublisher->id
						));
					}
					if ($this->campaign_campaignPublisher->campaign_campaignCPCMessage->gotValue){
						$this->linkHash = $this->campaign_campaignPublisher->campaign_campaignCPCMessage->linkHash;
					}
					break;
				case Campaign::ENUM_RACE_TWITTER:
					if (!is_object($this->campaign_campaignPublisher->campaign_campaignTwitterMessage) || !$this->campaign_campaignPublisher->campaign_campaignTwitterMessage->gotValue){
						$this->campaign_campaignPublisher->loadCampaign_campaignTwitterMessage(array(
							"campaign_campaignCPCMessage" => $this->campaign_campaignPublisher->id,
							"publisher_campaignCPCMessage" => $this->publisher_campaignPublisher->id
						));
					}
					if ($this->campaign_campaignPublisher->campaign_campaignTwitterMessage->gotValue){
						$this->linkHash = $this->campaign_campaignPublisher->campaign_campaignTwitterMessage->linkHash;
					}
					break;
			}
		}
	}

	/**
	 * Get publisher click referrer stats
	 *
	 * @return array
	 * @author Murat
	 */

	public function getReferrerStats(){
		if (is_null($this->linkHash)){
			$this->getLinkHash();
		}
		$data = Tool::callSLService("linkHashRefStats",array(
			"params"=>  json_encode(array(
				"linkHash"=>$this->linkHash
			))
		));

		if (!JUtil::isProduction()){
			$params = JUtil::getSearchParameter();
			if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
				Tool::echoLog("linkHashRefStats : " . print_r($data, true));
			}
		}

		return $data;
	}

	/**
	 * Get publisher click ip stats
	 *
	 * @return array
	 * @author Murat
	 */
	public function getIPStats(){
		$data = Tool::callSLService("linkHashIpStats",array(
			"params"=>  json_encode(array(
				"linkHash"=>$this->linkHash
			))
		));

		if (!JUtil::isProduction()){
			$params = JUtil::getSearchParameter();
			if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
				Tool::echoLog("linkHashIpStats : " . print_r($data, true));
			}
		}

		return $data;
	}

	/**
	 * Get publisher click user agent stats
	 *
	 * @return array
	 * @author Murat
	 */
	public function getUserAgentStats(){
		$data = Tool::callSLService("linkHashAgentStats",array(
			"params"=>  json_encode(array(
				"linkHash"=>$this->linkHash
			))
		));

		if (!JUtil::isProduction()){
			$params = JUtil::getSearchParameter();
			if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
				Tool::echoLog("linkHashAgentStats : " . print_r($data, true));
			}
		}

		return $data;
	}

	/**
	 * Get Publisher Blocked Referrers
	 *
	 * @return array
	 * @author Murat
	 */
	public function getBlockedReferrers(){
		//Get blocked referrers
		$data = Tool::callSLService("getLinkCursed",array("params"=>  json_encode(array(
			"linkHash" => $this->linkHash,
		))));
		//die(print_r($data));

		if (!JUtil::isProduction()){
			$params = JUtil::getSearchParameter();
			if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
				Tool::echoLog("getLinkCursed: " . print_r($data,true));
			}
		}

		return $data;
	}
}

