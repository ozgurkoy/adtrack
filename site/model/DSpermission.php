<?php
/**
 * DSpermission real class - firstly generated on 22-11-2013 18:08, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/DSpermission.php';

class DSpermission extends DSpermission_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function getPermissions() {

		if ( !isset( $_SESSION[ "DS_userId" ] ) ) return false;

		if(!isset($_SESSION["permissions"])){

			/*role permissions TODO: READ FROM SESSION*/
			$u    = new DSuser( $_SESSION[ "DS_userId" ] );
			if(isset($_SESSION["DS_isRoot"]) && $_SESSION["DS_isRoot"]==1){
				$role = new DSrole();
				$role->load();
			}
			else
				$role = $u->loadRoles();

			$pers = array();

			if ( $role && $role->gotValue ) {
				do {
					$per = $role->loadPermissions();

					if ( $per && $per->gotValue ) {
						do {
							$per->loadAction();
							if(!$per->action || $per->action->gotValue === false)
								continue;

							if ( !isset( $pers[ $per->action->label ] ) )
								$pers[ $per->action->label ] = isset($_SESSION["isRoot"]) && $_SESSION["isRoot"]==1?2047:0;


							$pers[ $per->action->label ] |= $per->bits;
						} while ( $per->populate() );
					}
				} while ( $role->populate() );
			}
//			print_r( $pers );
			$_SESSION[ "permissions" ] = $pers;
		}
		else
			$pers = $_SESSION[ "permissions" ];



		/*general permissions*/
		$perms = array();

		$p = new DSaction();
		$p->nopop()->load();

		while ( $p->populate() ) {
			$p->parseTasks();

			$perms[ $p->label ] = $p->parsedTasks;
		}
		$_SESSION[ "DSPerms" ] = $perms;

		return array($pers, $perms);

	}
}

