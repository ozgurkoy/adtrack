<?php
/**
 * campaignTwitter real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignTwitter.php';

class CampaignTwitter extends CampaignTwitter_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * get campaign expected follower count with budget
	 *
	 * @return int
	 * @author Murat
	 */
	public function getExpectedValue($field){
		if (!is_object($this->campaign_campaignTwitter)) $this->loadCampaign();
		if (is_null($this->maxCost) || $this->maxCost == 0)
			return 0;
		else
			return intval($this->$field * ($this->campaign_campaignTwitter->budget / $this->maxCost));
	}


	/**
	 * Get effect campaign summary stats
	 *
	 * @return array
	 * @author Murat
	 */
	public function getSummaryValues(){
		$data = array(
			"publisher"=>0,
			"follower"=>0,
			"influence"=>0
		);

		//todo:@Murat: implement this function

		return $data;
	}
}

