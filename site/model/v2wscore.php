<?php

class v2wscore {
	/**
	 * Singleton
	 */
	private static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance)
        {
            self::$_instance = new v2wscore();
        }

        return self::$_instance;
    }

	/**
	 * Request object for all webservice methods
	 */
	public $_request = null;

	/**
	 * Gets the Request headers
	 */
	public $_headers = null;
	/**
	 * Request variables for all webservice methods
	 */
	public $_requestVars = null;
	/**
	 * User object for all webservice methods
	 * @property Publisher
	 * @var Publisher
	 */
	public $_publisher = null;
	/**
	 * Company object for all webservice methods
	 * @property Company
	 */
	public $_company = null;
	/**
	 * Encryption key for token
	 */
	public $_encryptionKey = "Va#X8%JSKo&Z";
	/**
	 * Items in per page of lists
	 */
	public $_itemsPerPage = 100000;

	/**
	 * Language words
	 */
	public $_langWords = null;
	/**
	 * Log object for request logging
	 * @property logApi
	 */
	public $_log = null;

	/**
	 * Date format (only date)
	 * @property string
	 */
	public $_formatDate = "Y-m-d";

	/**
	 * Decrypted user token
	 * @property string
	 */
	private  $_token = null;




	/**
	 * Encrypt the given string
	 *
	 * @param string $s
	 * @return string
	 * @author Murat
	 */
	public function encrypt($s){

		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->_encryptionKey), $s, MCRYPT_MODE_CBC, md5(md5($this->_encryptionKey))));
	}


	/**
	 * Decrypt the given string
	 *
	 * @param string $s
	 * @return string
	 * @author Murat
	 */
	public function decrypt($s){

		return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->_encryptionKey), base64_decode($s), MCRYPT_MODE_CBC, md5(md5($this->_encryptionKey))), "\0");
	}

	/**
	 * Log Webservice Request
	 *
	 * @return void
	 * @author Murat
	 */
	public function logRequest($module,$action){


		$lw = new LogMobileAPI();
		$lw->requestDate = time();
		$lw->module = $module;
		$lw->action = $action;
		$lw->token = $this->_request->token;
		$lw->requestHeader = ADVANCED_DEBUG_MODE?json_encode($this->_headers):null;
		$lw->urlParams = json_encode($this->_request->request_vars);
		$lw->requestBody = json_encode($this->_request->json);
		$lw->IP = $_SERVER["REMOTE_ADDR"];
		$lw->save();
		$this->_log = $lw;
	}

	/**
	 * Log Webservice Response
	 *
	 * @return void
	 * @author Murat
	 */
	public function logResponse($response){
		if (!is_null($this->_log)){
			$response = (is_array($response) || is_object($response) ? json_encode($response) : $response);
			$this->_log->response = $response;
			$this->_log->responseDate = time();
			$this->_log->responseTime = $this->_log->responseDate - $this->_log->jdate;
			$this->_log->save();
		}
	}

	public function getToken()
	{
		if(is_null($this->_token)){
			$this->_token = $this->decrypt($this->_request->token);
		}
		return $this->_token;
	}

	/**
	 * Token check for validation
	 *
	 * @return boolean
	 * @author Murat
	 */
	public function tokenCheck()
	{
		//var_dump(debug_backtrace());
		$result = false;

		// check for admingle from header
		//$headers = apache_request_headers();


		// valid the token
		if (isset($this->_request->token)) {

			$token = $this->getToken();

			$pmd = PublisherMobileDevice::getPublisherForToken($token, true);
			if (!is_bool($pmd)) {
				$this->_publisher = $pmd;
				$result = true;
			}
		}

		if (!$result) {
			$this->sendResponse(403, "Bad token");
		}

		return $result;
	}

	/**
	 * Get proper word with selected language
	 * 
	 * @return string
	 * @author Murat
	 */
	public function getLangWord($wordCode){
		if (is_null($this->_langWords)){
			$this->_langWords = LangWords::getLangWords("word",  "EN");
		}
		if (isset($this->_langWords[$wordCode])){
			return $this->_langWords[$wordCode];
		} else {
			return null;
		}
	}

	/**
	 * Send response to requester
	 *
	 * @return string
	 * @author Murat
	 */
	public function sendResponse($responseCode,$responseBody=null){
		/*
		if (is_array($responseBody) && empty($responseBody["error"])){
			unset($responseBody["error"]);
		}
		*/
		$this->logResponse(array("responseCode"=>$responseCode,"responseBody"=>$responseBody));
		RestUtils::sendResponse($responseCode, $responseBody);
	}
}
?>
