<?php
/**
 * snFacebook real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/snFacebook.php';
require_once $__DP.'/site/service/facebook/facebook.php';

class SnFacebook extends SnFacebook_base
{
	private $config = null;
	private $FacebookObj;

	public static $savedId = null;
	public static $savedUsername = null;
	public static $savedFBId = null;


	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {

		// load Configuration
		if(is_null($this->FacebookObj))
			$this->FacebookObj = new Facebook($this->FacebookConfig());

		parent::__construct($id);
	}


	/**
	 * @param $facebookID
	 * @param $accessToken
	 * @return bool|SnFacebook
	 * @author ozgur - modified
	 */
	public static function loginFacebookAccount($facebookID,$accessToken) {

		$newFbUser = new SnFacebook();
		$newFbUser->load(array("fbid"=>$facebookID));



		if($newFbUser->gotValue)
		{
			// User exist
			$fbObj = $newFbUser->FacebookObj;
			$newFbUser->AccessToken = $accessToken;
			$fbObj->setAccessToken($accessToken);
			$newFbUser->updateInfo()->loadPublisher();

			$validateResponse =  $newFbUser->validateAccessToken();
			if($newFbUser->publisher_snFacebook && $newFbUser->publisher_snFacebook->gotValue &&  $validateResponse->valid)
			{
				if($facebookID == $validateResponse->fbId){
					$newFbUser->TokenValidDate = $validateResponse->expires_at;
					$newFbUser->publisher_snFacebook
						->writeHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "Login by facebook", UserHistory::ENUM_MADEBY_USER )
						->loadUser()
						->login( null, null, $newFbUser->publisher_snFacebook->user_publisher->id );

					return $newFbUser;
				}
			}
			else
				return false;
		}
		else{
			// User not exist, but we still get the facebook account
			self::addFacebookAccount( $facebookID, $accessToken );
			return false;

		}

	}

	/**
	 * Add new Facebook account, Check for valid token
	 *
	 * @param $facebookID
	 * @param $accessToken
	 * @return bool|SnFacebook false - user exist or token fail
	 */
	public static function addFacebookAccount($facebookID,$accessToken) {

		$newFbUser = new SnFacebook();
		$newFbUser->load(array("fbid"=>$facebookID));

		if($newFbUser->gotValue && !is_null($newFbUser->publisher_snFacebook))
		{
			// User exist
			return false;
		}
		else
		{
			// new user
			$newFbUser->fbid        = $facebookID;
			$newFbUser->AccessToken = $accessToken;

			$newFbUser->FacebookObj->setAccessToken( $accessToken );

			// Check if the token is valid
			$validateResponse =  $newFbUser->validateAccessToken();

			if($validateResponse->valid)
			{
				if($facebookID == $validateResponse->fbId){
					$newFbUser->TokenValidDate = $validateResponse->expires_at;
					$newFbUser->updateInfo();

					return $newFbUser;
				}
			}

			return false;
		}

	}


	public function FacebookConfig() {

		if(is_null($this->config))
		{
			$this->config = array(
				'appId' => Setting::getValue("FB_APP_ID") ,
				'secret' => Setting::getValue("FB_APP_SECRET") ,
				'fileUpload' => false);
		}

		return $this->config;
	}

	/**
	 * Get the access token from facebook after Getting the auth code
	 *
	 * @param $code
	 * @return $this
	 */
	public function GetAccessTokenForUser($code) {

		global $GLBS;

		if(isset($code)){
			$facebook = $this->FacebookObj;
			$user_id = $facebook->getUser();

			if(!$facebook->getAccessToken())
			{
				//$my_url = $GLBS["constants"]["siteAddress"];
				$my_url = JUtil::siteAddress();

				$token_url = "https://graph.facebook.com/oauth/access_token?"
					. "client_id=" . self::$config['appId'] . "&redirect_uri=" . urlencode($my_url)
					. "&client_secret=" . self::$config['secret'] . "&code=" . $code;

				$response = file_get_contents($token_url);
				$params = null;
				parse_str($response, $params);
				$new_access_token = $params['access_token'];


				$facebook->setAccessToken($new_access_token);
			}

			$this->facebookID = $user_id;
			$this->load();

			$this->AccessToken = $facebook->getAccessToken();
			$this->save();
		}

		return $this;
	}

	/*
         * Create sesstion, connect to user
         * Access from login
         */
	private function signin($redirectPage) {
		global $__DP,$_S;
		unset($_SESSION["FBredirectPage"]);

		//JLog::log("fb", "Facebook sign In Redirect - ".$redirectPage);
		// We Have User ID The Access token

		// Invite
		// 1. sign up -
		// 2. Sign IN
		// No User
		// Has User

		// Invite Mode Handle
		if($redirectPage == "invite")
		{
			$this->updateInfo();
			$this->user = null;
			$this->save();

			$invite = new Invite();
			$invite->AddFacebookUser($this->facebookID, $this->AccessToken);
			$_SESSION['facebookStatus'] = "added";

			header("Location: ".$_S."/".$redirectPage);
			exit();
		}


		//Check if ther is loged in with adMingleUserID
		if(isset($_SESSION["adId"]))
		{
			// From Sign up step 3 or update profile

			if(is_null($this->user))
			{
				// Refresh Facebook Date
				$this->updateInfo();

				// Fill Session With Facebook Data
				$this->fillSession();

				// Has adMingle User - sign up / update profile
				$this->user = $_SESSION["adId"];
				$this->save();
				$this->loadUser();
				$this->user->hasFacebook = 1;
				$this->user->save();
				$this->user->fillSession();
				$this->user->actLang();

			}
			else
			{
				if($_SESSION["adId"] != $this->user)
				{
					Tool::setLastError("user","FacebookAccountUserByAnotherUser","userFacebook->signin");
				}
				else {
					$this->loadUser();
					$this->user->fillSession();
					$this->user->actLang();
				}
			}

			header("Location: ".$_S."/".$redirectPage);
			exit();
		}
		else
		{
			// Main Page / Login / Sign up

			// Refresh Facebook Date
			$this->updateInfo();

			// Fill Session With Facebook Data
			$this->fillSession();


			// Hasn't adMingl User in session
			// Check if ther is any adMIngle account
			if($this->user)
			{
				// Has admingle account
				$this->loadUser();
				$this->user->fillSession();
				$this->user->actLang();
				header("Location: ".$_S."/".$redirectPage);
			}
			else
			{
				// No user, Go to SignUp Page
				header("Location: ".$_S."/"."sign-up-pub");
			}
			exit();

		}
	}

	/**
	 * archive the current and update current facebook Basic public info
	 *
	 * @return snFacebook
	 * @author Ozgur - modified
	 **/
	public function updateInfo()
	{
		$facebook = $this->FacebookObj;

		$user_profile = $facebook->api('/me','GET');

		$this->username     = isset( $user_profile[ "username" ] ) ? $user_profile[ "username" ] : null;
		$this->profileLink  = $user_profile[ "link" ];
		$this->name         = $user_profile[ "name" ];
		$this->location     = isset( $user_profile[ "location" ] ) ? $user_profile[ "location" ][ "name" ] : null;
		$this->gender       = $user_profile[ "gender" ];
		$this->locale       = $user_profile[ "locale" ];
		$this->verified     = isset( $user_profile[ "verified" ] ) ? $user_profile[ "verified" ] : false;
		$this->updated_time = $user_profile[ "updated_time" ];


		#stupid way below, because the login function is called statically and the response is checked on API.
		SnFacebook::$savedUsername = $this->username;
		SnFacebook::$savedFBId = $this->fbid;
		$r = $this->save();
		SnFacebook::$savedId = $this->id;

		return $r;
	}

	public function getUserEmail()
	{
		$facebook = $this->FacebookObj;

		$user_profile = $facebook->api('/me?fields=email');

		return isset($user_profile["email"])?$user_profile["email"]:null;

	}

	/**
	 * get avatar image
	 * @param $fbId
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function getAvatarImage( $fbId ) {
		$t0 = file_get_contents( "https://graph.facebook.com/{$fbId}/picture?type=large" );
		if(!(strlen($t0)>500))
			return false;
		$filename = JUtil::randomString(10);
		$temp = tempnam(sys_get_temp_dir(), $filename );
		$handle = fopen($temp, "w");

		fwrite( $handle, $t0 );

		rename( $temp, $temp . ".jpg" );

		return $temp . ".jpg";

	}


	/***
	 *
	 * redirect_uri: the url to go to after a successful login
	 * scope: comma separated list of requested extended perms
	 */
	public function getLoginUrl( $scope = NULL, $redirect_uri = NULL, $returnUrl = false ) {

		global $_S;
		// redirect_uri: the url to go to after a successful login
		// scope: comma separated list of requested extended perms

		if ( !$redirect_uri ) {
			$redirect_uri = $_S . '/site/service/facebook/fbCB.php';
		}
		if ( !$scope ) {
			$scope = 'email,user_about_me,user_birthday,user_hometown,user_likes,user_location,read_stream';
		}


		$loginParams = array( "redirect_uri" => $redirect_uri, "scope" => $scope );
		$url         = $this->FacebookObj->getLoginUrl( $loginParams );

		if($returnUrl)
			return $url;

		header( "Location: " . $url );
		exit();
	}

	public function logout() {
		$this->FacebookObj->clearAllPersistentData();
	}

	public function loginError($error,$code) {
		global $_S;
		$redirectPage = "";
		if(isset($_SESSION["fb_redirectPage"]))
		{
			$redirectPage = $_SESSION["fb_redirectPage"];
		}

		//JLog::log("fb", "Facebook  Login Error Code: ".$code ." Error: ".$error."Redirect - ".$redirectPage);
		$_SESSION['facebookStatus'] = "error";

		header("Location: ".$_S."/".$redirectPage);
		exit();
	}

	/**
	 * @return FacebookValidateResponse
	 */
	private function validateAccessToken()
	{
		$facebook = $this->FacebookObj;
		$app_access_token = $this->getAppAccessToken();
		//$app_access_token = Setting::getValue("FB_APP_TOKEN");

		//input_token: the access token you want to get information about
		//access_token: your [app access token][10] or a valid user access tœoken from a developer of the app.
		$tokenReq = "https://graph.facebook.com/debug_token?input_token=".$this->AccessToken."&access_token=".$app_access_token;

		//JLog::log("social",$tokenReq);

		//print_r($tokenReq);exit;

		$response = file_get_contents($tokenReq);
		//JLog::log("social",$response);
		$tokenResponse = json_decode($response);


		$facebookValidateResponse = new FacebookValidateResponse();

		$facebookValidateResponse->valid = $tokenResponse->data->is_valid;
		if($tokenResponse->data->is_valid)
		{
			$facebookValidateResponse->expires_at = $tokenResponse->data->expires_at;
			$facebookValidateResponse->fbId = $tokenResponse->data->user_id;
		}


		return $facebookValidateResponse;


	}

	/**
	 * login by username etc.
	 * first - First time doing login
	 * @return mixed
	 * @author Sam Ben Yakar
	 **/
	public function login($redirectPage="dashboard",$first=false) {
		global $__DP;
		//$this->FacebookObj->clearAllPersistentData();
		//JLog::log("fb", "Facebook  Login Reditect - ".$redirectPage);

		if(isset($_SESSION["fb_redirectPage"]))
		{
			$redirectPage = $_SESSION["fb_redirectPage"];
		}else{
			$_SESSION["fb_redirectPage"] = $redirectPage;
		}

		if($first)
			return $this->getLoginUrl();

		$facebook = $this->FacebookObj;
		$user_id = $facebook->getUser();
		//JLog::log("fb", "Facebook  Login user_id - ".$user_id);

		if($user_id != '0') {

			// We have a user ID, so the user has open session in facebook
			try {

				// Checking if we have valid token

				$this->facebookID = $user_id;
				$this->load();

				if($this->count && !is_null($this->user))
				{
					$facebook->setAccessToken($this->AccessToken);
					$validateResponse =  $this->validateAccessToken();

					if($validateResponse->valid)
					{
						$this->TokenValidDate = $validateResponse->expires_at;
						$this->save();
						$this->signin($redirectPage);
					}
					else
					{
						// Token not Valid
						return $this->getLoginUrl();
					}
				}else{
					// No User Record

					//Check for token in the sseion
					if($facebook->getAccessToken() == "")
					{
						// Get new Token
						return $this->getLoginUrl();
					}

					// Has Token
					// Create new Facebook User
					// Add 60 Days to Valid Token
					$this->TokenValidDate = time()+(60*60*24*60);
					$this->save();
					$this->signin($redirectPage);

				}

			} catch(FacebookApiException $e) {
				// If the user is logged out, you can have a
				// user ID even though the access token is invalid.
				// In this case, we'll get an exception, so we'll
				// just ask the user to login again here.
				error_log($e->getType());
				//JLog::log("fb", 'error - '.$e->getMessage());
				print_r($e);
				exit();
//return $this->getLoginUrl();
			}
		}
		else
		{
			// If User is not valid or not register
			// it will redirect to Facebook login

			return $this->getLoginUrl();
		}
	}
	/**
	 * Fills session variables of Facebook
	 */
	public function fillSession() {

		$_SESSION['fbid'] 		= $this->facebookID;
		$_SESSION['fbimg'] 		= $this->GetProfileimg();
		$_SESSION['fbUsername'] 	= is_null($this->username)?$this->username:$this->name;
		$_SESSION['fbNameSurname']     = $this->name;
	}

	public function GetProfileimg($type=null,$width=null,$height=null) {
		//?width=40&height=60
		//?type=square small normal large


		$url = "https://graph.facebook.com/".$this->facebookID."/picture?";

		if(!is_null($type))
			$url .= "&type=".$type;


		if(!is_null($width))
			$url .= "&width=".$width;


		if(!is_null($height))
			$url .= "&height=".$height;

		return $url;

	}

	/*
	 * Get all post Data
	 * return json
	 */
	public function GetPost($postID) {

		$facebook = $this->FacebookObj;

		$user_post = $facebook->api('/'.$postID);

		return $user_post;

	}

	public function getUserInfoFromToken() {
		$this->FacebookObj->getAccessToken();
		return array("id"=>$this->FacebookObj->getUser(),"token"=>$this->FacebookObj->getAccessToken());
	}

	protected function getAppAccessToken()
	{
		//FB_APP_TOKEN
		$new_access_token = Setting::getValue("FB_APP_TOKEN");

		if(empty($new_access_token))
		{
			$config = $this->FacebookConfig();

			$token_url = "https://graph.facebook.com/oauth/access_token?"
				. "client_id=" . $config['appId'] . "&grant_type=client_credentials"
				. "&client_secret=" . $config['secret'];

			$response = file_get_contents($token_url);

			$params = null;
			parse_str($response, $params);
			$new_access_token = $params['access_token'];
			Setting::setValue("FB_APP_TOKEN",$new_access_token);
		}
		return $new_access_token;
	}

}

class FacebookValidateResponse
{
	public $expires_at;
	public $fbId;
	public $valid;
}

