<?php
/**
 * storeCategory real class - firstly generated on 05-02-2014 14:14, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/storeCategory.php';

class StoreCategory extends StoreCategory_base
{
	public static $imagePath = "site/layout/images/store/categories/";

	public $hasChildren = false;

	public $hasProducts = false;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * @param null $parent
	 * @param int  $level
	 * @param null $nid
	 * @return array
	 * @author Özgür Köy
	 */
	public static function catList( $parent = null, $brandId, $level = 0, $nid = null ) {
		$cats = array();
		$m     = new StoreCategory();
		$m->nopop()->orderBy( "rorder ASC" )->load( array( "_custom" => "`categories`=$brandId AND `parentCategory` " . ( is_null( $parent ) ? "IS NULL" : "=$parent" ) . ( is_null( $nid ) ? "" : " AND id<>" . $nid ) ) );

		$first = true;
		while ( $m->populate() ) {
			$cats[ $m->id ] = array(
				"name"     => $m->name,
				"image"      => $m->image,
				"active"    => $m->active,
				"level"    => $level,
				"containsProducts"   => $m->containsProducts
			);

			$first = false;
			$t0    = (array)self::catList( $m->id, $brandId, $level + 1, $nid );

			$cats = $cats + $t0;
		}
		unset( $m );

		return $cats;
	}

	/**
	 * @return bool
	 * @author Özgür Köy
	 */
	public function hasChildren( ){
		$s = new StoreCategory();
		if(is_null($this->id)){
			return $this->hasChildren = false;
		}
//		JPDO::debugQuery();
		$s->load( array( "_custom" => "`parentCategory`=".$this->id ) );
//		JPDO::debugQuery(false);
		$q = $s->gotValue;
		unset( $s );
		return $this->hasChildren = $q;
	}

	/**
	 * @author Özgür Köy
	 */
	public function hasProducts() {
		$this->hasProducts = $this->countProducts(array("used"=>0)) > 0;
	}

	/**
	 * @param $params
	 * @param $_files
	 * @return bool
	 * @author Özgür Köy
	 */
	public function add( $params, $_files ) {
		global $__DP;
		$this->assignValuesToObject( $params );

		$this->active           = isset( $params[ "active" ] );
		$this->containsProducts = isset( $params[ "containsProducts" ] );

		if ( strlen( $_files[ "image" ][ "name" ] ) > 0 ) {
			$extension = explode( ".", $_files[ "image" ][ "name" ] );
			$extension = end( $extension );
			$file      = StoreCategory::$imagePath . JUtil::randomString( 17 ) . "." . $extension;

			@move_uploaded_file( $_files[ "image" ][ "tmp_name" ], $__DP . $file );
			#todo check for success
			$this->image = $file;
		}
		$this->parentCategory = $params[ "topCategory" ] > 0 ? $params[ "topCategory" ] : null;
		$this->save();
		$brand = new StoreBrand( $params[ "brandId" ] );
		$brand->saveCategories( $this );

		return true;
	}

	/**
	 * @param $params
	 * @author Özgür Köy
	 */
	public function addProducts( $params ) {
		$codes = explode( "\r\n", $params[ "codes" ] );
		$unprocessed = array();
		foreach ( $codes as $c0 ) {
			$c0 = trim( $c0 );
			if ( !strlen( $c0 ) > 0 ) continue;
			#check existing code
			if( $this->checkCodeExists( $c0 ) !== true ){
				$unprocessed[] = $c0;
				continue;
			}

			$p             = new StoreProduct();
			$p->expireDate = strtotime( $params[ "date" ] );
			$p->code       = $c0;
			$p->save();

			$this->saveProducts( $p );
		}
		return sizeof($unprocessed)==0?true:$unprocessed;

	}

	/**
	 * @param $code
	 * @return bool
	 * @author Özgür Köy
	 */
	public function checkCodeExists( $code ){
		$p = new StoreProduct();
		$p->load( array( "code" => $code, "products"=>$this->id ) );

		return !( $p->gotValue );
	}

	/**
	 * @param           $storeCategoryId
	 * @param Publisher $publisher
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function buyProduct( $storeCategoryId, Publisher &$publisher ) {
		if ( ($check = PublisherMoneyAction::checkPurchase( $publisher, $storeCategoryId )) !== true )
			return $check;


		$c = new StoreProduct();
		JPDO::beginTransaction();
		try {
			$tempCode = JUtil::randomString();

			$c->update(
				array( "products"     => $storeCategoryId,
				       "checkoutCode" => "IS NULL",
				       "expireDate"   => "&gt " . time()
				),
				array( "checkoutCode" => $tempCode,
				       "used"         => 1
				),
				1
			);

			#check if it's there and load
			$c = new StoreProduct();
			$c->load( array( "checkoutCode" => $tempCode ) );

			if ( $c->gotValue !== true )
				throw new JError( "problem with purchase : can't find the checkoutCode created" );


			$purchase = PublisherMoneyAction::purchase( $publisher, $c );

			if($purchase !== true)
				throw new JError( "problem with purchase, purchase save problem" );

			$publisher->saveStoreProduct( $c );

			JPDO::commit();

			return $c->code;
		}
		catch ( JError $j ) {
			JPDO::rollback();
			JLog::log( "cri", "product code buy problem publisher error : " .  $j->getMessage() . " {$publisher->id} storecategory:" . $storeCategoryId );

			return false;
		}

	}

	/**
	 * @param $id
	 * @return StoreBrand
	 * @author Özgür Köy
	 */
	public static function findBrand( $id ){
		$c = new StoreCategory( $id );
		$c->loadStoreCategory();

		if($c->parentCategory && $c->parentCategory->gotValue)
			return StoreCategory::findBrand( $c->parentCategory->id );
		else{
			$c->loadStoreBrand();

			return $c->categories;
		}
	}
}