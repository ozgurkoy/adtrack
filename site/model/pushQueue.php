<?php
/**
 * pushQueue real class - firstly generated on 18-11-2012 14:54, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/pushQueue.php';

class PushQueue extends PushQueue_base
{
    /**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

    /**
     * Create a push notification queue message
     *
     * @param PublisherMobileDevice $pmd publisher's mobile device
     * @param string $alert message text
     * @param string $type type of tge push notification
     * @param int $did id of the notification object
     * @return void
     * @author Murat
     */
    public static function createPushNotification(PublisherMobileDevice &$pmd, $alert, $type, $did){
        $pq = new PushQueue();
        $pq->type = $type=="cd"?PushQueue::ENUM_TYPE_NEW_CAMPAIGN : PushQueue::ENUM_TYPE_DASHBOARD_DETAIL;

//        $pq->deviceToken = $deviceToken;
        $pq->payload = json_encode(array(
            "aps" => array(
                "alert" => $alert,
                "sound" => "default"
            ),
            "type" => $type,
            "did" => $did
        ));
        $pq->state = "w";
        $pq->sendTime = time();
        $pq->tryCount = 0;
        $pq->publisherMobileDevice_pushQueue = $pmd->id;
        $pq->save();

    }

    /**
	 * Marks row as adding to queue
	 *
	 * @param int $id
	 */
	public static function addingToQueue($id) {
		if(JPDO::connect( JCache::read('JDSN.MDB') )) {
			$sql = "UPDATE pushQueue SET state='a', lastUpdate = UNIX_TIMESTAMP() WHERE id=".$id;
			JPDO::executeQuery($sql);
		}
	}

    /**
	 * Marks row as added to queue
	 *
	 * @param type $cronId
	 */
	public static function addedToQueue($id) {
		if(JPDO::connect( JCache::read('JDSN.MDB') )) {
			$sql = "UPDATE pushQueue SET state='q', lastUpdate = UNIX_TIMESTAMP() WHERE id=".$id;
			JPDO::executeQuery($sql);
		}
	}

	/**
	 * Marks row as given state
	 *
	 * @param type $cronId
	 */
	public static function changeState($id,$state) {
		if(JPDO::connect( JCache::read('JDSN.MDB') )) {
			$sql = "UPDATE pushQueue SET state='$state', lastUpdate = UNIX_TIMESTAMP() WHERE id=".$id;
			JPDO::executeQuery($sql);
		}
	}

    /**
     * Add to push messages to push queue
     *
     * @return void
     * @author Murat
     */
    public static function pushMessages(){
        global $__DP, $debugLevel;

        if(JPDO::connect( JCache::read('JDSN.MDB') )) {
            $sql = "
                SELECT
                    pq.id,
                    publisherMobileDevice_pushQueue mobDevice,
                    pq.payload
                FROM
                    pushQueue AS pq
                WHERE
                    pq.state IN ('w','e')
                    AND pq.sendTime <= UNIX_TIMESTAMP()
                    AND pq.trycount < 10
                ORDER BY
                    pq.sendTime
            ";
            //echo $sql; die();
            $rows = JPDO::executeQuery($sql);
            if(count($rows)) {
                if (!class_exists('AMQPConnection', false)) require_once $__DP.'site/cron/php-amqplib/amqp.inc';

                $conn = new AMQPConnection(RMQ_HOST, RMQ_PORT, RMQ_USER, RMQ_PASS, RMQ_VHOST);
                $ch = $conn->channel();

                $ch->queue_declare(RMQ_QUEUE_PUSH, false, true, false, false);

                foreach($rows as $row) {
//                    PushQueue::addingToQueue($row['id']);
                    $pmd = new PublisherMobileDevice($row["mobDevice"]);
                    $msg_body = json_encode(Array("type"=>"push","id" => $row['id'], "deviceToken" => $pmd->pushKey, "payload" => $row['payload']));
                    $pay = json_decode($row["payload"]);
                    switch($pmd->type){
                        case $pmd::ENUM_TYPE_ANDROID:
                            /* FOR MURAT */
                            $a = new PushAndroid(/*SETTINGS API KEY*/);
                            $response = $a->notify($pmd->pushKey,$pay["aps"]["alert"])->getOutputAsArray();
                            if($response["success"]==1){
                                /*WE ARE GOOD */

                            }
                            else{
                                /*NOT GOOD*/
                            }
                            /* ^^^^ CAN'T KNOW FOR SURE WHAT THE RESPONSE IS LIKE TILL WE CAN TEST , SAMPLE IS HERE : http://developer.android.com/google/gcm/notifications.html#response */
                            break;
                        /*APPLE , MURAT*/
                    }

                    $msg = new AMQPMessage($msg_body, array('content_type' => 'text/plain', 'delivery_mode' => 2));
                    $ch->basic_publish($msg, RMQ_EXCHANGE, RMQ_QUEUE_PUSH);
                    if($debugLevel > 0) {
                        Tool::echoLog($msg_body);
                    }
                    PushQueue::addedToQueue($row['id']);
                }

                $ch->close();
                $conn->close();
            } else {
                if($debugLevel > 0) {
                    Tool::echoLog('No push message to que');
                }
            }
        } else {
            JLog::log('cri','Push message queue could not read');
        }
    }

}

