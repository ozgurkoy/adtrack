<?php
//use Swagger\Annotations as SWG;

use Swagger\Annotations\Operation;
use Swagger\Annotations\Operations;
use Swagger\Annotations\Parameter;
use Swagger\Annotations\Parameters;
use Swagger\Annotations\Api;
use Swagger\Annotations\ErrorResponse;
use Swagger\Annotations\ErrorResponses;
use Swagger\Annotations\Resource;
use Swagger\Annotations\AllowableValues;

use Swagger\Annotations\Properties;
use Swagger\Annotations\Property;
use Swagger\Annotations\Model;
use Swagger\Annotations\Items;


/**
 * php53 swagger.phar -p ../../admingleV2/_dev/site/model/ -o 2
 * http://localhost/swagger/swagger-php/2/api-docs.json
 * http://zircote.com/swagger-php/annotations.html#model
 * http://docs.phonegap.com/en/2.9.0/cordova_device_device.model.md.html#device.model
 *
 * @package
 * @category
 * @subpackage
 *ֿֿֿ
 * @Resource(
 *  apiVersion="0.1",
 *  swaggerVersion="1.1",
 *  basePath="http://merkur.admingle.com/wsv2/test",
 *  resourcePath="/test"
 * )
 *
 */
include($__DP . "/site/model/wsObjects.php");
class v2wsTesting
{
	protected $base;


	function __construct()
	{
		$this->base = v2wscore::getInstance();
	}


	/**
	 * Test push notification
	 * @return LoginResponse
	 * @author    Sam
	 *
	 * @Api(
	 *            path="/pushUser",
	 *            description="send User Push",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="sending user push notification",
	 *            responseClass="acceptResponse",
	 *            nickname="pushTest",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="json",
	 *           description="User Login Data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="AndroidPushRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="406",
	 *            reason="Invalid input"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function TestPush()
	{

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$AndroidPushRequest = new AndroidPushRequest($this->base->_request->json);
		}

		// Add twitter to existing user
		$this->base->tokenCheck();

		//$p = new Publisher();
		$p = $this->base->_publisher;

		$p->loadPublisher_publisherMobileDevice();

		$res = new acceptResponse();

		do{

			if($p->publisher_publisherMobileDevice->devicePlatform == "android")
			{

				$android = new PushAndroid($AndroidPushRequest->pushkey);
				$resGoogle= $android->notify(array($p->publisher_publisherMobileDevice->pushKey),$AndroidPushRequest->msg);
				$res->value = $res->value. print_r($resGoogle,true);

			}


		}while($p->publisher_publisherMobileDevice->populate());

		$this->base->sendResponse(200,$res->getJsonData());

	}

	/**
	 * Test push notification
	 * @return LoginResponse
	 * @author    Sam
	 *
	 * @Api(
	 *            path="/pushDevice",
	 *            description="send User Push",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="sending push notification by device",
	 *            responseClass="acceptResponse",
	 *            nickname="pushTest",
	 * @parameters(
	 * @parameter(
	 *           name="json",
	 *           description="device data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="AndroidPushRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="406",
	 *            reason="Invalid input"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function TestPushByDevice()
	{
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$AndroidPushRequest = new AndroidPushRequest($this->base->_request->json);
		}

		$res = new acceptResponse();

		$pushKey = "APA91bHvQks1fLPd1NPlS_MC9mBrzCkJthgN2ns9_T6MUBmLM8XmVsdlg6Zuh8ks8ci_kqZsgJvf_yCq02sTjJQIAZWlAzw0PB6FEuLR7IxxiLPOiFPJ0F-4ooPSZ4JTkUKQltp413UPTNELg_uIU9H4yW2FaTaZHw";

		$android = new PushAndroid("AIzaSyCfwSZ0Car8ONZRITVMHHKJ8_i6zsQGKYk");

		$payload = json_encode(array(
			"header" => "header",
			"type" => "C",
			"itemId" => 1
		));

		//$res->value= $android->notify(array($AndroidPushRequest->registration_id),json_encode($AndroidPushRequest->data));

		if(PushMobileMessage::pushAndroidMessage($AndroidPushRequest->registration_id,$AndroidPushRequest->data)){
			$res->value = "Good";
		}else{
			$res->value = "error";
		}

		//$res->value =$res->value->output;

		$this->base->sendResponse(200,$res->getJsonData());

		/*
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$AndroidPushRequest = new AndroidPushRequest($this->base->_request->json);
		}

		$res = new acceptResponse();
		$android = new PushAndroid($AndroidPushRequest->pushkey);
		$res->value= $android->notify(array($AndroidPushRequest->deviceID),$AndroidPushRequest->msg);
		$res->value =$res->value->output;

		$this->base->sendResponse(200,$res->getJsonData());
		*/
	}

	/**
	 * Test push notification
	 * @return LoginResponse
	 * @author    Sam
	 *
	 * @Api(
	 *            path="/pushIOSDevice",
	 *            description="send User Push",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="sending push notification by PuTO IOS",
	 *            responseClass="acceptResponse",
	 *            nickname="pushTest",
	 * @parameters(
	 * @parameter(
	 *           name="json",
	 *           description="device data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="AndroidPushRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="406",
	 *            reason="Invalid input"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function TestPushByDeviceIOS()
	{
		global $__DP;
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$AndroidPushRequest = new AndroidPushRequest($this->base->_request->json);
		}

		$res = new acceptResponse();

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert',  $__DP."site/def/ck.pem");
		stream_context_set_option($ctx, 'ssl', 'passphrase', "adMingle2014");

		// Open a connection to the APNS server
		$err = null;
		$errstr = null;
		echo "<PRE>";
		echo "connecting to APNS server \n";
		$fp = stream_socket_client("ssl://gateway.sandbox.push.apple.com:2195", $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp){
			echo "Failed to connect APNS " . "ssl://gateway.sandbox.push.apple.com:2195" . " error: $err $errstr \n";
		} else {
			echo "Connected to APNS \n";
			//$payload = '{"aps":{"alert":"New Campaign","sound":"default"},"type":"cd","did":"89"}';

			$payload = json_encode(array(
				"aps" => array("alert"=>$AndroidPushRequest->data->header,"header"=>"New Campaign","sound"=>"default"),
				"type" => $AndroidPushRequest->data->type,
				"itemId" => $AndroidPushRequest->data->itemId
			));

			$message = chr(0) . pack('n', 32) . pack('H*', $AndroidPushRequest->registration_id) . pack('n', strlen($payload)) . $payload;

			// Send it to the server
			$result = fwrite($fp, $message, strlen($message));

			print_r($result);

			if (!$result){
				echo 'Message not delivered to APNS server' . PHP_EOL;
				//Mark push queue row as error
			} else {
				echo 'Message successfully delivered to APNS server' . PHP_EOL;
				//Mark push queue row as success
			}

			// Close the connection to the server
			fclose($fp);

		}

		$res->value = "Good";

		//$res->value =$res->value->output;

		$this->base->sendResponse(200,$res->getJsonData());


	}

	/**
	 * Set Message Status
	 * @return void
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/sendUserMessage",
	 *            description="send Message to user",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Send Message to user",
	 *            responseClass="AcceptResponse",
	 *            nickname="sendUserMessage",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="data",
	 *           description="messages ids for action",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="sendMessageRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="404",
	 *            reason="Message Not Found"
	 *          ),
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="406",
	 *            reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function sendUserMessage()
	{
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {

			$data = new sendMessageRequest($this->base->_request->json);

			//$p = new Publisher();
			$p = $this->base->_publisher;
			$u = $p->loadUser();

			$mesTpl = new LangMessageHistory();
			$mesTpl->messageBody = $data->body;
			$mesTpl->messageTitle = $data->header;
			$mesTpl->messageCategory = LangMessageHistory::ENUM_MESSAGECATEGORY_COSTUM;

			$mesTpl->save();

			$c = new Campaign();
			$c->load(array("publicID"=>$data->campaign));


			UserMessage::create($u->id,null,null,$mesTpl->id,$c->id,UserMessage::ENUM_MSGTYPE_SYSTEM);


			$response = new AcceptResponse();

			$response->value=true;

			$this->base->sendResponse(200, $response->getJsonData());
		}
	}

}