<?php
use Swagger\Annotations\Operation;
use Swagger\Annotations\Operations;
use Swagger\Annotations\Parameter;
use Swagger\Annotations\Parameters;
use Swagger\Annotations\Api;
use Swagger\Annotations\ErrorResponse;
use Swagger\Annotations\ErrorResponses;
use Swagger\Annotations\Resource;
use Swagger\Annotations\AllowableValues;

use Swagger\Annotations\Properties;
use Swagger\Annotations\Property;
use Swagger\Annotations\Model;
use Swagger\Annotations\Items;


/**
 * @package
 * @category
 * @subpackage
 *
 * @Resource(
 *  apiVersion="0.1",
 *  swaggerVersion="1.1",
 *  basePath="http://n.admingle.com/wsv2/campaign",
 *  resourcePath="/campaign"
 * )
 *
 */
include($__DP."/site/model/wsObjects.php");
class v2wsCampaign {
	protected $base;

	function __construct() {
		$this->base = v2wscore::getInstance();
	}

	/**
	 * List of publisher campaigns
	 *
	 * Get Start Messages , filter (read , unread),
	 * @return void
	 * @author Murat
	 *
	 *  @Api(
	 *   path="/campList",
	 *   description="Get All Publisher Campaigns ",
	 *   @operations(
	 *     @operation(
	 *       httpMethod="POST",
	 *       summary="Get Campaigns",
	 *       responseClass="CampaignsResponse",
	 *       nickname="campList",
	 *       @parameters(
	 *         @parameter(
	 *          name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 *         @parameter(
	 *          name="campParams",
	 *           description="Filter parameters",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="CampaignAllListRequest"
	 *         )
	 *       ),
	 *       @errorResponses(
	 *          @errorResponse(
	 *            code="88",
	 *           reason="Parameter Problem"
	 *          ),
	 *          @errorResponse(
	 *            code="321",
	 *           reason="Invalid camp list type"
	 *          ),
	 *          @errorResponse(
	 *            code="322",
	 *           reason="Invalid orderBy type"
	 *          ),
	 *          @errorResponse(
	 *            code="323",
	 *           reason="Invalid page number"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function pubCampaignList(){
		$this->base->tokenCheck();

		$clr = new CampaignListRequest($this->base->_request->json);
		$params = $clr->getArrayData();

		$params["pid"] = $this->base->_publisher->id;
		$vr = ValidateCampaign::pubCampList($params);
		$params["campStatus"] = "all";

		$cr = new CampaignsResponse();
		if ($vr->valid){
			$r = Campaign::pubCampList($params);
			$cr->recordCount = $r["recordCount"];
			$cr->currentPage = $r["currentPage"];
			$cr->campaigns = array();
			foreach ($r["camps"] as $campaign){
				$c = new CampaignResponse();

				switch($campaign["publisherStatus"]){
					case CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION:
						$c->status = "o";
						break;
					case CampaignPublisher::ENUM_STATE_APPROVED:
						if($campaign["status"] == Campaign::ENUM_STATE_FINISHED)
						{
							$c->status = "f";
						}else
						{
							$c->status = "a";
						}
						break;
					default:
						$c->status = "r";
						break;
				}

				$c->id = $campaign["publicID"];
				$c->race = $campaign["race"];
				$c->title = $campaign["title"];
				$c->date = $campaign["date"];
				$c->imgUrl="http://lagukaraoke.com/wp-content/themes/komodo/img/no-image-70.jpg";
				$c->price = $campaign["price"];
				$cr->campaigns[] = $c;
				unset($c);
			}
		} else {
			$cr->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
		}
		$this->base->sendResponse(200,$cr->getJsonData());
	}


		/**
	* List of publisher campaigns
	*
	* Get Start Messages , filter (read , unread),
	* @return void
	* @author Murat
	*
	*  @Api(
	*   path="/camps",
	*   description="Get All User System Messages",
	*   @operations(
	*     @operation(
	*       httpMethod="POST",
	*       summary="Get Campaigns",
	*       responseClass="CampaignsResponse",
	*       nickname="camps",
	*       @parameters(
	*         @parameter(
	*          name="token",
	*           description="User Token",
	*           paramType="header",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="string"
	*         ),
	*         @parameter(
	*          name="campParams",
	*           description="Filter parameters",
	*           paramType="body",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="CampaignListRequest"
	*         )
	*       ),
	*       @errorResponses(
	*          @errorResponse(
	*            code="88",
	*           reason="Parameter Problem"
	*          ),
	*          @errorResponse(
	*            code="321",
	*           reason="Invalid camp list type"
	*          ),
	*          @errorResponse(
	*            code="322",
	*           reason="Invalid orderBy type"
	*          ),
	*          @errorResponse(
	*            code="323",
	*           reason="Invalid page number"
	*          )
	*       )
	*     )
	*   )
	* )
	*/
	public function pubCampaigns(){
		$this->base->tokenCheck();

		$clr = new CampaignListRequest($this->base->_request->json);
		$params = $clr->getArrayData();

		$params["pid"] = $this->base->_publisher->id;
		$vr = ValidateCampaign::pubCamps($params);

		$cr = new CampaignsResponse();
		if ($vr->valid){
			$r = Campaign::pubCampList($params);
			$cr->recordCount = $r["recordCount"];
			$cr->currentPage = $r["currentPage"];
			$cr->campaigns = array();
			foreach ($r["camps"] as $campaign){
				$c = new CampaignResponse();
				$c->id = $campaign["publicID"];
				$c->race = $campaign["race"];
				$c->status = $params["campStatus"];
				$c->title = $campaign["title"];
				$c->date = $campaign["date"];
				if($campaign["race"] == Campaign::ENUM_RACE_CPC){
					$c->imgUrl=JUtil::siteAddress(). "site/layout/images/campaignRace_c.png";
				}elseif($campaign["race"] == Campaign::ENUM_RACE_TWITTER){
					$c->imgUrl=JUtil::siteAddress(). "site/layout/images/campaignRace_c.png";
				}else{
					$c->imgUrl=JUtil::siteAddress(). "site/layout/images/adlogofb.png";
				}

				$c->price = $campaign["price"];
				$cr->campaigns[] = $c;
				unset($c);
			}
		} else {
			$cr->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
		}
		$this->base->sendResponse(200,$cr->getJsonData());

                /*
		$type = "";
		$orderBy = "pushDate";
		$page = 0;

		if (isset($this->base->_requestVars->ctype)) $type = $this->base->_requestVars->ctype;

		if ($type == "" || !in_array($type, array("p","f","a","d"))) $type = "p";

		if($type == 'p') {
			$rows = Tool::getPublisherCampaignsWaiting($this->base->_user->id, $orderBy, $page, $this->base->_itemsPerPage);
		} elseif($type == 'f') {
			$rows = Tool::getPublisherCampaignsFinalized($this->base->_user->id, $orderBy, $page, $this->base->_itemsPerPage);
		} elseif($type == 'a') {
			$rows = Tool::getPublisherCampaignsActive($this->base->_user->id, $orderBy, $page, $this->base->_itemsPerPage);
		} elseif($type == 'd') {
			$rows = Tool::getPublisherCampaignsDenied($this->base->_user->id, $orderBy, $page, $this->base->_itemsPerPage);
		}
		
		if (count($rows)){
			for($i=0;$i<count($rows);$i++){
				if ($rows[$i]["race"] != "t"){
					unset($rows[$i]);
				} else {
					$rows[$i]["cost"] = $this->formatCurrency($rows[$i]["cost"]);
				}
			}
		}

		$response = new RestResponse();
		$response->setResponse(200, array("items"=>$rows));
                 * 

                
                $response1 = new CampaignResponse();
                $response1->id="1412412";
                $response1->name="Camp1";
                $response1->revenue="234";
                $response1->startDate="22/01/2013";
                $response1->status="a";
                $response1->type="cpc";
                
                $response2 = new CampaignResponse();
                $response2->id="1412235";
                $response2->name="Camp2";
                $response2->revenue="234";
                $response2->startDate="22/01/2018";
                $response2->status="a";
                $response2->type="awa";
                
                
                $response = new CampaignsResponse();
                $response->campaigns = array($response1,$response2);
                
                RestUtils::sendResponse(200, $response->getJsonData());		
             */
	}

	

	
	/**
	 * Detail information about campaign for publisher
	 *
	 * @return void
	 * @author Murat
	*  @Api(
        *   path="/campDetail",
        *   description="Get campaign detail information",
        *   @operations(
        *     @operation(
        *       httpMethod="POST",
        *       summary="Get Campaigns",
        *       responseClass="CampaignDetailResponse",
        *       nickname="campDetail",
        *       @parameters(
        *         @parameter(
        *          name="token",
        *           description="User Token",
        *           paramType="header",
        *           required="true",
        *           allowMultiple="false",
        *           dataType="string"
        *         ),
        *         @parameter(
        *          name="CampCode",
        *           description="Campaign Code",
        *           paramType="body",
        *           required="true",
        *           allowMultiple="false",
        *           dataType="SingleValueReq"
        *         )
        *       ),
        *       @errorResponses(
        *          @errorResponse(
        *            code="309",
        *            reason="Campaign Not Found"
        *          ),
        *       @errorResponse(
        *            code="403",
        *            reason="Bad token"
        *          ),
	    *       @errorResponse(
	    *           code="88",
	    *           reason="Parameter Problem"
	    *       ),
	    *       @errorResponse(
	    *           code="99",
	    *           reason="Parameter Problem"
	    *       )
        *      )
        *     )
        *   )
        * )
        */
	public function pubCampaignDetail(){
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {

			$this->base->sendResponse(406);
		} else {
			$publicID = new SingleValueReq($this->base->_request->json);
		}

		$params = array(
			"publicID"=>$publicID->value,
			"pid"=>$this->base->_publisher->id
		);

		$vr = ValidateCampaign::campaignPublisher($params);

		$cdr = new CampaignDetailResponse();

		if(is_bool($vr))
		{
			$cdr->error=null;

			$r = Campaign::pubCampDetail($params);

			//print_r($r);exit;

			//$cp = new CampaignPublisher();
			$cp = $r["campaignPublisher"];

			$cdr->id = $cp->id;
			$cdr->race = $cp->campaign_campaignPublisher->race;
			$cdr->state = $cp->state;
			$cdr->cState = $cp->campaign_campaignPublisher->state;
			$cdr->revenue = $cp->cost;

			$cdr->campTitle = $cp->campaign_campaignPublisher->title;
			$cdr->campLink = $cp->campaign_campaignPublisher->campaign_campaignClick->gotValue ? $cp->campaign_campaignPublisher->campaign_campaignClick->link : null;
			$cdr->brand = $cp->campaign_campaignPublisher->brand->name;
			$cdr->startDate = date($this->base->_formatDate,strtotime($cp->campaign_campaignPublisher->startDate));
			$cdr->endDate = date($this->base->_formatDate,strtotime($cp->campaign_campaignPublisher->endDate));
			$cdr->campaignNotes = str_replace("\\\\","\\",$cp->campaign_campaignPublisher->notes);
			$cdr->editMessage = false;
			if ($cdr->race == Campaign::ENUM_RACE_TWITTER){
				$cp->publisher_campaignPublisher->loadSnTwitter();
				$cdr->editMessage = ($cp->publisher_campaignPublisher->publisher_snTwitter->canSetOwnMessage ? true : false);
			}
			$cdr->cpcCost = Format::getFormattedCurrency($r["actionCost"]);

			if ($r["hasCountableLink"]){
				switch($cp->campaign_campaignPublisher->race){
					case Campaign::ENUM_RACE_TWITTER:
						$cdr->calculatedClick = 0;
						$cdr->totalClick = $cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->totalClick;
						break;
					case Campaign::ENUM_RACE_CPC:
						$cdr->calculatedClick = $cp->campaign_campaignPublisher->campaign_campaignCPCMessage->calculatedClick;
						$cdr->totalClick = $cp->campaign_campaignPublisher->campaign_campaignCPCMessage->totalClick;
						break;
					case Campaign::ENUM_RACE_CPL:
						//todo:@Murat: implement this
						break;
				}

				$totalRevenue = $cdr->calculatedClick * $r["actionCost"];
				$cdr->totalRevenue = Format::getFormattedCurrency($totalRevenue);
				$cdr->refStats = $r["refStats"]["refData"];
				$cdr->cpcLink = $r["cpcLink"];
				$cdr->imageLink = JUtil::siteAddress().$cp->campaign_campaignPublisher->uploadPath.$cp->campaign_campaignPublisher->campaign_campaignClick->previewFileName;
				$cdr->clickStats = array();
				if (is_array($r["clickStats"]) && count($r["clickStats"])){
					foreach($r["clickStats"] as $date=>$values){
						$cs = new ClickStats();
						$cs->clickDate = $date;
						$cs->calculatedClick = $values["calculatedClick"];
						$cs->suspectedClick = $values["suspectedClick"];
						$cs->totalClick = $values["totalClick"];
						$cdr->clickStats[] = $cs;
						unset($cs);
					}
				}
			} else {
				$cdr->calculatedClick = 0;
				$cdr->totalClick = 0;
				$cdr->clickStats = 0;
			}
			$cdr->charLimit = $r["charLimit"];
			$cdr->campaignMessages = array();

			do{
				$cm = new PairValueReq();
				$cm->key = $cp->campaign_campaignPublisher->campaign_campaignMessage->id;
				$cm->value = $cp->campaign_campaignPublisher->campaign_campaignMessage->message;
				if($cm->value){
					if ($cdr->race == Campaign::ENUM_RACE_CPC && $cdr->state == CampaignPublisher::ENUM_STATE_APPROVED){
						$cm->value .=  " " . $r["cpcLink"] . " #ad";
					}
					$cdr->campaignMessages[] = $cm;
				}
			} while ($cp->campaign_campaignPublisher->campaign_campaignMessage->populate());

			if ($cdr->race == Campaign::ENUM_RACE_TWITTER && $cdr->state == CampaignPublisher::ENUM_STATE_APPROVED){
				$cdr->tweetMessage = new CampaignTweetMessage();
				$cdr->tweetMessage->state = $cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->state;
				$cdr->tweetMessage->message = $cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->message;
				$cdr->tweetMessage->toSendDate = $cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->toSendDate;
				$cdr->tweetMessage->sendDate = $cp->campaign_campaignPublisher->campaign_campaignTwitterMessage->tweetSentDate;
			} else {
				$cdr->tweetMessage = null;
			}
		}
		else
		{
			$cdr->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
		}

		$this->base->sendResponse(200,$cdr->getJsonData());
	}

	/**
	* Campaign accept method
	*
	* @return void
	* @author Murat
	*  @Api(
	*   path="/campAccept",
	*   description="Accept Campaign",
	*   @operations(
	*     @operation(
	*       httpMethod="POST",
	*       summary="Accept Campaign Campaigns",
	*       responseClass="AcceptResponse",
	*       nickname="campAccept",
	*       @parameters(
	*         @parameter(
	*          name="token",
	*           description="User Token",
	*           paramType="header",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="string"
	*         ),
	*         @parameter(
	*          name="params",
	*           description="CampaignID",
	*           paramType="body",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="CampaignAcceptRequest"
	*         )
	*       ),
	*       @errorResponses(
	*          @errorResponse(
	*            code="406",
	*            reason="Bad parameters"
	*          ),
	*          @errorResponse(
	*            code="88",
	*            reason="Parameter missing"
	*          ),
	*          @errorResponse(
	*            code="309",
	*            reason="Campaign not found"
	*          ),
	*          @errorResponse(
	*            code="310",
	*            reason="Campaign offer action taken before"
	*          ),
	*          @errorResponse(
	*            code="311",
	*            reason="Custom message empty"
	*          ),
	*          @errorResponse(
	*            code="312",
	*            reason="Custom message character count is over limit"
	*          ),
	*          @errorResponse(
	*            code="313",
	*            reason="Invalid campaign message selected"
	*          ),
	*          @errorResponse(
	*            code="314",
	*            reason="Invalid send date"
	*          ),
	*          @errorResponse(
	*            code="315",
	*            reason="Invalid send time"
	*          ),
	*          @errorResponse(
	*            code="316",
	*            reason="Tweet send time should be in range"
	*          ),
	*          @errorResponse(
	*            code="317",
	*            reason="Tweet send time should be after current time"
	*          ),
	*          @errorResponse(
	*            code="318",
	*            reason="Late to take action"
	*          ),
	*          @errorResponse(
	*            code="319",
	*            reason="Budget ended"
	*          ),
	*          @errorResponse(
	*            code="320",
	*            reason="Unknown state to take action"
	*          )
	*       )
	*     )
	*   )
	* )
	*/
	public function pubAcceptCampaign(){
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$car = new CampaignAcceptRequest($this->base->_request->json);
			$params = $car->getArrayData();
			$params["pid"] = $this->base->_publisher->id;
			$params["dateFormatString"] = Setting::getValue("dateFormat");

			$vr = ValidateCampaign::pubAcceptCampaign($params);

			$ar = new AcceptResponse();
			if ($vr->valid){
				if (Campaign::pubAcceptCamp($params,$vr->returnParams["cp"])){
					$ar->value = "ok";
				} else {
					$err = new ErrResponse(500,"Internal Server Error");
					$err->errorText = $ar->value;
					$ar->error = $err;
				}
			} else {
				$ar->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
			}
			$this->base->sendResponse(200,$ar->getJsonData());
		}

		$this->base->sendResponse(200);
	}


	/**
	* Campaign deny method
	*  @Api(
	*   path="/campDeny",
	*   description="Deny Campaign",
	*   @operations(
	*     @operation(
	*       httpMethod="POST",
	*       summary="Deny Campaign",
	*       responseClass="DenyResponse",
	*       nickname="campDeny",
	*       @parameters(
	*         @parameter(
	*          name="token",
	*           description="User Token",
	*           paramType="header",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="string"
	*         ),
	*         @parameter(
	*          name="params",
	*           description="Campaign ID",
	*           paramType="body",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="CampaignDenyRequest"
	*         )
	*       ),
	*       @errorResponses(
	*          @errorResponse(
	*            code="406",
	*            reason="Bad parameters"
	*          ),
	*          @errorResponse(
	*            code="88",
	*            reason="Parameter missing"
	*          ),
	*          @errorResponse(
	*            code="309",
	*            reason="Campaign not found"
	*          ),
	*          @errorResponse(
	*            code="310",
	*            reason="Campaign offer action taken before"
	*          ),
	*          @errorResponse(
	*            code="318",
	*            reason="Late to take action"
	*          ),
	*          @errorResponse(
	*            code="319",
	*            reason="Budget ended"
	*          ),
	*          @errorResponse(
	*            code="320",
	*            reason="Unknown state to take action"
	*          )
	*       )
	*     )
	*   )
	* )
	*/
	public function pubDenyCampaign() {
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$car = new CampaignDenyRequest($this->base->_request->json);
			$params = $car->getArrayData();
			$params["pid"] = $this->base->_publisher->id;
			$vr = ValidateCampaign::pubDenyCampaign($params);

			$ar = new AcceptResponse();
			if ($vr->valid){
				if (Campaign::pubDenyCamp($params,$vr->returnParams["cp"])){
					$ar->value = "ok";
				} else {
					$ar->error = new ErrResponse(500,"Internal Server Error");
				}
			} else {
				$ar->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
			}
			$this->base->sendResponse(200,$ar->getJsonData());
		}

		$this->base->sendResponse(200);
	}



	/**
	* Campaign create method
	*  @Api(
	*   path="/campCreate",
	*   description="Create Campaign",
	*   @operations(
	*     @operation(
	*       httpMethod="POST",
	*       summary="Create Campaign",
	*       responseClass="CampaignCreateResponse",
	*       nickname="campCreate",
	*       @parameters(
	*         @parameter(
	*          name="params",
	*           description="CampaignData",
	*           paramType="body",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="CampaignCreateRequest"
	*         )
	*       ),
	*       @errorResponses(
	*          @errorResponse(
	*            code="406",
	*            reason="Bad parameters"
	*          ),
	*          @errorResponse(
	*            code="88",
	*            reason="Parameter missing"
	*          ),
	 *          @errorResponse(
	 *            code="301",
	 *            reason="Undefined campaign race"
	 *          ),
	 *          @errorResponse(
	 *            code="302",
	 *            reason="Invalid start date"
	 *          ),
	 *          @errorResponse(
	 *            code="303",
	 *            reason="Invalid end date"
	 *          ),
	 *          @errorResponse(
	 *            code="304",
	 *            reason="Brandname already used"
	 *          ),
	 *          @errorResponse(
	 *            code="305",
	 *            reason="Campaign min. start date did not reached"
	 *          ),
	 *          @errorResponse(
	 *            code="306",
	 *            reason="Campaign max. day exceeded"
	 *          ),
	 *          @errorResponse(
	 *            code="307",
	 *            reason="Campaign min. day insufficient"
	 *          ),
	 *          @errorResponse(
	 *            code="308",
	 *            reason="Campaign max. day should be greater than the min. day"
	 *          )
	*       )
	*     )
	*   )
	* )
	*/
	public function advCreateCampaign(){
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$aDay = 60 * 60 * 24;


			$ccr = new CampaignCreateRequest($this->base->_request->json);
			$params = $ccr->getArrayData();

			//$params["advertiserId"] = $advertiserId;
			$params["brand"] = -1;
			$vr = ValidateCampaign::validateStep1($params);

			$ar = new CampaignCreateResponse();
			if ($vr === true){
				$vr = ValidateCampaign::validateStep2($params);
				if ($vr === true){
					$params1 = array(
						"advertiserId" => $params["advertiserId"],
						"race" => $ccr->race

					);
					$campaign = Campaign::createStep1($params1);
					$params2 = array(
						"advertiserId" => $params["advertiserId"],
						"campaign" => $campaign,
						"title" => $ccr->title,
						"startDate" => $ccr->startDate,
						"endDate" => $ccr->endDate,
						"brand" => -1,
						"brandName" => $ccr->brandName
					);
					$campaign = Campaign::createStep2($params2);
					$params3 = array(
						"advertiserId" => $params["advertiserId"],
						"campaign" => $campaign,
						"age" => "13-17,18-24,25-34,35-44,45-54,55+",
						"sex" => 3,
						"interests" => Trend::getAll()->_ids,
						"budget" => 1000
					);
					$campaign = Campaign::createStep3($params3);
					$params4 = array(
						"campaign" => $campaign,
						"link" => $ccr->link,
						"previewFileName" => "previewFileName.png",
						"campDetail" => "Campaign detail text should be here",
						"keywords" => "keyword1,keyword2,keyword3",
						"campNotes" => $ccr->campNotes,
						"message" => array()
					);
					if (is_array($ccr->messages) && count($ccr->messages)){
						foreach ($ccr->messages as $message){
							$params4["message"][] = $message;
						}
					}
					$campaign = Campaign::createStep4($params4);
					$params5 = array(
						"advertiserId" => $params["advertiserId"],
						"campaign" => $campaign,
						"state" => Campaign::ENUM_STATE_INITIAL,
						"maxClick" => 1000,
						"cpcPrice" => 0.15,
						"commissionRate" => 0,
						"budget" => 1000
					);
					$campaign = Campaign::createStep5($params5);

					$ar->value = "ok";
					$ar->campaignId = $campaign->publicID;
				} else {
					$ar->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
				}
			} else {
				$ar->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
			}
			$this->base->sendResponse(200,$ar->getJsonData());
		}

		$this->base->sendResponse(200);
	}

	/**
	* Change campaign status method
	*  @Api(
	*   path="/campChangeStatus",
	*   description="Create Campaign",
	*   @operations(
	*     @operation(
	*       httpMethod="POST",
	*       summary="Change Campaign Status",
	*       responseClass="AcceptResponse",
	*       nickname="campChangeStatus",
	*       @parameters(
	*         @parameter(
	*          name="params",
	*           description="ChangeCampaignStatusRequest",
	*           paramType="body",
	*           required="true",
	*           allowMultiple="false",
	*           dataType="ChangeCampaignStatusRequest"
	*         )
	*       ),
	*       @errorResponses(
	*          @errorResponse(
	*            code="406",
	*            reason="Bad parameters"
	*          ),
	*          @errorResponse(
	*            code="88",
	*            reason="Parameter missing"
	*          ),
	*          @errorResponse(
	*            code="309",
	*            reason="Campaign not found"
	*          ),
	*          @errorResponse(
	*            code="324",
	*            reason="Invalid status"
	*          ),
	*          @errorResponse(
	*            code="325",
	*            reason="Campaign status can't set to back"
	*          )
	*       )
	*     )
	*   )
	* )
	*/
	public function advChangeCampaignStatus(){
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {

			$ccsr = new ChangeCampaignStatusRequest($this->base->_request->json);
			$params = $ccsr->getArrayData();

			$vr = ValidateCampaign::advChangeCampaignStatus($params);

			$ar = new AcceptResponse();
			if ($vr->valid === true){
				$c = $vr->returnParams["c"];
				$c->state = $ccsr->status;
				$c->save();
				switch($ccsr->status){
					case Campaign::ENUM_STATE_PENDING_PROGRESS:
						$param = array("id"=>$c->id,"publishDate"=>date('Y-m-d'),"publishTime"=>date('H:i'));
						Campaign::publishToPublishers($param);

						break;
					case Campaign::ENUM_STATE_PROGRESSING:
						if ($c->race == Campaign::ENUM_RACE_TWITTER){
							$cp = new CampaignPublisher();
							$cp->load(array(
								"campaign_campaignPublisher" => $c->id,
								"state" => CampaignPublisher::ENUM_STATE_WAITING_USER_ACTION
							));
							if ($cp->gotValue){
								do{
									$cp->state = CampaignPublisher::ENUM_STATE_LATE_TO_JOIN;
									$cp->save();
								}while($cp->populate());
							}
						}
						break;
					case Campaign::ENUM_STATE_FINISHED:
						$cp = new CampaignPublisher();
						Campaign::endCampaign($c->id);

						break;
				}
			} else {
				$ar->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
			}

			$this->base->sendResponse(200,$ar->getJsonData());
		}

		$this->base->sendResponse(200);
	}

}
?>