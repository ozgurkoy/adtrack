<?php
/**
 * cpaAffCampaign real class - firstly generated on 07-09-2014 20:55, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cpaAffCampaign.php';

class CpaAffCampaign extends CpaAffCampaign_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * get code for publisher
	 *
	 * @param Publisher $publisher
	 * @param null      $code
	 * @return CpaAffCampaignUser
	 * @author Özgür Köy
	 */
	public function getCode( Publisher $publisher = null, $code = null ) {
		if ( is_null( $code ) )
			$code = JUtil::randomString( 10 );

		$cpl = new CpaAffCampaignUser();
		if ( !is_null( $publisher ) ) {
			$cpl->load( array( "cpaAffCampaignUser_publisher" => $publisher , "cpaAffCampaign_cpaAffCampaignUser" => $this ) );
		}

		if ( $cpl->gotValue !== true || is_null( $publisher ) ) {
			$cpl       = new CpaAffCampaignUser();
			$cpl->code = $code;
			$cpl->save();
			$cpl->saveCpaAffCampaign( $this );

			if ( !is_null( $publisher ) )
				$cpl->saveCpaAffCampaignUser_publisher( $publisher );

		}

		return $cpl;
	}




	/**
	 * get short url for cpl.remote campaigns
	 *
	 * @param $publisherId
	 * @param $campaignID
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function getShortURL( $publisherId, $campaignID ) {
		$c = new Campaign();
		$c->load( array( "publicID" => $campaignID ) );

		if ( $c->gotValue ) {
			$c->loadCpaAffCampaign();
			$c->campaign_cpaAff->loadCpaAffCampaign_cpaAffCampaignUser(
				array( "cpaAffCampaignUser_publisher" => $publisherId )
			);
			$cu = &$c->campaign_cpaAff->cpaAffCampaign_cpaAffCampaignUser;

			if (
				$cu &&
				$cu->gotValue
			) {
				if ( strlen( $cu->shortUrl ) > 0 )
					return $cu->shortUrl;

				$shortUrl = Campaign::getRedLink(
					$c->publicID,
					$c->campaign_cpaAff->preparePreviewLink(),
					$cu->code,
					array(
						"title"       => $c->title,
						"description" => $c->campaign_cpaAff->campDetail,
						"image"       => $c->getPreviewImageLink(),
						"shortUrl"    => $c->campaign_cpaAff->SLDomain
					)
				);

				$cu->shortUrl = $shortUrl;
				$cu->save();

				unset( $cu, $c );

				if ( $shortUrl !== false )
					return $shortUrl;
			}
		}
		JLog::log( "cri", "Something went wrong with get cpl short url" );

		return false;
	}


	/**
	 * @author Özgür Köy
	 */
	public function campaignStart() {
		$this->state = CpaAffCampaign::ENUM_STATE_STABILE;
		$this->save();

		$this->writeHistory( "campaign started", CpaAffCampaignHistory::ENUM_MADEBY_SYSTEM );
	}

	/**
	 * @author Özgür Köy
	 */
	public function campaignComplete() {
		$this->state = CpaAffCampaign::ENUM_STATE_ENDED;
		$this->save(); // we make it here so that it wont happen again.

		$this->loadCampaign();

		$p = Backend::createSupervisionTask(
			"finalize CPA.Aff Campaign #".$this->campaign_cpaAff->id,
			"finalizeCPAAffCampaign"
		);

		if($p !== false){
			$fin = $p->addChildTask(
				"finalization",
				"finalizeCPAAffCampaign",
				array(
					'cpaCampaign'=>$this
				)
			);

			if ( $fin !== false ) {
				$fin->addChildTask(
					"sending emails:" . $this->campaign_cpaAff->id . " name:" . $this->campaign_cpaAff->title,
					"sendCampaignFinishingMails",
					array( "campaignId" => $this->campaign_cpaAff->id )
				);
			}

			$p->runTask();
		}
	}


	/**
	 * api response handler
	 *
	 * @param $response
	 * @return void
	 * @author Özgür Köy
	 */
	public static function apiAction() {
		$c         = new CpaAffCampaign();
		$c->getActCampaigns( CpaAffCampaign::ENUM_OPTYPE_API );
		$calledTypes = array();
		if ( $c->gotValue ) {
			do {
				$c->loadCpaAffService();
				if ( !in_array( $c->cpaAffService->id, $calledTypes ) ) {
					call_user_func( "CpaAffCampaign::apiCall_" . $c->cpaAffService->name );
					$calledTypes[ ] = $c->cpaAffService->id;
				}
			} while ( $c->populate() );
		}

		unset( $c );
	}

	public static function apiCall_CJ() {
		$url     = Setting::getValue( "cpaAffCampaignAPIUrl_CJ" );
//		$url     = "http://n.admingle.com/cpaAffApi";
		$data = array(
			"date-type"=>"posting",
			"start-date"=>date("Y-m-d", time() - 31 * 3600 * 24 ), //31 is the max days we can go back
			"end-date"=>date("Y-m-d", time() )
		);

		$pp = http_build_query( $data );

		$url .= "?$pp";

		$h = array(
			"authorization" => Setting::getValue( "cpaAffCampaignAPIKey_CJ" ),
			"Cache-Control" => "no-cache",
			"Accept"        => "*/*"
		);

		$request = new cRestRequest(
			$url,
			"GET",
			$data
		);

		$request->setHeaderOptions($h)->execute();
		$ch = $request->responseBody;
		/*
		 * TODO : better error handling
		*/
		if ( strlen( $ch ) < 2 || strpos( $ch, "xml version" ) == false ) {
			JLog::log( "api", "Cpa Aff CJ API Call failed" . serialize( $request ) );
		}
		else
			self::apiResponseParser_CJ( $ch );

	}

	/**
	 * parse api response
	 *
	 * @param $xmlString
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function apiResponseParser_CJ( $xmlString ) {
		$coms = CpaAffCampaign::parseAPIXML_CJ( $xmlString );
		JPDO::beginTransaction();

		try{
			foreach ( $coms as $com ) {
				#find the publisher
				if( !(strlen($com["sid"])>0)){
					continue;
				}

				$cpaUser = new CpaAffCampaignUser();
				$cpaUser->loadByCode( $com[ "sid" ] );
				if ( $cpaUser->gotValue !== true ){
					JLog::log("api","Cpa Aff CJ API Response, Unrelated lead.(($xmlString))");
					continue;
				}

				$lot0 = array(
					"actionId"    => $com[ "original-action-id" ],
					"status"      => $com[ "action-status" ],
					"description" => trim( $com[ "action-tracker-name" ] ),
					"amount"      => floatval( $com[ "commission-amount" ] ),
					"date"        => $com[ "event-date" ]
				);

				$cpaUser->loadCpaAffCampaign();
				$cpaUser->updateLeadAmount( $lot0 );
				/*
				if( !(strlen($com["sid"])>0)){
					continue;
				}

				$cpaUser->loadByCode( $com[ "sid" ] );

				if ( $cpaUser->gotValue === true ) {
					echo "FOUND{$com[ "sid" ]}<BR>";
					$cpaUser->loadCpaAffCampaign();
					$g = new CpaAffGain();
					$g->loadByUId( $com[ "original-action-id" ] );

					$h = new CpaAffGainHistory();

					if ( $g->gotValue )
						$h->oldStatus = $g->status;

					$newStatus      = strtolower( $com[ "action-status" ] ) == "closed" ? CpaAffGain::ENUM_STATUS_APPROVED : CpaAffGain::ENUM_STATUS_PENDING;
					$newAmount      = $com[ "commission-amount" ];

					if ( $g->status == $newStatus && $g->amount == $newAmount ) {
						//nothing changed.
						continue;
					}

					$g->status      = $newStatus;
					$g->amount      = $com[ "commission-amount" ];
					$g->details     = json_encode( $com );
					$g->ndate       = strtotime( $com[ "event-date" ] );
					$g->revPerc     = $cpaUser->cpaAffCampaign_cpaAffCampaignUser->revPerc;
					$g->uid         = $com[ "original-action-id" ];
					$g->description = $com[ "action-tracker-name" ];
					$g->save();
					$cpaUser->saveGain( $g );

					$h->newStatus = $g->status;
					$h->details   = json_encode( $com );
					$h->save();

					if ( $g->status == CpaAffGain::ENUM_STATUS_APPROVED ) {
						$cpaUser->loadCpaAffCampaign()->loadCampaign();

						//create a revenue
						$r = new PublisherRevenue();
						$r->loadByCustomId( $com[ "original-action-id" ] );

						if ( $r->gotValue === true ) {
							$cpaUser->cpaAffCampaign_cpaAffCampaignUser->writeHistory( "Revenue already payed. Check Response : " . json_encode( $com ), CpaAffCampaignHistory::ENUM_MADEBY_SYSTEM );
						}
						else {
							$r->amount                     = $g->amount * ( 100 - $cpaUser->cpaAffCampaign_cpaAffCampaignUser->revPerc ) / 100;
							$r->title                      = "Cpa.Aff {$g->uid}";
							$r->customId                   = $com[ "original-action-id" ];
							$r->status                     = PublisherRevenue::ENUM_STATUS_APPROVED;
							$r->approveDate                = time();
							$r->campaign_publisherRevenue  = $cpaUser->cpaAffCampaign_cpaAffCampaignUser->campaign_cpaAff->id;
							$r->publisher_publisherRevenue = $cpaUser->cpaAffCampaignUser_publisher;

							$r->save();

							$cpaUser->cpaAffCampaign_cpaAffCampaignUser->writeHistory( "Publisher Payment to $cpaUser->cpaAffCampaignUser_publisher , amount : {$r->amount}", CpaAffCampaignHistory::ENUM_MADEBY_SYSTEM );
						}
					}

					$g->saveHistory( $h );
				}
				else{
					JLog::log("api","Cpa Aff CJ API Response, Unrelated lead.(($xmlString))");
				}
				*/
			}

			return true;
		}catch (JError $j){
			JPDO::rollback();
			JLog::log("api","Cpa Aff CJ API ERROR".serialize($j));
			return false;
		}
	}


	/**
	 * parse xml string
	 *
	 * @param $details
	 * @return array
	 * @author Özgür Köy
	 */
	public static function parseAPIXML_CJ( $details ) {
		$p   = simplexml_load_string( $details );
		$res = array();
		foreach ( $p->commissions->commission as $pcc ) {
			$res[ ] = (array)$pcc;
		}

		return $res;
	}


	/**
	 * get campaigns to check for api
	 *
	 * @param null $opType
	 * @author Özgür Köy
	 */
	public function getActCampaigns( $opType=null ) {
		$filter = array(
			"campaign_cpaAff" => array(
				"race"    => Campaign::ENUM_RACE_CPA_AFF,
				"endDate" => "&ge " . date( 'Y-m-d', ( time() - intval( Setting::getValue( "cpaAffCampaignCheckDaysAfterEnd" ) * 3600 * 24 ) ) )
			)
		);
		if ( is_null( $opType ) !== true )
			$filter[ "opType" ] = $opType;
		$this->load( $filter );
	}

	public function getLeadAmounts() {
		if ( $this->gotValue !== true )
			return false;

		$g = new CpaAffGain();
		$g->groupBy(array(array("`cpaAffGain`.`status`","status")))->sum(
			"`cpaAffGain`.`amount`*(100-`cpaAffGain`.`revPerc`)/100",
			"ams",
			array(
				"gain"   => array(
					"cpaAffCampaign_cpaAffCampaignUser"=>$this->id
				)
			)
		);

		$res = array( "APPROVED" => 0, "PENDING" => 0 );

		if($g->gotValue){
			do {
				$res[ $g->status_options[$g->status] ] = Format::getFormattedCurrency($g->ams,true);
			} while ( $g->populate() );
		}
		unset( $g );

		return $res;
	}

	public function getLeadNumbers() {
		if ( $this->gotValue !== true )
			return false;

		$g = new CpaAffGain();
		$g->groupBy(array(array("`cpaAffGain`.`status`","status")))->count(
			"id",
			"cnt",
			array(
				"gain"   => array(
					"cpaAffCampaign_cpaAffCampaignUser"=>$this->id
				)
			)
		);

		$res = array( "APPROVED" => 0, "PENDING" => 0 );

		if($g->gotValue){
			do {
				$res[ $g->status_options[$g->status] ] = $g->cnt;
			} while ( $g->populate() );
		}
		unset( $g );

		return $res;
	}

	public function writeHistory( $action, $madeBy = CpaAffCampaignHistory::ENUM_MADEBY_USER ) {
		$cv          = new CpaAffCampaignHistory();
		$cv->content = $action;
		$cv->details = json_encode( $_SERVER );
		$cv->madeBy  = $madeBy;

		$cv->save();
		$cv->saveCpaAffCampaign( $this );

		unset( $cv );
	}

	public function preparePreviewLink() {
		if ( strpos( $this->link, "?" ) !== false )
			return $this->link . "&" . $this->hashCode . "=[code]";
		else
			return $this->link . "?" . $this->hashCode . "=[code]";
	}


	/**
	 * Create copy of preview image for campaign copy
	 *
	 * @return array
	 * @author Murat
	 */
	public function copyPreviewImage(){
		global $__DP;
		$uploadPath = "site/layout/images/campaign/";

		// image name
		$name = "_temp_" . JUtil::randomString(20) . "." . substr($this->previewFileName,-3);

		// location to save cropped image
		$url = $__DP.$uploadPath.$name;

		copy($uploadPath.$this->previewFileName,$url);

		$filesize = filesize( $url );

		//$fileName = $name . substr($url,-3);
		return array(
			"fullPath" => $url,
			"size"=>$filesize,
			"fileName" => $name
		);
	}


	/**
	 * @param      $pathToExcel
	 * @param bool $manualOperation
	 * @return array
	 * @author Özgür Köy
	 * @throws JError
	 */
	public function updateByExcel( $pathToExcel, $manualOperation=false ) {
		$f = file_get_contents( $pathToExcel );
		$p = preg_split( "~\r\n|\n|\r~sm", $f );
		$keys = array();

		//single column
		foreach ( $p as $fc=>$pline ) {
			if ( empty( $pline ) )
				continue;
			//rid of the internal commas
			if(strpos($pline,'"')!==false){
				$pline = preg_replace_callback("/\"(.+?)\"/mi",function($ms){
					if(strpos($ms[0],'"')!==false){
						return str_replace( ",", " ", $ms[ 0 ] );
					}

				}, $pline);
				$pline = preg_replace("/\"/m", "", $pline); //we remove all the " separately and intentionally
			}

			$parts       = explode( ",", $pline );
			$validStatus = array( "pending", "approved" );

			if ( sizeof( $parts ) != 6 ) {
				return array( "bad", "File must consist of a six columns : {".$pline."}" );
			}
			list( $date, $description, $status, $amount, $actionId, $sid )=$parts;
			$status = strtolower(trim( $status ));

			if(!is_numeric($amount)){
				return array("bad", "Amount must be numeric ( NOT '{$amount}' )");
			}
			if ( !( strlen( trim( $actionId ) ) > 0 ) ) {
				return array( "bad", "Action id is important" );
			}
			if ( !( strlen( trim( $sid ) ) > 0 ) ) {
				return array( "bad", "SID is important ( NOT '{$sid}' )" );
			}

			if ( !( strlen( trim( $status ) ) > 0 ) ) {
				return array( "bad", "Status is important ( NOT '{$status}' )" );
			}

			if ( in_array( $status, $validStatus ) === false )
				return array( "bad", $status . " is not a valid status. Valid statuses are " . implode( ",", $validStatus ) );

			$k0 = trim( $sid );
			if ( !array_key_exists( $k0, $keys ) )
				$keys[ $k0 ] = array( );

			$keys[ $k0 ][ ] = array(
				"actionId"    => $actionId,
				"status"      => ( $status == "approved" ? "closed" : "pending" ),
				"description" => trim( $description ),
				"amount"      => floatval( trim( $amount ) ),
				"date"        => trim( $date )
			);
		}

		if ( sizeof( $keys ) == 0 )
			return array( "bad", "Empty file" );

		//check for keys and apply
		JPDO::beginTransaction();
		$c = new CpaAffCampaignUser();
		try {
			foreach ( $keys as $key => $lot ) {
				$c->loadByCode( $key );
				foreach ( $lot as $lot0 ) {
					if ( $c->gotValue !== true )
						throw new JError( $key, 99 );

					$c->loadCpaAffCampaign();
					if( $c->cpaAffCampaign_cpaAffCampaignUser->id != $this->id )
						throw new JError( $key, 101 );

					unset( $css );

					$c->updateLeadAmount( $lot0 );
				}
			}
		}
		catch ( JError $j ) {
			JPDO::rollback();
			if($j->getCode() == 99 )
				return array( "bad", "Key not found in campaign : ".$j->getMessage() );
			elseif($j->getCode() == 101 )
				return array( "bad", "Key belongs to another campaign : ".$j->getMessage() );
			elseif($j->getCode() == 103 )
				return array( "bad", "Status change of action from approved to pending : ".$j->getMessage() );
			elseif($j->getCode() == 105 )
				return array( "bad", "Amount change on previously created action : ".$j->getMessage() );
		}

		JPDO::commit();
		return array( "good", "Updated, refreshing." );

	}
}

