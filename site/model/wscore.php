<?php

class wscore {
	/**
	 * Singleton
	 */
	private static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance)
        {
            self::$_instance = new wscore();
        }

        return self::$_instance;
    }

	/**
	 * Request object for all webservice methods
	 */
	public $_request = null;
	/**
	 * Request variables for all webservice methods
	 */
	public $_requestVars = null;
	/**
	 * User object for all webservice methods
	 * @property User
	 */
	public $_user = null;
	/**
	 * Company object for all webservice methods
	 * @property Company
	 */
	public $_company = null;
	/**
	 * Encryption key for token
	 */
	public $_encryptionKey = "Va#X8%JSKo&Z";
	/**
	 * Items in per page of lists
	 */
	public $_itemsPerPage = 100000;
	/**
	 * User lang string for request
	 */
	public $_uLang = DEFAULT_LANGUAGE;
	/**
	 * Language words
	 */
	public $_langWords = null;

	/**
	 * Encrypt the given string
	 *
	 * @param string $s
	 * @return string
	 * @author Murat
	 */
	public function encrypt($s){

		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($this->_encryptionKey), $s, MCRYPT_MODE_CBC, md5(md5($this->_encryptionKey))));
	}


	/**
	 * Decrypt the given string
	 *
	 * @param string $s
	 * @return string
	 * @author Murat
	 */
	public function decrypt($s){

		return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($this->_encryptionKey), base64_decode($s), MCRYPT_MODE_CBC, md5(md5($this->_encryptionKey))), "\0");
	}

	/**
	 * Log Webservice Request
	 *
	 * @return void
	 * @author Murat
	 */
	public function logRequest($module,$action){
		$lw = new LogWebservice();
		$lw->requestDate = time();
		$lw->request = json_encode(array("module"=>$module,"action"=>$action,"request"=>$this->_request));
		$lw->save();
	}

	/**
	 * Token check for validation
	 *
	 * @return boolean
	 * @author Murat
	 */
	public function tokenCheck(){
		$result = false;

		if (isset($this->_requestVars->token)){

			try{
				$this->_user = new User();
				$this->_user->webserviceToken = $this->decrypt($this->_requestVars->token);
				$this->_user->load();
				if ($this->_user->count){
					if ($this->_user->utype == "a")
						$this->_company = $this->_user->loadCompany();
					$result = true;
				}
			}catch (Exception $ex) {}
		}

		if (!$result){
			$response = new RestResponse();
			$response->setResponseCode(999);
			RestUtils::sendResponse(200,$response);
			exit();
		}

		return $result;
	}
	
	/**
	 * Get proper word with selected language
	 * 
	 * @return string
	 * @author Murat
	 */
	public function getLangWord($wordCode){
		if (is_null($this->_langWords)){
			$this->_langWords = LangWords::getLangWords("word",  $this->_uLang);
		}
		if (isset($this->_langWords[$wordCode])){
			return $this->_langWords[$wordCode];
		} else {
			return null;
		}
	}
}
?>
