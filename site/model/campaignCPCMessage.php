<?php
/**
 * campaignCPCMessage real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignCPCMessage.php';

class CampaignCPCMessage extends CampaignCPCMessage_base
{
	/**
	 * CampaignPublisher
	 *
	 * @var CampaignPublisher
	 **/
	public $campaignPublisher;

	/**
	 * PublisherRevenue
	 *
	 * @var PublisherRevenue
	 */
	public $publisherRevenue;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Load CampaignPublisher record
	 *
	 * @return CampaignPublisher
	 * @author Murat
	 */
	public function loadCampaignPublisher(){
		$this->campaignPublisher = new CampaignPublisher();
		$this->campaignPublisher->load(array(
			"campaign_campaignPublisher" => $this->campaign_campaignCPCMessage,
			"publisher_campaignPublisher" => $this->publisher_campaignCPCMessage,
		));
		return $this->campaignPublisher;
	}

	/**
	 * Load PublisherRevenue record
	 *
	 * @return PublisherRevenue
	 * @author Murat
	 */
	public function loadPublisherRevenue(){
		$this->publisherRevenue = new PublisherRevenue();
		$this->publisherRevenue->load(array(
			"campaign_publisherRevenue" => $this->campaign_campaignCPCMessage,
			"publisher_publisherRevenue" => $this->publisher_campaignCPCMessage,
		));
		if (!$this->publisherRevenue->gotValue){
			$this->publisherRevenue->campaign_publisherRevenue = $this->campaign_campaignCPCMessage;
			$this->publisherRevenue->publisher_publisherRevenue = $this->publisher_campaignCPCMessage;
			$this->publisherRevenue->msgindex = 0;
			$this->publisherRevenue->save();
		}
		return $this->publisherRevenue;
	}

	/**
	 * Block hash
	 *
	 * @param int $id
	 *
	 * @return string
	 *
	 * @author Murat
	 */
	public static function block($id,$kh,$dsUserId = null){
		$result = "";

		$ccm = new CampaignCPCMessage($id);
		if (!$ccm->gotValue){
			$result = "CampaignNotFound";
		} else {
			if ($ccm->isBlocked){
				$result = "AlreadyBlocked";
			} else {
				$pr = new PublisherRevenue();
				$pr->load(array("publisher_publisherRevenue"=>$ccm->publisherRevenue,"campaign_publisherRevenue"=>$ccm->campaign_campaignCPCMessage));
				if (!$pr->gotValue){
					$result = "UserRevenueRecordNotFound";
				} elseif ($pr->status == PublisherRevenue::ENUM_STATUS_APPROVED){
					$result = "UserRevenueAlreadyApproved";
				} else {
					$data = Tool::callSLService("setLinkAsBlack",array("params"=>  json_encode(array(
						"hash" => $ccm->linkHash,
						"killHistory" => $kh
					))));
					if (empty($data->error) && is_object($data) && $data->response == "OK"){
						$cbp = new CampaignBlockedPublisher();
						$cbp->load(array("campaignCPCMessage_campaignBlockedPublisher"=>$ccm->id));
						if (!$cbp->gotValue){
							$cbp->campaignCPCMessage_campaignBlockedPublisher = $ccm->id;
						}
						$cbp->count = $data->result;
						$cbp->killHistory = $kh;
						$cbp->campaignBlockedPublisher_DSuser = $dsUserId;
						$cbp->save();
						$ccm->isBlocked = 1;
						$ccm->save();
						$result = "ok";
					} else {
						$result = "InternalServerError";
					}
				}
			}
		}
		return $result;
	}
}

