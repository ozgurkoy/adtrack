<?php
/**
 * DSrole real class - firstly generated on 22-11-2013 18:08, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/DSrole.php';

class DSrole extends DSrole_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function parsePerms() {
		$perm = array();
		$this->loadPermissions();
		if ( $this->permissions ) {
			do {
				$this->permissions->loadAction();
				if($this->permissions->action)
					$perm[ $this->permissions->action->id ] = $this->permissions->bits;
			} while ( $this->permissions->populate() );
		}

		$this->parsedPerms = $perm;
	}

}

