<?php
/**
 * project real class - firstly generated on 28-10-2014 12:42, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/project.php';

class Project extends Project_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function loadByName( $name ) {
		$c = new Project();
		$c->load( array( "name" => "LIKE " . trim($name) ) );

		return $c->gotValue ? $c : false;

	}
}

