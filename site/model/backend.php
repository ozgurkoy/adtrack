<?php
/**
 * backend real class - firstly generated on 15-04-2014 08:16, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/backend.php';

class Backend extends Backend_base
{
	const JOB_ACTIVE  = 100;
	const LOG_SEPERATOR  = "\n/*&&&*/\n";

	public $jobObject = null;
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);

		JPDO::excludeFromTransaction( $this->dsn );
	}

	public function getDetails() {
		global $__DP;
		$content = "";
		if(is_file($__DP . 'site/backend/proc/active/' . $this->code)){
			$content .= "\n------\nOUTPUT : \n";
			$content .= file_get_contents( $__DP . 'site/backend/proc/active/' . $this->code );
		}
		if(is_file($__DP . 'site/backend/proc/errors/' . $this->code)){
			$content .= "\n------\nERROR : \n";
			$content .= file_get_contents( $__DP . 'site/backend/proc/errors/' . $this->code );
		}
		if(strlen($content)>0)
			$content .= "\n------\n";

		return $content;
	}

	/**
	 * new task
	 *
	 * @param       $job
	 * @param array $params
	 * @author Özgür Köy
	 */
	public function task( $label, $job, $params=array() ) {
		if ( is_null( $this->code ) )
			$this->hashCode( $job, $params );

		if( $this->assignJob($job) === true ){
			$pars = base64_encode(serialize( $params ));
			//base 64 for null chars. somehow pdo driver fails on null char
//			$pars = base64_encode(preg_replace( "|\\0|","",$pars ));

			if ( $this->gotValue !== true ) {
				$this->label       = $label;
				$this->job         = $job;
				$this->parameters  = $pars;
				$this->timeout     = $this->jobObject->getTimeout();
				$this->starter     = serialize( $_SESSION );
				$this->status      = Backend::ENUM_STATUS_PENDING;
				$this->description = $this->jobObject->description;
				$this->redirectURL = $this->jobObject->redirectURL;
				$this->save();
				$this->writeHistory( "Task inited" );
			}

			Backend::run( $this, $this->code );
		}
	}

	/**
	 * add child to operation
	 *
	 * @param       $label
	 * @param       $job
	 * @param array $params
	 * @return Backend|bool
	 * @author Özgür Köy
	 */
	public function addChildTask( $label, $job, $params=array() ) {
		if ( $this->gotValue === true ) {
			$b = new Backend();

			if ( is_null( $b->code ) )
				$b->hashCode( $job, $params );

			if ( $b->assignJob( $job ) === true ) {
				if ( $b->gotValue !== true ) {
					$pars = base64_encode(serialize( $params ));

					$b->label       = $label;
					$b->job         = $job;
					$b->parameters  = $pars;
					$b->timeout     = $b->jobObject->getTimeout();
					$b->starter     = serialize( $_SESSION );
					$b->status      = Backend::ENUM_STATUS_PENDING;
					$b->description = $b->jobObject->description;
					$b->redirectURL = $b->jobObject->redirectURL;

					$b->save();
					$b->gotValue = true;

					$b->writeHistory( "Task inited to parent:" . $this->id );
					$b->saveBackend( $this );
					$this->writeHistory( "Child Task added:" . $b->id );

					return $b;
				}
			}
		}

		return false;
	}

	/**
	 * create parent task
	 *
	 * @param $label
	 * @param $job
	 * @return Backend
	 * @author Özgür Köy
	 */
	public static function createSupervisionTask( $label, $job ) {
		$b = new Backend();
//		JPDO::excludeFromTransaction( $b->dsn );

		if ( is_null( $b->code ) )
			$b->hashCode( $job );

		if( $b->assignJob($job) === true ){
			$params = array();

			if ( $b->gotValue !== true ) {
				$pars = base64_encode(serialize( $params ));

				$b->label       = $label;
				$b->job         = $job;
				$b->parameters  = $pars;
				$b->timeout     = $b->jobObject->getTimeout();
				$b->starter     = serialize( $_SESSION );
				$b->status      = Backend::ENUM_STATUS_PENDING;
				$b->description = $b->jobObject->description;
				$b->redirectURL = $b->jobObject->redirectURL;
				$b->isParent    = 1;
				$b->save();
				$b->gotValue = true;

				$b->writeHistory( "created parent task" );

				return $b;
			}
		}

		return false;
	}

	/**
	 * @author Özgür Köy
	 */
	public function runTask() {
		if ( $this->gotValue/* && $this->isParent == 1*/ ) {
			$this->status = Backend::ENUM_STATUS_WORKING;
			$this->save();

			$ch = $this->loadChildren();

			if ( $ch && $ch->gotValue ) {
				do {
					Backend::run( $ch, $ch->code );
				} while ( $ch->populate() );
			}
		}
		else
			die( "task not present or is not a parent task" );
	}

	/**
	 * assign job
	 *
	 * @param $job
	 * @return bool
	 * @author Özgür Köy
	 */
	public function assignJob( $job ) {
		global $__DP;
		if ( is_file( $__DP . "site/backend/" . $job . ".php" ) && $this->isOnGoing() === false ) {
			include_once $__DP . "site/backend/" . $job . ".php";
			$f = ucfirst( $job );
			$this->jobObject = new $f;

			return true;
		}
		return false;
	}

	/**
	 * @return bool
	 * @author Özgür Köy
	 */
	public  function isOnGoing() {
		$r  = false;
		if ( $this->gotValue && in_array( $this->status, array( Backend::ENUM_STATUS_COMPLETE, Backend::ENUM_STATUS_ERROR ) ) ){
			$this->writeHistory( "Job check while it's ongoing" );
			$r = true;
		}

		unset( $b );
		return $r;
	}

	/**
	 * @param $job
	 * @param $params
	 * @author Özgür Köy
	 */
	public function hashCode( $job, $params=array() ) {
		global $__DP;
		$sort = true;
		foreach ( $params as $pk => $pv ) {
			if(is_object($pv)){
				$sort = false;
				break;
			}
		}

		if($sort === true)
			array_multisort( $params,SORT_REGULAR  );

		$code = md5( $job.json_encode($params) );

		if ( ( $b = Backend::loadByCode( $code ) ) !== false ) {
			unset( $b );
			$code = md5( $job . json_encode( $params ) . time() );
		}


		$this->code = $code;
	}

	/**
	 * @param null $code
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public function status( $code=null ) {
		global $__DP;

		if(is_null($code))
			$code = $this->code;

		if ( is_file( $__DP . 'site/backend/proc/errors/' . $code ) && filesize( $__DP . 'site/backend/proc/errors/' . $code ) > 0 ) {
			$f = file_get_contents( $__DP . 'site/backend/proc/errors/' . $code );

			if( strpos($f, "ERROR_INPUT") !== false ){
				JPDO::rollback(); // general precaution

				$this->errorState = file_get_contents( $__DP . 'site/backend/proc/errors/' . $code );
				$this->save();

				if ( $this->jobObject->haltOnError === true ) {
					$this->endTime = time();
					$this->status  = Backend::ENUM_STATUS_ERROR;
					$this->save();

					$this->jobObject->error();

					Processmanager::complete( $this->code, $this->pid, $this->killPid );

					$this->writeHistory( "Error on operation : " . serialize( $this->errorState ) );
					$this->completeTask(); // to check for parent.
					return Backend::ENUM_STATUS_ERROR;
				}
			}
		}

		if ( is_file( $__DP . 'site/backend/proc/active/' . $code ) ) {
			//check if it's really dead
			if ( !is_null( $this->jobObject->errorStatus ) && $this->jobObject->haltOnError === true ) {
				$this->status = Backend::ENUM_STATUS_ERROR;

				$this->errorState = $this->jobObject->errorStatus;
				$this->endTime    = time();
				$this->save();

				$this->jobObject->error();

				Processmanager::complete( $this->code, $this->pid, $this->killPid );

				$this->writeHistory( "Error on operation(class) : " . serialize( $this->errorState ) );

				return Backend::ENUM_STATUS_ERROR;
			}
			elseif ( $this->jobObject->completed == true ) {
				$this->writeHistory( "Completing from status() " );

				return $this->completeTask();
			}
			else
				return Backend::ENUM_STATUS_WORKING;
		}
		elseif ( is_file( $__DP . 'site/backend/proc/dead/' . $code ) ){
			return $this->completeTask();
		}
		else{
			return Backend::ENUM_STATUS_ERROR;
		}
	}

	/**
	 * @param mixed|null $action
	 * @return bool|JModel|void
	 * @author Özgür Köy
	 */
	public function writeHistory( $action ) {
		$h = new BackendHistory();
		$h->action = $action ;
		$h->save();

		$this->saveHistory( $h );
		unset( $h );
	}

	/**
	 * Complete task
	 *
	 * @author Özgür Köy
	 */
	public function completeTask() {
//		$this->reloadById(); #reload final status

		$this->writeHistory( "completting ".$this->status );

		if ( $this->status == Backend::ENUM_STATUS_WORKING ) {
			//some kill operation might have happened.
			$this->writeHistory( "task complete" );
			if ( !( strlen( $this->returnValue ) > 0 ) && is_object( $this->jobObject ) )
				$this->returnValue = $this->jobObject->returnValue;

			$this->status      = Backend::ENUM_STATUS_COMPLETE;
			$this->endTime     = time();
			$this->save();

			Processmanager::complete( $this->code, $this->pid, $this->killPid );
		}

		//check for parent
		$be = $this->loadBackend();
		if ( $be && $be->gotValue && in_array($be->status, array(Backend::ENUM_STATUS_WORKING, Backend::ENUM_STATUS_PENDING) ) ) {
			$cstatus = $be->checkChildrenStatus();
			$ret = $be->collectChildrenReturnValues();
			if ( $cstatus == "ERROR" ) {
				$be->writeHistory( "Error with child" . ( var_export( $cstatus, true ) ) );
				$be->returnValue   = sizeof( $ret ) > 0 ? implode( "||", $ret[ "return" ] ) : "";
				$be->contentOutput = implode( PHP_EOL, $ret[ "output" ] );
				$be->errorState    = implode( PHP_EOL, $ret[ "error" ] );
				$be->status        = Backend::ENUM_STATUS_ERROR;
				$be->endTime       = time();
				$be->save();

				DSNotification::addNotification( null, "Backend Job Got Error", $be->label, DSNotification::ENUM_TYPE_ERROR, "ds_backendDetail/".$be->id );
			}
			elseif ( $cstatus == "GOOD" ) {

				if ( !is_null( $be->returnValue ) )
					array_unshift( $ret, $be->returnValue );

				$be->returnValue   = implode( "||", $ret[ "return" ] );
				$be->contentOutput = implode( PHP_EOL, $ret[ "output" ] );
				$be->errorState    = implode( PHP_EOL, $ret[ "error" ] );
				$be->save();
				$be->completeTask();
			}

			unset( $be );
		}

		return $this->status;
	}

	/**
	 * Check for children status
	 *
	 * @author Özgür Köy
	 */
	public function checkChildrenStatus() {
		$b = new Backend();
		$b->count( "id", "cid",
			array(
				'status'  => array(Backend::ENUM_STATUS_ERROR, Backend::ENUM_STATUS_KILLED),
				'_custom' => "`parentBackend` = " . $this->id
			)
		);
		JPDO::debugQuery(false);

		$error = $b->cid;
		if( intval($error) > 0 ){
			unset( $b );
			return "ERROR";
		}

		$b = new Backend();
		$b->count( "id", "cid",
			array(
				'status'  => array( Backend::ENUM_STATUS_PENDING, Backend::ENUM_STATUS_WORKING ),
				'_custom' => "`parentBackend` = " . $this->id
			)
		);
		$working = $b->cid;
		unset( $b );

		if ($working > 0)
			return "BAD";
		else
			return "GOOD";
	}

	/**
	 * @return Backend
	 * @author Özgür Köy
	 */
	public function loadChildren( $all = false ) {
		$b = new Backend();
		$b->populateOnce(false)->load(
			array(
				'_custom' => "`parentBackend` = " . $this->id .
					( $all === false ? ( " AND `status`=" . Backend::ENUM_STATUS_PENDING ) : "" )
			)
		);

		return $b;
	}

	/**
	 * collect return values from children
	 *
	 * @return array
	 * @author Özgür Köy
	 */
	public function collectChildrenReturnValues() {
		$c   = $this->loadChildren( true );
		$ret = array("return"=>array(),"output"=>array());

		if ( $c->gotValue ) {
			do {
				$ret[ "return" ][ ] = $c->returnValue;
				$ret[ "output" ][ ] = $c->contentOutput;
				$ret[ "error" ][ ]  = $c->errorState;
			} while ( $c->populate() );
		}

		unset( $c );

		return $ret;
	}

	/**
	 * @return int
	 * @author Özgür Köy
	 */
	public function countChildren() {
		$b = new Backend();
		$b->count( "id", "cid",
			array(
				'_custom' => "`parentBackend` = $this->id"
			)
		);
		$c = $b->cid;
		unset( $b );
		return $c;
	}

	/**
	 * Kill by code
	 *
	 * @param $code
	 * @author Özgür Köy
	 */
	public static function killByCode( $code ) {
		$b = self::loadByCode( $code );
		if ( $b->gotValue && $b->status == Backend::ENUM_STATUS_WORKING ) {
			$b->status = Backend::ENUM_STATUS_KILLED;
			$b->save();
			$b->writeHistory( "Killed by assassin" );

			Processmanager::complete( $code, $b->pid, null, true );
			DSNotification::addNotification( null, "Backend Job Killed", $b->label, DSNotification::ENUM_TYPE_ERROR, "ds_backendDetail/".$b->id );

			$b->completeTask();
		}
		unset( $b );
	}

	public static function run( Backend &$b ) {
		global $__DP;

		$manager              = new Processmanager();
		$manager->executable  = PHP_BINDIR . "/php";

		$manager->addScript(
			$__DP . 'site/backend/lib/_run.php ' . $__DP . ' ' . $b->code,
			$b->timeout,
			$b->code
		);

		$pids = $manager->exec();

		//TODO: Support for multiple scripts ?
		$b->pid     = $pids[ 0 ][ 0 ];
		$b->killPid = $pids[ 0 ][ 1 ];
		$b->save();
	}

	/**
	 * execute function called by _run.php
	 *
	 * @param Backend $b
	 * @author Özgür Köy
	 */
	public static function exec( Backend &$b ) {
		$b->status = self::ENUM_STATUS_WORKING;
		$b->save();

		if ( $b->assignJob( $b->job ) !== false ) {
			$params = unserialize( base64_decode($b->parameters) );
			$b->jobObject->init( $params, $b->code );
		}

		define( "REGISTER_FUNCTION_OVERRIDE", 1 );
		set_error_handler( array( $b, "_backendErrorHandler" ) );
		register_shutdown_function( array( $b, "_backendShutdown" ) );

		$b->jobObject->run();

		//check children
		if ( ( $ch = $b->loadChildren() ) ) {
			if ( $ch->gotValue ) {
				$b->returnValue = $b->jobObject->returnValue;
				$b->save();

				$b->runTask();
			}
			else{
//				$b->status = $b->status();
//				$b->save();
//
				//this is obsolete now. but the status function does one thing, it checks for happening errors.
				//while ( $b->status() == Backend::ENUM_STATUS_WORKING ) {
				//$b->writeHistory( "Still working status" );
				//	sleep( 2 );
				//}
				Processmanager::complete( $b->code, $b->pid, $b->killPid );
			}
		}

	}

	public function _backendErrorHandler( $errno, $errstr, $errfile, $errline ) {
		$error = array( "error_file" => $errfile, "line" => $errline, "error_string" => $errstr );
		$this->appendToErrorFile( json_encode( $error ), "RUNTIME" );
	}

	public function _backendShutdown( ) {
		ob_end_clean();
		$error = error_get_last();
		if ( is_array( $error ) ) {
			$this->appendToErrorFile( json_encode( array( "line" => $error[ "line" ], "file" => $error[ "file" ], "message" => $error[ "message" ] ) ), "FATAL" );

			JPDO::rollback();
		}
		else
			$this->appendToFile( $this->contentOutput );

		$this->status = $this->status( null );
	}

	public function getOutput( $content ) {
		$this->writeHistory( "W:" . $content );
		$this->contentOutput = $content;
	}

	public static function rerunTask( $id ) {
		$b = new Backend( $id );

		if ( $b->gotValue ) {
			$b->resetStates();

			if($b->isParent == 1)
				$b->runTask();
			else
				Backend::run( $b, $b->code );
		}
		else
			echo "not found";
	}

	public function resetStates() {
		#JPDO::debugQuery();
		$this->status        = Backend::ENUM_STATUS_PENDING;
		$this->errorState    = '';
		$this->contentOutput = '';
		$this->jdate         = time();
		$this->endTime       = null;
		$this->retries       = intval( $this->retries ) + 1;
		$this->pid           = 0;
		$this->killPid       = 0;
		$this->save();

		$b = new Backend();
		$b->nopop()->load( array( "_custom" => "`parentBackend`=" . $this->id ) );
		while ( $b->populate() ) {
			$b->resetStates();
		}
		#JPDO::debugQuery( false );

	}

	/**
	 * this might be auto. not sure.
	 *
	 * @author Özgür Köy
	 */
	public function redirectToPending() {
		if( $this->jobObject->redirectToPendingPage !== false ){
			JUtil::jredirect( $this->jobObject->pendingPageURL . $this->code );
		}
	}

	/**
	 * @param $code
	 * @return Backend|bool
	 * @author Özgür Köy
	 */
	public static function loadByCode( $code ) {
		$b = new Backend();
		$b->load( array( "code" => $code ) );

		if($b->gotValue){
			return $b;
		}
		else{
			unset( $b );
			return false;
		}
	}

	public function appendToErrorFile($content, $prefix = "") {
		global $__DP;
		file_put_contents( $__DP . 'site/backend/proc/errors/' . $this->code, "ERROR_INPUT (".($prefix==""?"Error":$prefix).") :".$content.Backend::LOG_SEPERATOR, FILE_APPEND );
	}

	public function appendToFile($content, $prefix = "") {
		global $__DP;
		file_put_contents( $__DP . 'site/backend/proc/active/' . $this->code, $content.Backend::LOG_SEPERATOR, FILE_APPEND );
	}
}