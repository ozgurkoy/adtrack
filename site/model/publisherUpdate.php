<?php
/**
 * publisherUpdate real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherUpdate.php';

class PublisherUpdate extends PublisherUpdate_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Add/Update PublisherUpdate record
	 *
	 * @param Publisher $publisher
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function addUpdate($publisher,$calculate=true){
		$publisher->loadPublisher_publisherUpdate();
		if (!is_object($publisher->publisher_publisherUpdate) || (is_object($publisher->publisher_publisherUpdate) && !$publisher->publisher_publisherUpdate->gotValue)){
			$publisher->publisher_publisherUpdate = new PublisherUpdate();
			$publisher->publisher_publisherUpdate->publisher_publisherUpdate = $publisher;
		}
		$publisher->publisher_publisherUpdate->twitterTryCount = 0;
		$publisher->publisher_publisherUpdate->twitterStatus = PublisherUpdate::ENUM_TWITTERSTATUS_NEW;
		$publisher->publisher_publisherUpdate->calculateStatus = PublisherUpdate::ENUM_TWITTERSTATUS_NEW;
		$publisher->publisher_publisherUpdate->save();

		if ($calculate) self::bulkCalculate($publisher->publisher_publisherUpdate->id);
	}

	/**
	 * Publisher daily Twitter information bulk update
	 *
	 * @return void
	 * @author Murat
	 */
	public static function bulkUpdate(){
		global $debugLevel;
		//JPDO::beginTransaction();
		$pu = new PublisherUpdate();
		$yesterday = strtotime('-1 day');
		$yesterhour = strtotime('-1 hour');
		$filter = array(
			"publisher_publisherUpdate" => array(
				"user_publisher" => array(
					"status" => User::ENUM_STATUS_ACTIVE
				),
				"publisher_snTwitter" => array(
					//"errorStatus" => "IS NULL"
					"screenname" => "IS NOT NULL"
				)
			),
			"_custom" => "
				(`publisher_snTwitter`.`errorStatus` IS NULL OR `publisher_snTwitter`.`errorStatus` = 'NoFollower')
				AND
				(
					(
							`publisherUpdate`.`twitterStatus` = ". PublisherUpdate::ENUM_TWITTERSTATUS_DONE . "
						AND `publisherUpdate`.`twitterUpdateDate` < {$yesterday}
					) OR (
							`publisherUpdate`.`twitterStatus` IN(". PublisherUpdate::ENUM_TWITTERSTATUS_ERROR . ",". PublisherUpdate::ENUM_TWITTERSTATUS_QUEUE . ")
						AND `publisherUpdate`.`twitterUpdateDate` < {$yesterhour}
					) OR (
							`publisherUpdate`.`twitterStatus` = ". PublisherUpdate::ENUM_TWITTERSTATUS_NEW . "
					) OR (
							`publisherUpdate`.`twitterStatus` IS NULL
					)
				)
			"
		);
		/*

		$pu->populateOnce(false)->load($filter);
		if ($pu->gotValue){
			$rmq = new RabbitMQ();
			$rmq->declareQueue(RMQ_QUEUE_TWITTER);
			$ids = array();
			do {
				$msg_body = json_encode(array(
					"id" => $pu->id,
					"type" => "updateTwitter"
				));
				$rmq->publishMessage($msg_body,RMQ_QUEUE_TWITTER);

				//$pu->twitterStatus = PublisherUpdate::ENUM_TWITTERSTATUS_QUEUE;
				//$pu->twitterUpdateDate = time();
				//$pu->save();


				if ($debugLevel) Tool::echoLog("PublisherUpdate row#{$pu->id} pushed to RabbitMQ for TwitterUpdate");
			} while($pu->populate());
		$pu = new PublisherUpdate();
		$pu->update(array("id"=>$ids),array("twitterStatus"=>PublisherUpdate::ENUM_TWITTERSTATUS_QUEUE,"twitterUpdateDate"=>time()));
		} else {
			if ($debugLevel) Tool::echoLog("There's no active Twitter connected publisher");
		}
		 */
		$counter = new PublisherUpdate();
		$counter->count("id","cnt",$filter);
		$cnt = 0;

		if ($counter->cnt) {
			if ($debugLevel > 0) Tool::echoLog("Waiting calculate process count: " . $counter->cnt);
			while ($counter->cnt > 0) {
				$pu             = new PublisherUpdate();
				$pu->limitCount = 1000;
				$pu->populateOnce(false)->orderBy("id ASC")->load($filter);
				if ($pu->gotValue) {
					$rmq = new RabbitMQ();
					$rmq->declareQueue(RMQ_QUEUE_TWITTER);
					$ids = array();
					do {
						$msg_body = json_encode(array(
							"id"   => $pu->id,
							"type" => "updateTwitter"
						));
						$rmq->publishMessage($msg_body, RMQ_QUEUE_TWITTER);

						$pu->twitterStatus = PublisherUpdate::ENUM_TWITTERSTATUS_QUEUE;
						$pu->twitterUpdateDate = time();
						$pu->save();


						if ($debugLevel) Tool::echoLog("PublisherUpdate row#{$pu->id}# pushed to RabbitMQ for TwitterUpdate");
						$cnt++;
					} while ($pu->populate());
				} else {
					if ($debugLevel) Tool::echoLog("There's no active Twitter connected publisher in while");
				}
				$counter->count("id","cnt",$filter);
			}
			if ($debugLevel) Tool::echoLog("$cnt records pushed to RMQ for Twitter Update");
		} else {
			if ($debugLevel) Tool::echoLog("There's no active Twitter connected publisher");
		}
		//JPDO::commit();
	}

	/**
	 * Publisher TrueReach calculation (also available for single calculate)
	 *
	 * @param int|null $id
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function bulkCalculate($id=null){
		global $debugLevel;
		//JPDO::beginTransaction();
		$pu = new PublisherUpdate();
		$filter = array();
		if (is_null($id)){
			$filter = array(
				"publisher_publisherUpdate" => array(
					"user_publisher" => array(
						"status" => User::ENUM_STATUS_ACTIVE
					),
					"publisher_snTwitter" => array(
						"screenname" => "IS NOT NULL",
						"errorStatus" => "IS NULL"
					)
				),
				"_custom" => "
				(
						`publisherUpdate`.`calculateStatus` = ". PublisherUpdate::ENUM_CALCULATESTATUS_DONE . "
					AND `publisherUpdate`.`calculateDate` < " . strtotime('-1 day') . "
				) OR (
						`publisherUpdate`.`calculateStatus` IN(". PublisherUpdate::ENUM_CALCULATESTATUS_ERROR . ",". PublisherUpdate::ENUM_CALCULATESTATUS_QUEUE . ")
					AND `publisherUpdate`.`calculateDate` < " . strtotime('-1 hour') . "
				) OR (
						`publisherUpdate`.`calculateStatus` = ". PublisherUpdate::ENUM_CALCULATESTATUS_NEW . "
				) OR (
						`publisherUpdate`.`calculateStatus` IS NULL
				)
			"
			);
		} else {
			//Tool::echoLog("Update with id #{$id}#");
			$filter = array(
				"id"=>$id,
				"publisher_publisherUpdate" => array(
					"user_publisher" => array(
						"status" => User::ENUM_STATUS_ACTIVE
					),
					"publisher_snTwitter" => array(
						"screenname" => "IS NOT NULL",
						"errorStatus" => "IS NULL"
					)
				)
			);
		}
		/*
		$pu->populateOnce(false)->load($filter);

		if ($pu->gotValue){
			$rmq = new RabbitMQ();
			$rmq->declareQueue(RMQ_QUEUE_TWITTER);
			$ids = array();
			do {
				$msg_body = json_encode(array(
					"id" => $pu->id,
					"type" => "updateTrueReach"
				));
				$rmq->publishMessage($msg_body,RMQ_QUEUE_TWITTER);

				//$pu->calculateStatus = PublisherUpdate::ENUM_CALCULATESTATUS_QUEUE;
				//$pu->calculateDate = time();
				//$pu->save();


				if ($debugLevel) Tool::echoLog("Publisher queued to calculate #" . $pu->id);
			} while($pu->populate());
		$pu = new PublisherUpdate();
		$pu->update(array("id"=>$ids),array("calculateStatus"=>PublisherUpdate::ENUM_CALCULATESTATUS_QUEUE,"calculateDate"=>time()));
		}
		else {
			if ($debugLevel) Tool::echoLog("There's no active Twitter connected publisher to calculate");
		}
		*/
		$counter = new PublisherUpdate();
		$counter->count("id","cnt",$filter);
		$cnt = 0;

		if ($counter->cnt){
			if($debugLevel > 0) Tool::echoLog("Waiting calculate process count: " . $counter->cnt);
			while($counter->cnt > 0){
				$pu = new PublisherUpdate();
				$pu->limitCount = 1000;
				$pu->populateOnce(false)->orderBy("id ASC")->load($filter);
				if ($pu->gotValue){

					$rmq = new RabbitMQ();
					$rmq->declareQueue(RMQ_QUEUE_TWITTER);
					do {
						$msg_body = json_encode(array(
							"id" => $pu->id,
							"type" => "updateTrueReach"
						));
						$rmq->publishMessage($msg_body,RMQ_QUEUE_TWITTER);

						$pu->calculateStatus = PublisherUpdate::ENUM_CALCULATESTATUS_QUEUE;
						$pu->calculateDate = time();
						$pu->save();


						if ($debugLevel) Tool::echoLog("Publisher queued to calculate #" . $pu->id . "#");
						$cnt++;
					} while($pu->populate());
				}
				$counter->count("id","cnt",$filter);
			}
			if ($debugLevel) Tool::echoLog("$cnt records pushed to RMQ for Twitter Update");
		}
		else {
			if ($debugLevel) Tool::echoLog("There's no active Twitter connected publisher to calculate for this filter: " . json_encode($filter));
		}
		//JPDO::commit();
	}

	/**
	 * Publisher daily Twitter information single update
	 *
	 * @param int $id
	 *
	 * @return bool|string
	 *
	 * @author Murat
	 */
	public static function singleTwitterUpdateProcess($id){
		$pu = new PublisherUpdate($id);
		if ($pu->gotValue){
			$pu->loadPublisher()->loadSnTwitter();
			if (is_object($pu->publisher_publisherUpdate->publisher_snTwitter)){
				if ($pu->publisher_publisherUpdate->publisher_snTwitter->updateAccount()){
					$pu->twitterStatus = PublisherUpdate::ENUM_TWITTERSTATUS_DONE;
					$pu->twitterUpdateDate = time();
					$pu->save();
					return true;
				} else {
					$pu->twitterStatus = PublisherUpdate::ENUM_TWITTERSTATUS_ERROR;
					$pu->twitterUpdateDate = time();
					$pu->save();
					return $pu->publisher_publisherUpdate->publisher_snTwitter->updateError;
				}
			} else {
				$pu->twitterStatus = PublisherUpdate::ENUM_TWITTERSTATUS_ERROR;
				$pu->save();
				return "Twitter Account Not Found";
			}
		} else {
			return "PublisherUpdate Record Not Found";
		}
	}

	/**
	 * Publisher TrueReach single calculation
	 *
	 * @param int $id
	 *
	 * @return bool|string
	 *
	 * @author Murat
	 */
	public static function singleCalculateProcess($id){
		$pu = new PublisherUpdate($id);
		if ($pu->gotValue){
			$pu->loadPublisher()->loadSnTwitter();
			if (is_object($pu->publisher_publisherUpdate->publisher_snTwitter)){
				if ($pu->publisher_publisherUpdate->publisher_snTwitter->updateTrueReach()){
					$pu->calculateStatus = PublisherUpdate::ENUM_CALCULATESTATUS_DONE;
					$pu->calculateDate = time();
					$pu->save();
					return true;
				} else {
					$pu->calculateStatus = PublisherUpdate::ENUM_CALCULATESTATUS_ERROR;
					$pu->calculateDate = time();
					$pu->save();
					return "There was an error occured while trying to calculate request. Check the consumer logs";
				}
			} else {
				$pu->twitterStatus = PublisherUpdate::ENUM_TWITTERSTATUS_ERROR;
				$pu->save();
				return "Twitter Account Not Found";
			}
		} else {
			return "PublisherUpdate Record Not Found";
		}
	}
}

