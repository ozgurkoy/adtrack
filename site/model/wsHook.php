<?php

class wsHook {
	protected $base;

	public function __construct() {
		$this->base = v2wscore::getInstance();
	}


	/**
	 * Mail hook processor
	 *
	 * @return void
	 * @author Murat
	 */
	public function process() {
		if (isset($_POST["mandrill_events"])){
			$me = json_decode($_POST["mandrill_events"]);
			if (is_array($me) && count($me)){
				foreach ($me as $e){
					$mh = new MailHook();
					$mh->hookData = json_encode($e);
					$mh->state = MailHook::ENUM_STATE_RECEIVED;
					$mh->tryCount = 0;
					$mh->save();
				}
			}
		}
		echo "ok";
	}

	/**
	 * update publisher TrueReach and cost
	 *
	 * @return void
	 * @author Murat
	 */
	public function updatePublisherScore($updateKey) {
		if (empty($_SERVER["REMOTE_ADDR"]) || ($_SERVER['REMOTE_ADDR'] != Setting::getValue("CALCAPI_IP") && $_SERVER['REMOTE_ADDR'] != "127.0.0.1")){
			die("unauthorized");
		}
		$data = empty($_POST["data"]) ? null : json_decode($_POST["data"]);

		if (!is_object($data) && !is_array($data)){
			die("invalid data");
		} else {
			if (strlen($data->screen_name) && is_object($data->score_data)){
				$snt  = new SnTwitter();
				$snt->load(array("screenname"=>$data->screen_name));
				if ($snt->gotValue && md5($snt->id."-".$snt->twid) == $updateKey){
					$snt->updateScoreData($data->score_data);
				}
			}
			echo "ok";
		}
	}

	/**
	 * Periodic Campaign Update
	 *
	 * @return void
	 * @author Murat
	 */
	public function updateCampaignReports(){
		$data = json_decode($this->base->_requestVars->data);
		JPDO::beginTransaction();
		CampaignClickStat::bulkAddUpdateCCS($data);
		JPDO::commit();
		echo "ok";
	}

	/**
	 * Event Based Campaign Update
	 * This method update the campaign stats when the campaign has reach a limited percentage
	 *
	 * @return void
	 * @author Murat
	 */
	public function updateCampaignStats(){
		$data = json_decode($this->base->_requestVars->data);

		if (is_array($data) & isset($data[0])){
			$data = $data[0];
			JPDO::beginTransaction();
			$ccs = new CampaignClickStat();

			foreach ($data->clicks as $item){
				$ccs = new CampaignClickStat();
				$ccs->addUpdateCCS($item,$data->hash);

				$ccs->updateCampStatCounts();
			}

			$ccs->loadCampaign();

			$usedRate = 0;

			if (!is_null($ccs->campaign)){
				//if (!is_object($ccs->campaign->campaign_campaignClick)){
				$ccs->campaign->loadCampaignClick();
				//}
				//if (!is_object($ccs->campaign->campaign_campaignCPC)){
				$ccs->campaign->loadCampaign_campaignCPC();
				//}

				if ($ccs->campaign->campaign_campaignCPC->maxClick > 0){
					$usedRate = ceil(($ccs->campaign->campaign_campaignClick->calculatedClick/$ccs->campaign->campaign_campaignCPC->maxClick)*100);
				}

				if ($ccs->campaign->campaign_campaignClick->calculatedClick >= $ccs->campaign->campaign_campaignCPC->maxClick){
					$ccs->campaign->writeHistory(
						CampaignHistory::ENUM_ACTIONID_SLS_HOOK_END_CAMPAIGN,
						"Campaign end hook received from SLS",
						CampaignHistory::ENUM_MADEBY_SYSTEM,
						null
					);
					$ccs->campaign->finishCPCCampaign();
				} else {
					if ($usedRate >= 70 && $usedRate < 90 && !$ccs->campaign->campaign_campaignCPC->extendMailSent){
						$ccs->campaign->writeHistory(
							CampaignHistory::ENUM_ACTIONID_SLS_HOOK_PERCENT,
							"Campaign 70% complete ratio message received from SLS",
							CampaignHistory::ENUM_MADEBY_SYSTEM,
							null
						);
						if ($ccs->campaign->state == Campaign::ENUM_STATE_PROGRESSING) $ccs->campaign->campaign_campaignCPC->sendExtendMail();
					} elseif ($usedRate >= 90 && $usedRate < 100 && !$ccs->campaign->campaign_campaignCPC->finishingAlertSent){

						$ccs->campaign->writeHistory(
							CampaignHistory::ENUM_ACTIONID_SLS_HOOK_PERCENT,
							"Campaign 90% complete ratio message received from SLS",
							CampaignHistory::ENUM_MADEBY_SYSTEM,
							null
						);
						if ($ccs->campaign->state == Campaign::ENUM_STATE_PROGRESSING) $ccs->campaign->campaign_campaignCPC->setFinishingMailJob();
					} else {
						$ccs->campaign->writeHistory(
							CampaignHistory::ENUM_ACTIONID_SLS_HOOK_PERCENT,
							"Push action received from SLS when complete ratio: {$usedRate}%",
							CampaignHistory::ENUM_MADEBY_SYSTEM,
							null
						);
					}
				}
			}
			JPDO::commit();

		}
		echo "ok";
		unset($ccs);
	}

	/**
	 * Event Based Link Stat Update
	 * This method update the link stats for active campaigns
	 *
	 * @return void
	 * @author Murat
	 */
	public function updateLinkStats(){
		$data = json_decode($this->base->_requestVars->data);
		JPDO::beginTransaction();
		UserClickStat::updateLinkStats($data);
		JPDO::commit();
		echo "ok";
	}

	/**
	 * Update LangWord by Language System
	 *
	 * @return void
	 * @author Murat
	 */
	public function updateLangWord(){
		$data = json_decode($this->base->_requestVars->data);

		if (!empty($data->entry)){
			$entries = new stdClass();
			$entries->entry = array($data->entry);

			$langsID    = array();
			$l = new Lang();
			$l->load( array( "active" => 1 ) );
			do {
				$langsID[ $l->langShortDef ] = $l->id;
			} while ( $l->populate() );

			unset( $l );
			Lang::parseCoreResult($entries,$langsID);
			//flush jcache
			JCache::flush();

			//flush smarty cache
			JT::init();
			JT::clearAllCache();

			echo "ok";
		} else {
			echo "nok";
		}
	}
}