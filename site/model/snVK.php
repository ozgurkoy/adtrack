<?php
/**
 * snVK real class - firstly generated on 23-02-2014 21:57, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/snVK.php';
include $__DP . '/site/lib/VK/VK.php';
include $__DP . '/site/lib/VK/VKException.php';

class SnVK extends SnVK_base
{

	public $savedId=null;
	public $savedAvatar=false;
	public $vkObject = null;
	public $accessToken = null;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
		$this->vkObject = new \VK\VK( Setting::getValue( "VK_CLIENT_ID" ), Setting::getValue( "VK_SECRET" )  );
	}


	/**
	 * Add account
	 *
	 * @param array $vkToken
	 * @return bool|SnGoogle
	 */
	public function addAccount( $vkid, $info )
	{
		// check if it's a new user
		$token = $this->token;
		$atoken = $this->accessToken;

		$this->load( array( "vkid" => $vkid) );

		if ( !$this->gotValue || is_null( $this->publisher_snVK ) ) {
			$this->vkid        = $vkid;
			$this->accessToken = $atoken;
			$this->token       = $token;

			return $this->updateInfo( $info );
		}

		$this->loadPublisher();

		#this is an after fix. so that api works good.
		if(!$this->publisher_snVK || $this->publisher_snVK->gotValue === false )
			$this->publisher_snVK = null;

		return true;
	}

	/**
	 * @param $code
	 * @return bool
	 * @author Özgür Köy
	 */
	public function getAccessToken( $code ) {
		try {
			$token = $this->vkObject->getAccessToken( $code, Setting::getValue( "VK_CALLBACK_URL" ) );
		}
		catch ( \VK\VKException $J ) {
			//var_dump($J);exit;
			return false;
		}

		$this->vkid        = $token[ "user_id" ];
		$this->accessToken = $token[ "access_token" ];
		$this->token       = $code;

		return true;
	}


	/**
	 * @param $vkId
	 * @param $info
	 * @return bool|SnGoogle
	 * @author Özgür Köy
	 */
	public function login( $vkId, $info )
	{
		$token = $this->token;
		$atoken = $this->accessToken;

		$this->load( array(   "_custom"=>"`vkid`= '$vkId' AND `publisher_snVK` IS NOT NULL"));
		#acc.token does not expire
		if ( $this->gotValue ) {
			$this->accessToken = $atoken;
			$this->token       = $token;

			$this->loadPublisher()
				->writeHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "Login by VK", UserHistory::ENUM_MADEBY_USER )
				->loadUser()
				->login( null, null, $this->publisher_snVK->user_publisher->id );

			return $this->updateInfo( $info, $vkId );
		}
		else{
			//New User
			$this->load( array( "vkid" => $vkId ) );

			$this->accessToken = $atoken;
			$this->token       = $token;

			$this->updateInfo( $info,$vkId );
			return false;
		}

	}

	/**
	 * @param null $vkid
	 * @return bool
	 * @author Özgür Köy
	 */
	public function getInfo( $vkid = null ) {

		#does not require permission
		$user = $this->vkObject->api( "users.get",
			array(
				'uids'   => is_null( $vkid ) ? $this->vkid : $vkid,
				'fields' => "sex,bdate,first_name,last_name,city,country,photo_400_orig,screen_name"
			)
		);

		if ( is_array( $user ) && isset( $user[ "response" ] ) && isset( $user[ "response" ][ 0 ] ) ) {
			return $user[ "response" ][ 0 ];
		}
		else {
			JLog::log( "social", "VK Problem : can't fetch info id:" . $this->vkid );

			return false;
		}
	}

	/**
	 * @param array $info
	 * @return SnVK
	 */
	public function updateInfo( $info, $vkid=null ) {
		$this->name       = isset( $info[ "first_name" ] ) ? $info[ "first_name" ]. " ". $info[ "last_name" ] : "";
		$this->avatar     = isset( $info[ "photo_400_orig" ] ) ? $info[ "photo_400_orig" ] : "";
		$this->sex        = isset( $info[ "sex" ] ) ? $info[ "sex" ] : "";
		$this->birthDate  = isset( $info[ "bdate" ] ) ? $info[ "bdate" ] : "";
		$this->city       = isset( $info[ "city" ] ) ? intval( $info[ "city" ] ) : null;
		$this->country    = isset( $info[ "country" ] ) ? intval( $info[ "country" ] ) : null;
		$this->screenName = isset( $info[ "screen_name" ] ) ? $info[ "screen_name" ] : "";;
		if(!is_null($vkid))
			$this->vkid = $vkid;

		$r                 = $this->save();
		$this->savedId     = $this->id;
		$this->savedAvatar = $this->avatar;

		return $r;
	}

	public function getLoginUrl() {
		return "https://oauth.vk.com/authorize?client_id=".Setting::getValue("VK_CLIENT_ID")."&scope=".Setting::getValue("VK_PERMISSIONS")."&redirect_uri=".Setting::getValue("VK_CALLBACK_URL")."&response_type=code";
	}

	public function checkLoginInfo( $userId,$accessToken )
	{
		$this->vkObject->access_token = $accessToken;

		if ($this->vkObject->checkAccessToken()){
			$this->load(array(
				"vkid" => $userId,
			));
			if ( $this->gotValue ) {
				$this->accessToken = $accessToken;
				$this->save();
				return true;
			} else {
				return false;
			}

		}
		else {
			JLog::log( "social", "Google Problem : can't fetch info ." . $this->token );

			return false;
		}
	}

}

