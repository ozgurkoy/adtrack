<?php
	/**
	* bank real class for Jeelet - firstly generated on 28-11-2011 11:36, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/bank.php");

	class Bank extends Bank_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * Print The Bank as Html
		 * @author Sam
		 * @return string
		 */
		public function printHTML() {

			$HTML = "Bank: ".$this->bankName;
			// If Bank Have more fields like Code or Other Local Issues
			return $HTML;

		}

		/**
		 * load all active banks
		 *
		 * @return Bank
		 * @author Özgür Köy
		 **/
		public static function loadAll()
		{
			$t = new Bank();
			$t->orderBy( "bankName ASC" )->populateOnce( false )->load();
			return $t;
		}

	}

	?>