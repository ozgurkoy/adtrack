<?php
/**
 * userEmail real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/userEmail.php';

class UserEmail extends UserEmail_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * @param      $email
	 * @param bool $setDefault
	 * @return UserEmail
	 * @author Özgür Köy
	 */
	public static function newEmail( $email, $setDefault = false ) {
		$userEmail                = new userEmail();
		$userEmail->email         = $email;
		$userEmail->confirmedDate = null;
		$userEmail->markedAsApam  = 0;
		$userEmail->deleted       = 0;
		$userEmail->isDefault     = $setDefault;
		$userEmail->validate      = $setDefault ? JUtil::randomString( 20 ) : "";

		$userEmail->save();

		return $userEmail;
	}

	/**
	 * Save User email on sign up
	 * @param $email
	 * @return UserEmail
	 * @author Sam
	 * @author Murat (modified)
	 */
	public function updateUserEmail($email)
	{
		$this->loadUser();
		if(!Validate::checkUniqueEmail($email, $this->user_userEmail))
		{
			// exist
			return false;
		} else {
			$this->email = $email;
			$this->confirmedDate=null;
			$this->markedAsApam=0;
			$this->deleted=0;
			$this->isDefault = 1;
			$this->save();

			return true;
		}
	}

	/**
	 * Send a notification to user about spam mail
	 *
	 * @return bool
	 *
	 * @author Murat
	 * @author Ozgur Koy
	 */
	public function informUserSpamMail() {
		$this->loadUser();

		if ( !is_object( $this->user_userEmail->lang ) )
			$this->user_userEmail->loadLang();

		$lang = $this->user_userEmail->lang;

		$message = LangMessage::getLangMessages( $lang->langShortDef, "spamMailInformation" );
		if ( $message === false )
			return false;

		$informTitle = $message[ "title" ];
		$userName    = $this->user_userEmail->getDisplayName();

		// generate the message
		$messageVariables = array( "user" => $userName );

		UserMessage::create( $this->user_userEmail->id, array(), $messageVariables, $message[ "lastHistoryId" ] );

		$this->spamInformed = 1;
		$this->save();

		unset( $h, $informMessage, $informTitle, $informMes, $_sysMessages, $messageVariables, $msgTemp );

		return true;
	}

	/**
	 * Mark the mail address as invalid (can called only by Mail model)
	 *
	 * @param string $email //Email of the user
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function markAsInvalid($email){
		$um = new UserEmail();
		$um->load(array("email"=>$email));
		if ($um->gotValue){
			$um->invalid = 1;
			$um->save();
		}
	}

	/**
	 * Mark the mail address as spam
	 *
	 * @param string $email //Email of the user
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function markAsSpam($email){
		$um = new UserEmail();
		$um->load(array("email"=>$email));
		if ($um->gotValue){
			if ( $um->spamInformed != 1 ) {
				$um->informUserSpamMail();
			}
			$um->markedAsApam = 1;
			$um->save();
		}
	}

	/**
	 * Mark the mail address as unsubscribed
	 *
	 * @param string $email //Email of the user
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function markAsUnsubscribed($email){
		$um = new UserEmail();
		$um->load(array("email"=>$email));
		if ($um->gotValue){
			if ($um->unsubscribed != 1){
				//$um->user_userEmail->sendSysMessage("V2_unsubscribeFromEmail",null,null,null,$um); // I disabled this for Özgür's made publisher notification system.
				$um->unsubscribed = 1;
				$um->save();

				$um->loadUser();
			}
		}
	}

	/**
	 * Unsubscribe from mail footer
	 *
	 * @param string $email
	 * @return bool
	 * @author Murat
	 */
	public static function unsubscribeFromMail($email) {
		JPDO::beginTransaction();
		try{
			$um = new UserEmail();
			$um->with("user_userEmail")->load(array(
				"email" => $email
			));

			if ($um->gotValue){
				$um->unsubscribed = 1;
				$um->save();

				if ($um->user_userEmail->gotValue){
					$um->user_userEmail->sendSysMessage(
						"V2_unsubscribeMail",
						array(),
						array(
							"name" => $um->user_userEmail->getDisplayName(),
							"profileLink" => JUtil::siteAddress() . "profile"
						),
						null,
						$um,
						true
					);
				}
			}

			JPDO::commit();
		}
		catch(JError $j){
			JPDO::rollback();

			return false;
		}

		return true;
	}
}

