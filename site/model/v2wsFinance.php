<?php

use Swagger\Annotations\Operation;
use Swagger\Annotations\Operations;
use Swagger\Annotations\Parameter;
use Swagger\Annotations\Parameters;
use Swagger\Annotations\Api;
use Swagger\Annotations\ErrorResponse;
use Swagger\Annotations\ErrorResponses;
use Swagger\Annotations\Resource;
use Swagger\Annotations\AllowableValues;

use Swagger\Annotations\Properties;
use Swagger\Annotations\Property;
use Swagger\Annotations\Model;
use Swagger\Annotations\Items;



/**
 * @package
 * @category
 * @subpackage
 *
 * @Resource(
 *  apiVersion="0.1",
 *  swaggerVersion="1.1",
 *  basePath="http://mars.admingle.com/wsv2/finance",
 *  resourcePath="/finance"
 * )
 *
 */
include($__DP."/site/model/wsObjects.php");
class v2wsFinance
{
	protected $base;

	function __construct() {
		$this->base = v2wscore::getInstance();
	}

	/**
	 * Summary page that shows currentBalance and pending balance
	 * @Api(
	 *            path="/summary",
	 *            description="finance summary,  summary page that shows currentBalance and pending balance",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Summary of publisher finance actions",
	 *            responseClass="FinanceSummaryResponse",
	 *            nickname="FinanceSummary",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function summary() {
		$this->base->tokenCheck();

		$response  = new FinanceSummaryResponse();
		$publisher = $this->base->_publisher;

		$response->currentTotal = $publisher->recalculateWithdrawableBalance();
		$response->pendingWithdrawal = $publisher->calculatePendingBalance();


		$this->base->sendResponse(200, $response->getJsonData() );
	}

	/**
	 * List of Publisher's money transfer options
	 * @Api(
	 *            path="/publisherWithdrawOptions",
	 *            description="List of Publisher's money transfer options",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Get All Publisher Withdraw Options Data",
	 *            responseClass="PublisherPaymentOptionsResponse",
	 *            nickname="GetPWOptions",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="params",
	 *           description="lastUpdateTimestamp",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="No withdraw option"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function publisherPaymentOptions() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
			$publisher = $this->base->_publisher;
//			$publisher = new Publisher();

			$withdrawableBalance = $publisher->recalculateWithdrawableBalance();

			$singleValueReq = new SingleValueReq( $this->base->_request->json );

			$timestamp = $singleValueReq->value;
			$filter    = array( "deleted" => '0' );
			if ( $timestamp != null ) {
				$filter[ "jdate" ] = '&gt ' . $timestamp;
			}

			$options = $publisher->loadPublisher_publisherPaymentOption( $filter );

			$response = new PublisherPaymentOptionsResponse();

			if ( $options->gotValue ) {
				$response->timestamp                = time();
				$response->publisherWithdrawOptions = array();

				do {
					$options->loadPublisherWithdrawOption();

					$t0                     = new PublisherPaymentOptionResponse();
					$t0->id                 = $options->id;
					$t0->displayName        = $options->displayName;
					$t0->WithdrawOptionID   = $options->publisherWithdrawOption->id;
					$t0->canWithdraw        = ValidateFinance::publisherCanWithDraw( $withdrawableBalance, $options->publisherWithdrawOption->minWithdrawAmount );
					$t0->canWithDrawOnlyMax = (bool)$options->publisherWithdrawOption->canWithdrawOnlyMax;
					$t0->maxWithdrawAmount = $options->publisherWithdrawOption->maxWithdrawAmount;
					$t0->minWithdrawAmount = $options->publisherWithdrawOption->minWithdrawAmount;

					$response->publisherWithdrawOptions[ ] = $t0;

					unset( $t0 );
				} while ( $options->populate() );

			}

			$this->base->sendResponse(200, $response->getJsonData() );
		}
	}

	/**
	 * List of Publisher's money actions
	 * @Api(
	 *            path="/publisherMoneyActions",
	 *            description=" List of Publisher's money actions",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="List money actions",
	 *            responseClass="MoneyActionsResponse",
	 *            nickname="MoneyActions",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="params",
	 *           description="MoneyActionsRequest",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="MoneyActionsRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="No withdraw option"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function publisherMoneyActions() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
			$publisher = $this->base->_publisher;
//			$publisher = new Publisher();
			$req     = new MoneyActionsRequest( $this->base->_request->json );
			$perPage = Setting::getValue( "campListItemCount" );
			$page    = (int)$req->page;
			$page    = $page - 1;

			list( $totalCount, $actions ) = $publisher->loadMoneyActions( $page, $perPage );

			$response               = new MoneyActionsResponse();
			$response->currentPage  = (int)$req->page;
			$response->pageCount    = floor( $totalCount / $perPage ) + ( ( $totalCount % $perPage ) > 0 ? 1 : 0 );
			$response->recordCount  = $totalCount;
			$response->moneyActions = $actions;

			unset( $actions );

			$this->base->sendResponse( 200, $response->getJsonData() );
		}
	}

	/**
	 * List of Publisher's money actions
	 * @Api(
	 *            path="/publisherPaymentOptionAccount",
	 *            description=" Get  Publisher's money Account info",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Publisher money account info",
	 *            responseClass="PublisherPaymentOptionActionRequest",
	 *            nickname="publisherPaymentOptionAccount",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="params",
	 *           description="MoneyAccountID",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="No withdraw option"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function PublisherPaymentOptionAccount() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
			$publisher = $this->base->_publisher;

			$singleValueReq = new SingleValueReq( $this->base->_request->json );
			$AcountID         = array( "id" => $singleValueReq->value, "pid" => $this->base->_publisher->id );


			$publisher = new Publisher();

			$m = new PublisherPaymentOption();
			$m->load(array("publisher_publisherPaymentOption"=>$publisher->id,"id"=>$singleValueReq->value));

			$response = new PublisherPaymentOptionActionRequest();

			if ( $m->gotValue ) {

				$response->id = $m->id;
				if($m->bank)
				{
					$m->loadBank();
					$response->bank = $m->bank->id;
				}

				$response->details = $m->details;
				$response->displayName = $m->displayName;
				$response->publisherWithdrawOptionId = $m->publisherWithdrawOption;

				if($m->publisherBilling)
				{
					$response->billingInfo = new PublisherBillingInfo();

					$b = $m->loadPublisherBilling();
					$response->billingInfo->billingName = $b->billingName;
					$response->billingInfo->taxOffice = $b->taxOffice;
					$response->billingInfo->taxNumber =  $b->taxNumber;
					$response->billingInfo->address = $b->address;

				}

			}

			unset( $m );

			$this->base->sendResponse(200, $response->getJsonData() );
		}
	}

	/**
	 * Publisher's money action detail
	 * @Api(
	 *            path="/publisherMoneyActionDetail",
	 *            description="Publisher's money action detail",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Show money action detail",
	 *            responseClass="MoneyActionDetail",
	 *            nickname="MoneyActionDetailResponse",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="params",
	 *           description="id of the detail",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="No withdraw option"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function publisherMoneyActionDetail() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
			$singleValueReq = new SingleValueReq( $this->base->_request->json );
			$params         = array( "id" => $singleValueReq->value, "pid" => $this->base->_publisher->id );
			$r              = new MoneyActionDetailResponse();

			$vr = ValidateFinance::validateMoneyActionDetail( $params );

			if ( $vr->valid ) {
				$m = new PublisherMoneyAction( $singleValueReq->value );

				//load stuff
				$m->loadPaymentOption();
				$m->paymentOption->loadPublisherBilling();
				$m->paymentOption->loadPublisherWithdrawOption();
				$m->paymentOption->loadBank();

				//basic info
				$r->unixTime    = $m->lastUpdate;
				$r->id          = $m->id;
				$r->displayName = $m->paymentOption->displayName;
				$r->amount      = $m->amount;
				$r->state       = $m->status;

				$t0 = array();

				//billing info
				if ( $m->paymentOption->publisherBilling && $m->paymentOption->publisherBilling->gotValue ) {
					$r->billing              = new PublisherBillingInfo();
					$r->billing->address     = $m->paymentOption->publisherBilling->address;
					$r->billing->taxNumber   = $m->paymentOption->publisherBilling->taxNumber;
					$r->billing->taxOffice   = $m->paymentOption->publisherBilling->taxOffice;
					$r->billing->billingName = $m->paymentOption->publisherBilling->billingName;
				}

				//bank info
				if ( $m->paymentOption->bank && $m->paymentOption->bank->gotValue )
					$t0[ ] = $m->paymentOption->bank->bankName;

				//account details

				$fields = json_decode( $m->paymentOption->publisherWithdrawOption->fields, true );
				$values = json_decode( $m->paymentOption->details, true );
				foreach ( $fields as $field => $fieldType ) {
					if ( array_key_exists( $field, $values ) )
						$t0[ ] = $field . ":" . $values[ $field ];
				}
				$r->details = implode( ",", $t0 );

				unset( $m );
			}
			else {
				$r->error = new ErrResponse( $vr->errorCode, $vr->errorDetail );
			}


			$this->base->sendResponse(200, $response->getJsonData() );
		}
	}

	/**
	 * Add/Edit Publisher's withdraw option
	 * @Api(
	 *            path="/publisherPaymentOptionAction",
	 *            description="Add/Edit(with id) publisher withdraw option",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Add/Edit publisher's withdraw option",
	 *            responseClass="PublisherPaymentOptionActionResponse",
	 *            nickname="publisherPaymentOptionAction",
	 * @parameters(
	 * @parameter(
	 *          name="token",
	 *          description="User Token",
	 *          paramType="header",
	 *          required="true",
	 *          allowMultiple="false",
	 *          dataType="string"
	 *         ),
	 * @parameter(
	 *          name="PublisherPaymentOptionActionRequest",
	 *          description="Data related to option action",
	 *          paramType="body",
	 *          required="true",
	 *          allowMultiple="false",
	 *          dataType="PublisherPaymentOptionActionRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="Bad request"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function publisherPaymentOptionAction() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
//			$p = $this->base->_publisher;
//			$p = new Publisher();
			$response = new PublisherPaymentOptionActionResponse();

			$req             = new PublisherPaymentOptionActionRequest( $this->base->_request->json );
			$params          = $req->getArrayData();
			$params[ "pid" ] = $this->base->_publisher->id;

			$vr = ValidateFinance::publisherPaymentOptionAction( $params );


			if ( $vr->valid ) {
				if ( empty( $vr->returnParams[ "p" ] ) ) {
					$p             = new PublisherPaymentOption();
					$finalResponse = $p->add( $params, $this->base->_publisher );
				}
				else {
					$p             = $vr->returnParams[ "p" ];
					$finalResponse = $p->edit( $params );
				}


				if ( $finalResponse ) {
					$response->value = "ok";
				}
				else {
					$err             = new ErrResponse( 500, "Internal Server Error" );
					$err->errorText  = $response->value;
					$response->error = $err;
				}
			}
			else {
				$response->error = new ErrResponse( $vr->errorCode, $vr->errorDetail );
			}

			$this->base->sendResponse(200, $response->getJsonData() );
		}

	}

	/**
	 * Delete publisher's withdraw option
	 * @Api(
     *            path="/publisherPaymentOptionDelete",
	 *            description="Delete publisher's withdraw option",
	 *              @operations(
	 *                   @operation(
	 *                              httpMethod="POST",
	 *                              summary="Delete publisher's withdraw option",
	 *                              responseClass="PublisherPaymentOptionActionResponse",
	 *                              nickname="PWOptionDelete",
	 *                   @parameters(
	 *                        @parameter(
	 *                                 name="token",
	 *                                 description="User Token",
	 *                                 paramType="header",
	 *                                 required="true",
	 *                                 allowMultiple="false",
	 *                                 dataType="string"
	 *                                ),
	 *                        @parameter(
	 *                                 name="id of the option",
	 *                                 description="id of the option",
	 *                                 paramType="body",
	 *                                 required="true",
	 *                                 allowMultiple="false",
	 *                                 dataType="SingleValueReq"
	 *                                )
	 *                         ),
	 *                   @errorResponses(
	 *                        @errorResponse(
	 *                                   code="403",
	 *                                   reason="Bad token"
	 *                                 ),
	 *                        @errorResponse(
	 *                                   code="404",
	 *                                   reason="Bad request"
	 *                                 )
	 *                    )
     *              )
	 *   )
	 * )
	 */
	public function publisherPaymentOptionDelete() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
//			$p = new Publisher();
			$response = new SingleValueReq( $this->base->_request->json );
			$params   = array( "id" => $response->value, "pid" => $this->base->_publisher->id, "checkOwnerShipOnly" => true );

			$vr = ValidateFinance::publisherPaymentOptionAction( $params );


			if ( $vr->valid ) {
				$p          = $vr->returnParams[ "p" ];
				$p->deleted = 1;

				if ( $p->save() !== false ) {
					$response->value = "ok";
				}
				else {
					$err             = new ErrResponse( 500, "Internal Server Error" );
					$err->errorText  = $response->value;
					$response->error = $err;
				}
			}
			else {
				$response->error = new ErrResponse( $vr->errorCode, $vr->errorDetail );
			}

			$this->base->sendResponse( 200, $response->getJsonData() );
		}

	}

	/**
	 * List available admingle ways to withdraw Money
	 * @Api(
	 *            path="/withdrawOptions",
	 *            description="List available admingle ways to withdraw Money",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="List available admingle ways to withdraw Money",
	 *            responseClass="WithdrawOptionsResponse",
	 *            nickname="GetMessages",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="params",
	 *           description="lastUpdateTimestamp",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="no payment Option"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function withdrawOptions() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
			$response                  = new WithdrawOptionsResponse();
			$response->timestamp       = time();
			$response->withdrawOptions = array();

			$pwo = new PublisherWithdrawOption();


			$singleValueReq = new SingleValueReq( $this->base->_request->json );

			$timestamp = $singleValueReq->value;
			$filter    = array( "active" => 1 );
			if ( $timestamp != null ) {
				$filter[ "jdate" ] = '&gt ' . $timestamp;
			}

			$pwo->load( $filter );

			do {
				if( $pwo->name == "product" )
					continue;
				$res1           = new WithdrawOptionResponse();
				$res1->id       = $pwo->id;
				$res1->nameCode = $pwo->name;
				$res1->fields   = $pwo->fields;
				$res1->type     = $pwo->type;
				$res1->message  = $pwo->message;

				$response->withdrawOptions[ ] = $res1;
			} while ( $pwo->populate() );

			#add the brands
			$s = new StoreBrand();
			$s->listsActives();
			while($s->populate()){
				$res1           = new WithdrawOptionResponse();
				$res1->id       = $s ->id;
				$res1->nameCode = $s->name;
				$res1->fields   = null;
				$res1->type     = 'coupon';
				$res1->message  = $s->description;

				$response->withdrawOptions[ ] = $res1;

			}

			$this->base->sendResponse( 200, $response->getJsonData() );
		}
	}

	/**
	 * Request Withdrawal
	 * @Api(
	 *            path="/publisherRequestWithdrawal",
	 *            description="Request Withdrawal",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Request Withdrawal",
	 *            responseClass="PublisherWithdrawalResponse",
	 *            nickname="publisherRequestWithdrawal",
	 * @parameters(
	 * @parameter(
	 *          name="token",
	 *          description="User Token",
	 *          paramType="header",
	 *          required="true",
	 *          allowMultiple="false",
	 *          dataType="string"
	 *         ),
	 * @parameter(
	 *          name="PublisherWithdrawalRequest",
	 *          description="Request Withdrawal parameters",
	 *          paramType="body",
	 *          required="true",
	 *          allowMultiple="false",
	 *          dataType="PublisherWithdrawalRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="Bad request"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function publisherRequestWithdrawal() {
		$this->base->tokenCheck();

		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
//			$p = $this->base->_publisher;
			$response = new PublisherWithdrawalResponse();

			$publisher = $this->base->_publisher;
//			$publisher = new Publisher();

			$req             = new PublisherWithdrawalRequest( $this->base->_request->json );
			$params          = $req->getArrayData();
			$params[ "pid" ] = $this->base->_publisher->id;


			$vr = ValidateFinance::validateWithdrawalRequest( $params );
			if ( (is_bool($vr) && $vr) || (is_object($vr) && $vr->valid ) ) {

				if ( PublisherMoneyAction::addNewMoneyAction($publisher,$params) ) {
					$response->value = "ok";
				}
				else{
					$err             = new ErrResponse( 500, "Internal Server Error" );
					$err->errorText  = $response->value;
					$response->error = $err;
				}
			}
			else {
				$response->error = new ErrResponse( $vr->errorCode, $vr->errorDetail );
			}

			$this->base->sendResponse( 200, $response->getJsonData() );
		}

	}


	/**
	 * List of publisher coupons
	 *
	 * Get Start Messages , filter (read , unread),
	 * @return void
	 * @author Murat
	 *
	 *  @Api(
	 *   path="/getPublisherCoupon",
	 *   description="Get All Publisher coupon department codes",
	 *   @operations(
	 *     @operation(
	 *       httpMethod="POST",
	 *       summary="Coupon department codes",
	 *       responseClass="CouponDepartmentsResponse",
	 *       nickname="getPublisherCoupon",
	 *       @parameters(
	 *         @parameter(
	 *          name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 *         @parameter(
	 *          name="WithdrawOption",
	 *           description="Withdraw Option ID",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 *       @errorResponses(
	 *          @errorResponse(
	 *            code="88",
	 *           reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function getPublisherCoupon() {
		$this->base->tokenCheck();

		$wdo = new SingleValueReq( $this->base->_request->json );


		$dep = new StoreBrand();
		$dep->load( array( "id" => $wdo->value, "active" => 1 ) );

		$cdr              = new CouponDepartmentsResponse();
		$cdr->Departments = array();

		if ( $dep->gotValue ) {
			do {
				$tmp                 = new CouponDepartmentResponse();
				$tmp->DisplayName    = $dep->name;
				$tmp->id             = $dep->id;
				$cdr->Departments[ ] = $tmp;

			} while ( $dep->populate() );
		}

		$this->base->sendResponse( 200, $cdr->getJsonData() );
	}

	/**
	 * List of publisher campaigns
	 *
	 * Get Start Messages , filter (read , unread),
	 * @return void
	 * @author Murat
	 *
	 *  @Api(
	 *   path="/getPublisherDepartmentCoupon",
	 *   description="Get All Publisher coupon for department ",
	 *   @operations(
	 *     @operation(
	 *       httpMethod="POST",
	 *       summary="Coupon codes per department",
	 *       responseClass="CouponsResponse",
	 *       nickname="getPublisherDepartmentCoupon",
	 *       @parameters(
	 *         @parameter(
	 *          name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 *         @parameter(
	 *          name="department ID",
	 *           description="department ID",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 *       @errorResponses(
	 *          @errorResponse(
	 *            code="88",
	 *           reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function getPublisherDepartmentCoupon() {
		$this->base->tokenCheck();

		$dprt = new SingleValueReq( $this->base->_request->json );

		$brand = new StoreBrand( $dprt->value );
		$coup  = $brand->getCategories();

		$coupns          = new CouponsResponse();
		$coupns->coupons = array();

		if ( $coup->gotValue ) {
			do {
				if ( $coup->hasChildren() !== true )
					continue;

				$tmp                = new CouponResponse();
				$tmp->title         = $coup->name;
				$tmp->id            = $coup->id;
				$tmp->description   = $coup->description;
				$tmp->imageUrl      = $coup->image;
				$tmp->price         = $coup->price;
				$tmp->formattedPrice= Format::getFormattedCurrency( $coup->price );

				$coupns->coupons[ ] = $tmp;

			} while ( $coup->populate() );
		}
		$this->base->sendResponse( 200, $coupns->getJsonData() );
	}

	/**
	 * List of publisher campaigns
	 *
	 * Get Start Messages , filter (read , unread),
	 * @return void
	 * @author Murat
	 *
	 *  @Api(
	 *   path="/buyCoupon",
	 *   description="Gut A coupon Code",
	 *   @operations(
	 *     @operation(
	 *       httpMethod="POST",
	 *       summary="Buy Coupon  codes",
	 *       responseClass="BuyCouponResponse",
	 *       nickname="BuyCoupon",
	 *       @parameters(
	 *         @parameter(
	 *          name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 *         @parameter(
	 *          name="WithdrawOption",
	 *           description="Withdraw Option ID",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 *       @errorResponses(
	 *          @errorResponse(
	 *            code="88",
	 *           reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function buyCoupon() {
		$this->base->tokenCheck();

		$p          = $this->base->_publisher;
		$wdo        = new SingleValueReq( $this->base->_request->json );
		$couponCode = $wdo->value;

		$coupon = StoreCategory::buyProduct( $couponCode, $p);

		$res             = new BuyCouponResponse();
		$res->couponCode = $coupon;

		if ( $coupon === false ) {
			$err            = new ErrResponse( 500, "Internal Server Error" );
			$err->errorText = null;
			$res->error     = $err;
		}
		elseif ( $coupon == PublisherMoneyAction::NOT_ENOUGH_FUNDS ) {
			$err            = new ErrResponse( 500, "Internal Server Error" );
			$err->errorText = PublisherMoneyAction::NOT_ENOUGH_FUNDS;
			$res->error     = $err;
		}
		else {
			$res->couponCode = $coupon;
		}

		$this->base->sendResponse( 200, $res->getJsonData() );
	}


}