<?php
	/**
	* user  class
	* Base for any user type
	*
	* @package jeelet
	* @author Özgür Köy
	* @author tiger
	**/
	include($__DP."/site/model/base/user.php");

	class User extends User_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * Load user preferred language
		 *
		 * @return void
		 * @author Murat
		 */
		public function loadLangPreference($lang = null){
			if (is_null($lang)){
				$this->loadLang();
				if (is_object($this->lang)){
					$_SESSION[ 'adLang' ] = $this->lang->langShortDef;
					$_SESSION[ 'adLangRTL'] = $this->lang->RTL;
				} else {
					$lang = new Lang();
					$lang->loadByShort(Setting::getValue("DEFAULT_LANGUAGE"));
					$_SESSION[ 'adLang' ] = $lang->langShortDef;
					$_SESSION[ 'adLangRTL'] = $this->lang->RTL;
				}
			} else {
				$_SESSION[ 'adLang' ] = $lang;
			}
		}

		/**
		 * @param null $lang
		 * @author Özgür Köy - modified
		 */
		public function fillSession( $lang = null, $extras = array() ) {

			$this->getDefaultEmail();
			$this->loadLangPreference($lang);

			if ( strlen( $this->invCode ) > 0 ) {
				$_SESSION[ 'ivcode' ] = $this->invCode;
			}

			$_SESSION[ 'adLogged' ]     = 1;
			$_SESSION[ 'adId' ]         = $this->id;
			$_SESSION[ 'adUserStatus' ] = $this->status;
			$_SESSION[ 'adUName' ]      = $this->username;
			$_SESSION[ 'adEmail' ]      = $this->user_userEmail->email;
			$_SESSION[ 'adType' ]       = $this->utype;

			foreach ( $extras as $v0 => $v1 ) {
				$_SESSION[ $v0 ] = $v1;
			}


			if ( $this->utype == User::ENUM_UTYPE_PUBLISHER ) {
				$this->loadPublisher();
				$_SESSION[ 'adName' ] = $this->user_publisher->getFullName();
				$_SESSION[ 'adPublisherId' ] = $this->user_publisher->id;
				$_SESSION[ 'adPublisherImage'] = $this->user_publisher->profileImage;

				if ( $this->user_publisher->hasTwitter && !isset( $_SESSION[ "fbimg" ] ) ) {
					$ti = $this->user_publisher->loadSnTwitter();
					if ( is_object( $ti ) && $ti->gotValue ) {
						$_SESSION[ 'adScreenname' ] = $ti->screenname;
						$_SESSION[ 'timg' ]         = $ti->timg;
					}
					else {
						$_SESSION[ 'adScreenname' ] = $this->user_publisher->name;
						unset( $_SESSION[ 'timg' ] );
					}
					unset( $ti );
				}
				elseif ( $this->user_publisher->hasFacebook ) {
					$uf = $this->user_publisher->loadPublisher_snFacebook();
					if ( is_object( $uf ) && $uf->gotValue ) {
						$_SESSION[ 'adScreenname' ] = $uf->name;
					}
					else {
						$_SESSION[ 'adScreenname' ] = $this->user_publisher->name;
					}
					if ( isset( $_SESSION[ "timg" ] ) ) unset( $_SESSION[ 'timg' ] );
				}
				else {
					switch ( $this->utype ) {
						case User::ENUM_UTYPE_PUBLISHER:
							$_SESSION[ 'adScreenname' ] = $this->user_publisher->name;
							break;
						case User::ENUM_UTYPE_ADVERTISER:
							$_SESSION[ 'adScreenname' ] = $this->user_advertiser->companyName;
							break;
					}

					if ( isset( $_SESSION[ "timg" ] ) ) unset( $_SESSION[ 'timg' ] );
					if ( isset( $_SESSION[ "fbimg" ] ) ) unset( $_SESSION[ 'fbimg' ] );
				}
			}
			elseif ( $this->utype == User::ENUM_UTYPE_ADVERTISER ) {
				$this->loadAdvertiser();
				$_SESSION[ 'adAdvertiserId' ] = $this->user_advertiser->id;
				$_SESSION[ 'adName' ] = $this->user_advertiser->companyName;
			}
			else {
				die( 'naaa' );
				$_SESSION[ 'adScreenname' ] = null;
			}

			#$this->saveHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "user Login", UserHistory::ENUM_MADEBY_USER );
		}

		/**
		 * login user
		 * forceTwitterLogin -> autologin
		 * force User make auto login without checking user name and password
		 * @return bool
		 * @author Özgür Köy - modified
		 **/
		public function login($username=null,$password=null,$forceUser=null,$fromAPI=false)
		{
			// check if the user name is an email
			$tryEmail = Validate::validateEmail($username);
			JPDO::connect( JCache::read('JDSN.'.$this->dsn) );

			// Chcek if the user is already connected
			if(!$fromAPI && isset($_SESSION['adId'])) {
				return true;
			}

			if ( is_null( $forceUser ) ) {
				if ( Validate::checkPassword( $password ) ) {
					$this->with( "user_userEmail" );
					$password = md5( $password );

					if ( $tryEmail ) {
						// load by email and password
						$this->load( array(
							"user_userEmail" => array( "email" => $username ),
							"pass"           => $password
						) );
					}
					else {
						if ( Validate::checkUsername( $username ) ) {
							$this->load(
								array(
									"_custom" => '
									lower("username")=lower(' . JPDO::quote( $username ) . ')
									AND pass=\'' . $password . '\'
									AND status IN ('.User::ENUM_STATUS_SIGNUP_INCOMPLETE.',' . User::ENUM_STATUS_NOT_CONFIRMED . ',' . User::ENUM_STATUS_ACTIVE . ')
									'
								)
							);
						}
						else {
							return false;
						}
					}
				}
				else {
					return false;
				}

			}
			else {
				$this->load(
					array(
						"id"     => $forceUser,
						"status" => array( User::ENUM_STATUS_NOT_CONFIRMED, User::ENUM_STATUS_ACTIVE )
					)

				);
			}

			// todo: for api testing
			if(
				$this->gotValue &&
				$this->status != User::ENUM_STATUS_DELETED &&
				$this->status != User::ENUM_STATUS_DEACTIVATED &&
				$this->status != User::ENUM_STATUS_BLOCKED
			) {

				if( is_null($this->lastLogin) )
					$_SESSION[ "adNewUser" ] = true;


				//if($this->gotValue && $this->status == User::ENUM_STATUS_ACTIVE) {
				if(!$fromAPI)
				{
					$this->fillSession();

					if($_SESSION['adType'] == 'a') {
						if ( defined( "ENABLE_ADVERTISER_LOGIN" ) !== true ){
							return false;
						}

						$this->lastLogin = time();
						$this->save();
					}
				}

				// Save History
				$this->lastLogin  =time();
				$this->save();

				$this->saveHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "user login", UserHistory::ENUM_MADEBY_USER );
				return true;
			} else {
				return false;
			}
		}


		/**
		 * @param $userType - publisher / advertiser / visitor
		 * @param $userName - valid user name
		 * @param $password - valid password
		 * @param $lang - user language - int
		 *
		 * return UserID
		 */
		public static function signUp( $userType, $userData, $lang = null, $fromMobile = false, $fromMB = false, $mbUserID = null ) {
			global $_S;
			JPDO::beginTransaction();
			$user           = new User();
			if ($fromMB){
				$user->load($mbUserID);
			}
			$user->username = $userData[ "username" ];
			$user->utype    = $userType;
			$user->status   = self::ENUM_STATUS_SIGNUP_INCOMPLETE;
			$user->fromMobile = $fromMobile;

			if(isset($userData["password"]))
				$user->pass     = md5( $userData[ "password" ] );


			$user->save();
			$l = new Lang();
			$user->saveLang( is_null( $lang ) ? Lang::getDefaultLanguage() : $l->loadByShortDef($lang) );
			unset( $l );

			$um = null;
			if ($fromMB){
				$um = $user->getDefaultEmail();
				$um->email = $userData[ "email" ];
				$um->save();
				$userData[ "mbPublisher" ] = $user->loadPublisher();
			} else {
				$um = UserEmail::newEmail( strtolower($userData[ "email" ]), true );
			}
			// save connection
			$user->saveUser_userEmail( $um );

			// Save user type and connect it to user
			switch ( $userType ) {
				case self::ENUM_UTYPE_PUBLISHER:
//					JPDO::$debugQuery=true;
					$user->savePublisher( Publisher::signup( $userData ) );
//					JPDO::$debugQuery=false;
					break;
				case self::ENUM_UTYPE_ADVERTISER:
					$user->saveAdvertiser( Advertiser::signup( $userData, $um ) );


					break;
				case self::ENUM_UTYPE_VISITOR:
					//$user->saveVisitor(Publisher::signup($userData->name));
					break;
				default:
					// Todo: create error

			}

			//$user->sendSysMessage("_welcomePub",null,array("user"=>$name,"urlPanel"=>$_S.'login'));
			//$user->sendSysMessage("welcomePubDash_",array(),array("user"=>$name));

			JPDO::commit();
			return $user;
		}

		/**
		 * Sign up complete
		 * @return $this
		 * @author Özgür Köy
		 */
		public function signupComplete($fromMobile=false) {
			$this->status = User::ENUM_STATUS_NOT_CONFIRMED;
			$this->save();

			$code = ReplaceCode::getCode( $this, ReplaceCode::ENUM_TYPE_CONFIRMATION );

			#the email
			if ($fromMobile){
				$this->sendSysMessage( "V2_welcomePubWithMobile", array(), array(
						"name"           => $this->getDisplayName(),
						"validationLink" => Setting::getValue( "SERVER_PROTOCOL" ) . SERVER_NAME . "/validate/" . $code,
						"mobileValidationLink" => Setting::getValue( "SERVER_PROTOCOL" ) . SERVER_NAME . "/validate/" . $code . "/mobile" //"admingle://verify/" . $country->iso . "/" . $code
					),
					null,
					1
				);
			} else {
				$this->sendSysMessage( "V2_welcomePub", array(), array(
						"name"           => $this->getDisplayName(),
						"validationLink" => Setting::getValue( "SERVER_PROTOCOL" ) . SERVER_NAME . "/validate/" . $code ),
					null,
					1
				);
			}

			#dashboard
			$this->sendSysMessage( "publisherWelcomeDashboard", array(), array(
					"username"           => $this->username
				)
			);

			return $this;
		}

		public function resendValidation($fromMobile=false){
			if ( $this->status == User::ENUM_STATUS_NOT_CONFIRMED ) {
				$code = ReplaceCode::getCode( $this, ReplaceCode::ENUM_TYPE_CONFIRMATION );

				$this->sendSysMessage( ($fromMobile ? "V2_resendConfirmationMobile" : "V2_resendConfirmation"), array(), array(
						"username"       => $this->getDisplayName(),
						"validationLink" => Setting::getValue( "SERVER_PROTOCOL" ) . SERVER_NAME . "/validate/" . $code,
						"mobileValidationLink" => Setting::getValue( "SERVER_PROTOCOL" ) . SERVER_NAME . "/validate/" . $code . "/mobile" //"admingle://verify/" . $country->iso . "/" . $code
					),
					null,
					1
				);
			}
		}


		/**
		 * auto login user
		 * @param $alkey
		 * @return bool
		 * @author Özgür Köy
		 */
		public static function loadUserWithAL() {
			$alkey = JUtil::readCookie( COOKIE_PREFIX . "al" );

			if ( is_null($alkey)  )
				return false;


			$k = new UserKey();
			$k->load( array( "alKey" => $alkey,"alKeyExp"=>"&gt ".time() ) );

			if ( $k->id > 0 ) {
				$k->alKeyExp = time() + ( COOKIE_LIFE );
				$k->save();

				$k->loadUser();
				if ( !$k->user_userKey || $k->user_userKey->gotValue !== true ) {
					$k->delete();
					return false;
				}
				$k->user_userKey->saveHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "Login with ALKey", UserHistory::ENUM_MADEBY_USER );
				$k->user_userKey->login( null, null, $k->user_userKey->id, false, true );

				if ( isset( $_SESSION[ 'adTwitterErrorText' ] ) ) {
					//TODO : WHAT ?
					exit;
				}
				unset( $k );
				return true;
			}
			else {

				JLog::log( "sec", "invalid cookie al" . serialize( $_SERVER ) . serialize( $_SESSION ) );

				unset( $k );
				return false;
			}
		}

		/**
		 * save al key
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function saveALKey() {
			$k           = $this->loadUser_userKey();
			if(!$k || $k->gotValue !== true)
				$k = new UserKey();

			$k->alKey    = JUtil::randomString( 20 );
			$k->alKeyExp = time() + ( COOKIE_LIFE );
			$k->save();

			$k->saveUser( $this );

//			JUtil::setCookie( COOKIE_PREFIX . "al", "ABCDEF" );
			JUtil::setCookie( COOKIE_PREFIX . "al",$k->alKey);

			unset( $k );
		}

		/**
		 * delete al key
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public static function deleteALKey( $alkey ) {
			$k = new UserKey();
			$k->load( array( "alKey" => $alkey ) );

			if ( $k->id > 0 ) {
				$k->delete();
			}

			unset( $k );
		}

		/**
		 * save user action
		 *
		 * @return void
		 * @author Murat
		 **/
		public function saveHistory( $actionId, $actionText, $madeBy, $actionData=null ) {
			$h             = new UserHistory();
			$h->date       = time();
			$h->actionId   = $actionId;
			$h->actionText = $actionText;
			$h->actionData = $actionData;
			$h->madeBy     = $madeBy;
			$h->save();

			$this->saveUser_userHistory( $h );
			unset( $h );
		}

		/**
		 * Send message to user
		 *
		 * @param string $messageCode
		 * @param array $titleVariables
		 * @param array $messageVariables
		 * @param int $campaign
		 * @param UserEmail $email
		 *
		 * @return bool
		 *
		 * @author Murat
		 * @author Özgür Köy
		 */
		public function sendSysMessage( $messageCode, $titleVariables = null, $contentVariables = null, $campaign = null,$email = null, $forced = false, $previewImage = null ) {
			if (!is_object($this->lang) || !$this->lang->gotValue)$this->loadLang();
			$message = LangMessage::getLangMessages(
				$this->lang->langShortDef,
				$messageCode
			);

			// Sending mail
			if ( $message[ "email" ] == 1 ) {
				if (!is_object($email)) $email = $this->getDefaultEmail();

				if ( Validate::validateEmail( $email->email ) ) {
					//If the user mark an email as spam before, we shouldn't send a new email to that user again.
					if ( $email->markedAsApam ) {
						if ( $email->spamInformed != 1 ) {
							$email->informUserSpamMail();
						}
					}
					else{
						if ($forced || ($email->unsubscribed!=1 && $email->invalid!=1)){
							Mail::create( $this, $email->email, $titleVariables, $contentVariables, $message[ "lastHistoryId" ], $campaign, $previewImage );
						}
					}
				}
			}

			// Send to user dashboard
			if ( $message[ "dashboard" ] == 1 )
				UserMessage::create( $this->id, $titleVariables, $contentVariables, $message[ "lastHistoryId" ], $campaign );


			// Send as push notification only to publishers
			if ( $message[ "pushNotification" ] == 1 && $this->utype == User::ENUM_UTYPE_PUBLISHER )
				PushMobileMessage::create( $this, $titleVariables,$contentVariables, $message[ "lastHistoryId" ], $campaign );


			return true;
		}


		/**
		 * forgot pass reminder
		 *
		 * @return bool
		 * @author Sam
		 * @author Ozgur - modified
		 **/
		public static function forgotPass($emailAddress)
		{
			global $_S;

			// check if the user name is an email
			$tryEmail = Validate::validateEmail($emailAddress);
			$u = new User();

			if ( $tryEmail ) {
				// load by email and password
				$u->load( array(
					"status"         => User::ENUM_STATUS_ACTIVE,
					"utype"          => User::ENUM_UTYPE_PUBLISHER,
					"user_userEmail" => array( "email" => "LIKE " . $emailAddress )
				) );

				if ( $u->gotValue ) {
					#kill old codes.
					$code = ReplaceCode::getCode( $u, ReplaceCode::ENUM_TYPE_PASS );
					$u->sendSysMessage( "V2_forgetPass",
						array(),
						array(
							"name" => $u->getDisplayName(),
							"date" => Format::getActualDateTime(),
							"link" => JUtil::siteAddress() . "replacePass/" . $code
						),
						null,
						$u->getDefaultEmail(),
						true
					);

					return true;

				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}

		/**
		 * change pass
		 *
		 * @return bool
		 * @author Ozgur
		 **/
		public function changePass( $password, $code=null ) {
			JPDO::beginTransaction();
			$this->pass = md5( $password );
			try{
				if($this->save()!==false){
					$this->saveHistory( UserHistory::ENUM_ACTIONID_PASSWORD_CHANGE,'Password resetted',UserHistory::ENUM_MADEBY_USER );

					$this->sendSysMessage( "V2_passwordResetted", array(), array(
							"username" => $this->getDisplayName(),
							"date"     => Format::getActualDateTime(),
							 ),
						null,
						1
					);
					if(!is_null($code))
						ReplaceCode::usedCode( $this, $code  );

					JPDO::commit();
				}
				else {
					throw new JError( "could not save user pass on reset".var_export($this) );
				}
			}
			catch(JError $j){
				JPDO::rollback();
				return false;
			}

			return true;
		}

		/**
		 * @param $code
		 * @return bool
		 * @author Özgür Köy
		 */
		public function confirmUser( $code ) {
			JPDO::beginTransaction();
			try{
				ReplaceCode::usedCode( $this, $code  );

				$this->status = User::ENUM_STATUS_ACTIVE;
				if($this->save() !== false ){
					$this->saveHistory( UserHistory::ENUM_ACTIONID_USER_CONFIRMED,'User confirmed',UserHistory::ENUM_MADEBY_USER );
					$this->sendSysMessage( "V2_userConfirmed", array(), array(
							"username" => $this->getDisplayName(),
							"panellink" => Setting::getValue( "SERVER_PROTOCOL" ) . SERVER_NAME .'/dashboard'
						),
						null,
						1
					);

					// Check if there is any active campaign for the user
					if($this->utype == User::ENUM_UTYPE_PUBLISHER){
						$this->loadPublisher();
						$this->user_publisher->addToActiveCampaigns();
					}

				}
				else {
					throw new JError( "could not save user on confirmation".var_export($this) );
				}


				JPDO::commit();
			}
			catch(JError $j){
				JPDO::rollback();

				return false;
			}

			return true;
		}

		/**
		 * logout
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public static function logout( $redirect = false ) {
			global $_S;

			$u = new User( $_SESSION[ "adId" ] );
			$u->saveHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "User Logged Out", UserHistory::ENUM_MADEBY_USER );
			unset( $u );

			$ck = JUtil::readCookie( COOKIE_PREFIX . "al" );
			if ( strlen( $ck ) > 0 ) {
				User::deleteALKey( $ck );
				JUtil::deleteCookie( COOKIE_PREFIX . "al" );
			}


			unset( $_SESSION[ "adId" ] );
			unset( $_SESSION[ "adPublisherId" ] );
			unset( $_SESSION[ "adAdvertiserId" ] );
			unset( $_SESSION[ "adScreenname" ] );
			unset( $_SESSION[ "adName" ] );
			unset( $_SESSION[ "adEmail" ] );
			unset( $_SESSION[ "adALTried" ] );
			unset( $_SESSION[ "adUserStatus" ] );
			unset( $_SESSION[ "assignSN" ] );
			unset( $_SESSION[ "adPublisherImage" ] );
			unset( $_SESSION[ "adType" ] );
			unset( $_SESSION[ "adLogged" ] );
			unset( $_SESSION[ "adNewUser" ] );
			unset( $_SESSION[ "adRedirectAfter" ] );

			if (isset($_SESSION["mbFrom"])){
				unset($_SESSION[ "regClubId" ],$_SESSION["mbFrom"],$_SESSION["mbEmail"],$_SESSION["mbName"],$_SESSION["mbSurname"],$_SESSION["mbUserID"]);
			}


			if ( $redirect ) {
				if ( headers_sent() )
					JUtil::redirectHome();
				else
					header( "Location: {$_S}" );
			}
		}


		/**
		 * activate user lang
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function actLang()
		{
			$l = $this->loadLan();

			if(is_object($l) && $l->id>0) {
		   	    Lang::switchLang($l->langShort);
			}/* else {
				if (isset($_SESSION['lang']) && strlen($_SESSION['lang']) > 0){
					$l = new Lang();
					$l->loadByShort($_SESSION['lang']);
					$this->saveLan($l);
				}
			}*/
			unset($l);
		}


		/**
		 * checks if given invite code is used before
		 *
		 * @param string $code
		 * @return boolean TRUE if used
		 */
		public static function isInviteCodeUsed($code) {

			// check in registered users
			$users = new User();
			$users->invCode = $code;
			$users->load();

			if($users->count > 0) {
				return TRUE;
			} else {
				return FALSE;
			}

		}


		/**
		 * block user
		 * @author Özgür Köy
		 */
		public function blockUser($reason=null){
	        $this->status = User::ENUM_STATUS_BLOCKED;
	        $this->save();
	        $this->saveHistory( UserHistory::ENUM_ACTIONID_USER_BLOCKED, "User blocked:" . $reason, UserHistory::ENUM_MADEBY_ADMIN, null );
		}

		/**
		 * active user after blocked
		 * @author Özgür Köy
		 */
		public function activeUser($reason=null){
			$this->status = User::ENUM_STATUS_ACTIVE;
			$this->save();
			$this->saveHistory( UserHistory::ENUM_ACTIONID_USER_ACTIVE, "User Active:" . $reason, UserHistory::ENUM_MADEBY_ADMIN, null );
		}

		/**
		 * @param $langId
		 * @author Özgür Köy
		 */
		public function updateUserLang( $langId ) {
			$l = new Lang( $langId );

			$this->lang = $l->gotValue ? $langId : Lang::getDefaultLanguage()->id;

			$this->save();
		}

		/**
		 * Deactivate user , MUST BE CALLED FROM Publisher / Advertiser
		 * @return bool
		 * @author Özgür Köy
		 */
		public function deactivate() {
			$this->status = User::ENUM_STATUS_DEACTIVATED;

			#emails
			$this->loadUser_userEmail();
			$um = null;
			if ( $this->user_userEmail && $this->user_userEmail->gotValue ) {
				do {
					if ($this->user_userEmail->deleted == 0 && $this->user_userEmail->isDefault){
						$um = $this->user_userEmail;
					}
					$this->user_userEmail->deleted = 1;
					$this->user_userEmail->save();
				} while ( $this->user_userEmail->populate() );
			}
			$this->saveHistory( UserHistory::ENUM_ACTIONID_ACCOUNT_DEACTIVATION, "user deactivation", UserHistory::ENUM_MADEBY_USER );
			$this->sendSysMessage( "deactivationInformationMail", array(), array(
					"username" => $this->username,
					"date"     => date( "d-m-Y H:i", time() )
				),
				null,
				$um,
				true
			);

			#auto login keys
			$ck = JUtil::readCookie( COOKIE_PREFIX . "al" );

			if($ck && strlen($ck)>0){
				$this->deleteALKey( $ck );
				JUtil::deleteCookie( COOKIE_PREFIX . "al" );
			}

			return $this->save() !== false;
		}

		/**
		 * Get user default email from userEmail table
		 *
		 * @return UserEmail
		 * @author Murat
		 */
		public function getDefaultEmail(){
			$this->loadUser_userEmail(array(
				"user_userEmail" => $this->id,
				"email"          => "IS NOT NULL",
				"isDefault"      => 1
			));
			return $this->user_userEmail;
		}

		/**
		 * set user default email from userEmail table
		 *
		 * @return string
		 * @author Murat
		 */
		public function setDefaultEmail($email){

			$this->loadUser_userEmail(array(
				"user_userEmail" => $this->id,
				"isDefault" => 1
			));

			return $this->user_userEmail->updateUserEmail($email);
		}

		/**
		 * Display name for all user types
		 *
		 * @return string
		 * @author Özgür Köy
		 */
		public function getDisplayName() {
			$userName = "";

			if($this->utype == User::ENUM_UTYPE_PUBLISHER)
			{
				$this->loadPublisher();
				$userName = $this->user_publisher->getFullName();
			}
			elseif($this->utype == User::ENUM_UTYPE_ADVERTISER){
				$this->loadAdvertiser();
				$userName = $this->user_advertiser->companyName;
			}

			return $userName;
		}

		/**
		 * Check user that exist on adMingle or not
		 * @param string $type
		 * @param string $uid
		 * @return array
		 * @author Murat
		 */
		public static function check($type,$uid){
			$data = array(
				"found" => false,
				"user_id" => null
			);
			$u = new User();
			$filter = array(
				"status" => "NOT IN " . User::ENUM_STATUS_DEACTIVATED . "," . User::ENUM_STATUS_DELETED,
				"utype" => User::ENUM_UTYPE_PUBLISHER
			);
			switch ($type){
				case "email":
					if (Validate::validateEmail($uid)){
						$filter["user_userEmail"] = array(
							"email" => $uid,
							"deleted" => 0
						);
					}
					break;
				case "twitter":
					$filter["user_publisher"] = array(
						"hasTwitter" => true,
						"publisher_snTwitter" => array(
							"twid" => $uid
						)
					);
					break;
				case "facebook":
					$filter["user_publisher"] = array(
						"hasFacebook" => true,
						"publisher_snFacebook" => array(
							"fbid" => $uid
						)
					);
					break;
				case "gplus":
					$filter["user_publisher"] = array(
						"hasGPlus" => true,
						"publisher_snGoogle" => array(
							"gpid" => $uid
						)
					);
					break;
				case "vk":
					$filter["user_publisher"] = array(
						"hasVK" => true,
						"publisher_snVK" => array(
							"vkid" => $uid
						)
					);
					break;
				case "xing":
					$filter["user_publisher"] = array(
						"hasXING" => true,
						"publisher_snXING" => array(
							"xid" => $uid
						)
					);
					break;
			}
			$u->load($filter);
			if ($u->gotValue){
				$u->getDefaultEmail();
				$u->loadPublisher();
				$data = array(
					"found" => true,
					"user_id" => $u->id,
					"email" => $u->user_userEmail->email,
					"name" => $u->user_publisher->name,
					"surname" => $u->user_publisher->surname,
					"validated" => $u->status == User::ENUM_STATUS_ACTIVE ? true : false
				);
			}
			unset($u);
			return $data;
		}

		/**
		 * Register from Mingle Button
		 *
		 * @param string $email
		 * @param string $passHash
		 * @param int $clubID
		 * @param string $loginType
		 * @param int $mbID
		 * @return int
		 * @author Murat
		 */
		public static function mbRegister($email,$name,$surname,$clubID,$loginType,$mbID,$adMingleID=null){
			if (Validate::validateEmail($email)){
				$c = new Club($clubID);
				if ($c->gotValue){
					$u = new User();
					$filter = array(
						"status" => "NOT IN " . User::ENUM_STATUS_DEACTIVATED . "," . User::ENUM_STATUS_DELETED,
						"utype" => User::ENUM_UTYPE_PUBLISHER
					);
					if (!is_null($adMingleID) && is_numeric($adMingleID)){
						$filter["id"] = $adMingleID;
					} else {
						$filter["user_userEmail"] = array(
							"email" => $email,
							"deleted" => 0
						);
					}
					$u->load($filter);
					if (!$u->gotValue){
						$u = new User();
						$ue = UserEmail::newEmail($email,true);
						$u->status = User::ENUM_STATUS_SIGNUP_INCOMPLETE;
						$u->save();
						$u->saveUser_userEmail($ue);
						$p = new Publisher();
						$p->name = $name;
						$p->surname = $surname;
						$p->user_publisher = $u->id;
						$p->ownerClub = $clubID;
						$p->save();
					}
					$u->fromMB = true;
					if (is_null($u->publicKey)) $u->publicKey = JUtil::randomString(6);
					if (is_null($u->privateKey)) $u->privateKey = JUtil::randomString(6);

					$umb = new UserMingleButton();
					$umb->load(array(
						"mbClub" => $clubID,
						"mbID" => $mbID
					));
					if (!$umb->gotValue){
						$umb->mbClub = $clubID;
						$umb->mbID = $mbID;
						$u->loadPublisher()->saveClubs($clubID);
					}
					$umb->loginType = $loginType;
					$umb->save();
					$u->saveUser_mingleButton($umb);
					$u->save();

					return array(
						"uid" => $u->id,
						"publicKey" => $u->publicKey,
						"privateKey" => $u->privateKey
					);
				} else {
					return "unknown club";
				}
			} else {
				return "invalid email";
			}
		}

		/**
		 * Get MB User with public and private key
		 *
		 * @param string $publicKey
		 * @param string $privateKey
		 * @return User|null
		 * @author Murat
		 */
		public static function getMBUser($publicKey,$privateKey){
			$u = new User();
			$u->with("user_publisher","user_mingleButton")->load(array(
				"status" => User::ENUM_STATUS_SIGNUP_INCOMPLETE,
				"fromMB" => true,
				"publicKey" => $publicKey,
				"privateKey" => $privateKey
			));
			if ($u->gotValue){
				$u->getDefaultEmail();
				return $u;
			} else {
				unset($u);
				return null;
			}
		}

		/**
		 * Send reminder mail for complete information to incompleted users
		 *
		 * @return void
		 * @author Murat
		 */
		public static function sendReminder(){
			$u = new User();
			$u->limitCount = 10000;
			$u->with("user_userEmail")->load(array(
				"status" => User::ENUM_STATUS_SIGNUP_INCOMPLETE,
				"_custom" => "`reminderCount` IS NULL OR `reminderCount` < 3",
				"user_userEmail" => array(
					"isDefault" => true,
					"markedAsSpam" => false,
					"deleted" => false,
					"unsubscribed" => false,
					"invalid" => false
 				)
			));
			if ($u->gotValue){
				do{
					$u->sendSysMessage(
						"V2_registerIncompleteReminder",
						array(),
						array(
							"name" => $u->getDisplayName(),
							"link" => JUtil::siteAddress() . (is_null($u->publicKey) || is_null($u->privateKey) ? "signup-publisher-incomplete" : "signup-publisher-mb/{$u->publicKey}/{$u->privateKey}")
						),
						null,
						$u->user_userEmail
					);
					if (is_null($u->reminderCount)){
						$u->reminderCount = 1;
					} else {
						$u->reminderCount++;
					}
					$u->save();
					Tool::echoLog("Registration email scheduled for {$u->id}#{$u->username} to {$u->user_userEmail->email} with reminderCount {$u->reminderCount}");
				} while($u->populate());
			} else {
				Tool::echoLog("No user found for reminder");
			}
		}

	}
