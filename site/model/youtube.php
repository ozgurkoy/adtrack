<?php
/**
 * youtube real class - firstly generated on 10-10-2012 10:39, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/youtube.php';

class Youtube extends Youtube_base
{
	
	/**
	 * ava. video length
	 *
	 * @var array
	 **/
	public static $length=array(30,60,90,120,180);
	
	/**
	 * cost per video
	 *
	 * @var int
	 **/
	public static $costPerVideo=50;
	
	/**
	 * minimum campaign length
	 *
	 * @var int
	 **/
	public static $minCampaignDays=6;
	
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
		
		require_once 'Zend/Loader.php';
		
		Zend_Loader::loadClass('Zend_Gdata_YouTube');
		Zend_Loader::loadClass('Zend_Gdata_AuthSub');
		Zend_Loader::loadClass('Zend_Gdata_App_Exception');
		
	}
	
	/**
	 * get youtube auth link
	 *
	 * @return string
	 * @author Özgür Köy
	 **/
	public static function getAuthLink()
	{
		require_once 'Zend/Loader.php';
		
		Zend_Loader::loadClass('Zend_Gdata_YouTube');
		Zend_Loader::loadClass('Zend_Gdata_AuthSub');
		Zend_Loader::loadClass('Zend_Gdata_App_Exception');
		
		global $_S,$__DP;
		
	    $scope = 'https://gdata.youtube.com';
	    $secure = false;
	    $session = true;

		$nextUrl = $_S."site/service/youtubeCB.php";
		

	    $url = Zend_Gdata_AuthSub::getAuthSubTokenUri($nextUrl, $scope, $secure, $session);
		
		
		return $url;
	}
	
	/**
	 * token
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function persistentToken($singleToken)
	{	
		try {
			$persToken = Zend_Gdata_AuthSub::getAuthSubSessionToken($singleToken);
	    } catch (Zend_Gdata_App_Exception $e) {
	        // print 'ERROR - Token upgrade for ' . $singleUseToken
	            // . ' failed : ' . $e->getMessage();
	        return false;
	    }
	    
		
		return $persToken;
	}
	
	/**
	 * get youtube username
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function getInfo()
	{
		$httpClient = $this->getClient();
		if($httpClient==false) return false;
		
		
		$you = new Zend_Gdata_YouTube($httpClient,YOUTUBE_APPID,null,YOUTUBE_DEVKEY);
		
		try {
			$profile = $you->getUserProfile("default");
			
		} catch (Zend_Gdata_App_HttpException $e) {
		    if ($e->getResponse()->getStatus() === 401) {
				
			    JLog::log("youtube","youtube http error(401) :".$e->getResponse());
				
				if(strpos($e->getMessage(),"NoLinkedYouTubeAccount")!==false)
					$_SESSION['youtubeError'] = 'nolinkedaccount'; #no linked youtube account
				else
					$_SESSION['youtubeError'] = 'connerror'; #other error
					
				return false;
			}
		}
		
		return array(
			"username"=>$profile->getUserName(),
			"feed"=>$profile->getLink("alternate")->href,
			"name"=>$profile->getFirstName()." ".$profile->getLastName()
		);
		
	}
	
	/**
	 * Convenience method to obtain an authenticted Zend_Http_Client object.
	 * TODO: HANDLE ERRORS
	 *
	 * @return Zend_Http_Client An authenticated client.
	 * @author Özgür Köy
	 **/
	private function getClient()
	{
		if(!(strlen($this->token)>0)) return false;
		
	    try {
	        $httpClient = Zend_Gdata_AuthSub::getHttpClient($this->token);
	    } catch (Zend_Gdata_App_Exception $e) {
	        JLog::log("gen","youtube http error :".$e->getResponse());
			$_SESSION['youtubeError'] = 'connerror';
	        return false;
	    }
	    $httpClient->setHeaders('X-GData-Key', 'key='. $this->token);
	    return $httpClient;
		
	}
	
	/**
	 * username
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function checkUsername($username)
	{
		$this->customClause = "username LIKE '{$username}'";

		return $this->load()->count()>0;
	}
	
	/**
	 * upload video
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function uploadVideo($file,$title,$description,$keywords)
	{
		Zend_Loader::loadClass('Zend_Uri_Http');		
		Zend_Loader::loadClass('Zend_Gdata_App_MediaFileSource');
		
		$httpClient = $this->getClient();
		if($httpClient==false) return false;
		
		$yt = new Zend_Gdata_YouTube($httpClient);
		$myVideoEntry = new Zend_Gdata_YouTube_VideoEntry();

		$filesource = new Zend_Gdata_App_MediaFileSource($file);
		$httpClient = $this->getClient();
		
		$httpClient = new Zend_Gdata_HttpClient();
		$httpClient->setAuthSubToken($this->token);

		$yt = new Zend_Gdata_YouTube($httpClient,YOUTUBE_APPID,null,YOUTUBE_DEVKEY);
		// $filesource = $yt->newMe	diaFileSource($file); 
		
		$filesource->setContentType('video/mp4'); 
		$filesource->setSlug($file);  

		$myVideoEntry = new Zend_Gdata_YouTube_VideoEntry();
		
		// TODO: CHECK HEADER
		

		$myVideoEntry->setMediaSource($filesource);  
		$myVideoEntry->setVideoTitle($title); 
		$myVideoEntry->setVideoDescription($description); 

		$myVideoEntry->setVideoCategory('Entertainment');   //TODO : fix category
		$myVideoEntry->SetVideoTags($keywords);  
		$myVideoEntry->setVideoDeveloperTags(array('admingle','upload'));  
		

		$uploadUrl = 'http://uploads.gdata.youtube.com/feeds/api/users/default/uploads';
		
		try {  
		    $newEntry = $yt->insertEntry($myVideoEntry, $uploadUrl, 'Zend_Gdata_YouTube_VideoEntry'); 
		
			unset($yt,$filesource,$myVideoEntry);
			
		} catch (Zend_Gdata_App_HttpException $httpException) {   
		    $httpException->getRawResponseBody();
		    JLog::log("youtube","Http error:".serialize($httpException));
			return array("error"=>"Http error:".serialize($httpException));
		} catch (Zend_Gdata_App_Exception $e) {
		    $mes=$e->getMessage();
			JLog::log("youtube","App error:".serialize($mes));
			return array("error"=>$mes);
		}
		// TODO : CHECK DUPLICATE VIDEO !
		
		return array("id"=>$newEntry->getVideoId(),"url"=>$newEntry->getVideoWatchPageUrl());
	}
	
	/**
	 * check if file is video
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function isVideo($path)
	{
		global $__DP;
		
		if (!class_exists('getID3', false)) include($__DP."site/lib/getid3/getid3/getid3.php");

		$getID3 = new getID3;
		$info = $getID3->analyze($path);
		
		if(strpos($info["mime_type"], "video")!==false){
			//double check
			$movie = new ffmpeg_movie($path);
			
			$h =  $movie->hasVideo();
			unset($getID3,$movie);
			return $h;
			
		}
		
	}
	
	/**
	 * check file video length
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function getDuration($path)
	{
		global $__DP;
		$movie = new ffmpeg_movie($path);
		
		$h =  $movie->getDuration();
		unset($movie);
		return $h;
		
	}

}

