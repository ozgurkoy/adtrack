<?php
/**
 * storeBrand real class - firstly generated on 05-02-2014 14:14, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/storeBrand.php';

class StoreBrand extends StoreBrand_base
{
	public static $imagePath = "site/layout/images/store/brands/";

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * @param $params
	 * @param $_files
	 * @return bool
	 * @author Özgür Köy
	 */
	public function add( $params, $_files ) {
		global $__DP;
		$this->assignValuesToObject( $params );

		$this->active = isset( $params[ "active" ] );

		if ( strlen( $_files[ "image" ][ "name" ] ) > 0 ) {
			$extension = end( explode( ".", $_files[ "image" ][ "name" ] ) );
			$file      = StoreBrand::$imagePath . JUtil::randomString( 17 ) . "." . $extension;

			@move_uploaded_file( $_files[ "image" ][ "tmp_name" ], $__DP . $file );
			#todo check for success
			$this->image = $file;
		}
		$this->save();

		return true;
	}

	/**
	 * load active brands
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function listsActives() {
		#check for one with categories and products
		return $this->orderBy( "rorder ASC" )->useDistinct()->load(
			array( "active" => 1, "categories" => array( "id" => "&gt 0", "products" => array( "used"=>0 ) ) )
		);
	}

	/**
	 * load active categories
	 * @param null $parent
	 * @return storeCategory
	 * @author Özgür Köy
	 */
	public function getCategories( $parent = null ) {
		$cats = false;
//		if ( is_null( $parent ) ) {
//			JPDO::debugQuery();
			$cats = $this->loadCategories(
				array(
					"_custom" => "`storeCategory`.`active`=1 AND `parentCategory` " . ( is_null( $parent ) ? "IS NULL" : "=" . (int)$parent ) ),
				0, 50, "rorder ASC", false );
			JPDO::debugQuery( false );
//		}
		if ( !$cats || $cats->gotValue == false ) {
			return new StoreCategory();
		}

		return $cats;
	}



}

