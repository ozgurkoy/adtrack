<?php
/**
 * publisherPaymentOption real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherPaymentOption.php';

class PublisherPaymentOption extends PublisherPaymentOption_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * @param $params
	 * @return bool
	 * @author Ozgur
	 */
	public function add( $params, Publisher &$publisher ) {
		JPDO::beginTransaction();
		try {
			if(!is_string($params["details"])) # hopefully it s good
				$params[ "details" ] = json_encode( $params[ "details" ] );

			$this->displayName = $params[ "displayName" ];
			$this->details     = $params[ "details" ]; #json

			$this->deletePublisherBilling();

			//check for publisher billing..
			if ( empty( $params[ "billingInfo" ][ "billingName" ] ) === false ) {
				$b = new PublisherBilling();

				$b->billingName = $params[ "billingInfo" ][ "billingName" ];
				$b->taxOffice   = $params[ "billingInfo" ][ "taxOffice" ];
				$b->taxNumber   = $params[ "billingInfo" ][ "taxNumber" ];
				$b->address     = $params[ "billingInfo" ][ "address" ];
				$b->updateDate  = time();
				$b->save();

				$this->publisherBilling = $b->id;
			}

			//bank
			if ( !empty( $params[ "bank" ] ) ) {
				$this->bank = $params[ "bank" ];
			}


			//withdraw option
			$this->publisherWithdrawOption          = $params[ "publisherWithdrawOptionId" ];
			$this->publisher_publisherPaymentOption = $publisher->id;
			$this->save();

			JPDO::commit();

			return true;

		}
		catch ( Exception $ex ) {
			JPDO::rollback();
			JLOG::log( "cri", "Exception occured while saving publisher option : " . serialize( $ex ) . serialize( $params ) );

			return false;
		}

	}

	/**
	 * @param $params
	 * @return bool
	 * @author Ozgur
	 */
	public function edit( $params ) {
		JPDO::beginTransaction();

		try {
			$this->displayName = $params[ "displayName" ];
			$this->details     = $params[ "details" ]; #json

			//check for publisher billing..

			$b = $this->loadPublisherBilling();

			if ( !$b || $b->gotValue === false )
				$b = new PublisherBilling();

			if ( empty( $params[ "billingInfo" ][ "billingName" ] ) === false ) {
				$b->billingName = (string) $params[ "billingInfo" ][ "billingName" ];
				$b->taxOffice   = (string) $params[ "billingInfo" ][ "taxOffice" ];
				$b->taxNumber   = (string) $params[ "billingInfo" ][ "taxNumber" ];
				$b->address     = (string) $params[ "billingInfo" ][ "address" ];
				$b->updateDate   = time();
				$b->save();

				$this->publisherBilling = $b->id;
			}
			elseif( $b->gotValue ) // delete since on info is left behind.
				$this->deletePublisherBilling();

			//bank
			if ( empty( $params[ "bank" ] ) === false )
				$this->bank = $params[ "bank" ];

			//withdraw option
			$this->publisherWithdrawOption = $params[ "publisherWithdrawOptionId" ];

			$this->save();

			JPDO::commit();

			return true;

		}
		catch ( Exception $ex ) {
			JPDO::rollback();
			JLOG::log( "cri", "Exception occured while editing publisher option : " . serialize( $ex ) . serialize( $params ) );

			return false;
		}

	}

	/**
	 * Print The Payment as Html
	 * @author Sam
	 * @return string
	 */
	public function printHTML() {

		// 			$this->publisherWithdrawOption = $params[ "publisherWithdrawOptionId" ];
		//$this->details     = $params[ "details" ]; #json

		$HTML = "";

		if(!is_null($this->bank)){
			$this->loadBank();

			if($this->bank->gotValue){
				$HTML .= $this->bank->printHTML();
			}else{
				$HTML .= "Bank Not Found";
			}
			$HTML .= "<br/>";
		}

		$fd = json_decode($this->details);
		foreach($fd as $key=>$value){
			$HTML .= sprintf("%s: %s <br/>",$key,$value);
		}
		return $HTML;

	}

}

