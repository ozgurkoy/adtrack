<?php
/**
 * twitter action class
 * $connection->post('statuses/update', array('status' => 'Text of status here','in_reply_to_status_id' => 123456));
 *
 * @package adminge
 * @author Özgür Köy
 **/
class Twitter
{

	/**
	 * twitter id on twitter api
	 *
	 * @var int
	 **/
	public static $id=0;

	/**
	 * avatar url; https
	 *
	 * @var string
	 **/
	public static $pictureUrl='';

	/**
	 * fullname on twitter
	 *
	 * @var string
	 **/
	public static $fullname='';

	/**
	 * username on twitter
	 *
	 * @var string
	 **/
	public static $username='';

	/**
	 * location on twitter
	 *
	 * @var string
	 **/
	public static $location='';

	/**
	 * user is following N users
	 *
	 * @var int
	 **/
	public static $followingCount=0;

	/**
	 * user is followed by N users
	 *
	 * @var int
	 **/
	public static $followerCount=0;

	/**
	 * user posted N status messages
	 *
	 * @var int
	 **/
	public static $statusCount=0;

	/**
	 * user requires permission to show details (0/1)
	 *
	 * @var int
	 **/
	public static $protected=0;

	/**
	 * user listed on N lists
	 *
	 * @var int
	 **/
	public static $listedCount=0;

	/**
	 * user is verified (0/1)
	 *
	 * @var int
	 **/
	public static $verified=0;

	/**
	 * website of user
	 *
	 * @var string
	 **/
	public static $url='';

	/**
	 * twitter info of user
	 *
	 * @var string
	 **/
	public static $description='';

	/**
	 * twitter language of user
	 *
	 * @var string
	 **/
	public static $language='';

	/**
	 * twitter authentication key of user
	 *
	 * @var string
	 **/
	public static $key='';

	/**
	 * twitter authentication key secret of user
	 *
	 * @var string
	 **/
	public static $secret='';

	/**
	 * processed response error code
	 * 0: no error
	 * 1: unknown error
	 * 2: no user
	 * 3: authentication error
	 * 4: api error
	 * @var int
	 **/
	public static $errorCode=0;

	/**
	 * sign in button
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function signin() {
		global $__DP;

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		/* Build TwitterOAuth object with client credentials. */
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
		$logT = self::logRequest(CONSUMER_KEY, CONSUMER_SECRET, '', '', $connection->requestTokenURL());

		/* Get temporary credentials. */
		$request_token = $connection->getRequestToken(OAUTH_CALLBACK);
		self::logResponse($logT, $request_token);

		/* If last connection failed don't display authorization link. */
		switch ($connection->http_code) {
		  case 200:
			if (!is_array($request_token) || !isset($request_token['oauth_token']) || strlen($request_token['oauth_token']) < 1){
				die("Twitter down for maintenance. Please try again later");
			}
			/* Save temporary credentials to session. */
			$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
			$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

		    /* Build authorize URL and redirect user to Twitter. */
		    $url = $connection->getAuthorizeURL($token);
		    header('Location: ' . $url);
		    break;
                case 429:
                        JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
			$_SESSION['twitterApiError'] = 1;
			die('Twitter Error 429 - Too Many Requests');
                        break;
                case 500:
                        JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
			$_SESSION['twitterApiError'] = 1;
			die('Twitter Error 500 - Internal Server Error');
                        break;
                case 502:
                        JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
			$_SESSION['twitterApiError'] = 1;
			die('Twitter Error 502 - Bad Gateway, Twitter is down or being upgraded.');
                        break;
                case 503:
                        JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
			$_SESSION['twitterApiError'] = 1;
			die('Twitter Error 503 - Service Unavailable, The Twitter servers are up, but overloaded with requests. Try again later');
                        break;
                case 504:
                        JLog::log("cri","Twitter api error".serialize($_SERVER).serialize($connection));
			$_SESSION['twitterApiError'] = 1;
			die('Twitter Error 504 - Gateway timeout, The Twitter servers are up, but the request could not be serviced due to some failure within our stack. Try again later.');
                        break;
                
		  default:
		    /* Show notification if something went wrong. */
			JLog::log("cri","Twitter api error: SERVER: \n" . serialize($_SERVER) . "\n Connection: \n" . serialize($connection));
			$_SESSION['twitterApiError'] = 1;
			die('Twitter Error , Please contact to site Administrator ');
		    return -1;
		}
	}

	/**
	 * Executes on twitter App callback
	 *
	 * @return void
	 * @author Özgür Köy
	 * @author tiger
	 **/
	public function callback($request)
	{
		global $__DP, $_S;

		if(isset($_REQUEST['denied'])) {
			$_SESSION['oauth_status'] = 'denied';
			return false;
		}

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		/* If the oauth_token is old redirect to the connect page. */
		if (!isset($_SESSION['oauth_token']) || (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token'])) {
			JLog::log("sec","Twitter auth tokens dont match. SERVER: \n".serialize($_SERVER) . "\n SESSION: \n" . serialize($_SESSION));

			$_SESSION['oauth_status'] = 'error';
			return false;
		}

		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
		$logT = self::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret'], $connection->accessTokenURL());

		$access_token = $connection->getAccessToken(isset($_REQUEST['oauth_verifier']) ? $_REQUEST['oauth_verifier'] : null);
		self::logResponse($logT, $access_token);

		/* Save the access tokens. Normally these would be saved in a database for future use. */
		$_SESSION['access_token'] = $access_token;

		/* Remove no longer needed request tokens */
		unset($_SESSION['oauth_token']);
		unset($_SESSION['oauth_token_secret']);
		$user = new User();

		/* If HTTP response is 200 continue otherwise send to connect page to retry */
		if (200 == $connection->http_code) {
			/* The user has been verified and the access tokens can be saved for future use */
			$_SESSION['oauth_status'] = 'verified';
			$refill = $connection->get('account/verify_credentials');
			if(!($refill->id > 0)) {
				//wrong wrong
				JLog::log('eng','Twitter status error, and 200 !' . serialize($_SERVER) . serialize($refill));

				$_SESSION['oauth_status'] = 'error';
				return false;
			}
                        
                        if (ENABLE_INVITE >= 1)
                        {
                            $invite = new Invite();
                            $userInviteID = $invite->AddTwitterUser($access_token['oauth_token'],$access_token['oauth_token_secret'],$refill);
                            return $userInviteID;
                        }
                        
                        
			$twi = new TwitterInfo();
			$twi->customClause = 'twid = ' . intval($refill->id);
			$twi->load();

			if($refill->protected == 1) {
				$_SESSION['twitterProtected'] = TRUE;
			} else {
				unset($_SESSION['twitterProtected']);
			}

			self::updateSelfProperties($refill);

			//save user
			if($twi->id > 0) {
				//already registered
				//update twitter details
				$twi->twKey = $access_token['oauth_token'];
				$twi->twKeySecret = $access_token['oauth_token_secret'];
				$user = $twi->loadUser();

				self::updateDbData($twi);
				self::fillSession();

				if (!isset($_SESSION["adId"])){
					if($user && strlen($user->username) > 3) {
						$user->fillSession();
						$user->actLang();
						$user->resetErrorStatus();
						if (strlen($user->pass) > 3){
							return 'fullReggdWithPass';
						}else{
							return 'fullReggd';
						}
					} else {
						$_SESSION['twitterAct'] = true;
						return 'step2';
				    }
				}
			} else {
				$twi = new TwitterInfo();
				$twi->twKey = $access_token['oauth_token'];
				$twi->twKeySecret = $access_token['oauth_token_secret'];
				$twi->customCost = 0;
				$twi->cost = 0;
				$twi->save();
				self::updateDbData($twi);
				self::fillSession();

				$klout = new TwitterInfoKlout();
				$klout->twitterInfo = $twi->id;
				$klout->save();

				if (!isset($_SESSION["adId"])){
					$_SESSION['twitterAct'] = true;
					return 'step2';
				}
			}

			if (isset($_SESSION["adId"])){
				if (!isset($user) || is_null($user))
					$user = $twi->loadUser();

				if (!is_null($user) && $user->count && $user->id != $_SESSION["adId"]){
					Tool::setLastError("user","twitterAccountUserByAnotherUser","twitter->callback");
					if (isset($_SESSION["signupAddSNA"])){
						header("Location: ".$_S."sign-up-pub3");
					} else {
						header("Location: ".$_S."editProfile");
					}
					exit;
				} elseif (!is_null($user) && $user->count && $user->id == $_SESSION["adId"]){
					$user->addedSNA();
					$user->resetErrorStatus();
					Tool::setLastError("user","twitterAccountAlreadyAdded","twitter->callback");
					if (isset($_SESSION["signupAddSNA"])){
						header("Location: ".$_S."sign-up-pub3");
					} else {
						header("Location: ".$_S."editProfile");
					}
					exit;
				} elseif (is_null($user) || !$user->count){
					$user = new User($_SESSION["adId"]);
					if ($user->utype != "p"){
						Tool::setLastError("user","unAuthorizedActionHappened","twitter->callback");
						header("Location: ".$_S."editProfile");
						exit;
					} else {
						$twi->saveUser($user);
						$user->addedSNA();
						$user->resetErrorStatus();

						if (isset($_SESSION["signupAddSNA"])){
							header("Location: ".$_S."sign-up-pub3?addSNA=twitter&result=success");
						} else {
							header("Location: ".$_S."editProfile?addSNA=twitter&result=success");
						}
						exit;
					}
				} else {
					JLog::log("cri","Unknown situation happened on trying to relate the Twitter account to adMingle account.");
					Tool::setLastError("user","unhandledErrorHappened","twitter->callback");
					header("Location: ".$_S."editProfile");
					exit;
				}
			}
		} else {
			/* Save HTTP status for error dialog on connnect page.*/
			$_SESSION['oauth_status'] = 'error';
			JLog::log("cri","Twitter status error. SERVER: \n" . serialize($_SERVER) . "\n REQUEST : \n" . serialize($_REQUEST));
			if (isset($_SESSION["adId"])){
				Tool::setLastError("user","unhandledErrorHappened","twitter->callback");
				header("Location: ".$_S."dashboard");
				exit;
			} else {
				return false;
			}
		}
	}

	/**
	 * archive the current and update current twitter info
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function updateInfo(&$info, $refill,$twid=null,$twKey=null,$twKeySecret=null)
	{
		if(is_null($twid)){
			$th = new TwitterHistory();

			$th->timg 	= $info->timg;
			$th->name 			= $info->name;
			$th->screenname 	= $info->screenname;
			$th->location 		= $info->location;
			$th->following 		= $info->following;
			$th->follower 		= $info->follower;
			$th->statusCount 	= $info->statusCount;
			$th->url 			= $info->url;
			$th->description	= $info->description;
			$th->verified 		= $info->verified;
			$th->listed_count 	= $info->listed_count;
			$th->protected 		= $info->protected;
			$th->lang 			= isset($info->lang)?$info->lang:"";
			$th->user           = $info->user;
			$th->save();
		}

		//update info
		$info->timg 			= str_replace("_normal.", ".", $refill->profile_image_url);
		$info->name 			= $refill->name;
		$info->screenname 		= $refill->screen_name;
		$info->location 		= $refill->location;
		$info->following 		= $refill->friends_count;
		$info->follower 		= $refill->followers_count;
		$info->statusCount 		= $refill->statuses_count;
		$info->url 				= $refill->url;
		$info->description		= $refill->description;
		$info->verified 		= $refill->verified;
		$info->listed_count 	= $refill->listed_count;
		$info->protected 		= $refill->protected;
		$info->lang 			= $refill->lang;

		$info->save();

		$_SESSION['twInfoId'] 	= $info->id;
		$_SESSION['timg'] 		= $info->timg;
		$_SESSION['tname'] 		= $refill->name;
		$_SESSION['tsname'] 	= $refill->screen_name;
		$_SESSION['tlocation'] 	= $refill->location;

		if(is_null($twid)){
			$info->saveHistory($th);
		}

		if(!is_null($twKey) ){
			if(!is_null($twid))
				$info->twid = $twid;

			$info->twKey = $twKey;
			$info->twKeySecret = $twKeySecret;
			$info->save();
		}

	}

	/**
	 * login by username etc.
	 *
	 * @return mixed
	 * @author Özgür Köy
	 **/
	public static function login(SnTwitter $ti) {
		global $__DP;

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		$connection = new TwitterOAuth(Setting::getValue("CONSUMER_KEY"), Setting::getValue("CONSUMER_SECRET"), $ti->twKey, $ti->twKeySecret);
		$logT = self::logRequest(Setting::getValue("CONSUMER_KEY"), Setting::getValue("CONSUMER_SECRET"), $ti->twKey, $ti->twKeySecret, 'account/verify_credentials');

		$refill = $connection->get('account/verify_credentials');
		self::logResponse($logT, $refill);

		if(isset($refill->error)) {
			if(strpos($refill->error,"Not found") !== false) {

				//user deleted/deactivated
                return "errorTwitterNotFound";

			} elseif(strpos($refill->error,"Could not authenticate") !== false) {

                return "errorTwitterCouldNotAuth";

			}
		} else if(isset($refill->errors)) {

            return User::$errorTwitterCouldNotAuth;

		} else {

			//update twitter info, save history
			self::updateSelfProperties($refill);
			self::updateDbData($ti);
			self::fillSession();

			return 1;
		}
	}

	/**
	 * get last rt's count
	 *
	 * @return int
	 * @author Özgür Köy
	 **/
	public function getLastRT($cred,&$user,$count=18)
	{
		global $__DP;

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $cred->twKey, $cred->twKeySecret);
		$logT = self::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $cred->twKey, $cred->twKeySecret, 'statuses/retweets_of_me');

		$reTweetsOfUser = $connection->get('statuses/retweets_of_me');
		self::logResponse($logT, $reTweetsOfUser);

		if(is_object($reTweetsOfUser) && isset($reTweetsOfUser->error) && strlen($reTweetsOfUser->error)>0){
			if(strpos($reTweetsOfUser->error,"Not found")!==false)
                return User::$errorTwitterNotFound;//user deleted/deactivated
			elseif(strpos($reTweetsOfUser->error,"Could not authenticate")!==false)
                return User::$errorTwitterCouldNotAuth;
			else
				return User::$errorTwitterRts;

		}
		elseif(isset($reTweetsOfUser->errors)){
			return User::$errorTwitterApi;
		}

		$rtCount = 0;
		if(sizeof($reTweetsOfUser)>0){
			$c0 = 1;
			foreach ($reTweetsOfUser as $keys => $rows) {
				//temp fix, use api
				// print_r($rows);
				$rtCount += $rows->retweet_count;
				if($c0>=$count) break;
			}
		}
		return array($rtCount);
	}

	/**
	 * Updates local properties from successful response of twitter account/verify_credentials request
	 *
	 * @param mixed $refill
	 */
	public static function updateSelfProperties($refill) {
		//defaults
		self::$errorCode		= 0;
		self::$id				= 0;
		self::$pictureUrl		= '';
		self::$fullname			= '';
		self::$username			= '';
		self::$location			= '';
		self::$followingCount	= 0;
		self::$followerCount	= 0;
		self::$statusCount		= 0;
		self::$url				= '';
		self::$description		= '';
		self::$verified			= 0;
		self::$listedCount		= 0;
		self::$protected		= 0;
		self::$language			= '';

		//twitter api response to local properties
		self::$id				= $refill->id;
		self::$pictureUrl		= str_replace("_normal.", ".", $refill->profile_image_url_https);
		self::$fullname			= $refill->name;
		self::$username			= $refill->screen_name;
		self::$location			= $refill->location;
		self::$followingCount	= $refill->friends_count;
		self::$followerCount	= $refill->followers_count;
		self::$statusCount		= $refill->statuses_count;
		self::$url				= $refill->url;
		self::$description		= $refill->description;
		self::$verified			= strlen($refill->verified) ? 1 : 0;
		self::$listedCount		= $refill->listed_count;
		self::$protected		= strlen($refill->protected) ? 1 : 0;
		self::$language			= $refill->lang;
	}

	/**
	 * Loads user details from twitter api and updates local properties
	 *
	 * @return mixed
	 * @author tiger
	 **/
	public static function loadFromAPI($key, $secret, $debugLevel=0)
	{
		global $__DP;

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $key, $secret);
		$logT = self::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $key, $secret, 'account/verify_credentials');

		$refill = $connection->get('account/verify_credentials');
		self::logResponse($logT, $refill);

		if($debugLevel > 1) {
			Tool::echoLog('Response from API:');
			print_r($refill);
			echo PHP_EOL;
		}

		if(isset($refill->error)) {
			if(strpos($refill->error,"Not found") !== false) {

				//user deleted/deactivated
				self::$errorCode = 2;
                return FALSE;

			} elseif(strpos($refill->error,"Could not authenticate") !== false) {

				self::$errorCode = 3;
                return FALSE;

			} else {

				self::$errorCode = 1;
				return FALSE;

			}
		} elseif(isset($refill->errors)) {
			if (isset($refill->errors[0]->message) && strpos($refill->errors[0]->message,"Invalid or expired token") !== false){
				self::$errorCode = 3;
                return FALSE;
			} else {
				self::$errorCode = 4;
				return FALSE;
			}

		} else {

			self::updateSelfProperties($refill);
			return TRUE;
		}
	}

	/**
	 * Loads from API, saves to local properties, saves to db, inserts a twitter history
	 *
	 * @param integer $userId id of user
	 * @param integer $debugLevel level of debug (0/1/2)
	 * @author tiger
	 */
	public static function updateUserTwitter($userId, $debugLevel=0) {
		$now = time();
		$user = new User($userId);
		if ($user->count){
			$userUpdate = new UserUpdate();
			$userUpdate->customClause = 'user='.$user->id;
			$userUpdate->load();

			$userUpdate->twitterStatus = 'q';
			$userUpdate->twitterUpdateDate = $now;
			$userUpdate->save();

			$twitterInfo = new TwitterInfo();
			$twitterInfo->customClause = 'user='.$user->id;
			$twitterInfo->load();
			if($twitterInfo->count == 0) {
				if($debugLevel > 0) {
					Tool::echoLog('User has no twitter info. userId:'.$user->id);
				}
				JLog::log('cri','User has no twitter info. userId:'.$user->id);
				return FALSE;
			}

			//print_r($twitterInfo); die();

			if($debugLevel > 0) {
				Tool::echoLog('userId:'.$user->id.' username:'.$user->username);
			}

			if(self::loadFromAPI($twitterInfo->twKey, $twitterInfo->twKeySecret, $debugLevel) === TRUE) {

				if($debugLevel > 0) {
					Tool::echoLog($twitterInfo->screenname.' load from API');
				}

				self::updateDbData($twitterInfo);

				$userUpdate->twitterStatus = 'y';
				$userUpdate->twitterTryCount = 0;

			} else {

				if($debugLevel > 0) {
					Tool::echoLog($twitterInfo->screenname.' could not load from API. Error code: ' . self::$errorCode);
				}

				//update userUpdate : marks as user update failed
				if(self::$errorCode > 0) { // all errors
					$userUpdate->twitterStatus = 'e';
				}

				if(self::$errorCode == 3){
					$userUpdate->loadUser();
					$userUpdate->user->deActivateUser();
				}

				$userUpdate->twitterTryCount++;
			}

			$userUpdate->twitterUpdateDate = $now;
			$userUpdate->save();
		}else{
			if($debugLevel > 0) {
				Tool::echoLog("There is no such user for userId:".$userId);
			}
			JLog::log('cri','There is no such user for userId:'.$userId);
			return FALSE;
		}
	}

	/**
	 * Saves self properties to db and adds a record to history
	 *
	 * @param TwitterInfo $twitterInfo
	 */
	public static function updateDbData(SnTwitter $twitterInfo) {

		//update twitter info of user
		$twitterInfo->twid			= self::$id;
		$twitterInfo->timg			= self::$pictureUrl;
		$twitterInfo->name			= self::$fullname;
		$twitterInfo->screenname	= self::$username;
		$twitterInfo->location		= self::$location;
		$twitterInfo->following		= self::$followingCount;
		$twitterInfo->follower		= self::$followerCount;
		$twitterInfo->statusCount	= self::$statusCount;
		$twitterInfo->url			= self::$url;
		$twitterInfo->description	= self::$description;
		$twitterInfo->verified		= self::$verified;
		$twitterInfo->listedCount	= self::$listedCount;
		$twitterInfo->protected		= self::$protected;
		$twitterInfo->language		= self::$language;
		$twitterInfo->save();

		//insert into twitter history
		$twitterHistory = new SnTwitterHistory();
		$twitterHistory->timg			= self::$pictureUrl;
		$twitterHistory->name			= self::$fullname;
		$twitterHistory->screenname		= self::$username;
		$twitterHistory->location		= self::$location;
		$twitterHistory->following		= self::$followingCount;
		$twitterHistory->follower		= self::$followerCount;
		$twitterHistory->statusCount	= self::$statusCount;
		$twitterHistory->url			= self::$url;
		$twitterHistory->description	= self::$description;
		$twitterHistory->verified		= self::$verified;
		$twitterHistory->listedCount	= self::$listedCount;
		$twitterHistory->protected		= self::$protected;
		$twitterHistory->language		= self::$language;
		$twitterHistory->twitterInfo	= $twitterInfo->id;
		$twitterHistory->snTwitter_snTwitterHistory	        = $twitterInfo->id;
		$twitterHistory->save();
	}

	/**
	 * Fills session variables of twitter
	 */
	public static function fillSession() {
		//$_SESSION['twInfoId'] 	= $twitterInfo->id;
		$_SESSION['tid'] 		= self::$id;
		$_SESSION['timg'] 		= self::$pictureUrl;
		$_SESSION['tname'] 		= self::$fullname;
		$_SESSION['tsname'] 	= self::$username;
		$_SESSION['tlocation'] 	= self::$location;
	}

	/**
	 * Logs twitter api request
	 *
	 * @param type $cKey
	 * @param type $cSecret
	 * @param type $uKey
	 * @param type $uSecret
	 * @param type $rUrl
	 * @return LogTwitter
	 */
	public static function logRequest($cKey, $cSecret, $uKey, $uSecret, $rUrl) {
		$request = array(
			'cKey' => $cKey,
			'cSecret' => $cSecret,
			'uKey' => $uKey,
			'uSecret' => $uSecret,
			'url' => $rUrl
		);
		$logT = new LogTwitter();
		$logT->requestDate = time();
		$logT->request = serialize($request);
		$logT->save();
		return $logT;
	}

	/**
	 * Logs Twitter api response
	 *
	 * @param LogTwitter $logT
	 * @param type $response
	 */
	public static function logResponse(LogTwitter $logT, $response) {
		$logT->response = serialize($response);
		$logT->responseDate = time();
		$logT->save();
	}

	/**
	 * Validate the given tokens with Twitter API
	 *
	 * @return object
	 * @author Murat
	 **/
	public static function validate($token,$tokenSecret)
	{
		global $__DP;

		$result = new stdClass();
		$result->code = 0;
		$result->snTwitter = null;

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		$logT = self::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $token, $tokenSecret, "Validate");
		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $token, $tokenSecret);
		self::logResponse($logT, $connection);

		if (200 == $connection->http_code) {
			$data = $connection->get('account/verify_credentials');
			if(!($data->id > 0)) {
				JLog::log('eng','Twitter status error, and 200 !' . serialize($_SERVER) . serialize($data));
				$result->code = -1;
			} else {

				$snT = new snTwitter();
				$snT->load(array("twid"=>$data->id));

				self::updateSelfProperties($data);

				//save user
				if($snT->id > 0) {
					//already registered
					//update twitter details
					$snT->twKey = $token;
					$snT->twKeySecret = $tokenSecret;
					$publisher = $snT->loadPublisher();

					$snT->resetErrorStatus();
					self::updateDbData($snT);

					$result->code = 2;
					$result->snTwitter = $snT;
				} else {
					$snT = new snTwitter();
					$snT->twKey = $token;
					$snT->twKeySecret = $tokenSecret;
					$snT->customCost = 0;
					$snT->cost = 0;
					$snT->minCost = 0;
					$snT->maxCost = 0;
					$snT->originalCost = 0;
					self::updateDbData($snT);

					$result->code = 1;
					$result->snTwitter = $snT;
				}
			}
		} else {
			JLog::log("cri","Twitter status error. SERVER: \n" . serialize($_SERVER) . "\n REQUEST : \n" . serialize($_REQUEST));
			$result->code = -1;
		}

		return $result;
	}

} // END class Twitter
