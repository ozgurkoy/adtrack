<?php
	/**
	 * company real class for Jeelet - firstly generated on 24-05-2011 11:41, add edit anyway you like wont be touched over , ever again.
	 *
	 * @package jeelet
	 * @author Özgür Köy
	 **/
	include($__DP."/site/model/base/company.php");

	class Company extends Company_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * get accounts
		 *
		 * @return array()
		 * @author Özgür Köy
		 **/
		public function getAccounts($all=true)
		{
			$br = $this->loadCompanyBankAccounts();
			$banks = array();

			do {
				if($br->id>0 && ($all||$br->active==1))
					$banks[$br->id] = array("active"=>$br->active,"bank"=>$br->bank,"branch"=>$br->branch,"accountNumber"=>$br->accountNumber,"iban"=>$br->iban);
			} while ($br->populate());

			return $banks;
		}

		public function loadBrands() {
			$cb = new CompanyBrand();
			$cb->customClause = 'company=' . $this->id;
			$cb->load();
			return $cb;
		}

		public function loadCompanyBankAccounts() {
			$cba = new CompanyBankAccount();
			$cba->customClause = 'company=' . $this->id;
			$cba->load();
			return $cba;
		}

		/**
		 * recalc and save current balance
		 *
		 * @return float
		 **/
		public function recalculateBalance() {
			JPDO::connect( JCache::read('JDSN.MDB') );
			$sql = "
				SELECT
					SUM(amount) AS TOPLAM
				FROM
					companyMoneyAction
				WHERE
					company={$this->id}
					AND state='a'
			";
			$results = JPDO::executeQuery($sql);
			if(count($results)) {
				$sum = $results[0]['TOPLAM'];
			} else {
				$sum = 0;
			}

			$this->currentBalance = $sum;
			$this->save();

			return $sum;
		}

		/**
		 * recalc users waiting balance
		 *
		 * @return float
		 **/
		public function calculatePendingBalance() {
			JPDO::connect( JCache::read('mysqlDSN.MDB') );
			$sql = "
				SELECT
					SUM(amount) AS TOPLAM
				FROM
					companyMoneyAction
				WHERE
					company={$this->id}
					AND state='p'
			";
			$results = JPDO::executeQuery($sql);
			if(count($results)) {
				$sum = $results[0]['TOPLAM'];
			} else {
				$sum = 0;
			}

			return $sum;
		}

	}
