<?php
/**
 * ip2country real class - firstly generated on 15-05-2014 19:49, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/ip2country.php';

class Ip2country extends Ip2country_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function findCountry( $ip ) {
		$long = ip2long( $ip );
		$i    = new Ip2country();
		$i->load(
			array(
				"intHi"  => "&ge " . $long,
				"intLow" => "&le " . $long
			)
		);
		$r = "unknown";
		if ( $i->gotValue ) {
			$r = $i->code;
		}
		unset( $i );

		return $r;
	}
}

