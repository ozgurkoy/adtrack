<?php
/**
 * cpaAffCampaignUser real class - firstly generated on 07-09-2014 20:55, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cpaAffCampaignUser.php';

class CpaAffCampaignUser extends CpaAffCampaignUser_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * load user by code
	 *
	 * @param $code
	 * @return $this
	 * @author Özgür Köy
	 */
	public function loadByCode( $code ) {
		$this->load( array( "code" => $code ) );
		return $this;
	}


	public function countPixel( $code, $extras ) {
		$this->loadByCode( $code );

		if($this->gotValue){
			JPDO::beginTransaction();

			try{
				$this->pixelCount = $this->pixelCount + 1;
				$this->save();

				$p          = new CpaAffCampaignPixelLog();
				$p->details = serialize( $_SERVER );
				$p->code    = $code;
				$p->extras  = $extras;
				$p->save();

				$this->savePixelLog( $p );

				unset( $p );

				JPDO::commit();
			}
			catch(JError $j){
				JPDO::rollback();

				return false;
			}

			return true;
		}

		return false;
	}

	/**
	 * get gains for the publisher
	 *
	 * @return array|bool
	 * @author Özgür Köy
	 */
	public function getGains() {
		if ( $this->gotValue !== true )
			return false;
		$g = new CpaAffGain();
		$g->groupBy(array(array("`cpaAffGain`.`status`","status")))->sum(
			"`cpaAffGain`.`amount`*(100-`cpaAffGain`.`revPerc`)/100",
			"ams",
			array(
				"gain"   => $this->id
			)
		);

		$res = array( "APPROVED" => 0, "PENDING" => 0 );

		if($g->gotValue){
			do {
				$res[ $g->status_options[$g->status] ] = $g->ams;
			} while ( $g->populate() );
		}
		unset( $g );

		return $res;
	}


	/**
	 * get lead numbers
	 *
	 * @return array|bool
	 * @author Özgür Köy
	 */
	public function getLeadStatus() {
		if ( $this->gotValue !== true )
			return false;

		$g = new CpaAffGain();
		$g->groupBy(array(array("`cpaAffGain`.`status`","statz")))->count(
			"id",
			"cnt",
			array(
				"gain"   => $this->id
			)
		);
		$res = array( "APPROVED" => 0, "PENDING" => 0 );

		if($g->gotValue){
			do {
				$res[ $g->status_options[$g->statz] ] = $g->cnt;
			} while ( $g->populate() );
		}
		unset( $g );
		return $res;
	}

	/**
	 * @param bool $forceRefresh
	 * @return int|mixed|string
	 * @author Özgür Köy
	 */
	public function  getClickCount( $forceRefresh = false ){
		$count = 0;
		if ( $this->gotValue && strlen( $this->shortUrl ) > 0 ) {
			if ( $this->clicksUpdated == 1 && $forceRefresh === false )
				return $this->clickCount;

			$c0                  = explode( "/", $this->shortUrl );
			$count               = Campaign::getHashClickCount( end( $c0 ) );
			$this->clickCount    = $count;
			$this->clicksUpdated = 1;
			$this->save();

		}

		return $count;
	}

	/**
	 * update lead amount
	 *
	 * @param array $details
	 * @return bool
	 * @author Özgür Köy
	 * @throws JError
	 */
	public function updateLeadAmount( array $details ) {
		/*
		 *  "actionId"
			"status"
			"description"
			"amount"
			"date"
		 * */
		$this->loadCpaAffCampaign();
		
		$g = new CpaAffGain();
		$g->loadByUId( $details[ "actionId" ] );

		$h = new CpaAffGainHistory();

		$newStatus      = strtolower( $details[ "status" ] ) == "closed" ? CpaAffGain::ENUM_STATUS_APPROVED : CpaAffGain::ENUM_STATUS_PENDING;
		$newAmount      = $details[ "amount" ];

		if ( $g->gotValue ) {
			$h->oldStatus = $g->status;

			if ( $g->status == $newStatus && $g->amount == $newAmount ) {
				//nothing changed.
				return false;
			}

			if ( intval( $g->status ) > intval( $newStatus ) ) {
				throw new JError( $details[ "actionId" ] . " went from approved to pending", 103 );

				return false;
			}

			if ( intval( $g->amount ) != intval( $newAmount ) ) {
				throw new JError( $details[ "actionId" ] . " amount change, old:{$g->amount} new:{$newAmount} ", 105 );

				return false;
			}
		}

		$g->status      = $newStatus;
		$g->amount      = $details[ "amount" ];
		$g->details     = json_encode( $details );
		$g->ndate       = strtotime( $details[ "date" ] );
		$g->revPerc     = $this->cpaAffCampaign_cpaAffCampaignUser->revPerc;
		$g->uid         = $details[ "actionId" ];
		$g->description = $details[ "description" ];
		$g->save();
		$this->saveGain( $g );

		$h->newStatus = $g->status;
		$h->details   = json_encode( $details );
		$h->save();

		if ( $g->status == CpaAffGain::ENUM_STATUS_APPROVED ) {
			$this->loadCpaAffCampaign()->loadCampaign();

			//create a revenue
			$r = new PublisherRevenue();
			$r->loadByCustomId( $details[ "actionId" ] );

			/*
			 * TODO : WILL WE CHECK IF THE VALID LEAD IS INVALID NOW?
			 * */
			if ( $r->gotValue === true ) {
				$this->cpaAffCampaign_cpaAffCampaignUser->writeHistory( "Revenue already payed. Check Response : " . json_encode( $details ), CpaAffCampaignHistory::ENUM_MADEBY_SYSTEM );
			}
			else {
				$r->amount                     = $g->amount * ( 100 - $this->cpaAffCampaign_cpaAffCampaignUser->revPerc ) / 100;
				$r->title                      = "Cpa.Aff {$g->uid}";
				$r->customId                   = $details[ "actionId" ];
				$r->status                     = PublisherRevenue::ENUM_STATUS_APPROVED;
				$r->approveDate                = time();
				$r->campaign_publisherRevenue  = $this->cpaAffCampaign_cpaAffCampaignUser->campaign_cpaAff->id;
				$r->publisher_publisherRevenue = $this->cpaAffCampaignUser_publisher;

				$r->save();

				$this->cpaAffCampaign_cpaAffCampaignUser->writeHistory( "Publisher Payment to $this->cpaAffCampaignUser_publisher , amount : {$r->amount}", CpaAffCampaignHistory::ENUM_MADEBY_SYSTEM );
			}
		}

		$g->saveHistory( $h );

		return true;
	}

}

