<?php

/**
 * Exceu builder class
 *
 * @author Murat
 */

class ExcelBuilder{
	/**
	 * @var PHPExcel
	 */
	public $pe = null;

	/**
	 * @var PHPExcel_Worksheet
	 */
	public $sheet = null;

	const SET_BOLD = true;

	/**
	 * Current row
	 * @var int
	 */
	public $row = 1;

	public $columnIndex = 65; // that's A
	public $maxColCount = 2;

	function __construct(){
		global $__DP;
		require_once $__DP.'site/lib/PHPExcel/PHPExcel.php';
//		error_reporting(E_ALL);
//		ini_set('display_errors', TRUE);
//		ini_set('display_startup_errors', TRUE);
		$this->pe = new PHPExcel();
		$this->pe->getDefaultStyle()->getFont()->setName('Arial')->setSize(14);
		$this->sheet = $this->pe->getActiveSheet();
	}

	/**
	 * Create header row
	 *
	 * @param string $header
	 * @return ExcelBuilder
	 * @author Murat
	 */
	public function createHeaderRow($header, $length=1 ){
		$this->sheet->setCellValue( chr($this->columnIndex).$this->row, $header);
		$this->sheet->mergeCells(chr($this->columnIndex).$this->row.":".chr($this->columnIndex+$length).$this->row);
		$this->sheet->getStyle(chr($this->columnIndex).$this->row)->getFont()->setBold(true);
		$this->sheet->getStyle(chr($this->columnIndex).$this->row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->getStyle(chr($this->columnIndex).$this->row.":".chr($this->columnIndex+$length).$this->row)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E5ECEF');
		$this->increaseRowCount();
		return $this;
	}

	/**
	 * Create sub header row
	 *
	 * @param string $c1
	 * @param string $c2
	 * @return ExcelBuilder
	 * @author Murat
	 */
	public function createSubHeaderRow($c1,$c2){
		$this->sheet->setCellValue("A{$this->row}", $c1);
		$this->sheet->getStyle("A{$this->row}")->getFont()->setBold(true);
		$this->sheet->setCellValue("B{$this->row}", $c2)->getStyle()->getFont()->setBold();
		$this->sheet->getStyle("B{$this->row}")->getFont()->setBold(true);
		$this->increaseRowCount();
	}

	/**
	 * Create normal row
	 *
	 * @param string $c1
	 * @param string $c2
	 * @return ExcelBuilder
	 * @author Murat
	 */
	public function createRow($c1,$c2){
		if (!is_array($c2)){
			$this->sheet
				->setCellValue("A{$this->row}", $c1)
				->setCellValue("B{$this->row}", $c2);
			$this->increaseRowCount();
		} else {
			$this->sheet->setCellValue("A{$this->row}", $c1);
			$this->sheet->getStyle("A{$this->row}")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->sheet->mergeCells("A{$this->row}:A" . ($this->row+count($c2)-1));
			foreach($c2 as $v){
				$this->sheet->setCellValue("B{$this->row}", $v);
				$this->increaseRowCount();
			}
		}

		return $this;
	}

	/**
	 * Create empty row
	 *
	 * @return ExcelBuilder
	 * @author Murat
	 */
	public function createEmptyRow(){
		$this->increaseRowCount();
		return $this;
	}

	/**
	 * Create special excel export for bank
	 *
	 * @return ExcelBuilder
	 */
	public function specialExcelExportForPMA($data){
		$this->sheet->setCellValue("A1", 'Amount')
			->setCellValue("B1", 'Account Owner')
			->setCellValue("B2", 'Bank')
			->setCellValue("B3", 'IBAN')
			->setCellValue("C1", 'Info')
			->setCellValue("D1", 'GSM Phone')
			->setCellValue("E1", 'Address');
		$finalColumn = "E";
		if ($data["hasTax"]){
			$this->sheet->setCellValue("F1", 'Tax');
			$finalColumn = "F";
		}
		$this->sheet->mergeCells('A1:A3');
		$this->sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->sheet->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->sheet->getStyle('B3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->sheet->mergeCells('C1:C3');
		$this->sheet->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->mergeCells('D1:D3');
		$this->sheet->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->mergeCells('E1:E3');
		$this->sheet->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		if ($data["hasTax"]){
			$this->sheet->mergeCells('F1:F3');
			$this->sheet->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		}
		$this->sheet->getStyle("A1:{$finalColumn}3")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E5ECEF');


		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		foreach($data["rows"] as $d){
			$this->row+=3;
			$this->sheet->setCellValue("A$this->row", $d["amount"])
				->setCellValue("B$this->row", $d["accountHolder"])
				->setCellValue("B".($this->row+1), $d["bank"])
				->setCellValue("B".($this->row+2), $d["account"])
				->setCellValue("C$this->row", $d["info"])
				->setCellValue("D$this->row", $d["gsm"])
				->setCellValue("E$this->row", $d["address"]);
			if ($data["hasTax"]){
				$this->sheet->setCellValue("F$this->row", $d["tax"]);
			}

			$this->sheet->mergeCells("A$this->row:A".($this->row+2));
			$this->sheet->getStyle("A$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

			$this->sheet->getStyle("B$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->sheet->getStyle("B".($this->row+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->sheet->getStyle("B".($this->row+2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->sheet->mergeCells("C$this->row:C".($this->row+2));
			$this->sheet->getStyle("C$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->sheet->mergeCells("D$this->row:D".($this->row+2));
			$this->sheet->getStyle("D$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->sheet->mergeCells("E$this->row:E".($this->row+2));
			$this->sheet->getStyle("E$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			if ($data["hasTax"]){
				$this->sheet->mergeCells("F$this->row:F".($this->row+2));
				$this->sheet->getStyle("F$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			}

			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+2))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		}
		$this->sheet->getColumnDimension('A')->setAutoSize(true);
		$this->sheet->getColumnDimension('B')->setAutoSize(true);
		$this->sheet->getColumnDimension('C')->setAutoSize(true);
		$this->sheet->getColumnDimension('D')->setAutoSize(true);
		$this->sheet->getColumnDimension('E')->setAutoSize(true);
		if ($data["hasTax"]){
			$this->sheet->getColumnDimension('F')->setAutoSize(true);
		}
	}

	/**
	 * Create special excel export for paypal
	 *
	 * @return ExcelBuilder
	 */
	public function specialPaypalExportForPMA($data){
		$this->sheet->setCellValue("A1", 'Amount')
			->setCellValue("B1", 'Account Owner')
			->setCellValue("B2", 'Paypal Account')
			->setCellValue("C1", 'Info')
			->setCellValue("D1", 'GSM Phone')
			->setCellValue("E1", 'Address');
		$finalColumn = "E";
		if ($data["hasTax"]){
			$this->sheet->setCellValue("F1", 'Tax');
			$finalColumn = "F";
		}
		$this->sheet->mergeCells('A1:A2');
		$this->sheet->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->sheet->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->sheet->mergeCells('C1:C2');
		$this->sheet->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->mergeCells('D1:D2');
		$this->sheet->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		$this->sheet->mergeCells('E1:E2');
		$this->sheet->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		if ($data["hasTax"]){
			$this->sheet->mergeCells('F1:F2');
			$this->sheet->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
		}
		$this->sheet->getStyle("A1:{$finalColumn}2")->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('E5ECEF');

		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		foreach($data["rows"] as $d){
			$this->row+=2;
			$this->sheet->setCellValue("A$this->row", $d["amount"])
				->setCellValue("B$this->row", $d["accountHolder"])
				->setCellValue("B".($this->row+1), $d["account"])
				->setCellValue("C$this->row", $d["info"])
				->setCellValue("D$this->row", $d["gsm"])
				->setCellValue("E$this->row", $d["address"]);
			if ($data["hasTax"]){
				$this->sheet->setCellValue("F$this->row", $d["tax"]);
			}

			$this->sheet->mergeCells("A$this->row:A".($this->row+1));
			$this->sheet->getStyle("A$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

			$this->sheet->getStyle("B$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->sheet->getStyle("B".($this->row+1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->sheet->mergeCells("C$this->row:C".($this->row+1));
			$this->sheet->getStyle("C$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->sheet->mergeCells("D$this->row:D".($this->row+1));
			$this->sheet->getStyle("D$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$this->sheet->mergeCells("E$this->row:E".($this->row+1));
			$this->sheet->getStyle("E$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			if ($data["hasTax"]){
				$this->sheet->mergeCells("F$this->row:F".($this->row+1));
				$this->sheet->getStyle("F$this->row")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			}

			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$this->sheet->getStyle("A$this->row:{$finalColumn}".($this->row+1))->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

		}
		$this->sheet->getColumnDimension('A')->setAutoSize(true);
		$this->sheet->getColumnDimension('B')->setAutoSize(true);
		$this->sheet->getColumnDimension('C')->setAutoSize(true);
		$this->sheet->getColumnDimension('D')->setAutoSize(true);
		$this->sheet->getColumnDimension('E')->setAutoSize(true);
		if ($data["hasTax"]){
			$this->sheet->getColumnDimension('F')->setAutoSize(true);
		}
	}

	public function addColumn( $value, $bold=false ){
		$this->sheet->setCellValue(chr($this->columnIndex).$this->row, $value);
		if($bold)
			$this->sheet->getStyle(chr($this->columnIndex).$this->row)->getFont()->setBold(true);

		$this->columnIndex++ ;

		if($this->maxColCount < $this->columnIndex)
			$this->maxColCount = $this->columnIndex;

	}

	/**
	 * Finalize excel build
	 *
	 * @return void
	 * @author Murat
	 */
	public function finalizeDoc(){
		$this->sheet->getColumnDimension('A')->setAutoSize(true);
		$this->sheet->getColumnDimension('B')->setAutoSize(true);

		for ($i=1;$i<=$this->row;$i++){
			$this->sheet->getRowDimension($i)->setRowHeight(30);
		}
	}

	/**
	 * Get binary data of excel file
	 *
	 * @return mixed
	 * @author Murat
	 */
	public function getBinary($fileName="CampaignReport"){
		global $__DP;
		$objWriter = PHPExcel_IOFactory::createWriter($this->pe, 'Excel2007');

	 	$fileName = $fileName."-".date('Ymd-His').'.xls';


		$fileLocation = $__DP.'temp/';
		 $fullPath = $fileLocation.$fileName;
		$objWriter->save($fullPath);

		$this->send_file($fullPath);
	}

	public function send_file($name) {
		ob_end_clean();
		$path = $name;
		if (!is_file($path) or connection_status()!=0) return(FALSE);
		$filename = basename($path);
		$file_extension = strtolower(substr(strrchr($filename,"."),1));
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		header("Expires: ".gmdate("D, d M Y H:i:s", mktime(date("H")+2, date("i"), date("s"), date("m"), date("d"), date("Y")))." GMT");
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
		header("Content-Type: application/octet-stream");
		header("Content-Length: ".(string)(filesize($path)));
		header("Content-Disposition: inline; filename=" . str_replace("_", " ", $filename));
		header("Content-Transfer-Encoding: binary\n");

		if ($file = fopen($path, 'rb')) {
			while(!feof($file) and (connection_status()==0)) {
				print(fread($file, 1024*8));
				flush();
			}
			fclose($file);
		}
		unlink($path);
		return((connection_status()==0) and !connection_aborted());
	}

	public function increaseRowCount() {
		$this->columnIndex = 65;  //reset column index
		$this->row++;
	}

	public function convertArrayToExcelFile($data,$fileName){
		if (is_array($data) && count($data)){
			$labels = array_keys($data[0]);
			foreach ($data as $row){
				foreach ($row as $key=>$column)
				if (!in_array($key,$labels)) $labels[] = $key;
			}
			foreach ($labels as $l){
				$this->addColumn($l,ExcelBuilder::SET_BOLD);
			}
			$this->createEmptyRow();
			foreach ($data as $row){
				foreach ($row as $column)
					$this->addColumn($column);
				$this->createEmptyRow();
			}
			$this->finalizeDoc();
			$this->getBinary($fileName);
		}
	}
}