<?php
/**
 * mailId real class - firstly generated on 19-11-2014 19:51, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/mailId.php';

class MailId extends MailId_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function mailIdExists( $mid ) {
		$m = new MailId();
		$m->load( array( "mid" => $mid ) );
		if ( $m->gotValue )
			return true;
		else
			return false;
	}

	public static function saveMailId( $mid ) {
		$m = new MailId();
		$m->mid = $mid;
		$m->save();
	}
}

