<?php
/**
 * mailHook real class - firstly generated on 02-01-2014 09:53, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/mailHook.php';

class MailHook extends MailHook_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Process mail hook message
	 *
	 * @param int $id
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public static function process($id){
		global $debugLevel;
		$mh = new MailHook($id);
		if ($mh->gotValue && $mh->state == MailHook::ENUM_STATE_QUEUED){
			$me = json_decode($mh->hookData);
			if (isset($me->event)){
				if($debugLevel > 0) Tool::echoLog("State for '{$me->_id}' is '" . $me->event . "'");
				switch($me->event){
					case "send":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_SEND);
						break;
					case "deferral":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_DEFERRED);
						break;
					case "hard_bounce":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_HARD_BOUNCE);
						UserEmail::markAsInvalid($me->msg->email);
						break;
					case "soft_bounce":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_SOFT_BOUNCE);
						break;
					case "open":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_OPENED);
						break;
					case "click":
						Mail::countClick($me->_id);
						break;
					case "spam":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_SPAM);
						UserEmail::markAsSpam($me->msg->email);
						break;
					case "unsub":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_UNSUSCRIBED);
						UserEmail::markAsUnsubscribed($me->msg->email);
						break;
					case "reject":
						Mail::changeReadState($me->_id,Mail::ENUM_READSTATE_REJECTED);
						break;
					default:
						JLog::log("cri","Unknown event occured on MailHook #" . $mh->id);
						break;
				}
				//if($debugLevel > 0) Tool::echoLog("Mail updated for '{$me->_id}'");
				$mh->event = $me->event;
				$mh->state = MailHook::ENUM_STATE_PROCESSED;
				$mh->postid = $me->_id;
				$mh->save();
				//if($debugLevel > 0) Tool::echoLog("MailHook processed for '{$me->_id}'");
			}
			unset($me);
		} else {
			if($debugLevel > 0) Tool::echoLog("Mail hook not found for #" . $id . "'");
		}
		unset($mh);
	}

}

