<?php
/**
 * cplRemoteCampaignUser real class - firstly generated on 19-06-2014 12:10, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplRemoteCampaignUser.php';

class CplRemoteCampaignUser extends CplRemoteCampaignUser_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	/**
	 * get short link for remote
	 * OUT OF DATE - REMOVE
	 *
	 * @param       $campaignCode
	 * @param       $link
	 * @param       $hash
	 * @param array $details
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function getShortLink( $campaignCode, $link, $hash, $details=array() ) {
		$link = str_replace( "[code]", $hash, $link );
		$url = Tool::callSLService(
			"shortenURL",
			array(
				"params" => json_encode(
					array(
						"url"        => Format::parametizer( $link ),
						"camp"       => $campaignCode,
						"jsRedirect" => 1,
						"details"    => json_encode( $details )
					)
				)
			)
		);

		if ( is_object($url) && isset($url->code) && is_string( $url->code ) )
			//todo@Özgür:Implement this
			return Setting::getValue( "SHORTDOMAIN" ) . $url->code;
		else
			return false;
	}

	/**
	 * load user by code
	 *
	 * @param $code
	 * @return $this
	 * @author Özgür Köy
	 */
	public function loadByCode( $code ) {
		$this->load( array( "code" => $code ) );
		return $this;
	}


	public function countPixel( $code,$extras ) {
		$this->loadByCode( $code );

		if($this->gotValue){
			JPDO::beginTransaction();
			try{
				$this->pixelCount = $this->pixelCount + 1;
				$this->save();

				$p          = new CplRemoteCampaignPixelLog();
				$p->details = serialize( $_SERVER );
				$p->code    = $code;
				$p->extras = $extras;
				$p->save();

				$this->savePixelLog( $p );

				unset( $p );

				JPDO::commit();
			}
			catch(JError $j){
				JPDO::rollback();

				return false;
			}

			return true;
		}
		
		return false;
	}

	/**
	 * get click count from sls server for remote campaign
	 *
	 * @param $campPublicId
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function getHashClickCount( $hash ) {
		$url = Tool::callSLService(
			"redirectionStats",
			array(
				"params" => json_encode(
					array(
						"hash" => $hash
					)
				)
			)
		);

		return $url[ 0 ]->total;
	}

	/**
	 * @param bool $forceRefresh
	 * @return int|mixed|string
	 * @author Özgür Köy
	 */
	public function getClickCount( $forceRefresh = false ) {
		$count = 0;
		if ( $this->gotValue && strlen( $this->shortUrl ) > 0 ) {
			if ( $this->clicksUpdated == 1 && $forceRefresh === false )
				return $this->clickCount;

			$c0                  = explode( "/", $this->shortUrl );
			$count               = Campaign::getHashClickCount( end( $c0 ) );
			$count               = CplRemoteCampaignUser::getHashClickCount( end( $c0 ) );
			$this->clickCount    = $count;
			$this->clicksUpdated = 1;
			$this->save();

		}

		return $count;
	}


	/**
	 * update lead amount of user.
	 *
	 * @param      $amount
	 * @param bool $updatePendingLeads
	 * @return int
	 * @author Özgür Köy
	 */
	public function updateLeadAmount( $amount, $updatePendingLeads=false ) {
		/*TODO :: Add transaction */

		$this->loadCplRemoteCampaignUser_publisher()->loadUser();
		$c         = $this->loadCplRemoteCampaign();
		$c->loadCampaign();
		$amount = intval( $amount );


		if($updatePendingLeads === false) {
			if ( $c->getActiveLeadCount() - $this->currentCount + $amount > $c->maxLeadLimit ) {
				return "exceeds";
			}

			$c->writeHistory(
				"User's lead count changed -
				(user: " . $this->cplRemoteCampaignUser_publisher
					->user_publisher->username . "(" . $this->cplRemoteCampaignUser_publisher->user_publisher->id . "))
				Old : " . $this->currentCount . " New : " . $amount . "
				",
				CplRemoteCampaignHistory::ENUM_MADEBY_ADMIN
			);

			$c->campaign_cplRemoteCampaign->writeHistory(
				CampaignHistory::ENUM_ACTIONID_CAMPAIGN_UPDATE,
				"User's lead count changed -
				(user: " . $this->cplRemoteCampaignUser_publisher
					->user_publisher->username . "(" . $this->cplRemoteCampaignUser_publisher->user_publisher->id . "))
				Old : " . $this->currentCount . " New : " . $amount . "
				",
				CampaignHistory::ENUM_MADEBY_ADMIN,
				null
			);
			$this->currentCount = $amount;
		}
		else
			$this->pixelCount = $amount;

		$this->lastUpdate   = time();
		$this->save();

		if($updatePendingLeads === false){
			$rev = $this->loadCplRemoteCampaignUser_publisher()
				->loadPublisher_publisherRevenue(
					array( "campaign_publisherRevenue" => $c->campaign_cplRemoteCampaign->id )
				);

			$leads = $c->getActiveLeadCount( $this->cplRemoteCampaignUser_publisher->id );


			$rev->amount = $this->cplRemoteCampaign_cplRemoteCampaignUser->perLead * $leads;
			$rev->save();

			unset( $rev );
		}

		return intval( $amount );
	}
}

