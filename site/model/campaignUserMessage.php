<?php
/**
 * campaignUserMessage real class for Jeelet - firstly generated on 07-03-2012 11:14, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include($__DP."/site/model/base/campaignUserMessage.php");

class CampaignUserMessage extends CampaignUserMessage_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null)
	{
		parent::__construct($id);
	}

	/**
	 * Load campaignUser row to reach twitterInfo id
	 *
	 * @return CampaignUser
	 * @author Murat
	 */
	public function loadCampaignUser(){
		$cu = new CampaignUser();
		$cu->user = $this->user;
		$cu->campaign = $this->campaign;
		$cu->load();
		return $cu;
	}

	/**
	 * Sends campaign tweet of user
	 *
	 * @global string $__DP
	 * @return boolean success status
	 */
	public function sendTweet() {
		global $__DP;

		$user = $this->loadUser();
		$cu = $this->loadCampaignUser();
		$cu->loadTwitterHistory();
		//$twitterInfo = $user->loadTwitter();

		$twitterInfo = $cu->twitterHistory->twitterInfo;
		$campaign = $this->loadCampaign();

		unset($cu);

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $twitterInfo->twKey, $twitterInfo->twKeySecret);
		$logT = Twitter::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $twitterInfo->twKey, $twitterInfo->twKeySecret, 'statuses/update?status='.$this->message);

		$postResult = $connection->post('statuses/update', array('status' => $this->message));
		Twitter::logResponse($logT, $postResult);

		if(isset($postResult->error)) {
			unset($user, $twitterInfo, $campaign);
			if(strpos($postResult->error, 'Could not authenticate') !== false) {
				$this->state = 'e1';
				$this->tryCount++;
				$this->save();
				return FALSE;
			} else {
				// 	Status is a duplicate.
				return TRUE;
			}
		} elseif(isset($postResult->errors)) {
			//api error
			JLog::log('twi', 'Twitter, send post error'.serialize($postResult)."|||".serialize($_SESSION));
			if (isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 89){ //Invalid or expired token
				$this->state = 'e1';
				$this->tryCount++;
				$this->save();
			} elseif (isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 187){ //Status is a duplicate
				$this->state = 'sd';
				$this->tryCount++;
				$this->save();
			} else {
				$this->state = 'e';
				$this->tryCount++;
				$this->save();
			}
			unset($user, $twitterInfo, $campaign);
			return FALSE;
		} else {
			$this->state = 's';
			$this->tryCount = 0;
			$this->tweetId = $postResult->id_str;
			$this->tweetSentDate = time();
			$this->toCheckDate = time() + (72 * 60 * 60);
			$this->trueReachOnSend = $twitterInfo->getKloutTrueReach();
			$this->save();

            //$this->message = str_replace("\n","\n<br />",$this->message);
            $this->message = str_replace("  ","&nbsp;&nbsp;",$this->message);
			if ($campaign->id != 1){
				if ($campaign->race == "c"){
					global $_S;
					$user->sendSysMessage(
						'cpcTweetSent',
						array(),
						array(
							$user->name,
							$campaign->title,
							date('d/m/Y (H:i)'),
							$_S.'myCampDetail/'.$campaign->id,
							$this->message,
							"race"=>$campaign->race
						)
					);
				} else {
					$user->sendSysMessage(
						'tweetSent',
						array(),
						array(
							$user->name,
							$campaign->title,
							date('d/m/Y (H:i)'),
							$this->message,
							"race"=>$campaign->race
						)
					);
				}
			}

			//register cronjob for tweet check after 72 hours
			$cron = new Cron();
			$cron->type = 'tc';
			$cron->state = 'w';
			$cron->targetDate = $this->toCheckDate;
			$cron->user = $user->id;
			$cron->campaign = $campaign->id;
			$cron->msgindex = $this->msgindex;
			$cron->trycount = 0;
			$cron->save();
			unset($cron);

			unset($user, $twitterInfo, $campaign);

			return TRUE;
		}
	}

	public function tweetDeleted(User $user, Campaign $campaign) {
		$this->tweetCheckDate = time();
		$this->state = 'd';
		$this->tryCount++;
		$this->save();

		if ($campaign->race == "t" && $campaign->id != 1){
			$user->sendSysMessage(
				'tweetDeleted_',
				array(),
				array(
					$user->name,
					$campaign->title,
					date('d/m/Y (H:i)', $this->tweetSentDate),
					$this->message,
					"race"=>$campaign->race
				)
			);
			
			$ur = new UserRevenue();
			$ur->customClause = 'campaign=' . $campaign->id . ' AND user=' . $user->id . ' AND msgindex=' . $this->msgindex;
			$ur->load();

			$ur->state = 'c';
			$ur->save();

			$campaign->loadOwner();
			$company = $campaign->owner->loadCompany();
			$company->recalculateBalance();

			$cma = new CompanyMoneyAction();
			$cma->campaign		= $campaign->id;
			$cma->user			= $user->id;
			$cma->paymentType	= 'r';
			$cma->load();

			if (!$cma->count()){
				$cma->amount 		= ($ur->cost*PROFIT_MULTIPLIER);
				$cma->state 		= 'a';
				$cma->info			= 'tweet deleted - user:'.$user->username;
				$cma->paymentDate	= time();
				$cma->lastActionDate= time();
				$cma->amountAfter	= $company->currentBalance + $cma->amount;
				$cma->company		= $company->id;
				$cma->receiptInfo	= $campaign->title;
				$cma->save();

				$company->recalculateBalance();
			}

			unset($ur, $cma, $company);
		}

		$user->saveHistory('Deleted Tweet (' . $this->tweetId.  ') : '.$this->message);
	}

	public function tweetNotDeleted(User $user, Campaign $campaign, $postResult) {
		$this->tweetCheckDate = time();
		$this->state = 'y';
		$this->tryCount++;
		$this->retweetCountOnCheck = $postResult->retweet_count;
		$this->mentionCountOnCheck = count($postResult->entities->user_mentions);
		$this->save();

		$cu = new CampaignUser();
		$cu->customClause = 'campaign=' . $campaign->id . ' AND user=' . $user->id;
		$cu->load();

		if ($campaign->race == "t"){
			if ($campaign->id != 1){
				$user->sendSysMessage(
					'tweetNotDeleted_',
					array(),
					array(
						$user->name,
						$campaign->title,
						date('d/m/Y (H:i)', $this->tweetCheckDate),
						$cu->cost,
						"race"=>$campaign->race
					)
				);
			}

			//approve revenue
			$ur = new UserRevenue();
			$ur->customClause = 'campaign=' . $campaign->id . ' AND user=' . $user->id . ' AND msgindex=' . $this->msgindex;
			$ur->load();

			$ur->state = 'a';
			$ur->save();
		}
		$user->saveHistory('Revenue Approved : ' . $ur->cost);

		unset($cu, $ur);
	}

	/**
	 * checks if tweet is deleted
	 *
	 * @global string $__DP
	 * @return boolean FALSE on API error
	 */
	public function checkTweet() {
		global $__DP;
		global $debugLevel;

		$user = $this->loadUser();
		$campaign = $this->loadCampaign();
		//$twitterInfo = $user->loadTwitter();
		$cu = $this->loadCampaignUser();
		$cu->loadTwitterHistory();
		$twitterInfo = $cu->twitterHistory->twitterInfo;
		unset($cu);

		require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

		$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $twitterInfo->twKey, $twitterInfo->twKeySecret);
		$logT = Twitter::logRequest(CONSUMER_KEY, CONSUMER_SECRET, $twitterInfo->twKey, $twitterInfo->twKeySecret, 'statuses/show?id='.$this->tweetId);

		$postResult = $connection->get('statuses/show', array('id' => $this->tweetId, 'include_entities' => '1'));
		Twitter::logResponse($logT, $postResult);

		if(isset($postResult->error)) {
			if(strpos($postResult->error, 'No status found') !== false) {
				//deleted
				$this->tweetDeleted($user, $campaign);
			} elseif(strpos($postResult->error, 'Not found') !== false) {
				//user deleted/deactivated
				$this->tweetDeleted($user, $campaign);
			} else if(strpos($postResult->error, 'Could not authenticate') !== false) {
				$user->deActivateUser($debugLevel); //bu aslında tweet silindi olarak da işaretliyor.
			}
		} elseif(isset($postResult->errors)) {
			if (!(isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 34) || $this->tryCount<3){
				//api error
				JLog::log('twi', 'Twitter check error'.serialize($postResult)."|||".serialize($_SESSION));
				$this->tweetCheckDate = time();
				$this->state = 'e2';
				$this->tryCount++;
				$this->save();

				//register cronjob for tweet check 1 minute later
				$cron = new Cron();
				$cron->type = 'tc';
				$cron->state = 'w';
				$cron->targetDate = (time() + 60);
				$cron->campaign = $campaign->id;
				$cron->user = $user->id;
				$cron->msgindex = $this->msgindex;
				$cron->trycount = 0;
				$cron->save();
				unset($cron);
			}else{
				//user deleted/deactivated
				$this->tweetDeleted($user, $campaign);
			}
		} else {
			//not deleted
			$this->tweetNotDeleted($user, $campaign, $postResult);
		}
		unset($user, $campaign, $twitterInfo);
	}
	
	/**
	 * Get link stats from adm.ms service
	 * 
	 * @return void
	 * @author Murat
	 */
	public function getLinkStats($returnData=true){
		$ucs = new UserClickStat();
		$ucs->linkHash = $this->linkHash;
		$ucs->orderBy = "clickDate DESC";
		$ucs->limit = 1;
		$ucs->load();
		
		$refData = (isset($_SESSION["SLS_REFDATA_".$this->linkHash]) ? $_SESSION["SLS_REFDATA_".$this->linkHash] : null);
		//$countryData = (isset($_SESSION["SLS_COUNTRYDATA_".$this->linkHash]) ? $_SESSION["SLS_COUNTRYDATA_".$this->linkHash] : null);
		$rData = array("refData"=>null,"countryData"=>null);
		
		//if (!$returnData || !$ucs->count || (time() - $ucs->lastUpdate) >= 3 || is_null($refData) || is_null($countryData)){
		if (!$returnData || !$ucs->count || (time() - $ucs->lastUpdate) >= 3 || is_null($refData)){
			$data = Tool::callSLService("hashStats",array(
				"params"=>  json_encode(array(
					"linkHash"=>$this->linkHash,
					"lastUpdate"=>($ucs->count ? strtotime(date("Y-m-d",$ucs->lastUpdate) . " 00:00:00") : null)
				))
			));

			if (!JUtil::isProduction()){
				$params = Tool::getSearchParameter();
				if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
					Tool::echoLog("hashStats : " . print_r($data, true));
				}
			}
			//die(print_r($data));
			
			if (!is_null($data)){
				UserClickStat::updateLinkStats($data);
			}

			$error = null;
			if (isset($data->error)){
				$error = true;
			}
			
			if ($returnData){
				//Referrer data

				$data = Tool::callSLService("linkHashRefStats",array(
					"params"=>  json_encode(array(
						"linkHash"=>$this->linkHash
					))
				));

				if (!JUtil::isProduction()){
					$params = JUtil::getSearchParameter();
					if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
						Tool::echoLog("linkHashRefStats : " . print_r($data, true));
					}
				}

				/*
				$data = array(
					(object)array("_id"=>"http://www.google.com/murat", "count"=>rand(100,200)),
					(object)array("_id"=>"http://www.google.com.tr/arman", "count"=>rand(100,200)),
					(object)array("_id"=>"http://www.google.co.uk/ozgur", "count"=>rand(100,200)),
					(object)array("_id"=>"http://www.google.co.il/sam", "count"=>rand(100,200))
				);
				$data = (object)$data;
				*/
				//die(print_r($data,true));

				$rData["refData"] = Format::parseReferrer($data);
				//die(print_r($rData["refData"],true));
				$_SESSION["SLS_REFDATA_".$this->linkHash] = $rData["refData"];

				/*
				//Country Data

				$data = JUTil::callSLService("linkHashCountryStats",array(
					"params"=>  json_encode(array(
						"linkHash"=>$this->linkHash
					))
				));

				if (!JUtil::isProduction()){
					$params = JUtil::getSearchParameter();
					if (isset($params["isDebug"]) &&  $params["isDebug"] == "adMingle"){
						Tool::echoLog("linkHashCountryStats : " . print_r($data, true));
					}
				}

//				$data = array(
//					(object)array("_id"=>"TR", "count"=>rand(100,200)),
//					(object)array("_id"=>"GB", "count"=>rand(100,200)),
//					(object)array("_id"=>"DE", "count"=>rand(100,200)),
//					(object)array("_id"=>"US", "count"=>rand(100,200))
//				);
				//die(print_r($data,true));

				$result = array();

				if (is_array($data) && count($data)){
					foreach ($data as $item){
						$result[$item->_id] = $item->count;
					}
				}

				if(!count($result)) {
					$result[DEFAULT_LANGUAGE] = 1;
				}

				$rData["countryData"] = $result;
				$_SESSION["SLS_COUNTRYDATA_".$this->linkHash] = $rData["countryData"];
				unset($data,$refData,$countryData,$result);
				*/
				unset($data,$refData);
			} else {
				unset($data);
				if ($error)
					return false;
				else
					return true;
			}
		} else {
			$rData["refData"] = $_SESSION["SLS_REFDATA_".$this->linkHash];
			//$rData["countryData"] = $_SESSION["SLS_COUNTRYDATA_".$this->linkHash];
		}
		
		return $rData;
	}

}
