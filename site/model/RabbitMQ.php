<?php

require_once $__DP.(defined("DOCUMENT_ROOT_PROD")?DOCUMENT_ROOT_PROD:"").'site/cron/php-amqplib/amqp.inc';

class RabbitMQ{
	/**
	 * Connection
	 * @var AMQPConnection
	 */
	public $cn;

	/**
	 * Channel
	 * @var object
	 */
	public $ch;

	/**
	 * Class constructor
	 */
	function __construct(){
		$this->cn = new AMQPConnection(RMQ_HOST, RMQ_PORT, RMQ_USER, RMQ_PASS, RMQ_VHOST);
		$this->ch = $this->cn->channel();
	}

	/**
	 * Create channel
	 */
	public function declareQueue($queName){
		/*
			name: $queue
			passive: false
			durable: true // the queue will survive server restarts
			exclusive: false // the queue can be accessed in other channels
			auto_delete: false //the queue won't be deleted once the channel is closed.
		*/
		$this->ch->queue_declare($queName, false, true, false, false);
	}

	/**
	 * Publish message
	 */
	public function publishMessage($msg,$queName){
		$msg = new AMQPMessage($msg, array('content_type' => 'text/plain', 'delivery_mode' => 2));
		$this->ch->basic_publish($msg, RMQ_EXCHANGE, $queName);
	}

	/**
	 * Create new RabbitMQ message
	 *
	 * @return AMQPMessage
	 */
	public function createNewMessage($msg){
		return new AMQPMessage($msg, array('content_type' => 'text/plain', 'delivery_mode' => 2));
	}

	/**
	 * Close connection and channel
	 */
	public function close(){
		$this->ch->close();
		$this->cn->close();
	}
}