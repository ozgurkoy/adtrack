<?php
class Processmanager {
	public $executable       = "/opt/local/bin/php";	//the system command to call
	public $root             = "";		//the root path
	public $processes        = 3;		//max concurrent processes
	public $sleep_time       = 2;		//time between processes
	public $show_output      = false;	//where to show the output or not

	private $running          = array();//the list of scripts currently running
	private $scripts          = array();//the list of scripts - populated by addScript
	private $processesRunning = 0;		//count of processes running

	function addScript($script,  $max_execution_time = 300, $code = null)
	{
		$this->scripts[] = array("script_name" => $script, "max_execution_time" => $max_execution_time, "code"=>$code);
	}

	public static function complete( $code, $pid, $killPid = null, $killProcess = false ) {
		global $__DP;
		if(is_file($__DP . 'site/backend/proc/active/' . $code))
			@rename( $__DP . 'site/backend/proc/active/' . $code, $__DP . 'site/backend/proc/dead/' . $code );
		if(is_file($__DP . 'site/backend/proc/pid/active/' . $pid))
			@rename( $__DP . 'site/backend/proc/pid/active/' . $pid, $__DP . 'site/backend/proc/pid/dead/' . $pid );

		if ( $killProcess === true ) {
			self::kill( $pid );
		}

		if ( !is_null( $killPid ) && $killPid > 0 )
			self::kill( $killPid );
	}



	public static function kill($pid) {
		if(self::pidExists($pid))
			exec("kill -9 $pid");
	}

	// is still running?
	public static function isRunning( $pid )
	{
		return self::pidExists( $pid );
		//$status = proc_get_status($this->resource);
		//return $status["running"];
	}

	public static function pidExists( $pid ) {
		exec( "ps ax | grep $pid 2>&1", $output );
		while ( list( , $row ) = each( $output ) ) {
			$row_array = explode( " ", $row );
			$check_pid = $row_array[ 0 ];
			if ( $pid == $check_pid ) {
				return true;
			}
		}

		return false;
	}

	function exec()
	{
		global $__DP;
		$i = 0;
		$pids = array();

		while ( ( $this->processesRunning < $this->processes ) and ( $i < count( $this->scripts ) ) ) {
			if ( $this->show_output ) {
				ob_start();
				echo "<span style='color: orange;'>Adding script: " . $this->scripts[ $i ][ "script_name" ] . "</span><br />";
				ob_flush();
				flush();
			}
			$p = new Process(
				$this->executable,
				$this->root,
				$this->scripts[ $i ][ "script_name" ],
				$this->scripts[ $i ][ "max_execution_time" ],
				$this->scripts[ $i ][ "code" ]
			);


			ob_end_flush();

			ob_start();

			$index                   = sizeof( $this->running );
			$this->running[ $index ] = $p;
			$pids[ ] = array($p->pid, $p->killPid);
			$this->processesRunning++;
			$i++;
		}

		return $pids;
	}


}

class Process {
	public $resource;
	public $pipes;
	public $script;
	public $max_execution_time;
	public $start_time;
	public $code;
	public $pid ;
	public $killPid ;


	function __construct( &$executable, &$root, $script, $max_execution_time, $code = null ) {
		global $__DP;
		$this->code               = is_null( $code ) ? JUtil::randomString() : $code;
		$this->script             = $script;
		$this->max_execution_time = $max_execution_time;

		$command = $executable . " " . $root . $this->script . PHP_EOL . PHP_EOL . "/*END*/" . PHP_EOL . PHP_EOL;

		//prepare the file before
		file_put_contents( $__DP . 'site/backend/proc/active/' . $this->code,
			$command
		);

		$this->pid      = exec( $executable . ' ' . $root . $this->script . ' >> ' . $__DP . 'site/backend/proc/active/' . $this->code . '& echo $!' );

		//send the assasin
		$this->killPid = exec( $executable . ' ' . $__DP . 'site/backend/lib/_kill.php ' . $__DP . ' ' . $this->max_execution_time . ' ' . $this->pid . ' ' . $this->code . '  > /dev/null & echo $!' );
		$this->createPid( $this->pid, $this->code );
		$this->start_time = time();

		return $this;
	}





	public function getPid( ){
		return $this->pid;
		//$status = proc_get_status($this->resource);
		//return $status[ "pid" ];
	}

	public function getOutput( ){
		return stream_get_contents($this->pipes[1]);
	}

	// long execution time, proccess is going to be killer
	function isOverExecuted()
	{
		if ($this->start_time+$this->max_execution_time<time()) {
			echo 666;
			return true;}
		else return false;
	}

	public function createPid( $pid, $contents ) {
		global $__DP;
		file_put_contents( $__DP . "site/backend/proc/pid/active/$pid", $contents );
	}
}