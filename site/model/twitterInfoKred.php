<?php
/**
 * twitterInfoKred real class - firstly generated on 27-10-2012 20:09, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/twitterInfoKred.php';

class TwitterInfoKred extends TwitterInfoKred_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * update publishers kred information
	 * 
	 * @return void
	 * @author murat
	 */
	public function updateUserKredInformation($userId,$potentialReach,$realReach, $datas = null){
		global $debugLevel;
		$this->user = $userId;
		$this->load();
		if($this->count == 0) {
			$twitterInfo = new TwitterInfo();
			$twitterInfo->user = $userId;
			$twitterInfo->load();
			$this->user = $userId;
			$this->twitterInfo = $twitterInfo->id;
			$this->save();
			unset($twitterInfo);
			if($debugLevel > 0) {
				Tool::echoLog('Added twitterInfoKred record');
			}
		}

		$this->potentialReach7 = $potentialReach[7];
		$this->potentialReach30 = $potentialReach[30];
		$this->potentialReach60 = $potentialReach[60];
		$this->realReach7 = $realReach[7];
		$this->realReach30 = $realReach[30];
		$this->realReach60 = $realReach[60];
		$this->save();
		
		//insert kred history
		$history = new KredHistory();
		$history->user = $userId;
		$history->twitterInfoKred = $this->id;
		$history->potentialReach7 = $potentialReach[7];
		$history->potentialReach30 = $potentialReach[30];
		$history->potentialReach60 = $potentialReach[60];
		$history->realReach7 = $realReach[7];
		$history->realReach30 = $realReach[30];
		$history->realReach60 = $realReach[60];
		$history->save();
		
		if (!is_null($datas)){
			foreach ($datas as $data) {
				$krv = new KredReachValues();
				$krv->kredHistory = $history->id;
				$krv->reachDate = $data[0];
				$krv->potentialReach = $data[1]->potential;
				$krv->realReach = $data[1]->real;
				$krv->save();
				unset($krv);
			}
		}
	}

}

