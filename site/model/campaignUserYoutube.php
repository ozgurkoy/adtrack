<?php
/**
 * campaignUserYoutube real class - firstly generated on 16-10-2012 14:54, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignUserYoutube.php';

class CampaignUserYoutube extends CampaignUserYoutube_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}
	
	/**
	 * create preview video
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function createPreview()
	{
		$this->loadYoutubeVideo();
		$pname = "/tmp/".JUtil::randomString(10).".mp4";

		exec("ffmpeg -i ".$this->youtubeVideo[0]["path"].$this->youtubeVideo[0]["filename"]." -vcodec libx264 -vpre ipod640 -b 250k -bt 50k -vpre hq  -acodec libfaac -ab 56k -ac 2 -s 480x320 $pname");
		
		if(Youtube::isVideo($pname)){
			JDoc::addToLibrary(array("path"=>$pname,"type"=>"video/mp4"),$this->id,"campaignUserYoutube","previewVideo");
			$this->previewed = 1;
			$this->save();
			
			JLog::log("youtube","Converted : ".$this->youtubeVideo[0]["path"].$this->youtubeVideo[0]["filename"]);
		}
		else{
			JLog::log("youtube","Could not convert : ".$this->youtubeVideo[0]["path"].$this->youtubeVideo[0]["filename"]);
		}
			
		
	}
	
	/**
	 * lock upload time
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function lockUpload()
	{
		$this->uploadLock = date("Y-m-d H:i:s",time());
		$this->save();
	}
	
	/**
	 * push to youtube
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function pushToYoutube()
	{
		$this->lockUpload();

		$camp 	= $this->loadCampaignUser()->loadCampaign();
		$yc 	= $camp->loadYoutube();
		
		$this->loadYoutubeVideo();
		$this->loadUser()->loadYoutube();



		$uploadInfo = $this->user->youtube->uploadVideo($this->youtubeVideo[0]["path"].$this->youtubeVideo[0]["filename"],$yc->title,$yc->description,explode(",",$yc->keywords));

		if(!isset($uploadInfo["error"])){
			$this->url 		= $uploadInfo["url"];
			$this->videoId 	= $uploadInfo["id"];
			$this->isSent 	= 1;
			$this->approval = "g";
			$this->sentDate = date("Y-m-d H:i",time());
			$this->url 		= $uploadInfo["url"];
			$this->videoId 	= $uploadInfo["id"];

			$this->save();

			JLog::log("youtube","Auto Upload of video of campaign:{$camp->title} of user:{$this->user->email}-SUCCESS");
		}
		else{
			JLog::log("youtube","Could not upload video of campaign:{$camp->title} of user:{$this->user->email} Error : ".$uploadInfo["error"]);
		}
		
	}
	
}

