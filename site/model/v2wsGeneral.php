<?php

use Swagger\Annotations\Operation;
use Swagger\Annotations\Operations;
use Swagger\Annotations\Parameter;
use Swagger\Annotations\Parameters;
use Swagger\Annotations\Api;
use Swagger\Annotations\ErrorResponse;
use Swagger\Annotations\ErrorResponses;
use Swagger\Annotations\Resource;
use Swagger\Annotations\AllowableValues;

use Swagger\Annotations\Properties;
use Swagger\Annotations\Property;
use Swagger\Annotations\Model;
use Swagger\Annotations\Items;


/**
 * @package
 * @category
 * @subpackage
 *
 * @Resource(
 *  apiVersion="0.1",
 *  swaggerVersion="1.1",
 *  basePath="http://n.admingle.com/wsv2/general",
 *  resourcePath="/general"
 * )
 *
 */
include($__DP . "/site/model/wsObjects.php");

class v2wsGeneral {
	protected $base;

	function __construct() {
		$this->base = v2wscore::getInstance();
	}

        
    /**
       * Get All Static Data
       * 
       * @return json
       * @author sam
       *
       * @Api(
       *   path="/staticData/{requstedlist}",
       *   description="Timestamp, country , bank , city, interests",
       *   @operations(
       *     @operation(
       *       httpMethod="GET",
       *       summary="return json of data list",
       *       responseClass="StaticDataResponse",
       *       notes="each list has timestamp for last update",
       *       nickname="staticData",
       *       @parameters(
       *         @parameter(
       *           name="requested list",
       *           description="values list separate by ,",
       *           paramType="path",
       *           required="false",
       *           allowMultiple="false",
       *           dataType="string",
       *           defaultValue="1,bank,city,interests,lang"
       *         )
       *       ),
       *       @errorResponses(
       *          @errorResponse(
       *            code="404",
       *            reason="Lang Not Found"
       *          )
       *       )
       *     )
       *   )
       * )
       */
	public function staticData($parameter){
        // $parameter
		$Data = explode(',',$parameter);
		$staticDataResponse = new StaticDataResponse();

		$timestamp = null;
		foreach($Data as $item)
		{
			if(is_numeric($item))
			{
				$timestamp = $item;
			}else{
				switch($item){
					case "bank":
						$staticDataResponse->bank =  $this->getAllBanks($timestamp);
						break;
					case "interests":
						$staticDataResponse->interests =  $this->getAllInterests($timestamp);
						break;
					case "city":
						$staticDataResponse->city =  $this->getAllCities($timestamp);
						break;
				/*	case "education":
						$staticDataResponse->education =  $this->getAllEducations($timestamp);
						break;
					case "professional":
						$staticDataResponse->professional =  $this->getAllProfessionals($timestamp);
						break;
				*/	case "lang":
						$staticDataResponse->languages =  $this->getAllLanguages($timestamp);
						break;
					case "settings":
						$staticDataResponse->settings =  $this->getCountrySettings();
						break;
				}
			}
		}

		$staticDataResponse->timestamp = time();
		$this->base->sendResponse(200,$staticDataResponse->getJsonData());

	}

	/**
	 * Set All countries by timestamp
	 * @param $timestamp
	 * @return array
	 */
	private function getAllLanguages($timestamp)
	{
		$lang = new Lang();
		$lang->populateOnce(false);

		$filter["active"] = 1;
		if($timestamp !=null)
		{
			$filter["jdate"] = '&gt '.$timestamp;

		}

		$lang->nopop()->load($filter);

		$arrCountry = array();

		while ($lang->populate()) {

			$tempCountry = new LanguagesResponse();
			$tempCountry->shortCode = $lang->langShort;
			$tempCountry->shortCodeIso = $lang->langShortDef;
			$tempCountry->displayName = $lang->langName;
			$tempCountry->default = ($lang->langShort == Setting::getValue("DEFAULT_LANGUAGE"));

			$arrCountry[] = $tempCountry->getJsonData();
		}

		return $arrCountry;
	}

	/**
	 * Set All countries by timestamp
	 * @param $timestamp
	 * @return array
	 */
	private function getAllCountries($timestamp)
	{
		$country = new country();
		$country->populateOnce(false);
		if($timestamp !=null)
		{
			$filter["jdate"] = '&gt '.$timestamp;
			$country->nopop()->load($filter);
		}else
		{
			$country->nopop()->load();
		}

		$arrCountry = array();

		while ($country->populate()) {

			$tempCountry = new CountryResponse();
			$tempCountry->iso = $country->iso;
			$tempCountry->id = $country->id;
			$tempCountry->displayName = $country->name;
			$tempCountry->active = $country->active;
			$tempCountry->inviteMode = empty($country->url);

			$arrCountry[] = $tempCountry->getJsonData();
		}

		return $arrCountry;
	}

	/**
	 * Get all settings for that country
	 * @return array
	 */
	private function getCountrySettings()
	{
		$arrSettings = array();
		// social networks
		//FB_LOGIN , VK_LOGIN , XING_LOGIN

		// Trend limit
		$trendLimit = new siteSettingsResponse();
		$trendLimit->key = "trendLimit";
		$trendLimit->value =Setting::getValue("limitPublisherTrend");
		$arrSettings[] = $trendLimit->getJsonData();

		// Phone number validation
		$phonenumber = new siteSettingsResponse();
		$phonenumber->key = "PhoneLimit";
		$phonenumber->value =Setting::getValue("PHONE_MASK");
		$arrSettings[] = $phonenumber->getJsonData();

		// sharing options SHARE_OPTIONS

		$share = new siteSettingsResponse();
		$share->key = "SHARE_OPTIONS";
		$share->value =Setting::getValue("SHARE_OPTIONS");
		$arrSettings[] = $share->getJsonData();











		return $arrSettings;
	}

	/**
	 * Get all Banks by timestamp
	 * @param $timestamp
	 * @return array
	 */
	private function getAllBanks($timestamp)
	{

		$bank = new Bank();
		$bank->populateOnce(false);
		$filter["country"] = COUNTRY_CODE;
		if($timestamp !=null)
		{
			$filter["jdate"] = '&gt '.$timestamp;

		}
		$bank->nopop()->load($filter);
		$arrBank = array();

		while ($bank->populate()) {

			$tempBank = new BankResponse();
			$tempBank->id = $bank->id;
			$tempBank->displayName = $bank->bankName;
			$tempBank->active = $bank->active;


			$arrBank[] = $tempBank->getJsonData();
		}

		return $arrBank;
	}

	/**
	 * Get All Interests by Timestamp
	 * @param $timestamp
	 * @return array
	 */
	private function getAllInterests($timestamp)
	{
		$trend = new trend();
		$trend->populateOnce(false);
		if($timestamp !=null)
		{
			$filter["jdate"] = '&gt '.$timestamp;
			$trend->nopop()->load($filter);
		}else{
			$trend->nopop()->load();
		}

		$arrTrend = array();
		$langShort = Setting::getValue("DEFAULT_LANGUAGE");
		$trendWords =  $trend->getAllWithLang($langShort,true);

		while ($trend->populate()) {

			$tempTrend = new InterestsResponse();
			$tempTrend->id = $trend->id;
			$tempTrend->displayName = isset($trendWords[$trend->id])?$trendWords[$trend->id]:$trend->title;
			$tempTrend->active = $trend->active;

			$arrTrend[] = $tempTrend->getJsonData();
		}

		return $arrTrend;
	}

	/**
	 * Get All Education options by Timestamp
	 * @param $timestamp
	 * @return array
	 */
	private function getAllEducations($timestamp)
	{
		$education = new Education();
		$education->populateOnce(false);
		if($timestamp !=null)
		{
			$filter["jdate"] = '&gt '.$timestamp;
			$education->nopop()->load($filter);
		}else{
			$education->nopop()->load();
		}

		$arrTrend = array();

		while ($education->populate()) {

			$tempTrend = new EducationResponse();
			$tempTrend->id = $education->id;
			$tempTrend->displayName = $education->title;
			$tempTrend->active = $education->active;

			$arrTrend[] = $tempTrend->getJsonData();
		}

		return $arrTrend;
	}

	/**
	 * Get All Interests by Timestamp
	 * @param $timestamp
	 * @return array
	 */
	private function getAllProfessionals($timestamp)
	{
		$professional = new Professional();
		$professional->populateOnce(false);
		if($timestamp !=null)
		{
			$filter["jdate"] = '&gt '.$timestamp;
			$professional->nopop()->load($filter);
		}else{
			$professional->nopop()->load();
		}

		$arrTrend = array();

		while ($professional->populate()) {

			$tempTrend = new ProfessionalResponse();
			$tempTrend->id = $professional->id;
			$tempTrend->displayName = $professional->title;
			$tempTrend->active = $professional->active;

			$arrTrend[] = $tempTrend->getJsonData();
		}

		return $arrTrend;
	}

	/**
	 * Set All cities by timestamp
	 * @param $timestamp
	 * @return array
	 */
	private function getAllCities($timestamp)
	{
		$city = new city();
		$city->populateOnce(false);
		if($timestamp !=null)
		{
			$filter["jdate"] = '&gt '.$timestamp;
		}
		$filter["country_city"] = COUNTRY_CODE;
		$filter["active"] = 1;

		$city->nopop()->orderBy("cityName")->load($filter);

		$arrcity = array();

		while ($city->populate()) {

			$tempcity = new cityResponse();
			$tempcity->id = $city->id;
			$tempcity->displayName = $city->cityName;
			$tempcity->country = $city->country_city;
			$tempcity->active = $city->active;

			$arrcity[] = $tempcity->getJsonData();
		}


		return $arrcity;
	}

	/**
	 * country List for API
	 *
	 * @return json
	 * @author sam
	 *
	 * @Api(
	 *   path="/country/{timestamp}",
	 *   description="Get Countries that has API on invite mode",
	 *   @operations(
	 *     @operation(
	 *       httpMethod="GET",
	 *       summary="return json of data list",
	 *       responseClass="CountriesResponse",
	 *       notes="each list has timestamp for last update",
	 *       nickname="country",
	 *       @parameters(
	 *         @parameter(
	 *           name="timestamp",
	 *           description="lastUpdateTime",
	 *           paramType="path",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string",
	 *           defaultValue=""
	 *         )
	 *       ),
	 *       @errorResponses(
	 *          @errorResponse(
	 *            code="500",
	 *            reason="server Error"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function countryList($lastupdate){

		global $_S;

		$arrCountry = new CountriesResponse();

		$arrCountry->countries = array();

		$country = new Country();
		$country->populateOnce(false);

		$filter = array("active"=>"1");

		if(is_numeric($lastupdate))
		{
			$filter["jdate"] = '&gt '.$lastupdate;
		}


		$country->nopop()->load($filter);


		while ($country->populate()) {

			$tempCountry = new ActiveCountryResponse();
			$tempCountry->iso = $country->iso;
			$tempCountry->id = $country->id;
			$tempCountry->countryName = $country->name;
			$tempCountry->countryUrl = $country->url;
			$tempCountry->inviteMode = $country->invite;
			$tempCountry->phone = $country->countryCode;
			$tempCountry->oauth = $country->oauth;
			//$tempCountry->flagImg = $_S."site/layout/images/Flags/".$country->shortName.".png";

			if($arrCountry->timestamp < $country->jdate)
			{
				$arrCountry->timestamp = $country->jdate;
			}

			$arrCountry->countries[] = array($tempCountry->getJsonData());

		}

		$this->base->sendResponse(200, $arrCountry->getJsonData());
	}



       /**
       * Get Language File for application
       * 
       * @return json
       * @author sam
       *
       * @Api(
       *   path="/langFile/{v}",
       *   description="Get or update Language file for all texts",
       *   @operations(
       *     @operation(
       *       httpMethod="GET",
       *       summary="return new words",
       *       notes="Returns HTML with the terms",
       *       nickname="getUserTerms",
       *       @parameters(
       *         @parameter(
       *           name="v",
       *           description="values list seperate by ,",
       *           paramType="path",
       *           required="false",
       *           allowMultiple="false",
       *           dataType="string",
       *           defaultValue="Country,Bank,City,Intrests"
       *         )
       *       ),
       *       @errorResponses(
       *          @errorResponse(
       *            code="404",
       *            reason="Lang Not Found"
       *          )
       *       )
       *     )
       *   )
       * )
       */
	public function langFile($parameter){
        // $parameter - response type - json / html / mobile

		//Lang::switchLang($parameter);
		$this->base->sendResponse(200);
	}
        
        
    /**
     * Publisher signup form terms (response type is html)
     * 
     * @return string
     * @author Murat
     *
     * @Api(
     *   path="/terms",
     *   description="Operations about terms",
     *   @operations(
     *     @operation(
     *       httpMethod="GET",
     *       summary="Publisher Terms on signup",
     *       notes="Returns HTML with the terms",
     *       nickname="getUserTerms",
     *       @parameters(
     *         @parameter(
     *           name="lang",
     *           description="Terms and Conditions",
     *           paramType="path",
     *           required="true",
     *           allowMultiple="false",
     *           dataType="string",
     *           defaultValue="EN"
     *         )
     *       ),
     *       @errorResponses(
     *          @errorResponse(
     *            code="404",
     *            reason="Lang Not Found"
     *          )
     *       )
     *     )
     *   )
     * )
     */
	public function pubSignUpTerms($parameter){
        // $parameter - response type - json / html / mobile
		if (strlen($parameter) == 2) $parameter = strtoupper ($parameter);
			Lang::switchLang($parameter);
		JT::init();
		JT::assign("lang",$parameter);
		
		$this->base->sendResponse(200, "hello");
	}

        
        /**
         * User policy
         * 
         * @Api(
         *   path="/policy",
         *   description="Operations User Prolicy",
         *   @operations(
         *     @operation(
         *       httpMethod="GET",
         *       summary="Get user Policy",
         *       notes="Returns HTML with the terms",
         *       nickname="getUserTerms",
         *       @parameters(
         *         @parameter(
         *           name="lang",
         *           description="Get User Policy",
         *           paramType="path",
         *           required="true",
         *           allowMultiple="false",
         *           dataType="string",
         *           defaultValue="EN"
         *         )
         *       ),
         *       @errorResponses(
         *          @errorResponse(
         *            code="404",
         *            reason="Lang Not Found"
         *          )
         *       )
         *     )
         *   )
         * )
         */
        public function UserPolicy($parameter){
	        //if (strlen($parameter) == 2) $parameter = strtoupper ($parameter);
	        //Lang::switchLang($parameter);
	        echo Policy::getPolicy("publisheragrement",Setting::getValue("DEFAULT_LANGUAGE"));
        }
        
        /**
         * Publisher signup form terms (response type is html)
         * 
         * @Api(
         *   path="/cpcCampaign",
         *   description="Operations User Prolicy",
         *   @operations(
         *     @operation(
         *       httpMethod="GET",
         *       summary="Get cpcCampaign terms",
         *       notes="Returns HTML with the terms",
         *       nickname="getUserTerms",
         *       @parameters(
         *         @parameter(
         *           name="lang",
         *           description="Get User CPC Terms",
         *           paramType="path",
         *           required="true",
         *           allowMultiple="false",
         *           dataType="string",
         *           defaultValue="EN"
         *         )
         *       ),
         *       @errorResponses(
         *          @errorResponse(
         *            code="404",
         *            reason="Lang Not Found"
         *          )
         *       )
         *     )
         *   )
         * )
         */
	public function cpcCampaign($parameter){
		if (strlen($parameter) == 2) $parameter = strtoupper ($parameter);
		Lang::switchLang($parameter);
		JT::init();
		JT::assign("lang",$parameter);
		echo JT::pfetch("policy_privacyandpolicy");
	}
        
        /**
         * Publisher signup form terms (response type is html)
         * 
         * @Api(
         *   path="/awarnessCampaign",
         *   description="Operations User Prolicy",
         *   @operations(
         *     @operation(
         *       httpMethod="GET",
         *       summary="Get awarness Campaign terms",
         *       notes="Returns HTML with the terms",
         *       nickname="getUserTerms",
         *       @parameters(
         *         @parameter(
         *           name="lang",
         *           description="Get User awarness Campaign Terms",
         *           paramType="path",
         *           required="true",
         *           allowMultiple="false",
         *           dataType="string",
         *           defaultValue="EN"
         *         )
         *       ),
         *       @errorResponses(
         *          @errorResponse(
         *            code="404",
         *            reason="Lang Not Found"
         *          )
         *       )
         *     )
         *   )
         * )
         */
	public function awrCampaign($parameter){
		if (strlen($parameter) == 2) $parameter = strtoupper ($parameter);
		Lang::switchLang($parameter);

		JT::init();
		JT::assign("lang",$parameter);
		echo JT::pfetch("policy_UK_usePolicyUK");
	}

	public function availableSocialNetwork(){
		$s = array(
			"twitter" => Setting::getValue("TWITTER_LOGIN") ? true : false,
			"facebook" => Setting::getValue("FB_LOGIN") ? true : false,
			"google" => Setting::getValue("GOOGLE_LOGIN") ? true : false,
			"vk" => Setting::getValue("VK_LOGIN") ? true : false
		);
		$this->base->sendResponse(200, json_encode($s));
	}
}
?>