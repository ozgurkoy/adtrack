<?php
/**
 * kred class
 *
 * @package default
 * @author murat
 **/
class Kred
{
	/**
	 * kred object
	 *
	 * @var string
	 **/
	public static $kobj = NULL;
	
	/**
	 * request result status
	 * 
	 * @var string
	 */
	public static $status;
	
	/**
	 * request result data for given screenname
	 * 
	 * @var string
	 */
	public static $data;

	/**
	 * processed response error code
	 * 0: no error
	 * 1: api returned error
	 * 2: could not parse response
	 * 3: no response
	 * 4: waiting from kred
	 * 5: invalid status returned from kred
	 * @var int
	 **/
	public static $errorCode=0;

	/**
	 * get kred data for requested twitter username
	 *
	 * @return void
	 * @author murat
	 **/
	public static function loadFromAPI($username, $debugLevel=0)
	{
		self::$errorCode		= 0;
		self::$kobj 			= null;
		
		$apiUrl	 		= "http://api.peoplebrowsr.com/reach?last=yesterday&count=60&source=twitter&term=$username&app_id=" . KRED_API_ID . "&app_key=" . KRED_API_KEY;
		$logK = Kred::logRequest($apiUrl);

		$apiResponse 	= JUtil::fetchPage($apiUrl);
		Kred::logResponse($logK, $apiResponse);

		if($debugLevel > 0) {
			//echo 'API URL:'.$apiUrl.PHP_EOL;
			//echo 'Response string:'.$apiResponse.PHP_EOL;
		}

		if(strlen($apiResponse) > 1) {
			self::$kobj = json_decode($apiResponse);

			if($debugLevel > 1) {
				echo 'Response object:'.PHP_EOL;
				print_r(self::$kobj);
				echo PHP_EOL;
			}

			if(is_object(self::$kobj)) {
				if (in_array(self::$kobj->status, array("submitted", "processing", "waiting", "complete", "noresult"))){
					if(self::$kobj->status == "complete" && isset(self::$kobj->data) && is_array(self::$kobj->data)) {
						self::$errorCode		= 0;
						self::$status 			= self::$kobj->status;
						self::$data		 		= self::$kobj->data;
						return TRUE;
					} elseif (self::$kobj->status == "complete" && (!isset(self::$kobj->data) || !is_array(self::$kobj->data))){
						if($debugLevel > 1) JLog::log("api","Kred Api Fail; data node did not set or is not array. Details:".serialize($apiUrl).$apiResponse);
						self::$status = self::$kobj->status;
						self::$errorCode = 1;
						return FALSE;
					} else {
						self::$status = self::$kobj->status;
						self::$errorCode = 4;
						return FALSE;
					}
				} else {
					self::$status = self::$kobj->status;
					self::$errorCode = 5;
					return FALSE;
				}
			} else {
				if($debugLevel > 1) JLog::log("api","Kred Api Fail; could not parse result. Details:".serialize($apiUrl).$apiResponse);
				self::$errorCode = 2;
				return FALSE;
			}
		} else {
			if($debugLevel > 1) JLog::log("api","Kred Api Fail; no response. Details:".serialize($apiUrl));
			self::$errorCode = 3;
			return FALSE;
		}
	}

	/**
	 *	updates given userid's kred data from API
	 *
	 * @param integer $userId
	 * @param string $screenname
	 * @param integer $debugLevel
	 * @author murat
	 */
	public static function updateUserKred($userId, $screenname, $debugLevel=0) {
		if($debugLevel > 0) {
			Tool::echoLog('getting result from kred for screenname:'.$screenname);
		}
		
		$userUpdate = new UserUpdate();
		$userUpdate->user = $userId;
		$userUpdate->load();
	
		$potentialReach = array( 7 => 0, 30 => 0, 60 => 0);
		$realReach = array( 7 => 0, 30 => 0, 60 => 0);

		if(self::loadFromAPI($screenname, $debugLevel)) {

			if($debugLevel > 0) {
				Tool::echoLog($screenname.' load from API');
			}
			
			$ii = 0;
			$cnt = count(self::$data);
			for($i=$cnt-1;$i>-1;$i--){
				foreach(array(7,30,60) as $j){
					if ($ii<$j){
						$potentialReach[$j] += self::$data[$i][1]->potential;
						$realReach[$j] += self::$data[$i][1]->real;
						if (($ii+1) == $cnt){
							$potentialReach[$j] = intval($potentialReach[$j] / $cnt);
							$realReach[$j] = intval($realReach[$j] / $cnt);
						}
					} elseif ($ii == $j){
						$potentialReach[$j] = intval($potentialReach[$j] / $j);
						$realReach[$j] = intval($realReach[$j] / $j);
					}
				}
				$ii++;
			}
			
			//echo "potentialReach7 : $potentialReach[7] <br> realReach7 : $realReach[7] <br>";
			//echo "potentialReach30 : $potentialReach[30] <br> realReach30 : $realReach[30] <br>";
			//die("potentialReach60 : $potentialReach[60] <br> realReach60 : $realReach[60]");

			//update kred data
			$twitterInfoKred = new TwitterInfoKred();
			$twitterInfoKred->updateUserKredInformation($userId, $potentialReach, $realReach, self::$data);

			$userUpdate->kredStatus = 'c';
			$userUpdate->kredTryCount = 0;
			$userUpdate->save();
			
			unset($twitterInfoKred,$userUpdate);
			
			return TRUE;

		} else {

			if($debugLevel > 0) {
				Tool::echoLog($screenname.' could not load from API. Error Code: ' . self::$errorCode . " Status : " . self::$status);
			}

			//update userUpdate
			if(self::$errorCode == 4) { // probably waiting for process
				switch (self::$status) {
					case "submitted":
						$userUpdate->kredStatus = 's';
						break;
					case "processing":
						$userUpdate->kredStatus = 'p';
						break;
					case "waiting":
						$userUpdate->kredStatus = 'w';
						break;
					case "noresult":
						$twitterInfoKred = new TwitterInfoKred();
						$twitterInfoKred->updateUserKredInformation($userId, $potentialReach, $realReach);
						
						$userUpdate->kredStatus = 'x';
						$userUpdate->kredTryCount = 0;
						break;
					default:
						$userUpdate->kredStatus = 'e';
						break;
				}
			} else { // unregistered errors
				$userUpdate->kredStatus = 'e';
			}

			$userUpdate->kredUpdateDate = time();
			$userUpdate->kredTryCount++;
			$userUpdate->save();
			/**/
			return FALSE;
		}
	}

	/**
	 * Logs request made to kred
	 *
	 * @param string $rUrl
	 * @return LogKred
	 */
	public static function logRequest($rUrl) {
		$logK = new LogKred();
		$logK->requestDate = time();
		$logK->request = $rUrl;
		$logK->save();
		return $logK;
	}

	/**
	 * Logs response received from kred
	 *
	 * @param LogKred $logK
	 * @param type $response
	 */
	public static function logResponse(LogKred $logK, $response) {
		$logK->response = serialize($response);
		$logK->responseDate = time();
		$logK->save();
	}

} // END class Kred
