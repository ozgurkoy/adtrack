<?php
/**
 * Format clas
 *  General formatter functions should be here
 *
 * @author Murat
 */
class Format {

	/**
	 * @var array
	 */
	public static $_setting = array();

	/**
	 * Custom number formatting
	 *
	 * @return float
	 * @author Murat Hosver
	 */
	public static function NumberFormat($n,$d=2){
		if ($n == 0){
			return $n;
		} else {
			$ds = self::getSetting("NUMBER_FORMAT_DECIMAL_SEPERATOR");
			$n = number_format($n,$d,$ds,self::getSetting("NUMBER_FORMAT_THOUSAND_SEPERATOR"));

			if (strpos($n,$ds)){
				$loop = true;
				$count = 0;

				while ($loop && (substr($n,-1) == 0 || substr($n,-1) == $ds)){
					if (substr($n,-1) == $ds || $count > 19) $loop = false;
					$n = substr($n,0,-1);
					$count++;
				}
			}

			return $n;
		}
	}

	/**
	 * check the country code and format the given currency
	 *
	 * @return string
	 * @author Murat
	 */
	public static function getFormattedCurrency($amount,$formatNumber=false,$decimal=2){
		if ($formatNumber && is_numeric($amount)) $amount = self::NumberFormat($amount,$decimal);
		if (self::getSetting("CURRENCY_SYMBOL_AFTER"))
			return $amount.self::getSetting("CURRENCY_SYMBOL");
		else
			return self::getSetting("CURRENCY_SYMBOL").$amount;

	}

	/**
	 * Replace global variables in word texts
	 *
	 * @return string
	 * @author Murat
	 */
	public static function replaceGlobals($t){
		global $SYSTEM_EMAILS;

		$t = str_replace("{CURRENCY_SYMBOL}",self::getSetting("CURRENCY_SYMBOL"),$t);
		$t = str_replace("{ROOT_DOMAIN}",ROOT_DOMAIN,$t);
		$t = str_replace("{SERVER_NAME}",SERVER_NAME,$t);
		foreach ($SYSTEM_EMAILS as $key=>$email){
			$t = str_replace("{mail.$key}",$email,$t);
		}
		$t = preg_replace("/{([A-Z0-9{}\.,]+)\|cformat}/i",self::getFormattedCurrency('$1'),$t);
		return $t;
	}


	/**
	 * Replace string by param
	 *
	 *	$params = array("user" =>"sam","url"=>"http://sam.com","number"=>7 );
	 *  $s = "Hello [user], please check the [url] do it [number] of times";
	 *
	 * @return string
	 * @author Murat
	 */
	public static function replaceStringParam($params,$s){

		$param = array();
		$paramV = array();
		if(!is_null($params)){
			foreach ($params as $key=>$value){
				$param[]= '{'.$key.'}';
				$paramV[]= $value;
			}
			$s = str_replace($param,$paramV,$s);
		}
		unset($param,$paramV);
		$s = self::replaceGlobals($s);
		return $s;
	}

	/**
	 * Parametizer converts the given string to a nodejs parameter for SLS
	 *
	 * @return string
	 * @author Murat
	 */
	public static function parametizer($param){
		$param = str_replace('"',"‟",$param);
		$param = str_replace("'","`",$param);
		$param = urlencode($param);
		return $param;
	}

	/**
	 * Referrer parser
	 *
	 * @return array
	 * @author Murat
	 */
	public static function parseReferrer($data){
		$domains = array();
		$total = 0;
		if(!isset($data->error) && count($data)) {
			foreach ($data as $value) {
				$parsed = @parse_url($value->_id);
				$parsedHost = isset($parsed['host']) ? $parsed['host'] : '';
				if(strlen($parsedHost)) {
					$parsedHost = str_replace('www.', '', $parsedHost);
					if(!isset($domains[$parsedHost])) {
						$domains[$parsedHost] = 0;
					}
					$domains[$parsedHost]+=$value->count;
					$total+=$value->count;
				} else {
					if (!isset($domains["Unreferred"])) $domains["Unreferred"] = 0;
					$domains["Unreferred"]+=$value->count;
					$total+=$value->count;
				}
			}
		}
		/*
		if(!count($domains)) {
			$domains[SERVER_NAME] = 1;
			$total = 1;
		}
		*/
		$domains['Total'] = $total;

		return $domains;
	}

	/**
	 * Get actual date
	 *
	 * @return string
	 * @author Murat
	 */
	public static function getActualDateTime(){

		return date(self::getDateTimeFormat());
	}

	public static function getDateTimeFormated($date){

		return date(self::getDateTimeFormat(),$date);
	}

	public static function getDateTimeFormat(){

		$dateFormat = self::getSetting("dateFormat") . " " . self::getSetting("timeFormat");

		return $dateFormat;
	}

	public static function formatBytes($size, $precision = 2) {
		$base = log($size) / log(1024);
		$suffixes = array('', 'Kb', 'Mb', 'Gb', 'Tb');

		return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];

	}

	public static function getSetting($key){
		if (empty(self::$_setting[$key]))
			self::$_setting[$key] = Setting::getValue($key);

		return self::$_setting[$key];

	}

	public static function array_orderby() {
		$args = func_get_args();
		$data = array_shift( $args );

		if ( ! is_array( $data ) ) {
			return array();
		}

		$multisort_params = array();
		foreach ( $args as $n => $field ) {
			if ( is_string( $field ) ) {
				$tmp = array();
				foreach ( $data as $row ) {
					$tmp[] = $row[ $field ];
				}
				$args[ $n ] = $tmp;
			}
			$multisort_params[] = &$args[ $n ];
		}

		$multisort_params[] = &$data;
		call_user_func_array( 'array_multisort', $multisort_params );
		return end( $multisort_params );
	}

	/**
	 * Returns the values from a single column of the input array, identified by
	 * the $columnKey.
	 *
	 * Optionally, you may provide an $indexKey to index the values in the returned
	 * array by the values from the $indexKey column in the input array.
	 *
	 * @param array $input A multi-dimensional array (record set) from which to pull
	 *                     a column of values.
	 * @param mixed $columnKey The column of values to return. This value may be the
	 *                         integer key of the column you wish to retrieve, or it
	 *                         may be the string key name for an associative array.
	 * @param mixed $indexKey (Optional.) The column to use as the index/keys for
	 *                        the returned array. This value may be the integer key
	 *                        of the column, or it may be the string key name.
	 * @return array
	 */
	public static function array_column($input = null, $columnKey = null, $indexKey = null)
	{
		// Using func_get_args() in order to check for proper number of
		// parameters and trigger errors exactly as the built-in array_column()
		// does in PHP 5.5.
		$argc = func_num_args();
		$params = func_get_args();

		if ($argc < 2) {
			trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
			return null;
		}

		if (!is_array($params[0])) {
			trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
			return null;
		}

		if (!is_int($params[1])
			&& !is_float($params[1])
			&& !is_string($params[1])
			&& $params[1] !== null
			&& !(is_object($params[1]) && method_exists($params[1], '__toString'))
		) {
			trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		if (isset($params[2])
			&& !is_int($params[2])
			&& !is_float($params[2])
			&& !is_string($params[2])
			&& !(is_object($params[2]) && method_exists($params[2], '__toString'))
		) {
			trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
			return false;
		}

		$paramsInput = $params[0];
		$paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

		$paramsIndexKey = null;
		if (isset($params[2])) {
			if (is_float($params[2]) || is_int($params[2])) {
				$paramsIndexKey = (int) $params[2];
			} else {
				$paramsIndexKey = (string) $params[2];
			}
		}

		$resultArray = array();

		foreach ($paramsInput as $row) {

			$key = $value = null;
			$keySet = $valueSet = false;

			if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
				$keySet = true;
				$key = (string) $row[$paramsIndexKey];
			}

			if ($paramsColumnKey === null) {
				$valueSet = true;
				$value = $row;
			} elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
				$valueSet = true;
				$value = $row[$paramsColumnKey];
			}

			if ($valueSet) {
				if ($keySet) {
					$resultArray[$key] = $value;
				} else {
					$resultArray[] = $value;
				}
			}

		}

		return $resultArray;
	}
}