<?php
/**
 * Finance Validation Class
 *
 * @package default
 * @author Ozgur
 **/
require_once($__DP."site/country/".COUNTRY_CODE.".php");

class
ValidateFinance extends Validate
{
	/**
	 * Campaign validation error codes
	 * @var array
	 */
	public static $errorCodes = array(
		401=>"Payment option not found",
		402=>"Bank not found",
		409=>"Wrong payment option to edit.",
		410=>"Bank info problem",
		411=>"Security problem",
		412=>"Amount available is less then minimum withdrawal amount",
		413=>"Wrong payment option to show.",
		414=>"Must withdraw all on this type of account",
		415=>"Amount available is more than maximum withdrawal amount",
		416=>"invalid email",
		417=>"invalid IBAN",
		418=>"Amount requested is more than the current balance available",
		419=>"Account number is required",

	);

	public static $errorFields = array(
		402=>"bank",
		416=>"email",
		417=>"IBAN",
		412=>"amount",
		414=>"amount",
		415=>"amount",
		418=>"amount",
		419=>"accountnumber"

	);

	public static $errorLangDefs = array(
		88=>"manErr",
		402=>"manErr",
		416=>"wrongEmailFormatErr",
		417=>"invalidIBAN",
		412=>"requestedAmountIsLessThanMin",
		414=>"mustWithdrawAll",
		415=>"requestedAmountIsMoreThanMax",
		418=>"requestedAmountIsMoreThanBalance",
		419=>"manErr"
	);


	/**
	 * @param $params
	 * @author Ozgur
	 * @return ValidationResponse
	 */
	public static function publisherPaymentOptionAction( &$params ) {
		$r = new ValidationResponse();

		if ( isset($params[ "checkOwnerShipOnly" ]) )
			$mFields = array( "id", "pid" );
		else
			$mFields = array( "publisherWithdrawOptionId", "details", "displayName", "pid" );
		self::mandatoryCheck( $params, $mFields, $r );
		while ( empty( $r->errorCode ) ) {
			//editing
			if ( !empty( $params[ "id" ] ) ) {
				$p = new PublisherPaymentOption( (int)$params[ "id" ] );
				//check if exists
				if ( $p->gotValue === false ) {
					$r->errorCode = 409;
					break;
				}

				if ( $p->loadPublisher()->id != $params[ "pid" ] ) {
					$r->errorCode = 411;
					break;
				}

				$r->returnParams = array( "p" => $p );
			}

			if ( isset($params[ "checkOwnerShipOnly" ]) )
				break;


			//validate for method
			if ( ( $method = self::withdrawMethodExists( $params[ "publisherWithdrawOptionId" ] ) ) === false ) {
				$r->errorCode = 401;
				break;
			}

			if ( $method->name == "bank" &&
				( $bank = self::bankExists( $params[ "bank" ] ) ) === false
			) {
				$r->errorCode = 402;
				break;
			}

			//validate values for details.
			$values  = $params[ "details" ];
			if ( gettype( $values ) != "array" )
				$values = json_decode( $params[ "details" ], true );
			if ( self::validateWithDrawMethod( $method, $values, $r ) !== true )
				break;


			/*no publisher billing checking..*/

			break;
		}

		if ( !empty( $r->errorCode ) ) {
			$r->valid = false;
			if ( empty( $r->errorDetail ) ) $r->errorDetail = self::$errorCodes[ $r->errorCode ];
		}

		return $r;

	}

	/**
	 * for website.
	 * @param $params
	 * @return bool|ValidationResponse
	 * @author Özgür Köy
	 */
	public static function validatePublisherPaymentOption(  &$params ){
		$resp = self::publisherPaymentOptionAction( $params );
		return self::response( $resp );
	}


	/**
	 * @param $withdrawableBalance int
	 * @param $minAmount int min withdraw amount
	 * @author Ozgur
	 * @return bool
	 */
	public static function publisherCanWithDraw( $withdrawableBalance, $minAmount) {
		return $withdrawableBalance >= $minAmount;
	}

	/**
	 * @param                    $type
	 * @param                    $values
	 * @param ValidationResponse $r
	 * @return bool
	 */
	public static function validateWithDrawMethod( PublisherWithdrawOption $method, array $values, ValidationResponse &$r ) {
		$type = $method->name;
		switch ( $type ) {
			case "bank":
				if ( CountryValidate::validateBankInfo( $values, $r ) !== true )
					return false;

				break;
			case "paypal":
				if ( !isset( $values[ "email" ] ) || self::validateEmail( $values[ "email" ] ) !== true ) {
					$r->errorCode = 416;

					return false;
				}
				break;
			default: #account number
				if ( method_exists( "CountryValidate", "custom_" . $type ) === true ) {
					$response = call_user_func_array( array( "CountryValidate", "validate_" . $type ), array( $values, &$r ) );
					if ( $response !== true )
						return false;
				}
				else{
					$o = json_decode( $method->fields, true );
					foreach ( $o as $field => $fregx ) {
						if ( !isset( $values[ $field ] ) || preg_match( "|" . $fregx . "|i", $values[ $field ] ) == 0 ) {
							$r->errorCode = 419;

							return false;
						}
					}
				}


				break;
		}
		return true;
	}

	/**
	 * @param array $params
	 * @return ValidationResponse
	 */
	public static function validateWithdrawalRequest( array &$params ) {
		$r = new ValidationResponse();

		$mFields = array( "pid", "paymentOptionId", "amount" );
		self::mandatoryCheck( $params, $mFields, $r );

		while ( empty( $r->errorCode ) ) {
			$publisher           = new Publisher( $params[ "pid" ] );
			$withdrawableBalance = $publisher->recalculateWithdrawableBalance();


			$p = new PublisherPaymentOption( (int)$params[ "paymentOptionId" ] );

			//check if exists
			if ( $p->gotValue === false ) {
				$r->errorCode = 409;
				break;
			}

			$minWithdrawAmount  = $p->loadPublisherWithdrawOption()->minWithdrawAmount;
			$maxWithdrawAmount  = $p->loadPublisherWithdrawOption()->maxWithdrawAmount;
			$canWithDrawOnlyMax = (bool)$p->publisherWithdrawOption->canWithdrawOnlyMax;

			//check belonging
			if ( $p->loadPublisher()->id != $params[ "pid" ] ) {
				$r->errorCode = 411;
				break;
			}
			//check canwithdraw
			if ( self::publisherCanWithDraw( $withdrawableBalance, $minWithdrawAmount ) === false || self::publisherCanWithDraw( $params["amount"], $minWithdrawAmount ) === false ) {
				$r->errorCode = 412;
				break;
			}
			//check max amount
			if ( $maxWithdrawAmount < $params["amount"] ) {
				$r->errorCode = 415;
				break;
			}

			//check balance
			if ( $params[ "amount" ] > $withdrawableBalance ) {
				$r->errorCode = 418;
				break;
			}

			//is she withdrawing all of it ?
			if ( ( $withdrawableBalance != $params[ "amount" ] ) && $canWithDrawOnlyMax ) {
				$r->errorCode = 414;
				break;
			}

			//$r->returnParams = array( "p" => $p );

			break;
		}
		if ( !empty( $r->errorCode ) ) {
			$r->valid = false;
			if ( empty( $r->errorDetail ) ) $r->errorDetail = self::$errorCodes[ $r->errorCode ];
		}


		return self::response($r);

	}

	/**
	 * @param $methodId
	 * @return PublisherWithdrawOption|bool
	 */
	public static function withdrawMethodExists( $methodId ) {
		if ( !empty( $methodId ) ) {
			$method = new PublisherWithdrawOption( (int)$methodId );

			if ( $method->gotValue ) {
				return $method;
			}
		}

		return false;
	}

	/**
	 * @param $bankId
	 * @return Bank|bool
	 */
	public static function bankExists( $bankId ) {
		if ( !empty( $bankId ) ) {
			$bank = new Bank( (int)$bankId );

			if ( $bank->gotValue )
				return $bank;
		}

		return false;
	}

	/**
	 * @param array $params
	 * @return ValidationResponse
	 */
	public static function validateMoneyActionDetail( array &$params ) {
		$r       = new ValidationResponse();
		$mFields = array( "id", "pid" );

		self::mandatoryCheck( $params, $mFields, $r );

		while ( empty( $r->errorCode ) ) {
			$p = new PublisherMoneyAction( $params[ "id" ] );
			$p->loadPublisher();
			if ( !( $p->publisher_publisherMoneyAction && $p->publisher_publisherMoneyAction->gotValue && $p->publisher_publisherMoneyAction->id == $params[ "pid" ] ) ) {
				$r->errorCode = 413;
				break;
			}

			break;
		}

		if ( !empty( $r->errorCode ) ) {
			$r->valid = false;
			if ( empty( $r->errorDetail ) ) $r->errorDetail = self::$errorCodes[ $r->errorCode ];
		}

		return $r;

	}
}
