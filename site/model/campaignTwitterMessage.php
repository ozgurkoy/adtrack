<?php
/**
 * campaignTwitterMessage real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignTwitterMessage.php';

class CampaignTwitterMessage extends CampaignTwitterMessage_base
{
	/**
	 * CampaignPublisher
	 *
	 * @var CampaignPublisher
	 **/
	public $campaignPublisher;

	/**
	 * PublisherRevenue
	 *
	 * @var PublisherRevenue
	 */
	public $publisherRevenue;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Load CampaignPublisher record
	 *
	 * @return CampaignPublisher
	 * @author Murat
	 */
	public function loadCampaignPublisher(){
		$this->campaignPublisher = new CampaignPublisher();
		$this->campaignPublisher->load(array(
			"campaign_campaignPublisher" => $this->campaign_campaignTwitterMessage,
			"publisher_campaignPublisher" => $this->publisher_campaignTwitterMessage,
		));
		return $this->campaignPublisher;
	}

	/**
	 * Load PublisherRevenue record
	 *
	 * @return PublisherRevenue
	 * @author Murat
	 */
	public function loadPublisherRevenue(){
		$this->publisherRevenue = new PublisherRevenue();
		$this->publisherRevenue->load(array(
			"campaign_publisherRevenue" => $this->campaign_campaignTwitterMessage,
			"publisher_publisherRevenue" => $this->publisher_campaignTwitterMessage,
			"msgindex" => $this->msgindex
		));
		if (!$this->publisherRevenue->gotValue){
			$this->publisherRevenue->campaign_publisherRevenue = $this->campaign_campaignTwitterMessage;
			$this->publisherRevenue->publisher_publisherRevenue = $this->publisher_campaignTwitterMessage;
			$this->publisherRevenue->msgindex = $this->msgindex;
			$this->publisherRevenue->save();
		}
		return $this->publisherRevenue;
	}

	/**
	 * Send Twit
	 *
	 * @param int $campId
	 * @param int $publisherId
	 * @param int $msgIndex
	 *
	 * @return bool
	 * @author Murat
	 */
	public static function sendTweet($campId, $publisherId, $msgIndex){
		global $__DP,$debugLevel;

		$ctm = new CampaignTwitterMessage();
		$ctm->load(array(
			"campaign_campaignTwitterMessage"=>$campId,
			"publisher_campaignTwitterMessage"=>$publisherId,
			"msgindex"=>$msgIndex,
			"state"=>array(CampaignTwitterMessage::ENUM_STATE_WAITING,CampaignTwitterMessage::ENUM_STATE_TWITTER_ERROR_WHILE_TWEET_SEND,CampaignTwitterMessage::ENUM_STATE_UNKNOWN_ERROR_WHILE_TWEET_SEND)
		));
		if ($ctm->gotValue){
			$ctm->loadCampaignPublisher()->loadSnTwitterHistory();

			require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

			$consumerKey = Setting::getValue("CONSUMER_KEY");
			$consumerSecret = Setting::getValue("CONSUMER_SECRET");

			//if($debugLevel > 0) {
			//	JUtil::echoLog("ConsumerKey: $consumerKey | ConsumerSecret: $consumerSecret | userKey: {$ctm->campaignPublisher->snTwitterHistory->twKey} | userSecret: {$ctm->campaignPublisher->snTwitterHistory->twKeySecret}");
			//}

			$connection = new TwitterOAuth($consumerKey, $consumerSecret, $ctm->campaignPublisher->snTwitterHistory->twKey, $ctm->campaignPublisher->snTwitterHistory->twKeySecret);
			$logT = Twitter::logRequest($consumerKey, $consumerSecret, $ctm->campaignPublisher->snTwitterHistory->twKey, $ctm->campaignPublisher->snTwitterHistory->twKeySecret, 'statuses/update?status='.$ctm->message);

			$postResult = $connection->post('statuses/update', array('status' => $ctm->message));
			Twitter::logResponse($logT, $postResult);

			if(isset($postResult->error)) {
				JLog::log('twi', 'Twitter, send post error: ' . serialize($postResult));
				if(strpos($postResult->error, 'Could not authenticate') !== false) {
					$ctm->state = CampaignTwitterMessage::ENUM_STATE_TWITTER_ERROR_WHILE_TWEET_SEND;
					$ctm->tryCount++;
					$ctm->save();
				} else {
					$ctm->state = CampaignTwitterMessage::ENUM_STATE_UNKNOWN_ERROR_WHILE_TWEET_SEND;
					$ctm->tryCount++;
					$ctm->save();
				}

				if($debugLevel > 0) {
					JUtil::echoLog("Tweet couldn't sent #" . $ctm->id . " due to : " . $ctm->state);
				}
				return false;
			} elseif(isset($postResult->errors)) {
				//api error
				JLog::log('twi', 'Twitter, send post error: ' . serialize($postResult));
				if (isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 89){ //Invalid or expired token
					$ctm->state = CampaignTwitterMessage::ENUM_STATE_TWITTER_ERROR_WHILE_TWEET_SEND;
					$ctm->tryCount++;
					$ctm->save();
					$ctm->loadPublisher_campaignTwitterMessage();
					$ctm->publisher_campaignTwitterMessage->unauthorizedFromTwitter();
				} elseif (isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 187){ //Status is a duplicate
					$ctm->state = CampaignTwitterMessage::ENUM_STATE_STATUS_DUPLICATE;
					$ctm->tryCount++;
					$ctm->save();
				} else {
					$ctm->state = CampaignTwitterMessage::ENUM_STATE_UNKNOWN_ERROR_WHILE_TWEET_SEND;
					$ctm->tryCount++;
					$ctm->save();
				}

				if($debugLevel > 0) {
					JUtil::echoLog("Tweet couldn't sent #" . $ctm->id . " due to : " . $ctm->state);
				}

				return FALSE;
			} else {
				$ctm->loadPublisher_campaignTwitterMessage()->loadSnTwitter();
				$trueReach = $ctm->campaignPublisher->snTwitterHistory->trueReach;

				if ($ctm->publisher_campaignTwitterMessage->publisher_snTwitter->twid == $ctm->campaignPublisher->snTwitterHistory){
					$trueReach = $ctm->publisher_campaignTwitterMessage->publisher_snTwitter->trueReach;
				}
				$ctm->state = CampaignTwitterMessage::ENUM_STATE_TWEET_SENT;
				$ctm->tryCount = 0;
				$ctm->tweetId = $postResult->id_str;
				$ctm->tweetSentDate = time();
				$ctm->toCheckDate = time() + (72 * 60 * 60);
				$ctm->trueReachOnSend = $trueReach;
				$ctm->save();

				$ctm->loadCampaign();
				$ctm->publisher_campaignTwitterMessage->loadUser();
				$ctm->publisher_campaignTwitterMessage->user_publisher->sendSysMessage(
					'V2_tweetSent',
					array(),
					array(
						"name" => $ctm->publisher_campaignTwitterMessage->getFullName(),
						"campName" => $ctm->campaign_campaignTwitterMessage->title,
						"sendDate" => Format::getActualDateTime(),
						"message" => $ctm->message
					),
					$ctm->campaign_campaignTwitterMessage->id,
					null,
					null,
					$ctm->campaign_campaignTwitterMessage->getPreviewImageLink()
				);

				Cron::create(Cron::ENUM_TYPE_TWEET_CHECK,$ctm->toCheckDate,$ctm->publisher_campaignTwitterMessage->id,$ctm->campaign_campaignTwitterMessage->id,null,$ctm->msgindex);

				if($debugLevel > 0) {
					JUtil::echoLog("Tweet sent for campaign #" . $campId . " from " . $ctm->campaignPublisher->snTwitterHistory->screenname . ". Tweet message: '" . $ctm->message);
				}

				return TRUE;
			}
		} else {
			return false;
		}
	}

	/**
	 * Mark the row as tweet deleted
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public function markTweetDeleted() {
		global $debugLevel;

		$this->tweetCheckDate = time();
		$this->state = CampaignTwitterMessage::ENUM_STATE_TWEET_DELETED;
		$this->tryCount++;
		$this->save();

		$this->publisher_campaignTwitterMessage->user_publisher->sendSysMessage(
			'V2_tweetDeleted',
			array(),
			array(
				"name" => $this->publisher_campaignTwitterMessage->getFullName(),
				"campName" => $this->campaign_campaignTwitterMessage->title,
				"sendDate" => Format::getActualDateTime(),
				"message" => $this->message
			),
			$this->campaign_campaignTwitterMessage->id,
			null,
			null,
			$this->campaign_campaignTwitterMessage->getPreviewImageLink()
		);

		$this->publisherRevenue->status = PublisherRevenue::ENUM_STATUS_CANCELED;
		$this->publisherRevenue->save();

		$ama = new AdvertiserMoneyAction();
		$ama->load(array(
			"advertiser_advertiserMoneyAction" => $this->campaign_campaignTwitterMessage->advertiser,
			"campaign_advertiserMoneyAction" => $this->campaign_campaignTwitterMessage,
			"publisher" => $this->publisher_campaignTwitterMessage,
			"paymentType" => AdvertiserMoneyAction::ENUM_PAYMENTTYPE_REFUND
		));
		if (!$ama->gotValue){
			AdvertiserMoneyAction::create(
				$this->campaign_campaignTwitterMessage->advertiser,
				$this->campaign_campaignTwitterMessage,
				($this->campaignPublisher->cost * $this->campaign_campaignTwitterMessage->multiplier),
				"tweet deleted - user:" . $this->publisher_campaignTwitterMessage->user_publisher->username,
				$this->publisher_campaignTwitterMessage,
				AdvertiserMoneyAction::ENUM_STATE_APPROVED,
				AdvertiserMoneyAction::ENUM_PAYMENTTYPE_REFUND
			);
		}
		unset($ama);

		$this->publisher_campaignTwitterMessage->user_publisher->saveHistory(
			UserHistory::ENUM_ACTIONID_TWEET_DELETED,
			null,
			UserHistory::ENUM_MADEBY_SYSTEM,
			json_encode(array("campaign"=>$this->campaign_campaignTwitterMessage->id))
		);

		if($debugLevel > 0) {
			JUtil::echoLog("Tweet checked for " . $this->campaignPublisher->snTwitterHistory->screenname . " and it seems like deleted #" . $this->tweetId);
		}
	}

	/**
	 * Mark the row as tweet exist
	 *
	 * @param object $tweetData
	 *
	 * @return void
	 *
	 * @author Murat
	 */
	public function markTweetExist($tweetData) {
		global $debugLevel;

		$this->tweetCheckDate = time();
		$this->state = CampaignTwitterMessage::ENUM_STATE_TWEET_EXIST;
		$this->tryCount++;
		$this->retweetCountOnCheck = $tweetData->retweet_count;
		$this->save();

		$this->publisher_campaignTwitterMessage->user_publisher->sendSysMessage(
			'V2_tweetNotDeleted',
			array(),
			array(
				"name" => $this->publisher_campaignTwitterMessage->getFullName(),
				"campName" => $this->campaign_campaignTwitterMessage->title,
				"date" => Format::getActualDateTime(),
				"revenue" => $this->campaignPublisher->cost
			),
			$this->campaign_campaignTwitterMessage->id,
			null,
			null,
			$this->campaign_campaignTwitterMessage->getPreviewImageLink()
		);

		$this->publisherRevenue->status = PublisherRevenue::ENUM_STATUS_APPROVED;
		$this->publisherRevenue->approveDate = time();
		$this->publisherRevenue->save();

		$this->publisher_campaignTwitterMessage->user_publisher->saveHistory(
			UserHistory::ENUM_ACTIONID_CAMPAIGN_REVENUE_APPROVED,
			null,
			UserHistory::ENUM_MADEBY_SYSTEM,
			json_encode(array("campaign"=>$this->campaign_campaignTwitterMessage->id))
		);

		if($debugLevel > 0) {
			JUtil::echoLog("Tweet checked for " . $this->campaignPublisher->snTwitterHistory->screenname . " and it exists #" . $this->tweetId);
		}
	}

	/**
	 * Check twit
	 *
	 * @param int $campId
	 * @param int $publisherId
	 * @param int $msgIndex
	 *
	 * @return bool
	 * @author Murat
	 */
	public static function checkTweet($campId,$publisherId,$msgIndex) {
		global $__DP;
		global $debugLevel;

		$ctm = new CampaignTwitterMessage();
		$ctm->load(array(
			"campaign_campaignTwitterMessage"=>$campId,
			"publisher_campaignTwitterMessage"=>$publisherId,
			"msgindex"=>$msgIndex,
			"state"=>array(CampaignTwitterMessage::ENUM_STATE_TWEET_SENT,CampaignTwitterMessage::ENUM_STATE_TWITTER_ERROR_WHILE_TWEET_CHECK)
		));
		if ($ctm->gotValue){
			$ctm->loadCampaignPublisher()->loadSnTwitterHistory();
			$ctm->loadPublisher_campaignTwitterMessage()->loadUser();
			$ctm->loadCampaign();
			$ctm->loadPublisherRevenue();

			require_once($__DP.'site/lib/twitteroauth/twitteroauth.php');

			$consumerKey = Setting::getValue("CONSUMER_KEY");
			$consumerSecret = Setting::getValue("CONSUMER_SECRET");

			$connection = new TwitterOAuth($consumerKey, $consumerSecret, $ctm->campaignPublisher->snTwitterHistory->twKey, $ctm->campaignPublisher->snTwitterHistory->twKeySecret);
			$logT = Twitter::logRequest($consumerKey, $consumerSecret, $ctm->campaignPublisher->snTwitterHistory->twKey, $ctm->campaignPublisher->snTwitterHistory->twKeySecret, 'statuses/update?status='.$ctm->message);

			$postResult = $connection->get('statuses/show', array('id' => $ctm->tweetId, 'include_entities' => '1'));
			Twitter::logResponse($logT, $postResult);

			if(isset($postResult->error)) {
				if(strpos($postResult->error, 'No status found') !== false) {
					//deleted
					$ctm->markTweetDeleted();
				} elseif(strpos($postResult->error, 'Not found') !== false) {
					//user deleted/deactivated
					$ctm->markTweetDeleted();
				} else if(strpos($postResult->error, 'Could not authenticate') !== false) {
					$ctm->markTweetDeleted();
					$ctm->publisher_campaignTwitterMessage->unauthorizedFromTwitter();
				}
			} elseif(isset($postResult->errors)) {
				if (!(isset($postResult->errors[0]) && isset($postResult->errors[0]->code) && $postResult->errors[0]->code == 34) || $ctm->tryCount<3){
					$ctm->tweetCheckDate = time();
					$ctm->state = CampaignTwitterMessage::ENUM_STATE_TWITTER_ERROR_WHILE_TWEET_CHECK;
					$ctm->tryCount++;
					$ctm->save();

					//register cronjob for tweet check 1 minute later
					Cron::create(
						Cron::ENUM_TYPE_TWEET_CHECK,
						(time()+60),
						$ctm->publisher_campaignTwitterMessage,
						$ctm->campaign_campaignTwitterMessage,
						null,
						$ctm->msgindex
					);


					if($debugLevel > 0) {
						JUtil::echoLog("Tweet checked and it looks like deleted. But we will try again. Current try count: " . ($ctm->tryCount-1));
					}
				}else{
					//user deleted/deactivated
					$ctm->markTweetDeleted();
				}
			} else {
				//not deleted
				$ctm->markTweetExist($postResult);
			}
		} else {
			if($debugLevel > 0) {
				JUtil::echoLog("CampaignTwitterMessage not found on table");
			}
			return false;
		}
	}

	/**
	 * Create cron job for tweet send
	 *
	 * @return void
	 * @author Murat
	 */
	public function createCronForTweetSend(){
		$cron = new Cron();
		$cron->load(array(
			"campaign_cron" => $this->campaign_campaignTwitterMessage,
			"publisher_cron" => $this->publisher_campaignTwitterMessage
		));
		if (!$cron->gotValue){
			Cron::create(Cron::ENUM_TYPE_TWEET_SEND,$this->toSendDate,$this->publisher_campaignTwitterMessage,$this->campaign_campaignTwitterMessage,null,$this->msgindex);
		}
	}

	/**
	 * Approve custom campaign message
	 *
	 * @return void
	 * @author Murat
	 */
	public function approveCustomMessage($message){
		$this->createCronForTweetSend();
		$this->message = str_replace("\\'", "'", $message);
		$this->state = CampaignTwitterMessage::ENUM_STATE_WAITING;
		$this->save();
	}
}

