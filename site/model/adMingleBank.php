<?php
/**
 * adMingleBank real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/adMingleBank.php';

class AdMingleBank extends AdMingleBank_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function getadMingleBankArray(){
		$b = new AdMingleBank();

		$b->load(array("active"=>1));

		$bArray  = array();
		if($b->gotValue){
			do{
				$bArray[$b->id] = $b->bankName;
			}while($b->populate());
		}

		unset($b);

		return $bArray;
	}



}

