<?php
/**
 * paypal class
 *
 * @package default
 **/
class Paypal
{
	/**
	 * Log object
	 *
	 * @var LogPaypal
	 */
	public $log;

	/**
	 * Send HTTP POST Request
	 *
	 * @param	string	The API method name
	 * @param	string	The POST Message fields in &name=value pair format
	 * @return	array	Parsed HTTP Response body
	 */
	protected function PPHttpPost($methodName_, $nvpStr_) {

		// Set up your API credentials, PayPal end point, and API version.
		$API_UserName = urlencode(PAYPAL_MASS_USER);
		$API_Password = urlencode(PAYPAL_MASS_PASS);
		$API_Signature = urlencode(PAYPAL_MASS_SIGNATURE);

		$API_Endpoint = "https://api-3t.paypal.com/nvp";
		if("sandbox" === PAYPAL_ENVIRONMENT || "beta-sandbox" === PAYPAL_ENVIRONMENT) {
			$API_Endpoint = "https://api-3t.".PAYPAL_ENVIRONMENT.".paypal.com/nvp";
		}
		$version = urlencode('51.0');

		// Set the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);

		// Turn off the server and peer verification (TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);

		// Set the API operation, version, and API signature in the request.
		$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";

		// Set the request as a POST FIELD for curl.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

		// Get response from the server.
		$httpResponse = curl_exec($ch);

		$this->log->responseDate = time();
		$this->log->response = urldecode($httpResponse);
		$this->log->save();

		if(!$httpResponse) {
			JLog::log('cri', 'Paypal MassPay Fail:'.curl_error($ch).'('.curl_errno($ch).')');
			exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
		}

		// Extract the response details.
		$httpResponseAr = explode("&", $httpResponse);

		$httpParsedResponseAr = array();
		foreach ($httpResponseAr as $i => $value) {
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
			}
		}

		if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
			JLog::log('cri', 'Paypal MassPay HTTP Response Fail. Request:'.$nvpreq.' to '.$API_Endpoint);
			exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
		}

		return $httpParsedResponseAr;
	}

	/**
	 * Make paypal pay request for one payment over MassPay API using NVP technique
	 *
	 * @param string $emailSubjectStr email title for email to send user on successful payment by paypal
	 * @param strin $email receiver email
	 * @param float $amount money amount
	 * @param strin $currency default TRY
	 * @return boolean|array FALSE on fail else response array
	 */
	public function pay($emailSubject, $email, $amount, $currency='TRY') {
		$emailSubject = urlencode($emailSubject);
		$email = urlencode($email);

		//initial
		$nvpStr = "&EMAILSUBJECT={$emailSubject}&RECEIVERTYPE=EmailAddress&CURRENCYCODE={$currency}";
		//one user
		$nvpStr.= "&L_EMAIL0={$email}&L_AMT0={$amount}";

		$this->log = new LogPaypal();
		$this->log->requestDate = time();
		$this->log->request = $nvpStr;
		$this->log->save();

		$httpParsedResponseAr = $this->PPHttpPost('MassPay', $nvpStr);

		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
			//exit('MassPay Completed Successfully: '.print_r($httpParsedResponseAr, true));
			return $httpParsedResponseAr;
		} else  {
			//exit('MassPay failed: ' . print_r($httpParsedResponseAr, true));
			return FALSE;
		}
	}

}
