<?php
/**
 * cplRemoteCampaign real class - firstly generated on 19-06-2014 12:10, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/cplRemoteCampaign.php';

class CplRemoteCampaign extends CplRemoteCampaign_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	/**
	 * get active lead count
	 *
	 * @param null $publisherId
	 * @return int
	 * @author Özgür Köy
	 */
	public function getActiveLeadCount( $publisherId=null ) {
		if($this->gotValue !== true)
			return false;

		$r = new CplRemoteCampaignUser();
//		$r->cplRemoteCampaign_cplRemoteCampaignUser
		$r->sum(
			"currentCount",
			"cid",
			array(
				"cplRemoteCampaign_cplRemoteCampaignUser" => $this->id,
				"cplRemoteCampaignUser_publisher"         => $publisherId
			)
		);

		$s = intval($r->cid);
		unset( $r );
		return $s;
	}



	/**
	 * get code for publisher
	 *
	 * @param Publisher $publisher
	 * @param null      $code
	 * @return CplRemoteCampaignUser
	 * @author Özgür Köy
	 */
	public function getCode( Publisher $publisher = null, $code = null ) {
		if ( is_null( $code ) )
			$code = JUtil::randomString( 10 );

		$cpl = new CplRemoteCampaignUser();
		if ( !is_null( $publisher ) ) {
			$cpl->load( array( "cplRemoteCampaignUser_publisher" => $publisher , "cplRemoteCampaign_cplRemoteCampaignUser" => $this ) );
		}

		if ( $cpl->gotValue !== true || is_null( $publisher ) ) {
			$cpl       = new CplRemoteCampaignUser();
			$cpl->code = $code;
			$cpl->save();
			$cpl->saveCplRemoteCampaign( $this );

			if ( !is_null( $publisher ) )
				$cpl->saveCplRemoteCampaignUser_publisher( $publisher );

		}

		return $cpl;
	}

	/**
	 * get click count from sls server for remote campaign
	 *
	 * @param $campPublicId
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function getClickCount( $campPublicId ) {
		$url = Tool::callSLService(
			"redirectionStats",
			array(
				"params" => json_encode(
					array(
						"camp"=> $campPublicId
					)
				)
			)
		);
		return $url[0]->total;
	}



	/**
	 * get short url for cpl.remote campaigns
	 *
	 * @param $publisherId
	 * @param $campaignID
	 * @return bool|string
	 * @author Özgür Köy
	 */
	public static function getShortURL( $publisherId, $campaignID ) {
		$c = new Campaign();
		$c->load( array( "publicID" => $campaignID ) );

		if ( $c->gotValue ) {
			$c->loadCplRemoteCampaign();
			$c->campaign_cplRemoteCampaign->loadCplRemoteCampaign_cplRemoteCampaignUser(
				array( "cplRemoteCampaignUser_publisher" => $publisherId )
			);
			$cu = &$c->campaign_cplRemoteCampaign->cplRemoteCampaign_cplRemoteCampaignUser;

			if (
				$cu &&
				$cu->gotValue
			) {
				if ( strlen( $cu->shortUrl ) > 0 )
					return $cu->shortUrl;

				$shortUrl = Campaign::getRedLink(
					$c->publicID,
					$c->campaign_cplRemoteCampaign->preparePreviewLink(),
					$cu->code,
					array(
						"title"       => $c->title,
						"description" => $c->campaign_cplRemoteCampaign->campDetail,
						"image"       => $c->getPreviewImageLink(),
						"shortUrl"    => $c->campaign_cplRemoteCampaign->SLDomain
					)
				);

				$cu->shortUrl = $shortUrl;
				$cu->save();

				unset( $cu, $c );

				if ( $shortUrl !== false )
					return $shortUrl;
			}
		}
		JLog::log( "cri", "Something went wrong with get cpl short url" );

		return false;
	}


	/**
	 * @author Özgür Köy
	 */
	public function campaignStart() {
		$this->state = CplRemoteCampaign::ENUM_STATE_STABILE;
		$this->save();

		$this->writeHistory( "campaign started", CplCampHistory::ENUM_MADEBY_SYSTEM );
	}

	/**
	 * @author Özgür Köy
	 */
	public function campaignComplete( $leads ) {
		$this->state = CplRemoteCampaign::ENUM_STATE_ENDED;
		$this->save(); // we make it here so that it wont happen again.

		$this->loadCampaign();

		$p = Backend::createSupervisionTask(
			"finalize CPL.Remote Campaign #".$this->campaign_cplRemoteCampaign->id,
			"finalizeCPLRemoteCampaign"
		);

		if($p !== false){
			$fin = $p->addChildTask(
				"finalization",
				"finalizeCPLRemoteCampaign",
				array(
					'cplCampaign'=>$this,
					'leads'=>$leads
				)
			);

			if ( $fin !== false ) {
				$fin->addChildTask(
					"sending emails:" . $this->campaign_cplRemoteCampaign->id . " name:" . $this->campaign_cplRemoteCampaign->title,
					"sendCampaignFinishingMails",
					array( "campaignId" => $this->campaign_cplRemoteCampaign->id )
				);
			}

			$p->runTask();
		}
	}


	/**
	 * payment to publishers
	 *
	 * @param Campaign $campaign
	 * @author Özgür Köy
	 */
	public function payToPublishers( Campaign &$campaign ) {
		//load users
		$users = $this->loadCplRemoteCampaign_cplRemoteCampaignUser(
			array(
				"isActive" => 1,
				"cplCampaignUser_publisher"=>array("id"=>"IS NOT NULL") ),
				0,
				50,
				null,
				false
		);

		if ( $users->gotValue ) {
			do {
				$publisher  = $users->loadCplRemoteCampaignUser_publisher();
				$revenue = $publisher->loadPublisher_publisherRevenue(
					array(
						"campaign_publisherRevenue"  => $campaign->id,
						"publisher_publisherRevenue" => $publisher->id
					)
				);

				//get valid leads
				$leads   = $this->getActiveLeadCount( $publisher->id );
//				$actives = $leads[ "valid" ];
				echo "active (".$leads.") :" . $leads.PHP_EOL;

				echo $revenue->amount = $this->perLead * $leads;
				echo PHP_EOL;
				$revenue->status = PublisherRevenue::ENUM_STATUS_APPROVED;
				$revenue->approveDate = time();
				$revenue->save();
			} while ( $users->populate() );
		}
	}


	/**
	 * refunds for cpl.remote
	 *
	 * @param Campaign $campaign
	 * @param          $leads
	 * @author Özgür Köy
	 */
	public function checkRefunds( Campaign &$campaign, $leads ){
		if(!is_object($campaign->campaign_cplRemoteCampaign))
			$campaign->loadCplRemoteCampaign();

		if ( $campaign->campaign_cplRemoteCampaign->maxLeadLimit > $leads ) {
			$refundCount  = $campaign->campaign_cplRemoteCampaign->maxLeadLimit - $leads;
			$refundAmount = ( $refundCount * $campaign->campaign_cplRemoteCampaign->perAdvertiserLead );

			Tool::echoLog( "Refunded lead count : " . $refundCount );
			AdvertiserMoneyAction::create(
				$campaign->advertiser,
				$campaign->id,
				$refundAmount,
				'cpl.remote campaign refund: ' . $campaign->title,
				null
			);

			$campaign->writeHistory(
				CampaignHistory::ENUM_ACTIONID_FRAUD_CONTROL,
				"Refunded unused lead value amount: " . $refundAmount,
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				$refundAmount
			);
		}
		else {
			Tool::echoLog( "No leads to refund" );

			$campaign->writeHistory(
				CampaignHistory::ENUM_ACTIONID_FRAUD_CONTROL,
				"No refund needed for CPL.remote campaign",
				CampaignHistory::ENUM_MADEBY_SYSTEM,
				null
			);
		}
	}


	public function writeHistory( $action, $madeBy = CplRemoteCampaignHistory::ENUM_MADEBY_USER ) {
		$cv          = new CplRemoteCampaignHistory();
		$cv->content = $action;
		$cv->details = json_encode( $_SERVER );
		$cv->madeBy  = $madeBy;

		$cv->save();
		$cv->saveCplRemoteCampaign( $this );

		unset( $cv );
	}

	public function preparePreviewLink() {
		if ( strpos( $this->link, "?" ) !== false )
			return $this->link . "&" . $this->hashCode . "=[code]";
		else
			return $this->link . "?" . $this->hashCode . "=[code]";
	}


	/**
	 * Create copy of preview image for campaign copy
	 *
	 * @return array
	 * @author Murat
	 */
	public function copyPreviewImage(){
		global $__DP;
		$uploadPath = "site/layout/images/campaign/";

		// image name
		$name = "_temp_" . JUtil::randomString(20) . "." . substr($this->previewFileName,-3);

		// location to save cropped image
		$url = $__DP.$uploadPath.$name;

		copy($uploadPath.$this->previewFileName,$url);

		$filesize = filesize( $url );

		//$fileName = $name . substr($url,-3);
		return array(
			"fullPath" => $url,
			"size"=>$filesize,
			"fileName" => $name
		);
	}

	/**
	 * @param      $pathToExcel
	 * @param bool $updatePendingLeads
	 * @return array
	 * @author Özgür Köy
	 * @throws JError
	 */
	public function updateByExcel( $pathToExcel, $updatePendingLeads=false ) {

		$f = file_get_contents( $pathToExcel );
//		$p = preg_split( "|\r\n?|", $f );
		$p = preg_split( "~\r\n|\n|\r~sm", $f );

		$keys = array();

		//single column
		foreach ( $p as $pline ) {
			if ( empty( $pline ) )
				continue;

			if ( strpos( $pline, "," ) !== false && preg_match( "|.+,$|", $pline ) !== 1 ) {
				return array( "bad", "File must consist of a single column" );
			}
			$k0 = trim( str_replace( ",", "", $pline ) );
			if(!array_key_exists($k0, $keys))
				$keys[ $k0 ] = 1;
			else
				$keys[ $k0 ]++;
		}

		if ( sizeof( $keys ) == 0 )
			return array( "bad", "Empty file" );


		//check for keys and apply
		JPDO::beginTransaction();
		$c = new CplRemoteCampaignUser();
		try {
			foreach ( $keys as $key => $total ) {
				$c->loadByCode( $key );

				if ( $c->gotValue !== true )
					throw new JError( $key, 99 );

				$c->loadCplRemoteCampaign();
				if( $c->cplRemoteCampaign_cplRemoteCampaignUser->id != $this->id )
					throw new JError( $key, 101 );

				$c->updateLeadAmount( $total, $updatePendingLeads );
			}

		}
		catch ( JError $j ) {
			JPDO::rollback();
			if($j->getCode() == 99 )
				return array( "bad", "Key not found in campaign : ".$j->getMessage() );
			elseif($j->getCode() == 101 )
				return array( "bad", "Key belongs to another campaign : ".$j->getMessage() );
		}
		JPDO::commit();
		return  array( "good", "Updated, refreshing." );

	}
}

