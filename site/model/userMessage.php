<?php
	/**
	* userMessage real class for Jeelet - firstly generated on 13-11-2011 20:41, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/userMessage.php");

	class UserMessage extends UserMessage_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		private $_message = null;
		private $_title = null;


		public function getMessage()
		{
			if (!empty($this->customMessage)){
				return $this->customMessage;
			} else {
				$message = "";

				if(!is_object($this->langMessageHistory_userMessage))
					$this->loadLangMessageHistory_userMessage();

				if (is_object($this->langMessageHistory_userMessage) && $this->langMessageHistory_userMessage->gotValue){
					if(!empty($this->messageVariables))
					{
						$messageVariables = json_decode($this->messageVariables,true);
						$message = Format::replaceStringParam($messageVariables,$this->langMessageHistory_userMessage->messageBody);
					}else{
						$message = $this->langMessageHistory_userMessage->messageBody;
					}

					return $message;
				} else {
					return "";
				}

			}

		}

		public function getTitle()
		{
			if (!empty($this->customMessageTitle)){
				return $this->customMessageTitle;
			} else {
				$_title = "";

				if(!is_object($this->langMessageHistory_userMessage))
					$this->loadLangMessageHistory_userMessage();

					if(isset($this->titleVariables))
					{
						$messageVariables = json_decode($this->titleVariables,true);
						if(is_object($this->langMessageHistory_userMessage))
							$_title = Format::replaceStringParam($messageVariables,$this->langMessageHistory_userMessage->messageTitle);
						else
							$_title = "";
					}
					elseif (!is_null($this->langMessageHistory_userMessage)){
						$_title = $this->langMessageHistory_userMessage->messageTitle;
					} else {
						$_title = $this->customMessageTitle;
					}
				return $_title;
			}
		}

		public function markAsRead()
		{
			$this->readed = 1;
			$this->save();
		}

		public static function markAsReadFromCampaign($userId,$cid)
		{
			$m = new UserMessage();
			$m->update(array( "user_userMessage" => $userId, "campaign" => $cid ),array("readed"=>1));
			JCache::delete("statNumbers_" . $userId);
		}

		public function markAsDeleted()
		{
			$this->deleted = 1;
			$this->save();
		}

		/**
		 * Get user Messages With paging
		 *
		 * @param array $params Array of required parameters
		 *
		 * @return array
		 * @author Sam
		 */
		public static function getUserMessagesCount($uId){

			$mes = new UserMessage();
			$loadParams = array("deleted" => 0,"readed" => 0,"user_userMessage"=>$uId,"msgtype"=>self::ENUM_MSGTYPE_CAMPAIGN);
			$msgCount =  $mes->count("id","cnt",$loadParams)->cnt;
			$loadParams["msgtype"] = self::ENUM_MSGTYPE_SYSTEM;
			$sysCount =  $mes->count("id","cnt",$loadParams)->cnt;

			$r = array("totalCount" => $sysCount+$msgCount, "campCount" => $msgCount, "sysCount" => $sysCount);
			return $r;
		}

		/**
		 * delete message
		 * @param $userId
		 * @param $messageId
		 * @return bool
		 * @author Özgür Köy
		 */
		public static function deleteMessage( $userId, $messageId ) {
			JPDO::beginTransaction();
			try{
				$m = new UserMessage();

				$m->load( array( "user_userMessage" => $userId, "id"=>$messageId ) );

				if($m->gotValue){
					$m->markAsDeleted();
				}

				JCache::delete("statNumbers_" . $userId);

				JPDO::commit();
			}
			catch(JError $j){
				JPDO::rollback();
				return false;
			}

			return true;

		}

		/**
		 * read message
		 * @param $userId
		 * @param $messageId
		 * @return bool
		 * @author Murat
		 */
		public static function readMessage( $userId, $messageId ) {
			JPDO::beginTransaction();
			try{
				$m = new UserMessage();

				if ($messageId == "all"){
					$m->update(array( "user_userMessage" => $userId, "readed"=>0 ),array("readed"=>1));
				} else {
					$m->load( array( "user_userMessage" => $userId, "id"=>$messageId ) );

					if($m->gotValue){
						$m->markAsRead();
					}
				}

				JCache::delete("statNumbers_" . $userId);

				JPDO::commit();
			}
			catch(JError $j){
				JPDO::rollback();
				return false;
			}

			return true;

		}

		/**
		 * Get user Messages With paging
		 *
		 * @param array $params Array of required parameters
		 *
		 * @return array
		 * @author Sam
		 */
		public static function getUserMessages($params){

			$mes = new UserMessage();
			$loadParams = array("deleted" => 0,"user_userMessage"=>$params["uId"]);

			if(isset($params["msgtype"])){
				if(array_key_exists($params["msgtype"],$mes->msgtype_options)){

					$loadParams["msgtype"] = $params["msgtype"];

				}
			}

			$mes->count("id","cnt",$loadParams);
			$r = array("recordCount" => $mes->cnt, "messages" => array(), "currentPage" => 0);

			// For full site display
			$markAsRead = isset($params["markAsRead"]);



			$mes = new UserMessage();

			$params["page"] = intval($params["page"]);

			if ($params["page"] < 1)
				$params["page"] = 1;

			$r["currentPage"] = $params["page"];

			$params["page"]--;
			$messageListItemCount = Setting::getValue("messageListItemCount");
			if (empty($messageListItemCount))
				$messageListItemCount = 10;

			$mes->limit($params["page"]*$messageListItemCount,$messageListItemCount)->orderBy("jdate DESC")->load($loadParams);

			if ($mes->gotValue){
				do {
					$r["messages"][] = array(
						"id" => $mes->id,
						"date" => $mes->jdate,
						"msgType" => $mes->msgtype,
						"message" => $mes->getMessage(),
						"title" => $mes->getTitle(),
						"readed" => $mes->readed
					);

				} while($mes->populate());
			}
//print_r($r);
//			exit;
			return $r;
		}

		/**
		 * Get user Messages With paging
		 *
		 * @param array $params Array of required parameters
		 *
		 * @return array
		 * @author Sam
		 */
		public static function getUserMessage($userId,$messageId){

			$mes = new UserMessage();
			$loadParams = array("deleted" =>0, "id" => $messageId,"user_userMessage"=>$userId);
			//print_r($loadParams);exit;

			$mes->load($loadParams);

			$r = null;
			if ($mes->gotValue){

				$campaignId = null;
				if(isset($mes->campaign)){
					$mes->loadCampaign();
					if(!is_null($mes->campaign))
					{
						$campaignId = $mes->campaign->publicID;
					}
				}

				$r = array(
					"id" => $mes->id,
					"date" => $mes->jdate,
					"msgType" => $mes->msgtype,
					"message" => $mes->getMessage(),
					"title" => $mes->getTitle(),
					"campaign" => $campaignId,
					"readed" => $mes->readed
				);


				$mes->markAsRead();

				return $r;
			}else{
				return null;
			}
		}

		public static function sendUserMessage($userId,TemplateMessage $template,$messageCode,$MesageType=UserMessage::ENUM_MSGTYPE_SYSTEM,$bodyParams=null,$headerParams=null,$campaign = null){

			// Save in user message
			$h = new UserMessage();
			$h->user_userMessage = $userId;
			$h->msgtype = $MesageType;
			$h->readed = 0;
			$h->deleted = 0;
			$h->messageCode = $messageCode;
			$h->messageVariables = json_encode($bodyParams);
			$h->titleVariables = json_encode($headerParams);
			$h->campaign = $campaign;
			$h->templateMessage = $template->id;
			$h->save();

		}

		/**
		 * Get user Messages With paging
		 *
		 * @param array $params Array of required parameters
		 *
		 * @return array
		 * @author Sam
		 */
		public static function setUserMessageStatus($userId,array $messageId,$status){

			$mes = new UserMessage();
			$loadParams = array("id" => $messageId,"user_userMessage"=>$userId);
			//print_r($loadParams);exit;


			if($status =="r")
			{
				//mark as read
				return $mes->update($loadParams,array("readed"=>1));
			}elseif($status =="d"){
				// delete message
				return $mes->update($loadParams,array("deleted"=>1));
			}else{
				// no action!
				return false;
			}
		}

		/**
		 * Create user message row
		 *
		 * @param int|User $user
		 * @param string $to
		 * @param string $subjectParams
		 * @param string $contentParams
		 * @param string $langMessageHistoryID
		 * @param string $campaign
		 * @param string $messageType
		 *
		 * @return void
		 *
		 * @author Murat
		 */
		public static function create($user,$titleVariables,$messageVariables,$langMessageHistoryID,$campaign=null,$messageType=UserMessage::ENUM_MSGTYPE_SYSTEM){
			$um = new UserMessage();
			$um->user_userMessage = $user;
			$um->langMessageHistory_userMessage = $langMessageHistoryID;
			$um->titleVariables = is_array($titleVariables) ? json_encode($titleVariables) : $titleVariables;
			$um->messageVariables = is_array($messageVariables) ? json_encode($messageVariables) : $messageVariables;
			$um->campaign = $campaign;
			$um->msgtype = $messageType;

			if(!is_null($campaign))
				$um->msgtype = UserMessage::ENUM_MSGTYPE_CAMPAIGN;

			$um->readed = 0;
			$um->deleted = 0;
			$um->save();
		}

		/**
		 * Create user message row
		 *
		 * @param int|User $user
		 * @param string $subject
		 * @param string $content
		 * @param string $campaign
		 * @param string $messageType
		 *
		 * @return void
		 *
		 * @author Murat
		 */
		public static function createManual($user,$subject,$content,$campaign=null,$messageType=UserMessage::ENUM_MSGTYPE_SYSTEM){
			$um = new UserMessage();
			$um->user_userMessage = $user;
			$um->campaign = $campaign;
			$um->msgtype = $messageType;
			$um->customMessageTitle = $subject;
			$um->customMessage = $content;

			if(!is_null($campaign))
				$um->msgtype = UserMessage::ENUM_MSGTYPE_CAMPAIGN;

			$um->readed = 0;
			$um->deleted = 0;
			$um->save();
		}
	}

	?>