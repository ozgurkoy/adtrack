<?php
/**
 * snXING real class - firstly generated on 27-02-2014 15:02, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/snXING.php';
require $__DP.'site/lib/oauth/http.php';
require $__DP.'site/lib/oauth/oauth_client.php';

class SnXING extends SnXING_base
{

	public $savedId=null;
	public $savedAvatar=false;
	public $XINGObject = null;
	public $accessToken = null;
	public $XINGUser = null;


	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct( $id = null ) {
		parent::__construct( $id );

		$this->XINGObject                  = new oauth_client_class;
		$this->XINGObject->debug           = 0;
		$this->XINGObject->debug_http      = 0;
		$this->XINGObject->server          = 'XING';
		$this->XINGObject->session_started = true;
		$this->XINGObject->redirect_uri    = Setting::getValue( "XING_CALLBACK_URL" );
		$this->XINGObject->client_id       = Setting::getValue( "XING_KEY" );
		$this->XINGObject->client_secret   = Setting::getValue( "XING_SECRET" );
	}

	public function processResponse() {
		if ( ( $success = $this->XINGObject->Initialize() ) ) {
			if ( ( $success = $this->XINGObject->Process() ) ) {
				if ( strlen( $this->XINGObject->access_token ) ) {
					$this->accessToken = $this->XINGObject->access_token;

					return true;
				}
			}
		}

		JLog::log( "social", "Problem with XING:" . print_r( $this->XINGObject->error ) );

		return false;
	}


	/**
	 * Add account
	 *
	 * @param array $XINGToken
	 * @return bool|SnGoogle
	 */
	public function addAccount( $infoObject, $accessToken )
	{
		$XINGId = $infoObject->id;

		// check if it's a new user
		$this->load( array( "xid" => $XINGId) );

		if ( !$this->gotValue || is_null( $this->publisher_snXING ) ) {
			$this->XINGid      = $XINGId;
			$this->accessToken = $accessToken;

			return $this->updateInfo(  $infoObject, $accessToken );
		}

		$this->loadPublisher();

		#this is an after fix. so that api works good.
		if(!$this->publisher_snXING || $this->publisher_snXING->gotValue === false )
			$this->publisher_snXING = null;

		return true;
	}

	/**
	 * @param $XINGId
	 * @param $info
	 * @return bool|SnGoogle
	 * @author Özgür Köy
	 */
	public function login( $infoObject, $accessToken )
	{
		$XINGId = $infoObject->id;

		$this->load( array(   "_custom"=>"`xid`= '$XINGId' AND `publisher_snXING` IS NOT NULL"));
		#acc.token does not expire
		if ( $this->gotValue ) {
			$this->accessToken = $accessToken;

			$this->loadPublisher()
				->writeHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "Login by XING", UserHistory::ENUM_MADEBY_USER )
				->loadUser()
				->login( null, null, $this->publisher_snXING->user_publisher->id );

			return $this->updateInfo( $infoObject, $accessToken );
		}
		else{
			//New User
			$this->load( array( "XINGid" => $XINGId ) );
			$this->accessToken = $accessToken;

			$this->updateInfo( $infoObject, $accessToken );
			return false;
		}

	}


	/**
	 * @return bool
	 * @author Özgür Köy
	 */
	public function getInfo() {
		$u0 = null;
		$success = $this->XINGObject->CallAPI(
			'https://api.xing.com/v1/users/me',
			'GET', array(), array( 'FailOnAccessError' => true ), $u0 );

		$success = $this->XINGObject->Finalize( $success );
		if ( $success && is_object( $u0 ) && isset( $u0->users ) && sizeof( $u0->users ) > 0 ) {
			$this->XINGUser = reset( $u0->users );

			return true;
		}

		return false;
	}

	/**
	 * @param array $info
	 * @return SnGoogle
	 */
	public function updateInfo( $infoObject, $accessToken ) {
		$this->name        = isset( $infoObject->first_name ) ? $infoObject->first_name . " " . $infoObject->last_name : "";
		$this->displayName = isset( $infoObject->display_name ) ? $infoObject->display_name : "";
		$this->email       = isset( $infoObject->active_email ) ? $infoObject->active_email : "";
		$this->avatar      = isset( $infoObject->photo_urls ) ? $infoObject->photo_urls->large : "";
		$this->gender      = isset( $infoObject->gender ) ? $infoObject->gender : "";
		$this->birthDate   = ( isset( $infoObject->birth_date ) && is_object( $infoObject->birth_date ) ) ? $infoObject->birth_date->year . "/" . $infoObject->birth_date->month . "/" . $infoObject->birth_date->day : "";
		$this->link        = isset( $infoObject->permalink ) ? $infoObject->permalink : "";
		$this->xid         = isset( $infoObject->id ) ? $infoObject->id : "";
		$this->details     = json_encode( $infoObject );
		$this->accessToken = $accessToken;
		$r                 = $this->save();

		$this->savedId     = $this->id;
		$this->savedAvatar = $this->avatar;

		return $r;
	}

	public function getLoginUrl() {

		return "/site/service/XINGRE.php";

		$this->XINGObject->Initialize();
		$url = null;
		$this->XINGObject->GetDialogURL( $url, Setting::getValue( "XING_CALLBACK_URL" ) );
		return $url;
	}

}

