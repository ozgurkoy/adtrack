<?php
/**
 * publisherBilling real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherBilling.php';

class PublisherBilling extends PublisherBilling_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Print The Payment as Html
	 * @author Sam
	 * @return string
	 */
	public function printHTML() {

		// 			$this->publisherWithdrawOption = $params[ "publisherWithdrawOptionId" ];
		//$this->details     = $params[ "details" ]; #json

		$HTML = "";
		$printFields = array('jdate', 'updateDate', 'billingName', 'taxOffice', 'taxNumber', 'address');

		$HTML .= sprintf("%s: %s <br/>","Name",$this->billingName);
		$HTML .= sprintf("%s: %s <br/>","Tax Office",$this->taxOffice);
		$HTML .= sprintf("%s: %s <br/>","Tax Number",$this->taxNumber);
		$HTML .= sprintf("%s: %s <br/>","Address",$this->address);
		$HTML .= sprintf("%s: %s <br/>","Create Date",Format::getDateTimeFormated($this->jdate));
		$HTML .= sprintf("%s: %s <br/>","Update Date",Format::getDateTimeFormated($this->updateDate));

		return $HTML;

	}

}

