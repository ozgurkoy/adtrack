<?php
	/**
	* userHistory real class for Jeelet - firstly generated on 30-09-2011 00:32, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/userHistory.php");

	class UserHistory extends UserHistory_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		public static function write($userID,$actionId,$actionText=null,$actionData=null,$madeBy=UserHistory::ENUM_MADEBY_USER){
			$his = new UserHistory();
			$his->user_userHistory = $userID;
			$his->actionId = $actionId;
			$his->actionText = $actionText;
			$his->actionData = $actionData;
			$his->madeBy = $madeBy;
			$his->save();
			unset($his);
		}
	}

	?>