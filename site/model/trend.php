<?php
	/**
	* trend real class for Jeelet - firstly generated on 24-05-2011 11:41, add edit anyway you like wont be touched over , ever again.
	*
	* @package jeelet
	* @author Özgür Köy
	**/
	include($__DP."/site/model/base/trend.php");

	class Trend extends Trend_base
	{
		/**
		 * constructor for the class
		 *
		 * @return void
		 * @author Özgür Köy
		 **/
		public function __construct($id=null)
		{
			parent::__construct($id);
		}

		/**
		 * load all active trends
		 *
		 * @return Trend
		 * @author Özgür Köy
		 **/
		public static function loadAll()
		{

			$t = new Trend();
			$t->orderBy( "title ASC" )->populateOnce( false )->load(array("active"=>1));
			return $t;
		}

		/**
		 * Get all active trends as object
		 *
		 * @return Trend
		 * @author Murat
		 */
		public static function getAll(){
			$t = new Trend();
			$t->populateOnce( true )->load(array("active"=>1));
			return $t;
		}

		/**
		 * Get all active trends as object
		 *
		 * @return Trend
		 * @author Murat
		 */
		public static function getAllArray(){
			$t = new Trend();
			$t->populateOnce( false )->load(array("active"=>1));

			return $t->_ids;
		}

		/**
		 * Get all active trends as array with translations
		 *
		 * @return Trend
		 * @author Murat
		 */
		public static function getAllWithLang($lang,$withSort = true){
			$t = new Trend();
			$t->populateOnce( false )->load(array("active"=>1));
			$trends = array();
			$tt=null;
			do{
				$tt = LangWord::getLangWords("word",$lang,$t->title);
				$trends[$t->id] = reset($tt);
			} while($t->populate());
			if ($withSort) asort($trends);
			return $trends;
		}

		/**
		 * Get all active trends labels for preferred language
		 *
		 * @return Trend
		 * @author Murat
		 */
		public static function getTrendLabels($lang){
			$t = new Trend();
			$t->populateOnce( false )->load(array("active"=>1));
			$trends = array();
			$tt=null;
			do{
				$tt = LangWord::getLangWords("word",$lang,$t->title);
				$trends[$t->title] = reset($tt);
			} while($t->populate());
			//print_r($trends);
			//exit;
			return $trends;
		}

	}

	?>