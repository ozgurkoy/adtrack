<?php
//use Swagger\Annotations as SWG;

use Swagger\Annotations\Operation;
use Swagger\Annotations\Operations;
use Swagger\Annotations\Parameter;
use Swagger\Annotations\Parameters;
use Swagger\Annotations\Api;
use Swagger\Annotations\ErrorResponse;
use Swagger\Annotations\ErrorResponses;
use Swagger\Annotations\Resource;
use Swagger\Annotations\AllowableValues;

use Swagger\Annotations\Properties;
use Swagger\Annotations\Property;
use Swagger\Annotations\Model;
use Swagger\Annotations\Items;


/**
 * php53 swagger.phar -p ../../admingleV2/_dev/site/model/ -o 2
 * http://localhost/swagger/swagger-php/2/api-docs.json
 * http://zircote.com/swagger-php/annotations.html#model
 * http://docs.phonegap.com/en/2.9.0/cordova_device_device.model.md.html#device.model
 *
 * @package
 * @category
 * @subpackage
 *ֿֿֿ
 * @Resource(
 *  apiVersion="0.1",
 *  swaggerVersion="1.1",
 *  basePath="http://n.admingle.com/wsv2/user",
 *  resourcePath="/user"
 * )
 *
 */
include($__DP . "/site/model/wsObjects.php");
class v2wsUser
{
	protected $base;


	function __construct()
	{
		$this->base = v2wscore::getInstance();
	}


	/**
	 * Forgotten password reminder for user
	 *
	 * @return void
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/forgotpass",
	 *            description="Forget Password",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="user Forgot Password",
	 *            responseClass="AcceptResponse",
	 *            nickname="UserForget",
	 * @parameters(
	 * @parameter(
	 *           name="email",
	 *           description="User email address",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 * @errorResponses  (
	 * @errorResponse(
	 *            code="404",
	 *            reason="User Not Found"
	 *          ),
	 * @errorResponse(
	 *            code="406",
	 *            reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function forgotPassword()
	{
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {

			$singleValueReq = new SingleValueReq($this->base->_request->json);

			$res = new AcceptResponse();
			if (User::forgotPass($singleValueReq->value))
			{
				// sent mail
				$res->error=null;
				$res->value = true;
			}else{
				// Not found
				$res->error=new ErrResponse(404,"not found");
				$res->value = false;
			}

			$this->base->sendResponse(200,$res->getJsonData());
		}
	}


	/**
	 * Login user with given username and password
	 * Get UserName and Password (UserName can be email)
	 * @return LoginResponse
	 * @author    Sam
	 *
	 * @Api(
	 *            path="/login",
	 *            description="UserLogin",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Login For Registered User",
	 *            responseClass="LoginResponse",
	 *            nickname="LoginUser",
	 * @parameters(
	 * @parameter(
	 *           name="json",
	 *           description="User Login Data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="loginReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="406",
	 *            reason="Invalid input"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function login()
	{

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$loginReq = new loginReq($this->base->_request->json);
		}

		if (!isset($loginReq->username) || !isset($loginReq->password)) {
			$res = new LoginResponse();
			$res->error = new ErrResponse(666,(!isset($loginReq->username) ? "username" : "password"));
			$this->base->sendResponse(200,$res->getJsonData());
		} else {
			//try username

			$u = new User();
			$isLogged = $u->login($loginReq->username,$loginReq->password,null,true);


			if($isLogged && $u->utype == User::ENUM_UTYPE_PUBLISHER) {

				$token = JUtil::randomString();

				// Get or create the device
				$pmd = PublisherMobileDevice::getUserDevice($u,$loginReq->device);

				// connect device to User
				$u->user_publisher->savePublisher_publisherMobileDevice($pmd);
				$u->save();

				$res = new LoginResponse();
				$res->Token = $this->base->encrypt($pmd->authKey);
				$res->userName = $u->username;
				$res->userNameSurname = $u->user_publisher->name;
				$res->registrationComplete = $u->user_publisher->isRegistrationComplete();

				$this->base->sendResponse(200,$res->getJsonData());

			} else {
				$res = new LoginResponse();
				$res->error = new ErrResponse(502);
				$this->base->sendResponse(200,$res->getJsonData());
			}
		}

	}

	/**
	 * Dashboard contents for user
	 * Get Start Messages , filter (read , unread),
	 * @return void
	 * @author    Murat
	 * @Api(
	 *            path="/messages",
	 *            description="Get All User System Messages",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Get Messages",
	 *            responseClass="MessagesResponse",
	 *            nickname="GetMessages",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="pagedata",
	 *           description="The currect messages page",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="MessageListRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="404",
	 *            reason="User Not Found"
	 *          ),
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="406",
	 *            reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function messages()
	{
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {

			$messageListRequest = new MessageListRequest($this->base->_request->json);
			//$p = new Publisher();
			$p = $this->base->_publisher;

			$params = array("uId"=>$p->user_publisher->id,"page"=>$messageListRequest->page);
			$r = UserMessage::getUserMessages($params);

			$response = new MessagesResponse();
			$response->recordCount = $r["recordCount"];
			$response->currentPage = $r["currentPage"];
			$response->messages = array();
			foreach ($r["messages"] as $message){
				$m= new MessageResponse();
				$m->messageID = $message["id"];
				$m->date = $message["date"];
				$m->type = $message["msgType"];
				$m->title = $message["title"];
				$m->read = $message["readed"];

				$response->messages[] = $m;
				unset($m);
			}

			$this->base->sendResponse(200, $response->getJsonData());
		}
	}

	/**
	 * Dashboard detail content
	 * Get Message ID
	 * @return void
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/messagesDetail",
	 *            description="Get Full Messages",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Get full Message",
	 *            responseClass="FullMessagesResponse",
	 *            nickname="GetFullMessage",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="umid",
	 *           description="Unique message ID",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="404",
	 *            reason="Message Not Found"
	 *          ),
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="406",
	 *            reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function messagesDetail()
	{
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {

			$umid = new SingleValueReq($this->base->_request->json);

			//$p = new Publisher();
			$p = $this->base->_publisher;

			$response = new FullMessagesResponse();
			if(!isset($umid->value)){
				$response->error = new ErrResponse(88,"Mandatory field missing");
				$this->base->sendResponse(200, $response->getJsonData());
			}

			$r = UserMessage::getUserMessage($p->user_publisher->id,$umid->value);

			if(is_null($r))
			{
				$response->error = new ErrResponse(404,"Message not found");

			}else{
				$response->messageID = $r["id"];
				$response->date = $r["date"];
				$response->message = $r["message"];
				$response->title = $r["title"];
				$response->campaign = $r["campaign"];
				$response->read = $r["readed"];
				$response->type = $r["msgType"];
			}
			$this->base->sendResponse(200, $response->getJsonData());
		}
	}


	/**
	 * Set Message Status
	 * @return void
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/messagesAction",
	 *            description="Change Message status",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Set Message status",
	 *            responseClass="AcceptResponse",
	 *            nickname="messagesAction",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="data",
	 *           description="messages ids for action",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="messageActionRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="404",
	 *            reason="Message Not Found"
	 *          ),
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="406",
	 *            reason="Parameter Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function messagesAction()
	{
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {

			$data = new messageActionRequest($this->base->_request->json);

			//$p = new Publisher();
			$p = $this->base->_publisher;

			$r = UserMessage::setUserMessageStatus($p->user_publisher->id,$data->messagesId,$data->action);

			$response = new AcceptResponse();
			if($r === false)
			{
				$response->error = new ErrResponse(404,"Action not Found");
				$response->value=false;

			}else{
				$response->value=true;
			}
			$this->base->sendResponse(200, $response->getJsonData());
		}
	}



	/**
	 * Check the token is valid
	 *
	 * @return bool
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/savePhoneNumber",
	 *            description="Check If number is valid and send SMS",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="AcceptResponse",
	 *            nickname="savePhoneNumber",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="data",
	 *           description="phone number",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="signUpPhoneValidateRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function savePhoneNumber()
	{
		$this->base->tokenCheck();

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$data = new signUpPhoneValidateRequest($this->base->_request->json);

			//$p = new Publisher();
			$p = $this->base->_publisher;

			// TOdo : Create Phone validation
			$res = new AcceptResponse();
			$res->value=true;

			$this->base->sendResponse(200,$res->getJsonData());
		}


	}

	/**
	 * Check the token is valid
	 *
	 * @return bool
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/validatePhoneNumber",
	 *            description="Check If the sms code is valid",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="AcceptResponse",
	 *            nickname="validatePhoneNumber",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="data",
	 *           description="SMS Code",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="SingleValueReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function validatePhoneNumber()
	{
		$this->base->tokenCheck();

		$res = new AcceptResponse();
		$res->value=true;

		$this->base->sendResponse(200,$res->getJsonData());
	}

	/**
	 * Check the token is valid
	 *
	 * @return bool
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/contactUs",
	 *            description="Check If the sms code is valid",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="AcceptResponse",
	 *            nickname="validatePhoneNUmber",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="data",
	 *           description="contactForm",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="ContactUsRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function contactUs()
	{
		$this->base->tokenCheck();

		$cur = new ContactUsRequest($this->base->_request->json);
		$cur->ip = $_SERVER["REMOTE_ADDR"];
		$cur->publisher = $this->base->_publisher->id;

		$r = ValidateUser::validateContactUs($cur->getArrayData());

		$res = new AcceptResponse();

		if($r === true)
		{
			Contact::createNew($cur->getArrayData());
		}
		else
		{
			$res->error = new ErrResponse($r->errorCode,$r->errorDetail);
		}

		$this->base->sendResponse(200,$res->getJsonData());
	}

	/**
	 * Check the token is valid
	 *
	 * @return bool
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/check",
	 *            description="Check If user Tiken is Valid",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="AcceptResponse",
	 *            nickname="CheckToken",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function checkToken()
	{
		$this->base->tokenCheck();

		$res = new AcceptResponse();
		$res->value=true;

		$this->base->sendResponse(200,$res->getJsonData());
	}

	/**
	 * Logout the user and destroy the token
	 *
	 * @return bool
	 * @author    Murat
	 * @Api(
	 *            path="/logout",
	 *            description="User LogOut",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="AcceptResponse",
	 *            nickname="logout",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function logOut()
	{
		$this->base->tokenCheck();

		$res = new AcceptResponse();
		//$p = new Publisher();
		$p = $this->base->_publisher;

		if(PublisherMobileDevice::deActiveDevice($p->id,$this->base->getToken())){

			$res->value=true;
		}else{
			$res->value=false;
		}


		$this->base->sendResponse(200,$res->getJsonData());

	}

	/**
	 * Twitter Call Back method for publisher
	 *
	 * @return void
	 * @author    Sam
	 * @Api(
	 *            path="/twitter",
	 *            description="twitter Callback",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="SNLoginResponse",
	 *            nickname="twitter",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token - only after user register",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="json",
	 *           description="User login Data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="TwitterLoginReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="400",
	 *            reason="Twitter Auth Problem"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function twitterCallBack()
	{
		global $__DP;

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {

			$twitterData = new TwitterLoginReq($this->base->_request->json);
			$response    = new SNLoginResponse();
			$response->error= null;

			if(isset($this->base->_request->token))
			{
				// Add twitter to existing user
				$this->base->tokenCheck();

				$twResponse = SnTwitter::addTwitterAccount($twitterData->oauth_token,$twitterData->oauth_token_secret);

				if(is_bool($twResponse))
				{
					// cant add user
					$error = new ErrResponse(250);
					$error->errorDetail="cant add Twitter account, bad access token or other user ";
					$response->error= $error;
				}
				else
				{
					//$p = new Publisher();
					$p = $this->base->_publisher;
					$this->base->_publisher->addSnTwitter($twResponse);
					$response->newUser =false;
					$response->registrationComplete =$this->base->_publisher->isRegistrationComplete();
				}
			}
			else{

				// login with twitter
				$twResponse = SnTwitter::loginTwitterAccount($twitterData->twitter_id,$twitterData->oauth_token,$twitterData->oauth_token_secret);


				if(is_bool($twResponse))
				{
					$error = new ErrResponse(404);
					$error->errorDetail="user not found";
					$response->error= $error;
					$response->newUser =true;
					$response->registrationComplete =false;
				}
				else{
					$twResponse->loadPublisher();
					if ( isset( $twResponse->publisher_snTwitter ) ) {
						$pmd = PublisherMobileDevice::createNewDevice( $twitterData->device );
						$twResponse->publisher_snTwitter->savePublisher_publisherMobileDevice( $pmd );
						$response->newUser              = false;
						$response->registrationComplete = $twResponse->publisher_snTwitter->isRegistrationComplete();
						$response->userToken            = $this->base->encrypt( $pmd->authKey );
					}
					else {
						$response->newUser              = true;
						$response->registrationComplete = false;
						$response->userToken            = null;
					}

				}
			}

			$this->base->sendResponse(200, $response->getJsonData());
		}

	}



	/**
	 * Facebook Call Back method for publisher
	 *
	 * @return void
	 * @author    Sam
	 * @Api(
	 *            path="/facebook",
	 *            description="Facebook Callback",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Facebook Call",
	 *            responseClass="SNLoginResponse",
	 *            nickname="facebook",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token - only after user register",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="json",
	 *           description="Facebook login Data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="FacebookLoginReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 *  @errorResponse(
	 *            code="230",
	 *            reason="Facebook Account registered to other adMingle User"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function facebookCallBack()
	{
		global $__DP;

		if (!isset($this->base->_request->json)) {

			$this->base->sendResponse(406);
		}
		else {

			$facebookData    = new FacebookLoginReq($this->base->_request->json);
			$response        = new SNLoginResponse();
			$response->error = null;

			if(isset($this->base->_request->token))
			{
				// Add Facebook
				$this->base->tokenCheck();

				$fbResponse = SnFacebook::addFacebookAccount( $facebookData->facebook_id, $facebookData->facebook_token );

				if ( is_bool( $fbResponse ) ) {

					// cant add user
					$error              = new ErrResponse( 250 );
					$error->errorDetail = "cant add Facebook account, bad access token or other user ";
					$response->error    = $error;
				}
				else
				{
//					$p = new Publisher();
					$p = $this->base->_publisher;
					$p->hasFacebook = true;
					$p->savePublisher_snFacebook($fbResponse);
					$p->save();

					$response->newUser              = false;
					$response->registrationComplete = $p->isRegistrationComplete();
				}
			}
			else{
				// login
				$fbResponse = SnFacebook::loginFacebookAccount($facebookData->facebook_id,$facebookData->facebook_token);
				if ( is_bool( $fbResponse ) ) {
					$error              = new ErrResponse( 404 );
					$error->errorDetail = "user not found";

					$response->error                = $error;
					$response->newUser              = true;
					$response->registrationComplete = false;
				}
				else {
					$fbResponse->loadPublisher();

					if ( $fbResponse->publisher_snFacebook->gotValue ) {

						$pmd = PublisherMobileDevice::createNewDevice( $facebookData->device );
						$fbResponse->publisher_snFacebook->savePublisher_publisherMobileDevice( $pmd );

						$response->newUser              = false;
						$response->registrationComplete = $fbResponse->publisher_snFacebook->isRegistrationComplete();
						$response->userToken            = $this->base->encrypt( $pmd->authKey );
					}
					else {
						$response->newUser              = true;
						$response->registrationComplete = false;
						$response->userToken            = null;
					}
				}
			}

			$this->base->sendResponse(200, $response->getJsonData());
		}
	}

	/**
	 * Google Call Back method for publisher
	 *
	 * @return void
	 * @author    Ozgur
	 * @Api(
	 *            path="/google",
	 *            description="Google Callback",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="SNLoginResponse",
	 *            nickname="google",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token - only after user register",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="json",
	 *           description="User access_token",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="GoogleLoginReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function googleCallBack()
	{
		global $__DP;
		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
			$req = new GoogleLoginReq( $this->base->_request->json );

			$response        = new SNLoginResponse();
			$response->error = null;
			$g               = new SnGoogle();

			// Check login info
			$googleInfo = $g->checkLoginInfo( (array)$req );

			if ( is_bool($googleInfo) ) {
				// Error on auth
				$error              = new ErrResponse( 250 );
				$error->errorDetail = "cant validate google account, bad access token ";
				$response->error    = $error;

				$this->base->sendResponse( 200, $response->getJsonData() );
			}
			else{
				// Valid user

				if ( isset( $this->base->_request->token ) ) {
					// Add google
					$this->base->tokenCheck();

					$accountAdded = $g->addAccount( (array)$req, $googleInfo );

					if ( $accountAdded == false ) {
						// Error on adding user

						// Check if the account re
						$error              = new ErrResponse( 250 );
						$error->errorDetail = "cant add google account, bad access token or other user, refresh token problem ";
						$response->error    = $error;

						$this->base->sendResponse( 200, $response->getJsonData() );
					}
					else {

						$p = $this->base->_publisher;
						//$p = new Publisher();

						if ( is_null( $g->publisher_snGoogle ) ) {
							$p->hasGPlus = true;
							$p->save();
							$p->savePublisher_snGoogle( $g );
						}
						else {
							$g->loadPublisher();
							if ( $g->publisher_snGoogle->id != $p->id ) {
								$error              = new ErrResponse( 251 );
								$error->errorDetail = "User belong to other user, can't add user ";
								$response->error    = $error;
							}
						}

						$response->newUser              = false;
						$response->registrationComplete = $p->isRegistrationComplete();
						$this->base->sendResponse( 200, $response->getJsonData() );
					}
				}
				else {
					// login
					$googleResponse = $g->login( (array)$req, $googleInfo );

					if ( $googleResponse == false ) {
						$error              = new ErrResponse( 404 );
						$error->errorDetail = "user not found";

						$response->error                = $error;
						$response->newUser              = true;
						$response->registrationComplete = false;
					}
					else {
						$googleResponse->loadPublisher();

						if ( isset( $googleResponse->publisher_snGoogle ) ) {
							$pmd = PublisherMobileDevice::createNewDevice( $req->device );
							$googleResponse->publisher_snGoogle->savePublisher_publisherMobileDevice( $pmd );

							$response->newUser              = false;
							$response->registrationComplete = $googleResponse->publisher_snGoogle->isRegistrationComplete();
							$response->userToken            = $this->base->encrypt( $pmd->authKey );
						}
					}
				}
			}

			$this->base->sendResponse( 200, $response->getJsonData() );


			/*// Make Twiiter Check and save userin Daabase

			$response             = new SNLoginResponse();
			$response->newUser    = 1;
			$response->userAvater = "http://avatar/img.png";
			$response->userName   = "Mr Sam";
			$response->userToken  = "jffqjkfqf3289h82";

			$this->base->sendResponse(200, $response->getJsonData());*/
		}

	}

	/**
	 * Get User Settings
	 *
	 * @Api(
	 *            path="/settings",
	 *            description="User Settings",
	 * @operations(
	 * @operation(
	 *            httpMethod="GET",
	 *            summary="Get User settings",
	 *            responseClass="settingsResponse",
	 *            nickname="logout",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     ),
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="update User Setting",
	 *            responseClass="AcceptResponse",
	 *            nickname="logout",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="Insert User Settings",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="PushNotification",
	 *           description="Set user Push Notification",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="settingsResponse"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function settings()
	{
		//die(print_r($this->base,true));
		$this->base->tokenCheck();

		//$p = new Publisher();
		$p = $this->base->_publisher;
		$pmd = new PublisherMobileDevice();
		$pmd->load(array("authKey" => $this->base->getToken()));


		$response = new settingsResponse();
		if(!$pmd->gotValue)
		{
			$response->error = new ErrResponse(404,"not found");
		}else{

			$p->loadUser();
			$response = new settingsResponse();

			switch ($this->base->_request->getMethod()) {
				case "get":
				{
					$response->language     = $p->user_publisher->lang;
					$response->campaignNotification = $pmd->enableNotification;
					$response->messagesNotification = $pmd->enableMessagesNotification;
					break;
				}
				case "post":
				{
					$settings = new settingsResponse($this->base->_request->json);


					if(isset($settings->language))
					{
						$lang = new Lang();

						$lang->limit(1)->load(array("langShort"=>$settings->language));
						$p->user_publisher->lang = $lang;
						$p->user_publisher->save();
					}

					if(isset($settings->campaignNotification))
					{
						$pmd->enableNotification = $settings->campaignNotification;

					}
					if(isset($settings->messagesNotification))
					{
						$pmd->enableMessagesNotification = $settings->messagesNotification;

					}

					if(isset($settings->campaignNotification) || isset($settings->messagesNotification))
					{
						$pmd->save();
					}

					$response = new AcceptResponse();
					$response->value= true;
					break;
				}
			}
		}
		$this->base->sendResponse(200, $response->getJsonData());
	}


	/**
	 * Get User Settings
	 *
	 * @Api(
	 *            path="/howtomakemoremoney",
	 *            description="How to make more money",
	 * @operations(
	 * @operation(
	 *            httpMethod="Post",
	 *            summary="Get Texts for how to make more money screen",
	 *            responseClass="AcceptResponse",
	 *            nickname="howtomakemoremoney",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token login or signup",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function howtomakemoremoney()
	{
		//die(print_r($this->base,true));
		$this->base->tokenCheck();

		//$p = new Publisher();
		$p = $this->base->_publisher;

		//Get Publisher Language

		//$this->base->sendResponse(200, $response->getJsonData());

		JT::init();
		echo JT::pfetch("info_makemoremoney");
	}

	/**
	 * Get User Settings
	 *
	 * @Api(
	 *            path="/customMessage",
	 *            description="Get a special message for the user",
	 * @operations(
	 * @operation(
	 *            httpMethod="Post",
	 *            summary="Get Texts for how to make more money screen",
	 *            responseClass="AcceptResponse",
	 *            nickname="customMessage",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token login or signup",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function customMessage()
	{
		//die(print_r($this->base,true));
		$this->base->tokenCheck();

		//$p = new Publisher();
		$p = $this->base->_publisher;

		//Get Publisher Language

		$response = new AcceptResponse();
		$response->error=null;

		if(rand(1,10)%2 == 0 )
		{
			$response->value = "New message for the user maybe it will be gone next time";
		}else{
			$response->value = "";
		}

		$response->value = "";

		$this->base->sendResponse(200, $response->getJsonData());
	}


	/**
	 * Image method for publisher
	 *
	 * @return void
	 * @author    Sam
	 * @Api(
	 *            path="/image",
	 *            description="update Image for publisher",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Image for user",
	 *            responseClass="AcceptResponse",
	 *            nickname="image",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token login or signup",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="ImageType",
	 *           description="1- profile , 2-background",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="406",
	 *            reason="Bad parameters"
	 *          ),
	 * @errorResponse(
	 *            code="206",
	 *            reason="image file extension isn't allowed"
	 *          ),
	 * @errorResponse(
	 *            code="207",
	 *            reason="image file size is too big"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function pubImage()
	{
		$this->base->tokenCheck();

		$params = JUtil::getSearchParameter();
		$ar = new AcceptResponse();

		if (!empty($_FILES) && !empty($params["ImageType"])){
			$file = $_FILES[key($_FILES)];

			$vr = ValidateUser::validateImageFile($file);

			if ($vr === true){
				switch($params["ImageType"]){
					case 1: //Profile
					case 2: //Background
						$this->base->_publisher->changeImage($params["ImageType"],$file);
						$ar->value = true;
						break;
					default:
						$ar->error = new ErrResponse(406,"Bad parameters : ImageType");
						break;
				}

			} else {
				$ar->error = new ErrResponse($vr->errorCode,$vr->errorDetail);
			}
		} else {
			$ar->error = new ErrResponse(406,"Bad parameters");
		}

		$this->base->sendResponse(200,$ar->getJsonData());

	}

	/**
	 * Sign-up method for publisher
	 *
	 * @return void
	 * @author    Sam
	 * @Api(
	 *            path="/signUp",
	 *            description="sign up new user",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="signup new user",
	 *            responseClass="signUpResponse",
	 *            nickname="signUp",
	 * @parameters(
	 * @parameter(
	 *           name="userData",
	 *           description="User Token",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="signUpRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="406",
	 *            reason="Invalid input"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function pubSignUp()
	{
		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		}


		$signUpRequest = new signUpRequest($this->base->_request->json);


		$r = ValidateUser::validateSignupStep1($signUpRequest->getArrayData(),true);

		$res = new SignUpResponse();

		if(is_bool($r))
		{
			$res->error=null;

			// Created user by the type and email
			$user = User::signUp(user::ENUM_UTYPE_PUBLISHER,$signUpRequest->getArrayData());

			// Create device
			$pmd = PublisherMobileDevice::createNewDevice($signUpRequest->device);


			// Connect device to publisher
			$user->user_publisher->savePublisher_publisherMobileDevice($pmd);

			// For Testing -
			$user->status = User::ENUM_STATUS_ACTIVE;
			$user->save();


			$res->Token = $this->base->encrypt($pmd->authKey);

		}
		else
		{
			// Make extra check for time out
			if($r->errorCode == 202 || $r->errorCode == 203)
			{
				$u = new User();
				$isLogged = $u->login($signUpRequest->username,$signUpRequest->password,null,true);

				if($isLogged && $u->utype == User::ENUM_UTYPE_PUBLISHER) {

					// Get or create the device
					$pmd = PublisherMobileDevice::getUserDevice($u,$signUpRequest->device);

					$res->error=null;
					$res->Token = $this->base->encrypt($pmd->authKey);
				}else{
					$res->error = new ErrResponse($r->errorCode,$r->errorDetail);
				}

			}else{
				$res->error = new ErrResponse($r->errorCode,$r->errorDetail);
			}

		}

		$this->base->sendResponse(200,$res->getJsonData());

	}

	/**
	 * update user for publisher signup form an edit profile
	 * @Api(
	 *            path="/UserData",
	 *            description="sign up new user",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Updating new/existing User",
	 *            responseClass="AcceptResponse",
	 *            nickname="updateProfile",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token login or signup",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="userData",
	 *           description="User extra Data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="signUpDataRequest"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="403",
	 *            reason="UserName Taken"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function pubSignUpFormData()
	{
		$this->base->tokenCheck();

		//$p = new Publisher();
		$p = $this->base->_publisher;


		$signUpDataRequest = new SignUpDataRequest($this->base->_request->json);

		$params = $signUpDataRequest->getArrayData();

		$r = ValidateUser::validateSignupStep2($params,true);

		$res = new AcceptResponse();

		if(is_bool($r))
		{
			$res->error=null;

			$p->updateProfile($params);
			$res->value=true;
			$u = $p->loadUser()->signupComplete();

			if(isset($params["email"])){
				if(!$u->setDefaultEmail($params["email"]))
				{
					$res->value=false;
					$res->error = new ErrResponse("400","Email Already Exist");
				}
			}
		}
		else
		{
			$res->error = new ErrResponse($r->errorCode,$r->errorDetail);
		}

		$this->base->sendResponse(200,$res->getJsonData());
	}

	/**
	 * Publisher profile save method
	 *
	 * @return void
	 * @author  Sam
	 * @Api(
	 *            path="/profile",
	 *            description="get the user full profile",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="used on profile page",
	 *            responseClass="UserProfileResponse",
	 *            nickname="profile",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token from social network",
	 *           paramType="header",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          ),
	 * @errorResponse(
	 *            code="404",
	 *            reason="User Not Found"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function pubProfile()
	{
		$this->base->tokenCheck();

		//$p = new Publisher();
		$p = $this->base->_publisher;

		$res = new UserProfileResponse();

		if(is_null($p))
		{
			$res->error = new ErrResponse(404,"not found");
		}
		else
		{
			$res->error=null;

			$p->loadUser();
			$p->user_publisher->loadUser_userEmail();
			$res->email = $p->user_publisher->user_userEmail->email;
			$res->username = $p->user_publisher->username;
			$res->fb = $p->hasFacebook;
			$res->tw = $p->hasTwitter;
			$res->gp = $p->hasGPlus;
			// calculated data
			//$res->publisherLevel = "Expert Publisher";
			$res->publisherLevel = "";
			$res->campaigns = $p->getCampaignCount(Campaign::ENUM_STATE_ACCEPTED);
			$res->totalRevenue = Format::getFormattedCurrency($p->currentBalance);
			//$res->trueReach = JUtil::randomNumber(500,1000000);
			$res->trueReach = "";
			$res->socialConnection = $p->socialConnectionCount();

			if (!is_null($p->profileImage)){
				$res->avatar = JUtil::siteAddress() . "site/layout/images/publisher/" . $p->profileImage;
			} else {
				$res->avatar = JUtil::siteAddress() . "site/layout/images/noavatar.png?r=".JUtil::randomString(5);
			}
			if (!is_null($p->backgroundImage)){
				$res->background = JUtil::siteAddress() . "site/layout/images/publisher/" . $p->backgroundImage;
			} else {
				$res->background = JUtil::siteAddress() . "site/layout/images/mobile_profile_background.jpg?r=".JUtil::randomString(5);
			}

			$res->signUpDataRequest = new signUpDataRequest();
			$res->signUpDataRequest->bDay = $p->bday;
			$res->signUpDataRequest->country = $p->country;
			$res->signUpDataRequest->city = $p->city;
			$res->signUpDataRequest->address = $p->address;
			$res->signUpDataRequest->name = $p->name;
			$res->signUpDataRequest->surname = $p->surname;
			$res->signUpDataRequest->education = $p->education;
			$res->signUpDataRequest->professional = $p->professional;
			$p->loadTrends();
			$res->signUpDataRequest->interests = $p->trends->_ids;
			$res->signUpDataRequest->gender = $p->gender;
			$res->signUpDataRequest->otherInfo = $p->otherInfo;
			$gsm = new signUpPhoneValidateRequest();
			$gsm->gsmCode = $p->gsmNumberCode;
			$gsm->gsmNumber = $p->gsmNumber;
			$res->signUpDataRequest->gsm = $gsm;


		}

		$this->base->sendResponse(200,$res->getJsonData());
	}


	/**
	 * Save invite users
	 *
	 * @return void
	 * @author    Sam
	 * @Api(
	 *            path="/invite",
	 *            description="User invite data",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="invite Call",
	 *            responseClass="AcceptResponse",
	 *            nickname="invite",
	 * @parameters(
	 * @parameter(
	 *           name="json",
	 *           description="invite Data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="InviteReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="400",
	 *            reason="Email Exist"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function invite()
	{
		if (!isset($this->base->_request->json)) {

			$this->base->sendResponse(406);
		}
		else {

			$inviteData = new InviteReq($this->base->_request->json);
			$response = new AcceptResponse();

			if(!Validate::validateEmail($inviteData->email)){
				$response->value = false;
				$response->error = new ErrResponse(88,"Email not Valid");
				$this->base->sendResponse(200, $response->getJsonData());
			}

			if(!Validate::checkValidCountry($inviteData->country_id)){
				$response->value = false;
				$response->error = new ErrResponse(88,"Country Code not Valid");
				$this->base->sendResponse(200, $response->getJsonData());
			}

			$a = new InviteApp();
			$a->load(array("email"=>$inviteData->email,"countryCode"=>$inviteData->country_id));

			if($a->gotValue){
				$response->value = false;
				$response->error = new ErrResponse(400,"Email Exist");
			}else{
				$a->email = $inviteData->email;
				$a->countryCode = $inviteData->country_id;
				$a->ip = $_SERVER["REMOTE_ADDR"];
				$a->deviceModel = $inviteData->device->device_Model;
				$a->devicePlatform = $inviteData->device->device_Platform;
				$a->deviceUUID = $inviteData->device->device_UUID;
				$a->save();

				$response->value = true;

			}

			$this->base->sendResponse(200, $response->getJsonData());
		}
	}
	/**
	 * User validation
	 * @return LoginResponse
	 * @author    Murat
	 *
	 * @Api(
	 *            path="/validate",
	 *            description="UserValidate",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="Login For Registered User",
	 *            responseClass="LoginResponse",
	 *            nickname="LoginUser",
	 * @parameters(
	 * @parameter(
	 *           name="json",
	 *           description="Validate Data",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="validateToken"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="406",
	 *            reason="Invalid input"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function validate()
	{

		if (!isset($this->base->_request->json)) {
			$this->base->sendResponse(406);
		} else {
			$validateToken = new validateToken($this->base->_request->json);
		}

		if (!isset($validateToken->token)) {
			$res = new LoginResponse();
			$res->error = new ErrResponse(666,"token");
			$this->base->sendResponse(200,$res->getJsonData());
		} else {
			$codeObj = ReplaceCode::isCodeValid( ReplaceCode::ENUM_TYPE_CONFIRMATION, $validateToken->token );
			if ( $codeObj !== false ) {
				$good = $codeObj->loadUser()->confirmUser( $validateToken->token );
			}
			else
				$good = false;
			//try username



			if($good) {
				$u = $codeObj->user_replaceCode;
				if ($u->utype == User::ENUM_UTYPE_PUBLISHER){
					$token = JUtil::randomString();

					// Get or create the device
					$pmd = PublisherMobileDevice::getUserDevice($u,$validateToken->device);

					// connect device to User
					$u->user_publisher->savePublisher_publisherMobileDevice($pmd);
					$u->save();

					$res = new LoginResponse();
					$res->Token = $this->base->encrypt($pmd->authKey);
					$res->userName = $u->username;
					$res->userNameSurname = $u->user_publisher->name;
					$res->registrationComplete = $u->user_publisher->isRegistrationComplete();

					$this->base->sendResponse(200,$res->getJsonData());
				} else {
					$res = new LoginResponse();
					$res->error = new ErrResponse(527);
					$this->base->sendResponse(200,$res->getJsonData());
				}
			} else {
				$res = new LoginResponse();
				$res->error = new ErrResponse(527);
				$this->base->sendResponse(200,$res->getJsonData());
			}
		}

	}

	/**
	 * VK Call Back method for publisher
	 *
	 * @return void
	 * @author    Murat
	 * @Api(
	 *            path="/vk",
	 *            description="VK Callback",
	 * @operations(
	 * @operation(
	 *            httpMethod="POST",
	 *            summary="User Token",
	 *            responseClass="SNLoginResponse",
	 *            nickname="google",
	 * @parameters(
	 * @parameter(
	 *           name="token",
	 *           description="User Token - only after user register",
	 *           paramType="header",
	 *           required="false",
	 *           allowMultiple="false",
	 *           dataType="string"
	 *         ),
	 * @parameter(
	 *           name="json",
	 *           description="User access_token",
	 *           paramType="body",
	 *           required="true",
	 *           allowMultiple="false",
	 *           dataType="GoogleLoginReq"
	 *         )
	 *       ),
	 * @errorResponses(
	 * @errorResponse(
	 *            code="403",
	 *            reason="Bad token"
	 *          )
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function vkCallBack()
	{
		global $__DP;
		if ( !isset( $this->base->_request->json ) ) {
			$this->base->sendResponse( 406 );
		}
		else {
			$req = new VKLoginReq( $this->base->_request->json );

			$response        = new SNLoginResponse();
			$response->error = null;
			$vk              = new SnVK();

			// Check login info
			$vkInfo = $vk->checkLoginInfo($req->user_id, $req->access_token );

			if ( $vkInfo === false) {
				// Error on auth
				$error              = new ErrResponse( 250 );
				$error->errorDetail = "cant validate vk account, bad access token ";
				$response->error    = $error;

				$this->base->sendResponse( 200, $response->getJsonData() );
			}
			else{
				// Valid user

				if ( isset( $this->base->_request->token ) ) {
					// Add VK
					$this->base->tokenCheck();

					$accountAdded = $vk->addAccount( $req->user_id, $vk->getInfo() );

					if ( $accountAdded == false ) {
						// Error on adding user

						// Check if the account re
						$error              = new ErrResponse( 250 );
						$error->errorDetail = "cant add VK account, bad access token or other user, refresh token problem ";
						$response->error    = $error;

						$this->base->sendResponse( 200, $response->getJsonData() );
					}
					else {

						$p = $this->base->_publisher;
						//$p = new Publisher();

						if ( is_null( $vk->publisher_snVK ) ) {
							$p->hasVK = true;
							$p->save();
							$p->savePublisher_snVK( $vk );
						}
						else {
							$vk->loadPublisher();
							if ( $vk->publisher_snVK->id != $p->id ) {
								$error              = new ErrResponse( 251 );
								$error->errorDetail = "User belong to other user, can't add user ";
								$response->error    = $error;
							}
						}

						$response->newUser              = false;
						$response->registrationComplete = $p->isRegistrationComplete();
						$this->base->sendResponse( 200, $response->getJsonData() );
					}
				}
				else {
					// login
					$vkResponse = $vk->login( $req->user_id, $vk->getInfo() );

					if ( $vkResponse == false ) {
						$error              = new ErrResponse( 404 );
						$error->errorDetail = "user not found";

						$response->error                = $error;
						$response->newUser              = true;
						$response->registrationComplete = false;
					}
					else {
						$vkResponse->loadPublisher();

						if ( isset( $vkResponse->publisher_snVK ) ) {
							$pmd = PublisherMobileDevice::createNewDevice( $req->device );
							$vkResponse->publisher_snVK->savePublisher_publisherMobileDevice( $pmd );

							$response->newUser              = false;
							$response->registrationComplete = $vkResponse->publisher_snVK->isRegistrationComplete();
							$response->userToken            = $this->base->encrypt( $pmd->authKey );
						}
					}
				}
			}

			$this->base->sendResponse( 200, $response->getJsonData() );
		}

	}


}