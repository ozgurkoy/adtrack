<?php
/**
 * campaignCPC real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/campaignCPC.php';

class CampaignCPC extends CampaignCPC_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Send budget extend offer mail to advertiser
	 *
	 * @return void
	 * @author Murat
	 */
	public function sendExtendMail(){
		//todo:@murat: Implement this function when writing advertiser module
		$this->extendMailSent = 1;
		$this->save();
	}

	/**
	 * Set campaign ending alert job for cron
	 *
	 * @return void
	 * @author Murat
	 */
	public function setFinishingMailJob(){
		Cron::create(
			Cron::ENUM_TYPE_CAMPAIGN_FINISH_ALERT,
			time(),
			null,
			$this->campaign_campaignCPC
		);
		$this->finishingAlertSent = 1;
		$this->save();
	}

}

