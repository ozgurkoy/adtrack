<?php
/**
 * snTwitterHistory real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/snTwitterHistory.php';

class SnTwitterHistory extends SnTwitterHistory_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public static function copyFromSnTwitter(SnTwitter $original){

		$twitterHistory = new SnTwitterHistory();
		$twitterHistory->twid		        = $original->twid;
		$twitterHistory->screenname         = $original->screenname;
		$twitterHistory->twKey              = $original->twKey;
		$twitterHistory->twKeySecret        = $original->twKeySecret;
		$twitterHistory->timg		        = $original->timg;
		$twitterHistory->name			    = $original->name;
		$twitterHistory->screenname		= $original->screenname;
		$twitterHistory->location			= $original->location;
		$twitterHistory->following	    = $original->following;
		$twitterHistory->follower	        = $original->follower;
		$twitterHistory->statusCount		= $original->statusCount;
		$twitterHistory->protected		= $original->protected;
		$twitterHistory->language			= $original->language;
		$twitterHistory->url				= $original->url;
		$twitterHistory->verified			= $original->verified;
		$twitterHistory->description		= $original->description;
		$twitterHistory->listedCount		= $original->listedCount;
		$twitterHistory->trueReach		= $original->trueReach;
		$twitterHistory->cost		= $original->minCost;
		$twitterHistory->maxCost		= $original->maxCost;
		$twitterHistory->originalCost		= $original->originalCost;
		$twitterHistory->customCost		= $original->customCost;

		$twitterHistory->save();
		$twitterHistory->saveSnTwitter($original);

		unset($twitterHistory);
	}

}

