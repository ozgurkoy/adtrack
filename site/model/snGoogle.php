<?php
/**
 * snGoogle real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP . '/site/model/base/snGoogle.php';
include $__DP . '/site/lib/google/Google.php';
/**
 * Class SnGoogle
 */
class SnGoogle extends SnGoogle_base
{

	public $savedId=null;
	public $savedAvatar=false;

	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}


	/**
	 * Add account
	 *
	 * @param array $googleToken
	 * @return bool|SnGoogle
	 */
	public function addAccount( array $googleToken, $info )
	{
		$googleId = $info[ "id" ];

		// check if it's a new user
		$this->load( array( "gpid" => $googleId ) );

		if ( !$this->gotValue || is_null( $this->publisher_snGoogle ) ) {
			// token exists, we are good
			if ( isset( $googleToken[ "refresh_token" ] ) )
				$this->refreshToken = $googleToken[ "refresh_token" ];
			$this->tokenValidDate = $googleToken[ "created" ] + $googleToken[ "expires_in" ];

			return $this->updateInfo( $info );
		}

		$this->loadPublisher();

		#this is an after fix. so that api works good.
		if(!$this->publisher_snGoogle || $this->publisher_snGoogle->gotValue === false )
			$this->publisher_snGoogle = null;

		return true;
	}

	/**
	 * load g account by g info
	 *
	 * @param array $googleToken
	 * @return bool
	 */
	public function checkAccount( $info )
	{
		$googleId = $info[ "id" ];

		// check if it's a new user
		$this->load( array( "gpid" => $googleId ) );

		return $this->gotValue;
	}


	/**
	 * Login account
	 * @param array $googleToken
	 * @param       $info
	 * @return bool|SnGoogle
	 * @author Özgür Köy
	 */
	public function login( array $googleToken , $info )
	{
		$googleId = $info[ "id" ];

		$this->load( array(   "_custom"=>"`gpid`= '$googleId' AND `publisher_snGoogle` IS NOT NULL"));

		if ( isset( $googleToken ) && isset( $googleToken[ "refresh_token" ] ) ) {
			// token exists, we are good
			$this->refreshToken = $googleToken[ "refresh_token" ];
		}
		$this->tokenValidDate = $googleToken[ "created" ] + $googleToken[ "expires_in" ];

		if ( $this->gotValue ) {
			$this->loadPublisher()
				->writeHistory( UserHistory::ENUM_ACTIONID_USER_LOGIN, "Login by google", UserHistory::ENUM_MADEBY_USER )
				->loadUser()
				->login( null, null, $this->publisher_snGoogle->user_publisher->id );

			return $this->updateInfo( $info );
		}
		else{
			//New User
			$this->updateInfo( $info );
			return false;
		}

	}

	public function checkLoginInfo( array $googleToken )
	{
		$this->token = $this->_squeeze( $googleToken );

		Google::setToken( $this->token );
		$info = Google::getInfo();

		if ( $info ) {
			$info[ "token" ] = $this->token;

			return $info;
		}
		else {
			JLog::log( "social", "Google Problem : can't fetch info ." . $this->token );

			return false;
		}
	}

	public function getInfo( array $googleToken )
	{
		$this->token = $this->_squeeze( $googleToken );

		Google::setToken( $this->token );
		$info = Google::getInfo();

		if ( $info ) {
			$info[ "token" ] = $this->token;

			return $info;
		}
		else {
			JLog::log( "social", "Google Problem : can't fetch info ." . $this->token );

			return false;
		}
	}

	/**
	 * @param array $info
	 * @return SnGoogle
	 */
	public function updateInfo( array $info ) {
		$this->token     = isset( $info[ "token" ] ) ? $info[ "token" ] : "";
		$this->name      = isset( $info[ "name" ] ) ? $info[ "name" ] : "";
		$this->avatar    = isset( $info[ "picture" ] ) ? $info[ "picture" ] : "";
		$this->link      = isset( $info[ "link" ] ) ? $info[ "link" ] : "";
		$this->gender    = isset( $info[ "gender" ] ) ? $info[ "gender" ] : "";
		$this->birthDate = isset( $info[ "birthday" ] ) ? $info[ "birthday" ] : "";
		$this->locale    = isset( $info[ "locale" ] ) ? $info[ "locale" ] : "";
		$this->googleHD  = isset( $info[ "hd" ] ) ? $info[ "hd" ] : "";;
		$this->verified = isset( $info[ "verified_email" ] ) ? $info[ "verified_email" ] : 0;
		$this->email    = isset( $info[ "email" ] ) ? $info[ "email" ] : null;
		$this->gpid     = $info[ "id" ];

//		JPDO::$debugQuery = true;

		$r                 = $this->save();
		$this->savedId     = $this->id;
 		$this->savedAvatar = $this->avatar;


		return $r;
	}



	/**
	 * private auth method
	 *
	 * @param array $googleToken
	 * @return array|bool
	 */
	private function _auth( array $googleToken )
	{

		if ( Google::authenticate( $googleToken ) ) {
			//token valid, get user info including id.
			$this->token = $this->_squeeze( $googleToken );


			Google::setToken( $this->token ); //already set up on authenticate but anyway..
			$info = Google::getInfo();

			if ( $info )
				return $info;
		}
		else {
			JLog::log( "social", "Google Problem : can't authenticate ." . $this->token );

			return false;
		}

		return false;
	}


	private function _squeeze( array $data ) {
		return json_encode( $data );
/*			array(
				"access_token" => $data->access_token,
				"token_type"   => $data->token_type,
				"expires_in"   => $data->expires_in,
				"id_token"     => $data->id_token,
				"created"      => $data->created
			)
		);*/
	}

	public function getLoginUrl() {
		return Google::getLoginUrl();
	}

}

