<?php
/**
 * publisherMobileDevice real class - firstly generated on 20-08-2013 14:12, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/publisherMobileDevice.php';

class PublisherMobileDevice extends PublisherMobileDevice_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/**
	 * Check iphone device token is valid
	 *
	 * @return bool
	 * @author Murat
	 */
	public static function isValidDeviceToken($deviceToken)
	{
		if (strlen($deviceToken) != 64)
			return false;

		if (preg_match("/^[0-9a-fA-F]{64}$/", $deviceToken) == 0)
			return false;

		return true;
	}

	public static function deActiveDevice($publisherID,$token)
	{

		$publisherMobileDevice = new PublisherMobileDevice();
		$publisherMobileDevice->load(array("publisher_publisherMobileDevice"=>$publisherID,"authKey"=>$token));

		if($publisherMobileDevice->gotValue){
			$publisherMobileDevice->status = PublisherMobileDevice::ENUM_STATUS_PASSIVE;

			return $publisherMobileDevice->save();
		}else{
			return false;
		}

	}

	public static function deviceLogout($device)
	{
		$token = JUtil::randomString();
		$pmd = new PublisherMobileDevice();
		// if device exists update it
		//$pmd->load(array("deviceUUID" => $device->device_UUID));
	}

	public static function createNewDevice($device)
	{
		$token = JUtil::randomString();
		$pmd = new PublisherMobileDevice();
		// if device exists update it
		$pmd->load(array("deviceUUID" => $device->device_UUID));

		$pmd->authKey = $token;
		$pmd->enableNotification = 1;
		$pmd->enableMessagesNotification = 1;
		$pmd->status = PublisherMobileDevice::ENUM_STATUS_ACTIVE;
		$pmd->manufacturer = $device->device_Manufacturer;
		$pmd->deviceModel = $device->device_Model;
		$pmd->deviceName = $device->device_name;
		$pmd->devicePlatform = $device->device_Platform;
		$pmd->deviceUUID = $device->device_UUID;
		$pmd->deviceVersion = $device->device_Version;
		$pmd->pushKey = $device->device_Token;
		if (strtolower($device->device_Platform) == "android")
			$pmd->type = PublisherMobileDevice::ENUM_TYPE_ANDROID;
		elseif (strtolower($device->device_Platform) == "ios")
			$pmd->type = PublisherMobileDevice::ENUM_TYPE_IOS;
		$pmd->save();

		return $pmd;
	}

	/**
	 * @param $deviceToken
	 * @param bool $loadPublisher if true return publisher
	 * @return bool|Publisher
	 */
	public static function getPublisherForToken($deviceToken,$loadPublisher = false){

		$pmd = new PublisherMobileDevice();

		$pmd->load(array(
			"authKey" => $deviceToken
		));

		if ($pmd->gotValue){

			if($pmd->status == self::ENUM_STATUS_ACTIVE)
			{
				if($loadPublisher)
				{
					$pmd->loadPublisher();
					$pmd->publisher_publisherMobileDevice->loadUser();
					if($pmd->publisher_publisherMobileDevice->user_publisher->status == User::ENUM_STATUS_ACTIVE ||
						$pmd->publisher_publisherMobileDevice->user_publisher->status == User::ENUM_STATUS_NOT_CONFIRMED ||
						$pmd->publisher_publisherMobileDevice->user_publisher->status == User::ENUM_STATUS_SIGNUP_INCOMPLETE)
					{
						return $pmd->publisher_publisherMobileDevice;
					}
				}else{
					return true;
				}
			}
		}
		return false;
	}


	public static function getUserDevice(User $user,$deviceDate){

		$pmd = new PublisherMobileDevice();

		$user->loadPublisher();

		$pmd->load(array(
			"publisher_publisherMobileDevice" => $user->user_publisher->id,
			"deviceUUID" => $deviceDate->device_UUID
		));

		$token = JUtil::randomString();

		//new device
		if ($pmd->gotValue){
			if($pmd->status != PublisherMobileDevice::ENUM_STATUS_ACTIVE){
				$pmd->authKey = $token;
				$pmd->status = PublisherMobileDevice::ENUM_STATUS_ACTIVE;
			}
		}else{
			$pmd->authKey = $token;
			$pmd->status = PublisherMobileDevice::ENUM_STATUS_ACTIVE;
			$pmd->manufacturer = $deviceDate->device_Manufacturer;
			$pmd->deviceModel = $deviceDate->device_Model;
			$pmd->deviceName = $deviceDate->device_name;
			$pmd->devicePlatform = $deviceDate->device_Platform;
			$pmd->deviceUUID = $deviceDate->device_UUID;
			if (strtolower($deviceDate->device_Platform) == "android")
				$pmd->type = PublisherMobileDevice::ENUM_TYPE_ANDROID;
			elseif (strtolower($deviceDate->device_Platform) == "ios")
				$pmd->type = PublisherMobileDevice::ENUM_TYPE_IOS;
		}

		// Update everything
		$pmd->pushKey = $deviceDate->device_Token;
		$pmd->deviceVersion = $deviceDate->device_Version;

		$pmd->save();

		return $pmd;
	}



}

