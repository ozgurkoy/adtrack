<?php
/**
 * bugHistory real class - firstly generated on 26-10-2014 20:28, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/bugHistory.php';

class BugHistory extends BugHistory_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	public function sendInformEmails( $skipUser = null, $onlyRootUsers = false ) {
		$this->loadBug();
		$this->loadBugUser();
		$this->loadBug();
		if ( $onlyRootUsers === false )
			$f = $this->history->loadFollower();
		else
			$f = DSuser::loadRootUsers();

		$title = $this->operation == BugHistory::ENUM_OPERATION_OPEN ? "NEW" : "UPDATE";

		if ( $f && $f->gotValue ) {
			do {
				if ( $f->id == $skipUser )
					continue;

				DSNotification::addNotification( $f->id, "Admingle.Track - {$title} [#" . $this->history->id . "] ", $this->details, ( $title == "NEW" ? DSNotification::ENUM_TYPE_CHECKOUT : DSNotification::ENUM_TYPE_GOOD ), "ds_bug?bugId=" . $this->history->id );

				JT::init();

				JT::assign( "title", $title );
				JT::assign( "link", $this->history->link );
				JT::assign( "bug", $this );
				JT::assign( "bugId", $this->history->id );
				JT::assign( "username", $f->username );
				JT::assign( "ddate", time() );
				$d = JT::pfetch( "DS_mail" );
				//fixes
				//$d = preg_replace( "/(http(?:s)?:\/\/[^\\s]+)/uism", "<a href=\"$1\" target='_blank'>$1</a>", $d );

				$r = Mail::sendMail( $f->email, "Admingle.Track - ".$title." [#" . $this->history->id . "] ", $d );
			} while ( $f->populate() );
		}

		return true;
	}

}

