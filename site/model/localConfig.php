<?php
/**
 * localConfig real class - firstly generated on 14-03-2013 16:37, add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.'/site/model/base/localConfig.php';

class LocalConfig extends LocalConfig_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

	/*
	 * Write the Changes to Local Config file
	 */
	public static function CommitChanges(){

		try
		{
			Tool::generateConfigfile();
		}catch (Exception $ex)
		{
			JLog::log("gen",$ex);
			return false;
		}
		return true;
	}

	public static function SaveList($parameterlist){

		$localConfig = null;

		foreach($parameterlist as $key => $value)
		{
			$localConfig = new LocalConfig();
			$localConfig->load(array("name" => $key));
			if($localConfig->gotValue)
			{
				$localConfig->value=$value;
				$localConfig->save();
			}
		}

	}

	public function GetKeyValueList()
	{
		$this->load();

		$localConfigArr = array();

		do{
			$localConfigArr[$this->name] = $this->value;
		} while($this->populate());

		return $localConfigArr;
	}

}

