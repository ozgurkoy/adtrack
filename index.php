<?php
header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Pragma: no-cache');
header('Content-Type: text/html; charset=utf-8');
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_detect_order('UTF-8');

require 'site/def/postFilter.php';
require 'site/def/constants.php';
require 'core/run/exec.php';


require 'site/def/routings.php';
require 'core/run/route.php';
