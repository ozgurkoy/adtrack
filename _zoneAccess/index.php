<?php
require 'auth.php';
require '../site/def/generated/_routings.php';
require 'lib/JAccess.php';
$e = JAccess::readCurrent();
$fls = JAccess::getBackups();
echo
'
<pre>
<h1>Admingle Zone Access Panel</h1>
<div style="width:50%;float:left">
<form action="preview.php" method="POST">';
foreach ( $routingBulk as $zone => $bulk ) {
	echo "<h3>ZONE : $zone</h3>
	<BR>";
	echo '
		Allowed IPs:<br/><textarea name="zone['.$zone.']" rows=10 cols=50>'.(isset($e[$zone])?implode("\n",$e[$zone]):"").'</textarea>
		<br />
';

}

echo '
<div style="width:100%;text-align: center"><input type="submit" value="preview" style="font-size:40px"></div>
</form>
</div>
<div style="width:50%;float:left">
<h3>Backups</h3>
';
foreach ( $fls as $fl ) {
	if ( substr($fl,0, 1 ) == "." ) continue;
	echo '<a href="restore.php?f='.$fl.'">'.$fl.'</a><br>';
}

echo'
</div>
';