<?php
require 'auth.php';

require '../site/def/generated/_routings.php';
require 'lib/JAccess.php';
echo "<PRE>
<h1>Admingle Zone Access Panel</h1>
";
if(isset($_GET["f"]) && is_file("backup/".$_GET["f"])){
	if ( isset( $_GET[ "r" ] ) ) {
		JAccess::restoreBackup( $_GET[ "f" ] );

		echo "Htaccess restored";
		echo '
			<div style="float:left"><a href="index.php">Back</a></div>
		';
		exit;
	}


	$source = file_get_contents( "backup/".$_GET[ "f" ] );
	echo '
	<div style="width:80%;margin:auto">
	Please review and accept to restore.

	<textarea readonly name="content" style="width:80%;height:600px">'.$source.'</textarea>

	<div style="float:left"><a href="index.php">Back</a></div>
	<div style="float:right"><a href="restore.php?f='.$_GET["f"].'&r=1">Restore it</a></div>'
	;
	echo '';

}