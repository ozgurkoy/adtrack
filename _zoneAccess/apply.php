<?php
require 'auth.php';
session_start();
if(!isset($_SESSION[ "AACCESS_PREVIEW" ])){
	echo '<script>location.href="index.php"</script>';
}
require '../site/def/generated/_routings.php';
require 'lib/JAccess.php';

ini_set( "display_errors", 0 );

JAccess::backup();
JAccess::writeFromTest();
unset( $_SESSION[ "AACCESS_PREVIEW" ] );
echo

'<h1>Admingle Zone Access Panel</h1>
Settings applied. You can reverse it from <a href="index.php">here</a>';