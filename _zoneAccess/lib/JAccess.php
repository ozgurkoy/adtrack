<?php

class JAccess{

	private static  $replacementHeader = "REPLACEMENT";

	public static function readCurrent() {
		if ( is_file( "../.htaccess" ) ) {
			$f = file_get_contents( "../.htaccess" );
		}
		preg_match_all( "|\#".self::$replacementHeader."(.+?)\#/".self::$replacementHeader."|sm", $f, $capture,PREG_SET_ORDER );
		preg_match_all( "|#RULES\s(.+?$)(.+?)\#\/RULES|sm", $capture[0][1], $rules,PREG_SET_ORDER );
		$ret = array();
		foreach ( $rules as $rule ) {
			preg_match_all( "|(?:RewriteCond \%\{REMOTE_ADDR\}\s!\^(.+?$))+|m", $rule[2], $matches2 );
			if(isset($matches2[1])){
				foreach ( $matches2[ 1 ] as &$mm ) {
					$mm = str_replace( "\\", "", $mm );
				}


				$ret[ trim($rule[ 1 ]) ] = $matches2[ 1 ];
			}
		}
		return $ret;
	}

	public static function prepareFinal( array $zones ) {
		$source = file_get_contents( 'source/htaccess' );

		$z = implode( PHP_EOL, $zones );

		return preg_replace('|\#'.self::$replacementHeader.'.+?\#\/'.self::$replacementHeader.'|sm','#'.self::$replacementHeader.PHP_EOL.$z.PHP_EOL.'#/'.self::$replacementHeader.'',$source);
	}

	public static function prepForZone( $zone, $urls, array $ips ) {
		if(sizeof($ips)>0 && !(strlen($ips[0])>0) ) return false;

		$urls = explode( ',', $urls);
		$pattern = array( '#RULES ' . $zone );
		foreach ( $ips as $ip ) {
			if(preg_match('|\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}|',$ip)!=1)
				continue;
			$ip = str_replace( ".", "\.", $ip );
			$pattern[] = 'RewriteCond %{REMOTE_ADDR} !^'.$ip;
		}

		$pattern[] ='RewriteRule .* - [E=IPGOOD:true]';

		foreach ( $urls as $c=>$url ) {
			$url = substr( trim($url), 3, -3 );
			$pattern[] = 'RewriteCond %{QUERY_STRING} deff='.$url.' '.($c==sizeof($urls)-1?'[NC]':'[OR]').'';
		}

		$pattern[]= 'RewriteRule .* - [E=URLGOOD:true]';
		$pattern[]= 'RewriteCond  %{ENV:IPGOOD}  true';
		$pattern[]= 'RewriteCond  %{ENV:URLGOOD}  true';
		$pattern[]= 'RewriteRule ^(.*)$ index.php [F,L]';
		$pattern[]= '#/RULES '.$zone;


		return implode(PHP_EOL,$pattern);
	}

	public static function writeToFile( $path, $content ) {
		file_put_contents( "." . $path . "/.htaccess", str_replace('\\\\','\\', $content ) );
	}

	public static function backup() {
		$f = file_get_contents( "../.htaccess" );

		$filename = "htaccess_".date( "d-m-YHi", time() );
		file_put_contents( "backup/" . $filename, $f );
	}

	public static function writeFromTest() {
		$f = file_get_contents( "test/.htaccess" );
		self::writeToFile( "/..", $f );
	}

	public static function getBackups() {
		$fls = scandir( "backup" );
		$fls = array_reverse( $fls );
		return $fls;

	}

	public static function restoreBackup( $f ) {
		$f = file_get_contents( "backup/".$f );
		self::writeToFile( "/..", $f );
	}
}