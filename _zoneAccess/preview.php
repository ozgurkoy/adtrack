<?php
require 'auth.php';

require '../site/def/generated/_routings.php';
require 'lib/JAccess.php';
echo "<PRE>";
if(isset($_POST["zone"])){
	echo
	'<h1>Admingle Zone Access Panel</h1>

	<form action="test.php" method="POST">';

	$reps = array();
	foreach ( $_POST[ "zone" ] as $zone => $ips ) {
		$ips = explode( "\n", trim($ips) );

		$ret = JAccess::prepForZone( $zone, $routingBulk[ $zone ], $ips );

		if($ret!==false)
			$reps[ ] = $ret;

	}
	$source = JAccess::prepareFinal( $reps );
	echo '
	You can define a pattern here , or accept the way it is. <br><br>
	A pattern is like : <br>
	<b>RewriteCond %{QUERY_STRING} deff=ds_* [NC]</b>


	<textarea name="content" style="width:80%;height:600px">'.$source.'</textarea>';
	echo '
<div style="width:100%;text-align: center"><input type="submit" value="test" style="font-size:40px"></div>
</form>';
}