<?php
/*
	router
*/
global $__NS,$_serverName;

if(SERVER_NAME != $_serverName)
	define( 'ALT_DOMAIN', true );

if ( strpos( $_SERVER[ 'QUERY_STRING' ], "pushh=1" ) !== false ) $_SESSION[ 'pushh' ] = 1;


if ( JUtil::checkSiteHalt() == true && !isset( $_SESSION[ 'pushh' ] ) ) {
	//$r01 = rand(3,15); sleep($r01);
	JUtil::siteHalt( "upd01202" );
}

if ( !isset( $_REQUEST[ "deff" ] ) ) $_REQUEST[ "deff" ] = "";
if ( !isset( $_REQUEST[ "action" ] ) ) $_REQUEST[ "action" ] = "";
//clean deff
if ( strpos( $_REQUEST[ "deff" ], "/" ) == ( strlen( $_REQUEST[ "deff" ] ) - 1 ) ) $_REQUEST[ "deff" ] = substr( $_REQUEST[ "deff" ], 0, strlen( $_REQUEST[ "deff" ] ) - 1 );

//identify..
$_globParams    = array();
$preAction      = false;
$preActionArgs  = array();
$postAction     = false;
$postActionArgs = array();
$foundRouting   = true;
// print_r($_REQUEST['deff']); die();

if ( isset( $_REQUEST[ "deff" ] ) && strlen( $_REQUEST[ "deff" ] ) > 0 ) {
	$action0 = explode( "/", $_REQUEST[ "deff" ] );
	$_cpath  = trim( $action0[ 0 ] );
	$key     = trim( $action0[ 0 ] );
	$__NS    = null;

	#we don't let the routing to be used other than main domain
	if(defined( "ALT_DOMAIN" )){
		$foundRouting = false;
	}

	array_shift( $action0 );
	$_globParams = $action0;

	foreach ( $_globParams as $glo => $lob )
		$_globParams[ $glo ] = trim( $lob );


	if ( !isset( $_routings[ $key ] ) && defined( "ALT_DOMAIN" ) !== true ) {
		#check for alternate routing files.
		$foundRouting = false;
		foreach ( $routingBulk as $ns => $routeDef ) {
			if ( strpos( $routeDef, "___{$key}___" ) !== false ) {
				$__NS = $ns;
				require $__DP . "site/def/zone/{$ns}/routings.php";
				$foundRouting = true;
				$_routings    = $__routings;

				#attach zone's smarty extensions
				JT::attachClass( $ns );

				break;
			}
		}
	}

	#custom handling, if it fails send the request to original server
	if ( $foundRouting !== true ) {
		include $__DP . "site/controller/J.php";
		if( ($akos=J_controller::custom($key,$_globParams)) !== false ){
			exit; // die , we must handle the controls at custom controller level
		}
		elseif(defined( "ALT_DOMAIN" )===true){
			$loc = $_S . $_SERVER[ 'REQUEST_URI' ];
			$loc = str_replace( "//", "/", $loc );
			$loc = str_replace( "http:/" . SERVER_NAME, "http://" . $_serverName, $loc );
			$loc = str_replace( "https:/" . SERVER_NAME, "https://" . $_serverName, $loc );

			//sending the request to original server
			header( "Location:$loc" );
			exit;
		}
		else{

			JUtil::redirectHome();
		}
	}


	if ( !is_array( $_routings[ $key ] ) )
		$actionBody = $_routings[ $key ];
	else {
		$actionBody     = $_routings[ $key ][ 0 ];
		$preAction      = $_routings[ $key ][ 1 ];
		$preActionArgs  = $_routings[ $key ][ 2 ];
		$postAction     = $_routings[ $key ][ 3 ];
		$postActionArgs = $_routings[ $key ][ 4 ];
	}
}
else {
	//check here out
	if(defined( "ALT_DOMAIN" )){
		header( "Location: http://$_serverName/" );
		exit;
	}

	$actionBody = ( $_REQUEST[ 'action' ] ? $_REQUEST[ 'action' ] : "Home_display" );
	$mainPage   = 1;
}

if ( !( strpos( $actionBody, "_" ) > 0 ) ) $actionBody .= "_";

$actionBody  = explode( "_", $actionBody );
$_globAction = $actionBody[ 0 ];
$_globJob    = $actionBody[ 1 ];

require_once $__DP . 'core/base/JController.php';

if ( !is_file( $_routeBase . $_globAction . ".php" ) ) {
	if ( !JUtil::isProduction() )
		JUtil::configProblem( "controller $_globAction is missing." );
	else
		JUtil::siteHalt( "RT" );

	JLog::log( "911", "Missing controller class : " . $_routeBase . $_globAction . ".php" );

}
else
	require $_routeBase . $_globAction . ".php";

$interfaceON = ucFirst( $_globAction ) . "_controller";
$kontent     = "";

if ( !class_exists( $interfaceON ) /* CHECK USERNAME HERE !!!*/ ) {
	JUtil::redirectHome();
}
else {

	$interfaceObj = new $interfaceON( $kontent );
	if ( !method_exists( $interfaceObj, $_globJob ) /*&& !isset($_SESSION['pushh'])*/ ) {
		if ( !JUtil::isProduction() ) {
			JUtil::configProblem( "controller $_globAction is missing." );
		}
		else {
			JUtil::siteHalt( "RT" );
		}
		JLog::log( "911", "Missing controller method : " . $interfaceON . ">" . $_globJob );
	}


	if ( $preAction !== false && $_globJob != $preAction )
		call_user_func_array( array( $interfaceObj, $preAction ), $preActionArgs );

	$return = (array)call_user_func_array( array( $interfaceObj, $_globJob ), $_globParams );

	if ( $postAction !== false )
		call_user_func_array( array( $interfaceObj, $postAction ), $postActionArgs );

	foreach ( $return as $ret => $urn )
		$$ret = $urn;
}
