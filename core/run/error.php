<?php
/*
	default extension function from the manual
	purpose : logging for errors
*/

function userErrorHandler($errno, $errmsg, $filename, $linenum, $vars)
{
	global $__DP;
    // timestamp for the error entry
    $dt = date("Y-m-d H:i:s (T)");
    // define an assoc array of error string
    // in reality the only entries we should
    // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
    // E_USER_WARNING and E_USER_NOTICE
    $errortype = array (
                E_ERROR              => 'Error',
                E_WARNING            => 'Warning',
                E_PARSE              => 'Parsing Error',
                E_NOTICE             => 'Notice',
                E_CORE_ERROR         => 'Core Error',
                E_CORE_WARNING       => 'Core Warning',
                E_COMPILE_ERROR      => 'Compile Error',
                E_COMPILE_WARNING    => 'Compile Warning',
                E_USER_ERROR         => 'User Error',
                E_USER_WARNING       => 'User Warning',
                E_USER_NOTICE        => 'User Notice',
                E_STRICT             => 'Runtime Notice',
                E_RECOVERABLE_ERROR  => 'Catchable Fatal Error'
                );
    // set of errors for which a var trace will be saved
    $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);

    $err = "<errorentry>\n";
    $err .= "\t<datetime>" . $dt . "</datetime>\n";
    $err .= "\t<errornum>" . $errno . "</errornum>\n";
    $err .= "\t<errortype>" . $errortype[$errno] . "</errortype>\n";
    $err .= "\t<errormsg>" . $errmsg . "</errormsg>\n";
    $err .= "\t<scriptname>" . $filename . "</scriptname>\n";
    $err .= "\t<scriptlinenum>" . $linenum . "</scriptlinenum>\n";

//    if (in_array($errno, $user_errors)) {
//        $err .= "\t<vartrace>" . serialize($vars) . "</vartrace>\n";
//    }
    $err .= "</errorentry>\n\n";

	$isP = JUtil::isProduction();

	if($isP==false && isset($_SERVER['QUERY_STRING']) && strpos($_SERVER['QUERY_STRING'],"pushh=1")===false){
		if (PHP_SAPI !== 'cli') {
			if($isP==false && strpos($_SERVER['QUERY_STRING'],"pushh=1")===false){
				JLog::log("gen",$err);
				echo '<pre>'.$err.'</pre>';
				echo '<pre>'.print_r($_REQUEST, true).'</pre>';
				echo '<pre>';
				debug_print_backtrace();
				echo '</pre>';
				return false;
			}
		} else {
			JLog::log("gen",$err.print_r($_REQUEST, true));
			echo '<pre>'.$err.'</pre>';
			echo '<pre>'.print_r($_REQUEST, true).'</pre>';
			echo '<pre>';
//			debug_print_backtrace();
			echo '</pre>';
			return false;
		}

		if((PHP_SAPI !== 'cli') && isset($_SERVER['QUERY_STRING']) && strpos($_SERVER['QUERY_STRING'],"pushh=1")===false) {
			return true;
		}

		return false;
	}
}

set_error_handler("userErrorHandler");

function fatalErrorHandler() {
	$error = error_get_last();
	if($error !== NULL) {
		$info = '[SHUTDOWN] Dump:'.print_r($error, true).print_r($_REQUEST, true);
		JLog::log("cri",$info);

		if(JUtil::isProduction()) {
			JUtil::siteError("FATAL");
		} else {
			echo '<pre>'.$info.'</pre>';
			print_r(debug_backtrace());

		}
	} else {
		// it also executes on each page end
	}
	return false;
}

register_shutdown_function('fatalErrorHandler');
