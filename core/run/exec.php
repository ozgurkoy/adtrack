<?php
/************************************
*
*    executioner
*
************************************/
require_once $__DP . 'core/run/inc.php';

ini_set('include_path', get_include_path().':/'.DOCUMENT_ROOT);

ini_set('memory_limit', '256M');
ini_set('gd.jpeg_ignore_warning', 1);

//init core defs
$_jd = new JDef();

if ( JCache::readStatik("siteVarsInited") == false ) {
	foreach ( $GLBS as $key=>$value )
		JCache::writeDirect($key,$value);
	JCache::write('siteVarsInited',1);
}

$_S = JUtil::siteAddress();//JCache::read('constants.siteAddress');

//die("Server Name:" . $_SERVER["SERVER_NAME"] . " S: " . SERVER_NAME);
//if(PHP_SAPI !== 'cli' && strpos($_S, "www")!==false && strpos($_SERVER["HTTP_HOST"], "www")===false){
//	header("Location:".$_S.$_SERVER['REQUEST_URI']);
//	exit;
//}

if(PHP_SAPI !== 'cli' && $_SERVER["SERVER_NAME"] != SERVER_NAME ){
	global $_altServerNames;
	if ( isset( $_altServerNames ) && in_array( $_SERVER[ "SERVER_NAME" ], $_altServerNames ) ) {

		define( 'SERVER_NAME', $_SERVER[ "SERVER_NAME" ] );
	}
	else {
		$loc = $_S . $_SERVER[ 'REQUEST_URI' ];
		$loc = str_replace( "//", "/", $loc );
		$loc = str_replace( "http:/" . SERVER_NAME, "http://" . SERVER_NAME, $loc );
		$loc = str_replace( "https:/" . SERVER_NAME, "https://" . SERVER_NAME, $loc );
		header( "Location:$loc" );
		exit;
	}
}

$JTS = microtime(true);

//cache init complete
//init smarty
//JT::singleton();

// register autoload function
function autoload($name) {
    global $__DP;
	$file =is_file($__DP.'site/model/'.lcfirst($name).'.php')?$__DP.'site/model/'.lcfirst($name).'.php':$__DP.'site/model/'.$name.'.php';
	if (is_readable($file))
		require $file;
}

spl_autoload_register( "autoload" );
$__LOADEDOB = array();

//session stuff
require_once $__DP.'core/lib/session/session.php';