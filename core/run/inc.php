<?php
$_loadedObjects = array(1);
$_objectCounts = array();

require_once $__DP.'core/def/JDef.php';

//include bases
// require_once $__DP.'/core/base/JCore.php';
require $__DP.'core/base/JController.php';
require $__DP.'core/base/JModel.php';
require $__DP.'core/base/JMT.php';

//defs and constants
//require $__DP.'site/def/constants.php';
require $__DP.'site/def/mailServers.php';

//util lib
require $__DP.'core/helper/JUtil.php';
//require_once $__DP.'core/helper/form.php';
//require_once $__DP.'/site/lib/util.php';

//pdo stuff
require $__DP.'core/lib/JPDO.php';
require $__DP.'core/lib/mJPDO.php';


//log stuff
require $__DP.'core/helper/JLog.php';

//Error lib
require $__DP.'core/helper/JError.php';
require $__DP.'core/run/error.php';

//site cache
require $__DP.'core/helper/JCache.php';
if(JUtil::isProduction()) {
	define('IS_PRODUCTION',1);
	ini_set('display_errors',0);
	error_reporting(0);
} else {
	define('IS_PRODUCTION',0);
	ini_set('display_errors',1);
	error_reporting(E_ALL);
}

//smarty
//require_once $__DP.'core/lib/smarty.php';

