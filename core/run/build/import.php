<?php

require_once '../../../site/def/constants.php';
require_once $__DP.'core/run/exec.php';

if(isset($_POST["__delete"])){
	$dir = $__DP . "site/export/";
	$files = array_diff(scandir($dir), array('.','..'));
	foreach ($files as $file) {
		unlink("$dir/$file");
	}

	//header("Location: ".$GLBS["constants"]["siteAddress"]."core/run/build/import.php");
	header("Location: ".JUtil::siteAddress()."core/run/build/import.php");
}
if ( isset( $_GET[ "f" ] ) && is_file( $__DP . "site/export/" . $_GET[ "f" ] ) ) {
//	JPDO::beginTransaction();
	try{
	echo "<PRE>";
	include $__DP . "site/export/" . $_GET[ "f" ];

	$operations  = unserialize( base64_decode($__operations));
	$connections = unserialize( base64_decode($__connections) );
	$objects     = unserialize( base64_decode($__objects));
//		echo "<PRE>";
//	print_r( $objects );
//		exit;
	$validOps     = array( "flush", "append", "insertOnly", "updateOnly", "put" );
	$processedIds = array();
	$stillOn      = true;

	while ( list( $obj, $opx ) = each( $operations ) ) {
		echo "READING: " . $obj . "@" . PHP_EOL; //. sizeof( $objects[ $obj ] ) . PHP_EOL;
		$subs = $opx[ "subs" ];
		#print_r( $subs );
		$op = $opx[ "operation" ];

		if ( !( isset( $objects[ $obj ] ) && sizeof( $objects[ $obj ] ) > 0 ) ) {
			echo "|$obj|";
			print_r( $objects );
			echo "NO DATA FOUND : " . $obj . PHP_EOL; //. sizeof( $objects[ $obj ] ) . PHP_EOL;
			continue;
		}

		if ( $op == "flush" ) {
//			JPDO::executeQuery( "SET foreign_key_checks = 0" );

			echo "FLUSHING $obj" . PHP_EOL;
			#flush the data now
			$o = new $obj();
			JPDO::truncate( $o->dbName, $o->selfname );

//			JPDO::$debugQuery = false;
			$op = "insertOnly";
//			JPDO::executeQuery( "SET foreign_key_checks = 1" );
		}

		if ( !in_array( $op, $validOps ) ) $op = "append";

		foreach ( $objects[ $obj ] as $obData ) {
//			JPDO::$debugQuery = true;


			$o      = new $obj();
			$filter = array();
			$custom = "1=1";

			foreach ( $opx[ "key" ] as $obkey ) {
				$custom           .= " AND `$obkey`='" . html_entity_decode( $obData[ $obkey ] ) . "'";
				$filter[ $obkey ]  = html_entity_decode( $obData[ $obkey ] );
			}

			$filter = array( "_custom" => $custom );
			if($obj=="setting")
			JPDO::$debugQuery = true;
			$o->load( $filter );
			JPDO::$debugQuery = false;

			#echo $op . "..." . PHP_EOL;

			#insert skipped
			if ( $op == "insertOnly" && $o->gotValue ) {
				echo "Skipped inserting to $obj with values(" . print_r( $filter, true ) . ")" . PHP_EOL;
				continue;
			}
			#update skipped because nothing to update, sorry
			if ( $op == "updateOnly" && $o->gotValue != true ) {
				echo "Skipped updating $obj with values(" . print_r( $filter, true ) . ")" . PHP_EOL;
				continue;
			}

			#put will either update or insert.

			$ob = new $obj();

			#if updateOnly and the key is id, only same id's will be updated.
			#if insertOnly and the key is id, only non existing id's will be inserted, see above.

			if ( $op != "append" && isset( $filter[ "id" ] ) ){
				#echo $obj."<<<<<<<<<<<".PHP_EOL;
				$ob->overrideId( !$o->gotValue ); #here we are, writing with id
			}

			#since we made it here, we can process the subs too.
			foreach ( $subs as $sobj => $sdet ) {
				$sop = $sdet[ "operation" ];

				if ( isset( $operations[ $sobj ] ) ) {
					continue;
				}

				#echo "ADDING SUB  $sobj to $obj" . PHP_EOL;

//				echo "<<<" . $sobj . "added" . PHP_EOL;
				$operations[ $sobj ] = $sdet;
			}

			#all the update and insert controls are checked above. we should be safe by now.
			foreach ( $obData as $ofield => $ovalue ) {
				if ( $ofield == "id" ) {
					if ( $o->gotValue != false )
						$ob->$ofield = $o->id; #we are using the loaded id to edit, we need that you know.
					continue;
				}

				if ( !isset( $ob->_relatives[ $ofield ] ) ) { # do not set relatives here, or FK will scream.
					//echo $ofield . "<<" . PHP_EOL;
					$ob->$ofield = html_entity_decode( $ovalue );
				}
				elseif ( $o->gotValue && in_array( $ofield, $o->_fields ) ) #relative. save it for now. if the connection is in the export it will update , otherwise we must keep the old value.
					$ob->$ofield = $o->$ofield;
//				else
//					echo "SKIPPING $obj " .$ofield. PHP_EOL;
			}
//			if($obj=="setting")
				JPDO::debugQuery( true );

				$e = $ob->save();
//			JPDO::debugQuery( false );
//			if($obj=="DSpermission")
//				var_dump( JPDO::lastId('"devV15_main"."DSpermission_id_seq"') );
//			JPDO::$debugQuery = false;
			echo "SAVING/UPDATING $obj id:[" .$ob->id."]". PHP_EOL;

			if ( !isset( $processedIds[ $obj ] ) )
				$processedIds[ $obj ] = array();

			#change the value, since we arrived here.
			$processedIds[ $obj ][ $obData[ "id" ] ] = $ob->id;;
			echo "DATA $obj [" . $obData[ "id" ] . "] " . $ob->id.PHP_EOL;

		}

	}
//JPDO::commit();exit;
	echo PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL . "_______" . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL;
//print_r( $processedIds );
	# we have to create connections after the regular data import is complete
//	print_r( $connections );

	#TODO : delete connections before saving?????
	echo "<H3>Connections</H3>";
	foreach ( $connections as $obj => $ids ) {
		foreach ( $ids as $id => $ops ) {
			if ( !isset( $processedIds[ $obj ] ) || !isset( $processedIds[ $obj ][ $id ] ) ) {
				echo "NOGOOD:" . $obj."($id)".PHP_EOL;
				continue;
			}
			$o = new $obj();

			$o->load( $processedIds[ $obj ][ $id ] );

			foreach ( $ops as $opName => $opIds ) {
//				echo $opName;
				$k0 = array_search( $opName, $o->relativeMethods );
				if ( $k0 === false ) {
//					echo "@$opName@" . PHP_EOL;

					continue;
				}
				$k1 = $o->_relatives[ $k0 ];

				$opName       = str_replace( "load", "save", $opName ); #simplicity
				$opDelName    = str_replace( "save", "delete", $opName ); #simplicity

				foreach ( $opIds as $opId ) {
					if ( !isset( $processedIds[ $k1 ][ $opId ] ) ) { # not processed, we skip this one
						echo "!{$o->selfname}! $opId > $opName" . PHP_EOL;

						continue;
					}
					if ( method_exists( $o, $opDelName ) )
						$o->$opDelName( $processedIds[ $k1 ][ $opId ] );

					$o->$opName( $processedIds[ $k1 ][ $opId ] );
				}
			}
		}
	}
//	JPDO::commit();
	}catch(JError $j){
		echo $j->getMessage();exit;
	}
//	rename($__DP . "site/export/" . $_GET[ "f" ], $__DP . "site/export/__comp".date( "d-m-Y H:i", time() )."_". $_GET[ "f" ]);
	echo "<h2>import complete.</h2>";

}
elseif(isset($_GET["f"]))
	echo "File could not be found/processed";

$fls = scandir( $__DP . "site/export/" );
$fls = array_reverse( $fls );

echo "
<h1>Select file to import</h1>
";
foreach ( $fls as $fl ) {
	if ( in_array($fl, array("..","."))===true || strpos($fl,".php")===false || (strpos( $fl, "__" ) !==false && strpos( $fl, "__" ) == 0) ) continue;

	echo '<a href="import.php?f='.$fl.'">'.$fl.'</a><br>';
}
?>
<?php
if(sizeof($fls)>2){?>
<br />
<form action="<?php echo JUtil::siteAddress();?>core/run/build/import.php" method="POST">
	<input type="submit" name="__delete" value="DELETE ALL IMPORT FILES">
</form>
<?php
}
else
	echo "What? Where?";
	?>