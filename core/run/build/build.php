<?php
require_once '../../../site/def/constants.php';
include $__DP."core/lib/addendum/annotations.php";
class JCTL extends Annotation {}
class JCTLPre extends Annotation
{
	public $func;
	public $args = array();
}
class JCTLPost extends Annotation
{
	public $func;
	public $args = array();
}
class JCTLNS extends Annotation {}

if ( !isset( $_GET[ "exportOnly" ] ) ){
	if ( !is_dir( $__DP . "site/def/generated" ) )
		mkdir( $__DP . "site/def/generated", 0775 );

	if(
		!is_file( $__DP . "site/def/generated/_routings.php" ) ||
		filesize( $__DP . "site/def/generated/_routings.php" ) < 300
	){
		touch( $__DP . "site/def/generated/_routings.php" );
		file_put_contents($__DP . "site/def/generated/_routings.php",'<?php $__routings=array();');
	}

	if(
		!is_file($__DP . "site/def/generated/_smartyCacheDirs.php") ||
		filesize( $__DP . "site/def/generated/_smartyCacheDirs.php" ) < 300
	){
		touch( $__DP . "site/def/generated/_smartyCacheDirs.php" );
		file_put_contents($__DP . "site/def/generated/_smartyCacheDirs.php",'<?php $__smartyCacheDirs=array();');
	}

	//create the necessary files if they don't exist
	if (
		isset( $_GET[ "only" ] ) &&
		$_GET[ "only" ] == "routings"
	){
		if(is_file( $__DP . "site/def/generated/_routings.php" ))
			unlink( $__DP . "site/def/generated/_routings.php" );

		touch( $__DP . "site/def/generated/_routings.php" );
		file_put_contents($__DP . "site/def/generated/_routings.php",'<?php $__routings=array();');
	}

	if(
		isset( $_GET[ "only" ] ) &&
		$_GET[ "only" ] == "templates"
	){
		if(is_file($__DP . "site/def/generated/_smartyCacheDirs.php"))
			unlink( $__DP . "site/def/generated/_smartyCacheDirs.php" );

		touch( $__DP . "site/def/generated/_smartyCacheDirs.php" );
		file_put_contents($__DP . "site/def/generated/_smartyCacheDirs.php",'<?php $__smartyCacheDirs=array();');
	}

}
if(is_file($__DP.'site/def/state/building-0'))
	@rename($__DP.'site/def/state/building-0', $__DP.'site/def/state/building-1');
require $__DP.'core/run/exec.php';
require $__DP.'core/build/JBuild.php';
JBuild::emptyCache();

if ( JCache::readStatik("siteVarsInited") == false ) {
	foreach ( $GLBS as $key=>$value )
		JCache::writeDirect($key,$value);
	JCache::write('siteVarsInited',1);
}


?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Building...</title>
</head>
<body>
<?php
//	check controllers.
echo "<PRE>";
JLog::$writeToFile = false;
//JPDO::executeQuery( 'DROP SCHEMA IF EXISTS "devV15_main" CASCADE;' );
//exit;
$j = new JBuild();
if ( !isset( $_GET[ "exportOnly" ] ) ){

	if(!isset($_GET["only"])){
		$j->buildMain(isset($_GET["doindex"]) ? $_GET["doindex"] : false);
	}

	if(!isset($_GET["only"]) || $_GET["only"]=="routings"){
		unlink( $__DP . "site/def/generated/_routings.php" );
		$j->buildRoutings();
	}

	if(!isset($_GET["only"]) || $_GET["only"]=="templates"){
		unlink( $__DP . "site/def/generated/_smartyCacheDirs.php" );
		$j->buildTemplates();
	}

	Tool::generateConfigfile();
}
if ( !isset($_GET["only"]) && (isset( $_GET[ "export" ] ) || isset( $_GET[ "exportOnly" ] )) )
	$j->buildExport(); #TODO : MOVE TO BOTTOM
elseif ( !isset($_GET["only"])) {
	echo "<a href='http://".SERVER_NAME."/core/run/build/build.php?exportOnly'>Export Data</a>";
}
JLog::$writeToFile = true;


// Tool::echoLog("Configuration file generated successfully<br /><hr />\n");
// Tool::echoLog("All build operation completed succesfully");
if(is_file($__DP.'site/def/state/building-1'))
	rename($__DP.'site/def/state/building-1', $__DP.'site/def/state/building-0');
?>
<?php
if ( !isset( $_GET[ "exportOnly" ] ) && !isset( $_GET[ "export" ] ) && !isset($_GET["only"]) ){

	?>
	<script>
		if(confirm('Do you want to export now?')){
			location.href = '/core/run/build/build.php?exportOnly';
		}
	</script>
<?php
}
?>
</body>
</html>