<?php
/************************************
 *
 *		utility class
 *		özgür köy
 *
 *
 ************************************/
class JUtil
{
	public static $cipher		= NULL;
	public static $cipherMode	= NULL;
	public static $cipherKey	= NULL;
	public static $ivSize		= NULL;

	static $instance;
	public static $IV = NULL;

	static function singleton(){
		self::$instance ||
		self::$instance = new JUtil();
		return self::$instance;
	}

	public function __construct() {
	}

	public static function resetIV() {
		self::$IV = NULL;
		self::deleteCookie('IV');
		return self::getIV();
	}

	public static function getIV() {
		self::$cipher		= MCRYPT_RIJNDAEL_128;
		self::$cipherMode	= MCRYPT_MODE_CBC;
		self::$cipherKey	= '348298791817d5088a6de6c1b6364756d404a50bd64e645035f'; // This should be a random string, recommended 32 bytes
		self::$cipherKey	= hash('tiger128,3', self::$cipherKey, FALSE);
		self::$ivSize		= mcrypt_get_iv_size(self::$cipher, self::$cipherMode);

		if(is_null(self::$IV)) {
			if(!isset($_COOKIE['IV'])) {
				$iv = mcrypt_create_iv(self::$ivSize, MCRYPT_RAND);
				self::setCookie('IV', self::base64_url_encode($iv), false);
			} else {
				$iv = self::base64_url_decode(self::readCookie('IV', false));
			}
			self::$IV = $iv;
		}

		// fix for pre-generated keys in cookies
		if(strlen(self::$IV) > self::$ivSize) {
			JLog::log('gen', 'IV Size Problem:'.strlen(self::$IV));
			self::$IV = substr(self::$IV, 0, self::$ivSize);
			self::setCookie('IV', self::base64_url_encode(self::$IV), false);
		}

		return self::$IV;
	}

	public static function base64_url_encode($input) {
		return strtr(base64_encode($input), '+/=', '-_,');
	}

	public static function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_,', '+/='));
	}

	public static function getEncrypted($text, $b64=true) {
		$iv = self::getIV();

		$mcc = mcrypt_encrypt(self::$cipher, self::$cipherKey, $text, self::$cipherMode, $iv);

		if($b64) {
			$mcc = self::base64_url_encode($mcc);
		} else {
			$mcc = urlencode($mcc);
			$mcc = str_replace('%', 'zZz', $mcc);
		}

		return $mcc;
	}

	public static function getDecrypted($text, $b64=true) {
		$iv = self::getIV();

		if($b64) {
			$premcc = self::base64_url_decode($text);
		} else {
			$text1 = str_replace('zZz', '%', $text);
			$premcc = urldecode($text1);
		}

		$mcc = mcrypt_decrypt(self::$cipher, self::$cipherKey, $premcc, self::$cipherMode, $iv);
		//JLog::log('gen', 'Included Files:' . print_r(get_included_files(), true).' TOKENN:'. $text . ' URL Decoded:'.$premcc . ' Decrypted:'.$mcc);
		return $mcc;
	}

	public static function customCScript( $script, $parameters=null,$forceExit=true,$tm=1 ){
		if(!is_null($parameters)){
			$params = array();
			foreach((array)$parameters as $p=>$ar){
				if( strlen(trim($ar))>0 ){
					if(!in_array($ar, array("true","false"))){
						$ar = str_replace("'","\'",$ar);
						$ar = preg_replace('/\\r?\\n?/', '', $ar);
						$ar = "'".$ar."'";
					}

					$params[$p] = $ar;
				}
			}
			$parameters = implode(",",$params);
		}
		if (!headers_sent())
			header('Content-Type: text/html; charset=utf-8');

		echo "<script>window.parent.setTimeout(function(){window.parent.".$script."(".$parameters.");},$tm);</script> ";

		if($forceExit)
			exit;
	}

	public static function pageClearErrorMessage( $messages="" ){
		echo "<script>window.parent.clearErrorMessage();</script>";
	}

	public static function randomString($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890',$minLength=null)
	{
		if(!is_null($minLength)){
			$length = rand($minLength,$length);
		}

		// Length of character list
		$chars_length = (strlen($chars) - 1);

		// Start our string
		$string = $chars{rand(0, $chars_length)};

		// Generate random string
		for ($i = 1; $i < $length; $i = strlen($string))
		{
			// Grab a random character from our list
			$r = $chars{rand(0, $chars_length)};

			// Make sure the same two characters don't appear next to each other
			if ($r != $string{$i - 1}) $string .=  $r;
		}

		// Return the string
		return $string;
	}

	public static function dateFormat( $d ){
		if(!($d>1000)) return "-";
		return date("d/m/Y H:i", $d);
	}

	public static function getDateParams($dstr){
		$xpl = strpos($dstr,"-")>0?"-":"/";

		$x = explode($xpl,$dstr);
		return $x;

	}

	public static function isProduction(){
		global $__DP;
		return (is_file("$__DP/site/def/state/production-1") && PHP_SAPI !== 'cli' && !in_array(SERVER_NAME , array("merkur.admingle.com","n.admingle.com","l.admingle.com")));
	}

	public static function checkSiteHalt(){
		global $__DP;
		return is_file("$__DP/site/def/state/halt-1");
	}

	public static function siteHalt($from=""){
		JT::init();

		if(JUtil::isProduction()) {
			JT::assign( "words", LangWord::collectWords( $_SESSION[ "adLang" ], array( "new", "front" ) ) );
			echo JT::pfetch("error_halt");
			exit;
		}
	}

	public static function siteError($from=""){
		JT::init();
		JT::assign("time",date("d-m-Y H:i",time()));
		JT::assign("from",$from);

		if(JUtil::isProduction()) {
			JT::assign( "words", LangWord::collectWords( $_SESSION[ "adLang" ], array( "new", "front" ) ) );
			echo JT::pfetch("error_error");
			//die("error!");
			exit;
		}
	}

	public static function configProblem($problem){
		JT::assign("date",date("d-m-Y H:i",time()));
		JT::assign("problem",$problem);

		JLog::log("911",$problem);

		echo JT::pfetch("_configProblem");
	}

	public static function readCookie($title ,$enc=false){
		if(!isset($_COOKIE[$title])) return null;

		$ck = $_COOKIE[$title];
		if(strlen($ck)==0) return null;

		if($enc) {
			return self::getDecrypted( $ck,true );
		}
		else {
			return $ck;
		}
	}

	public static function setCookie( $title, $str , $enc=false ){
		setcookie ( $title, ($enc?self::getEncrypted($str,true):$str), time() + COOKIE_LIFE , "/"/*, ".".JCache::read("constants.cookieDomain"), 1*/);
	}

	public static function deleteCookie($title){
		setcookie ( $title, "", time() - 3600, "/"/*, ".".JCache::read("constants.cookieDomain"), 1*/);
	}

	public static function normalize($string, $low=false, $spaceReplacement="-"){
		$string = str_replace(" ", $spaceReplacement, $string);
		$string = strtr($string,array("ş"=>"s","ı"=>"i","ğ"=>"g","ü"=>"u","ö"=>"o","Ş"=>"s","İ"=>"i","Ğ"=>"g","Ü"=>"u","Ö"=>"o","Ç"=>"c","ç"=>"c","&"=>"-ve-"));
		$string = preg_replace("/[^a-zA-Z0-9.".$spaceReplacement."]/", "", $string);
		if($low)
			$string = strtolower($string);
		else
			$string = strtoupper($string);

		return utf8_encode($string);
	}

	public static function aposArray($ar,$ap="'"){
		if(!is_array($ar)||!sizeof($ar)>0) return false;
		foreach ($ar as $key => $value) {
			if(substr($value, 0, 1)==$ap) continue; //already patched!
			$ar[$key] = $ap.$value.$ap;
		}

		return $ar;
	}

	public static function multistr($str,$c,$sep=","){
		$r = array();
		for ($i=0; $i < $c; $i++) {
			$r[] = $str;
		}

		return implode($sep,$r);

	}

	public static function isHttps(){
		//$sp = Setting::getValue("SERVER_PROTOCOL");
		//if (!is_null($sp))
		//	return ($sp == "https://");
		if (defined("SERVER_PROTOCOL") && SERVER_PROTOCOL == "https")
			return true;

		if(!empty($_SERVER['HTTPS']))
			if(!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
				return true; //https
			else
				return false; //http
		else
			if(!empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == 443)
				return true; //https
			else
				return false; //http

	}

	/*
	 * Return the server url with http or https
	 */
	public static function siteAddress(){
		global $_serverName;
		if(!defined("SERVER_NAME"))
			define("SERVER_NAME", $_serverName);

		if(self::isHttps()){
			return "https://".SERVER_NAME."/";
		}
		return "http://".SERVER_NAME."/";

	}

	public static function redirectHome($error=true){
		if($error)
			JLog::log("user", "wrong page red(get:".(serialize($_GET)."post:".(serialize($_POST)))."server:".(serialize($_SERVER)).")");

		echo "<script>
			if(window.parent)
				window.parent.location.href='http://".SERVER_NAME."';</script>
			else
				location.href='http://".SERVER_NAME."';
		</script>
		";
		exit;
		// die("going home");

	}

	public static function informAndRedirect($JSMessage, $location="", $error=false, $timeout=2){
		echo "<script>
		window.parent.notifik('".nl2br($JSMessage)."', '".($error?'error':'info')."',$timeout, 'http://".SERVER_NAME."/$location');
		</script>";
		exit;
	}

	public static function jredirect($col="", $doExit=false){
		echo "<script>window.parent.location.href='http://".SERVER_NAME."/$col';</script>";
		if($doExit)
			exit;
	}

	public static function refreshPage(){
		echo "
		<script>
			if(typeof window.parent._ref == 'undefined')
				window.parent.location.href=window.parent.location.href;
			else
				window.parent._ref();

		</script>";
		exit;
	}

	public static function combineKeyValueArrayToString( $array=array(), $excludeKeys=array(),$groupGlue="&",$keyValueGlue="=" ) {
		$r = array();

		foreach ( $array as $a0 => $a1 ) {
			if(!in_array($a0,$excludeKeys))
				$r[ ] = $a0 . $keyValueGlue . urlencode($a1);
		}

		return implode( $groupGlue, $r );

	}

	public static function getSearchParameter( $escapeOutput=true, $returnAsURI=false, $excludeFiltering=array() ) {
		$t0 = explode("?",$_SERVER['REQUEST_URI']); $t0 = end($t0);

		if(strpos($t0,"&")!==false)
			$t1 = explode("&",$t0);
		else
			$t1 = explode("/",$t0);

		// print_r($t1);
		$ret= array();

		foreach ($t1 as $tsearch) {

			if(strpos($tsearch,"=")===false){
				$ret[] = urldecode($tsearch);
				continue;
			}
			if($returnAsURI){
				$ret[] = $tsearch;
			}
			else{
				$t3 = explode("=",$tsearch);
				$t3[ 0 ] = str_replace( "%5B%5D", "", $t3[ 0 ] );

				if(isset($ret[$t3[0]]) && !is_array($ret[$t3[0]])){
					$ret[ $t3[ 0 ] ] = array( $ret[ $t3[ 0 ] ] );
					if(in_array($t3[0],$excludeFiltering) || $escapeOutput !== true  )
						$ret[$t3[0]][] = urldecode($t3[1]);
					else
						$ret[$t3[0]][] = self::escapeFieldName(urldecode($t3[1]));
				}
				else{
					if(in_array($t3[0],$excludeFiltering) || $escapeOutput !== true)
						$ret[$t3[0]] = urldecode($t3[1]);
					else
						$ret[$t3[0]] = self::escapeFieldName(urldecode($t3[1]));
				}
			}
		}
		return $ret;
	}
	public static function escapeFieldName( $s ) {
		return preg_replace( "~[^#a-z0-9\s\-]~si", "", $s );
	}

	public static function tableName($tableName)
	{
		return JDef::$prefix.$tableName;
	}

	/**
	 * Set last URL to session for login redirect
	 *
	 * @return void
	 * @author Murat
	 */
	public static function setLastLocation(){
		if(session_id() == '') {
			session_start();
		}

		$pageURL = 'http';
		if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
			$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}

		$_SESSION["LAST_URL"] = $pageURL;
	}

	/**
	 * Random number
	 *
	 * @return int
	 * @author Murat hosver
	 */
	public static function randomNumber($in,$out,$step=null){
		if (!is_null($step)){
			$vals = array();
			for ($i=$in;$i<$out;$i+=$step){
				$vals[] = $i;
			}
			return $vals[array_rand($vals)];
		} else {
			srand((double) microtime() * 1000000);

			return rand($in, $out);
		}
	}

	/**
	 * Log line writer (especially for background processes)
	 * @param string $msg
	 * @param null $appName
	 * @author Murat
	 */
	public static function echoLog($msg,$appName = null){
		echo date("Y.m.d H:i:s") . " => " . (is_null($appName) ? "" : $appName . " => ") . $msg . PHP_EOL;
	}

	/**
	 * Hard encryption
	 * @param string $str
	 * @author Murat
	 */
	public static function hardEncrypt($str){
		$e = new Encryption(MCRYPT_BlOWFISH, MCRYPT_MODE_CBC);
		global $__site;
		$str = self::base64_url_encode($e->encrypt($str,$__site));
		unset($e);
		return $str;
	}

	/**
	 * Hard decryption
	 * @param string $str
	 * @author Murat
	 */
	public static function hardDecrypt($str){
		$e = new Encryption(MCRYPT_BlOWFISH, MCRYPT_MODE_CBC);
		global $__site;
		$str = $e->decrypt(self::base64_url_decode($str),$__site);
		unset($e);
		return $str;

	}

	public static function quoteString( &$item, $key, $quoteMark="'" ) {
		$item = $quoteMark . $item . $quoteMark;
	}

	public static function stripQuotes( &$item, $key, $quoteMark="'" ) {
		$item = str_replace( $quoteMark, "", $item );

	}

	public  function __destruct() {}



}