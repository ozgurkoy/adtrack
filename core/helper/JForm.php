<?php
global $__NS;
require $__DP."site/def/zone/".(!is_null($__NS)?$__NS:"default")."/JFormZone.php";

class JForm_base extends JFormZone
{
    static $instance;
    static $formkey = 'A1B2C3D4E5f6g7h8i9j0A1B2C3D4E5f6';
    static $delimiter = '@@@';

	/**
	 * CORE
	 * @param $forForm
	 * @return mixed|string
	 * @author Özgür Köy
	 */
	public static function getFormToken( $forForm ) {

		if(!array_key_exists('formKeys', $_SESSION)) {
			$_SESSION['formKeys'] = array();
		}

		if(!isset($_SESSION['formKeys'][$forForm]))
			$_SESSION['formKeys'][$forForm] = JUtil::randomString(32);

		return JUtil::getEncrypted(self::$formkey . self::$delimiter . $forForm . self::$delimiter . time() . self::$delimiter . $_SESSION['formKeys'][$forForm]. self::$delimiter . 'a');
    }

	/**
	 * CORE
	 * @param $token
	 * @return array
	 * @author Özgür Köy
	 */
	public static function parseFormToken( $token ) {
		$tokenen = JUtil::getDecrypted($token);
		$tokenPieces = explode(self::$delimiter, $tokenen);
		return $tokenPieces;
	}

	/**
	 * CORE
	 * @param      $formName
	 * @param null $ref
	 * @return array|bool
	 * @author Özgür Köy
	 */
	public static function formCheck( $formName, &$ref = null ,$clearErrors=true) {
		global $__DP, $__NS;
		//token control
		if ( !isset( $_REQUEST[ 't0ken' ] ) ) {
			JLog::log( 'gen', 'Token: Not received!' );

			return false;
		}

		if ( !isset( $_SESSION[ 'formKeys' ][ $formName ] ) ) {
			JLog::log( 'gen', 'Token: Session key missing in session' );

			return false;
		}

		$tokenPieces = self::parseFormToken( $_REQUEST[ 't0ken' ] );

		if ( sizeof( $tokenPieces ) != 5 ) {
			JLog::log( 'gen', 'Token: Invalid size:' . sizeof( $tokenPieces ) . ' Pieces:' . print_r( $tokenPieces, true ) );

			return false;
		}

		if ( $tokenPieces[ 0 ] != self::$formkey ) {
			JLog::log( 'gen', 'Token: Wrong key:' . $tokenPieces[ 0 ] . ' IV:' . JUtil::$IV . ' IV Encoded:' . JUtil::base64_url_encode( JUtil::$IV ) );

			return false;
		}

		if ( $tokenPieces[ 1 ] != $formName ) {
			JLog::log( 'gen', 'Token: Wrong Form Name: Expected:' . $formName . ' Incoming:' . $tokenPieces[ 1 ] );

			return false;
		}

		$timeDiff = ( $tokenPieces[ 2 ] - time() ) / 60;
		if ( $timeDiff > 30 ) {
			JLog::log( 'gen', 'Token: Timeout: created:' . date( 'Y-m-d H:i:s', $tokenPieces[ 2 ] ) . ' and now:' . date( 'Y-m-d H:i:s', time() ) . ' diff:' . $timeDiff );

			return false;
		}

		if ( $tokenPieces[ 3 ] != $_SESSION[ 'formKeys' ][ $formName ] ) {
			JLog::log( 'gen', 'Token: Wrong From Random Key: Expected:' . $_SESSION[ 'formKeys' ][ $formName ] . ' Incoming:' . $tokenPieces[ 3 ] );

			return false;
		}
		if($clearErrors)
			JUtil::pageClearErrorMessage();
		#include req file
		if( is_null( $__NS ) )
			include $__DP . 'site/def/requiredFormFields.php';
		else
			include $__DP . 'site/def/zone/'.$__NS.'/requiredFormFields.php';
		//fields control
		$errors       = array();
		$replacements = array();
		if ( array_key_exists( $formName, $formFields ) && sizeof( $formFields[ $formName ] ) > 0 ) {
			$ffx = $formFields[ $formName ];
			$valCount = 0;
			foreach ( $ffx as $field => &$info ) {
				$field = str_replace( "@", "", $field );
				if ( !isset( $_REQUEST[ $field ] ) )
					$_REQUEST[ $field ] = null;
//					$function           = "matchEmpty";
//				}
//				else
				$function = ( isset( $info[ 1 ] ) ? $info[ 1 ] : "matchEmpty" );

				$value        = $_REQUEST[ $field ];
				$errorMessage = $info[ 0 ];
				$parameters   = ( isset( $info[ 2 ] ) ? $info[ 2 ] : "" );
				$k            = call_user_func( array( "JFORM", $function ), $value, $parameters, $ref );

				if ( !is_array( $k ) && !$k && !isset( $errors[ $field ] ) )
					$errors[ $field ] = $errorMessage;
				elseif ( is_array( $k ) && $k[ 0 ] == false ) {
					$errors[ $field ]       = $errorMessage;
					$replacements[ $field ] = $k[ 1 ];
				}
				elseif(isset($info[3]) && is_array($info[3])){
					#recursion
					$newKey = $info[ 3 ][0];
					while ( isset( $formFields[ $formName ][ $newKey ] ) )
						$newKey .= "@";
					if($valCount == sizeof($ffx)-1)
						echo "<PRE>WARNING : Don't use the deep validation on the last element of the validation array</PRE>".PHP_EOL;

					$ffx[ $newKey ] = array( $info[ 3 ][ 1 ], isset( $info[ 3 ][ 2 ] ) ? $info[ 3 ][ 2 ] : null, isset( $info[ 3 ][ 3 ] ) ? $info[ 3 ][ 3 ] : null );


				}
				$valCount++;

			}
		}

		if ( sizeof( $errors ) > 0 )
			return array( "errors" => $errors, "replacements" => $replacements );

		#since we are good.
		#self::removeSessionKey( $formName );

		return true;
	}

	/**
	 * CORE
	 * After successful form process session key has to be removed
	 * @param $formName
	 * @author Özgür Köy
	 */
	public static function removeSessionKey($formName) {
		unset($_SESSION['formKeys'][$formName]);
	}

	/**
	 * CORE
	 * kill form response
	 * @param      $formName
	 * @param null $ref
	 * @author Özgür Köy
	 */
	public static function formResponse( $formName, &$ref=null ,$clearErrors=true) {
		$fc = self::formCheck( $formName, $ref ,$clearErrors);
		self::pageResponse( $formName, $fc );
	}

	/**
	 * CORE
	 * @param $formName
	 * @param $fc
	 * @author Özgür Köy
	 */
	public static function pageResponse( $formName, $fc ) {

		if ( $fc !== true ) {
			if ( !isset( $_SESSION[ 'formKeys' ][ $formName ] ) ) {
				JUtil::refreshPage();
			}
			else {
				JUtil::customCScript( "fieldError", array( $formName, json_encode( $fc ) ) );
			}
			exit;
		}
	}

	/**
	 * CORE
	 * Validate form with validation response
	 * @param      $formName
	 * @param      $resp
	 * @param bool $useLangDefs
	 * @author Özgür Köy
	 */
	public static function validationResponse( $formName, $resp, $useLangDefs=false ) {
		if((is_object($resp) && $resp->valid !== true) || (!is_object($resp) && $resp !== true)){
			if(is_null($resp->errorField))#TODO: error field or errorcode, for language based
				JUtil::customCScript( "message", array( "bad", isset($resp->errorLangDef)?$resp->errorLangDef:$resp->errorDetail ),true ); #exited here
			self::pageResponse( $formName, array( "errors"=>array($resp->errorField => ($useLangDefs && isset($resp->errorLangDef)?$resp->errorLangDef:$resp->errorDetail) ) ) ) ;
		}
	}

	public static function matchEmpty( $vl ){
		if(is_array($vl)) $vl = reset( $vl );

		if ( strlen( trim( (string)$vl ) ) > 0 && !is_null($vl) ) return true;
		else return false;
	}
	public static function matchNumeric( $vl ){
		if ( strlen( trim( (string)$vl ) ) > 0 && is_numeric($vl) ) return true;
		else return false;
	}

	public static function matchEqual( $vl1,$vl2 ){
		if( trim($vl1)==trim($_REQUEST[$vl2]) ) return true;
		else return false;
	}


	public static function matchEmail( $vl ){
		if (preg_match('/^[\\w-+\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$/', $vl))
			return true;

		return false;
	}

	public static function validateIfEmpty( $vl1, $vl2 ) {
		if ( !isset( $_POST[ $vl2 ] ) || empty( $_POST[ $vl2 ] ) ) {
			return self::matchEmpty( $vl1 );
		}

		return true;
	}

	public static function checkPublisherTrendSelection( $trends, $params=null, $replacements ) {
		return array( !( sizeof( $trends ) > Setting::getValue( "limitPublisherTrend" ) ), $replacements );
	}


	/**
	 * @param $vl1
	 * @param $opts array(fieldname, value)
	 * @return bool
	 */
	public static function validateIfValue( $vl1, $opts ) {
		if ( !empty( $_POST[ $opts[0] ] ) && $_POST[ $opts[0] ]==$opts[1] ) {
			return self::matchEmpty( $vl1 );
		}
		return true;
	}


	/**
	 * @param $vl1
	 * @param $vl2
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function validateIfFull( $vl1, $vl2 ) {
		if ( is_array( $vl2 ) ) {
			$method = $vl2[ 1 ];
			$vl2    = $vl2[ 0 ];
		}
		else
			$method = "matchEmpty";

		#!empty( $_POST[ $vl2 ] )
		if ( !empty( $_POST[ $vl2 ] ) ) {
			return call_user_func_array( array( "JForm_base", $method ), (array)$vl1 );
			#return self::matchEmpty( $vl1 );
		}

		return true;
	}
	// END class FORM
}
