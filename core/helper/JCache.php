<?php
/************************************
 *
 *		Jcache - jeelet
 *		özgür köy
 *
 ************************************/

//memcached stuff
require_once "$__DP/core/lib/memcached.php";

$hasMC = (bool)function_exists("memcache_pconnect");
//$hasMC=false;
if($hasMC){
	//init memcache
	MC::singleton();

	//try memcached
	$hasMD = sizeof(MC::$mc_servers_count)>0;
}

//try apc
$hasAPC = (bool)function_exists("apc_fetch");

class JCache{
	const STATIK		= 1;
	const RETURNCHAIN 	= true;

	public static function read( $key, $callback="" ) {
		global $GLBS, $hasAPC, $__site, $hasMC;

		$key = $__site . "|" . $key;

		//try memcached
		$keychain = self::parseKey( $key, null, self::RETURNCHAIN );


		if ( $hasMC )
			$ret = MC::get( $keychain );
		else
			$ret = false;

		if ( $ret!==false && (( is_array( $ret ) /*&& sizeof( $ret ) > 0*/ ) || ( trim( strlen( $ret ) ) > 0 )) )
			return self::parseKey( $key, $ret );


		if ( $hasAPC ) {
			$ret = apc_fetch( $keychain );

			if ( ( is_array( $ret ) && sizeof( $ret ) > 0 ) || ( trim( strlen( $ret ) ) > 0 ) )
				return self::parseKey( $key, $ret );
		}

		//nothing.. sorry..
		if($callback=="")
			return false;
		else{
			return $callback();
		}

	}

	public static function readStatik($key){
		global $hasAPC,$hasMC,$__site;

		$key = $__site."|".$key;

		//try memcached
		$keychain = self::parseKey( $key, null, self::RETURNCHAIN );
		$ret = $hasMC && MC::get($keychain);

		if( (is_array($ret) && sizeof($ret)>0) || (trim(strlen($ret))>0)) {
			return self::parseKey($key,$ret);
		}
		//try apc
		if($hasAPC){
			$ret = apc_fetch($keychain);
			if(trim(strlen($ret))>0) return self::parseKey($key,$ret);
		}


		return false;
	}

	public static function flush(){
		global $__site,$hasAPC;

#clear apc.
		if($hasAPC){
			$cachedKeys = new APCIterator('user', '/^'.$__site.'/', APC_ITER_VALUE);

			foreach ($cachedKeys AS $key => $value) {
				apc_delete( $key );
			}
		}
#clear mc
		MC::flush();

	}

	public static function writeDirect($key, $value, $ttl=0){
		global $__site;
		$key = $__site."|".$key;
		self::doWrite( $key, $value, $ttl );
	}

	public static function write($key, $value, $ttl=0, $_initial=true){
		global $__site;
		if($_initial)
			$key = $__site."|".$key;

		if ( is_array( $value ) ) {
			foreach ( $value as $v0 => $v1 ) {
				$newKey = $key . "|" . $v0;

				if ( is_array( $v1 ) )
					self::write( $newKey, $v1, $ttl, false );
				else
					self::doWrite( $key, $value, $ttl );
			}
		}
		else {
			self::doWrite( $key, $value, $ttl );
		}

	}

	private static function doWrite($key, $value, $ttl){
		global $hasAPC,$hasMC;
		//write to memcached
		$hasMC && MC::set( $key, $value, 0, $ttl );
		//write to APC
		if ( $hasAPC )
			apc_store( $key, $value, $ttl );

	}

	public static function delete($key){
		global $GLBS,$hasAPC,$__site,$hasMC;
		$key = $__site."|".$key;

		//write to memcached
		$hasMC && MC::delete($key);

		//write to APC
		if($hasAPC)
			apc_delete($key);

		//set global (fr what??)
		unset($GLBS[$key]);
	}

	public static function addToSiteVariables($key,$value){
		global $GLBS,$hasAPC,$__site;
		$key = $__site."|".$key;

		if($hasAPC){
			apc_store($key,$value,0);
		}
		else
			$GLBS[$key] = $var;
	}

	private static function parseKey($key,$ret=null,$returnRootChainOnly=false){
		$keyparts = array();

		if(strpos($key,".")>0) {
			$keyparts = explode( ".",$key );
			$key = $keyparts[0];
		}

		if($returnRootChainOnly) return $key;

		if( sizeof( $keyparts ) > 1 ){
			for( $i=1; $i<sizeof( $keyparts ); $i++){
				if(!isset($ret[ $keyparts[$i] ])) return false;
				$ret = $ret[ $keyparts[$i] ];
			}
		}

		return $ret;
	}

}
