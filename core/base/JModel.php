<?php
/**
 * base class for Jeelet models, the all extend this one. only variables
 * todo : remove load params or _originalClause
 * @package jeelet
 * @author Özgür Köy
 **/
class JModel
{

	/**
	 * shortcut for left join forcing on with commands
	 */
	const DO_LEFT_JOIN = 99;

	/**
	 * shortcut for right join forcing on with commands
	 */
	const DO_RIGHT_JOIN = 100;

	/**
	 * shortcut for ignoring left join forcing on with commands
	 */
	const NO_LEFT_JOIN = false;

	const OPTYPE_REGULAR = 3;

	const OPTYPE_GROUP = 4;

	/**
	 * order
	 *
	 * @var string
	 **/
	public $_order;

	/**
	 * query for build
	 * @var null
	 */
	public $_query = null;

	/**
	 * Query values for injection.
	 * @var array
	 */
	public $_injectValues = array();

	/**
	 * dsn
	 *
	 * @var string
	 **/
	public $dsn;

	/**
	 * database name
	 *
	 * @var string
	 **/
	public $dbName;

	/**
	 * @var bool
	 */
	public $_overrideId=false;
	
	/**
	 * load parameters
	 *
	 * @var array
	 **/
	public $loadParams=array();


	/**
	 * where method loading parameters
	 *
	 * @var null|array
	 **/
	public $_whereParams=null;

	/**
	 * @var bool
	 */
	public $_doLeftJoin = false;

	/**
	 * @var bool
	 */
	public $_nullLast = true;

	/**
	 * @var bool
	 */
	public $_doRightJoin = false;

	/**
	 * @var array
	 */
	public $_leftJoins = array();

	/**
	 * @var array
	 */
	public $_rightJoins = array();

	/**
	 * @var bool
	 *
	 * distinct before fields
	 */
	public $_useDistinct = false;

	/**
	 * custom WHERE clause
	 *
	 * @var string
	 **/
	public $customClause = null;

	/**
	 * custom WHERE clause IS SET
	 *
	 * @var string
	 **/
	public $__customClauseIsSet = false;

	/**
	 * custom extra clause
	 *
	 * @var string
	 **/
	public $extraClause = array();

	/**
	 * @var string
	 */
	public $_extraClauseGlue = "AND";
	
	/**
	 * if it s returned any values
	 *
	 * @var bool
	 **/
	public $gotValue = false;

	/**
	 * original clause for further population
	 *
	 * @var null
	 */
	public $_originalClause = null;

	/**
	 * do not populate again.
	 *
	 * @var bool
	 */
	public $_populateOnce = true;

	/**
	 * injection for order by addition for unique key
	 *
	 * @var bool
	 */
	public $_orderByInjected = false;

	/**
	 * auto load on sum, max etc
	 *
	 * @var bool
	 */
	public $_autoLoad = true;

	/**
	 * ids object currently holding
	 *
	 * @var array
	 **/
	public $_ids=array();

	/**
	 * data variable which holds the recordsets
	 *
	 * @var array
	 **/
	public $data;

	/**
	 * date of adding the record
	 *
	 * @var string
	 **/
	public $jdate;

	/**
	 * current id
	 *
	 * @var int
	 **/
	public $id=null;

	/**
	 * selfname
	 *
	 * @var string
	 **/
	public $selfname=null;

	/**
	 * selfname override
	 *
	 * @var string
	 **/
	public $selfnameOverride=null;

	/**
	 * index of the data array
	 *
	 * @var int
	 **/
	public $dataIndex=null;

	/**
	 * variable to control populate upon load
	 *
	 * @var string
	 **/
	public $nonPopulate=false;

	/**
	 * real order integer
	 *
	 * @var string
	 **/
	private $_rorder=null;

	/**
	 * @var bool
	 */
	public $_lockOnSelect = false;

	/**
	 *  relatives
	 *
	 * @var array
	 **/
	public $_relatives=array();

	/**
	 *  'properties'
	 *
	 * @var array
	 **/
	public $_ownage=array();

	/**
	 *  fields
	 *
	 * @var array
	 **/
	public $_fields=array();

	/**
	 *  fields
	 *
	 * @var array
	 **/
	public $_remoteConnections=array();

	/**
	 *  relativeMethods
	 *
	 * @var array
	 **/
	public $relativeMethods=array();

	/**
	 * loading relatives
	 *
	 * @var array
	 **/
	public $_loadingRelatives=array();

	/**
	 * fields to load 
	 *
	 * @var array
	 **/
	public $_fieldsToLoad=array();
	
	/**
	 * clauses to load 
	 *
	 * @var array
	 **/
	public $_clauses=array();

	/**
	 * glues of classes
	 *
	 * @var array
	 **/
	public $_clauseGlues=array();

	/**
	 * filter array passed to load
	 * @var array
	 */
	public $_filterArray = array();

	/**
	 * values for clause
	 *
	 * @var array
	 **/
	public $_clauseValues=array(null);

	/**
	 * values for clause
	 *
	 * @var array
	 **/
	public $_rootClauseValues=array();

	/**
	 * extra fields array
	 *
	 * @var array
	 **/
	public $_extraFields=array();
	
	/**
	 * joins to load 
	 *
	 * @var array
	 **/
	public $_joins=array();

	/**
	 * having or where
	 *
	 * @var bool
	 **/
	public $_having=false;

	/**
	 * group by probability
	 *
	 * @var string
	 **/
	public $_groupBy="";

	/**
	 * order by probability
	 *
	 * @var string
	 **/
	public $_orderBy="";

	/**
	 * raw order by to skip order by replacements for statements like to_char(to_timestamp(`jdate`), 'YYYY-MM-DD') ASC
	 *
	 * @var bool
	 */
	public $_rawOrderBy = false;
	
	/**
	 * having clause, if used with count
	 *
	 * @var string
	 **/
	public $_havingClause="";
	
	/**
	 * limit low
	 *
	 * @var int
	 **/
	public $limitLow=0;
	
	/**
	 * ever populated
	 *
	 * @var bool
	 **/
	public $populated=false;
	
	/**
	 * undocumented class variable
	 *
	 * @var string
	 **/
	public $limitCount=50;

	/**
	 * count and sum operations.
	 *
	 * @var bool
	 */
	public $groupOperation=false;


	/**
	 * last operation
	 *
	 * @var bool
	 */
	public $__lastOperation = JModel::OPTYPE_REGULAR;


	/**
	 * constructor, which is called by the class that extends this one
	 *
	 * @param null $id
	 * @return \JModel
	 */
	public function __construct($id=null)
	{
//		 echo "@".$this->selfname."@";
		if(!is_null($id)){
			JMT::load($this, $id);

			if($this->id>0)
				return true;
		}
		return false;
	}
	
	/**
	 * takes in array of arguements
	 *
	 * @param array $options
	 * @return JModel|bool
	 * @author Özgür Köy
	 */
	public function load( $options = array() ) {
		$this->resetObject();

		if( $this->__lastOperation != JModel::OPTYPE_REGULAR ){
			$this->__lastOperation = JModel::OPTYPE_REGULAR;
			$this->_extraFields = array();
			$this->customClause = null;

			if(sizeof($options)>0)
				$this->_whereParams = null;
		}
		$this->_extraFields = array();
//		$this->_joins = array();

		if ( $this->__customClauseIsSet === false )
			$this->customClause = null;

		$isJPP = is_object( $options ) && get_class( $options ) == "JPP";

		if ( $isJPP === true )
			$this->_whereParams = null;

		if( is_null($this->_whereParams)!==true ){
			$this->_whereParams->indexes = array();
			$this->_whereParams->clauseValues = array();
		}

		return JMT::load( $this, $isJPP?$options:(array)$options, false, $this->_rootClauseValues, false );
	}

	public function reload() {
		$this->resetIndex();
		JMT::load($this, $this->_filterArray, false, $this->_rootClauseValues );
	}
	
	/**
	 * add count field, beware, no validation
	 *
	 * @param string $field 
	 * @param string|null $label label for the field
	 * @param array $options options like load.
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function count( $field, $label = null, $options = array() ) {
		$this->groupOperation = true;
		JMT::groupByField( $this, "COUNT", $field, $label );

		if ( $this->_autoLoad )
			return JMT::load( $this, $options );
		else
			return $this;
	}

	/**
	 * add sum field, beware, no validation
	 *
	 * @param string $field
	 * @param string $label label for the field
	 * @param array  $options
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function sum($field, $label=null, $options=array())
	{
		$this->groupOperation = true;

		JMT::groupByField($this, "SUM", $field, $label);

		if($this->_autoLoad)
			return JMT::load($this, $options);
		else
			return $this;
	}

	/**
	 * add max field, beware, no validation
	 *
	 * @param string $field
	 * @param string $label label for the field
	 * @param array  $options
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function max($field, $label=null, $options=array())
	{
		$this->groupOperation = true;
		JMT::groupByField($this, "MAX", $field, $label);

		if($this->_autoLoad)
			return JMT::load($this, $options);
		else
			return $this;
	}

	/**
	 * for postgresql , default is nulls are always at last, run this BEFORE load to change it to NULL FIRST
	 *
	 * @return $this
	 * @author Özgür Köy
	 */
	public function nullFirst( ) {
		$this->_nullLast = false;
		return $this;
	}

	/**
	 * @return $this
	 * @author Özgür Köy
	 */
	public function filter() {
		$clauses = func_get_args();

		JMT::filter( $this, $clauses );
		return $this;
	}

	/**
	 * @param bool $useOR
	 * @author Özgür Köy
	 */
	public function filterGlue( $useOR = true ) {
		$this->_extraClauseGlue = $useOR === true ? "OR" : "AND";
	}
	
	/**
	 * add min field, beware, no validation
	 *
	 * @param string $field
	 * @param string $label label for the field
	 * @param array  $options
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function min($field, $label=null, $options=array())
	{
		$this->groupOperation = true;
		JMT::groupByField($this, "MIN", $field, $label);

		if($this->_autoLoad)
			return JMT::load($this, $options);
		else
			return $this;
	}

	/**
	 * @param array $loadArgs
	 * @param array $values
	 * @return JModel
	 *
	 * update functionality
	 */
	public function update( array $loadArgs, array $values, $limit=null ) {
		return JMT::update( $this, $loadArgs, $values, $limit );
	}

	/**
	 * @param array $loadArgs
	 * @param array $values
	 * @return JModel
	 *
	 * update functionality
	 */
	public function multiDelete( array $loadArgs, $limit=null ) {
		return JMT::deleteWFilter( $this, $loadArgs, $limit );
	}

	/**
	 * @param $tablename
	 * @return $this
	 *
	 * override table name, for dynamic tables
	 */
	public function overrideTableName( $tablename ) {
		$this->selfnameOverride = $tablename;

		return $this;
	}
	
	/**
	 * with function takes in strings as relative names.
	 *
	 * @return object
	 * @author Özgür Köy
	 */
	public function with()
	{
		$_f0 = func_get_args();
		
		JMT::with($this,$_f0);

		return $this;
	}
	
	/**
	 *
	 * the object
	 *
	 * @return bool|JModel
	 * @author Özgür Köy
	 */
	public function save()
	{

		return JMT::save($this);
	}


    /**
     * build query for loading
     *
     * @return object
     * @author Özgür Köy
     **/
    public function buildQuery()
    {
        $c0 = array();

        if(!is_null($this->customClause)) return $this;

        //read fields
        if(!isset($this->_fields) || !sizeof($this->_fields)>0) return false;


        foreach ($this->_fields as $_field) {
//            echo $_field." ".$this->$_field.PHP_EOL;
            if(!is_null($this->$_field) && ((is_array($this->$_field)&&sizeof($this->$_field)>0) || (!is_array($this->$_field)&&strlen($this->$_field)>0)) ){

	            if($_field != "id" && $this->populated==false )
		             $this->_originalClause[$_field] = $this->$_field;

                $c0[] = "`".JUtil::tableName($this->selfname).
                    "`.$_field ".
                    (is_array($this->$_field)?"IN(".implode(",",JUtil::aposArray($this->$_field)).")":"='".addslashes($this->$_field)."'");
            }


        }
        if(sizeof($c0)>0){
            $this->customClause = implode(" AND ",$c0);
        }
        return $this;
    }


    /**
	 * having with grouped, be careful to use it with group
	 *
	 * @param string $clause 
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function having($clause=null)
	{
		$this->_having = true;
		$this->_havingClause = $clause;

		return $this;
	}
	
	/**
	 * group by
	 *
	 * @param string|array $group
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function groupBy( $group ) {
		if ( !is_array( $group ) )
			$this->_groupBy = $group;
		else {
			$t0 = array();
			foreach ( $group as $gr0 ) {
				if ( strpos( $gr0[ 0 ], "." ) !== false && strpos( $gr0[ 0 ], "`" ) === false )
					$gr0[ 0 ] = preg_replace( "|(\w+?)\.(\w+)|", '`$1`.`$2`', $gr0[ 0 ] );

				$t0[ ]                           = $gr0[ 0 ];
				$this->_extraFields[ $gr0[ 1 ] ] = $gr0[ 0 ] . " AS `" . $gr0[ 1 ] . "`";
				$this->$gr0[ 1 ] = null;

			}
			$this->_groupBy = implode( ",", $t0 );
			unset( $t0 );
		}

		return $this;
	}
	
	/**
	 * order by
	 *
	 * @param string $order 
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function orderBy($order=null)
	{
		$this->_orderBy = $order;
		
		return $this;
	}

	/**
	 * raw order by
	 *
	 * @param string $order
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function rawOrderBy($order=null)
	{
		$this->_rawOrderBy = true;
		$this->_orderBy = $order;

		return $this;
	}

	
	/**
	 * set fields of object
	 *
	 * @param array $fields 
	 * @return object
	 * @author Özgür Köy
	 */
	public function set($fields)
	{
		foreach ($fields as $ff => $fv) 
			$this->$ff = $fv;
		
		return $this;
	}
	
	/**
	 * create object with data only
	 *
	 * @param array $fields 
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function __inject($fields)
	{
		foreach ($fields as $key => $value) {
			$this->$key = $value;
		}
		$this->gotValue = true;
		return $this;
	}
	
	/**
	 * limiter
	 *
	 * @return JModel
	 **/
	public function limit($low,$count=null)
	{
		$this->limitLow   = $low;
		$this->limitCount = is_null($count)?$this->limitCount:$count;
		
		return $this;
	}

	/**
	 * delete the current instance.
	 *
	 * @param null $id
	 * @return bool
	 * @author Özgür Köy
	 */
	public function delete($id=null)
	{
		return JMT::delete($this, $id);
	}

	/**
	 * disable onload population
	 *
	 * @return JModel
	 **/
	public function nopop()
	{
		$this->nonPopulate = true;
		return $this;
	}


	private function _where( &$filters, $glue ) {
		if(is_null($this->_whereParams))
			$this->_whereParams = JPP::init($glue);
		elseif($this->_whereParams->glue != $glue)
			die( "You started with ".($this->_whereParams->glue).", can't use ".$glue." now" );

		if(sizeof($filters)==2 && is_string($filters[0]))
			$filters = array($filters[0]=>$filters[1]);

		if(sizeof($filters)>0)
			$this->_whereParams->add( $filters );
	}

	public function where() {
		$filters = func_get_args();
		$this->_where( $filters, JPP::PP_AND );
	}

	public function clearFilter() {
		$this->_whereParams = null;
		return $this;
	}

	public function either() {
		$filters = func_get_args();
		$this->_where( $filters, JPP::PP_OR );
	}

	/**
	 * populus!
	 *
	 * @param bool $full
	 * @param bool $back
	 * @return Jmodel|bool
	 * @author Özgür Köy
	 */
	public function populate($full=false, $back=false)
	{
		return JMT::populate($this, $full, $back);
	}

	/**
	 * reset index.
	 *
	 * @return JModel
	 */
	public function resetIndex() {
		return JMT::resetIndex( $this );
	}

	/**
	 * reset index
	 *
	 * @return object
	 **/
	public function resetObject() {
		JMT::clearSelf( $this );
		$this->dataIndex        = -1;
		$this->_orderByInjected = false;
		$this->loadParams       = array();
		$this->_clauseValues    = array();
		$this->_clauses         = array();
		//$this->doLeftJoin    = false;
		//$this->customClause  = null;
		//$this->limitCount    = 10;
		//$this->limitLow      = 0;

		return $this;
	}
	
	/**
	 * get relative's dsn
	 *
	 * @param string $tag 
	 * @return mixed
	 * @author Özgür Köy
	 */
	public function getRelativeDsn($tag)
	{
		if(isset($this->_relatives[$tag])){
			$d0  = ucfirst($this->_relatives[$tag]);
			return $d0::getDsn();
		}
		
		return false;
	}

	/**
	 * enable or disable populate more than once
	 *
	 * @param boolean $good
	 * @return JModel
	 */
	public function populateOnce( $good )
	{
		$this->_populateOnce = $good;

		return $this;
	}

	/**
	 * enable or disable autoload on sum, max..
	 *
	 * @param boolean $good
	 * @return JModel
	 */
	public function multipleGroupOperation( $good )
	{
		$this->_autoLoad = !$good;

		return $this;
	}


	
	/**
	 * add custom clause
	 *
	 * @param string $customClause 
	 * @return JModel
	 * @author Özgür Köy
	 */
	public function customClause($customClause)
	{
		$this->__customClauseIsSet = true;
		$this->customClause = $customClause;
		
		return $this;
	}

	/**
	 * add custom field
	 *
	 * @param $field
	 * @param $label
	 * @return $this
	 * @author Özgür Köy
	 */
	public function customField( $field, $label ) {
		if($this->groupOperation !== true)
			$this->_extraFields[ $label ] = $field . " AS `$label`";

		return $this;
	}

	/**
	 * Use left join instead of inner
	 * @author Özgür Köy
	 * @return JModel
	 *
	 */
	public function leftJoin(){
		$this->_doLeftJoin = true;
		$f = func_get_args();
		foreach ( $f as $arg ) {
			$this->_leftJoins[ ] = $arg;
		}

		return $this;
	}

	/**
	 * Use right join instead of inner
	 * @author Özgür Köy
	 * @return JModel
	 *
	 */
	public function rightJoin(){
		$this->_doRightJoin = true;
		$f = func_get_args();
		foreach ( $f as $arg ) {
			$this->_rightJoins[ ] = $arg;
		}

		return $this;
	}

	/**
	 * Use distinct
	 * @author Özgür Köy
	 * @return JModel
	 */
	public function useDistinct() {
		$this->_useDistinct = true;

		return $this;
	}

	/**
	 * Override id parameter
	 *
	 * @return JModel
	 */
	public function overrideId( $do = true ) {
		$this->_overrideId = $do;

		return $this;
	}

	/**
	 * abstract method
	 * @return string
	 */
	public function modelQuery( $type, $options=null ){ }


	/**
	 * @param $jdate
	 */
	public function updateJDate( $jdate ) {
		JMT::updateJDate( $this, intval( $jdate ) );
	}

	/**
	 * lock row on select, use with caution,
	 * @param $good
	 * @author Özgür Köy
	 */
	public function lockOnSelect( $good=true ) {
		$this->_lockOnSelect = $good;
	}

	/**
	 * assign parameters to object
	 * @param       $params
	 * @param array $exceptions
	 * @author Özgür Köy
	 */
	public function assignValuesToObject( $params, $exceptions=array() ) {
		foreach ( $this->_fields as $f0 ) {
			if ( in_array( $f0, array( "id", "jdate" ) ) || !isset( $params[ $f0 ] )  || is_array( $params[ $f0 ] ) || in_array($f0, $exceptions) )
				continue;

			$this->$f0 = $params[ $f0 ];
		}

	}

	public function cloneMe( $resetIds=true, $saveObject=false ){
		$n = clone $this;

		if ( $resetIds === true ) {
			$n->id   = null;
			$n->_ids = array();
		}

		if ( $saveObject === true ){
			$n->gotValue = false;

			$n->save();
		}

		return $n;
	}

	public static function instantiate( $objectName, $loadFilter = array(), $orderBy=null, $limit=null ) {
		$objName = ucfirst( $objectName );

		$o = new $objName();

		if(!is_null($orderBy))
			$o->orderBy( $orderBy );

		if(!is_null($limit))
			$o->limit( $limit[0],$limit[1] );

		$o->load( $loadFilter );

		return $o;
	}

}
