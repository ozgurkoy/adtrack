<?php
include $__DP . "core/lib/db/".DB_ENGINE."/JMT_base.php";

/**
 * base class for Jeelet models, the all extend this one.
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JMT extends JMT_base
{
	/**
	 * field comparisons
	 *
	 * @var array
	 **/
	public static $_comparingClauses = array(
		'&lt'=>"<",
		'&ne'=>"<>",
		'&le'=>"<=",
		'&ge'=>">=",
		'&gt'=>">"
	);

	private static $_caching = false;

	/**
	 * TEMPORARY FOR CACHING
	 *
	 * @var bool
	 */
	public static $_initial = true;

	private static $_innerCounter = 20;

	/**
	 * constructor, which is called by the class that extends this oneu
	 *
	 * @return bool
	 **/
	public function __construct($id=null)
	{
		if(!is_null($id)){
			$this->load($id);

			if($this->id>0)
				return true;
		}
		return false;
	}

	/**
	 * take in relatives to load with
	 *
	 * @return Jmodel|bool
	 * @author Özgür Köy
	 */
	public static function with(JModel &$ins, $options)
	{
		if(!sizeof($ins->relativeMethods)>0) return false;

		$ins->_loadingRelatives = self::_withInner( $ins, $options, $ins );
		return $ins;
	}

	/**
	 * inner ops for with clauses
	 *
	 * @param JModel $mainObject
	 * @param        $options
	 * @param JModel $object
	 * @param null   $repTag
	 * @return array
	 * @author Özgür Köy
	 */
	private static function _withInner(JModel &$mainObject, $options, JModel &$object, $repTag=null){
		$relativeArray = array();
		foreach ($options as $relative) {
			$doLeft = false;
			$remotes = false;

			if ( is_array( $relative ) ) {
				$remotes  = isset( $relative[ 2 ] ) ? $relative[ 2 ] : false;
				$doLeft   = $relative[ 1 ];
				$relative = $relative[ 0 ];
			}

			if ( array_key_exists( $relative, $object->relativeMethods ) ) {
				if( $object->_remoteConnections[ $relative ][ "type" ] == "NtoN" )
					die( $relative. " is N to N you can't use it with 'with' " );

				if ( $doLeft == JModel::DO_LEFT_JOIN )
					$mainObject->leftJoin( $relative );
				elseif( $doLeft == JModel::DO_RIGHT_JOIN )
					$mainObject->rightJoin( $relative );

				$o0           = ucfirst( $object->_remoteConnections[ $relative ][ "tableName" ] );
				$o0           = new $o0;
				$joinToAdd    = $object->_remoteConnections[ $relative ][ "join" ];

				if ( !is_null( $repTag ) ){
					$joinToAdd = str_replace( "`{$object->selfname}`.", "`$repTag`.", $object->_remoteConnections[ $relative ][ "join" ] );}

				self::addIfNotInArray( $mainObject->_joins, $joinToAdd, true );
				$fieldsToLoad = array();

				foreach ( $o0->_fields as $_f0 ) {
					self::addToFieldArray( $mainObject->_fieldsToLoad, $relative, $_f0, substr( $relative . "." . $_f0, -60 ) );
					$fieldsToLoad[ ] = substr( $relative . "." . $_f0, -60 );
				}

				$relativeArray[ $relative ] = array( "table"=>$o0->selfname, "fields"=>$fieldsToLoad, "subs"=>array() );

				if ( $remotes !== false ) {
					foreach( $remotes as $remote ){
						$r0 = self::_withInner( $mainObject, array($remote), $o0, $relative );
						$relativeArray[ $relative ][ "subs" ] = array_merge_recursive( $relativeArray[ $relative ][ "subs" ], $r0 );
					}
				}

				unset( $o0 ); //TODO : fix here, so that we don't have to create the object for only fields.


			}

		}

		return ( $relativeArray );

	}

	/**
	 * @param JModel $ins
	 * @param array  $clauses
	 * @param bool   $returnRelations
	 * @param bool   $indirect
	 * @return array|JModel
	 */
	public static function load( JModel &$ins, $clauses=array(), $returnRelations=false, &$clauseValues=null, $dontExecute=false )
	{
		$ins->_filterArray  = $clauses;
		$ins->_clauseValues = array();
		$ins->_clauses      = array();
//		$ins->_joins        = array();

		$doReplacement = false;
		$cacheLoaded   = false;
		$hashKey       = null;

		self::simplifyClause( $ins, $clauses );

		if (
			$returnRelations === false &&
			sizeof( $clauses ) > 0 &&
			self::$_caching === true
		) {
			$keys          = array();
			$replacements  = array();
			$doReplacement = true;
			$hashKey       = self::hashTheClauses( $clauses );

			if ( ( $_cache = JCache::read( $hashKey ) ) !== false ) {
				$cacheLoaded  = true;
				$replacements = $_cache[ 1 ];
				$newClauses   = self::cacheRead( $clauses, $replacements );
				array_unshift( $newClauses, null );
				$ins->_query        = $_cache[ 0 ];
				$ins->_clauseValues = $newClauses;
			}
			else {
				if ( is_int( $clauses ) )
					$clauses = array( "id" => $clauses );

				//we save the original clause here for caching
				$claux = $clauses;

				//caching preparations
				self::parseVars( $claux, $keys, $replacements );

				//we replace it to monitor placement changes
				$clauses = $claux;
			}
		}

		if( self::$_caching === false && $cacheLoaded === false ){
			$rets = array();

			if ( $ins->groupOperation !== false )
				$ins->_originalClause = array();

			//prepare self fields.
			foreach ($ins->_fields as $sff)
				self::addToFieldArray( $ins->_fieldsToLoad, $ins->selfname, $sff, $sff );

			if ( sizeof( $clauses ) == 0 && is_null($ins->_whereParams) !== true  ){
				$clauses = $ins->_whereParams;
			}

//			$ins->_whereParams = null; #kill where params anyway


			if ( sizeof( $clauses ) == 0 && sizeof( $ins->_originalClause ) == 0 ) {
				$ins->buildQuery();


				// just put something
		        if(is_null($ins->customClause))
			        $ins->_originalClause[] = 1;
	        }
	        else if(is_null($ins->customClause)){
	            self::clearSelf($ins);
	            $ins->customClause = null;

		        $clauseValues = array();
		        // numeric id, numeric or numerical array
		        if ( is_numeric( $clauses ) || (is_array($clauses) && isset($clauses[0])) )
			        $clauses = array( "id" => $clauses );

		        if ( JPP::is( $clauses ) === true ) {
			        $clauses->resolve();
			        $finalClause = JPP::indexResolve($clauses);

			        $clauses = $clauses->clauseValues;
			        $clauseIndex = array();
//			        print_r( $clauses );

			        foreach ( $clauses as $token => $clx ) {
				        self::simplifyClause( $ins, $clx );
				        self::parseClauses($ins, $clx, $returnRelations, $clauseValues, $dontExecute, $rets);

				        $diffe = array_diff($ins->_clauses,$clauseIndex);
//				        print_r( $diffe );
				        $finalClause = str_replace($token,reset( $diffe ),$finalClause);
				        $clauseIndex[ $token ] = reset($diffe);

			        }
					#temp fixes for n-ns
			        $finalClause = preg_replace("/ (AND|OR)\\s+(AND|OR)/uUsm", " $1", $finalClause);
			        $finalClause = preg_replace("/(AND|OR)\\s+\\)/uUsm", ")", $finalClause);
			        $finalClause = preg_replace("/\\(\\s+(AND|OR)/uUsm", "(", $finalClause);
			        $finalClause = preg_replace("/\\(\\)/uUsm", "1=1", $finalClause);
			        $finalClause = preg_replace("/\\(\s+?\\)/uUsm", "1=1", $finalClause);

			        //$ins->customClause( $finalClause );
			        $ins->customClause=$finalClause;#not using the method
		        }
				else
		            self::parseClauses($ins, $clauses, $returnRelations, $clauseValues, $dontExecute, $rets);
		        
	            // for recursive calls from processValue
	            if($returnRelations){
		            return array("clauses"=>$ins->_clauses, "clauseValues"=>array(), "joins"=>$ins->_joins);
	            }

	        }
			$ins->loadParams    = $clauses;

			// prepare the query.
			self::prepare($ins);

			$ins->_clauseValues = $rets;

			if ( $cacheLoaded === false && $doReplacement === true && self::$_caching === true ) {
				//match the altered values with current one.
				$indexPlacements = array();

				preg_match_all( "|%JREP[0-9]+?%|i", $ins->_query, $pieces );
				if(!isset($pieces[0]))
					die( "problem with cache replacement #1" );

				$ind = 0 ;
				foreach ( $ins->_clauseValues as $cval => $cRealVal ) {
					if ( ( $placementIndex = array_search( $cval, $pieces[0] ) ) !== false && array_key_exists($cRealVal,$replacements) ) {
						$indexPlacements[ $ind++ ] = $placementIndex;
					}
					$hashPlacement = array(
						preg_replace("|%JREP[0-9]+?%|i","?",$ins->_query),
						$indexPlacements
					);

					JCache::writeDirect( $hashKey, $hashPlacement );
				}
			}

			self::buildQueryExec( $ins );

			if ( sizeof($ins->_injectValues) > 0 ) {
				$ins->_clauseValues = array_merge( $ins->_clauseValues, $ins->_injectValues );
			}

			#for sub query execution
			if($dontExecute) return false;

			array_unshift($ins->_clauseValues, null);
		}
//		echo 555//		echo $ins->_query;
		$ins->_query = self::checkNameOverride( $ins, $ins->_query );

		JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );

		$r0 = JPDO::executeQuery( $ins->_query, $ins->_clauseValues );
		$popd = self::populateData($ins, $r0);

		//after load, populate..
		if($ins->nonPopulate==false)
			self::populate($ins);

		//we are done here, since groupOperation is a single result operation.
		if($ins->groupOperation === true){
			$ins->groupOperation = false;
			//$ins->_extraFields = array();
		}

		return $ins;
	}

	public static function parseClauses( &$ins, &$clauses, $returnRelations=false, &$clauseValues=null, $dontExecute=false, &$rets, $forceToken=null ) {
		//prepare fields, clauses , joins
		foreach ($clauses as $field => $value) {
			$found     = true;
			$processed = false;
			if(!$returnRelations)
				$clauseValues = array();

			$isAField = in_array( $field, $ins->_fields );
			$isARemoteField = isset( $ins->_remoteConnections[ $field ] );

			if ( $isARemoteField === true && is_string($value) !== true ) {
				// relations are hard.
				$remo = $ins->_remoteConnections[ $field ];
				if ( strpos( $field, "_reverse" ) !== false ) #for n-n reverse loading.
					$field = substr( $field, 0, -8 );

				$processed = self::processValue(
					$ins,
					$remo[ "table" ],
					$field,
					$value,
					$remo[ "clause" ],
					$remo[ "join" ],
					$clauseValues,
					$forceToken
				);

				if ( sizeof( $processed ) > 0 ) {
					$rets = self::mergeSingle( $rets, $clauseValues );
					if ( strlen( $remo[ "defClause" ] ) > 0 )
						self::addIfNotInArray( $ins->_clauses, $remo[ "defClause" ] );
					self::addIfNotInArray( $ins->_joins, $remo[ "join" ], true );
				}
				else
					$found = false;
			}
			elseif ( $isAField === true ) {
				$t0 = $t1 = null; #just for kicks
				// check for local fields.
				$processed = self::processValue( $ins, $ins->selfname, $field, $value, $t0, $t1, $clauseValues,$forceToken );
				$rets      = self::mergeSingle( $rets, $clauseValues );

			}
			elseif ( $field == "_custom" && strlen( $value ) > 0 ) {
				$processed = array( array( "(" . $value . ")") );
				$rets      = self::mergeSingle( $rets, $clauseValues );
			}
			else {
				$found = false;
			}

			if($found){
				foreach ($processed as $proc) {
					if ( $proc[ 0 ] !== false && !is_null( $proc[ 0 ] ) )
						self::addIfNotInArray( $ins->_clauses, $proc[ 0 ], true );

					// extra joins
					if ( isset( $proc[ 1 ] ) && $proc[ 1 ] !== false ) {
						foreach ( (array)$proc[ 1 ] as $joinz )
							self::addIfNotInArray( $ins->_joins, $joinz, true);
					}
				}
			}
		}

	}

	/**
	 * process incoming value for fields
	 *
	 * @param mixed $value
	 * @param string $forceType
	 * @return array
	 * @author Özgür Köy
	 */
	private static function processValue(JModel &$ins, &$relatedTable, &$field, &$value, &$passedClause=null, &$joinClause=null, &$clauseValues=null, $forceToken=null )
	{
		#global $dps;
		//0->clause , 1->value
		$ret = array();

		if ( is_string( $value ) || is_numeric( $value )  ) {
			if ( !is_null( $passedClause ) /* && $relatedTable == $field*/ ) {
				if ( array_key_exists( $field, $ins->_relatives ) && is_numeric( $value ) ) {
					$value = array("id"=>$value);
				}
				else {
//					if ( !is_null( $clauseValues ) ){
//						$clauseValues[ ] = $value;
//					}

					$passedClause = JUtil::aposArray( explode( ".", $passedClause ), '`' );
					$token = is_null($forceToken)?self::getToken( $clauseValues ):$forceToken;
					$clauseValues[ $token ] = $value;
					$ar           = array( array( implode( ".", $passedClause ) . "=".$token ) );
					return $ar;
				}
			}
			else {
				$processedString = self::processString( $field, $value, $relatedTable, $clauseValues, $forceToken );
				if ( !is_null( $joinClause ) && strpos( $joinClause, "|JOINS_" ) !== false ) {
					if ( strlen( $processedString[ 0 ] ) > 0 ) {
						#array_unshift($dps,$processedString[ 1 ]);
						if ( !is_null( $clauseValues ) ) {
//							array_push( $clauseValues, $processedString[ 1 ] );
//							array_unshift( $clauseValues, $processedString[ 1 ] );
						}


						$joinClause = str_replace( "|CLAUSES_{$relatedTable}|", $processedString[ 0 ]." AND |CLAUSES_{$relatedTable}|", $joinClause );
					}
					return array( array( null ) );
				}
				else {
					//NOT N-N
					#$dps[]=$processedString[ 1 ];
					if ( !is_null( $clauseValues ) /*&& $field != "_custom"*/ )
						return array( $processedString );
				}

			}
		}

		if ( is_array( $value ) && self::isRelative( $ins, $value, $field ) ) {
			if(array_key_exists($field, $ins->_relatives)){
				if ( isset( $value[ 0 ] ) ){
					//we use the array for ids
					$value = array( "id" => $value );}
				else{
					#lurk deeper
//					self::arrayTheIntegers( $value );
				}
			}

			// relative array with like "owner"=>array("name"=>"\abc%")
			$tm0 = array();
			foreach ( $value as $key => $val ) {
//				echo $key . PHP_EOL;
				$relationField = $ins->_relatives[ $field ];
				$claz          = ucfirst( $relationField );
				$tmp           = new $claz();

				if ( is_array( $val ) ) {
					// omg another relationship??
					$fromRemote = (array)self::load( $tmp, array( $key => $val ), true, $clauseValues,false );
					if ( sizeof( $fromRemote ) > 0 ) {

						$cnMax = max( sizeof( $fromRemote[ "clauses" ] ), sizeof( $fromRemote[ "clauseValues" ] ), sizeof( $fromRemote[ "joins" ] ) );
						for ( $tmIndex = 0; $tmIndex < $cnMax; $tmIndex++ ) {
							#replacing the relation name with the tag.
							$join   = isset( $fromRemote[ "joins" ][ $tmIndex ] ) ?
								str_replace( "`" . $relationField . "`.", "`" . $field . "`.", $fromRemote[ "joins" ][ $tmIndex ] ) :
								false;

							$clauz  = isset( $fromRemote[ "clauses" ][ $tmIndex ] ) ?
								str_replace( "`" . $relationField . "`.", "`" . $field . "`.", $fromRemote[ "clauses" ][ $tmIndex ] ) :
								false;

							#n-n management
							if ( !is_null( $joinClause ) && isset( $fromRemote[ "joins" ][ $tmIndex ] ) && strpos( $joinClause, "|JOINS_" ) !== false && $join != false ) {
								$joinClause = str_replace( "|JOINS_{$field}|", "|JOINS_{$field}| " . $join, $joinClause );
//								echo $joinClause ;

//								echo "!!!!" . PHP_EOL. PHP_EOL. PHP_EOL;

								$join = false;
								#clause placement too.
//								var_dump( $clauz );
//								echo $key;
								if ( $clauz !== false ){
									$joinClause = str_replace( "|CLAUSES_{$key}|", $fromRemote[ "clauses" ][ $tmIndex ]." AND |CLAUSES_{$key}|" , $joinClause );
									$joinClause = str_replace( "|CLAUSES_{$field}|", $fromRemote[ "clauses" ][ $tmIndex ]." AND |CLAUSES_{$field}| ", $joinClause );
								}
								$clauz = false;
								#$valuez=false;
							}
							elseif ( !is_null( $joinClause ) && strpos( $joinClause, "|JOINS_" ) !== false && $clauz!==false ) {
								#N-N thing , for id=>array and like.
								$joinClause = str_replace( "|CLAUSES_{$key}|", $clauz." AND |CLAUSES_{$key}|", $joinClause );
								$joinClause = str_replace( "|CLAUSES_{$field}|",  $clauz." AND |CLAUSES_{$field}|", $joinClause );
								$clauz = false;


							}
							else{

							}

							#array_unshift($dps,isset( $fromRemote[ "clauseValues" ][ $tmIndex ] ) ? $fromRemote[ "clauseValues" ][ $tmIndex ] : false);
							#if ( !is_null( $clauseValues ) )
							#array_unshift( $clauseValues, $valuez );

							$tm0[ ] = array(
								$clauz,
								$join
							);
						}
					}
				}
//				elseif(is_object($val)){
//					echo
//				}
				else {
					#TODO: CHECK FOR DIRECT N-N PROPERTY VALUE SETTING
					$t0     = null;

					$tm1    = self::processValue( $tmp, $relatedTable, $key, $val, $t0, $joinClause, $clauseValues );
					$tm0[ ] = reset( $tm1 ); // returning value will be array too.
				}
			}
			return $tm0;
		}
		elseif ( is_object( $value ) && self::isRelative( $ins, $value ) && is_numeric( $value->id ) && $value->id > 0 ) {
			$remo = $ins->_remoteConnections[ $field ];

			if(is_null($passedClause) && $remo["type"]="NtoN"){
				// NtoN specific problem
				$tmpField = "id";
				$processedString = self::processString( $tmpField, $value->id, $remo["table"], $clauseValues, $forceToken );

				$joinClause1 = str_replace( "|CLAUSES_{$remo["table"]}|", $processedString[ 0 ]." AND |CLAUSES_{$remo["table"]}|", $remo["join"] );
				$joinClause1 = str_replace( "`" . $ins->selfname . "`.", "`" . $relatedTable . "`.", $joinClause1 );
				$joinClause .= $joinClause1;

				return array( array( null ) );
			}

			$passedClause = JUtil::aposArray( explode( ".", $passedClause ), '`' );
			// relative object

			$token = is_null($forceToken)?self::getToken( $clauseValues ):$forceToken;
			$clauseValues[ $token ] = $value->id;

			return array( array( implode(".",$passedClause) . "=".$token ) );
		}
		elseif ( is_array( $value ) && sizeof( $value ) > 0 ) {
			$v0 = reset( $value );
			if ( is_array( $v0 ) ){
				$value = $v0;
				unset( $v0 );
			}

				//must be numeric id array{}
			if ( sizeof( $value ) > 1 ) {
				$tokens = array();

				foreach ( $value as $v0 ) {
					$token = is_null($forceToken)?self::getToken( $clauseValues ):$forceToken;
					$clauseValues[ $token ] = $v0;
					$tokens[ ]              = $token;
				}

				return array( array( "`" . $relatedTable . "`.`" . $field . "` IN (" . implode( ",", $tokens ) . ")" ) );

			}
			else {
				$tok = is_null($forceToken)?self::getToken( $clauseValues ):$forceToken;
				$clauseValues[ $tok ] = reset( $value );

				return array( array( "`" . $relatedTable . "`.`" . $field . "`=" . $tok ) );
			}
		}
		else{
			return array();}
	}

	/**
	 * place data from query to instance
	 *
	 * @param object $ins
	 * @param string $data
	 * @return bool
	 * @author Özgür Köy
	 */
	private static function populateData(&$ins, &$data)
	{
		$ins->dataIndex = -1;
		$ins->_ids      = array();
		$ins->gotValue  = false;
		if(!is_array($data) || !(sizeof($data)>0)){
			// $ins->data
			$ins->dataIndex = null;
			return false;
		}

		$ins->data      = $data;
		if ( $ins->groupOperation !== true ) {
			foreach ( $data as $dInd => $data1 ) {
				//load ids
				$ins->_ids[ ] = $data1[ "id" ];
			}
		}

		return true;

	}

	/**
	 * populate data of the object
	 *
	 * @param bool $full load all relatives too
	 * @param bool $back backwards populating.
	 * @return bool|Jmodel
	 * @author Özgür Köy
	 */
	public static function populate( JModel &$ins, $full = false, $back = false ) {
		if ( is_null( $ins->dataIndex ) ) {
			return false;
		}

		if (
			(
				( !$back && $ins->dataIndex == sizeof( $ins->data ) - 1 ) || // max index reached
				( $back && $ins->dataIndex == 0 )
			)
//			&& $ins->groupOperation === false
		) { // index at minimum
			/*doing extra population*/
			if ( $ins->_populateOnce === false && $ins->groupOperation !== true ) {
				if ( !$ins->gotValue ) // not populated return false
					return false;

				$ins->limitLow = $ins->limitLow + $ins->limitCount;
				if(is_null($ins->_whereParams))
					self::load( $ins, $ins->loadParams, false  );
				else{
					$ins->_whereParams->indexes = array();
					$ins->_whereParams->clauseValues = array();
					$ins->customClause = null;
//					print_r( $ins );
					self::load( $ins );
				}

				if ( $ins->nonPopulate )
					self::populate( $ins ); // < required for reloads, other wise it will have the latest data

				if ( is_null( $ins->dataIndex ) )
					return false;

				return $ins;
			}
			else{
//				echo sizeof($ins->data);
				return false;
			}


		}

		// else
		// 	return false;

		self::clearSelf($ins);

		JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
		$ins->gotValue = true;
		$ins->dataIndex += ( $back && $ins->dataIndex != 0 ? -1 : 1 );


		/*rendering data below*/
		foreach ($ins->_fields as $_f) {
			if(isset( $ins->data[$ins->dataIndex][$_f] )){
				$ins->$_f = $ins->data[$ins->dataIndex][$_f];
			}
		}

		if ( sizeof( $ins->_extraFields ) > 0 ) {
			$extras = array_keys( $ins->_extraFields );
			foreach ( $extras as $_f ) {
				if ( array_key_exists( $_f ,$ins->data[ $ins->dataIndex ] ) ) {
					$ins->$_f = $ins->data[ $ins->dataIndex ][ $_f ];
				}
			}
			unset( $extras );
		}

		if(property_exists($ins,"_relatives")){

			foreach ($ins->_relatives as $_f=>$_ff0) {
				if(isset( $ins->data[$ins->dataIndex][$_f] )){
					$ins->$_f = $ins->data[$ins->dataIndex][$_f];
				}
			}

		}
		// if full , load connecteds
		if($full==true && property_exists($ins,"_relatives")){
			foreach ($ins->_relatives as $ps => $prel) {
				$ps = "load".ucfirst($ps);

				if( method_exists($ins, $ps) ){
					$ins->$ps();
				}
			}
		}

		if ( sizeof( $ins->_loadingRelatives ) > 0 && property_exists( $ins, "_relatives" ) )
			self::_relativesAssign( $ins, $ins->_loadingRelatives, $ins->data, $ins->dataIndex );

		$ins->populated = true;

		return $ins;
	}

	/**
	 * assign loading relatives
	 *
	 * @param JModel $mainObject
	 * @param        $ar
	 * @param        $data
	 * @param        $dataIndex
	 * @author Özgür Köy
	 */
	private static function _relativesAssign( JModel &$mainObject, &$ar, &$data, &$dataIndex ){
		foreach($ar as $relative=>$f0){
			$fields       = $f0[ "fields" ];
			$table        = ucfirst( $f0[ "table" ] );
			$injectFields = array();
			foreach ( $fields as $field ) {
				if ( array_key_exists( $field, $data[ $dataIndex ] ) ) {
					$f1 = explode( ".", $field );
					//$fieldName = str_replace( $relative . ".", "", $field );
					$fieldName = array_pop( $f1 );
					$fvalue    = $data[ $dataIndex ][ $field ];

					if ( !is_null( $fvalue ) && strlen( $fvalue ) > 0 )
						$injectFields[ $fieldName ] = $fvalue;
				}
//				else{
//					echo "PROBLEM FIELD DOESNT EXIST:::".$field . PHP_EOL;
//					print_r( $data[ $dataIndex  ] );
//				}
			}

			$mainObject->$relative = new $table;

			if ( sizeof( $injectFields ) )
				$mainObject->$relative->__inject( $injectFields );

			if ( sizeof( $f0[ "subs" ] ) > 0 )
				self::_relativesAssign( $mainObject->$relative, $f0[ "subs" ], $data, $dataIndex );
		}
	}

	/**
	 * to clear the variables and relatives before populate
	 *
	 * @return bool|void
	 **/
	public static function clearSelf(&$ins)
	{
		// return;
		if(!isset($ins->_fields) || !sizeof($ins->_fields)>0) return false;

		foreach ($ins->_fields as $_f) {
			$ins->$_f = null;
		}

		//$ins->groupOperation = false;

		if(property_exists($ins,"_relatives")){
			foreach ($ins->_relatives as $_f=>$_ff0) {
				$ins->$_f = null;
				// unset($ins->$_f);
			}
		}
	}

	/**
	 * @param JModel $ins
	 * @return JModel
	 */
	public static function resetIndex(JModel &$ins) {
		$ins->dataIndex = -1;
		$ins->limitLow  = 0 ;
		$ins->_orderByInjected = false;
		self::clearSelf($ins);

		$ins->populate();
		return $ins;
	}

	/**
	 * count , can only be called if load is run
	 *
	 * @param JModel $ins
	 * @param string $groupBy
	 * @return object
	 */
	public static function count(JModel &$ins, $groupBy="id")
	{
		global $__DP;

		if(is_null($ins->customClause))
			$ins->buildQuery();
		$options = array(
			"docount" 	=> true,
			"groupby" 	=> null, //$groupBy;->wont work:)
			"clauses" 	=> $ins->customClause
		);

		include($__DP."/".JDef::$queryFolder.$ins->selfname.".php");
	  	$q = $ins->modelQuery($ins->customClause);

		JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
		$r0 = JPDO::executeQuery($q);


		return (int)$r0[0]["cid"];
	}

	/**
	 * delete the current id only, or if an id or an array of ids
	 *
	 * @return bool
	 **/
	public static function delete( JModel &$ins, $id=null)
	{
		global $__DP;
		if(is_null($id) && (!($ins->id>0) || is_null($ins->id))) return false;

		JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );

		if(is_array($id)){
			$ids = $id;
			$q   = $ins->modelQuery("deleteMulti");
			$q  .= " IN (".(JUtil::multistr("?", sizeof($id))).")";
		}
		else{
			$ids = $ins->id;
			$q   = $ins->modelQuery("delete");
		}


			JPDO::executeQuery(self::checkNameOverride($ins, $q), $ids);

		return true;
	}

	/**
	 * load 1-1 remote connection
	 *
	 * @param object $ins object instance
	 * @param string $objectType  the type of the relative object
	 * @param string $tag the label of connection between two object types
	 * @param bool $reverse if 1-N connection and this one is the N
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function loadRemote($ins, $objectType, $tag, $reverse=false)
	{
		if($reverse){
			if(!($ins->id>0) || is_null($ins->id)) {
				return false;
			}

			$cn = ucfirst($objectType);
			// echo print_r(array($tag=>array("id"=>$ins->id)), true);
			$ins->$tag = new $cn(array($tag=>array("id"=>$ins->id)));
			return $ins->$tag;
		}

		if(!isset($ins->$tag) ){
			// echo PHP_EOL.$tag;
			// print_r($ins);
			return false;
		}

		//already an object. return away
		if(is_object($ins->$tag))
			return $ins->$tag;

		if(is_numeric($ins->$tag) && $ins->$tag>0){
			$cn = ucfirst($objectType);
			$ins->$tag = new $cn($ins->$tag);
		}
		return $ins->$tag;
	}

	/**
	 * save remote 1-1 and 1-N
	 *
	 * @param object $ins object instance
	 * @param mixed $id id of the relative object, might be of many types
	 * @param string $objectType the type of the relative object
	 * @param string $tag the tag. basically the label of connection between two object types
	 * @return object
	 * @author Özgür Köy
	 */
	public static function

	saveRemote(&$ins, $id, $objectType, $tag, $reverse=false)
	{
		global $__DP;
		if(!($ins->id>0) || is_null($ins->id)) {return false;}

		if(is_numeric($id)) $id = array( $id );

		if(is_array($id)) {
			//well , it's a reverse
			$q = $ins->modelQuery("save_".$tag);

			JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
			foreach ($id as $idk) {
				if ( $reverse == false )
					JPDO::executeQuery( $q, $idk, $ins->id );
				else
					JPDO::executeQuery( $q, $ins->id, $idk );

				$ins->$tag = $idk;

			}
			JPDO::revertDsn();

		}
		elseif(is_null($id)){
			//id is null, we are using the already loaded object to save.
			if(!is_object($ins->$tag)) return false; // not defined

			$q = $ins->modelQuery("save_".$tag);

			if(!$reverse){
				JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
				JPDO::executeQuery($q, $ins->$tag->id, $ins->id);
			}
			else{
				JPDO::connect( JCache::read('JDSN.'.$ins->$tag->dsn) );
				JPDO::executeQuery($q, $ins->id, $ins->$tag->id);
			}
			JPDO::revertDsn();
		}
		elseif(is_object($id) && isset($id->selfname)){
			//an object is passed
			if(!($id->id>0)) return false;

			$q = $ins->modelQuery("save_".$tag);
			// JPDO::debugQuery();

			if(!$reverse){
				JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
				JPDO::executeQuery($q, $id->id, $ins->id);
			}
			else{
				JPDO::connect( JCache::read('JDSN.'.$id->dsn) );
				JPDO::executeQuery($q, $ins->id, $id->id);
			}
			JPDO::revertDsn();

			$ins->$tag = $id;
		}

		return $ins;
	}

	/**
	 * save the self fields, only for the current id, important.
	 *
	 * @return JModel|bool
	 **/
	public static function save(JModel &$ins)
	{
		global $__DP;

		if($ins->groupOperation === true)
			return $ins;

		JPDO::connect( JCache::read('JDSN.' . $ins->dsn) );
		$good = false;

		if(is_array($ins->id)) {
			$ins->id = reset($ins->id);
		}

		if ( $ins->_overrideId == false && ( $ins->id > 0 ) && isset( $ins->_fields ) ) {
			//editing
			$q          = $ins->modelQuery( "edit" );
			$fieldArray = self::buildFieldArray( $ins, array( "id" => $ins->id ) );

			$k          = JPDO::executeQuery( self::checkNameOverride($ins, $q), array_values( $fieldArray ) );

			$good = true;
		}
		elseif ( $ins->_overrideId || ( $ins->id == null ) || ( $ins->id == 0 ) ) {
			$fieldArray = array_values( self::buildFieldArray( $ins ) );

			//inserting
			if ( $ins->_overrideId && $ins->id>0) {
				$fieldArray[ 0 ] = $ins->id;
				array_unshift( $fieldArray, null );
				$q = $ins->modelQuery( "save_id" );
			}
			else
				$q = $ins->modelQuery( "save" );

			$k = JPDO::executeQuery( self::checkNameOverride($ins, $q), $fieldArray );

			if ( $ins->_overrideId !== true ){
				$lid = JPDO::lastId( $ins->dbName, is_null( $ins->selfnameOverride ) ? $ins->selfname : $ins->selfnameOverride );

			}else {
				JPDO::resetSequence( $ins->dbName, $ins->selfname );
				$lid = $ins->id;
			}

			if (  $lid > 0 ) {
				//just effect the active id. nice.
				$ins->id   = $lid;
				$ins->_ids = array( $ins->id );
				$good      = true;

			}
			else{
//				var_dump( $lid );
//				echo $ins->selfname;
//				echo "LAST ID NOT FOUND";
			}
		}

		if ( $good == false && $ins->_overrideId !== true && JPDO::$queryCollecting !== true ) {
			//something went wrong
			JLog::log( "user", "insert failure:" . $ins->selfname );

			return false;
		}
		else
			return $ins;
	}

	/**
	 * build field array
	 *
	 * @param object $ins
	 * @return array
	 * @author Özgür Köy
	 */
	private static function buildFieldArray( JModel &$ins, $append = array() ) {
		$c0 = array( null );
		if ( !isset( $ins->_fields ) || !sizeof( $ins->_fields ) > 0 ) {
			return array();
		}

		//read fields and fill an array
		for ( $fi = 2; $fi < sizeof( $ins->_fields ); $fi++ ) {
			$_field = $ins->_fields[ $fi ];

			if ( strlen( $_field ) == 0 )
				$_field = null;
			else
				$_field = $ins->_fields[ $fi ];

			if ( is_array( $ins->$_field ) )
				$c0[ $_field ] = isset( $ins->$_field[ 0 ] ) ? $ins->$_field[ 0 ] : '';
			elseif ( is_object( $ins->$_field ) )
				$c0[ $_field ] = isset( $ins->$_field->id ) ? $ins->$_field->id : '';
			else
				$c0[ $_field ] = $ins->$_field;

			#append field defaults
			if ( is_null( $c0[ $_field ] ) && isset( $ins->_fieldDefaults[ $_field ] ) )
				$c0[ $_field ] = $ins->_fieldDefaults[ $_field ];

		}

		foreach ( $append as $ap => $ed )
			$c0[ $ap ] = $ed;

		return $c0;
	}

	/**
	 * delete 1-1, and 1-N. be careful, no validation->speed!
	 *
	 * @param object $ins   main object
	 * @param string $tag   relation tag
	 * @param bool   $reverse
	 * @param mixed  $id    optional id.
	 * @param bool   $one2N one2N ops.
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function deleteOne(JModel &$ins, $tag, $reverse=false, $id=null, $one2N=false)
	{
		global $__DP;

		if(is_array($ins->id)) $ins->id = reset($ins->id);

		if(!($ins->id>0) || is_null($ins->id)) return false;

		if($reverse){
			$q         = $ins->modelQuery("delete_".$tag);
			if($id=="all"){
				$q         = $ins->modelQuery("deleteAll_".$tag);
				$remoteId  = array($ins->id);
			}
			elseif(is_array($id)){
				$q	 	  .= " IN (".(JUtil::multistr("?", sizeof($id))).")";
				array_unshift($id, $ins->id);

				$remoteId  = $id;
			}
			elseif(is_numeric($id)){
				$q		  .= "=?";

				$remoteId  = array($ins->id, $id);
			}
			elseif(is_object($id)){
				$q		  .= "=?";
				$remoteId  = array($ins->id, $id->id);
			}
			elseif(is_null($id)){
				//supposed to have a loaded relative
				if($one2N){
					$q		  .= "=?";
					$remoteId  = array($ins->id, (is_object($ins->$tag) ? $ins->$tag->id  : $ins->$tag));
				}
				else
					$remoteId  = array(is_object($ins->$tag) ? $ins->$tag->id  : $ins->$tag);
			}

			$dsn = is_object($ins->$tag) ? $ins->$tag->dsn : ($ins->getRelativeDsn($tag)) ;
		}
		else{
			$q        = $ins->modelQuery("delete_".$tag);
			$remoteId = array( is_null($id) ? $ins->id : (is_object($id)?$id->id:$id) );
			$dsn      = $ins->dsn;
		}

		// jpdo execution fix
		array_unshift($remoteId,0);

		JPDO::connect( JCache::read('JDSN.'.$dsn ) );
		JPDO::executeQuery($q, $remoteId);

		$ins->$tag = null;

		JPDO::revertDsn();

		return true;
	}

	/**
	 * delete N-N
	 *
	 * @param object $ins main object
	 * @param string $tag relation tag
	 * @param mixed $id
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function deleteMany(&$ins, $tag, $id=null, $reverse=false)
	{
		if(!($ins->id>0) || is_null($ins->id) || !is_numeric($ins->id)) return false;

		$id       = is_null($id) ? array() : array($id);

		$q        = $ins->modelQuery("delete_".$tag,array("deleteAll"=>sizeof($id)==0));
		$_prevDsn = JCache::read('JDSN.'.$ins->dsn);

		if($reverse)
			$dsn = is_object($ins->$tag) ? $ins->$tag->dsn : ($ins->getRelativeDsn($tag)) ;
		else
			$dsn = $ins->dsn;

		JPDO::connect( JCache::read('JDSN.'.$dsn) );
		// JPDO::debugQuery();

		if(sizeof($id)>0){
			//delete one or more
			foreach ($id as $i0v) {
				if(is_object($i0v) && isset($i0v->id) && $i0v->id>0)
					$i0v = $i0v->id;

				if(!is_numeric($i0v)) continue;

				// if(!$reverse)
					JPDO::executeQuery($q, $ins->id, $i0v);
				// else
					// JPDO::executeQuery($q, $i0v, $ins->id);
			}
		}
		else{
			//delete all!
			// if($reverse)
				JPDO::executeQuery($q, $ins->id);
			// else
				// JPDO::executeQuery($q, $ins->id);
		}
		// JPDO::debugQuery(false);

		if(is_null($id))
			$ins->$tag = null;

		JPDO::connect($_prevDsn);
		return true;
	}


	/**
	 * check if the object is relative
	 *
	 * @return bool
	 **/
	private static function isRelative(&$ins, &$obj, &$tag=null)
	{
		if($tag == "id")
			return false;
		elseif( is_object($obj) && ((isset($obj->selfname) && in_array($obj->selfname, $ins->_relatives)) || method_exists($obj, "selfname")) ){
			if(isset($obj->selfname))
				$oself = $obj->selfname;
			else
				$oself = call_user_func(array($obj, 'selfname'));


			foreach($ins->_relatives as $rk=>$rv){
				if($rv==$oself)
					return $rk;
			}
		}
		elseif(is_array($obj) && isset($ins->_relatives[$tag])){
			return true;
		}

		return false;
	}

	/**
	 *
	 * @param JModel $ins
	 * @param        $q
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function checkNameOverride(JModel &$ins, $q) {
		if ( !is_null( $ins->selfnameOverride ) ){
			$q = preg_replace( "/`".$ins->selfname."`/", "`".$ins->selfnameOverride."`", $q );
			$q = preg_replace( "/".$ins->selfname."\./", "`{$ins->selfnameOverride}`.", $q );
			$q = preg_replace( "/\.".$ins->selfname." /", ".`{$ins->selfnameOverride}` ", $q );
		}
		return $q;
	}

	/**
	 * filter preparation
	 *
	 * @author Özgür Köy
	 */
	public static function filter() {
		$filters = func_get_args();
		$ins     = array_shift( $filters );
		$filters = reset( $filters ); #rid of the array key

		list( $exCls, $exClsValues ) = self::filterProcess( $ins, $filters );
		if(sizeof($exClsValues>0)){
			$ins->extraClause[ ]   = implode( " ", $exCls );
			$ins->_injectValues    = array_merge( $ins->_injectValues, $exClsValues );
		}
	}

	/**
	 * filter process
	 *
	 * @param JModel $ins
	 * @param        $filters
	 * @return array
	 * @author Özgür Köy
	 */
	private static function filterProcess(JModel &$ins, $filters){
		$o0          = ucfirst( $ins->selfname );
		$exCls       = array();
		$exClsValues = array();

		foreach ( $filters as $key=>$filterArray ) {
			if ( is_string( $filterArray ) ) {
				$exCls[ ] = " " . $filterArray;
				continue;
			}
			elseif ( is_array( $filterArray ) ) {
//				$vk = reset( $filterArray );
				$vk = key( $filterArray );

				if ( is_numeric( $vk ) ) { #not a field mate
					list( $cls, $vls ) = self::filterProcess( $ins, $filterArray );
					$exCls[ ]    = "(";
					$exClsValues = array_merge( $exClsValues, $vls );
					$exCls       = array_merge( $exCls, $cls );
					$exCls[ ]    = ")";
					continue;
				}
			}

			$obj = new $o0();
			$cl0 = array();

			self::load( $obj, $filterArray, false, $cl0, true );

			$obj->_query = preg_replace( "/SELECT (?!DISTINCT).+?FROM/s", "SELECT \"{$ins->selfname}\".id FROM", $obj->_query ); #remove unnecessary fields
//			$obj->_query = preg_replace( "/ORDER BY.+?(ASC|DESC)/s", "", $obj->_query ); #remove order by;
			$obj->_query = preg_replace( "/ORDER BY.+?(ASC|DESC)(\\s|\\n)+(NULLS.+?(LAST|FIRST))?/uism", "", $obj->_query ); #remove order by;
			$obj->_query = preg_replace( "/LIMIT.+$/s", "", $obj->_query ); #remove limit

			$exCls[ ]    = "(
				`{$ins->selfname}`.`id` NOT IN (" .
				$obj->_query .
				")
			)";

			$exClsValues = array_merge( $exClsValues, $obj->_clauseValues );
			unset( $obj );
		}

		return array( $exCls, $exClsValues );
	}

	/**
	 * just prepare the fields with specified tag for query
	 *
	 * @param string $fields
	 * @param string $table
	 * @return void
	 * @author Özgür Köy
	 */
	private static function prepareFieldsForQuery(&$fields,$tag)
	{
		foreach ($fields as $fk=>$fv)
			$fields[$fk] = $tag.".".$fv;
	}

	/**
	 * add not present in array, incredibly life saving world changing method
	 *
	 * @param array $array
	 * @param string $value
	 * @return void
	 * @author Özgür Köy
	 */
	private static function addIfNotInArray( &$array, $value, $filterReplacements=false, $doReplace=true ) {
//		echo $value;
		//get rid of the possible replacements
		if($filterReplacements==false){
			if ( !in_array( $value, $array ) )
				array_push( $array, $value );
		}
		else{
			$valueAlt = preg_replace( "|%JREP[a-z0-9]+?%|i", "?", $value );
			$found  = false;
			foreach ( $array as &$av0 ) {
				if( $valueAlt == preg_replace( "|%JREP[a-z0-9]+?%|i", "?", $av0 ) ){
					if($doReplace === true)
						$av0 = $value;
					$found = true;
					break;
				}
			}

			if($found == false)
				array_push( $array, $value );
		}
	}

	/**
	 *
	 *
	 * @param $toArray
	 * @param $fromArray
	 * @return array
	 * @author Özgür Köy
	 */
	public static function mergeSingle( &$toArray, $fromArray ){
//		return array_merge( $toArray, $fromArray );

		$f = reset( $fromArray );

		if ( is_array( $f ) ) {
			foreach ( $fromArray as $at0 ) {
				$toArray = array_merge( $toArray, (array)$at0 );
			}
		}
		elseif ( $f !== false && !is_null($f) ){
			return array_merge( $toArray, $fromArray );
		}

		return $toArray;
	}

	/**
	 * DEPRECATED METHOD
	 * basically convert single integers to arrays for id controls
	 *
	 * @param $ar
	 * @author Özgür Köy
	 */
	public static function arrayTheIntegers( &$ar ) {
		foreach ( $ar as $ak => &$av ) {
			if(is_numeric($av) &&
				sizeof($ar)==1
				&& !is_numeric($ak)
			){ #single integers
//				if(is_array())
//				echo "INTD";
				$av = array( $av );
			}
			elseif( is_array($av) )
				self::arrayTheIntegers( $av );

		}
//		print_r( $ar );

	}

	/**
	 * parse the clause array , find out and replace with replacements
	 *
	 * @param       $vars
	 * @param array $keys
	 * @param array $replacements
	 * @author Özgür Köy
	 */
	public static function parseVars( &$vars, &$keys=array(), &$replacements=array() ) {
		foreach ( $vars as $v0 => &$v1 ) {
			$place = true;
			if ( is_array( $v1 ) && self::arrayHasIndexes( $v1 ) === true ) {
				$keyType = "S_";
				$keys[ ] = $keyType.$v0;

				self::parseVars( $v1, $keys, $replacements );
			}
			else {
				if ( !is_array( $v1 ) ) {
					$replacement = mt_rand( 10000, 20000 );

					if ( is_object( $v1 ) ) {
						$keyType                      = "O_";
						$replacements[ $replacement ] = $v1->id;
					}
					elseif ( is_string( $v1 ) ) {
						if ( $v0 == "_custom" ){
							$keyType                      = "CC_";
							$replacement = $replacement."=".$replacement;
							$replacements[ $replacement ] = $replacement;
						}
						else{
							//we don't deal with strings.
							$keyType = "STR_";
							$place = false;
						}
					}
					else {
						$keyType                      = "R_";
						$replacements[ $replacement ] = $v1;

					}

					$keys[ ] = $keyType.$v0;
					if($place === true)
						$v1 = $replacement;
				}
				else {
					$keyType = "A_(".sizeof($v1).")"; //we index with array length
					$keys[ ] = $keyType.$v0;
					//regular array for IN()
					foreach ( $v1 as &$v2 ) {
						$replacement                  = mt_rand( 10000, 20000 );
						$replacements[ $replacement ] = $v2;
						$v2                           = $replacement;
					}
				}
			}

		}
	}

	/**
	 * @param $vars
	 * @return array|string
	 * @author Özgür Köy
	 */
	public static function hashTheClauses( &$vars ) {
		$keys = array();

		self::createKeyHash( $vars, $keys );

		$keys = md5( serialize( $keys ) );

		return $keys;
	}

	/**
	 * read the cache
	 *
	 * @param       $vars
	 * @param array $replacements
	 * @return array
	 * @author Özgür Köy
	 */
	public static function cacheRead( &$vars, &$replacements = array() ) {
		$keys = array();
		self::straightTheClause( $vars, $keys );
//		print_r( $replacements );
//		print_r( $keys );
//		exit;
		$newKeys = array();
		foreach ( $replacements as $v0=>$v1 )
			$newKeys[ $v1 ] = $keys[ $v0 ];

		ksort( $newKeys );
//		print_r( $newKeys );
		return $newKeys;
	}

	/**
	 * straight the clause
	 *
	 * @param       $vars
	 * @param array $response
	 * @author Özgür Köy
	 */
	public static function straightTheClause( &$vars, &$newKeys = array() ) {
		foreach ( $vars as &$v1 ) {
			if ( is_array( $v1 ) )
				self::straightTheClause( $v1, $newKeys );
			elseif ( is_object( $v1 ) )
				$newKeys[ ] = $v1->id;
			else
				$newKeys[ ] = $v1;
		}
	}

	/**
	 * create a hash for cache
	 *
	 * @param       $vars
	 * @param array $keys
	 * @author Özgür Köy
	 */
	public static function createKeyHash( &$vars, &$keys=array() ) {
		foreach ( $vars as $v0 => &$v1 ) {
			if ( is_array( $v1 ) && self::arrayHasIndexes( $v1 ) === true ) {
				$keyType = "S_";
				$keys[ ] = $keyType.$v0;
				self::createKeyHash( $v1, $keys );
			}
			else {
				if ( !is_array( $v1 ) ) {
					if ( is_object( $v1 ) )
						$keyType = "O_";
					else
						$keyType = "R_";

//					echo $v1.PHP_EOL;
					$keys[ ] = $keyType.$v0;
				}
				else {
//					foreach ( $v1 as $v2 ) {
//						echo $v2 . PHP_EOL;
//					}

					$keyType = "A_(".sizeof($v1).")"; //we index with array length
					$keys[ ] = $keyType.$v0;
				}
			}
		}
	}

	/**
	 * simplify the clause for single integers
	 *
	 * @param $ins
	 * @param $clauses
	 * @author Özgür Köy
	 */
	private static function simplifyClause( &$ins, &$clauses ) {
		if ( is_object( $clauses ) && get_class( $clauses ) == "JPP" )
			return true;

		if ( !is_array( $clauses ) ){
			$clauses = array( "id" => $clauses );
		}

		foreach ( $clauses as $v0 => &$v1 ) {
			if ( is_array( $v1 ) && self::arrayHasIndexes( $v1 ) === true )
				self::simplifyClause( $ins, $v1 );
			elseif(is_int($v1))
				$v1 = array( $v1 );
		}
	}

	/**
	 * check if the array contains keys or a regular value array
	 *
	 * @param $arr
	 * @return bool
	 * @author Özgür Köy
	 */
	private static function arrayHasIndexes( &$arr ) {
		foreach ( $arr as $a0 => $a1 ) {
			if ( is_string( $a0 ) || ( is_array( $a1 ) && self::arrayHasIncrementalIndex( $a1 ) !== true ) ) {
				return true;
			}

		}

		return false;
	}

	/**
	 * check if it's a value array or a deeper one
	 *
	 * @param $ar
	 * @return bool
	 * @author Özgür Köy
	 */
	public static function arrayHasIncrementalIndex( &$ar ){
		$aks = array_keys( $ar );
		foreach ( $aks as $a0 ) {
			if ( !is_int( $a0 ) )
				return false;
		}

		return true;

	}

	public static function matchReplacements( $_originalClause){

	}

	/**
	 * get random string for query replacements
	 *
	 * @param     $arrayCheck
	 * @param int $size
	 * @return array|string
	 * @author Özgür Köy
	 */
	public static function getToken( &$arrayCheck, $size = 1 ) {
		if ( $size == 1 ){
			$s = "%JREP" . self::$_innerCounter++ . "%";

			if(!in_array($s,$arrayCheck))
				return $s;
			else{
				return self::getToken( $arrayCheck );}
		}
		else {
			$toks = array();
			$i0 = 0;
			while($i0<$size){
				$s = "%JREP" . self::$_innerCounter++ . "%";
				if(!in_array($s,$arrayCheck)){
					$toks[] = $s;
					$i0++;
				}
			}

			return $toks;
		}
	}

	/**
	 * create the query for execution
	 *
	 * @param $query
	 * @param $ins
	 * @author Özgür Köy
	 */
	private static function buildQueryExec( &$ins ) {
		$params = array();
//		print_r( $ins->_clauseValues );
		$ins->_query = preg_replace_callback( "|%JREP[a-z0-9]+?%|i", function ( $e ) use ( &$ins, &$params ) {
			if ( array_key_exists( $e[ 0 ], $ins->_clauseValues ) === false ) {
				echo $ins->_query;
				print_r( $e );
				print_r( $ins->_clauseValues );
//				echo $query;
//				return "?";
//				exit;
			}
			$params[ ] = $ins->_clauseValues[ $e[ 0 ] ];
			return "?";
		}, $ins->_query );

		$ins->_clauseValues = $params;
	}

} // END class JModel


class JPP{ #preposition
	public $filters = array();
	public $clauses = array();

	public $glue    = null;
	public $name = "%JPP";

	public static $oCount     = 0;
	public static $cCount     = 0;
//	public $index     = array();
	public $indexes     = array();
	public $clauseValues     = array();
	public static $index     = array();
//	public static $indexes     = array();
//	public static $clauseValues     = array();

	const PP_OR     = " OR ";
	const PP_AND    = " AND ";

	public static function init( $glue=JPP::PP_AND ) {
		$j = new JPP();
		$j->name .= ++JPP::$oCount;
		$j->glue = $glue;

//		JPP::$index[$j->name] = &$j;
//		JPP::$indexes[ $j->name ] = array();
		JPP::$index[$j->name] = &$j;
		$j->indexes[ $j->name ] = array();

		return $j;
	}

	public static function O( ){
		$filters = func_get_args();
		$j = self::init(JPP::PP_OR);
		$j->filters = $filters;


		return $j;
	}

	public static function A( ){
		$filters = func_get_args();
		$j = self::init(JPP::PP_AND);
		$j->filters = $filters;

		return $j;
	}


	public function add() {
		$filters = func_get_args();
		$this->filters = array_merge($this->filters ,$filters);
		return $this;
	}

	public function resolve() {
		return $this->__resolve(false,$this->filters, $this);
	}

	public function __resolve( $_internal = false, $filters = array(), &$_parent = null ) {
		foreach ( $filters as $f0 => $fv ) {
			if(is_array($fv)){
				$akeys = array_keys( $fv );
				$firstKey = reset( $akeys );
				if ( sizeof( $akeys ) > 1  ) {
					#same array filtering
					foreach ( $fv as $fv0=>$fv1 ) {
						$this->__resolve( true, array(array($fv0=>$fv1)), $_parent );
					}

				}
				elseif(is_numeric( $firstKey )){
					#a regular array
					foreach ( $fv as $fv0=>$fv1 ) {
						$this->__resolve( true, array($fv1), $_parent );
					}
				}
				else{
					$clname = "%JPPCL_" . ( ++JPP::$oCount );

					$_parent->clauseValues[$clname] = $fv;
					$_parent->indexes[$this->name][] = $clname;
				}
			}
			elseif ( is_object( $fv ) && get_class( $fv ) == "JPP" ){
				$_parent->indexes[$this->name][] = $fv->name;
				$fv->__resolve( true, $fv->filters, $_parent );
			}
			else
				die("unsupported filter:".print_r($fv));
		}
		if($_internal !== false)
			return true;

		#final resolve
//		$_parent->indexes = array_reverse( $_parent->indexes, true );
//		print_r( $_parent->indexes );

		return $this;
	}

	public static function indexResolve(JPP &$j,$key=null) {
		$is = $j->indexes;
		$totals = array();

		if(is_null($key)){
			$scls = reset( $is );
			$skey = key( $is );
		}
		else{
			$scls = $is[$key];
			$skey = $key;
		}

		foreach ( $scls as $scl ) {
			if(strpos($scl,"%JPPCL_")!==false){
				$totals[ ] = $scl;
			}
			else
				$totals[ ] = JPP::indexResolve($j,$scl);
		}
		$obj = JPP::$index[ $skey ];
		$totals = "(".implode($obj->glue,$totals).")";
		return $totals;
	}


	public static function arrayHasArrayValues( &$ar ) {
		foreach ( $ar as $a0 ) {
			if ( is_array( $a0 ) ) {
				return true;
			}
		}
		return false;
	}

	public static function is(  &$object ) {
		return is_object( $object ) && get_class( $object ) == "JPP";
	}
}
?>