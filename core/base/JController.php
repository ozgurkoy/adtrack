<?php
/************************************
 *
 *		base controller class
 *		özgür köy
 *
 *		functions may return any other value by
 *			return array( "kontent"=>$kontent );
 *
 ************************************/


class JController
{
	//public $smarty ;
	public $kontent;
	public $set;

	//public function __construct( &$smarty , &$kontent )
	public function __construct( &$kontent=null )
	{
		$this->set = 1;
		$this->kontent =& $kontent;
	}

	public function setGlobal( $name, $val ){
		$_GLOBALS[ $name ] = $val;
	}


	public function __destruct() {

	}

	/*pre - post*/
	public function checkPerms( $type, $action ) {
//		die( '123' );
		//print_r($_SERVER); exit;

		if ( !isset( $_SESSION[ "DS_userId" ] ) ) {
			if (!empty($_SERVER["REQUEST_URI"]) && strlen($_SERVER["REQUEST_URI"]) > 1 && empty($_SERVER["X-Requested-With"]) && empty($_SERVER["HTTP_X_REQUESTED_WITH"]) && empty($_POST)){
				$_SESSION["LAST_DS_URL"] = $_SERVER["REQUEST_URI"];
				echo JT::pfetch( "DS_login" );
				exit;
			} else {
				JUtil::customCScript( "timeoutWarning",null,true );
			}
		} else {
			if (empty($_SERVER["X-Requested-With"]) && empty($_SERVER["HTTP_X_REQUESTED_WITH"])){
				$_SESSION["LAST_DS_URL"] = $_SERVER["REQUEST_URI"];
			}
		}

		#check permissions
		#TODO : store the whole output somewhere(!)
		$perms = array();

		$p = new DSaction();
		$p->nopop()->load( array( "label" => $type ) );

		while ( $p->populate() ) {
			$p->parseTasks();

			$perms[ $p->label ] = $p->parsedTasks;
		}
		if (
		!(
			isset( $perms[ $type ][ $action ] ) &&
			isset( $_SESSION[ "DS_permissions" ][ $type ] ) &&
			$perms[ $type ][ $action ] & $_SESSION[ "DS_permissions" ][ $type ]
		)
		) {

			#echo $perms[ $type ][ $action ] ." ". $_SESSION[ "DS_permissions" ][ $type ];
			#exit;
			$this->logOperation( "Unauthorized access to $action/$type" );
			JUtil::jredirect( "darkside" );
			exit;
		};
	}

	public function logOperation( $op ){
//		return;
		if(isset($_SESSION["DS_userId"]))
			DSuser::record( $_SESSION["DS_userId"], $op);
	}

	public function checkRoot() {
		if ( !isset( $_SESSION[ "DS_userId" ] ) && (!isset($_SESSION[ "DS_isRoot" ]) || $_SESSION[ "DS_isRoot" ]!=1) ){
			$_SESSION[ "adRedirectAfter" ] = substr($_SERVER[ "REQUEST_URI" ],1);

			echo JT::pfetch( "DS_login" );
			exit;
		}
	}

	public static function _assignPostToObject( &$a, $exceptions=array() ) {
		foreach ( $a->_fields as $f0 ) {
			if ( in_array( $f0, array( "id", "jdate" ) ) || !isset( $_POST[ $f0 ] )  || is_array( $_POST[ $f0 ] ) || in_array($f0, $exceptions) )
				continue;

			$a->$f0 = $_POST[ $f0 ];
		}

	}

}
