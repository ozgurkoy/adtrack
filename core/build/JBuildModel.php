<?php
include $__DP . "core/lib/db/".DB_ENGINE."/JBuildModel_base.php";

/**
 * Builds the model and the base model files
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JBuildModel extends JBuildModel_base
{
	/**
	 * site array
	 *
	 * @var array
	 **/
	private $site;

	/**
	 * index array
	 *
	 * @var array
	 **/
	private $index;
	
	/**
	 * relative array to collect.
	 *
	 * @var array
	 **/
	private $relatives=array();

	/**
	 * builder function
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function buildModel($site, $index=null, $dsns)
	{
		//now building model
		$this->debugLog("building model...<BR>");
		
		global $__DP;
		
		//now building static queries
		$this->site  = $site;
		$this->index = $index;

		foreach ($this->site as $f => $inf) {
			$dsnDetails    = $dsns[$f];
			$modelBaseFile = $__DP.JDef::$mdlFolder."base/".$f.".php";
			$modelFile     = $__DP.JDef::$mdlFolder.$f.".php";
			$dsn           = $inf["dsn"];
			
			if($f=="JDoc") continue;

			ob_start();
echo '<?php
/**
 * '.$f.' generated on '.date('d-m-Y H:i',time()).'
 *
 * @package jeelet
 **/

class '.ucfirst($f).'_base extends JModel 
{
';
			$classHeader = ob_get_contents();
			ob_end_clean();

			$classFooter =
"

}
";

			//standart content
			ob_start();

if(sizeof($inf["enums"])>0){
	foreach ($inf["enums"] as $ekey=>$enu) {
echo '
	/**
	 * List of options of '.$ekey.'
	 *
	 **/
	public $'.$ekey.'_options = array(
	';
		foreach ($enu as $ek=>$ev) {
			echo '
			"'.$ev.'"=>"'.$ek.'", ';
		}

			echo '
	);

';
		foreach ($enu as $ek=>$ev) {
echo 
'
	/**
	 * ENUM '.$ev.' of '.$ek.' 
	 *
	 **/
	const ENUM_'.strtoupper($ekey).'_'.strtoupper($ek).' = "'.$ev.'";
';
		}
	}
	
}


echo
'
	/**
	 * definition of the model
	 *
	 * @var string
	 **/
	public $selfname=\''.$f.'\';

	/**
	 * database name
	 *
	 * @var string
	 **/
	public $dbName=\''.$dsns[$f][1]["schema"].'\';
';

echo
'
	/**
	 * dsn of the object
	 *
	 * @var string
	 **/
	public $dsn=\''.$inf["dsn"].'\';
';

if($inf["ordering"]==1){
	echo
'
	/**
	 * order of the current index
	 *
	 * @var string
	 **/
	public $_order=0;
';

}
// add 1-1
$added1s=array();

if(isset($inf["remote"]) && sizeof($inf["remote"]>0)){
		echo '	/**
	 * all remote info
	 * include remote table fields here, because it`s much faster than remote static method. might change in the future though
	 * @var array
	 **/
	public $_remoteConnections = array(';

	self::remoteConnections( $inf );

}

if(sizeof($inf["joins"])>0){
	foreach ($inf["joins"] as $njoin) {
		$added1s[] = $njoin["tag"];

		echo
'
	/**
	 * '.$njoin["targetTable"].', 1-1 Connection object and an id when saving
	 *
	 * @var '.ucfirst($njoin["targetTable"]).'
	 **/
	public $'.$njoin['tag'].';
';
	}
}
// add N-N
if(sizeof($inf["connections"])>0){
	foreach ($inf["connections"] as $njoin) {
		echo
'
	/**
	 * '.$njoin['targetTable'].' '.$njoin['sourceTable'].', N-N Connection object
	 *
	 * @var '.ucfirst($njoin["targetTable"]).'
	 **/
	public $'.($njoin['tag']).';
';
	}
}

// add fields
if(sizeof($inf["fields"])>0){
	foreach ($inf["fields"] as $field) {
		if(in_array($field, $added1s)){
			continue;
		}

		echo
'
	/**
	 * '.$f.' field : '.$field.'
	 *
	 * @var string
	 **/
	public $'.($field).'=null;
';
	}
}
	// add main fields
	// if(sizeof($inf["fields"])>0){
			echo
	'
	/**
	 * fields of the object
	 *
	 * @var array
	 **/
	public $_fields = array(\'id\', \'jdate\', '.(sizeof($inf['fields'])?(implode(', ',JUtil::aposArray($inf['fields']))):'').',);

	/**
	 * defaults of fields
	 *
	 * @var array
	 **/
	public $_fieldDefaults = array(';
	foreach ( $inf["fieldDefaults"] as $_f => $_v ) {
		echo '"'.$_f.'"=>'.(is_null($_v)?'null':'"'.$_v.'"').',';
	}
	echo '
	);';
	// }
// }

// add relatives
if(sizeof($inf["relatives"])>0){
	echo
'
	/**
	 * relatives of the object
	 *
	 * @var array
	 **/
	public $_relatives = array(';
	foreach ($inf["relatives"] as $rtag => $rval) {
		echo "'{$rtag}'=>'{$rval}', ";

	}
	echo');';

}
if(sizeof($inf["ownage"])>0){
	echo
	'
	/**
	 * \'properties\' of the object
	 *
	 * @var array
	 **/
	public $_ownage = array(';
	foreach ($inf["ownage"] as $rown) {
		echo "\"$rown\", ";

	}
	echo');';

}

if(isset($this->index[$f])){
	//index entries
	echo
'

	/**
	 * index finals.
	 *
	 * @var array
	 **/
	public $indexFinal=array(
';

	foreach ($this->index[$f]["final"] as $finale) {
		echo
'		array(
			"class"=>"'.$finale["top"].'",
			"index"=>'.$finale["index"].',
			"indexDef"=>"'.$finale["ifield"].'",
			"indexField"=>"'.$finale["field"].'",
			"indexLabel"=>"'.$finale["indexName"].'",
			"type"=>"'.$finale["type"].'",
		),
';

	}
	echo
'	);
';

	echo
'

	/**
	 * index bridges.
	 *
	 * @var array
	 **/
	public $indexBridge=array(
';

	foreach ($this->index[$f]["bridge"] as $finale) {
		echo
'		array(
			"class"=>"'.$finale["top"].'",
			"action"=>"'.$finale["action"].'",
			"index"=>'.$finale["index"].',
			"indexDef"=>"'.$finale["ifield"].'",
			"indexField"=>"'.$finale["field"].'",
			"indexLabel"=>"'.$finale["indexName"].'",
			"type"=>"'.$finale["type"].'",
		),
';

	}
	echo
'	);
';
}

echo
'

	/**
	 * constructor of the class
	 *
	 * @return JModel
	 **/
	public function __construct($id=null)
	{
		parent::__construct($id);
	}
	
	/**
	 * get dsn of the object
	 *
	 * @author Özgür Köy
	 */
	// get type`s dsn info without initiating
	public static function getDsn()
	{
		return \''.$inf["dsn"].'\';
	}
	
	/**
	 * get sql of the object
	 *
	 * @param object $type
	 * @param array $options
	 * @return string
	 * @author Özgür Köy
	 */
	public function modelQuery($type, $options=null){
		if(is_null($options))
			$options=array();

		$deleteAll		= isset($options["deleteAll"])&&$options["deleteAll"]==true;

		$myQueries = array(
			"save"=>"';
				if($inf["ordering"] == 1) {
					echo 'INSERT INTO `'.$dsns[$f][1]["schema"].'`.`' . JUtil::tableName($f).'`'
				. '(`jdate``jorder`'
				. (count($inf['fields']) > 0 ? ',' . implode(',', JUtil::aposArray($inf['fields'],"`")) : '')
				. ') SELECT '.JPDO::$unix_timestamp.',(SELECT max(`jorder`)+1 FROM  '
				. '`'.JUtil::tableName($f).'`'
				. '),'
				. (count($inf['fields']) > 0 ? ',' . JUtil::multistr('?', sizeof($inf['fields'])) : '');
				} else {
					echo '
				INSERT INTO `'.$dsns[$f][1]["schema"].'`.`'.JUtil::tableName($f).'`(`jdate`'
				. (sizeof($inf['fields']) > 0 ? ',' . implode(',', JUtil::aposArray($inf['fields'],'`')) : '')
				. ') VALUES ('.JPDO::$unix_timestamp.''
				. (count($inf['fields']) > 0 ? str_repeat(',?', count($inf['fields'])) : '')
				. ')",';

				}
				echo '
			"save_id"=>"INSERT INTO `'.$dsns[$f][1]["schema"].'`.`' . (JUtil::tableName($f)) . '`(`id`,`jdate`'
				. (sizeof($inf['fields']) > 0 ? ',' . implode(',', JUtil::aposArray($inf['fields'],'`')) : '')
				. ') VALUES (?,'.JPDO::$unix_timestamp.''
				. (count($inf['fields']) > 0 ? str_repeat(',?', count($inf['fields'])) : '')
				. ')",
			"delete"=>"DELETE FROM `'.$dsns[$f][1]["schema"].'`.`'.JUtil::tableName($f).'` WHERE id=?",
				
			"deleteMulti"=>"DELETE FROM `'.$dsns[$f][1]["schema"].'`.`'.JUtil::tableName($f).'` WHERE id ",//" IN(%s)",
				
			"edit"=>"UPDATE `'.$dsns[$f][1]["schema"].'`.`'.JUtil::tableName($f).'` SET ';
				$t0 = array();
				foreach ($inf['fields'] as $mmain) {
					$t0[] = '`'.$mmain."`=?";
				}
				echo implode(",", $t0) . ' WHERE id=?",'.PHP_EOL;
				
				/*
					1-1S
				*/
				if(sizeof($inf["joins"])>0){

					//save query
					foreach ($inf["joins"] as $nj) {
						if($nj["type"]=="1to1"){
							echo '
			"save_'.$nj["tag"].'"=>"UPDATE `'.$dsns[$f][1]["schema"].'`.`'.$nj["table"].'` SET `'.$nj["tag"].'`=? WHERE `id`=?",'.PHP_EOL;

			if(in_array($nj["tag"],$inf["ownage"]))
							echo '
			"delete_'.$nj["tag"].'"=>"DELETE FROM `'.$dsns[$f][1]["schema"].'`.`'.$nj["table"].'` WHERE `id`=?",'.PHP_EOL;
			else
							echo '
			"delete_'.$nj["tag"].'"=>"UPDATE `'.$dsns[$f][1]["schema"].'`.`'.$nj["table"].'` SET `'.$nj["tag"].'`=NULL WHERE `id`=?",'.PHP_EOL;
						}
						elseif($nj["type"]=="1toN"){
							echo '
			"save_'.$nj["tag"].'"=>"UPDATE `'.$dsns[$f][1]["schema"].'`.`'.$nj["targetTable"].'` SET `'.$nj["tag"].'`=? WHERE id=?",'.PHP_EOL;
			if(in_array($nj["tag"],$inf["ownage"]))
							echo '
			"deleteAll_'.$nj["tag"].'" => "DELETE FROM `'.$dsns[$f][1]["schema"].'`.`'.$nj["targetTable"].'` WHERE `'.$nj["tag"].'`=?",'.PHP_EOL;
			else
							echo '
			"deleteAll_'.$nj["tag"].'" => "UPDATE `'.$dsns[$f][1]["schema"].'`.`'.$nj["targetTable"].'` SET `'.$nj["tag"].'`=NULL WHERE `'.$nj["tag"].'`=?",'.PHP_EOL;
			if(in_array($nj["tag"],$inf["ownage"]))
							echo '
			"delete_'.$nj["tag"].'" => "DELETE FROM `'.$dsns[$f][1]["schema"].'`.`'.$nj["targetTable"].'` WHERE `'.$nj["tag"].'`=?  AND id ",'.PHP_EOL;
			else
							echo '
			"delete_'.$nj["tag"].'" => "UPDATE `'.$dsns[$f][1]["schema"].'`.`'.$nj["targetTable"].'` SET `'.$nj["tag"].'`=NULL WHERE `'.$nj["tag"].'`=?  AND id ",'.PHP_EOL;
						}
					}
				}
				
				/*
					N-NS
				*/
				if(sizeof($inf["connections"])>0){
					foreach ($inf["connections"] as $nr) {
						echo
			'
			"save_'.$nr["tag"].'"=>';
						if($nr["ordering"]==1){
							echo
			'"INSERT INTO `'.$dsns[$f][1]["schema"].'`.`'.$nr["joinTable"].'`(`'.$nr["sourceTable"].'`,`'.$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"").'`'
			.',`tag``jorder`,`jdate`,`uniqueID`
				SELECT ?,?,"'.$nr["tag"].'",(SELECT max(`jorder`)+1 FROM  `'.$dsns[$f][1]["schema"].'`.`'.$nr["joinTable"].'`),'.JPDO::$unix_timestamp.', ?",
			';
						} 
						else {
							echo
			'
			"INSERT INTO `'.$dsns[$f][1]["schema"].'`.`'.$nr["joinTable"].'`(`'.$nr["sourceTable"].'`,`'.$nr["targetTable"].($nr["sourceTable"]==$nr["targetTable"]?"_":"").'`'
			.',`tag`,`jdate`,`uniqueID`)
			SELECT ?,?,\''.$nr["tag"].'\','.JPDO::$unix_timestamp.', ?
			WHERE NOT EXISTS (SELECT `uniqueID` FROM `'.$dsns[$f][1]["schema"].'`.`'.$nr["joinTable"].'` WHERE `uniqueID`=?)",'.PHP_EOL;
//			VALUES(?,?,\''.$nr["tag"].'\','.JPDO::$unix_timestamp.', ?)",'.PHP_EOL;
						}
					
						echo '
			"delete_'.$nr["tag"].'"=>"DELETE FROM `'.$dsns[$f][1]["schema"].'`.`'.$nr["joinTable"].'`'
							.' WHERE `tag`=\''.$nr["tag"].'\'
							AND `'.$nr[isset($nr["reverse"])?"targetTable":"sourceTable"].'`=?".($deleteAll?"":" AND `'.$nr[isset($nr["reverse"])?"sourceTable":"targetTable"].($nr["sourceTable"]==$nr["targetTable"]?'_':'').'`=?"),';
					}
				}
				echo'
		);
	
		return $myQueries[$type];
	}

	';
	/*
		1-1
	*/
	if(sizeof($inf["joins"])>0){
		
		foreach ($inf["joins"] as $njoin) {
			
			/*
			
			Previous one, used to use tags.
			
			$isReversed = isset($njoin["reverse"]);
			$methodPrefix = $isReversed ? (ucfirst($njoin["targetTable"])."of") : "";
			echo
'
			public function load'.$methodPrefix.ucfirst($njoin["tag"]).'(){
				
			*/
			//check for reverse loading, the case of being owned
			$isReversed                     = isset($njoin["reverse"]) && $njoin["reverse"]==true;
			$is1Reversed                    = isset($njoin["11Reverse"]) && $njoin["11Reverse"]==true;
			$methodName                     = $isReversed || $is1Reversed ? (ucfirst($njoin["targetTable"])) : ucfirst($njoin["tag"]);
			$this->relatives[$njoin["tag"]] = 'load'.$methodName;
			
			echo
'
	/**
	 * load '.$njoin["targetTable"].'
	 *
	 * @return '.$njoin["targetTable"].'
	 * @author Özgür Köy
	 */
	public function load'.$methodName.'('.($njoin["type"]=="1to1"?"":($njoin["targetTable"]!="JDoc"?'$id=null, $start=0, $count=50, $orderBy=null, $populateOnce=true':'')).'){
		';
		if($njoin["targetTable"]!="JDoc"){
			if($njoin["type"]=="1to1"){
				echo
				'
			return JMT::loadRemote($this, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'", '.($isReversed?'true':'false').');
				';
			}
			else{
				$targetClass = ucfirst($njoin["targetTable"]);
				echo
				'
		if(!($this->id>0))
			return false;

		if(is_numeric($id) && !is_null($id))
			$id = array("id"=>$id);

		$t0 = new '.$targetClass.'();
		if(!is_null($id))
			$id["'.$njoin["tag"].'"] = $this->id;
		else
			$id = array("'.$njoin["tag"].'"=>$this->id);

		$this->'.$njoin["tag"].' = $t0->populateOnce($populateOnce)->orderBy($orderBy)->limit($start,$count)->load($id);
		unset($t0);
		return $this->'.$njoin["tag"].';
				';
			}
		}
		else{
			echo
			'
		$this->'.$njoin["tag"].' = JDoc::load("'.$f.'", $this->id, "'.$njoin["tag"].'");
		return $this;
			';
		}
		echo
		'
	}';

	if($njoin["type"]!="1to1"){
		echo
			'
	/**
	 * count '.$njoin["targetTable"].'
	 *
	 * @return int
	 * @author Özgür Köy
	 */
	public function count'.$methodName.'('.($njoin["targetTable"]!="JDoc"?'$id=null':'').'){
		';
		if($njoin["targetTable"]!="JDoc"){

		$targetClass = ucfirst($njoin["targetTable"]);
		echo
			'
		if(!($this->id>0))
			return false;

		if(is_numeric($id) && !is_null($id))
			$id = array("id"=>$id);

		$t0 = new '.$targetClass.'();
		if(!is_null($id))
			$id["'.$njoin["tag"].'"] = $this->id;
		else
			$id = array("'.$njoin["tag"].'"=>$this->id);

		$t0->count("id", "__cid", $id);
		$cid = isset($t0->__cid)?$t0->__cid:0;
		unset($t0);
		return $cid;


				';
		}
		else{
			#TODO
			echo
				'
			$this->'.$njoin["tag"].' = JDoc::load("'.$f.'", $this->id, "'.$njoin["tag"].'");
		return $this;
			';
		}
		echo
		'
	}';

	}

	echo '
	/**
	 * save '.$njoin["targetTable"].'
	 *
	 * @param mixed $id 
	 * @return JModel|bool
	 * @author Özgür Köy
	 */
	public function save'.$methodName.'($id=null){
		';
		if($njoin["targetTable"]!="JDoc"){
		echo
		'
		// '.$njoin["type"].'
		return JMT::saveRemote($this, $id, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'", '.( ( ($isReversed||$njoin["type"]=="1toN") && !isset($njoin["1NReverse"]) && $njoin["targetTable"]!=$f  )?'true':'false').');
		';
		}
		else{
		echo
		'
		if(is_null($id) || !isset($id["name"]) || (isset($id["name"]) && (is_array($id["name"])&&strlen($id["name"][0])<2)||(!is_array($id["name"])&&strlen($id["name"])<2))  ) return;

		//1-1, so delete previous :)
		$this->delete'.ucfirst($njoin["tag"]).'();
		JDoc::add("'.$f.'",$this->id,$id,"'.$njoin["tag"].'");
		$this->checkIndex("'.$njoin["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	';
	if($njoin["type"]=="1toN"){
	echo 
	'
	/**
	 * delete all '.$njoin["targetTable"].'
	 *
	 * @author Özgür Köy
	 */	
	public function deleteAll'.$methodName.'(){
		';
		// $njoin["type"]!="1toN"
		if($njoin["targetTable"]!="JDoc"){
			if($isReversed){
				echo
		'
		if(!(is_object($this->'.$njoin["tag"].') || strlen($this->'.$njoin["tag"].')>0))
			JMT::loadRemote($this, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'",'.($isReversed?'true':'false').');';
			}
		echo
		'
		return JMT::deleteOne($this, "'.$njoin["tag"].'", '.((($isReversed||$njoin["type"]=="1toN")&&!isset($njoin["1NReverse"]))?'true':'false').', "all");
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$njoin["tag"].'");
		$this->checkIndex("'.$njoin["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	';
	}
	echo 
	'
	/**
	 * delete '.$njoin["targetTable"].' 
	 *
	 * @author Özgür Köy
	 */
	public function delete'.$methodName.'('.($njoin["type"]=="1toN"?'$id=null':'').'){

		';
		// $njoin["type"]!="1toN"
		if($njoin["targetTable"]!="JDoc"){
			if($isReversed){
				echo
		'
		if(!(is_object($this->'.$njoin["tag"].') || strlen($this->'.$njoin["tag"].')>0))
			JMT::loadRemote($this, "'.$njoin["targetTable"].'", "'.$njoin["tag"].'",'.($isReversed?'true':'false').');';
			}
		echo
		'
		return JMT::deleteOne($this, "'.$njoin["tag"].'", '.((($isReversed||$njoin["type"]=="1toN")&&!isset($njoin["1NReverse"])&& $njoin["targetTable"]!=$f )?'true':'false').' '.($njoin["type"]=="1toN"?', $id, true':'').');
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$njoin["tag"].'");
		$this->checkIndex("'.$njoin["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
';
		if( $njoin["type"]!="1toN" ){
	
		}
	}
}
/*
	N-NS
*/
	if(+sizeof($inf["connections"])>0){
		foreach ($inf["connections"] as $nr) {
			$isReversed                     = isset($nr["reverse"]);
			$methodPrefix                   = $isReversed ? (ucfirst($nr["sourceTable"])."of") : "";
			$targetClass                    = ucfirst($nr[$isReversed ? "sourceTable": "targetTable"]);
			$this->relatives[$nr["tag"]] 	= 'load'.$methodPrefix.ucfirst($nr["tag"]);
			
		echo
		'
	/**
	 * load n-n connection
	 *
	 * @param mixed $id 
	 * @param number $start 
	 * @param number $count
	 * @param string $orderBy
	 * @return '.$targetClass.'
	 * @author Özgür Köy
	 */
	public function load'.$methodPrefix.ucfirst($nr["tag"]).'('.($nr["targetTable"]!="JDoc"?'$id=null, $start=0, $count=50, $orderBy=null, $populateOnce=true':'').'){
		';
		if($nr["targetTable"]!="JDoc"){
		echo
		'
		if(!($this->id>0)) return false;

		$t0 = new '.$targetClass.'();
		$clause = array( "'.$nr["tag"].'" => array( "id" => $this->id ) );
		if(!is_null($id)){
			if(is_int($id))
				$clause[ "id" ] = $id;
			elseif(is_array($id)){
				foreach ( $id as $ik => $iv ) {
						$clause[ $ik ] = $iv;
					//if(in_array($ik,$t0->_fields))
					//elseif(isset($t0->_relatives[$ik]))
					//	$clause[ "'.$nr["tag"].'" ][ $ik ] = $iv;

				}
				unset($ik, $iv);

			}
		}


		$t0 = $this->'.$nr["tag"].' = $t0->populateOnce($populateOnce)->orderBy($orderBy)->limit($start,$count)->load($clause);
		unset($t0);
		return $this->'.$nr["tag"].';
		';
		}
		else{
		echo
		'
		$this->'.$nr["tag"].' = JDoc::load("'.$f.'",$this->id,"'.$nr["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	
	/**
	 * count the object
	 *
	 * @param object $type 
	 * @param array $options 
	 * @return string
	 * @author Özgür Köy
	 */
	public function count'.$methodPrefix.ucfirst($nr["tag"]).'( $options=null ){
		';
		if($nr["targetTable"]!="JDoc"){
			echo
		'
		if(!($this->id>0)) return false;

		$t0 = new '.$targetClass.'();
		$clause = array( "'.$nr["tag"].'" => array( "id" => $this->id ) );
		if(!is_null($options)){
			if(is_int($options))
				$clause[ "id" ] = $id;
			elseif(is_array($options)){
				foreach ( $options as $ik => $iv ) {
					$clause[ $ik ] = $iv;

					//if(in_array($ik,$t0->_fields))
					//	$clause[ "'.$nr["tag"].'" ][ $ik ] = $iv;
					//elseif(isset($t0->_relatives[$ik]))
					//	$clause[ $ik ] = $iv;

				}
				unset($ik, $iv);

			}
		}

		$t0 = new '.$targetClass.'();
		$t0->count("id", "__cid", $clause);
		$cid = isset($t0->__cid)?$t0->__cid:0;
		unset($t0);
		return $cid;
		';
			}
			else{
				#TODO BELOW
				echo
					'
					$this->'.$nr["tag"].' = JDoc::load("'.$f.'",$this->id,"'.$nr["tag"].'");
		return $this;
		';
			}
			echo
				'

	}

	/**
	 * save '.$njoin["targetTable"].'
	 *
	 * @param mixed $id 
	 * @return JModel|bool
	 * @author Özgür Köy
	 */
	public function save'.$methodPrefix.ucfirst($nr["tag"]).'($id=null){
		';
		if($nr["targetTable"]!="JDoc"){
			echo
		'
		return JMT::saveMany($this, $id, "'.$nr[$isReversed?"sourceTable":"targetTable"].'", "'.$nr["tag"].'", '.($isReversed?'true':'false').');
		';
		}
		else{
			echo
		'
		JDoc::add("'.$f.'",$this->id,$id,"'.$nr["tag"].'",'.($nr["ordering"]==1?"true":"false").');
		$this->checkIndex("'.$nr["tag"].'");
		return $this;
		';
		}
			echo
		'
	}

	/**
	 * delete '.$njoin["targetTable"].'
	 *
	 * @param mixed $id 
	 * @return bool
	 * @author Özgür Köy
	 */
	public function delete'.$methodPrefix.ucfirst($nr["tag"]).'($id){
		if(!isset($id) || is_null($id)) return false;
		';
		if($nr["targetTable"]!="JDoc"){
		echo
		'
		return JMT::deleteMany($this, "'.$nr["tag"].'", $id, '.($isReversed?'true':'false').');
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$nr["tag"].'", $id);
		$this->checkIndex("'.$nr["tag"].'");
		return $this;
		';
		}
		echo
		'
	}
	
	/**
	 * delete all '.$njoin["targetTable"].'
	 *
	 * @return bool
	 * @author Özgür Köy
	 */
	public function deleteAll'.$methodPrefix.ucfirst($nr["tag"]).'(){
		';
		if($nr["targetTable"]!="JDoc"){
		echo
		'
		return JMT::deleteMany($this, "'.$nr["tag"].'", null, '.($isReversed?'true':'false').');
		';
		}
		else{
		echo
		'
		JDoc::clear("'.$f.'",$this->id,"'.$nr["tag"].'");
		$this->checkIndex("'.$nr["tag"].'");
		return $this;
		';
		}
		echo
	//clause , docount, groupby, orderby, limit
			'
	}

';
			}
		}

	$classBody = ob_get_contents();
	ob_end_clean();


	if(!is_writable($__DP.JDef::$mdlFolder."base/")){
		JUtil::configProblem("can't write to model dir:".$modelBaseFile);
	}

	if(is_file($modelBaseFile) && !is_writable($modelBaseFile)){
		JUtil::configProblem("can't write to model file:".$modelBaseFile);
	}

	$fp = fopen($modelBaseFile, 'w+');
	
	if(sizeof($this->relatives)>0){
		$tf = 
	'
	/**
	 * relative methods 
	 *
	 * @var array
	 **/
	public $relativeMethods = array(';
	foreach ($this->relatives as $rtag => $rval) {
		$tf.="'{$rtag}'=>'{$rval}', ";

	}
	$tf .=');
	';
		
		$classFooter = $tf.$classFooter;
	}
	
	fwrite($fp, $classHeader.$classBody.$classFooter);
	fclose($fp);

	//chmod($modelBaseFile, 0777);
	if(!is_file($modelFile)){
		//extender file
		$ex =
	'<?php
/**
 * '.$f.' real class - firstly generated on '.date('d-m-Y H:i',time()).', add edit anyway you like wont be touched over , ever again.
 *
 * @package jeelet
 **/
include $__DP.\'/site/model/base/'.($f).'.php\';

class '.ucfirst($f).' extends '.ucfirst($f).'_base
{
	/**
	 * constructor for the class
	 *
	 * @return void
	 **/
	public function __construct($id=null) {
		parent::__construct($id);
	}

}

';
				if(!is_writable($__DP.JDef::$mdlFolder)){
					JUtil::configProblem("can't write to model dir:".$modelFile);
				}

				if(is_file($modelFile) && !is_writable($modelFile)){
					JUtil::configProblem("can't write to query file:".$modelFile);

				}

				$fp = fopen($modelFile, 'w+');
				fwrite($fp, $ex);
				fclose($fp);

				chmod($modelFile, 0777);

		}

		}

	}
	
	
	/**
	 * debug to screen
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function debugLog($log)
	{
		echo date("Y.m.d H:i:s") . " >>> " . $log . PHP_EOL;
	}
	
}