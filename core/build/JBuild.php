<?php

include $__DP . "core/lib/db/".DB_ENGINE."/buildDef.php";

/**
 * Main Building function which inst. other sub classes and complete the process
 *
 * @package jeelet
 * @author  Özgür Köy
 **/
class JBuild extends BuildDef
{

	private $restrictedZones = array( "default","_routings" );
	/**
	 * siteArray holds the structure of the site, read&filled from a config file
	 *

	 * @var string
	 **/
	private $siteArray;

	/**
	 * index array
	 *
	 * @var string
	 **/
	public $indexArray = array();

	/**
	 * @var array
	 * export types
	 */
	public $exportTypes = array( "flush", "append", "insertOnly", "updateOnly", "put" );


	/**
	 * dsns
	 *
	 * @var string
	 **/
	private $dsnArray;

	/**
	 * constructor
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function __construct() {

	}

	/**
	 * main builder function_exists
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function buildMain( $doindex = false ) {
		global $__DP;
		ini_set( "max_execution_time", 3600 );
		echo "<PRE>";

		$this->readYamls();

		//build site
		$this->checkNecessaryTables();

		JLog::startEvent( "Building Site" );

		$toRunConnections = array();
		$allFields        = array();
		$enums            = array();
		$allIndexes       = array();
		$altersToRun      = array();
		$indexToRun       = array();
		$fieldsToRemove   = array();
		$tableFields      = array();
		$dsns             = array();

		// check structure
		$this->validateYaml();
		// exit;
		foreach ( $this->siteArray as $table => $tableConf ) {
			if ( !isset( $tableFields[ $table ] ) )
				$tableFields[ $table ] = array();

			$tableName      = JUtil::tableName( $table );
			$tableDSN       = $this->getDsn( $table );
			$dsns[ $table ] = array( $tableDSN, $this->getDSNDetails( $tableDSN ) );
			$dbName         = $dsns[ $table ][ 1 ][ "db" ];
			$schema         = $dsns[ $table ][ 1 ][ "schema" ];
			$ordering       = false;

			if ( !isset( $allFields[ $table ] ) )
				$allFields[ $table ] = array( "id" );
			else
				$allFields[ $table ][ ] = "id";

			JPDO::connect( JCache::read( 'JDSN.' . $tableDSN ) );
			$this->runPostDBCommand( $schema );

			$tableExists = JPDO::tableExists( $tableName, $dbName, $schema );

			//create table if not exists
			if ( !$tableExists ) {
				$dbQuery = $this->sql( "createDB", $schema );
				JPDO::executeQuery( $dbQuery);

				$this->runPostDBCommand( $schema );


				$tableQuery = $this->sql( 'createTable', $schema, $tableName );

				JPDO::executeQuery( $tableQuery );
				JLog::log( "build", "created table $tableName", 1 );
				$this->debugLog( "created table $tableName" );

				$prvFields = array();
				$indexes   = array();
			}
			else {
				$indexes = $this->getTableIndexes( $tableDSN, $table );

//				if($table=="word"){
//					echo "@@@$table@@".PHP_EOL; print_r( $indexes );}
				JPDO::connect( JCache::read( 'JDSN.' . $tableDSN ) );
				$prvFields = JPDO::listFields( $tableName, $schema );
//				echo $tableName . PHP_EOL;
//				print_r( $prvFields );
			}

			$allIndexes[ $tableName ] = $indexes;

			//check for ordering
			if ( isset( $tableConf[ "ordering" ] ) ) {
				$ordering = true;
				unset( $tableConf[ "ordering" ] );
			}

			$this->createOnSiteArray( $table, $tableName );

			//extra features
			$this->site[ $table ][ "ordering" ] = $ordering ? 1 : 0;
			$this->site[ $table ][ "self" ]     = $tableName;
			$this->site[ $table ][ "dsn" ]      = isset( $this->dsnArray[ $table ] ) && array_key_exists( $table, $this->dsnArray ) ? $this->dsnArray[ $table ] : "MDB";


			foreach ( $tableConf as $field ) {
				$fieldSql     = null;
				$fieldType    = isset( $this->fieldTypes[ $field[ "type" ] ] ) ? $this->fieldTypes[ $field[ "type" ] ] : null;
				$fieldDetails = array();
				$defaultValue = isset( $field[ "default" ] ) ? $field[ "default" ] : null;
				$isPrepared   = $this->prepareField( $field, $table, $toRunConnections, $fieldType, $fieldDetails, $defaultValue );

				if ( $isPrepared ) {
					if ( $field[ "type" ] == "1toN" || ( $field[ "type" ] == "1to1" && isset( $field[ "owns" ] ) ) ) {
						//prepare field changes..
						$remoteDsn = $this->getDsn( $field[ "chain" ] );

						JPDO::connect( JCache::read( 'JDSN.' . $remoteDsn ) );
						JPDO::$hideError = true;

						$eprvFields      = JPDO::listFields( $field[ "chain" ], $schema );

						JPDO::$hideError = false;
						JPDO::revertDsn();

						if ( !is_array( $eprvFields ) || !array_key_exists( $field[ "tag" ], $eprvFields ) ) { // new field
//							echo $field[ "chain" ]. $field[ "tag" ]. "<1" . PHP_EOL;

							$altersToRun[ ] = array( "addField", $schema, $field[ "chain" ], $field[ "tag" ], $fieldType, "", "" );
						}
						elseif ( strtolower( preg_replace( '|\(.+?\)|i', '', $eprvFields[ $field[ 'tag' ] ] ) ) != strtolower( preg_replace( '|\(.+?\)|i', '', $fieldType ) ) ) { // field type changed
//							echo $field[ 'tag' ]."--". $eprvFields[ $field[ 'tag' ]]."!=".strtolower( $fieldType )  . "<<<<" . PHP_EOL;
							$altersToRun[ ] = array( "editField", $schema, $field[ "chain" ], $field[ "tag" ], $field[ "tag" ], $fieldType, "", "" );
						}

						// echo PHP_EOL.$field["tag"]."==".$field["chain"].PHP_EOL;

						if ( !in_array( $field[ "chain" ], $tableFields[ $table ] ) )
							$tableFields[ $field[ "chain" ] ][ ] = $field[ "tag" ];

						if ( !in_array( $field[ "chain" ], $allFields[ $table ] ) )
							$allFields[ $field[ "chain" ] ][ ] = $field[ "tag" ];

						$altTableName = JUtil::tableName( $field[ "chain" ] );
						$this->createOnSiteArray( $field[ "chain" ] );

						$this->site[ $field[ "chain" ] ][ "fields" ][ ] = $field[ "tag" ];

						continue;
					}

					if ( !in_array( $field[ "tag" ], $tableFields[ $table ] ) )
						$tableFields[ $table ][ ] = $field[ "tag" ];
					if ( !in_array( $field[ "tag" ], $allFields[ $table ] ) )
						$allFields[ $table ][ ] = $field[ "tag" ];

					if ( sizeof( $fieldDetails ) > 0 ) {
						$this->site[ $table ][ "enums" ][ $field[ "tag" ] ] = $fieldDetails;
					}

					//prepare field changes..
					$collSql   = ( in_array( $fieldType, $this->noCollationFieldTypes ) === false ) ? ( $this->sql( "collation", $schema ) ) : "";
					$defValSql = is_null( $defaultValue ) ? "" : "DEFAULT '$defaultValue'";


					//check for new-previous field situation
					if ( !is_array( $prvFields ) || !array_key_exists( $field[ "tag" ], $prvFields ) ) { // new field
//						echo $field[ 'tag' ]."--". $prvFields[ $field[ 'tag' ]]."!=".strtolower( $fieldType )  . "@@@@" . PHP_EOL;

						$altersToRun[ ] = array( "addField", $schema, $tableName, $field[ "tag" ], $fieldType, $collSql, $defValSql );
					}
					elseif (
						$prvFields[ $field[ 'tag' ] ] != "USER-DEFINED" &&
						strtolower( $prvFields[ $field[ 'tag' ] ] ) != strtolower( $fieldType )
					) { // field type changed
//						echo $field[ 'tag' ]."--". $prvFields[ $field[ 'tag' ]]."!=".strtolower( $fieldType )  . "<<<<" . PHP_EOL;

						$altersToRun[ ] = array( "editField", $schema, $tableName, $field[ "tag" ], $field[ "tag" ], $fieldType, $collSql, $defValSql );
					}
					else {
						// get default value
						$tableDSN = $this->getDsn( $tableName );
						JPDO::connect( JCache::read( 'JDSN.' . $tableDSN ) );
						$defSql = $this->sql( "getFieldDefault", $field[ "tag" ], $tableName );

						$def0 = JPDO::executeQuery( $defSql );
						if ( str_replace( "'", "", $defaultValue ) != preg_replace( '|::.+|i', '', preg_replace( "|['\(\)]|", "", $def0[ 0 ][ "default" ] )) ) {
//							echo str_replace( "'", "", $defaultValue ) ." != ". preg_replace( '|::.+|i', '', preg_replace( "|['\(\)]|", "", $def0[ 0 ][ "default" ] ) ).PHP_EOL;
							if ( strlen( $defaultValue ) > 0 ) {
//								echo $field[ 'tag' ]."--". $prvFields[ $field[ 'tag' ]]."!=".strtolower( $fieldType )  . "|||||" . PHP_EOL;
								$altersToRun[ ] = array( "editField", $schema, $tableName, $field[ "tag" ], $field[ "tag" ], $fieldType, $collSql, $defValSql );
							}
							elseif ( is_null( $defaultValue ) && strlen( $def0[ 0 ][ "default" ] ) > 0 ) { // this one won't be used very often
//								echo $field[ 'tag' ]."--". $prvFields[ $field[ 'tag' ]]."!=".strtolower( $fieldType )  . "^^^^" . PHP_EOL;
								$altersToRun[ ] = array( "editField", $schema, $tableName, $field[ "tag" ], $field[ "tag" ], $fieldType, $collSql, "DEFAULT NULL" );
							}

						}
					}
					//set, to add to unique index.
					$indexToRun[ ] = array( $field, $table, $toRunConnections, $allIndexes[ $table ] );

					//add to main fields
					$this->site[ $table ][ "main" ][ ]          = $field[ "tag" ];
					$this->site[ $table ][ "fieldDefaults" ][ $field[ "tag" ] ] = $defaultValue;

					if (
						!isset( $field[ "chain" ] ) ||
						( isset( $field[ "chain" ] ) && !isset( $field[ "reverse" ] ) && $field[ "type" ] == "1to1" )
					)
						// if(!in_array($field["tag"], $this->site[$table]["fields"][]))
						$this->site[ $table ][ "fields" ][ ] = $field[ "tag" ];
				}
			}

			//clear previous fields
			$fieldsToRemove[ $table ]  = array( $prvFields, $table, $tableDSN );
			$this->siteArray[ $table ] = $tableConf;
		}
		JPDO::$returnError = true;
		foreach ( $altersToRun as $alt ) {
			$tableDSN = $this->getDsn( $alt[ 1 ] );
			JPDO::connect( JCache::read( 'JDSN.' . $tableDSN ) );

			#pgsql and similar that requires enum to be a seperate data typee
			if (
				( stripos( $alt[ 4 ], "enum" ) !== false || stripos( $alt[ 3 ], "enum" ) !== false ) &&
				( $enumSql = $this->sql( "enumCreation", $alt[1], $alt[3], $alt[ stripos( $alt[ 5 ], "enum" ) === false ? 4 : 5 ] ) ) !== false
			){
//				print_r( $alt );
//				echo $alt[ 4 ] . PHP_EOL;
				if ( JPDO::typeExists( $alt[ 3 ] ) !== true ){
//					echo $enumSql;
					JPDO::executeQuery( $enumSql ); #create type if it does not exist
				}
				else { #try to add the values to existing enum values.
					preg_match_all( "|'(.+?)'|i", $alt[ stripos( $alt[ 5 ], "enum" ) === false ? 4 : 5 ], $enum0 );
					foreach ( $enum0[1] as $enum1 )
						JPDO::executeQuery( $this->sql( "enumAddValue", $schema, $alt[ 3 ], $enum1 ) );

				}
				if($alt[0]=="addField"){
					# no collation and type definition
					$fieldSql = $this->sql(
						$alt[ 0 ],
						$alt[1],
						$alt[ 2 ],
						$alt[ 3 ],
						$alt[1].".".$alt[3],
						"",
						$alt[6]
					);
				}
				else{
					# no collation and type definition
					$fieldSql = $this->sql(
						$alt[ 0 ],
						$alt[1],
						$alt[ 2 ],
						$alt[ 3 ],
						$alt[ 4 ],
						$alt[1].".".$alt[3],
						"",
						$alt[7]
					);
				}
			}
			else
				$fieldSql = $this->sql( $alt[ 0 ], $alt[ 1 ], $alt[ 2 ], $alt[ 3 ], $alt[ 4 ], $alt[ 5 ],$alt[ 6 ], isset( $alt[ 7 ] ) ? $alt[ 7 ] : null );


			// drop index if we have to
			if ( $alt[ 0 ] == "editField"  ) {
				if ( isset( $allIndexes[ $alt[ 1 ] ] ) && isset( $allIndexes[ $alt[ 1 ] ][ $alt[ 2 ] ] ) ) {
					$db0 = $allIndexes[ $alt[ 1 ] ][ $alt[ 2 ] ];
					foreach ( $db0 as $db1 ) {
						if ( isset( $db1[ "gotFK" ] ) ) {
							JPDO::executeQuery( $this->sql( "dropColumnFK", $db1[ "schema" ], JUtil::tableName( $alt[ 1 ] ), $db1[ "label" ] ) );
							break;
						}
					}
				}
				#for more tender dbs(pgsql)
				if ( ( $defaultChangeSql = $this->sql( $alt[ 0 ], $alt[ 1 ], $alt[ 2 ], $alt[ 3 ], $alt[ 4 ], $alt[ 5 ], $alt[ 6 ], isset( $alt[ 7 ] ) ? $alt[ 7 ] : null ) ) !== false )
					JPDO::executeQuery( $defaultChangeSql );

			}
			echo( $fieldSql ).PHP_EOL;

			$e = JPDO::executeQuery( $fieldSql );
			if(is_bool($e)===false){
				echo "<h3>ERROR ON FIELD OP</h3>".$e."<BR>Try removing the column first.";
				exit;
			}
		}
		JPDO::$returnError = false;

//		echo " @@@@  $table" . PHP_EOL;
//		print_r( $indexToRun );
		foreach ( $indexToRun as $itr )
			$this->processFieldIndex( $itr[ 0 ], $itr[ 1 ], $itr[ 2 ], $itr[ 3 ] );

		/*
		// remove prv fields
		foreach ($fieldsToRemove as $remoteTable=>$ftr) 
			$this->removePreviousFields($ftr[0], $ftr[1], $ftr[2], $tableFields[$remoteTable], $allIndexes[$remoteTable]);
		*/

		$classPile = array();


		include "$__DP/core/build/JBuildModel.php";

		//run connections
		echo "<PRE>";
		$this->buildConnections( $toRunConnections, $allFields, $allIndexes, $enums );

		//nice site indexing.
		// $this->buildSiteIndex($classPile);
		$jm = new JBuildModel();
		$jm->buildModel( $this->site, $classPile, $dsns );

		// exit;

		// if($doindex==true)
		// 	JIndex::_rebuildAllIndex( $this->indexArray, array_keys($this->siteArray) );

		$this->debugLog( "done! <br/> <hr />" );
	}

	/**
	 * process field index
	 *
	 * @param string $field
	 * @return bool
	 * @author Özgür Köy
	 */
	public function processFieldIndex( &$field, &$table, &$toRunConnections, $indexes ) {
		$gotIndex = false;
		$fk       = array();
		if ( isset( $indexes[ $field[ "tag" ] ] ) ) {
			//07echo $field[ "tag" ];

			return false;
		}
		$indexes    = isset( $indexes[ $field[ "tag" ] ] ) ? $indexes[ $field[ "tag" ] ] : array();
		$tableDSN   = $this->getDsn( $table );
		$dsnDetails = $this->getDSNDetails( $tableDSN );

		//do not create u. index for 1-1
		if ( isset( $field[ "index" ] ) &&
			in_array( $field[ "index" ], $this->indexes ) &&
			!isset( $toRunConnections[ $table . "." . $field[ "tag" ] ] ) &&
			!in_array( $field[ "tag" ], $indexes )
		) {
//			print_r( $indexes );
			if ( sizeof( $indexes ) > 0 ) {
				foreach ( $indexes as $index ) {
					if ( isset( $index[ "gotIndex" ] ) )
						$gotIndex = true;

					if ( isset( $index[ "isUnique" ] ) && $index[ "isUnique" ] == 1 )
						$gotUnique = true;

					if ( isset( $index[ "gotFK" ] ) )
						continue;
				}
			}
			//if($field[ "index" ] == "index"){
			//	print_r( $indexes );
			//	print_r( $field );}
			//situazion
//			echo $field[ "index" ];

			//temp solution
			if( stripos($field["type"],"char")!==false || stripos($field["type"],"char")!==false )
				$isText = "Text";
			else
				$isText = "";

			if ( $field[ "index" ] == "unique" && !isset( $gotUnique ) ) { //< do not add unique to already active unique index. but you can add unique to non unique indexed fields
				JPDO::$hideError=true;

				$sql = $this->sql( "uniqueIndex".$isText,
					$dsnDetails[ "schema" ],
					$table,
					$table.$field[ "tag" ],
					$field[ "tag" ]
				);
				JPDO::$hideError=false;
			}
			elseif ( $field[ "index" ] == "index" && $gotIndex!==true ) {
				JPDO::$hideError=true; #temporary
				$sql = $this->sql( "addIndex".$isText,
					$dsnDetails[ "schema" ],
					$table,
					$table.$field[ "tag" ],
					$field[ "tag" ]
				);
				echo PHP_EOL;
				JPDO::$hideError=false;

			}
			else
				return false;


			$tableDSN = $this->getDsn( $table );
			// print_r($tableDSN);
			JPDO::connect( JCache::read( 'JDSN.' . $tableDSN ) );
			JPDO::executeQuery( $sql );


			return true;
		}

		$ret = array();

		if ( isset( $this->indexes[ $field[ "tag" ] ] ) ) {

		}

		return null;
	}

	/**
	 * validate site.yaml
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function validateYaml() {
		$oppositeConnections = array();
		$owns                = array();
		$owning              = array();
		$remoteConn          = array();
		$conns               = array();
		$gotError            = false;

		foreach ( $this->siteArray as $table => $tableConf ) {
			if ( in_array( strtoupper( $table ), $this->reservedKeywords ) === true ) {
				JLog::log( 'cri', 'reserved field used :' . $table, 0 );
				echo '<br />table structure is wrong: reserved keyword used as object name : ' . $table;
				die();
			}
			if(!is_array($tableConf)){
				echo "PROBLEM WITH YAML";
				var_dump( $tableConf );
				exit;
			}
			foreach ( $tableConf as $t0 => $field ) {
				if ( is_array( $field ) ) {
					if ( !array_key_exists( 'tag', $field ) ) {
//						print_r( $t0 );
						JLog::log( 'cri', 'Wrong site YAML structure; tag of table :' . $table, 0 );
						echo '<br />' . $table . ' structure is wrong: cannot read tag';
						die();
					}

					if ( in_array( strtoupper( $field[ "tag" ] ), $this->reservedKeywords ) === true ) {
						JLog::log( 'cri', 'reserved field used :' . $field[ "tag" ], 0 );
						echo '<br />' . $table . ' structure is wrong: reserved keyword used : ' . $field[ "tag" ];
						die();
					}

					if ( isset( $field[ "owns" ] ) && strpos( $field[ "type" ], "has lots" ) !== false ) {
						JLog::log( 'cri', 'Wrong site YAML structure; owns runs for 1-1 and 1-N only (' . $field[ "tag" ] . ') :' . $table . "-" . $field[ "type" ], 0 );
						echo '<br />' . $table . 'Wrong site YAML structure; owns runs for 1-1 and 1-N only (' . $field[ "tag" ] . ') :' . $field[ "type" ];
						die();
					}

					if ( !array_key_exists( 'type', $field ) ) {
						JLog::log( 'cri', 'Wrong site YAML structure; type of table :' . $table, 0 );
						echo '<br />' . $table . ' structure is wrong: cannot read type of ' . $field[ 'tag' ];
						die();
					}

					if ( in_array( strtolower( $field[ "tag" ] ), $this->reservedFields ) === true ) {
						JLog::log( 'cri', 'reserved field used :' . $table, 0 );
						print_r( $field[ "tag" ] );
						echo '<br />' . $table . ' structure is wrong: reserved field used : ' . $field[ 'tag' ];
						die();
					}

					if ( !isset( $field[ "type" ] ) ) {
						echo "field type missing {$tableName} {$field["tag"]} <br />";
						JLog::log( "cri", "field type missing {$tableName} {$field["tag"]} ", 1 );
						die();
					}

					if ( $field[ "tag" ] == $table ) {
						echo "field tag({$field["tag"]}) can't be the same with the table <br />";
						JLog::log( "cri", "field tag({$field["tag"]}) can't be the same with the table", 1 );
						die();
					}

					if ( !is_string( $field[ "type" ] ) ) {
						echo( "wrong configuration(check yaml file):field type problem ({$table}.{$field["tag"]})" );
						exit;
					}

//					if ( $field[ "type" ] == "text" && isset( $field[ "index" ] ) ) {
//						echo( "wrong configuration(check yaml file):can't add index to text field({$table}.{$field["tag"]}, add it manually(index length and all).)" );
//						exit;
//					}

					if ( $field[ "type" ] == "text" && isset( $field[ "default" ] ) ) {
						echo( "wrong configuration(check yaml file):text fields can't have default values.({$table}.{$field["tag"]}))" );
						exit;
					}

					if ( in_array( $field[ "type" ], array( "select", "enum" ) ) && !array_key_exists( "options", $field ) ) {
						echo( "wrong configuration(check yaml file): must add `options` to select and enum. ({$table}.{$field["tag"]})" );
						exit;
					}

					if ( in_array( $field[ "type" ], array( "int", "float", "bigint", "tinyint1" ) ) && isset( $field[ "default" ] ) && !is_numeric( $field[ "default" ] ) ) {
						echo( "wrong configuration(check yaml file): integer ,floats and boolean values can have integer only default values. ({$table}.{$field["tag"]})" );
						exit;
					}

					if ( $field[ "type" ] == "enum" ) {
						if ( isset( $field[ "default" ] ) && !in_array( $field[ "default" ], $field[ "options" ] ) ) {
							echo( "wrong default value for enum. ({$table}.{$field["tag"]})" );
							exit;
						}
					}

					if ( $field[ "type" ] == "select" ) {
						if ( isset( $field[ "default" ] ) && !array_key_exists( $field[ "default" ], $field[ "options" ] ) ) {
							echo( "wrong default value for select. ({$table}.{$field["tag"]})" );
							exit;
						}
					}

					// validate connections..
					if ( !is_null( ( $chain = $this->detectFieldConnection( $field[ "type" ] ) ) ) ) {
						// echo $field["tag"]." ".$table ;print_r($chain);

						if ( !isset( $conns[ $field[ "tag" ] ] ) )
							$conns[ $field[ "tag" ] ] = array();
						if ( isset( $conns[ $field[ "tag" ] ][ $chain[ "chain" ] ] ) ) {
							echo "<strong>" . $field[ "tag" ] . "</strong> is already connected to <strong>" . $conns[ $field[ "tag" ] ][ $chain[ "chain" ] ] . "</strong> by label <strong>" . $field[ "tag" ] . "</strong>, choose another label to connect to the <strong>" . $table . "</strong><BR>";
							$gotError = true;
						}
						else
							$conns[ $field[ "tag" ] ][ $chain[ "chain" ] ] = $table;

						if ( $field[ "tag" ] == $chain[ "chain" ] && strpos( $field[ "type" ], "has lots" ) !== false ) {
							echo "N-N connections tag must be different from the target table, table:" . $table . " tag:" . $field[ "tag" ] . "  target:" . $chain[ "chain" ] . " <BR>";
							$gotError = true;
						}


						if ( isset( $field[ "unique" ] ) ) {
							echo "connection fields can not be unique labeled.";
							die();
						}

						if ( array_key_exists( $chain[ "chain" ], $owns ) && isset( $field[ "owns" ] ) ) {
							echo "<h3>WARNING : " . $chain[ "chain" ] . " is already owned by " . $owns[ $chain[ "chain" ] ] . ", {$table} is trying to own it again.</h3>";
							// die();
						}

						if ( array_key_exists( $chain[ "chain" ] . "." . $field[ "tag" ], $owning ) && isset( $field[ "owns" ] ) ) {
							echo "Error : " . $chain[ "chain" ] . " is owned by another object({$owning[$chain[ "chain" ] . "." . $field[ "tag" ]]}) with the same label : {$field["tag"]}. one of them must be different yo.";
							die();
						}

						if ( isset( $oppositeConnections[ $chain[ "chain" ] . "." . $table ] ) && $oppositeConnections[ $chain[ "chain" ] . "." . $table ] == $field[ "tag" ] ) {
							echo "same labels({$field["tag"]}) can't be used for both tables({$chain["chain"]},$table)<br />";
							JLog::log( "cri", "same labels({$field["tag"]}) can't be used for both tables({$chain["chain"]},$table)<br />", 1 );
							die();
						}

						if ( isset( $field[ "owns" ] ) ) {
							$owns[ $chain[ "chain" ] ]                           = $table;
							$owning[ $chain[ "chain" ] . "." . $field[ "tag" ] ] = $table;


							if ( $chain[ "type" ] == "1to1" ) {
								if ( !isset( $remoteConn[ $chain[ "chain" ] ] ) )
									$remoteConn[ $chain[ "chain" ] ] = array();
								if ( array_key_exists( $field[ "tag" ], $remoteConn[ $chain[ "chain" ] ] ) ) {
									echo "<h3>Error : " . reset( $remoteConn[ $chain[ "chain" ] ] ) . " is already connected to {$chain["chain"]} with {$field["tag"]} but <u>{$table}</u> is still trying to connect to the same object <br>with the same label. sorry, you must change it.</h3>";
									die();
								}
								else
									$remoteConn[ $chain[ "chain" ] ][ $field[ "tag" ] ] = $table;
							}
							// echo $field["tag"]."___".$chain["chain"]."___".$table.PHP_EOL;
						}

						if ( $chain[ "type" ] == "1toN" ) {
							if ( !isset( $remoteConn[ $chain[ "chain" ] ] ) )
								$remoteConn[ $chain[ "chain" ] ] = array();

							if ( array_key_exists( $field[ "tag" ], $remoteConn[ $chain[ "chain" ] ] ) ) {
								echo "<h3>Error : " . reset( $remoteConn[ $chain[ "chain" ] ] ) . " is already connected to {$chain["chain"]} with {$field["tag"]} but <u>{$table}</u> is still trying to connect to the same object <br>with the same label. sorry, you must change it.</h3>";
								die();
							}
							else
								$remoteConn[ $chain[ "chain" ] ][ $field[ "tag" ] ] = $table;
						}


						$oppositeConnections[ $table . "." . $chain[ "chain" ] ] = $field[ "tag" ];
					}
					elseif ( !array_key_exists( $field[ "type" ], $this->fieldTypes ) ) {
						echo '<br />' . $field[ "type" ] . ' what kind of a field type is this? ';
						die();
					}
				}
				else {
					print_r( $field );
					echo '<br />' . $table . ' structure is wrong: invalid field definition';
				}
			}
		}
		if ( $gotError )
			die();

		// print_r($remoteConn);
	}

	/**
	 * check necessary functions , just at the start of the building
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function checkNecessaryTables() {
		$this->debugLog( "checking necessary tables...<BR>\n" );

		$mdbCon      = JCache::read( 'JDSN.MDB' );
		$logbCon     = JCache::read( 'JDSN.LOG' );

		$this->logTableSQLs = array(
			"db"     => $this->sql( "createDB", $logbCon[ "schema" ] ),
			"logTable"      => $this->sql( "logTable", $logbCon[ "schema" ], self::$logtable ),
			"logEventTable" => $this->sql( "logEventTable", $logbCon[ "schema" ], self::$logeventtable, $logbCon[ "schema" ] )
		);

		$this->tableSQLs = array(
			"db"     => $this->sql( "createDB", $mdbCon[ "schema" ] ),
			"init"          => $this->sql( "init" ),
			"sessionsTable" => $this->sql( "sessionsTable", $mdbCon[ "schema" ], self::$sessionstable )
		);


		JPDO::connect( $mdbCon );

		foreach ( $this->tableSQLs as $p=>$sql ) {
			if ( $sql !== false && strlen( $sql ) > 0 )
				JPDO::executeQuery( $sql );
			#too lazy to make it singls TODO : FIX
			if($p=="db")
			$this->runPostDBCommand( $mdbCon["schema"] );
		}

		JPDO::connect( $logbCon );

		foreach ( $this->logTableSQLs as $p=>$sql ) {
			if ( $sql !== false && strlen( $sql ) > 0 )
				JPDO::executeQuery( $sql );
			if($p=="db"){
//				JPDO::debugQuery();
			$this->runPostDBCommand( $logbCon["schema"] );
			}
		}

		unset( $mdbCon, $logbCon );
	}

	/**
	 * table indexes
	 *
	 * @param string $db
	 * @param string $table
	 * @return array
	 * @author Özgür Köy
	 */
	public function getTableIndexes( $db, $table ) {
		$dsnDetails = $this->getDSNDetails( $db );
		JPDO::connect( JCache::read( 'JDSN.' . $db ) );
		$coin = JPDO::listIndexes( JUtil::tableName( $table ) );
//		 print_r($coin);
		$inr = $inl = array();

		foreach ( $coin as $coco ) {
			if ( !isset( $inl[ $coco[ "Column_name" ] ] ) )
				$inl[ $coco[ "Column_name" ] ] = array();

			$i0 = array(
				"gotIndex" => true,
				"isUnique" => $coco[ "Non_unique" ] == 0,
				"db"       => $dsnDetails[ "db" ],
				"schema"   => $dsnDetails[ "schema" ],
				"label"    => $coco[ "Key_name" ]
			);

			$inl[ $coco[ "Column_name" ] ][ ] = $i0;
		}

		$sql = $this->sql( "getAllFK", $dsnDetails[ "schema" ], $table );
		$ins = (array)JPDO::executeQuery( $sql );
		foreach ( $ins as $in ) {
			$ins = array(
				"gotFK"   => true,
				"label"   => $in[ "CONSTRAINT_NAME" ],
				"db"      => $dsnDetails[ "db" ],
				"schema"  => $dsnDetails[ "schema" ],
				"rtable"  => $in[ "REFERENCED_TABLE_NAME" ],
				"rcolumn" => $in[ "REFERENCED_COLUMN_NAME" ]
			);
			if ( !isset( $inl[ $in[ "COLUMN_NAME" ] ] ) )
				$inl[ $in[ "COLUMN_NAME" ] ] = array();

			$inl[ $in[ "COLUMN_NAME" ] ][ ] = $ins;
		}

		echo $table . PHP_EOL;
//		print_r( $inl );

		// echo $table.PHP_EOL;print_r($inl);
		return $inl;
	}

	/**
	 * get host and db name from the dsn info
	 *
	 * @param string $dsn
	 * @return array
	 * @author Özgür Köy
	 */
	public function getDSNDetails( $dsn ) {
		$t0 = JCache::read( 'JDSN.' . $dsn );
		if ( preg_match_all( '|.+:host=(.+?);dbname=(.+)|i', $t0[ "dsn" ], $match, PREG_SET_ORDER ) ) {
			return array( "host" => $match[ 0 ][ 1 ], "db" => $match[ 0 ][ 2 ], "schema" => isset($t0["schema"])?$t0["schema"]:$match[ 0 ][ 2 ] );
		}

		return array();
	}

	/**
	 * read yaml files
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public function readYamls() {
		global $__DP;

		include_once "$__DP/core/lib/spyc/spyc.php";


		//read site config
		$tempFileContent = file_get_contents( $__DP . '/site/def/config/site.yaml' );
		//echo $tempFileContent;
		$this->siteArray = Spyc::YAMLLoad( $tempFileContent );

		#exp yamls
		$yamls = scandir( $__DP . "site/def/config" );

		foreach ( $yamls as $tmm ) {
			if ( strpos( $tmm, ".yaml" ) !== false && strpos( $tmm, "site-" ) !== false ) {
				$tempFileContent = file_get_contents( $__DP . 'site/def/config/' . $tmm );
				if ( !( strlen( $tempFileContent ) > 10 ) ) continue;
				$addon = Spyc::YAMLLoad( $tempFileContent );
				if ( !is_array( $addon ) ) continue;

				#check for duplicates.

				$t1 = array_intersect( array_keys( $this->siteArray ), array_keys( $addon ) );
				if ( sizeof( $t1 ) > 0 ) {

					print_r( $t1 );
					die( "Some keys are already defined in previous yamls, yaml trying to overwrite is : " . $tmm );
				}
				$this->siteArray += $addon;
			}
		}
//		print_r( $this->siteArray );
		// echo '<pre>'; print_r($this->siteArray); echo '</pre>'; die();

		if ( is_file( "$__DP/site/def/config/dsn.yaml" ) ) {
			$this->dsnArray = Spyc::YAMLLoad( file_get_contents( "$__DP/site/def/config/dsn.yaml" ) );
		}
	}

	/**
	 * prepare field and values
	 *
	 * @param array  $field
	 * @param array  $toRunConnections
	 * @param string $fieldType
	 * @return bool
	 * @author Özgür Köy
	 */
	public function prepareField( &$field, &$table, &$toRunConnections, &$fieldType, &$fieldDetails, &$defaultValue ) {
		$tableDSN = $this->getDsn( $table );
		// $defaultValue 	= isset($field["default"])?$field["default"]:null;

		//the IF
		if ( !is_null( ( $chain = $this->detectFieldConnection( $field[ "type" ] ) ) ) ) {
			$field[ "type" ]  = $chain[ "type" ];
			$field[ "chain" ] = $chain[ "chain" ];
			$fieldType        = $chain[ "fieldType" ];
			$reverse          = $chain[ "reverse" ];
			$owns             = isset( $field[ "owns" ] );


			$toRunConnections[ $table . "." . $field[ "tag" ] ] = array(
				$table . "." . $field[ "chain" ],
				$field[ "chain" ] . ".id",
				$field[ "tag" ],
				$field[ "type" ],
				( isset( $field[ "ordering" ] ) && $field[ "ordering" ] == 1 ? true : false ),
				$tableDSN,
				$table,
				$owns
			);

			//no need to change any fields for this.
			if ( is_null( $fieldType ) )
				return false;
		}
		elseif ( $field[ "type" ] == "select" ) {
			$fieldType = array();
			foreach ( $field[ "options" ] as $key => $value ) {
				if ( is_null( $defaultValue ) ) $defaultValue = $key;
				$fieldType[ ] = "'" . $key . "'";
			}

			$fieldType = "enum(" . implode( ",", $fieldType ) . ")";
		}
		elseif ( $field[ "type" ] == "enum" ) {
			$fieldType = array();

			foreach ( $field[ "options" ] as $key => $value ) {
				if ( is_null( $defaultValue ) ) $defaultValue = $value;
				$fieldDetails[ $key ] = $value;
			}
			$fieldType = $this->sql("minInt");
		}
		elseif ( $field[ "type" ] == "bool" ) {
			$fieldType = $this->sql("bool");
			if ( is_null( $defaultValue ) ) $defaultValue = 0;
		}

		return true;
	}

	/**
	 * detect and return field connection details
	 *
	 * @param string $fieldType
	 * @return mixed
	 * @author Özgür Köy
	 */
	public function detectFieldConnection( $fieldType ) {
		foreach ( $this->connectionStrings as $keyf => $keyft ) {
			if ( strpos( $fieldType, $keyf . " " ) !== false ) { // thin validation
				$chain = trim( str_replace( $keyf . ' ', '', $fieldType ) );

				return array(
					"chain"     => $chain,
					"type"      => $keyft[ "label" ],
					"fieldType" => $keyft[ "fieldType" ],
					"reverse"   => isset( $keyft[ "reverse" ] )
				);
			}
		}

		return null;

	}

	/**
	 * remove previous(not used) fields from table.
	 *
	 * @param array $prvFields
	 * @param array $table
	 * @return void
	 * @author Özgür Köy
	 */
	public function removePreviousFields( $prvFields, $table, $tableDSN, $tableFields, $indexes ) {
		$prvFields = array_keys( $prvFields );

		JPDO::connect( JCache::read( 'JDSN.' . $tableDSN ) );

		$dsnDetails = $this->getDSNDetails( $tableDSN );

		// JPDO::connect( JCache::read('JDSN.'.$dsnDetails["db"]) );
		$ins = (array)JPDO::executeQuery( $this->sql( "findTargetFK", $dsnDetails[ "schema" ], $table ) );

		//strict operation needed
		foreach ( $prvFields as $pf ) {
			JPDO::$hideError = true;
			$tIndex          = isset( $indexes[ $pf ] ) ? $indexes[ $pf ] : array();

			if ( !in_array( $pf, $this->reservedFields ) && !in_array( $pf, $tableFields ) ) {
				foreach ( $tIndex as $tin ) {
					// remove owning FK
					if ( isset( $tin[ "gotFK" ] ) ) {
						$sql = $this->sql( "dropColumnFK", $tin[ "schema" ], $table, $tin[ "label" ] );
						JPDO::executeQuery( $sql );
					}
					// remove index
					elseif ( isset( $tin[ "gotIndex" ] ) ) {
						$sql = $this->sql( "dropIndex", $tin[ "schema" ], $table, $tin[ "label" ] );
						JPDO::executeQuery( $sql );
					}

				}

				//find targetted FK's
				foreach ( $ins as $in ) {
					$sql = $this->sql( "dropColumnFK", $in[ "TABLE_SCHEMA" ], $in[ "TABLE_NAME" ], $in[ "CONSTRAINT_NAME" ] );
					JPDO::executeQuery( $sql );
				}

				$sql = $this->sql( "dropField", $dsnDetails["schema"], $table, $pf );
				JPDO::executeQuery( $sql );
			}

			JPDO::$hideError = false;
		}
	}

	/**
	 * get table fields
	 *
	 * @param array  $allFields
	 * @param string $table
	 * @return array
	 * @author Özgür Köy
	 */
	private function getFields( $allFields, $table, $tag = null, $suf = "" ) {
		// $suf = ($table == $tag ? "_t" : "");
		// create fields
		foreach ( $allFields[ $table ] as $ff )
			$fieldsStr[ ] = is_null( $tag ) ? $ff : ( "`$tag.$suf`.`$ff` `$tag" . ucfirst( $ff ) . "`" );


		return $fieldsStr;
	}

	/**
	 * build process : build connection for the 2 objects
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function buildConnections( &$toRunConnections, &$allFields, &$allIndexes, &$enums ) {
		foreach ( $toRunConnections as $con ) {
			$source    = $con[ 0 ];
			$dest      = $con[ 1 ];
			$tag       = $con[ 2 ];
			$type      = $con[ 3 ] ? $con[ 3 ] : "1to1";
			$ordering  = $con[ 4 ] ? $con[ 4 ] : false;
			$dsn       = $con[ 5 ];
			$tableName = $con[ 6 ];
			$owns      = $con[ 7 ];

			$tm0              = explode( ".", $source );
			$sourceTable      = $tm0[ 0 ];
			$sourceField      = $tm0[ 1 ];
			$tm0              = explode( ".", $dest );
			$destTable        = $tm0[ 0 ];
			$destField        = $tm0[ 1 ];
			$indexes          = $allIndexes[ $sourceTable ];
			$dsnDetails       = $this->getDSNDetails( $dsn );
			$dsnRemote        = $this->getDsn( $destTable );
			$dsnRemoteDetails = $this->getDSNDetails( $dsnRemote );

			if ( $type == "1to1" ) {
				// echo PHP_EOL.$sourceTable."<<<<".PHP_EOL;

				if ( $owns ) {
					$tm0         = $sourceTable;
					$sourceTable = $destTable;
					$destTable   = $tm0;
					//re-build
					$indexes          = $allIndexes[ $sourceTable ];
					$dsnDetails       = $this->getDSNDetails( $dsn );
					$dsnRemote        = $this->getDsn( $destTable );
					$dsnRemoteDetails = $this->getDSNDetails( $dsnRemote );

				}

				if ( !isset( $this->siteArray[ $destTable ] ) && $destTable != "JDoc" ) die( "Wrong dest table($sourceTable) : " . $destTable );

				//add to relatives
				$this->site[ $sourceTable ][ "relatives" ][ $tag ] = $destTable;
				$this->site[ $destTable ][ "relatives" ][ $tag ]   = $sourceTable;

				if($owns){
					//$this->site[ $sourceTable ][ "ownage" ][ ]         = $tag;
					$this->site[ $destTable ][ "ownage" ][ ]         = $tag;
					echo $destTable . " OWNAGE ADDED : " . $tag . PHP_EOL;
				}

				//if it's the same table connection
				$suff           = $sourceTable == $destTable ? "_top" : "";
				$suffTableField = $tag == $sourceTable ? "_t" : "";
				// echo $sourceTable."<<<<".PHP_EOL;

				$fieldsStr        = $this->getFields( $allFields, $sourceTable, $tag, $suffTableField );
				$remFields        = $this->getFields( $allFields, $destTable, $tag );
				$remoteDsn        = $this->getDsn( $destTable );
				$remoteDsnDetails = $this->getDSNDetails( $remoteDsn );
				// echo $sourceTable;
				// print_r($fieldsStr);

				$this->site[ $sourceTable ][ "joins" ][ $tag ] = array(
					"table"       => $sourceTable,
					"targetTable" => $destTable,
					"tag"         => $tag,
					"type"        => "1to1",
					"reverse"     => false,
					"11Reverse"   => $owns ? true : false
				);

				$this->site[ $sourceTable ][ "remote" ][ $tag ] = array(
					"join"      => $this->sql(
							"innerJoin",
							$dsnRemoteDetails[ "schema" ],
							JUtil::tableName( $destTable ),
							$tag . $suffTableField,
							JUtil::tableName( $sourceTable ),
							$tag,
							$tag . $suffTableField,
							"id"
						),
					"type"      => "1to1",
					"defClause" => "",
					"clause"    => $tag . $suffTableField . ".id",
					"fields"    => implode( ",", $fieldsStr ),
					"table"     => $tag . $suffTableField,
					"tableName" => JUtil::tableName( $destTable ),
				);

				if ( $destTable == "JDoc" ) return;

				//ADD TO REMOTE TABLE
				$this->site[ $destTable ][ "joins" ][ $tag ] = array(
					"table"       => $sourceTable,
					"targetTable" => $sourceTable,
					"tag"         => $tag,
					"reverse"     => true,
					"type"        => "1to1"
				);
				// print_r($this->site[$destTable]["joins"][$tag]);
				$this->site[ $destTable ][ "remote" ][ $tag ] = array(
					"join"      => $this->sql(
							"innerJoin",
							$dsnDetails[ "schema" ],
							JUtil::tableName( $sourceTable ),
							$tag . $suffTableField,
							JUtil::tableName( $destTable ),
							"id",
							$tag . $suffTableField,
							$tag
						),
					"type"      => "1to1",
					"defClause" => "",
					"clause"    => $tag . $suffTableField . "." . $tag,
					"table"     => $tag . $suffTableField,
					"fields"    => implode( ",", $remFields ),
					"tableName" => JUtil::tableName( $sourceTable )
				);


				// indexing and fk
				$hasIndex = false;
				$hasFK    = false;
				$fkLabel  = $this->getFKLabel( $sourceTable, $tag, $destTable /*, $owns*/ );

				JPDO::connect( JCache::read( 'JDSN.' . $dsn ) );
				if ( sizeof( $indexes ) > 0 && isset( $indexes[ $tag ] ) ) {
					foreach ( $indexes[ $tag ] as $index ) {
						if ( isset( $index[ "gotFK" ] ) ) {

							// has a FK , check if it's ours..
							if ( $index[ "label" ] == $fkLabel )
								$hasFK = true;
							else {
								// not ours, old school maybe. so? gotta remove it.
								JPDO::connect( JCache::read( 'JDSN.' . $dsn ) );
								JPDO::executeQuery( $this->sql( "dropColumnFK", $index[ "schema" ], JUtil::tableName( $sourceTable ), $index[ "label" ] ) );

								JPDO::connect( JCache::read( 'JDSN.' . $remoteDsn ) );
								JPDO::executeQuery( $this->sql( "dropColumnFK", $remoteDsnDetails[ "schema" ], JUtil::tableName( $destTable ), $index[ "label" ] . "_121reverse" ) );

								JPDO::connect( JCache::read( 'JDSN.' . $dsn ) );
							}
							// break;
						}
						if ( isset( $index[ "gotIndex" ] ) )
							$hasIndex = true;
					}
				}

				if ( !$hasIndex ) {

					JPDO::$hideError=true;
					$sql = $this->sql( "addIndex", $dsnDetails[ "schema" ], JUtil::tableName( $sourceTable ), $tag, $tag );
					JPDO::executeQuery( $sql );
					JPDO::$hideError=false;

				}
				if ( !$hasFK ) { /*($owns?"deleteFK":"updateFK")*/
//					echo $fkLabel . PHP_EOL;
//					print_r( $indexes );
					$sql = $this->sql( ( $owns ? "deleteFK" : "updateFK" ), $dsnDetails[ "schema" ], JUtil::tableName( $sourceTable ), $fkLabel, $tag, $remoteDsnDetails[ "schema" ], JUtil::tableName( $destTable ), "id" );
					JPDO::executeQuery( $sql );
				}
				JPDO::$hideError = true;
				JPDO::connect( JCache::read( 'JDSN.' . $remoteDsn ) );
				// if($owns){
				// 	echo $sourceTable;
				// 	echo $sql = $this->sql("deleteFK", $remoteDsnDetails["db"], JUtil::tableName($destTable), $fkLabel."_121reverse", "id", $dsnDetails["db"], JUtil::tableName($sourceTable), $tag );
				// 	JPDO::executeQuery($sql);
				// }
				// else{
				// 	echo $sourceTable;
				// 	echo $sql = $this->sql("dropColumnFK", $remoteDsnDetails["db"], JUtil::tableName($destTable), $fkLabel."_121reverse");
				// 	JPDO::executeQuery( $sql );
				// 	
				// 	JPDO::connect( JCache::read('JDSN.'.$dsn) );
				// }
				JPDO::$hideError = false;
			}
			elseif ( $type == "1toN" ) {
				if ( !isset( $this->siteArray[ $destTable ] ) && $destTable != "JDoc" ) die( "Wrong dest table : " . $destTable );

				//add to relatives
				$this->site[ $sourceTable ][ "relatives" ][ $tag ] = $destTable;
				$this->site[ $destTable ][ "relatives" ][ $tag ]   = $sourceTable;
				if($owns)
					$this->site[ $sourceTable ][ "ownage" ][ ]     = $tag;



				$suffTableField                                    = $tag == $sourceTable ? "_t" : "";

				$fieldsStr = $this->getFields( $allFields, $sourceTable, $tag, $suffTableField );
				$remFields = $this->getFields( $allFields, $destTable, $tag );


				//if it's the same table connection
				$suff           = $sourceTable == $destTable ? "_top" : "";
				$suffTableField = $tag == $destTable ? "_t" : "";

				$this->site[ $sourceTable ][ "joins" ][ $tag ] = array(
					"table"       => $sourceTable,
					"targetTable" => $destTable,
					"tag"         => $tag,
					"type"        => "1toN"
				);

				$this->site[ $sourceTable ][ "remote" ][ $tag ] = array(
					"join"      => $this->sql(
							"innerJoin",
							$dsnRemoteDetails[ "schema" ],
							JUtil::tableName( $destTable ),
							$tag . $suffTableField,
							JUtil::tableName( $sourceTable ),
							"id",
							$tag . $suffTableField,
							$tag
						),
					"type"      => "1toN",
					"defClause" => "",
					"clause"    => $tag . $suffTableField . ".id",
					"fields"    => implode( ",", $remFields ),
					"table"     => $tag . $suffTableField,
					"tableName" => JUtil::tableName( $destTable )
				);

				if ( $destTable == "JDoc" ) return;

				//ADD TO REMOTE TABLE
				$this->site[ $destTable ][ "joins" ][ $tag ] = array(
					"table"       => $destTable,
					"targetTable" => $sourceTable,
					"tag"         => $tag,
					"reverse"     => true,
					"1NReverse"   => true,
					"type"        => "1to1"
				);

				$this->site[ $destTable ][ "remote" ][ $tag ] = array(
					"join"      => $this->sql(
							"innerJoin",
							$dsnDetails[ "schema" ],
							JUtil::tableName( $sourceTable ),
							$tag . $suffTableField,
							JUtil::tableName( $destTable ),
							$tag,
							$tag . $suffTableField,
							"id"
						),
					"type"      => "1to1",
					"defClause" => "",
					"clause"    => $tag . $suffTableField . ".id",
					"table"     => $tag . $suffTableField,
					"tableName" => JUtil::tableName( $sourceTable ),
					"fields"    => implode( ",", $fieldsStr )
				);

				// $this->site[$destTable]["fields"][] 		 = $tag;

				$indexes = $allIndexes[ $destTable ];

				// indexing and fk
				$hasIndex = false;
				$hasFK    = false;
				$fkLabel  = $this->getFKLabel( $destTable, $tag, $sourceTable, $owns );

				$remoteDsn        = $this->getDsn( $destTable );
				$remoteDsnDetails = $this->getDSNDetails( $remoteDsn );

				if ( sizeof( $indexes ) > 0 && isset( $indexes[ $tag ] ) ) {
					$cdetail = JCache::read( 'JDSN.' . $remoteDsn );
					JPDO::connect( $cdetail  );

					foreach ( $indexes[ $tag ] as $index ) {
						if ( isset( $index[ "gotFK" ] ) ) {
							// has a FK , check if it's ours..
							if ( $index[ "label" ] == $fkLabel ) {
								$hasFK = true;
							}
							else {
								// not ours, old school maybe. so? gotta remove it.
								echo $index[ "label" ] . " " . $fkLabel . PHP_EOL;
								echo $sql = $this->sql( "dropColumnFK", $cdetail[ "schema" ], JUtil::tableName( $destTable ), $index[ "label" ] );
								JPDO::executeQuery( $sql );
							}
							// break;
						}
						if ( isset( $index[ "gotIndex" ] ) )
							$hasIndex = true;
					}
				}
				else{
					echo "NOTFOUND : ".$tag.PHP_EOL;
				}

				if ( !$hasIndex ) {
					JPDO::$hideError=true;

					$sql = $this->sql( "addIndex", $remoteDsnDetails[ "schema" ], JUtil::tableName( $destTable ), $tag, $tag );
					JPDO::executeQuery( $sql );
					JPDO::$hideError=false;


				}
				if ( !$hasFK ) {
					$sql = $this->sql( ( $owns ? "deleteFK" : "updateFK" ), $remoteDsnDetails[ "schema" ], JUtil::tableName( $destTable ), $fkLabel, $tag, $dsnDetails[ "schema" ], JUtil::tableName( $sourceTable ), "id" );
					JPDO::executeQuery( $sql );
				}

			}
			elseif ( $type == "NtoN" ) {
				if ( !isset( $this->siteArray[ $destTable ] ) && $destTable != "JDoc" ) die( "Wrong dest table(something is really wrong) : " . $destTable );

				//add to relatives
				$this->site[ $sourceTable ][ "relatives" ][ $tag ] = $destTable;
				$this->site[ $destTable ][ "relatives" ][ $tag ]   = $sourceTable;

				$suff = ( $sourceTable == $destTable ? "_" : "" );


				// create fields
				// echo "FETCHING FIELDS FOR TABLE $tableName".PHP_EOL;

				$fieldsStr = $this->getFields( $allFields, $sourceTable, $tag );
				$remFields = $this->getFields( $allFields, $destTable, $tag );


				$joTable = self::$prefix . "C_" . $sourceTable . "_" . $destTable;

				$n2nCon= $this->sql(
						"N2NConnection",
						$joTable . "_" . $tag,
						JUtil::tableName( $sourceTable ),
//						$tag,
						$dsnDetails[ "schema" ],
						$joTable,
						$joTable . "_" . $tag,
						$dsnDetails[ "schema" ],
						JUtil::tableName( $destTable ),
						$tag,
						$tag,
						$joTable . "_" . $tag,
						JUtil::tableName( $destTable ),
						$tag,
						$tag,
						$joTable . "_" . $tag,
						$tag,
						$tag,
						$tag,
						JUtil::tableName( $sourceTable ),
						JUtil::tableName( $sourceTable )
					).PHP_EOL;


				$this->site[ $sourceTable ][ "remote" ][ $tag ] = array(
					"join"      => $n2nCon,
					"N2NConnection" => $n2nCon,
					"defClause" => "",//"{$joTable}_{$tag}.tag='$tag'",
					"clause"    => "$tag.id",
					"type"      => "NtoN",
					"fields"    => implode( ",", $remFields ),
					"table"     => $tag,
					"tableName" => JUtil::tableName( $destTable ),
				);

				$this->site[ $sourceTable ][ "connections" ][ $tag ] = array(
					"joinTable"   => $joTable,
					"sourceTable" => $sourceTable,
					"targetTable" => $destTable,
					"ordering"    => $ordering,
					"tag"         => $tag
				);
				$destKey = $destTable;
				//ADD TO REMOTE TABLE

				$joinSql = $this->sql(
						"innerJoin",
						$dsnDetails[ "schema" ],
						$joTable,
						$joTable . "_" . $tag,
						JUtil::tableName( $destTable ),
						"id",
						$joTable . "_" . $tag,
						JUtil::tableName( $destTable )
					) .
					" " .
					$this->sql(
						"innerJoin",
						$dsnDetails[ "schema" ],
						JUtil::tableName( $sourceTable ),
						$tag,
						$joTable . "_" . $tag,
						$sourceTable.$suff,
						$tag,
						"id"
					);

				$n2nCon= $this->sql(
						"N2NConnection",
						$joTable . "_" . $tag,
						JUtil::tableName( $destTable ),
						$dsnDetails[ "schema" ],
						$joTable,
						$joTable . "_" . $tag,
						$dsnDetails[ "schema" ],
						JUtil::tableName( $sourceTable ),
						$tag,
						$tag,
						$joTable . "_" . $tag,
						JUtil::tableName( $sourceTable ),
						$tag,
						$tag,
						$joTable . "_" . $tag,
						$tag,
						$tag,
						$tag,
						JUtil::tableName( $destTable ),
						JUtil::tableName( $destTable )
					).PHP_EOL;


				$tagSuff = $destTable == $sourceTable ? "_reverse" : "";
				$this->site[$destTable]["remote"][$tag.$tagSuff] 	    = array(
					"join"      => $n2nCon,
					"defClause" => "{$joTable}_{$tag}.tag='$tag'",
					"clause"    => "$tag.id",
					"type"      => "NtoN",
					"fields"    => implode( ",", $fieldsStr ),
					"table"     => $tag,
					"tableName" => JUtil::tableName( $sourceTable ),
				);
				if($destTable!=$sourceTable){
					$this->site[$destTable]["connections"][$tag] 	= array(
						"joinTable"   => $joTable,
						"sourceTable" => $sourceTable,
						"targetTable" => $destTable,
						"ordering"    => $ordering,
						"reverse"     => true,
						"tag"=>$tag
					);
				}




				// stop for jdoc NOW!
				if ( $destTable == "JDoc" ) return;

				//no need to connect to self dsn again.
				$conVars = JCache::read( 'JDSN.' . $dsn );
				JPDO::connect( $conVars );
				$tableExists = JPDO::tableExists( $joTable, $dsnDetails["db"], $dsnDetails["schema"] );

				if ( !$tableExists ) {
					$remoteDsn        = $this->getDsn( $destTable );
					$remoteDsnDetails = $this->getDSNDetails( $remoteDsn );

					$destField = ( $destTable . ( $sourceTable == $destTable ? "_" : "" ) );
					$csqls     = array(
						$this->sql( "createJOTable", $joTable, $sourceTable, $destField, $dsnDetails["schema"]  ),
						$this->sql( "addIndex", $dsnDetails[ "schema" ], $joTable, $joTable.$sourceTable, $sourceTable ),
						$this->sql( "addIndex", $dsnDetails[ "schema" ], $joTable, $joTable.$destField, $destField ),
						$this->sql( "addIndex", $dsnDetails[ "schema" ], $joTable, $joTable."_jdate", "jdate" ),
						$this->sql( "deleteFK", $dsnDetails[ "schema" ], $joTable, $this->getFKLabel( $sourceTable, $tag, $destTable, true ), $destField, $remoteDsnDetails[ "schema" ], JUtil::tableName( $destTable ), "id" ),
						$this->sql( "deleteFK", $dsnDetails[ "schema" ], $joTable, $this->getFKLabel( $destTable, $sourceTable, $sourceTable, true ), $sourceTable, $dsnDetails[ "schema" ], JUtil::tableName( $sourceTable ), "id" )

					);

					foreach ( $csqls as $c0=>$csql ) {
//						echo $c0 . PHP_EOL;

						JPDO::connect( JCache::read( 'JDSN.' . $dsn ) );
						JPDO::executeQuery( $csql );
					}
				}
			}
		}
	}

	/**
	 * build index from index.yaml
	 *
	 * @return void
	 * @author Özgür Köy
	 * TODO : VALIDATE INDEX
	 **/
	private function buildSiteIndex( &$classPile ) {
		global $__DP;

		if ( is_file( "$__DP/site/def/config/index.yaml" ) ) {
			$this->indexArray = Spyc::YAMLLoad( file_get_contents( "$__DP/site/def/config/index.yaml" ) );

			foreach ( $this->indexArray as $ip => $indexe ) {
				if ( !array_key_exists( $ip, $this->site ) ) {
					JUtil::configProblem( "bad class name : $ip" );
				}
				// $classPile[$ip]["final"] = $classPile[$ip]["bridge"]  = array();

				foreach ( $indexe as $indexName => $iloot ) {
					$bigBridge[ $indexName ] = $bigFinal[ $indexName ] = array();

					foreach ( $iloot as $loot ) {
						if ( !in_array( "fields", array_keys( $loot ) ) || !in_array( "type", array_keys( $loot ) ) || !in_array( "name", array_keys( $loot ) ) ) {
							JUtil::configProblem( "missing definition for <strong>$indexName</strong>, fields, type and name keys are required" );
						}

						foreach ( $loot[ "fields" ] as $ifindex => $ifield ) {
							// echo $ifindex;

							if ( strpos( $ifield, "." ) !== false ) {
								$i0         = explode( ".", $ifield );
								$tempfclass = $ip;

								foreach ( $i0 as $if0 ) {
									if ( !isset( $classPile[ $tempfclass ][ "final" ] ) ) $classPile[ $tempfclass ][ "final" ] = array();
									if ( !isset( $classPile[ $tempfclass ][ "bridge" ] ) ) $classPile[ $tempfclass ][ "bridge" ] = array();

									if ( isset( $this->site[ $tempfclass ] ) && isset( $this->site[ $tempfclass ][ "relatives" ][ $if0 ] ) ) {
										//digging deeper.
										// if($tempfclass!=$ip)
										if ( substr( $ifield, strlen( $ifield ) - 1, 1 ) == "!" )
											$ifield = substr( $ifield, 0, strlen( $ifield ) - 1 );


										$classPile[ $tempfclass ][ "bridge" ][ ] = array( "index"     => $ifindex,
										                                                  "ifield"    => $ifield,
										                                                  "action"    => $if0,
										                                                  "type"      => $loot[ "type" ],
										                                                  "field"     => $loot[ "name" ],
										                                                  "indexName" => $indexName,
										                                                  "top"       => $ip
										);

										$tempfclass = $this->site[ $tempfclass ][ "relatives" ][ $if0 ];
									}
									elseif ( in_array( $if0, $this->site[ $tempfclass ][ "fields" ] ) || in_array( $if0, array( "id", "jdate" ) ) ) {
										//dug enough, found the field.
										$classPile[ $tempfclass ][ "final" ][ ] = array( "index"     => $ifindex,
										                                                 "ifield"    => $ifield,
										                                                 "indexName" => $indexName,
										                                                 "type"      => $loot[ "type" ],
										                                                 "top"       => $ip,
										                                                 "field"     => $loot[ "name" ]
										);

									}
									else {
										//problem, if not forced (!)
										if ( substr( $if0, strlen( $if0 ) - 1, 1 ) != "!" ) {
											JUtil::configProblem( "Wrong relativity for class $tempfclass -> $if0 . <BR>
																  if you want to define a custom field, use a ! at the end of the field" );
										}
										else {
											//dug enough, found the field.
											$classPile[ $tempfclass ][ "final" ][ ] = array( "index"     => $ifindex,
											                                                 "ifield"    => ( substr( $ifield, strlen( $ifield ) - 1, 1 ) == "!" ? substr( $ifield, 0, strlen( $ifield ) - 1 ) : $ifield ),
											                                                 "indexName" => $indexName,
											                                                 "type"      => $loot[ "type" ],
											                                                 "field"     => $loot[ "name" ],
											                                                 "top"       => $ip
											);

											// $classPile[$tempfclass]["bridge"] 	= $bridges;


										}

									}

								}
							}
							elseif ( in_array( $ifield, $this->site[ $ip ][ "fields" ] ) || in_array( $ifield, array( "id", "jdate" ) ) ) {
								$classPile[ $ip ][ "final" ][ ] = array( "index"     => $ifindex,
								                                         "ifield"    => $ifield,
								                                         "indexName" => $indexName,
								                                         "type"      => $loot[ "type" ],
								                                         "top"       => $ip,
								                                         "field"     => $loot[ "name" ]
								);

							}
							elseif ( isset( $this->site[ $ip ][ "relatives" ][ $ifield ] ) ) {
								if ( substr( $ifield, strlen( $ifield ) - 1, 1 ) == "!" )
									$ifield = substr( $ifield, 0, strlen( $ifield ) - 1 );

								$classPile[ $ip ][ "bridge" ][ ] = array( "index"     => $ifindex,
								                                          "ifield"    => $ifield,
								                                          "action"    => $ifield,
								                                          "type"      => $loot[ "type" ],
								                                          "field"     => $loot[ "name" ],
								                                          "indexName" => $indexName,
								                                          "top"       => $ip
								);

							}

							else {
								if ( substr( $ifield, strlen( $ifield ) - 1, 1 ) == "!" ) {
									$classPile[ $ip ][ "final" ][ ] = array( "index"     => $ifindex,
									                                         "ifield"    => substr( $ifield, 0, strlen( $ifield ) - 1 ),
									                                         "indexName" => $indexName,
									                                         "type"      => $loot[ "type" ],
									                                         "top"       => $ip,
									                                         "field"     => $loot[ "name" ]
									);

								}
								else {
									JUtil::configProblem( "Wrong field definition : $indexName -> $ifield" );
								}
							}
						}
					}
				}
				JIndex::buildIndexTable( $ip, $indexe, $this->site[ $ip ][ "dsn" ] );
			}


			// print_r($this->indexArray);

		}

	}

	/**
	 * get dsn of taable
	 *
	 * @return string
	 * @author Özgür Köy
	 **/
	private function getDsn( $table ) {
		if ( isset( $this->dsnArray[ $table ] ) && array_key_exists( $table, $this->dsnArray ) ) {
			return $this->dsnArray[ $table ];
		}
		else {
			return 'MDB';
		}
	}

	private function getFKLabel( $source, $field, $remote, $cascade = false ) {
		return substr( ( $cascade ? "jfc_" : "jfnc_" ) . $source . "_" . $remote . "_" . $field, 0, 60 );
	}

	private function createOnSiteArray( $table ) {
		if ( isset( $this->site[ $table ] ) ) return false;

		$this->site[ $table ] = array(
			"main"          => array(),
			"fields"        => array(),
			"fieldDefaults" => array(),
			"uniques"       => array(),
			"label"         => "",
			"joins"         => array(),
			"enums"         => array(),
			"connections"   => array(),
			"relatives"     => array(),
			"ownage"        => array()
		);

	}

	/**
	 * debug to screen
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	private function debugLog( $log ) {
		echo date( "Y.m.d H:i:s" ) . " >>> " . $log . PHP_EOL;
	}

	public function buildRoutings() {
		global $__DP;
		$conts       = array( "___main" => array() );
		$pres        = array("___main" => array());
		$preArgs     = array("___main" => array());
		$posts       = array("___main" => array());
		$postArgs    = array("___main" => array());
		$allMethods  = array();
		$bulkMethods = array();

		//find controller files.
		$controllers = scandir( $__DP . "site/controller" );
		foreach ( $controllers as $cont ) {
			if ( strpos( $cont, ".php" ) !== false && $cont != "J.php" ) { // discard J.php
				include( $__DP . "site/controller/" . $cont );
			}
		}

		$clz = get_declared_classes();
		foreach ( $clz as $cl ) {
			if ( strpos( $cl, "_controller" ) === false ) continue;

			$a = new ReflectionAnnotatedClass( $cl );

			$_pre  = $a->getAllAnnotations( "JCTLPre" );
			$_post = $a->getAllAnnotations( "JCTLPost" );
			$_ns   = $a->getAllAnnotations( "JCTLNS" );

			$_ns = sizeof( $_ns ) == 0 ? "___main" : $_ns[ 0 ]->value;

			if ( in_array($_ns, $this->restrictedZones ) )
				die( "Can't use ".$_ns." for namespace" );

			#check folder locations
			if( !is_dir($__DP."site/def/zone") )
				die("Create {$__DP}site/def/zone folder");

			if( $_ns!== "___main" && !(is_dir($__DP."site/def/zone/".$_ns) && is_writable($__DP."site/def/zone/".$_ns)) )
				die("Create {$__DP}site/def/zone/{$_ns} folder with write permissions to http user");

			if( $_ns != "___main" && !(is_file( ($zoneFormPath=$__DP."site/def/zone/".$_ns."/JFormZone.php")  ))){
				JT::init();
				$zoneForm = JT::pfetch( "_jformZone" );
				file_put_contents( $zoneFormPath, $zoneForm );

				unset( $zoneForm );

				self::debugLog("Create JFormZone.php for zone ".$_ns);
			}

//			if(!is_writable($__DP . "site/def/generated/_routings.php"))
//				die("Make sure the {$__DP}site/def/generated/_routings.php file is writable");


			$methods = get_class_methods( $cl );
			foreach ( $methods as $mm ) {
				if ( !isset( $pres[ $_ns ] ) ) $pres[ $_ns ] = array();
				if ( !isset( $posts[ $_ns ] ) ) $posts[ $_ns ] = array();
				if ( !isset( $preArgs[ $_ns ] ) ) $preArgs[ $_ns ] = array();
				if ( !isset( $postArgs[ $_ns ] ) ) $postArgs[ $_ns ] = array();

				$a = new ReflectionAnnotatedMethod( $cl, $mm );

				$r = $a->getAllAnnotations( "JCTL" );
				if ( sizeof( $r ) > 0 ) {
					foreach ( (array)$r[ 0 ]->value as $c0 ) {

						if ( $_ns != "___main" ) {
							if ( !isset( $allMethods[ $_ns ] ) ) $allMethods[ $_ns ] = array();

							$allMethods[ $_ns ][ ] = "___" . $c0 . "___";
						}


						if ( isset( $bulkMethods[ $c0 ] ) )
							die( $c0 . " is already set as a controller for " . $conts[ $bulkMethods[ $c0 ] ][ $c0 ] . " and '$cl' is trying to that again on '$mm''" );
						else
							$bulkMethods[ $c0 ] = $_ns;

						if ( !isset( $conts[ $_ns ] ) ) $conts[ $_ns ] = array();

						$conts[ $_ns ][ $c0 ] = str_replace( "_controller", "", $cl ) . "_" . $mm;
					}

					#pre controller op operations
					if ( sizeof( $_pre ) == 0 )
						$act = $a->getAllAnnotations( "JCTLPre" );
					else
						$act = $_pre;

					if ( sizeof( $act ) > 0 ) {
						foreach ( (array)$r[ 0 ]->value as $c0 ) {
							$pres[ $_ns ][ $c0 ]    = $act[ 0 ]->func;
							$preArgs[ $_ns ][ $c0 ] = isset( $act[ 0 ]->args ) ? var_export( $act[ 0 ]->args, true ) : array();
						}
					}

					#post controller op operations
					if ( sizeof( $_post ) == 0 ) {
						$act = $a->getAllAnnotations( "JCTLPost" );
					}
					else
						$act = $_post;


					if ( sizeof( $act ) > 0 ) {
						foreach ( (array)$r[ 0 ]->value as $c0 ) {
							$posts[ $_ns ][ $c0 ]    = $act[ 0 ]->func;
							$postArgs[ $_ns ][ $c0 ] = isset( $act[ 0 ]->args ) ? var_export( $act[ 0 ]->args, true ) : array();
						}
					}

				}
			}
		}

		$t0                 = array_shift( $conts );
		$conts[ "___main" ] = $t0;
		#writing routings file
		foreach ( $conts as $ns => $cont ) {
			$t0 = date( "d-m-Y H:i", time() );
			JT::init();
			JT::assign( "time", $t0 );
			JT::assign( "routings", $cont );
			JT::assign( "pres", $pres[ $ns ] );
			JT::assign( "preArgs", $preArgs[ $ns ] );
			JT::assign( "posts", $posts[ $ns ] );
			JT::assign( "postArgs", $postArgs[ $ns ] );

			if ( $ns == "___main" ) {

				if(!is_file($__DP . "site/def/generated/_routings.php")){
					touch( $__DP . "site/def/generated/_routings.php" );
					file_put_contents($__DP . "site/def/generated/_routings.php",'<?php $__routings=array();');
				}

				JT::assign( "allMethods", $allMethods );
				$path = $__DP . "site/def/generated/_routings.php";
			}
			else {
				$path = $__DP . "site/def/zone/{$ns}/routings.php";
			}

			$rtp = JT::pfetch( "_routings" );
			file_put_contents( $path, $rtp );
		}

		self::debugLog( "Routings complete" );
		self::debugLog( "<a href='/core/run/build/'>Back to tasks</a>" );

	}

	public function buildExport() {
		global $__DP;
		include_once "$__DP/core/lib/spyc/spyc.php";


		if ( defined( 'ENVIRONMENT_CODE' ) !== true )
			die( "You don't have the ENVIRONMENT_CODE defined. You need that to export" );

		if ( is_file( $__DP . '/site/def/config/export.yaml' ) ) {
			$tempFileContent = file_get_contents( $__DP . '/site/def/config/export.yaml' );
			if ( sizeof( $tempFileContent ) > 0 ) {
				$objects     = array();
				$connections = array();
				$operations  = array();
				$ymx         = Spyc::YAMLLoad( $tempFileContent );
				$warned      = array();
//				print_r( $ymx );exit;

				foreach ( (array)$ymx as $tobj => $details ) {
					$oKey = isset( $details[ "key" ] ) ? explode( ",", $details[ "key" ] ) : array( "id" );
					$oOp  = isset( $details[ "operation" ] ) ? $details[ "operation" ] : "insertOnly";
					if ( !in_array( $oOp, $this->exportTypes ) )
						die( "<b>ERROR:</b> " . $oOp . " is not a valid operation type" );

					$operations[ $tobj ] = array(
						"operation" => $oOp,
						"key"       => $oKey,
						"subs"      => array()
					);

					$o0 = new $tobj();

					foreach ( $oKey as $s0 ) {
						if ( !in_array( $s0, $o0->_fields ) )
							die( "<b>ERROR:</b> " . $o0->selfname . " does not have a field named: $s0" );

						if ( isset( $o0->_relatives[ $s0 ] ) )
							die( "<b>ERROR:</b> You can only filter by fields, not relatives." . $o0->selfname . " => $s0" );
					}

					if ( !in_array( $oOp, array( "insertOnly", "updateOnly", "put" ) ) && !isset( $warned[ $o0->selfname ] ) ) {
						echo "<b>WARNING:</b> keys will not be effective if you are not using insertOnly or updateOnly ({$o0->selfname})" . PHP_EOL;
						$warned[ $o0->selfname ] = true;
					}

					$o0->populateOnce( false )->nopop()->load();

					$relativeMethods = $o0->relativeMethods;
					if ( !isset( $connections[ $o0->selfname ] ) ) $connections[ $o0->selfname ] = array();


					$subs = isset( $details[ "subs" ] ) ? $details[ "subs" ] : array();

					while ( $o0->populate() ) {

						if ( !isset( $connections[ $o0->selfname ][ $o0->id ] ) ) $connections[ $o0->selfname ][ $o0->id ] = array();
						if ( !isset( $objects[ $o0->selfname ] ) ) $objects[ $o0->selfname ] = array();

						$objects[ $o0->selfname ][ $o0->id ] = $this->getObjectFields( $o0 );

						foreach ( $subs as $sub => $opx ) {
							$operation = $opx[ "operation" ];

							if ( !isset( $relativeMethods[ $sub ] ) )
								die( "<b>ERROR:</b> " . "$tobj does not show any resemblance : $sub" );

							$m0 = $relativeMethods[ $sub ];

							if ( !method_exists( $o0, $m0 ) )
								die( "<b>ERROR:</b> " . "$tobj does not show any resemblance(method) : $sub" );

							$r0 = call_user_func_array( array( $o0, $m0 ), array( null, 0, 10, null, false ) ); # for non 1-1 loadings

							if ( $r0 && $r0->gotValue ) {
								$subKey = isset( $opx[ "key" ] ) ? explode( ",", $opx[ "key" ] ) : array( "id" );

								$diff = array_diff( $r0->_fields, array_keys( (array)$r0->_relatives ), array("id","jdate") );

								if ( $operation != "flush" && $subKey[0] == "id" && !isset( $warned[ $r0->selfname."_" ] ) ) {
									#id as key, and no flushing, and as a sub, you probably got a problem bro
									echo "<b>WARNING:</b>\nA problem might occur while importing (Mostly duplicates) : ".PHP_EOL;
									echo "You are exporting <b>$tobj</b> with <b>$sub({$r0->selfname})</b> with key as id(if you haven't set a key for export op, it will be id)".PHP_EOL;
									if(sizeof($diff)>0)
										echo "Suggestion : Either use flush in this case or you can use one of the these fields as key : <b>".implode(",",$diff)."</b></span>".PHP_EOL;
									else
										echo "Suggestion : You must use flush because {$r0->selfname} does not have any fields other than relatives and standard fields</span>".PHP_EOL;
									$warned[ $r0->selfname."_" ] = true;
								}



								if ( !in_array( $operation, array( "insertOnly", "put", "updateOnly" ) ) && !isset( $warned[ $r0->selfname ] ) && $subKey[0] != "id" ) {
									echo "<b>WARNING:</b> keys will not be effective if you are not using put, insertOnly or updateOnly ({$r0->selfname})" . PHP_EOL;
									$warned[ $r0->selfname ] = true;
								}
								foreach ( $subKey as $s0 ) {
									if ( !in_array( $s0, $r0->_fields ) )
										die( "<b>ERROR:</b> " . $r0->selfname . " does not have a field named: $s0" );
								}


								if ( !isset( $objects[ $r0->selfname ] ) ) $objects[ $r0->selfname ] = array();
								$operations[ $tobj ][ "subs" ][ $r0->selfname ] = array(
									"operation" => $operation,
									"key"       => $subKey,
									"subs"      => array()
								);

								do {
									if ( !isset( $connections[ $o0->selfname ][ $o0->id ][ $m0 ] ) )
										$connections[ $o0->selfname ][ $o0->id ][ $m0 ] = array();

									$objects[ $r0->selfname ][ $r0->id ]               = $this->getObjectFields( $r0 );
									$connections[ $o0->selfname ][ $o0->id ][ $m0 ][ ] = $r0->id;
								} while ( $r0->populate() );
							};


						}

					}

				}

				if ( sizeof( $objects ) > 0 ) {
					JT::init();
					JT::assign( "operations", base64_encode(serialize( $operations )) );
					JT::assign( "connections", base64_encode(serialize( $connections )) );
					JT::assign( "objects", base64_encode(serialize( $objects )) );

					$f        = JT::pfetch( "_export" );
					$filename = date( "Y-m-d_H-i", time() ) . "_" . ENVIRONMENT_CODE . ".php";
					file_put_contents( $__DP . "site/export/" . $filename, $f );

					self::debugLog( "Export Complete" );
					self::debugLog( "<a href='/core/run/build/'>Back to tasks</a>" );

				}
				else echo "export failed";
			}
		}


	}

	public function buildTemplates( $dir = null, $return = false ) {
		global $__DP;

		#check permissions
//		if(!is_writable($__DP . "site/def/generated/_smartyCacheDirs.php"))
//			die( $__DP . "site/def/generated/_smartyCacheDirs.php is not writable, make sure it gets to be." );

		$tempDir = ( is_null( $dir ) ? $__DP . "site/templates" : $dir ) . "/";
		$temps   = array();
		if ( !is_file( $__DP . "site/def/generated/_smartyCacheDirs.php" ) ){
			touch( $__DP . "site/def/generated/_smartyCacheDirs.php" );
			file_put_contents($__DP . "site/def/generated/_smartyCacheDirs.php",'<?php $__smartyCacheDirs=array();');
		}

		$templates = scandir( $tempDir );

		foreach ( $templates as $tmm ) {
			if ( strpos( $tmm, ".tpl" ) !== false && $tmm[ 0 ] != "_" ) {
//				echo $tempDir;
				$t8                                             = str_replace( $__DP . "site/templates", "", $tempDir );
				$t9                                             = explode( "/", preg_replace( '/^\/(.+?)\/?$/', '$1', $t8 ) );
				$dname                                          = is_null( $dir ) ? "" : ( implode( "/", $t9 ) );
				$tname                                          = is_null( $dir ) ? $tmm : ( str_replace( "/", "_", $dname ) . "_" . $tmm );
				$temps[ preg_replace( '/\.tpl/', '', $tname ) ] = ( $dname == "" ? $dname : $dname . "/" ) . $tmm;
			}
			elseif ( preg_match( '/\.+/', $tmm ) === 0 && is_dir( $tempDir . $tmm ) ) {
//				echo $tmm;exit;
				$t1    = $this->buildTemplates( $tempDir . $tmm, true );
				$temps = $temps + $t1;
			}
		}

		if ( $return !== false )
			return $temps;

		$t0 = date( "d-m-Y H:i", time() );

		JT::init();
		JT::assign( "time", $t0 );
//		JT::assign( "time", date( "d-m-Y H:i", time() ) );
		JT::assign( "templates", $temps );
		$rtp = JT::pfetch( "_templates" );
		file_put_contents( $__DP . "site/def/generated/_smartyCacheDirs.php", $rtp );

		self::debugLog( "Templates complete" );
		self::debugLog( "<a href='/core/run/build/'>Back to tasks</a>" );

	}

	private function runPostDBCommand( $schema ) {
		$postDBQuery = $this->sql( "postCreateDB", $schema );

		if ( strlen( $postDBQuery ) > 0 ) {
			JPDO::$hideError = true;
			JPDO::executeQuery( $postDBQuery);
			JPDO::$hideError = false;
		}

	}

	private function getObjectFields( &$obj ) {
		$_relatives = isset( $obj->_relatives ) ? $obj->_relatives : array();
		$_fields    = isset( $obj->_fields ) ? $obj->_fields : array();

		$ret = array();
		foreach ( $_fields as $f0 ) {
			$ret[ $f0 ] = htmlentities( $obj->$f0, ENT_QUOTES );
		}

		return $ret;
	}


	public static function emptyCache() {
		global $__DP;
		JT::init();
		JT::clearCompiled();
		JT::clearAllCache();
//		self::delTree($__DP."/temp/templateCompiled");
//		self::delTree($__DP."/temp/templateCache");
//		mkdir($__DP."/temp/templateCompiled",0777);
//		mkdir($__DP."/temp/templateCache",0777);


		JCache::write('siteVarsInited',false);
		JCache::flush();
	}

	public static function delTree($dir) {
		$files = array_diff(scandir($dir), array('.','..'));
		foreach ($files as $file) {
			(is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
		}
		return rmdir($dir);
	}

} // END class Jbuild