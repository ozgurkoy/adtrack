<?php
/**
 * JDoc class
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JDoc
{
	/**
	 * image quality
	 *
	 * @var int
	 **/
	private static $imQuality=90;

	/**
	 * self name for filtering
	 *
	 * @var string
	 **/
	private static $selfname="JDoc";

	/**
	 * ids object currently holding
	 *
	 * @var array
	 **/
	public static $_ids=array();


	/**
	 * constructor , not useful
	 *
	 * @return void
	 **/
	private  function __construct()
	{
	}

	/**
	 * add to lib
	 *
	 * @return array
	 **/
	public static function add($type,$id,$data,$subtype=null,$ordering=false)
	{
		$newIds = array();
		if(!is_array($data["name"])) return self::addOneFile($type,$id,$data,$subtype,$ordering);

		$_prevDsn = JPDO::$dsn;
		for($i=0;$i<sizeof( $data['name'] );$i++){
			$tempName	= $data['tmp_name'][$i];
			$fname 		= JUtil::randomString(10).".".JUtil::normalize($data['name'][$i]);
			$flType 	= $data['type'][$i];

			if(@filesize ( $tempName )==0) continue;

			$storage = self::getStoragePlace($type,$subtype);


			$uploadfile = $storage["data"]["path"].$fname;
			if(@move_uploaded_file($tempName, $uploadfile)==false){
				JLog::log("cri","file upload error:".$tempName.' FILES:'.print_r($_FILES, true));
			}
			if($ordering==false)
				$insertQuery = "INSERT INTO `".JDef::$doctable ."`(`sto`,`type`,`subtype`,`refId`,`filename`,`conType`,`jdate`)
								VALUES(?,?,?,?,?,?,?);
				";
			else
				$insertQuery = "INSERT INTO `".JDef::$doctable ."`(`jorder`,`sto`,`type`,`subtype`,`refId`,`filename`,`conType`,`jdate`)
								SELECT (SELECT max(`jorder`)+1 FROM  `".JDef::$doctable ."`), ?,?,?,?,?,?,?;
				";

			JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );

			// echo $insertQuery,$storage["alias"],$type,is_null($subtype)?"":$subtype,$id,$fname,$flType;

			JPDO::executeQuery($insertQuery,$storage["alias"],$type,is_null($subtype)?"":$subtype,$id,$fname,$flType,time());
//			TODO : Fix below
			$newIds[] = JPDO::lastId();
		}

		JPDO::connect($_prevDsn);
		
		return $newIds;
	}

	/**
	 * add to lib
	 *
	 * @return array
	 **/
	public static function addOneFile($type,$id,$data,$subtype=null,$ordering=false) {
		$tempName	= $data['tmp_name'];
		$fname 		= JUtil::randomString(10).".".JUtil::normalize($data['name']);
		$flType 	= $data['type'];

		if(@filesize ( $tempName )==0) return false;

		$storage = self::getStoragePlace($type,$subtype);

		$uploadfile = $storage["data"]["path"].$fname;
		if(@move_uploaded_file($tempName, $uploadfile)==false){
			JLog::log("cri","file upload error:".$tempName.' FILES:'.print_r($_FILES, true));
			return false;
		}
		
		if($ordering==false)
			$insertQuery = "INSERT INTO `".JDef::$doctable ."`(`sto`,`type`,`subtype`,`refId`,`filename`,`conType`,`jdate`)
							VALUES(?,?,?,?,?,?,?);
			";
		else
			$insertQuery = "INSERT INTO `".JDef::$doctable ."`(`jorder`,`sto`,`type`,`subtype`,`refId`,`filename`,`conType`,`jdate`)
							SELECT (SELECT max(`jorder`)+1 FROM  `".JDef::$doctable ."`), ?,?,?,?,?,?,?;
			";

		$_prevDsn = JPDO::$dsn;
		
		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );

		// echo $insertQuery,$storage["alias"],$type,is_null($subtype)?"":$subtype,$id,$fname,$flType;

		JPDO::executeQuery($insertQuery,$storage["alias"],$type,is_null($subtype)?"":$subtype,$id,$fname,$flType,time());


		//			TODO : Fix below
		$lastId = JPDO::lastId();

		JPDO::connect($_prevDsn);
		
		return $lastId;
	}
	
	/**
	 * add to library
	 * path and type required within $data
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public function addToLibrary($data,$id,$type,$subtype)
	{
		$storage = self::getStoragePlace($type,$subtype);

		$t0 		= explode("/",$data['path']);
		$fname 		= array_pop($t0);
		$fname 		= JUtil::randomString(10).".".JUtil::normalize($fname);
		$uploadfile = $storage["data"]["path"].$fname;
		
		if(@rename($data['path'], $uploadfile)){
		
			$insertQuery = "INSERT INTO `".JDef::$doctable ."`(`sto`,`type`,`subtype`,`refId`,`filename`,`conType`,`jdate`)
							VALUES(?,?,?,?,?,?,?);
			";
		
			JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );
			JPDO::executeQuery($insertQuery,$storage["alias"],$type,$subtype,$id,$fname,$data['type'],time());

		
			JPDO::revertDsn();
			return true;
		}
		return false;
	}

	/**
	 * load from lib
	 *
	 * @return array
	 **/
	public static function load($type,$id,$subtype=null)
	{
		$selectQuery = "SELECT * FROM `".JDef::$doctable ."` WHERE `refId`=? AND `type`=?".(is_null($subtype)?"":" AND `subtype`=?")." ORDER BY `jorder`,`jdate` ASC";

		$storage = self::getStoragePlace($type,$subtype);

		
		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );

		if(is_array($id)){
			if(isset($id[0])) $id = $id[0];
			else return false;
		}

		// echo $selectQuery." ".$id." ".$type." ".(is_null($subtype)?"":$subtype);

		$kf = JPDO::executeQuery($selectQuery,$id,$type,is_null($subtype)?"":$subtype);
		
		JPDO::revertDsn();
		
		if(sizeof($kf)>0){
			foreach ($kf as $data1) {
				//load ids
				self::$_ids[] = $data1["id"];
			}
		}
		
		return self::finalizeArray($kf,$storage);
	}

	/**
	 * load by docid
	 * TODO: FIX the id -> storage place.
	 * DEPRECATED : CAN NOT KNOW DSN
	 *
	 * @return array
	 **/
	public static function loadById($id,$type="")
	{
		$selectQuery = "SELECT * FROM `".JDef::$doctable ."` WHERE `id`=?";

		$storage = self::getStoragePlace($type);

		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );
		$kf = JPDO::executeQuery($selectQuery,$id);
		if(sizeof($kf)>0){
			foreach ($kf as $data1) {
				//load ids
				self::$_ids[] = $data1["id"];
			}
		}
		
		JPDO::revertDsn();

		return self::finalizeArray($kf,$storage);
	}

	/**
	 * finalize returning array
	 *
	 * @return void
	 **/
	private static function finalizeArray($dbArray,$storageArray)
	{
		if(!is_array($dbArray) || !is_array($storageArray)) return null;
		$kf = array();

		foreach ($dbArray as $dbai => $dba) {
			if(!is_array($storageArray["data"])) continue;
			$kf[$dbai] = array_merge($dba, $storageArray["data"]);

			$kf[$dbai]["realPath"] = "http://".$storageArray["data"]["host"]."/".$storageArray["data"]["dpath"]."/".$dba["filename"];
		}

		return $kf;
	}

	/**
	 * thumber
	 *
	 * @return array
	 **/
	public static function thumbById($docid,$type,$subtype,$maxW,$maxH)
	{
		$storage = self::getStoragePlace($type,$subtype);

		//check for cache
		if(is_file($storage["data"]["path"]."/".$docid.".".$maxW.".".$maxH.".jpg"))
			return "http://".$storage["data"]["host"]."/".$storage["data"]["dpath"]."".$docid.".".$maxW.".".$maxH.".jpg";

		$selectQuery = "SELECT * FROM `".JDef::$doctable."` WHERE `id`=?";

		$_prevDsn = JPDO::$dsn;
		
		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );
		$kf = JPDO::executeQuery($selectQuery,$docid);

		if(!is_file( $storage["data"]["path"]."/".$kf[0]["filename"] )) {
			$storage["data"]["path"] = JUtil::siteAddress()."site/layout/admin/images/";
			$kf[0]["filename"] = "default.jpg";

			// return "";
		}

		if(!is_readable( $storage["data"]["path"]."/".$kf[0]["filename"] )) return false;

		$t0 = @getimagesize( $storage["data"]["path"]."/".$kf[0]["filename"] );
		if($t0==false) {
			JLog::log("user","resize failure:".$docid);
			return false;
		}
		
		JPDO::connect($_prevDsn);

		$t99=explode(".",$kf[0]["filename"]);
		$extension	= array_pop($t99);

		list($width, $height) = $t0;

		$newWidth  = $width;
		$newHeight = $height;
		$ratio     = $width / $height;

		while( !($maxW >= $newWidth &&  $maxH >=$newHeight)) {
			if( $newHeight > $maxH ){
				$newHeight	= $maxH;
				$newWidth  = $newHeight * $ratio;
			}
			else if( $newWidth > $maxW ){
				$newWidth	= $maxW;
				$newHeight = $newWidth / $ratio;
			}
		}

		$image_p 	= imagecreatetruecolor($newWidth, $newHeight);

		if(strpos($kf[0]["conType"], "gif")!==false)
			$image = imagecreatefromgif($storage["data"]["path"]."/".$kf[0]["filename"]);
		elseif(strpos($kf[0]["conType"], "png")!==false)
			$image = imagecreatefrompng($storage["data"]["path"]."/".$kf[0]["filename"]);
		else
			$image = imagecreatefromjpeg($storage["data"]["path"]."/".$kf[0]["filename"]);

		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

		imagejpeg($image_p, $storage["data"]["path"]."/".$docid.".".$maxW.".".$maxH.".jpg",self::$imQuality);

		return "http://".$storage["data"]["host"]."/".$storage["data"]["dpath"].$docid.".".$maxW.".".$maxH.".jpg";

	}

	/**
	 * thumber by type and id
	 *
	 * @return void
	 **/
	public static function thumb($maxw, $maxh, $type, $id, $subtype=null)
	{
		$selectQuery = "SELECT * FROM `".JDef::$doctable ."` WHERE `refId`=? AND `type`=?".(is_null($subtype)?"":" AND `subtype`=?");

		$storage = self::getStoragePlace($type,$subtype);

		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );
		$kf = JPDO::executeQuery($selectQuery,$id,$type,is_null($subtype)?"":$subtype);
		
		JPDO::revertDsn();

		return self::thumbById($kf["id"],$type,$maxw,$maxh);

	}

	/**
	 * clears the document file and conns
	 *
	 * @return bool
	 **/
	public static function clear($type,$id,$subtype=null,$docid=null)
	{
		$storage = self::getStoragePlace($type,$subtype);

		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );

		if(!is_null($docid)){
			$selectQuery = "SELECT * FROM `".JDef::$doctable ."` WHERE `id`=?";
			$kf = JPDO::executeQuery($selectQuery,$docid);

			$deleteQuery = "DELETE FROM `".JDef::$doctable ."` WHERE `id`=?";
			JPDO::executeQuery($deleteQuery,$docid);
			// echo $deleteQuery,$docid;
		}
		else{
			//delete all
			$selectQuery = "SELECT * FROM `".JDef::$doctable ."` WHERE `refId`=? AND `type`=?".(is_null($subtype)?"":" AND `subtype`=?");
			$kf = JPDO::executeQuery($selectQuery,$id,$type,(is_null($subtype)?"":$subtype));

			$deleteQuery = "DELETE FROM `".JDef::$doctable ."` WHERE `refId`=? AND `type`=?".(is_null($subtype)?"":" AND `subtype`=?");
			JPDO::executeQuery($deleteQuery,$id,$type,(is_null($subtype)?"":$subtype));
		}
		
		JPDO::revertDsn();

		//clear files
		if(sizeof($kf)>0)	{
			foreach ($kf as $kff0) {
				if(file_exists($storage["data"]["path"]."/".$kff0["filename"]))
					@unlink($storage["data"]["path"]."/".$kff0["filename"]);
			}
		}
		return true;
	}

	/**
	 * count nr of documents
	 *
	 * @return int
	 **/
	public static function count($type,$id,$subtype=null)
	{
		$selectQuery = "SELECT count(*) `cid` FROM `".JDef::$doctable ."` WHERE `refId`=? AND `type`=?".(is_null($subtype)?"":" AND `subtype`=?");

		$storage = self::getStoragePlace($type,$subtype);

		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );
		$kf = JPDO::executeQuery($selectQuery,$id,$type,is_null($subtype)?"":$subtype);
		
		JPDO::revertDsn();

		return intval($kf[0]["cid"]);
	}

	/**
	 * instantiate the object with id or ids
	 *
	 * @return object
	 **/
	public static function instant($docid)
	{
		self::$_ids = is_array($docid)?$docid:array($docid);

		return self;
	}

	/**
	 * move up the order
	 *
	 * @return void
	 **/
	public static function moveUp($type,$tag,$id,$upid=null)
	{
		$storage = self::getStoragePlace($type,$tag);
		$_prevDsn = JPDO::$dsn;
		

		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );

		$currentOrder = JPDO::executeQuery("SELECT `jorder`,`refId` FROM `".JDef::$doctable ."` WHERE id=?",$id);

		if(is_null($upid)){
			$selectUpperOrder = JPDO::executeQuery("SELECT `jorder` FROM `".JDef::$doctable ."`
													WHERE `type`=? AND refId=? AND `subtype`=? AND ?>jorder
													ORDER BY `jorder` DESC LIMIT 0,1", $type, $currentOrder[0]["refId"], $tag, $currentOrder[0]["jorder"]
													);
		}
		else
			$selectUpperOrder = JPDO::executeQuery("SELECT `jorder` FROM `".JDef::$doctable ."` WHERE id=?",$upid);


		JPDO::executeQuery("UPDATE `".JDef::$doctable."` SET `jorder`=?
							WHERE `refId`=? AND `jorder`=?",$currentOrder[0]["jorder"],$currentOrder[0]["refId"], $selectUpperOrder[0]["jorder"]
							);

		JPDO::executeQuery("UPDATE `".JDef::$doctable."` SET `jorder`=? WHERE `id`=?",$selectUpperOrder[0]["jorder"],$id);
		

		//we have to trigger the index, if exists
		$tempc = ucfirst($type);
		$tempc = new $tempc( $currentOrder[0]["refId"] );
		$tempc->checkIndex($tag);
		
		JPDO::connect($_prevDsn);
		
		unset($tempc);
	}

	/**
	 * filter self ids through the types and subtypes
	 *
	 * @return mixed
	 **/
	public static function filter($type,$subtype)
	{

		$selectQuery = "SELECT * FROM `".JDef::$doctable ."` WHERE `type`=? AND `subtype`=? AND `id` IN(".implode(",",self::$_ids).")";

		$storage = self::getStoragePlace($type,$subtype);

		JPDO::connect( JCache::read('JDSN.'.(isset($storage["data"]["dsn"])?$storage["data"]["dsn"]:"MDB")) );
		$kf = JPDO::executeQuery($selectQuery,$type,$subtype);
		$r0 = array();

		foreach ($kf as $data1) {
			//load ids
			$r0 = $data1["id"];
		}
		JPDO::revertDsn();

		return $r0;

	}

	public static function getStoragePath($type,$server,$p,$hibernate=false){
		$hrx = SiteCache::read( "storageServer.".$type.".".$server );
		$ret = $hrx[ $p ];

		if($hibernate)
			$ret = str_replace( "/","|",$ret );


		return $ret;
    }


	public static function getStoragePlace( $type, $subtype=null, $key=null ){
		$hrx = JCache::read("storageServer.".$type.(is_null($subtype)?"":"-".$subtype));
		
		if($hrx==false){
			$hrx = JCache::read("storageServer.default");
		}
		if(!is_array($hrx)) return false;

		$hrf = array_keys($hrx);

		$cnt = rand(0, sizeof($hrx)-1);

		//update here.. RANDOM for now..
		return array( "data"=>( $key==null ? $hrx[ $hrf[$cnt] ] : $hrx[ $hrf[$cnt] ][$key] ), "alias"=>$hrf[$cnt] );
	}

	/**
	 * return the selfname
	 *
	 * @return string
	 **/
	public function selfname()
	{
		return self::$selfname;
	}

	/**
	 * return _ids
	 *
	 * @return array
	 **/
	public function ids()
	{
		return self::$_ids;
	}

	/**
	 * thumber for any image
	 *
	 * @return string
	 **/
	public static function thumber($path,$maxW,$maxH)
	{
		global $__DP;
		//check for cache
		$t98=explode("/",$path);
		$t99=explode(".",$path);
		$extension	= array_pop($t99);
		$fileName 	= array_pop($t98);
		$filehash 	= md5($path.$maxW.$maxH);

		if(is_file($__DP."/temp/output/".$filehash.".".$extension) || is_file(strtolower($__DP."/temp/output/".$filehash.".".$extension)))
			return JUtil::siteAddress()."temp/output/".$filehash.".".strtolower($extension);


		if(!is_file($path) && strpos($path,"http:")===false) return "";

		$t0 = @getimagesize( $path );
		// print_r($t0);

		if($t0==false) {
			JLog::log("user","jutil resize failure:".$path);
			return false;
		}

		list($width, $height) = $t0;
		$mime = $t0["mime"];

		$newWidth  = $width;
		$newHeight = $height;
		$ratio     = $width / $height;

		while( !($maxW >= $newWidth &&  $maxH >=$newHeight)) {
			if( $newHeight > $maxH ){
				$newHeight	= $maxH;
				$newWidth  = $newHeight * $ratio;
			}
			else if( $newWidth > $maxW ){
				$newWidth	= $maxW;
				$newHeight = $newWidth / $ratio;
			}
		}

		$image_p 	= imagecreatetruecolor($newWidth, $newHeight);
		// echo stripos($extension, "gif")."-".$extension;
		if(!(stripos($mime, "gif")===false))
			$image = imagecreatefromgif($path);
		elseif(!(stripos($mime, "png")===false))
			$image = imagecreatefrompng($path);
		else
			$image = @imagecreatefromjpeg($path);

		imagecopyresampled($image_p, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
		imagejpeg($image_p, $__DP."/temp/output/".$filehash.".jpg" );

		return JUtil::siteAddress()."temp/output/".$filehash.".jpg";//.strtolower($extension);
	}
	
} // END class JDoc
