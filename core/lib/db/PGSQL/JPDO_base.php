<?php
class JPDO_base{
	/**
	 * used for integer time of now
	 * @var string
	 */
	public static $unix_timestamp = "EXTRACT(epoch FROM now())::INT";

	public static $localeDefDarwin = 'en_US.UTF-8';
	public static $localeDef = 'en_US.utf8';
	/**
	 * Make a connection with a database using PDO Object.
	 *
	 */
	public static function connect($conf, $logging=false, $forced=false ) {
		if(!is_array($conf)){

			if($logging==false) {
				JLog::log("cri",'Buggy dsn?: ' . serialize($conf));
			}
			return false;
		}
		else

			if(isset($conf["dsn"])){
				$label = isset($conf[ "label" ])?$conf[ "label" ]:$conf[ "dsn" ];


				// forced connection
				if ( !$forced && static::$label == $label ) {
					static::$connection = static::$connections[ $label ][ "connection" ];

					return true;
				}

				// same connection
				if(array_key_exists( $label, static::$connections )){
					static::$connection = static::$connections[$label]["connection"];
					static::$dsn        = $conf['dsn'];
					static::$username   = $conf['user'];
					static::$password   = $conf['pass'];
					static::$label      = $label;
					return true;
				}

				// all new
				if(!is_null(static::$dsn))
					static::$prevDsn = array( "dsn"=>static::$dsn, "user"=>static::$username, "pass"=>static::$password );

				static::$dsn 		= $conf['dsn'];
				static::$username   = $conf['user'];
				static::$password	= $conf['pass'];
				static::$label	    = $label;

			}
		// elseif(!JUtil::isProduction())
		// 	die("buggy query");

		try {
			$pdoConnect = new PDO( static::$dsn, static::$username, static::$password );
			$pdoConnect->setAttribute( PDO::ATTR_TIMEOUT, 5000 );
//			$pdoConnect->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);


			static::$connection = $pdoConnect; // & ?
			static::$connections[ $label ] = array( "connection"=>$pdoConnect, "transaction"=>0);

			static::transactConnection();
			return true;
		} catch (PDOException $e) {
			if(!JUtil::isProduction()) {
				echo $e->getMessage().'<br />';
				die();
			}
			JLog::log("cri",'Connection failed: ' . $e->getMessage());
			JUtil::siteHalt("PDOC");
			// exit;
			return false;
		}
	}


	/**
	 * check if table exists
	 * params : database, schema, table
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function tableExists( $table, $database, $schema ) {
		$k = static::executeQuery( "
			SELECT EXISTS(
				SELECT 1 FROM information_schema.tables
            	WHERE
            		table_catalog='$database' AND
                    table_schema='$schema' AND
                    table_name='$table'
            )
		" );
		return $k[0]["exists"] == 1;
	}

	/**
	 * truncate
	 * @param $schema
	 * @param $table
	 * @author Özgür Köy
	 */
	public static function truncate( $schema, $table){
//		echo 'DELETE FROM "'.$schema.'"."'.$table.'"' ;
//		echo 'TRUNCATE "'.$schema.'"."'.$table.'" RESTART IDENTITY CASCADE';
//		static::executeQuery( 'TRUNCATE "'.$schema.'"."'.$table.'" RESTART IDENTITY CASCADE' );
		static::executeQuery( 'DELETE FROM "'.$schema.'"."'.$table.'"' );
	}

	/**
	 * check if data type exists
	 * params : typeName
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function typeExists( $typeName ) {
		echo "SELECT EXISTS (SELECT 1 FROM pg_type WHERE typname = '$typeName');";
		$k = static::executeQuery( "
			SELECT EXISTS (SELECT 1 FROM pg_type WHERE typname = '$typeName');
		" );
		return $k[0]["exists"] == 1;
	}

	/**
	 * Last insert id
	 *
	 */
	public static function lastId($schema, $table) {
		if ( static::getConnection() ) {
//			if ( static::$isTransAction != 1 )
			$r = static::getConnection()->lastInsertId( '"' . $schema . '"."' . $table . '_id_seq"' );

			if($r !== false)
				return $r;
			else{
				static::$returnError=true;
				$lastId = static::executeQuery( "SELECT lastval() AS last" );
				static::$returnError=false;
				if(!is_array($lastId)){
					#possible error
					return false;
				}
				else
					return $lastId[0]["last"];
			}
		}
	}

	/**
	 * get table indexes
	 *
	 * @return mixed
	 * @author Özgür Köy
	 **/
	public static function listIndexes($table, $schema=null)
	{
		$recordset = static::executeQuery(
			"
			SELECT
			regexp_replace(regexp_matches(indexdef,'.+?\(\"?(.+?)\"?\)')::TEXT,'[{,}]','','g') AS \"Column_name\",
			regexp_replace(regexp_matches(indexdef,'CREATE.+?INDEX \"?(.+?)\"? ON')::TEXT,'[{,}]','','g') AS \"Key_name\",
			(strpos(indexdef,'UNIQUE')>0)::INT AS \"Non_unique\"
			FROM pg_indexes WHERE tablename = '$table'
			"
		);

		if(!sizeof($recordset)>0) return array();

		return $recordset;
	}

	/**
	 * list table's fields
	 *
	 * @return array
	 * @author Özgür Köy
	 **/
	public static function listFields( $table, $schema = null ) {
		$fieldNames = array();

		$recordset = static::executeQuery( "
		SELECT \"column_name\" AS \"Field\",
		CASE WHEN \"data_type\"='character varying' THEN upper(\"data_type\"||'('||\"character_maximum_length\"||')')
		ELSE upper(\"data_type\")
		END AS \"Type\"
		FROM information_schema.columns
		WHERE table_schema = '$schema'
		AND table_name   = '$table'
		"
		);
		if ( !sizeof( $recordset ) > 0 || $recordset === false ) return false;

		foreach ( $recordset as $field )
			$fieldNames[ $field[ 'Field' ] ] = $field[ "Type" ];

		return $fieldNames;
	}

	public static function prepareQuery( &$query ) {
		$query = str_replace( "`", '"', $query );
	}

	public static function analyseError( $error ) {
		return $error[ 2 ];
	}

	public static function resetSequence( $schema, $table ) {
		$q = "SELECT setval(pg_get_serial_sequence('\"$schema\".\"$table\"', 'id'),
		(SELECT MAX(id) FROM \"$schema\".\"$table\"))";

		static::executeQuery( $q );
	}

	public static function getLocaleString() {
		$colString = "localeDef" . PHP_OS;
		$colString = property_exists( "JPDO", $colString ) ? $colString : "localeDef";
		$v = new ReflectionProperty( "JPDO", $colString );

		return $v->getValue();
	}
}