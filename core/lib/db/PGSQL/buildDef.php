<?php
/**
 * build definition class, sqls etc
 *
 * @package default
 * @author  Özgür Köy
 **/
class BuildDef extends JDef
{

	/**
	 * Field types of no collation
	 *
	 * @var array
	 */
	public $noCollationFieldTypes = array(
		"SMALLINT",
		"NUMERIC",
		"INTEGER",
		"BOOLEAN",
		"DATE",
		"DATETIME",
		"FLOAT",
		"JSON",
		"BIGINT"
	);

	/**
	 * reserved fields.
	 *
	 * @var array
	 **/
	public $reservedFields = array( "id", "jdate", "log", "logEvents", "sessions" );


	/**
	 * reserved keywords , mysql and php
	 *
	 * @var array
	 **/
	public $reservedKeywords = array(
		"DATA",
		"ACCESSIBLE",
		"ADD",
		"ALL",
		"ALTER",
		"TAG",
		"ANALYZE",
		"AND",
		"AS",
		"ASC",
		"ASENSITIVE",
		"BEFORE",
		"BETWEEN",
		"BIGINT",
		"BINARY",
		"BLOB",
		"BOTH",
		"BY",
		"CALL",
		"CASCADE",
		"CASE",
		"CHANGE",
		"CHAR",
		"CHARACTER",
		"CHECK",
		"COLLATE",
		"COLUMN",
		"CONDITION",
		"CONSTRAINT",
		"CONTINUE",
		"CONVERT",
		"CREATE",
		"CROSS",
		"CURRENT_DATE",
		"CURRENT_TIME",
		"CURRENT_TIMESTAMP",
		"CURRENT_USER",
		"CURSOR",
		"DATABASE",
		"DATABASES",
		"DAY_HOUR",
		"DAY_MICROSECOND",
		"DAY_MINUTE",
		"DAY_SECOND",
		"DEC",
		"DECIMAL",
		"DECLARE",
		"DEFAULT",
		"DELAYED",
		"DELETE",
		"DESC",
		"DESCRIBE",
		"DETERMINISTIC",
		"DISTINCT",
		"DISTINCTROW",
		"DIV",
		"DOUBLE",
		"DROP",
		"DUAL",
		"EACH",
		"ELSE",
		"ELSEIF",
		"ENCLOSED",
		"ESCAPED",
		"EXISTS",
		"EXIT",
		"EXPLAIN",
		"FALSE",
		"FETCH",
		"FLOAT",
		"FLOAT4",
		"FLOAT8",
		"FOR",
		"FORCE",
		"FOREIGN",
		"FROM",
		"FULLTEXT",
		"GET",
		"GRANT",
		"GROUP",
		"HAVING",
		"HIGH_PRIORITY",
		"HOUR_MICROSECOND",
		"HOUR_MINUTE",
		"HOUR_SECOND",
		"IF",
		"IGNORE",
		"IN",
		"INDEX",
		"INFILE",
		"INNER",
		"INOUT",
		"INSENSITIVE",
		"INSERT",
		"INT",
		"INT1",
		"INT2",
		"INT3",
		"INT4",
		"INT8",
		"INTEGER",
		"INTERVAL",
		"INTO",
		"IO_AFTER_GTIDS",
		"IO_BEFORE_GTIDS",
		"IS",
		"ITERATE",
		"JOIN",
		"KEY",
		"KEYS",
		"KILL",
		"LEADING",
		"LEAVE",
		"LEFT",
		"LIKE",
		"LIMIT",
		"LINEAR",
		"LINES",
		"LOAD",
		"LOCALTIME",
		"LOCALTIMESTAMP",
		"LOCK",
		"LONG",
		"LONGBLOB",
		"LONGTEXT",
		"LOOP",
		"LOW_PRIORITY",
		"MASTER_BIND",
		"MASTER_SSL_VERIFY_SERVER_CERT",
		"MATCH",
		"MAXVALUE",
		"MEDIUMBLOB",
		"MEDIUMINT",
		"MEDIUMTEXT",
		"MIDDLEINT",
		"MINUTE_MICROSECOND",
		"MINUTE_SECOND",
		"MOD",
		"MODIFIES",
		"NATURAL",
		"NOT",
		"NO_WRITE_TO_BINLOG",
		"NULL",
		"NUMERIC",
		"ON",
		"OPTIMIZE",
		"OPTION",
		"OPTIONALLY",
		"OR",
		"ORDER",
		"OUT",
		"OUTER",
		"OUTFILE",
		"PARTITION",
		"PRECISION",
		"PRIMARY",
		"PROCEDURE",
		"PURGE",
		"RANGE",
		"READ",
		"READS",
		"READ_WRITE",
		"REAL",
		"REFERENCES",
		"REGEXP",
		"RELEASE",
		"RENAME",
		"REPEAT",
		"REPLACE",
		"REQUIRE",
		"RESIGNAL",
		"RESTRICT",
		"RETURN",
		"REVOKE",
		"RIGHT",
		"RLIKE",
		"SCHEMA",
		"SCHEMAS",
		"SECOND_MICROSECOND",
		"SELECT",
		"SENSITIVE",
		"SEPARATOR",
		"SET",
		"SHOW",
		"SIGNAL",
		"SMALLINT",
		"SPATIAL",
		"SPECIFIC",
		"SQL",
		"SQLEXCEPTION",
		"SQLSTATE",
		"SQLWARNING",
		"SQL_BIG_RESULT",
		"SQL_CALC_FOUND_ROWS",
		"SQL_SMALL_RESULT",
		"SSL",
		"STARTING",
		"STRAIGHT_JOIN",
		"TABLE",
		"TERMINATED",
		"THEN",
		"TINYBLOB",
		"TINYINT",
		"TINYTEXT",
		"TO",
		"TRAILING",
		"TRIGGER",
		"TRUE",
		"UNDO",
		"UNION",
		"UNIQUE",
		"UNLOCK",
		"UNSIGNED",
		"UPDATE",
		"USAGE",
		"USE",
		"USING",
		"UTC_DATE",
		"UTC_TIME",
		"UTC_TIMESTAMP",
		"VALUES",
		"VARBINARY",
		"VARCHAR",
		"VARCHARACTER",
		"VARYING",
		"WHEN",
		"WHERE",
		"WHILE",
		"WITH",
		"WRITE",
		"XOR",
		"YEAR_MONTH",
		"ZEROFILL",
		"stdClass",
		"Exception",
		"ErrorException",
		"php_user_filter",
		"Closure",
		"Generator",
		"self",
		"static",
		"parent",
		"class",
		"__halt_compiler",
		"abstract",
		"and",
		"array",
		"as",
		"break",
		"callable",
		"case",
		"catch",
		"class",
		"clone",
		"const",
		"continue",
		"declare",
		"default",
		"die",
		"do",
		"echo",
		"else",
		"elseif",
		"empty",
		"enddeclare",
		"endfor",
		"endforeach",
		"endif",
		"endswitch",
		"endwhile",
		"eval",
		"exit",
		"extends",
		"final",
		"for",
		"foreach",
		"function",
		"global",
		"goto",
		"if",
		"implements",
		"include",
		"include_once",
		"instanceof",
		"insteadof",
		"interface",
		"isset",
		"list",
		"namespace",
		"new",
		"or",
		"print",
		"private",
		"protected",
		"public",
		"require",
		"require_once",
		"return",
		"static",
		"switch",
		"throw",
		"trait",
		"try",
		"unset",
		"use",
		"var",
		"while",
		"xor",
		"__CLASS__",
		"__DIR__",
		"__FILE__",
		"__FUNCTION__",
		"__LINE__",
		"__METHOD__",
		"__NAMESPACE__",
		"__TRAIT__"
	);

	/**
	 * available indexes
	 *
	 * @var array
	 **/
	public $indexes = array(
		"index",
		"unique"
	);

	/**
	 * connection strings used in yamls
	 *
	 * @var array
	 **/
	public $connectionStrings = array(
		"has a"       => array( "label" => "1to1", "fieldType" => "INTEGER" ),
		"has many"    => array( "label" => "1toN", "fieldType" => "INTEGER", "reverse" => true ),
		"has lots of" => array( "label" => "NtoN", "fieldType" => null )
	);


	/**
	 * field types
	 *
	 * @var array
	 */
	public $fieldTypes = array(
		"date"     => "DATE",
		"dateTime" => "TIMESTAMP WITHOUT TIME ZONE",
		"char-5"   => "CHARACTER VARYING(5)",
		"char-10"  => "CHARACTER VARYING(10)",
		"char-40"  => "CHARACTER VARYING(40)",
		"char-100" => "CHARACTER VARYING(100)",
		"char-200" => "CHARACTER VARYING(200)",
		"char-255" => "CHARACTER VARYING(255)",
		"char-400" => "CHARACTER VARYING(400)",
		"int"      => "NUMERIC",
		"integer"  => "INTEGER",
		"bool"     => "BOOLEAN",
		"float"    => "NUMERIC",
		"bigint"   => "BIGINT",
		"file"     => null,
		"select"   => null,
		"enum"     => null,
		"editable" => "TEXT",
		"json"     => "JSON",
		"text"     => "TEXT",
		"tinyint1" => "SMALLINT"
	);

	public $sqls = array(
		"createDB"           => 'CREATE SCHEMA IF NOT EXISTS "%s"',
		"postCreateDB"       => 'CREATE COLLATION "%s"."|COLLATE|" ( locale = \'|COLLATE|\' );',
		"createTable"        => '
				CREATE TABLE IF NOT EXISTS "%s"."%s" (
				id Serial NOT NULL ,
				jdate NUMERIC,
				PRIMARY KEY (id)
			)',
		"addField"           => 'ALTER TABLE "%s"."%s" ADD COLUMN "%s" %s %s NULL %s',
		"editField"          => 'ALTER TABLE "%s"."%s" ALTER COLUMN  "%s" TYPE %s %s',
		"changeFieldDefault" => 'ALTER TABLE "%s"."%s" ALTER COLUMN  "%s" SET %s',
		"dropField"          => 'ALTER TABLE "%s"."%s" DROP COLUMN %s',
		"enumCreation"       => 'CREATE TYPE "%s"."%s" AS %s',
		"enumAddValue"       => 'ALTER TYPE "%s"."%s" ADD VALUE IF NOT EXISTS \'%s\'',
		"getFieldDefault"    => "SELECT adsrc AS default
							  	FROM pg_attrdef pad, pg_attribute pat, pg_class pc
								WHERE pc.relname='%s'
							    AND pc.oid=pat.attrelid AND pat.attname='%s'
							    AND pat.attrelid=pad.adrelid AND pat.attnum=pad.adnum",
		"collation"          => 'COLLATE "%s"."|COLLATE|"',
		"logTable"           => '
				CREATE TABLE IF NOT EXISTS "%s"."%s" (
				id Serial NOT NULL,
				log text NULL,
				type CHARACTER VARYING(200) NULL,
				level SMALLINT NULL,
				"eventId" SMALLINT NULL,
				"dateAdded" TIMESTAMP WITHOUT TIME ZONE  NULL,
				PRIMARY KEY (id)
			)',
		"logEventTable"      => '
				CREATE TABLE IF NOT EXISTS "%s"."%s" (
				id Serial NOT NULL,
				"eventName" CHARACTER VARYING( 220 ) COLLATE "%s"."|COLLATE|"  NULL,
				"startDate" TIMESTAMP WITHOUT TIME ZONE  NULL,
				"endDate" TIMESTAMP WITHOUT TIME ZONE  NULL,
				PRIMARY KEY (id)
			) ',
		"sessionsTable"      => '
				CREATE TABLE IF NOT EXISTS "%s"."%s" (
				id CHARACTER VARYING(220)  NOT NULL,
				sdata text NOT NULL,
				"lastModified" NUMERIC NOT NULL,
				"sessionsCustomData" CHARACTER VARYING(220)  NULL,
				PRIMARY KEY (id)
			)',
		"createJOTable"      => "CREATE TABLE IF NOT EXISTS \"%s\".\"%s\" (
				\"%s\" INTEGER,
				\"%s\" INTEGER,
				\"tag\" CHARACTER VARYING(100),
				\"uniqueID\" CHARACTER VARYING(200) UNIQUE,
				\"jorder\" NUMERIC NOT NULL DEFAULT  '1',
				\"jdate\" NUMERIC,
				\"dateAdded\" TIMESTAMP WITHOUT TIME ZONE NULL
			) ",
		"getAllFK"           => "
				SELECT tc.constraint_name AS \"CONSTRAINT_NAME\",
				kcu.column_name AS \"COLUMN_NAME\",
				ccu.table_name AS \"REFERENCED_TABLE_NAME\",
				ccu.column_name AS \"REFERENCED_COLUMN_NAME\"
				FROM information_schema.table_constraints  tc
				JOIN information_schema.key_column_usage kcu ON tc.constraint_name = kcu.constraint_name
				JOIN information_schema.constraint_column_usage ccu ON ccu.constraint_name = tc.constraint_name
				WHERE constraint_type = 'FOREIGN KEY' AND tc.table_schema='%s' AND tc.table_name= '%s'
		",
		"findTargetFK"       => '!!!TODO!!!',
		"addIndex"           => 'CREATE INDEX "j_%s" ON "%s"."%s" USING btree( "%s" ASC )',
		"addIndexText"       => 'CREATE INDEX "j_%s" ON "%s"."%s" ( "%s" ASC )',
		"uniqueIndex"        => 'CREATE UNIQUE INDEX "j_%s" ON "%s"."%s" USING btree( "%s" ASC NULLS LAST );',
		"uniqueIndexText"    => 'CREATE UNIQUE INDEX "j_%s" ON "%s"."%s" ( "%s" ASC NULLS LAST );',
//		"uniqueIndex"        => 'ALTER TABLE  "%s"."%s" ADD CONSTRAINT ju_%s UNIQUE( "%s" )',
		"dropIndex"          => 'DROP INDEX IF EXISTS "%s"."%s"',
		"updateFK"           => '
			ALTER TABLE "%s"."%s"
			ADD CONSTRAINT "%s"
			FOREIGN KEY ("%s") REFERENCES "%s"."%s"("%s")
			ON UPDATE CASCADE
			ON DELETE SET NULL
			DEFERRABLE INITIALLY DEFERRED
			',
		"deleteFK"           => '
			ALTER TABLE "%s"."%s"
			ADD CONSTRAINT "%s"
			FOREIGN KEY ("%s") REFERENCES "%s"."%s"("%s")
			ON UPDATE CASCADE
			ON DELETE CASCADE
			DEFERRABLE INITIALLY DEFERRED
			',
		"dropColumnFK"       => 'ALTER TABLE "%s"."%s" DROP CONSTRAINT IF EXISTS "%s"',
		"innerJoin"          => "/*jpl*/INNER JOIN/*ejpl*/ `%s`.`%s` AS `%s` ON `%s`.`%s`=`%s`.`%s`",
		"N2NConnection"      => "
		INNER JOIN
		(
			SELECT DISTINCT `%s`.`%s`
			FROM `%s`.`%s` `%s`
			/*jpl*/INNER JOIN/*ejpl*/ `%s`.`%s` AS `%s` ON `%s`.id = `%s`.`%s`
			|JOINS_%s|
			WHERE |CLAUSES_%s| `%s`.tag=\'%s\'
		) AS `%s` ON `%s`.`%s` = `%s`.id",
		"minInt"             => "SMALLINT",
		"bool"               => "SMALLINT",

	);

	/**
	 * get sql
	 *
	 * @param string $sql
	 * @return string
	 * @author Özgür Köy
	 */
	public function sql() {
		$args  = func_get_args();
		$sql   = array_shift( $args );
		$query = "";

		if ( isset( $this->sqls[ $sql ] ) ) {
			if ( in_array( $sql, array( "addIndex", "addIndexText", "uniqueIndex", "uniqueIndexText" ) ) ) {
				/*
				 * IN MYSQL
				 * 0 database
				 * 1 table
				 * 2 indexname
				 * 3 field
				*/
				$args0 = array(
					$args[ 2 ],
					$args[ 0 ],
					$args[ 1 ],
					$args[ 3 ],
				);

				$args = $args0;
			}
			elseif ( $sql == "dropIndex" ) {
				/*
				 * IN MYSQL
				 * 0 database
				 * 1 table
				 * 2 index
				*/
				$args0 = array(
					$args[ 0 ],
					$args[ 2 ],
				);
				$args  = $args0;
			}
			elseif ( $sql == "getFieldDefault" ) {
				/*
				 * IN MYSQL
				 * 0 field
				 * 1 table
				*/
				$args0 = array(
					$args[ 1 ],
					$args[ 0 ]
				);
				$args  = $args0;
			}
			elseif ( $sql == "createJOTable" ) {
				array_unshift( $args, $args[ 3 ] ); #add the schema to header
			}
			elseif ( in_array( $sql, array( "dropField", "editField", "addField", "enumCreation" ) ) ) {
				/*
				 * IN MYSQL - same
				*/

				if ( $sql == "addField" && strpos( $args[ 3 ], "." ) !== false ) {
					$t0        = explode( ".", $args[ 3 ] );
					$args[ 3 ] = '"' . $t0[ 0 ] . '"."' . $t0[ 1 ] . '"'; #for user types
					unset( $t0 );
				}

				if ( in_array( $sql, array( "editField", "enumCreation" ) ) )
					unset( $args[ 3 ] );

			}


			array_unshift( $args, $this->sqls[ $sql ] );
			$query .= call_user_func_array( 'sprintf', $args );

			#collate fix
			if(strpos($query,"|COLLATE|")!==false){
				$localeString = JPDO::getLocaleString();
				$query = str_replace( "|COLLATE|", $localeString,$query );
			}
			return $query;
		}
		else
			return false;
	}

} // END class BuildDef
?>