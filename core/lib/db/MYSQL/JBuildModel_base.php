<?php
class JBuildModel_base{
	public static function remoteConnections( &$inf ) {

		foreach ( $inf[ "remote" ] as $tag => $remote ) {
			// manually typing the values, for loop no likes
			echo '
		"' . $tag . '"=>array(
					"join"      =>"' . $remote[ "join" ] . '",
					"table"     =>"' . $remote[ "table" ] . '",
					"tableName" =>"' . $remote[ "tableName" ] . '",
					"type"      =>"' . $remote[ "type" ] . '",
					"defClause" =>\'\',
					"clause"    =>"' . $remote[ "clause" ] . '"
					),';

		}
		echo PHP_EOL . '
	);';
	}

	public static function assetOutput( $field ){
		return "`$field`";
	}

	public static function aposArray($ar){
		return JUtil::aposArray($ar, '`') ;
	}
}