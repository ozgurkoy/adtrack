<?php
class JPDO_base{
	/**
	 * used for integer time of now
	 * @var string
	 */
	public static $unix_timestamp = "UNIX_TIMESTAMP()";

	/**
	 * Make a connection with a database using PDO Object.
	 *
	 */
	public static function connect($conf,$logging=false,$forced=false) {
		if(!is_array($conf)){

			if($logging==false) {
				JLog::log("cri",'Buggy dsn?: ' . serialize($conf));
			}
			return false;
		}
		else

			if(isset($conf["dsn"])){
				// forced connection
				if(!$forced && static::$dsn == $conf["dsn"]){
					static::$connection = static::$connections[static::$dsn]["connection"];
					return true;
				}

				// same connection
				if(array_key_exists( $conf["dsn"], static::$connections )){
					static::$connection = static::$connections[$conf["dsn"]]["connection"];
					static::$dsn        = $conf['dsn'];
					static::$username   = $conf['user'];
					static::$password   = $conf['pass'];

					return true;
				}

				// all new
				if(!is_null(static::$dsn))
					static::$prevDsn = array("dsn"=>static::$dsn,"user"=>static::$username,"pass"=>static::$password);

				static::$dsn 		= $conf['dsn'];
				static::$username   = $conf['user'];
				static::$password	= $conf['pass'];

			}
		// elseif(!JUtil::isProduction())
		// 	die("buggy query");

		try {
			$pdoConnect = new PDO(static::$dsn, static::$username, static::$password);
			$pdoConnect->setAttribute( PDO::ATTR_TIMEOUT, 5000 );
			$pdoConnect->query('SET NAMES UTF8');

			static::$connection = $pdoConnect;
			static::$connections[static::$dsn] = array( "connection"=>$pdoConnect, "transaction"=>0);

			static::transactConnection();
			return true;
		} catch (PDOException $e) {
			if(!JUtil::isProduction()) {
				echo $e->getMessage().'<br />';
				die();
			}
			JLog::log("cri",'Connection failed: ' . $e->getMessage());
			JUtil::siteHalt("PDOC");
			// exit;
			return false;
		}

	}

	/**
	 * check if table exists
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function tableExists($table, $database=null, $schema=null)
	{
		static::executeQuery( "SHOW TABLE STATUS LIKE '$table'" );
		return static::$effected>0;
	}

	/**
	 * truncate
	 * @param $database
	 * @param $table
	 * @author Özgür Köy
	 */
	public static function truncate( $database, $table){
		static::executeQuery( 'TRUNCATE `'.$database.'`.`'.$table.'`' );
	}

	/**
	 * get table indexes
	 *
	 * @return mixed
	 * @author Özgür Köy
	 **/
	public static function listIndexes($table)
	{
		$recordset = static::executeQuery("SHOW INDEX FROM `$table`");
		if(!sizeof($recordset)>0) return array();

		return $recordset;
	}


	/**
	 * list table's fields
	 *
	 * @return array
	 * @author Özgür Köy
	 **/
	public static function listFields( $table, $schema = null ) {
		$fieldNames = array();
		$recordset = static::executeQuery( "SHOW COLUMNS FROM $table" );

		if ( !is_array($recordset) || !(sizeof( $recordset ) > 0) ) return false;

		foreach ( $recordset as $field )
			$fieldNames[ $field[ 'Field' ] ] = $field[ "Type" ];

		return $fieldNames;
	}


	public static function prepareQuery( &$query ) {
		//ouch nothing.
	}

	/**
	 * Last insert id
	 *
	 */
	public static function lastId() {
		if (static::getConnection()) {
			return static::getConnection()->lastInsertId();
		}
		return false;
	}

	public static function analyseError( $error ) {
		return $error[ 2 ];
	}

	public static function resetSequence( $schema, $table ) {
		// nono thank you
	}

}