<?php
class JMT_base{

	/**
	 * prepare query for execution
	 *
	 * @param object $ins
	 * @return void
	 * @author Özgür Köy
	 */
	public static function prepare(JModel &$ins)
	{
		// king
		$str   = array("SELECT ".($ins->_useDistinct?"DISTINCT ":""));
		$tableName = is_null( $ins->selfnameOverride ) ? $ins->selfname : $ins->selfnameOverride;

		if ( $ins->groupOperation !== true ) {
			$str[] = implode(",", $ins->_fieldsToLoad);
		}

		// extra fields
		$str[] = sizeof( $ins->_extraFields ) > 0 ? ( ( $ins->groupOperation === true ? "" : "," ) . implode( ",", $ins->_extraFields ) ) : "";
		// main table
		$str[] = "FROM ".$ins->dbName.".".$tableName." AS ".$tableName;
		// join join join
		$str[] = implode(PHP_EOL,$ins->_joins);
		// clauses
		if ( !is_null( $ins->customClause ) ) {
			$str[ ] = "WHERE (" . $ins->customClause . ")";
		}
		elseif ( sizeof( $ins->_clauses ) > 0 || sizeof( $ins->extraClause )>0 ) {
			$str[ ] = "WHERE " . implode( " AND ", $ins->_clauses );
			if(sizeof( $ins->extraClause )>0)
				$str[ ] = "AND ({$ins->extraClause})";
		}
		// group by?
		$str[] = strlen($ins->_groupBy)>0?"GROUP BY ".$ins->_groupBy:"";
		// having clauses
		if($ins->_having)
			$str[] = "HAVING ".$ins->_havingClause;
		// order by?
		if($ins->_orderBy == "RANDOM" )
			$ins->_orderBy = "RAND()";

		$str[] = "ORDER BY ".(strlen($ins->_orderBy)>0?$ins->_orderBy:"{$tableName}.id ASC");
		// limit
		if ( $ins->groupOperation !== true ) {
			$str[] = "LIMIT {$ins->limitLow},{$ins->limitCount}";
		}

		// echo implode("\n",$str);
		$str = implode("\n",$str);

		/*if($ins->_doLeftJoin === true)
			$str = str_replace("INNER JOIN", "LEFT JOIN",$str);*/

		if ( $ins->_doLeftJoin === true ){
			foreach($ins->_leftJoins as $lef){
				$str = preg_replace('|/\*jpl\*/INNER JOIN/\*ejpl\*/ (`.+?`\.`.+?`) AS `'.$lef.'`|',
					"LEFT JOIN $1 AS `$lef`",
					$str
				);
			}

			if(sizeof($ins->_leftJoins)==0){
				$str = str_replace( "INNER JOIN", "LEFT JOIN", $str );
			}
		}

		#clear joins.
		$str = preg_replace( '/\|(JOINS|CLAUSES)_.+?\|/', "", $str );

		if ( $ins->_lockOnSelect )
			$str .= " FOR UPDATE";

		$ins->_query = $str;
		//echo $ins->_query;
	}

	/**
	 * @param JModel $ins
	 * @param int    $jdate
	 * @return bool
	 *
	 * update jdate
	 */
	public static function updateJDate( JModel &$ins, $jdate ){
		if($ins->id>0){
			$sql = "UPDATE `{$ins->selfname}` SET `jdate`=$jdate WHERE id=".$ins->id;

			JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
			$r0 = JPDO::executeQuery($sql);
		}
		return false;
	}

	/**
	 * @param JModel $ins
	 * @param array  $args
	 * @param array  $values
	 * @return JModel update functionality
	 */
	public static function update( JModel &$ins, array &$args, array &$values, $limit=null ) {
		$updateQuery = "UPDATE `".$ins->dbName."`.`".$ins->selfname."` SET ";
		if(!is_null($limit))
			$ins->limit( 0, $limit );

		$ins->lockOnSelect();

		static::load( $ins, $args, false, $ins->_rootClauseValues, true );
		$nq = $ins->_query;

		if ( is_null( $limit ) )
			$nq = preg_replace( "/LIMIT [0-9]+?,[0-9]+/", "", $ins->_query ); #remove limit

		$nq = preg_replace( "/SELECT(.+?)FROM/s", "SELECT `{$ins->selfname}`.id FROM", $nq ); #remove unnecessary fields
		$nq = preg_replace( "/ORDER BY.+?(ASC|DESC)/s", "", $nq ); #remove order by

		$t0 = "";
		foreach ( $values as $f0 => $v0 ) {
			if(in_array($f0, $ins->_fields)){
				array_unshift( $ins->_clauseValues, $v0 );
				$t0 =  "`".$f0."`=?,".$t0;
			}
		}

		$updateQuery = substr($updateQuery.$t0,0,-1)." WHERE id IN( SELECT id FROM ({$nq}) jTmp )";
		array_unshift($ins->_clauseValues, null);
//		echo $updateQuery;
//		var_dump( $ins->_clauseValues );exit;

		JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
		static::checkNameOverride($ins, $updateQuery);
		JPDO::executeQuery(static::checkNameOverride($ins, $updateQuery), $ins->_clauseValues);

		$ins->lockOnSelect(false);

		return $ins;
	}

	/**
	 * @param JModel $ins
	 * @param array  $args
	 * @param array  $values
	 * @return JModel BIG delete functionality
	 */
	public static function deleteWFilter( JModel &$ins, array &$args, $limit=null ) {
		$delQuery = "DELETE FROM `".$ins->dbName."`.`".$ins->selfname."`  ";
		if(!is_null($limit))
			$ins->limit( 0, $limit );

		$ins->lockOnSelect();

		static::load( $ins, $args, false, $ins->_rootClauseValues, true );
		$nq = $ins->_query;

		if ( is_null( $limit ) )
			$nq = preg_replace( "/LIMIT [0-9]+?,[0-9]+/", "", $ins->_query ); #remove limit

		$nq = preg_replace( "/SELECT(.+?)FROM/s", "SELECT `{$ins->selfname}`.id FROM", $nq ); #remove unnecessary fields
		$nq = preg_replace( "/ORDER BY.+?(ASC|DESC)/s", "", $nq ); #remove order by

		$delQuery = " WHERE id IN( SELECT id FROM ({$nq}) jTmp )";
		echo $delQuery;exit;
		array_unshift($ins->_clauseValues, null);
//		echo $updateQuery;
//		var_dump( $ins->_clauseValues );exit;

		JPDO::connect( JCache::read('JDSN.'.$ins->dsn) );
		static::checkNameOverride($ins, $delQuery);
		JPDO::executeQuery(static::checkNameOverride($ins, $delQuery), $ins->_clauseValues);

		$ins->lockOnSelect(false);

		return $ins;
	}

	/**
	 * add group by field field
	 *
	 * @param JModel $ins
	 * @param string $field
	 * @param string $label
	 * @return object
	 * @author Özgür Köy
	 */
	public static function groupByField(JModel &$ins, $type, $field, $label=null)
	{
		if ( is_null( $label ) ) $label = $field;

		if(strpos($field,".")===false)
			$gField = "`{$ins->selfname}`.".$field;
		else
			$gField = $field;

		#TODO: why ifs??

		if ( property_exists( $ins, $label ) !== true )
			$ins->$label = null;

		if ( $type == "COUNT" )
			$ins->_extraFields[ $label ] = "COUNT( $gField) `$label`";
		elseif ( $type == "SUM" )
			$ins->_extraFields[ $label ] = "SUM( $gField ) `$label`";
		elseif ( $type == "MAX" )
			$ins->_extraFields[ $label ] = "MAX( $gField ) `$label`";
		elseif ( $type == "MIN" )
			$ins->_extraFields[ $label ] = "MIN( $gField ) `$label`";

		$ins->__lastOperation = JModel::OPTYPE_GROUP;

		return $ins;
	}

	/**
	 * add not present in field array
	 *
	 * @param $array
	 * @param $db
	 * @param $field
	 * @author Özgür Köy
	 */
	public static function addToFieldArray(&$array, $db, $field, $label=null)
	{
		$value = $db . "." . $field . ( is_null( $label ) ? "" : " AS `$label`" );
		if(!in_array($value,$array))
			array_push($array,$value);
	}

	public static function assetOutput( $field ){
		return "`$field`";
	}

	/**
	 * process clause string
	 *
	 * @param string $field
	 * @param mixed $value
	 * @param string $relatedTable
	 * @param bool $onlyClause
	 * @return string|array
	 * @author Özgür Köy
	 */
	public static function processString(&$field, &$value, &$relatedTable, &$clauseValues=array())
	{
		$onlyClause=false; //is this called as a parameter?
		$ret = array();

		if($field=="_custom"){
			#$ret = array("(?)", strval($value));
			$ret = array("(".strval($value).")",null);
		}
		elseif(strpos($value, "LIKE ")!==false){
			// echo "MATCH % --:".substr($value, 5).PHP_EOL;
			$ret   = array("`".$relatedTable."`.`".$field."` LIKE ?", substr($value, 5));
		}
		elseif(preg_match('/.+?&or.+?/i', $value)==1){
			// 'abc' OR 'def'
			$tm0   = explode(' &or ', $value);
			$tmStr = $tmVal = array();

			foreach ($tm0 as $tm1){
				$tm1 = static::processString($field, $tm1, $relatedTable);

				$tmStr[] = $tm1[0];
				if(is_array($tm1[1])){
					foreach ( $tm1[ 1 ] as $tm2 ) {
						$tmVal[] = $tm2;
					}
				}
				else
					$tmVal[] = $tm1[1];
			}

			$tmStr = "(".implode(" OR ", $tmStr).")";

			$ret = array($tmStr, $tmVal);
		}
		elseif(strpos($value, "IS NOT NULL")!==false){
			// echo "MATCH %:".$value.PHP_EOL;
			$ret   = array("`".$relatedTable."`.`".$field."` IS NOT NULL", null);
		}
		elseif(strpos($value, "IS NULL")!==false){
			// echo "MATCH %:".$value.PHP_EOL;
			$ret   = array("`".$relatedTable."`.`".$field."` IS NULL", null);
		}
		elseif(strpos($value,"BETWEEN")!==false){
			// BETWEEN 4,5
			$value = substr($value, 8);
			$value = explode( ",", $value );
			$ret   = array("(`".$relatedTable."`.`".$field."` BETWEEN ? AND ?)", $value);
		}
		elseif(strpos($value,"NOT IN")!==false){
			// BETWEEN 4,5
			$value = substr($value, 7);
			$value = explode( ",", $value );
			$ret   = array("(`".$relatedTable."`.`".$field."` NOT IN (".(JUtil::multistr("?", sizeof($value))).") )", $value);
		}
		elseif(
			strpos($value,'&lt')!==false ||
			strpos($value,'&gt')!==false ||
			strpos($value,'&ge')!==false ||
			strpos($value,'&ne')!==false ||
			strpos($value,'&le')!==false
		){
			$tm1 = static::$_comparingClauses[ (substr($value, 0, 3)) ];
			$ret = array( "`".$relatedTable."`.`".$field."`  ".$tm1." ?", strval(substr($value, 4, 10)) );
		}
		else{
			$ret = array("`".$relatedTable."`".".`".$field."`=?", strval($value));
		}


		return $onlyClause?$ret[0]:$ret;


	}

	/**
	 * save N-N's, prevent uniqueness for connector below
	 *
	 * @param object $ins related object
	 * @param mixed $id passed id, array or int
	 * @param string $type table name
	 * @param string $tag relation label
	 * @param bool $reverse reverse connection, changes the dsn.
	 * @return mixed
	 * @author Özgür Köy
	 */
	public static function
	saveMany(&$ins, $id, $type, $tag, $reverse=false)
	{
		if(!($ins->id>0) || is_null($ins->id)) return false;

		if($reverse)
			$dsn = is_object($ins->$tag) ? $ins->$tag->dsn : ($ins->getRelativeDsn($tag)) ;
		else
			$dsn = $ins->dsn;

		if(is_null($id) || !isset($id)){
			//need something
			return false;
		}
		elseif(is_numeric($id)){
			//some id is passed
			$cn = ucfirst($type);
			if(!is_null( $ins->$tag = new $cn($id) )){
				$q = $ins->modelQuery("save_".$tag);

				JPDO::$hideError = true;
				JPDO::connect( JCache::read('JDSN.'.$dsn) ); // connector table is here..
				if($reverse)
					JPDO::executeQuery($q, $id, $ins->id, $id.".".$ins->id.".".$tag);
				else
					JPDO::executeQuery($q, $ins->id, $id, $ins->id.".".$id.".".$tag);

				JPDO::$hideError = false;
			}
			else return false;
		}
		elseif(is_array($id)){
			$q = $ins->modelQuery("save_".$tag);

			foreach ($id as $id0) {
				if(!is_numeric($id0)) continue;

				JPDO::connect( JCache::read('JDSN.'.$dsn) ); // connector table is here..
				JPDO::$hideError = true;
//				JPDO::$debugQuery=true;
				if($reverse)
					JPDO::executeQuery($q, $id0, $ins->id, $id0.".".$ins->id.".".$tag);
				else
					JPDO::executeQuery($q, $ins->id, $id0, $ins->id.".".$id0.".".$tag);

//				JPDO::$debugQuery=false;
				JPDO::$hideError = false;
			}
		}
		elseif(is_object($id) && property_exists($id,"selfname") && $id->selfname==$type){
			//an object is passed
			if ( !( $id->id > 0 ) || $id->gotValue !== true )
				return false;

			$q = $ins->modelQuery("save_".$tag);
			$firstSet = false;
			$id->resetIndex(); // TODO CHECK HERE
			do {
				$id0 = $id->id;
				if(!is_numeric($id0)) continue;

				$firstSet==false && $ins->$tag = $id && $firstSet = true;

				JPDO::connect( JCache::read('JDSN.'.$dsn) ); // connector table is here..
				JPDO::$hideError = true;
				if($reverse)
					JPDO::executeQuery($q, $id0, $ins->id, $id0.".".$ins->id.".".$tag);
				else
					JPDO::executeQuery($q, $ins->id, $id0, $ins->id.".".$id0.".".$tag);

				JPDO::$hideError = false;

			} while ( $id->populate() );
		}
		else
			return false;

		return $ins;
	}

}