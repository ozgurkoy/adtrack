<?php
/************************************
*
*		smarty extended class
*		özgür köy
*
************************************/

class JTemplate{
	static $instance = null;
	/**
	 * @var Smarty
	 */
	static $_smarty = null;
	private static $_classesToExtend = array();

	/**
	 * Singleton to call from all other functions
	 */
	public static function singleton(){
		global $__DP, $SYSTEM_EMAILS;
		if ( self::$instance == null ) {
			#default extension
			self::$_classesToExtend[ "UserSmarty" ] = "$__DP/site/lib/smarty.php";
			self::$_smarty                          = new Smarty();
			self::$_smarty->template_dir            = "$__DP/site/templates";
			self::$_smarty->cache_dir               = "$__DP/temp/templateCache";
			self::$_smarty->compile_dir             = "$__DP/temp/templateCompiled";
			self::$_smarty->plugin_dir              = "$__DP/core/lib/smarty/libs/plugins";

			self::assignGlobalVariables();

			self::$_smarty->use_sub_dirs  = true;
			self::$_smarty->caching       = 2;
			self::$_smarty->compile_check = IS_PRODUCTION == 1 ? 0 : 1;

			self::$_smarty->config_dir = "$__DP/site/template/templateConfig";
			self::$_smarty->securePage = 'http://' . SERVER_NAME . '/admingle/';

			//register user smarty functions
			self::extendClasses();

			self::$_smarty->load_filter( 'output', 'stripManySlashes' );

			include $__DP . "site/lib/smarty/_blocks.php";
			JTBlock::init( self::$_smarty );
		}

		try{
			self::$instance ||
					self::$instance = new JTemplate();
		}
		catch (E $e){
			return false;
		}

		return self::$instance;
	}

	public static function init(){
		self::$_smarty->clear_all_assign();
		self::assignGlobalVariables();
	}
	
	public static function assignGlobalVariables(){
		global $__DP, $SYSTEM_EMAILS, $__site;
		self::$_smarty->assign("DP",$__DP);
		self::$_smarty->assign("SITEROOT",SERVER_NAME);
		self::$_smarty->assign("SYSTEM_EMAILS",$SYSTEM_EMAILS);

		if (defined('ENABLE_INVITE')) self::$_smarty->assign("ENABLE_INVITE",ENABLE_INVITE);
		if (defined('COUNTRY_CODE')) self::$_smarty->assign("COUNTRY_CODE",COUNTRY_CODE);

		if(isset($_COOKIE["adcookiepolicyagree"])){
			self::$_smarty->assign("COOKIELAW", TRUE);
		}else{
			self::$_smarty->assign("COOKIELAW", FALSE);
		}
		if(isset($_SESSION["is_mobile"])){
			self::$_smarty->assign("IS_MOBILE", $_SESSION["is_mobile"]);
		}else{
			self::$_smarty->assign("IS_MOBILE", 0);
		}

		self::$_smarty->assign("_emptyArray", array());
		self::$_smarty->assign("ADM_SETTINGS",Setting::getAllSettings());
		self::$_smarty->assign("lastBuildTime",getlastmod());
		self::loadLanguage();
	}

	public static function assign( $vl, $ip ){
		self::$_smarty->assign($vl,$ip);
	}

	public static function assignByRef( $vl, &$ip ){
		self::$_smarty->assign_by_ref($vl,$ip);
	}

	public static function display( $template, $cacheId="", $compileId=""){
		self::$_smarty->display($template, $cacheId="", $compileId="");
	}

	public static function setCacheTime(){
		self::$_smarty->cache_lifetime = 0;
	}

	public static function loadLanguage(){

		if(Lang::activeCount() > 1){
			self::$_smarty->assign("MULTI_LANG", 1);
			self::$_smarty->assign("LANGS", Lang::activeArray());
		}else{
			self::$_smarty->assign("MULTI_LANG", 0);
		}
	}

	public static function cacheCheck( $resource ){
		$args = func_get_args() ;
		array_shift( $args );

		$cacheStr = self::buildCacheString( $resource, $args );

		if($cacheStr["timeout"]==0)
			self::$_smarty->caching				= 0;
		else
			self::$_smarty->caching				= 2;

		return self::$_smarty->is_cached( $cacheStr["dir"] , $cacheStr["str"] ) ;
	}

	public static function pfetch( $resource ){
		/*
		*
		*	changed the smarty path for 0 ers, will try
		*/
		global $__DP;
		$args = func_get_args() ;
		array_shift( $args );
		$cacheStr = self::buildCacheString( $resource, $args );

		self::$_smarty->cache_lifetime 	= intval( $cacheStr["timeout"] );

		if(isset($_SESSION["LAST_ERROR"])){
			$lang = isset($_SESSION["lang"]) ? $_SESSION["lang"] : Setting::getValue("DEFAULT_LANGUAGE");

			//require_once $__DP . 'site/langFiles/errMessages.php';
			$_errMessages = LangWords::getLangWords("errMessage",$_SESSION["lang"]);

			$error = json_decode($_SESSION["LAST_ERROR"]);
			if (isset($_errMessages[$error->errorCode])) $error = $_errMessages[$error->errorCode];

			self::$_smarty->assign("hasError","Y");
			self::$_smarty->assign("LAST_ERROR",$error);
			unset($_SESSION["LAST_ERROR"]);
		}
		else{
			self::$_smarty->assign("hasError","N");
			self::$_smarty->assign("LAST_ERROR","");
		}

		if (isset($_SESSION["adFromMobile"])){
			self::$_smarty->assign("_fromMobile",true);
		}

		if($cacheStr["timeout"]==0){
			self::$_smarty->caching				= 0;
			return self::$_smarty->fetch( $cacheStr["dir"] );
		}
		else{
			self::$_smarty->caching				= 2;
			return self::$_smarty->fetch( $cacheStr["dir"] , $cacheStr["str"] );
		}
	}

	public static function dFetch( $path, $caching=0 ) {
		self::$_smarty->caching				= $caching;
		return self::$_smarty->fetch( $path );
	}

	public static function clearCache( $resource ){
		$args = func_get_args() ;
		array_shift( $args );
		$cacheStr = self::buildCacheString( $resource, $args );
		self::$_smarty->clear_cache( null, preg_replace('/\\|0$/', '', $cacheStr["str"]) );
	}

	public static function clearAllCache( ){
		self::$_smarty->clear_all_cache();
	}
	public static function clearCompiled() {
		self::$_smarty->clear_compiled_tpl();
	}

	private static function buildCacheString( $resource, $args ){
		global $smartyCacheDirs;

		$s = $smartyCacheDirs[ $resource ];

		$argCount = substr_count( $s["cacheDir"], "%" );
		if(sizeof($s)==0) {print_r($args);echo $resource;die("fatal smarty config error-> ".$resource);}
		$xstr = $s["cacheDir"];

		if( $argCount != sizeof($args) ){
			//get rid of the %s starting from end.. mostly pages.. but be careful..
			$cnt = 0;
			while( substr_count( $xstr, "%" ) != sizeof($args) ){
				$xstr = preg_replace('/%s(?!.*%s.*).*$/i', '0', $xstr);

				if(++$cnt == 10) {
					JLog::log("cri","fatal smarty error->$resource");
					JUtil::configProblem("fatal smarty error->$resource");
				}
			}
		}

	 	$cacheStr = @vsprintf( $xstr, $args );
		if(!isset($s["cacheTimeout"])) $s["cacheTimeout"] = 0;
		return array( "str"=>$cacheStr,"dir"=>$s["realPath"], "timeout"=>$s["cacheTimeout"] );
	}

	public static function attachClass( $ns ) {
		global $__DP;
		$path = $__DP."site/lib/smarty/{$ns}.php";

		if ( is_file( $path ) ) {
			require $path;
			$className = ucfirst($ns)."_smarty";
			$userMethods = get_class_methods( $className );
			//self::$_classesToExtend[ $className ] = $path;

			foreach ( $userMethods as $method ) {
				self::$_smarty->register_function( $method, array( $className, $method ) );
			}
		}
	}

	private static function extendClasses(){
		foreach ( self::$_classesToExtend as $className => $path ) {
			include $path;

			$userMethods = get_class_methods( $className );

			foreach ( $userMethods as $method ) {
				self::$_smarty->register_function( $method, array( $className, $method ) );
			}

		}
	}

	private function __construct(){}
}
?>