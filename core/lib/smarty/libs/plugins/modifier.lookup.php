<?php 
function smarty_modifier_lookup($value='', $from=array()) { 
	if (array_key_exists(lcfirst($value), $from)) {
		return $from[lcfirst($value)];
	}else if (array_key_exists(ucfirst($value), $from)) {
		return $from[ucfirst($value)];
	}else{
		return null;
	}
}
?>
