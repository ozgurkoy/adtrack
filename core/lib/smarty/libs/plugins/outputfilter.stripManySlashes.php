<?php
/**
 * strip many slashes
 *
 * @return void
 * @author Özgür Köy
 **/
function smarty_outputfilter_stripManySlashes($source, &$smarty)
{
    /*
    //$source = preg_replace('/[\\\\]/si', 'x~z~ad', $source);
	$source = preg_replace('/[\\\\]+\'/si', '\'', $source);
	$source = preg_replace('/[\\\\]+"/si', '"', $source);
	$source = preg_replace('/[\\\\]+/si', '\\', $source);

    //$source = preg_replace('/x~z~ad/si', '\\', $source);
	return $source;
    */
    global $_smCount,$_smCountaps;
    if(stripos($source,'<!--noreplace-->')!==false){
        $_smCount=0;
        $_smCountaps = array();

        $source = preg_replace_callback('%<!--noreplace-->.+?<!--/noreplace-->%si',
            function ($groups) {
                global $_smCount,$_smCountaps;
                $groups[0] = preg_replace('%<!--/?noreplace-->%si', '', $groups[0]);
                $_smCountaps[$_smCount] = array("~D".$_smCount."~",$groups[0]);
                return "~D".($_smCount++)."~";
            },
            $source);

        $source = preg_replace('/[\\\\]+\'/si', '\'', $source);
        $source = preg_replace('/[\\\\]+"/si', '"', $source);
        $source = preg_replace('/[\\\\]+/si', '\\', $source);

        foreach ($_smCountaps as $_smCountap) {
            $source = str_ireplace($_smCountap[0], $_smCountap[1], $source);
        }
    }
    else{

        $source = preg_replace('/[\\\\]+\'/si', '\'', $source);
        $source = preg_replace('/[\\\\]+"/si', '"', $source);
        $source = preg_replace('/[\\\\]+/si', '\\', $source);
    }
    return $source ;
}
?>