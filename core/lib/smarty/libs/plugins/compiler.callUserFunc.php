<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {callUserFunc} compiler function plugin
 *
 * Type:     compiler function<br>
 * Name:     callUserFunc<br>
 * Date:     Sep 08 2006<br>
 * Purpose:  provide a while-loop<br>
 * @author Frank Habermann <lordlamer at lordlamer dot de>
 * @param string containing var-attribute and value-attribute
 * @param Smarty_Compiler
 * Examples: The following code will loop 5 times ;)
 *     <pre>
 * {assign var="test" value=1}
 * {while ($test <= 5)}
 *    {assign var="test" value="`$test+1`"}
 *   jo
 * {/while}
 *     </pre>
 */

//$this->register_compiler_function('/doWhile', 'smarty_compiler_enddoWhile');

function smarty_compiler_callUserFunc($tag_arg, &$smarty) {
//	print_r( $tag_arg );
	$f=preg_replace( '/\$(\w+)/', "\$this->_tpl_vars[\"$1\"]", $tag_arg );
//	$f=preg_replace( '/\$(.+?)([-\(])/', "\$this->_tpl_vars['$1']$2", $tag_arg );

	return $f;
	print_r( $f );
	$res = $smarty->_compile_if_tag($tag_arg);
	preg_match("/<\?php if (.*): \?>/",$res,$token);
	print_r( $token );
	return $token[1];
}

?>