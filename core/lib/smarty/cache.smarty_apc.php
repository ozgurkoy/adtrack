<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Type:     cache handler
 * Name:     apc
 * Purpose:  Utilize the APC memory storage as a Smarty cache
 *
 * @author Danny Berger <code@dannyberger.com>
 * @link http://www.dannyberger.com/code/php-smarty-apc-cache-handler/
 * @version 0.9.6.1
 *
 * @param string $action
 * @param Smarty &$smarty
 * @param string &$cache_content
 * @param string $tpl_file
 * @param string $cache_id
 * @param string $compile_id
 * @param integer $exp_time
 */
function smarty_cache_apc($action, &$smarty, &$cache_content, $tpl_file=null, $cache_id=null, $compile_id=null, $exp_time=null) {
    static $_enabled = false, $_md5root = false, $_compile_dir_sep = false;
   
    if (!$_enabled) {
        if (!function_exists('apc_store')) {
            $smarty->trigger_error('cache_handler: APC extension is unavailable');
            return false;
        }
       
        $_enabled = true;
        $_md5root = 'sca_' . substr(md5($smarty->cache_dir), 0, 8);
        $_compile_dir_sep = $smarty->use_sub_dirs ? DIRECTORY_SEPARATOR : '^';
    }
   
    $_auto_id = $smarty->_get_auto_id($cache_id, $compile_id);
    $_cache_name = $smarty->_get_auto_filename('', $tpl_file, $_auto_id);
    $_cache_file = $_md5root . $_cache_name;
   
    switch ($action) {
        case 'read':
            if (($cache_content = apc_fetch($_cache_file)) === false) {
                $cache_content = null;
            }
           
            break;
        case 'write':
            apc_store($_cache_file, $cache_content, (($smarty->cache_lifetime > -1) ? $smarty->cache_lifetime : null));
           
            break;
        case 'clear':
            $_strlen = strlen($_cache_file);
            $_time = time();
           
            $_items = apc_cache_info('user');
            if (!isset($_items['cache_list'])) break;
            $_items = $_items['cache_list'];
           
            foreach ($_items as $_item) {
                if (substr($_item['info'], 0, $_strlen) == $_cache_file) {
                    if ($_item['creation_time'] + $exp_time <= $_time) {
                        apc_delete($_item['info']);
                        if ($tpl_file) break;
                    }
                }
            }
           
            break;
        default:
            $smarty->trigger_error('cache_handler: unknown action "' . $action . '"');
           
            return false;
    }
   
    return true;
}

?>