<?php
/**
 * JIndex class for indexing
 *
 * @package jeelet
 * @author Özgür Köy
 **/
class JIndex
{
	/**
	 * debug
	 *
	 * @var array
	 **/
	private static $ranIndex=array();
	/**
	 * build index table
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	static function buildIndexTable($class,$config,$dsn)
	{
		$tclass = ucfirst($class);
		// $tclass = new $tclass();
	
		JPDO::connect( JCache::read('JDSN.'.$dsn) );

		foreach ($config as $indexName => $index) {
			// JPDO::executeQuery("DROP TABLE `JI_$indexName`");
				
			$tableDef = "
			CREATE TABLE IF NOT EXISTS `JI_$indexName` (
			  `id` INT NOT NULL AUTO_INCREMENT ,
			  `idate` int(11),
			   PRIMARY KEY ( `id` )
			) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;
			";
		
			JPDO::executeQuery($tableDef);

			unset($tclass);

			foreach ($index as $sindex) {
			
				$prvFields = JPDO::listFields("JI_$indexName");

				if($sindex["type"]=="int")
					$ct = "int(11) NULL";
				elseif($sindex["type"]=="float")
					$ct = "FLOAT NULL";
				elseif($sindex["type"]=="date")
					$ct = "DATE NULL";
				elseif($sindex["type"]=="text")
					$ct = "text CHARACTER SET utf8 COLLATE utf8_turkish_ci NULL";
				else
					$ct = "TEXT CHARACTER SET utf8 COLLATE utf8_turkish_ci NULL , ADD FULLTEXT (`".$sindex["name"]."`)";
					
			
				if(!in_array( $sindex["name"], $prvFields )){
					JPDO::executeQuery("ALTER TABLE `JI_$indexName` 
										ADD `".$sindex["name"]."` $ct ;");				
																			
				}
				else{
					JPDO::executeQuery("ALTER TABLE `JI_$indexName` 
										CHANGE `".$sindex["name"]."` $ct ;");
				}
			}
			
		}
		JLog::log("build","index table JI_$indexName built");
		// echo "Index table JI_$indexName build<BR>";
		
		
	}
	
	/**
	 * drop index
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	static function dropIndex($dsn, $table, $field)
	{
		JPDO::connect( JCache::read('JDSN.'.$dsn) );
		
		$dp = "ALTER TABLE `JI_keyword` DROP `$field`";
		JPDO::executeQuery($dp);
		
	}

	/**
	 * destruct self indexes
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function destruct($dsn, $tables,$ids)	
	{
		JPDO::connect( JCache::read('JDSN.'.$dsn) );

		foreach ($tables as $table) {
			$dp = "DELETE FROM `JI_".$table."` WHERE id IN(".implode(",",$ids).")";
			JPDO::executeQuery($dp);
			
		}
	}

	/**
	 * trigger index
	 *
	 * @return void
	 * @author Özgür Köy	
	 **/
	static function trigger($rclass, $config,$fullRun=false)
	{
		/*	
		"class"=>"product",
		"action"=>"stock",
		"index"=>0,
		"indexDef"=>"productColor.productColorSize.stock.amount",
		"indexField"=>"stokz",
		"indexLabel"=>"totalStockAmount",
		"type"=>"int",
		*/
		
		//accept the class and render the fields array
		$jfield = $config["indexDef"];
		$self	= $rclass->selfname;
		
		$class 	= ucfirst($config["class"]);
		$class 	= new $class;
		
		$indexValues = array();
		$psa 	= array();
		$topids	= array();
		// echo "TRIGG:".$config["indexDef"]." ".$rclass->selfname." ".$rclass->id ."<BR>
		// ";
		// echo ">".$jfield."
		// ";
		if(strpos($jfield, ".")!==false){
			$t0 	= explode(".",$jfield);
			$base 	= ucfirst($config["class"]);
			
			//check if it's the parent triggering
			if($self!=$config["class"])	{
				//this is a chil1d triggering..
				if(isset($rclass->id) && ($rclass->id>0) /*&& isset($rclass->_relatives[ $t0[sizeof($t0)-2] ])*/){
					$arrayMap = array($base);
					foreach ($t0 as $dotelement) {
						$tempc = new $base;
						
						if(array_key_exists($dotelement,$tempc->_relatives)){
							if($base==ucfirst($rclass->selfname)){
								break;
							}
							$base = ucfirst($tempc->_relatives[$dotelement]);
							$arrayMap[] = $base;
						}
					}
										
					$filterClass = $rclass;
					
					for ($ci=sizeof($arrayMap)-2; $ci >=0 ; $ci--) {  // 1 for field(amount), 1 for self class(stock) - see above
						
						$pars = $arrayMap[$ci];
						$tempc = new $pars();
						
						if($ci==sizeof($arrayMap)-2){ 
							//first step will load cause we have no id s to filter from.
							$tempc->load($filterClass);
						}
						else{
							$tempc->filter($filterClass);
						}

						$filterClass = $tempc;
					}

					$rclass = $tempc;
					
					//reverse filtering , so we need to collect all the owner parents' ids
					$topids = $rclass->_ids;

				}
				else{
					// buggy field it seems?
					// echo "Failed:".$jfield."<";
					// echo "
					// ";
					
					
					
					JLog::log("admin","non relevant INDEX field $jfield for class ".$config["class"]);
					return;	
				} 
			}
			else{
				//parent triggering
				$topids = array($rclass->id);
			}

		}
		else{
			//straight field
			if(in_array($jfield,$class->_fields) || (method_exists($class, $imet="JIF_".$jfield))){
				// echo "single field:".$jfield." (".$rclass->id.")\n";
				$topids = array($rclass->id);
			}
			else{
				if(isset($class->_relatives[$jfield])){
					//like jdoc
					$topids = array($rclass->id);
				}
				else{
					echo "non-e relevant INDEX field $jfield for class ".$class->selfname."<BR>";
				
					JLog::log("build","non relevant INDEX field $jfield for class ".$class->selfname);
					return;
				}
			}
		}
		
		
		
		foreach ($topids as $toppi) {
			// echo "top id : ". $toppi."<BR>";
			
			if($fullRun && in_array($config["indexField"].".".$config["indexDef"].".".$toppi,self::$ranIndex)){
				// echo "die:".$config["indexDef"].".".$toppi."<BR>";
				return;
			}
			else
				self::$ranIndex[] = $config["indexField"].".".$config["indexDef"].".".$toppi;
		}
		
		//now indexing..
		if(sizeof($topids)>0){
			foreach ($topids as $topid) {
				//check for the record
				JPDO::connect( JCache::read('JDSN.'.$class->dsn) );
				$recs = (array)JPDO::executeQuery("SELECT * FROM `JI_".$config["indexLabel"]."` WHERE id=?",$topid);

				
				// if(sizeof($recs)>0){
				if(sizeof($recs)>0/* && isset($recs[0][$config["indexField"]]) && strlen($recs[0][$config["indexField"]])>0*/){
					//already full.
						$fieldGroup = explode("|",$recs[0][$config["indexField"]]);
						//just the active field.
						// if(!isset($fieldGroup[$config["index"]]))
							// print_r($fieldGroup);
							
						if(isset($fieldGroup[$config["index"]]))
							$activeIndex = $fieldGroup[$config["index"]];
						// print_r($topid);
						
						$class->load($topid);
						
						$t0 = explode(".",$config["indexDef"]);
						
						
						// echo "@@@@START PREC. ( {$config["indexDef"]} ) $topid// ";
						$psa = self::precursion($t0, $class);
						// echo "// @@@@@END PREC.( {$config["indexDef"]} ) $topid// ";
						
						//collected data
						
						// if(is_array($psa) && sizeof($psa)==0) $psa="-";

						if(isset($psa[0]) && !is_array($psa[0]))
							$psa = array_unique($psa);
						else{
							//a jdoc array
							$tempj = array();
							
							foreach ($psa as $pstemp) {
								if(isset($pstemp["id"]))
								$tempj[] = $pstemp["id"];
							}
							$psa = $tempj;
						}
						
						foreach($psa as $ps=>$sa){
							if(is_array($sa)){print_r($t0);print_r($psa);print_r($config);print_r($class);exit;}
							if(trim($sa)=="") unset($ps);
						}
						
						//normalize if pile
						if($config["type"]=="pile"){
							foreach ($psa as $pkey => $pval) {
								$psa[$pkey] = JUtil::normalize($pval);
							}
						}
						
						$fieldGroup[$config["index"]] = ",".implode(",",$psa).","; // trailing , will help the search.
						
						if(sizeof($psa)==1)
							$fieldGroup[$config["index"]] = $psa[0];
						else{
							$fieldGroup[$config["index"]] = ",".implode(",",$psa).", "; // trailing , will help the search.
						}
						ksort($fieldGroup);

						$idata = implode("|",$fieldGroup);
						JPDO::executeQuery("UPDATE `JI_".$config["indexLabel"]."` SET `".$config["indexField"]."`=? WHERE id=?",$idata,$topid);
						// echo "!!!
						// UPDATE `JI_".$config["indexLabel"]."` SET `".$config["indexField"]."`=? WHERE id=?",$idata," ",$topid;
					// }
					// else{
					// 	echo "non existent record.";
					// 	echo "SELECT `".$config["indexField"]."` FROM `JI_".$config["indexLabel"]."` WHERE id=?",$topid;
					// 	
					// 	print_r($recs);
					// 	
					// }
				}
				else{
					//empty index so need to rebuild
					//build the index for the id, all of the field components
					
					$fin = array_merge($rclass->indexBridge, $rclass->indexFinal);
					$idata = array();
					
					// echo "empty building for id : $topid, ".$config["indexField"];
					
					foreach ($fin as $fina) {
						if($fina["indexField"]!=$config["indexField"]){
							continue;
						}
						
						// echo "running ".$fina["indexDef"]."
						// ";
						
						

						$class->load($topid);
						
						$t0 = explode(".",$fina["indexDef"]);
						
						$psa = self::precursion($t0, $class);
						
						foreach($psa as $ps=>$sa){
							if(trim($sa)=="") unset($ps);
						}
						
						//collected data
						if(isset($psa[0]) && !is_array($psa[0]))
							$psa = array_unique($psa);

						if(sizeof($psa)==1)
							$idata[$fina["index"]] = $psa[0];
						else
							$idata[$fina["index"]] = ",".implode(",",$psa).", "; // trailing , will help the search.

						// $idata[$fina["index"]] = ",".implode(",",$psa).","; // trailing , will help the search.
					}
					ksort($idata);
					// echo "collected data:";print_r($idata);
					
					$idata = implode("|",$idata);
					JPDO::executeQuery("INSERT INTO `JI_".$config["indexLabel"]."`(id,idate, `".$config["indexField"]."`) VALUES(?,?,?)",$topid,time(),$idata);
					// echo "@@
					// INSERT INTO `JI_".$config["indexLabel"]."`(id, `".$config["indexField"]."`) VALUES(?,?)",$topid,$idata;
					
					
				}
			}
		}
		else{
			// echo "empty id set:".$config["indexField"]."\n";
			//nothing found to index
		}
		
		
		// switch ($type) {
		// 	case 'field':
		// 
		// 		break;
		// 	case 'variable':
		// 		# code...
		// 		break;
		// 	default:
		// 
		// 		break;
		// }

	}

	private static function precursion($props, $class, $prindex=0 ){   
		$att = array();
		
		$currentMethod = "load".ucfirst($props[$prindex]);
		if(is_object($class) && method_exists($class, $currentMethod )){
			do {
				if(isset($props[$prindex+1])){
					$tm0 = self::precursion($props,$class->$currentMethod(),$prindex+1);
					if(is_array($tm0)){
						$att = array_merge($att,$tm0);
					}
					else
						$att[] = $tm0;
				}
				else{
					if($class->_relatives[ $props[$prindex] ]=="JDoc"){
						$class->$currentMethod();

						$att = array_merge($att,(array)$class->{$props[$prindex]});
					}
					elseif(is_object($class)){
						do {
							$att[] =  $class->{$props[$prindex]};               
						} 
						while ($class->populate());
					}
				}
			} while ($class->populate());
		}
		elseif(is_object($class)){
			do {
				if(property_exists($class, $props[$prindex])){
					$att[] =  $class->{$props[$prindex]};
				}
				elseif(method_exists($class, $imet="JIF_".$props[$prindex])){
					//responder, if exists
					$att[] =  $class->$imet();
				}
				else{
					$att[] = "";}
			} 
			while ($class->populate());
		}
		else{
			// echo "
			// St:DEAD
			// ";
			// print_r($props);
		}
		return $att;
	}
	
	/**
	 * rebuild index
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function _rebuildAllIndex($indexArray)
	{
		foreach ($indexArray as $ip => $indexe) {
			$cln = ucfirst($ip);
			
			//need parsed loading here.. TODO!
			$indexCount = 0;
			$rclass = new $cln;
			$maxC = $rclass->count();
			while($indexCount<=$maxC){
				$rclass = new $cln;
				$rclass->load(null,$indexCount);
				
				// $fin = array_merge($rclass->indexBridge, $rclass->indexFinal);
				$fin = array_merge($rclass->indexBridge, $rclass->indexFinal);
				
				echo "Auto Index for $cln pile($indexCount - ".($indexCount+200).")<BR>";
				if(sizeof($rclass->_ids)>0)	{
					
					do {
					
						foreach ($fin as $fina) {
							// if(!in_array($fina["indexField"],$completedFields)){
								// echo "\n------GOGO!:".$fina["indexField"]." ".$rclass->id." ".$rclass->selfname."\n";
							// print_r($fina);
								self::trigger($rclass,$fina,true);
								// $completedFields[] = $fina["indexField"];
							// }
							// else
								// echo "RAN before:".$fina["indexField"];
					
							// break;
						}
						// break;
					} while ($rclass->populate());
				}
				else{
					echo "Skipped Auto Index for $cln pile($indexCount - ".($indexCount+200).")<BR>";
					
				}
				
				unset($rclass);
				
				$indexCount+=200;
			}
		}
		JLog::log("build","all index rebuilt");
		
		echo "index rebuilt<BR>";
		
		
	}


} // END class JIndex
?>