<?php
/************************************
 *
 *		session handling file
 *
 *		1. memcached
 *		2. mysql
 *		3. file ( default )
 *
 *		özgür köy
 *
 ************************************/
require_once $__DP.'core/lib/session/sessionBase.php';
// read user config
require_once $__DP.'site/def/sessions.php';

$_sessionEngineUp = false;

foreach($_sessionEngines as $s ){
	include_once "session_".$s.".php";
	if( $_sessionEngineUp ) break;

}
ini_set('session.gc_probability', 5);
ini_set('session.gc_divisor', 100);
// ini_set('session.gc_maxlifetime', 100);

if($_sessionEngineUp){
	ini_set('session.save_handler', 'user');
	ini_set('url_rewriter.tags', 0);

	session_set_save_handler(
		array($_sessObj, 'open'),
		array($_sessObj, 'close'),
		array($_sessObj, 'read'),
		array($_sessObj, 'write'),
		array($_sessObj, 'destroy'),
		array($_sessObj, 'gc')
	);
}

//	 print_r($_SESSION);
if(isset($_POST['s']) && strlen($_POST['s'])) {
	//session will be started later
} else {
	session_start();
}
