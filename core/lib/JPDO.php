<?php
/**
*		pdo class - jeelet
*		özgür köy
*		inspired and modified from connection.class by Adler Brediks Medrado
*		09-04-2013 added multi db transaction
*/
include $__DP . "core/lib/db/".DB_ENGINE."/JPDO_base.php";

class JPDO extends JPDO_base{

	public static $connection;
	public static $dsn=null;
	public static $label=null;
	public static $prevDsn=null;
	public static $username;
	public static $password;
	public static $effected ;
	public static $queryCount=0;
	public static $utfExecuted = false;
	public static $lastExecutionTime = 0;
	public static $isTransAction = 0;
	public static $hideError = false;
	public static $debugQuery = false;
	public static $returnError = false;
	public static $transactionCollection = array();
	public static $transactionExclusion = array();
	public static $queryCollecting=false;
	public static $queryCollection=array();
	public static $queryVars="";

	public static $instance;
	public static $connections = array();
	
	private function __construct() {}

	public static function singleton(){
	        if (!isset(self::$instance)) {
				$c = __CLASS__;
				self::$instance = new $c;
	        }

	        return self::$instance;
	}

	

	public static function debugQuery($set=true) {
		return self::$debugQuery = $set;
	}

	/**
	 * @return PDO
	 * @author Özgür Köy
	 */
	public static function getConnection() {
		return self::$connection;
	}


	/**
	 * revert to previous dsn
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function revertDsn()
	{
		if(!is_null(self::$prevDsn)) {
			self::connect(self::$prevDsn);
		}
		self::$prevDsn = null;
	}
	
	/**
	 * begin transaction for the active connection, must be called after assignment
	 *
	 * @return void
	 * @author Özgür Köy
	 **/
	public static function transactConnection()
	{
		if (
			self::$isTransAction == 1 &&
			self::$connections[ self::$label ][ "transaction" ] == 0 &&
			!in_array( self::$label, self::$transactionExclusion ) &&
			(
				sizeof( self::$transactionCollection ) == 0 ||
				in_array( self::$label, self::$transactionCollection )
			)
		)
		{
			self::startTransaction();
			self::$connections[ self::$label ][ "transaction" ] = 1;
		}
	}

	/**
	 * Execute a DML
	 *
	 * @param String $query
	 */

	public static function executeDML($query) {
		$a = self::getConnection();
		if($a==false) return false;

		if (!self::getConnection()->query($query)) {
			throw new JError( "ERROR_FATAL_QUERY_".self::getConnection()->errorInfo() ,911);
		} else {
			return true;
		}
	}

	public static function collectQueries() {
		self::$queryCollecting = true;
	}

	public static function runQueryCollection() {
		if(self::$queryCollecting!== true)
			return false;
//		JPDO::debugQuery();
		self::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);
//		echo ".".microtime( true );
//		$q = explode( "|||", self::$queryVars );
//		echo ".".microtime( true );
//		array_unshift( $q, null );

		self::$queryCollecting = false;
		$qq = str_replace("`",'"',implode( ";", self::$queryCollection ));
		self::executeQuery( $qq );
		self::$queryVars       = "";
		self::$queryCollection = array();
		self::$connection->setAttribute( PDO::ATTR_EMULATE_PREPARES, 0 );
	}

	/**
	 * Execute a query
	 *
	 * @param String $query
	 * @return PDO ResultSet Object
	 */
	public static function executeQuery($query) {
		global $__DP;
		// echo $query."\n\n";
		self::$queryCount++;

		if(strlen($query)==0) return "";

		$args = func_num_args();
		$xtransform = array();
		//check if an array is passed as arg
		if($args>1 && is_array(func_get_arg(1))){
			$xtransform = func_get_arg(1);
			array_shift($xtransform);
			foreach($xtransform as &$xa){
				$xa = ($xa == "" && $xa !== floatval(0) && $xa !== intval(0) ? null : $xa);
			}
		}
		else{
			for( $i=1; $i<$args; $i++){
				$a = func_get_arg( $i );
				$xtransform[] = $a === "" ?null : $a;
			}
		}

		if ( self::$queryCollecting === true && strpos( $query, "SELECT " ) === false ) {
			foreach ( $xtransform as $x2=>$x3 ) {
				if(is_string($x3))
					$xtransform[ $x2 ] = self::quote( $x3 );
				elseif(empty($x3))
					$xtransform[ $x2 ] = "null";
			}

			array_unshift($xtransform, str_replace("?","%s",$query));

			$query = call_user_func_array('sprintf', $xtransform);

			self::$queryCollection[ ] = $query;
			return "";
		}


//		var_dump( $xtransform );
		$a = self::getConnection();
		if($a==false) return false;

		if(self::$utfExecuted==false){
			self::$utfExecuted = true;
		}

		$rs = null;

		#for db driver
		self::prepareQuery( $query );
		if(self::$debugQuery){
			//print_r( $xtransform );
			$xtDebug = $xtransform;
			array_unshift($xtDebug, str_replace("?","%s",$query));

		    $debugQ = call_user_func_array('sprintf', $xtDebug);

			echo PHP_EOL."
			<PRE>
			<< DEBUG QUERY:".PHP_EOL;
			echo $debugQ;
			echo PHP_EOL."END DEBUG QUERY >>
			</PRE>
			".PHP_EOL;
		}


		if ($stmt = self::getConnection()->prepare($query)) {
			if ( ( $exserr = self::executePreparedStatement( $stmt, $rs, $xtransform, $query ) ) === true )
				return $rs;
			else
				return $exserr;
		}
		elseif(!is_file($__DP."/site/def/state/building-1")) {
			if(!JUtil::isProduction()) {
				echo 'Buggy query (2): ' . $query . '<br />';
				die();
			}
			JLog::log("user",'Buggy query (2): ' . $query);
			JUtil::siteHalt("PDOQ");
		}
	}

	public static function executeQueryBatch( array $sqls ) {
		foreach ( $sqls as $sql ) {
			self::executeQuery( $sql );
		}

	}

	/**
	 * Quote string
	 * @param $string
	 * @return string
	 * @author Özgür Köy
	 */
	public static function quote( $string ) {
		return self::getConnection()->quote( $string );
	}

	/**
	 * shortcut to executeQuery
	 *
	 * @return bool
	 * @author Özgür Köy
	 **/
	public static function q($query)
	{
		return self::executeQuery($query);
	}

	/**
	 * Init a PDO Transaction
	 * TODO : ADD ERROR HANDLER
	 */
	public static function beginTransaction() {
		$onlyThese = func_get_args();

		if ( sizeof( $onlyThese ) > 0 )
			self::$transactionCollection[ ] = $onlyThese;

		foreach (self::$connections as $dsn => $connection) {
			if ( ( is_array( $onlyThese ) && sizeof($onlyThese) > 0 && !in_array( $dsn, $onlyThese ) ) || in_array( $dsn, self::$transactionExclusion ) )
				continue;

			if ( $connection[ "transaction" ] == 0 ) {
//				self::$connection = self::$connections[ $dsn ];
//				self::executeQuery( "SET foreign_key_checks = 0;" );
				self::startTransaction( $connection[ "connection" ] );
				self::$connections[ $dsn ][ "transaction" ] = 1;
			}
		}
		self::$isTransAction = 1;

		//echo "\n In Begin Transaction: \n";
		//print_r(self::$connections);
	}
	
	/**
	 * Init a PDO Transaction
	 */
	private static function startTransaction($connection=null) {
		if(is_null($connection)) $connection = self::getConnection();
		
		if ($connection->beginTransaction()==false) {
			echo "\nPDO::errorInfo1(): \n";
			//print_r($connection->errorInfo());
			return false;
		}
//		echo "TRANSACTION STARTED";
		//echo "\nPDO::errorInfo2(): \n";
		//print_r($connection->errorInfo());
		return true;
		// }
	}
	/**
	 * Commit a transaction
	 *
	 */
	public static function commit() {
		if(self::$isTransAction!=1) return false;
		foreach (self::$connections as $dsn => $connection) {
			if ($connection["transaction"]==1) {
				
				$commitResult = $connection["connection"]->commit();
//				self::$connection = self::$connections[ $dsn ];
//				self::executeQuery( "SET foreign_key_checks = 1;" );

				if ($commitResult!==false) {
					self::$connections[$dsn]["transaction"] = 0;
				}
				else{
					$error = $connection[ "connection" ]->errorInfo();
					throw new JError("commit error : ".$error[2]);

					JLog::log("user","PDO commit error:".$connection["connection"]->errorInfo());
				}
			}
			else{
				JLog::log("user","No active transaction");
//				throw new JError("No active transaction for connection : ".print_r($connection,true));
			}
		}
		
		self::$isTransAction = 0;
		self::$transactionCollection = array();

		//echo "\n In Commit Transaction: \n";
		//print_r(self::$connections);
		
	}
	
	/**
	 * Rollback a transaction
	ADD ERROR HANDLER
	 *
	 */
	public static function rollback() {
		if(self::$isTransAction!=1) return false;
		
		foreach (self::$connections as $dsn => $connection) {
			if ($connection["transaction"]==1) {
				$rollbackResult = $connection["connection"]->rollback();
				if ($rollbackResult!==false) {
					self::$connections[$dsn]["transaction"] = 0;
				} else {
					JLog::log("user","PDO rollback error:".$connection["connection"]->errorInfo());
				}
			}
		}
		self::$transactionCollection = array();
		self::$isTransAction = 0;
	}

	public static function excludeFromTransaction( $dsn ) {
		if ( !in_array( $dsn, self::$transactionExclusion ) )
			self::$transactionExclusion[ ] = $dsn;

		if ( isset( self::$connections[ $dsn ] ) && self::$connections[ $dsn ]["transaction"] == 1 ) {
			self::$connections[ $dsn ][ "connection" ]->commit();
			self::$connections[ $dsn ][ "transaction" ] = 0;
		}
	}

	/**
	 * column count
	 *
	 * @param String $query
	 */
	public static function count() {
		if (self::getConnection()) {
			return self::$effected;//self::getConnection()->rowCount();
		}
		else
			return false;
	}

	/**
	 * Execute a prepared statement
	 * it is used in executeQuery method
	 *
	 * @param PDOStatement Object $stmt
	 * @param Array $row
	 * @return boolean
	 */
	private static function executePreparedStatement( PDOStatement $stmt, &$row = null, $statements, $query ) {
		global $__DP;
		$boReturn = false;
		// if(self::$isTransAction){
		// 	echo 88;
		// 	self::startTransaction();
		// }

		$a = $stmt->execute( $statements );
		if ( $a ===true ) {
			if ( $row = $stmt->fetchAll() ) {
				$boReturn = true;
			}
			else {
				$boReturn = false;
			}
			self::$lastExecutionTime = time();
		}
		else {
			$boReturn = false;
			if( !JUtil::isProduction() && self::$hideError == false ) {
				if ( self::$returnError === true )
					return self::analyseError( $stmt->errorInfo() );


				echo "JPDO ERROR (2) : dsn : " . self::$dsn . " " . $query . "-" . serialize( $statements ) . ' Code:' . $stmt->errorCode() . ' Info:' . print_r( $stmt->errorInfo() );
//				print_r( get_included_files() );
				print_r(debug_backtrace());

				die();
			}
			if ( !is_file( $__DP . "site/def/state/building-1" ) && self::$hideError == false ) {
				JLog::log( "gen", "JPDO ERROR  : dsn : " . self::$dsn . " " . $query . "-" . serialize( $statements ) . ' Code:' . $stmt->errorCode() . ' Info:' . print_r( $stmt->errorInfo(), true ) );
				JUtil::siteError( "PDOQ" );
			}
		}
		self::$effected = $stmt->rowCount();

		return $boReturn;
	}



}
?>