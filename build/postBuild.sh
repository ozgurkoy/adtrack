#!/bin/bash

touch /home/$1/public_html/site/def/localConfig.php
chmod 777 /home/$1/public_html/site/def/localConfig.php
chmod -R 777 /home/$1/public_html/site/def
sudo chown -R $1:www-data /home/$1/public_html/*
sudo chown -R $1:www-data /home/$1/static
sudo chmod -R 775 /home/$1/public_html/*
sudo chmod -R 775 /home/$1/static
sudo service apache2 graceful
curl "$2/core/run/build/build.php"